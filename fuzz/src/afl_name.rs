#[macro_use]
extern crate afl;

mod name;

fn main() {
    #[rustfmt::skip]
    fuzz!(|input: &[u8]| { name::fuzz(input) });
}
