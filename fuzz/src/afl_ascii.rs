#[macro_use]
extern crate afl;

mod ascii;

fn main() {
    #[rustfmt::skip]
    fuzz!(|input: &[u8]| { ascii::fuzz(input) });
}
