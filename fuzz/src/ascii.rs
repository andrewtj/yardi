use yardi::{
    ascii::{
        parse::{self, Entry},
        Context,
    },
    msg::Rr,
    zone::{OriginDirective, TtlDirective, ZoneEntry},
};

pub fn fuzz(mut input: &[u8]) {
    let mut c = Context::default();
    let mut cur_pos = parse::Position::default();
    while !input.is_empty() {
        let value = match parse::from_entry::<ZoneEntry>(input, cur_pos, &c, true) {
            Ok(Entry::End) => return,
            Ok(Entry::Value { read, pos, value }) => {
                input = &input[read..];
                cur_pos = pos;
                value
            }
            Ok(Entry::Empty { read, pos }) => {
                input = &input[read..];
                cur_pos = pos;
                continue;
            }
            Ok(Entry::Short) => unreachable!(),
            Err(_) => return,
        };
        match value {
            ZoneEntry::Origin(OriginDirective(origin)) => {
                c.set_origin(origin);
            }
            ZoneEntry::Include(_) => (),
            ZoneEntry::Rr(Rr { name, .. }) => {
                c.set_last_owner(name);
            }
            ZoneEntry::Ttl(TtlDirective(ttl)) => {
                c.set_default_ttl(ttl);
            }
        }
    }
}
