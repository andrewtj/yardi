use yardi::{
    ascii,
    datatypes::{Class, Ty},
    rdata::{self, Generic},
    wire::{Reader, Unpack, Writer},
};

pub fn fuzz(input: &[u8]) {
    let mut r = Reader::new(input);
    let (cl, ty): (Class, Ty) = match Unpack::unpack(&mut r) {
        Ok(tup) => tup,
        Err(_) => return,
    };
    let rdata_bytes = match r.get_bytes_rest() {
        Ok(bytes) => bytes,
        Err(_) => return,
    };
    let mut temp_string = String::with_capacity(0xFFFF);
    let mut temp_bytes = vec![0u8; 0xFFFF];
    let r = Reader::new(rdata_bytes);
    let unpacked = match rdata::unpacked_len(cl, ty, &mut r.clone()) {
        Ok(rdlen) => {
            assert!(
                cl.is_data() && ty.is_data(),
                "unpacked_len succeded for non data ty or cl: {}/{}\n{:?}",
                ty,
                cl,
                rdata_bytes
            );
            assert!(rdlen <= 0xFFFF, "rdlen > 0xFFFF");
            let mut w = Writer::new(&mut temp_bytes);
            if let Err(err) = rdata::copy(cl, ty, &mut r.clone(), &mut w) {
                panic!(
                    "copy failed after unpacked_len succeeded for {}/{}: {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            true
        }
        Err(_) => {
            let mut w = Writer::new(&mut temp_bytes);
            if rdata::copy(cl, ty, &mut r.clone(), &mut w).is_ok() {
                panic!(
                    "copy succeeded after unpacked_len failed for {}/{}\n{:?}",
                    cl, ty, rdata_bytes
                );
            }
            false
        }
    };
    match <Generic>::unpack(cl, ty, &mut r.clone()) {
        Ok(ref rdata) if unpacked => {
            let mut w = Writer::new(&mut temp_bytes);
            if let Err(err) = rdata.pack(&mut w) {
                panic!(
                    "Generic pack failed for {}/{} {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            if let Err(err) = ascii::present::to_string(rdata, &mut temp_string) {
                panic!(
                    "Failed to present for {}/{} {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            match Generic::from_str(cl, ty, &temp_string) {
                Ok(rdata2) => {
                    assert_eq!(
                        *rdata, rdata2,
                        "Parse/present mismatch for {}/{}\n{:?}",
                        cl, ty, rdata_bytes
                    );
                }
                Err(err) => {
                    panic!(
                        "Failed to parse what was presented for {}/{}: {:?}\n{:?}",
                        cl, ty, err, rdata_bytes
                    );
                }
            }
        }
        Ok(_) => {
            panic!(
                "Generic::unpack succeeded when it shouldn't have {}/{}\n{:?}",
                cl, ty, rdata_bytes
            );
        }
        Err(ref err) if unpacked => {
            panic!(
                "Generic::unpack failed when it shouldn't have {}/{}: {:?}\n{:?}",
                cl, ty, err, rdata_bytes
            );
        }
        Err(_) => (),
    }
}
