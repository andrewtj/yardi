#[macro_use]
extern crate honggfuzz;

mod rdata_wire;

fn main() {
    loop {
        #[rustfmt::skip]
        fuzz!(|input: &[u8]| { rdata_wire::fuzz(input) });
    }
}
