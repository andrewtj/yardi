#[macro_use]
extern crate afl;

mod rdata_wire;

fn main() {
    #[rustfmt::skip]
    fuzz!(|input: &[u8]| { rdata_wire::fuzz(input) });
}
