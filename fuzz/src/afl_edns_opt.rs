#[macro_use]
extern crate afl;

mod edns_opt;

fn main() {
    #[rustfmt::skip]
    fuzz!(|input: &[u8]| { edns_opt::fuzz(input) });
}
