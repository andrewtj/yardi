#[macro_use]
extern crate honggfuzz;

mod ascii;

fn main() {
    loop {
        #[rustfmt::skip]
        fuzz!(|input: &[u8]| { ascii::fuzz(input) });
    }
}
