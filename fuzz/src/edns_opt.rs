use yardi::{
    datatypes::{DsDigestAlg, Nsec3HashAlg},
    dnssec::SigAlg,
    msg::edns::*,
    wire::{Reader, Unpack, UnpackError},
};

pub fn fuzz(input: &[u8]) {
    let _ = fuzz_impl(input);
}

macro_rules! fuzz_impl_match {
    ($code:expr, $reader:expr, $($ty:ty),+) => {
        match $code {
            $(<$ty>::CODE => <$ty>::unpack($reader).map(|_| ()),)+
            _ => Ok(()),
        }
    }
}

#[derive(yardi::Unpack)]
struct KtNat(#[allow(unused)] KeyTagNative<Vec<u16>>);

impl KtNat {
    const CODE: OptCode = OptCode(65001);
}

#[derive(yardi::Unpack)]
struct KtNet(#[allow(unused)] KeyTagNetwork<Vec<u8>>);

impl KtNet {
    const CODE: OptCode = OptCode(65002);
}

fn fuzz_impl(input: &[u8]) -> Result<(), UnpackError> {
    let mut reader = Reader::new(input);
    loop {
        let code = OptCode::unpack(&mut reader)?;
        let mut opt_data_reader = reader.reader16()?;
        let _ = fuzz_impl_match!(
            code,
            &mut opt_data_reader,
            Chain,
            ClientSubnet<Vec<u8>>,
            Cookie,
            Dau<Vec<SigAlg>>,
            Dhu<Vec<DsDigestAlg>>,
            Expire,
            /*
            KeyTagNative<Vec<u16>>,
            KeyTagNetwork<Vec<u8>>,
            */
            KtNat,
            KtNet,
            Llq,
            N3u<Vec<Nsec3HashAlg>>,
            Nsid<Vec<u8>>,
            Owner,
            Padding<Vec<u8>>,
            TcpKeepalive,
            UpdateLease
        );
    }
}
