#[macro_use]
extern crate yardi;

use std::{fs, io::Write, path::Path, str::FromStr};

use yardi::{
    datatypes::{Class, DsDigestAlg, Nsec3HashAlg, Ty},
    dnssec::SigAlg,
    msg::edns::{Cookie, Dau, Dhu, Expire, KeyTagNative, KeyTagNetwork, Llq, N3u, Opt, OptCode},
    wire::{Pack, Writer},
};

fn main() {
    let crate_path = Path::new(env!("CARGO_MANIFEST_DIR"));
    for arg in std::env::args().skip(1) {
        match arg.as_str() {
            "ascii" => ascii(crate_path),
            "edns_opt" => edns_opt(crate_path),
            "name" => name(crate_path),
            "rdata_wire" => rdata_wire(crate_path),
            _ => panic!("bad arg: {}", arg),
        }
    }
}

fn ascii(crate_path: &Path) {
    let rdata_path = crate_path.join("../yardi/src/rdata");
    let ascii_sample_path = crate_path.join("afl_workspace/ascii/in");
    fs::create_dir_all(&ascii_sample_path).expect("create_dir_all");
    let mut sample_zone = Vec::with_capacity(0xFFFF);
    sample_zone.extend_from_slice(
        b"
$INCLUDE foo
$INCLUDE foo bah
$ORIGIN example.
$TTL 3600
",
    );
    for rdata_entry in fs::read_dir(&rdata_path).expect("read_dir") {
        let rdata_entry = rdata_entry.expect("rdata_entry");
        let rdata_path = rdata_entry.path();
        if !rdata_path.is_dir() {
            continue;
        }
        let opt_ty = rdata_path
            .file_name()
            .and_then(|os| os.to_str())
            .and_then(|s| Ty::from_str(s).ok());
        let ty = match opt_ty {
            Some(ty) if ty.is_data() => ty,
            _ => continue,
        };
        let samples = collect_rdata_ascii_samples(&rdata_path);
        for sample in samples {
            writeln!(sample_zone, " {} {}", ty, sample).expect("write sample");
        }
    }
    fs::write(ascii_sample_path.join("zone.txt"), sample_zone).expect("write sample");
}

fn edns_opt(crate_path: &Path) {
    let edns_opt_sample_path = crate_path.join("afl_workspace/edns_opt/in");
    fs::create_dir_all(&edns_opt_sample_path).expect("create_dir_all");
    let mut temp = vec![0u8; 0xFFFF];
    let mut w = Writer::new(&mut temp);
    let mut w = w.plain();
    {
        let mut c = Cookie::default();
        for sc in [&[], &[0u8; 8][..], &[0u8; 32][..]].iter() {
            c.set_server(sc).expect("set_server");
            Cookie::CODE.pack(&mut w).expect("pack cookie opt code");
            c.pack(&mut w.writer16().unwrap()).expect("pack cookie");
        }
    }
    {
        <Dau<&[SigAlg]>>::CODE
            .pack(&mut w)
            .expect("pack dau opt code");
        Dau {
            understood: [SigAlg::ED25519],
        }
        .pack(&mut w.writer16().unwrap())
        .expect("pack dau");
    }
    {
        <Dhu<&[DsDigestAlg]>>::CODE
            .pack(&mut w)
            .expect("pack dhu opt code");
        Dhu {
            understood: [DsDigestAlg::SHA_256],
        }
        .pack(&mut w.writer16().unwrap())
        .expect("pack dhu");
    }
    {
        Expire::CODE.pack(&mut w).expect("pack expire opt code");
        Expire(!0)
            .pack(&mut w.writer16().unwrap())
            .expect("pack expire");
    }
    {
        <KeyTagNative<Vec<u16>>>::CODE
            .pack(&mut w)
            .expect("pack key tag opt code");
        let mut kt = KeyTagNative::default();
        kt.push(0xDD);
        kt.pack(&mut w.writer16().unwrap()).expect("pack key tag");
    }
    {
        <KeyTagNetwork<Vec<u8>>>::CODE
            .pack(&mut w)
            .expect("pack key tag opt code");
        let mut kt = KeyTagNetwork::default();
        kt.push(0xDD);
        kt.pack(&mut w.writer16().unwrap()).expect("pack key tag");
    }
    {
        Llq::CODE.pack(&mut w).expect("pack llq opt code");
        Llq::default()
            .pack(&mut w.writer16().unwrap())
            .expect("pack llq");
    }
    {
        <N3u<&[Nsec3HashAlg]>>::CODE
            .pack(&mut w)
            .expect("pack n3u opt code");
        N3u {
            understood: [Nsec3HashAlg::SHA_1],
        }
        .pack(&mut w.writer16().unwrap())
        .expect("pack n3u");
    }
    {
        OptCode::OWNER.pack(&mut w).expect("pack owner opt code");
        w.write_bytes16(&[0u8; 20][..]).expect("pack owner");
    }
    {
        OptCode::TCP_KEEPALIVE
            .pack(&mut w)
            .expect("pack tcp keepalive opt code");
        0xCAFEu16
            .pack(&mut w.writer16().unwrap())
            .expect("pack tcp keepalive");
    }
    for key_lease in &[None, Some(!0u32)] {
        OptCode::UPDATE_LEASE
            .pack(&mut w)
            .expect("pack update lease opt code");
        let mut w = w.writer16().unwrap();
        0u32.pack(&mut w).expect("pack update lease - lease");
        key_lease
            .pack(&mut w)
            .expect("pack update lease - key lease");
    }
    OptCode::CHAIN.pack(&mut w).expect("pack chain opt code");
    name!("x.")
        .pack(&mut w.writer16().unwrap())
        .expect("pack chain opt");
    OptCode::PADDING
        .pack(&mut w)
        .expect("pack padding opt code");
    w.write_bytes16(b"padding").expect("pack padding");
    OptCode::NSID.pack(&mut w).expect("pack nsid opt code");
    w.write_bytes16(b"nsid").expect("pack nsid");
    OptCode::CLIENT_SUBNET
        .pack(&mut w)
        .expect("pack client subnet opt code");
    w.write_bytes16(&[0x00, 0x00, 8, 7, 0xFF << 1])
        .expect("pack client subnet");
    fs::write(edns_opt_sample_path.join("sample.bin"), w.written()).expect("write edns opts");
}

fn name(crate_path: &Path) {
    let name_sample_path = crate_path.join("afl_workspace/name/in");
    fs::create_dir_all(&name_sample_path).expect("create_dir_all");
    let mut temp = vec![0u8; 0xFF];
    let mut w = Writer::new(&mut temp);
    for name in name!("a.b.c.").names() {
        name.pack(&mut w).expect("pack name");
    }
    fs::write(name_sample_path.join("sample.bin"), w.written()).expect("write name sample");
}

fn rdata_wire(crate_path: &Path) {
    let rdata_path = crate_path.join("../yardi/src/rdata");
    let rdata_sample_path = crate_path.join("afl_workspace/rdata_wire/in");
    fs::create_dir_all(&rdata_sample_path).expect("create_dir_all");
    for rdata_entry in fs::read_dir(&rdata_path).expect("read_dir") {
        let rdata_entry = rdata_entry.expect("rdata_entry");
        let rdata_path = rdata_entry.path();
        if !rdata_path.is_dir() {
            continue;
        }
        let opt_ty = rdata_path
            .file_name()
            .and_then(|os| os.to_str())
            .and_then(|s| Ty::from_str(s).ok());
        let ty = match opt_ty {
            Some(ty) => ty,
            None => continue,
        };
        let samples = collect_rdata_wire_samples(&rdata_path);
        for (i, sample) in samples.iter().enumerate() {
            let mut temp = vec![0u8; 4 + 0xFFFF];
            let mut w = Writer::new(&mut temp);
            (Class::IN, ty).pack(&mut w).expect("pack cl/ty");
            w.write_bytes(&sample[..]).expect("write sample");
            let sample_file_name = format!("{}_{:03}.bin", ty, i);
            let sample_path = rdata_sample_path.join(&sample_file_name);
            fs::write(&sample_path, w.written()).expect("write sample");
        }
    }
}

fn collect_rdata_wire_samples(path: &Path) -> Vec<Vec<u8>> {
    let mut v = Vec::new();
    for entry in fs::read_dir(path).expect("read_dir") {
        let entry = entry.expect("entry");
        let path = entry.path();
        if path.extension().and_then(|os| os.to_str()) != Some("bin") {
            continue;
        }
        v.push(fs::read(path).expect("read"));
    }
    v
}

fn collect_rdata_ascii_samples(path: &Path) -> Vec<String> {
    let mut v = Vec::new();
    for entry in fs::read_dir(path).expect("read_dir") {
        let entry = entry.expect("entry");
        let path = entry.path();
        if path.extension().and_then(|os| os.to_str()) != Some("txt") {
            continue;
        }
        v.push(fs::read_to_string(path).expect("read"));
    }
    v
}
