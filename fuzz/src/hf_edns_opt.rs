#[macro_use]
extern crate honggfuzz;

mod edns_opt;

fn main() {
    loop {
        #[rustfmt::skip]
        fuzz!(|input: &[u8]| { edns_opt::fuzz(input) });
    }
}
