#[macro_use]
extern crate honggfuzz;

mod name;

fn main() {
    loop {
        #[rustfmt::skip]
        fuzz!(|input: &[u8]| { name::fuzz(input) });
    }
}
