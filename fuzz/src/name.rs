use yardi::datatypes::Name;
use yardi::wire::{Pack, Reader, Unpack, Writer};

pub fn fuzz(input: &[u8]) {
    let mut r = Reader::new(input);
    while !r.is_empty() {
        let n = <Name>::unpack(&mut r).ok();
        let b = match r.get_bytes8() {
            Ok(b) => b,
            Err(_) => continue,
        };
        if let Some(n) = n.as_ref() {
            let mut t = vec![0u8; n.len()];
            let mut w = Writer::new(&mut t);
            assert!(n.pack(&mut w).is_ok());
            let mut r2 = Reader::new(w.written());
            assert_eq!(Name::unpack(&mut r2).expect("r2"), n);
        }
        let s = if let Ok(s) = std::str::from_utf8(b) {
            s
        } else {
            continue;
        };
        if let Ok(n) = Name::from_str_origin(s, n.as_ref()) {
            let mut t = vec![0u8; n.len()];
            let mut w = Writer::new(&mut t);
            assert!(n.pack(&mut w).is_ok());
            let nb = w.written();
            assert_eq!(nb.len(), n.len());
            let mut r = Reader::new(nb);
            let n2 = <Name>::unpack(&mut r).expect("n2");
            assert_eq!(n, n2);
        }
    }
}
