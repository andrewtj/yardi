#!/usr/bin/env bash
set -eu -o pipefail
cd $(dirname "${0}")
usage() {
    local TARGETS=$(sed -n -E 's/^name = "hf_([a-z_]*)".*/    \1/gp' Cargo.toml)
    cat >&2 << EOF
usage: $0 target [run|debug]

  where target is one of:
$TARGETS
EOF
}
if [[ $# -lt 2 ]]; then usage; exit 1; fi
TARGET="${1}"
TARGET_BIN="hf_${1}"
if [[ `grep -c "name = \"${TARGET_BIN}\"" Cargo.toml` -ne "1" ]]; then usage; exit 1; fi
case "${2}" in
    run)
        HFUZZ_RUN_ARGS="--quiet --exit_upon_crash --run_time 600" cargo hfuzz run "${TARGET_BIN}"
        ;;
    debug)
        cargo hfuzz run-debug "${TARGET_BIN}" hfuzz_workspace/${TARGET_BIN}/*.fuzz
        ;;
    *)
        usage
        exit 1
esac

