#!/usr/bin/env bash
set -eu -o pipefail
cd $(dirname "${0}")
usage() {
    local TARGETS=$(sed -n -E 's/^name = "afl_([a-z_]*)".*/    \1/gp' Cargo.toml)
    cat >&2 << EOF
usage: $0 target [start|resume]

  where target is one of:
$TARGETS
EOF
}
if [[ $# -lt 2 ]]; then usage; exit 1; fi
TARGET="${1}"
TARGET_BIN="afl_${1}"
if [[ `grep -c "name = \"${TARGET_BIN}\"" Cargo.toml` -ne "1" ]]; then usage; exit 1; fi
AFL_WORKSPACE="afl_workspace/${TARGET}"
AFL_IN="${AFL_WORKSPACE}/in"
AFL_OUT="${AFL_WORKSPACE}/out"
case "${2}" in
    start)
        test -d "${AFL_IN}" || cargo run --bin build_afl_input "${TARGET}"
        mkdir -p "${AFL_OUT}"
        cargo afl build --bin "${TARGET_BIN}"
        cargo afl fuzz -i "${AFL_IN}" -o "${AFL_OUT}" "target/debug/${TARGET_BIN}"
        ;;
    resume)
        cargo afl build --bin "${TARGET_BIN}"
        cargo afl fuzz -i - -o "${AFL_OUT}" "target/debug/${TARGET_BIN}"
        ;;
    *)
        usage
        exit 1
esac

