#![warn(
    missing_debug_implementations,
    bare_trait_objects,
    unused_lifetimes,
    unused_qualifications,
    missing_docs,
    rust_2018_idioms
)]
// TODO: probably some good ideas in serde_derive
//        * https://docs.rs/crate/serde_derive/1.0.103/source/src/dummy.rs
//        * #[automatically_derived]
// TODO: check_rdlen should be reversed so it's opt-in to a speed up; rather than opt-in to correctness
//        * might be able to replace with a compile time if statement using associated const in Pack
// TODO: should check return type of closures if possible
// TODO: tests
use syn::parse_macro_input;

#[proc_macro_derive(Parse, attributes(yardi))]
pub fn derive_parse(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_parse(&input).into()
}

#[proc_macro_derive(Present, attributes(yardi))]
pub fn derive_present(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_present(&input).into()
}

#[proc_macro_derive(Pack, attributes(yardi))]
pub fn derive_pack(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_pack(&input).into()
}

#[proc_macro_derive(Unpack, attributes(yardi))]
pub fn derive_unpack(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_unpack(&input).into()
}

#[proc_macro_derive(WireOrd, attributes(yardi))]
pub fn derive_wire_ord(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_wire_ord(&input).into()
}

#[proc_macro_derive(UnpackedLen, attributes(yardi))]
pub fn derive_unpacked_len(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_unpacked_len(&input).into()
}

#[proc_macro_derive(Rdata, attributes(yardi))]
pub fn derive_rdata(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input: syn::DeriveInput = parse_macro_input!(input);
    yardi_internal::derive_rdata(&input).into()
}
