use proc_macro2::TokenStream;

use crate::const_name;
use crate::iana::TypeRegistry;
use crate::template::{AssocNumConstants, ToFromMnemonic};

pub fn build(r: &TypeRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("Ty");
    let mut mnemonic_table = ToFromMnemonic::new("Ty");
    for info in r.by_num.values() {
        let title = if let Some(desc) = info.desc.as_ref() {
            format!("{} - {}", info.mstr, desc)
        } else {
            info.mstr.clone()
        };
        let const_id = const_name(&info.mstr);
        mnemonic_table.insert(const_id.clone(), info.mstr.clone(), info.num);
        assoc_constants.insert(const_id, title, info.num, &info.refs[..]);
    }
    quote! {
        use super::Ty;
        #assoc_constants
        #mnemonic_table
    }
}
