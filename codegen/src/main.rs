#![recursion_limit = "256"]
// TODO: normalise "SHA-1" to "SHA1", etc
/*
#![warn(
    missing_debug_implementations,
    bare_trait_objects,
    unused_lifetimes,
    unused_qualifications,
    rust_2018_idioms,
)]
*/
// TODO: Generate From impls for RR that wrap a single type?
// TODO: Generate examples from samples.
// TODO: Generate reference snippets suitable for doc include.
// TODO: Generate inter-item references? Ty <-> impl

#[macro_use]
extern crate quote;

pub mod a_aaaa;
pub mod asset;
pub mod caa;
pub mod cert;
pub mod class_mod;
pub mod csync_mod;
pub mod dnskey;
pub mod ds;
pub mod edns_mod;
pub mod errors;
pub mod iana;
pub mod nsec3;
pub mod rr;
pub mod sig;
pub mod sshfp;
pub mod template;
pub mod tlsa;
pub mod tsig;
pub mod tuple_impls;
pub mod ty_mod;

use std::collections::{BTreeSet, HashMap, HashSet};
use std::fmt::Write as _;
use std::fs::{self, File};
use std::io::{BufWriter, Write};
use std::path::Path;
use std::process::Command;

use elementtree::Element;
use proc_macro2::{Span, TokenStream};

use crate::asset::Asset;

const IANA_ASSIGNMENT_NS: &str = "http://www.iana.org/assignments";
const RFC_INDEX_NS: &str = "https://www.rfc-editor.org/rfc-index";

const CLASS_REGISTRY: &str = "dns-parameters-2";
const RRTYPE_REGISTRY: &str = "dns-parameters-4";
const OPCODE_REGISTRY: &str = "dns-parameters-5";
const RCODE_REGISTRY: &str = "dns-parameters-6";
const _AFSDB_REGISTRY: &str = "dns-parameters-8";
const _DHCID_REGISTRY: &str = "dns-parameters-9";
const _LABEL_REGISTRY: &str = "dns-parameters-10";
const EOPT_REGISTRY: &str = "dns-parameters-11";
const EDE_REGISTRY: &str = "extended-dns-error-codes";
const _HEADER_REGISTRY: &str = "dns-parameters-12";
const EHEADER_REGISTRY: &str = "dns-parameters-13";
const _EVERSION_REGISTRY: &str = "dns-parameters-14";
const CSYNC_REGISTRY: &str = "csync-flags";
const IPSECKEYALG_REGISTRY: &str = "ipseckey-rr-parameters-1";

const MANUAL_RR: &[(&str, Option<u16>)] = &[
    ("HIP", None),
    ("IPSECKEY", None),
    ("LOC", None),
    ("WKS", Some(1)),
];

fn build_rfc_index() -> HashMap<String, String> {
    let fh = Asset::RfcIndex.open();
    let mut hm = HashMap::new();
    let root = Element::from_reader(fh).expect("root");
    for entry in root.find_all((RFC_INDEX_NS, "rfc-entry")) {
        let doc_id = entry
            .find((RFC_INDEX_NS, "doc-id"))
            .expect("doc-id")
            .text()
            .to_string();
        let title = entry
            .find((RFC_INDEX_NS, "title"))
            .expect("title")
            .text()
            .to_string();
        assert!(hm.insert(doc_id, title).is_none());
    }
    assert!(!hm.is_empty());
    hm
}

pub fn const_name<T: AsRef<str>>(mnemonic: T) -> syn::Ident {
    let mnemonic = mnemonic.as_ref();
    let mut ident = mnemonic.as_bytes().to_vec();
    for (i, b) in ident.iter_mut().enumerate() {
        match *b {
            b'A'..=b'Z' | b'_' => (),
            b'0'..=b'9' if i > 0 => (),
            b'a'..=b'z' => *b ^= 0x20,
            b'-' | b' ' | b'.' => *b = b'_',
            _ => panic!(
                "unexpected mnemonic char {:?}({}) in {}",
                *b as char, i, mnemonic
            ),
        }
    }
    syn::Ident::new(std::str::from_utf8(&ident).unwrap(), Span::call_site())
}

fn struct_name(mnemonic: &str) -> syn::Ident {
    let mut ident = String::with_capacity(mnemonic.len());
    for p in mnemonic.split('-') {
        for t in p.as_bytes().iter().cloned().enumerate() {
            match t {
                (0, b @ b'A'..=b'Z') | (_, b @ b'0'..=b'9') => ident.push(b as char),
                (_, b @ b'A'..=b'Z') => ident.push((b + (b'a' - b'A')) as char),
                (_, b) => panic!("didn't expect: {:?}", b as char),
            }
        }
    }
    syn::Ident::new(&ident, Span::call_site())
}

fn mod_name(mnemonic: &str) -> String {
    let mut m = String::with_capacity(mnemonic.len());
    for (i, p) in mnemonic.split('-').enumerate() {
        if i != 0 {
            m.push('_');
        }
        for t in p.as_bytes().iter().cloned().enumerate() {
            match t {
                (_, b @ b'0'..=b'9') => m.push(b as char),
                (_, b @ b'A'..=b'Z') => m.push((b + (b'a' - b'A')) as char),
                (_, b) => panic!("didn't expect: {:?}", b as char),
            }
        }
    }
    m
}

fn member_name(s: &str) -> syn::Ident {
    let accept = match s {
        "Certificate Association Data" => return syn::Ident::new("ca_data", Span::call_site()),
        "Signature Expiration" => return syn::Ident::new("expire", Span::call_site()),
        "Signature Inception" => return syn::Ident::new("incept", Span::call_site()),
        other => other
            .as_bytes()
            .iter()
            .cloned()
            .enumerate()
            .all(|(i, b)| match b {
                b'A'..=b'Z' | b'a'..=b'z' => true,
                b' ' | b'0'..=b'9' | b'\'' | b'-' => i > 0,
                _ => false,
            }),
    };
    if !accept {
        panic!("unacceptable member_name input: {}", s)
    }
    let mut out = String::with_capacity(s.len());
    for (i, s) in s.split(|c| c == ' ' || c == '-').enumerate() {
        if i > 0 {
            out.push('_');
        }
        match s.to_lowercase().replace('\'', "").as_str() {
            "certificate" => out.push_str("cert"),
            "algorithm" => out.push_str("alg"),
            "type" => out.push_str("ty"),
            other => out.push_str(other),
        }
    }
    syn::Ident::new(&out, Span::call_site())
}

fn generate_dnssec_tests() -> String {
    let bits = [1024, 1280, 2040, 2048, 3072, 4096];
    let tests = [
        ("rsasha1", 1024, 5000),
        ("rsasha256", 1024, 2048),
        ("rsasha512", 1024, 2048),
    ];
    let mut out = String::new();
    for (alg, verify_min, verify_max) in tests {
        for bits in bits {
            if bits >= verify_min {
                writeln!(
                    out,
                    r#"#[test]
                #[cfg_attr(miri, ignore)]
                fn verify_{alg}_{bits}() {{
                    dnssec_verify_test!("{alg}_{bits}_rr.txt");
                }}"#,
                    alg = alg,
                    bits = bits
                )
                .unwrap();
            }
            if bits >= verify_max {
                writeln!(
                    out,
                    r#"#[test]
                #[cfg_attr(miri, ignore)]
                fn sign_{alg}_{bits}() {{
                    dnssec_sign_test!("{alg}_{bits}_rr.txt", "{alg}_{bits}_private.txt");
                }}"#,
                    alg = alg,
                    bits = bits
                )
                .unwrap();
            }
        }
    }
    out
}

fn generate_opcode_constants(r: &iana::OpcodeRegistry) -> TokenStream {
    let mut assoc_constants = template::AssocNumConstants::new("Opcode");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in &r.0 {
        let title = if let Some(desc) = info.desc.as_ref() {
            format!("{} - {}", info.mstr, desc)
        } else {
            info.mstr.clone()
        };
        let const_id = const_name(&info.mstr);
        let debug_str = &info.mstr;
        mnemonics.push(quote! { (Opcode::#const_id, #debug_str) });
        assoc_constants.insert(const_id.clone(), title.clone(), info.num, &info.refs[..]);
    }
    quote! {
        use super::Opcode;

        #assoc_constants

        pub(super) const MNEMONICS: &'static [(Opcode, &'static str)] = &[
            #(#mnemonics),*
        ];
    }
}

fn generate_rcode_constants(r: &iana::RcodeRegistry) -> TokenStream {
    let mut assoc_constants = template::AssocNumConstants::new("Rcode");
    let mut mnemonics = Vec::with_capacity(r.std.len());
    for info in r.std.values() {
        let title = format!("{} - {}", info.mstr, info.desc);
        let const_id = const_name(&info.mstr);
        let debug_str = &info.mstr;
        mnemonics.push(quote! { (Rcode::#const_id, #debug_str) });
        assert!(info.num <= 0xFF);
        let num = info.num as u8;
        assoc_constants.insert(const_id, title, num, &info.refs[..]);
    }
    quote! {
        use super::Rcode;

        #assoc_constants

        pub(super) const MNEMONICS: &'static [(Rcode, &'static str)] = &[
            #(#mnemonics),*
        ];
    }
}

fn generate_dnssec_sigalg_constants(r: &iana::DnssecAlgRegistry) -> TokenStream {
    let mut assoc_constants = template::AssocNumConstants::new("SigAlg");
    let mut mnemonic_table = template::ToFromMnemonic::new("SigAlg");
    mnemonic_table.no_to_str();
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in &r.0 {
        let title = info.mstr.to_string();
        let const_id = const_name(&info.mstr);
        let str_const_id = const_name(format!("{} STR", &info.mstr));
        mnemonics.push(quote! { (SigAlg::#const_id, #str_const_id) });
        mnemonic_table.insert(const_id.clone(), info.mstr.clone(), info.num);
        assoc_constants.insert(const_id, title, info.num, &info.refs[..]);
    }
    quote! {
        use super::SigAlg;

        #assoc_constants

        #mnemonic_table

        pub(super) const MNEMONICS: &'static [(SigAlg, &str)] = &[
            #(#mnemonics),*
        ];
    }
}

fn generate_ednscode_mod(r: &iana::EdnsOptRegistry) -> TokenStream {
    let mut assoc_constants = template::AssocNumConstants::new("OptCode");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in r.0.iter() {
        let title = info.name.clone();
        let const_id = const_name(&title);
        let debug_str = &info.name;
        mnemonics.push(quote! { (OptCode::#const_id, #debug_str) });

        assoc_constants.insert(const_id, title, info.value, &info.refs[..]);
    }
    quote! {
        use super::OptCode;

        #assoc_constants

        pub(super) const MNEMONICS: &'static [(OptCode, &'static str)] = &[
            #(#mnemonics),*
        ];
    }
}

fn generate_edns_rcode_mod(r: &iana::RcodeRegistry) -> TokenStream {
    let mut assoc_constants = template::AssocNumConstants::new("ExtRcode");
    let mut mnemonics = Vec::with_capacity(r.edns.len());
    for info in r.edns.values() {
        let title = format!("{} - {}", info.mstr, info.desc);
        let const_id = const_name(&info.mstr);
        let debug_str = &info.mstr;
        mnemonics.push(quote! { (ExtRcode::#const_id, #debug_str) });
        assoc_constants.insert(const_id, title, info.num, &info.refs[..]);
    }
    // TODO: Should use From/TryFrom impls instead.
    // TODO: Are these shifts correct?
    quote! {
        /// Extended RCODE.
        #[derive(Clone, Copy, Hash, Eq, PartialEq)]
        pub struct ExtRcode(u16);
        impl ExtRcode {
            /// Return the standard portion.
            pub fn std(self) -> crate::msg::Rcode {
                crate::msg::Rcode::from_u8((0xF & self.0) as u8).unwrap()
            }
            /// Return the extended bits.
            pub fn extended_bits(self) -> u8 {
                (self.0 >> 4) as u8
            }
            /// Construct from an `Rcode` and a byte.
            pub fn from(rcode: crate::msg::Rcode, bits: u8) -> ExtRcode {
                ExtRcode(u16::from(bits) << 4 | u16::from(rcode.to_u8()))
            }
            /// Construct from a u16.
            pub fn from_u16(rcode: u16) -> Option<ExtRcode> {
                if rcode <= 0xFFF {
                    Some(ExtRcode(rcode))
                } else {
                    None
                }
            }
            /// Convert to a u16.
            pub fn to_u16(self) -> u16 {
                self.0
            }
        }

        impl core::fmt::Debug for ExtRcode {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(c, m) in MNEMONICS {
                    if c == *self {
                        return write!(f, "ExtRcode({})", m);
                    }
                }
                write!(f, "ExtRcode({})", self.0)
            }
        }

        #assoc_constants

        pub(super) const MNEMONICS: &[(ExtRcode, &str)] = &[
            #(#mnemonics),*
        ];
    }
}

fn generate_svcb_param_key_constants(r: &iana::SvcParamKeyRegistry) -> TokenStream {
    let mut assoc_constants = template::AssocNumConstants::new("SvcbParamKey");
    let mut mnemonic_table = template::ToFromMnemonic::new("SvcbParamKey");
    mnemonic_table.sensistive_str();
    for info in &r.0 {
        let title = info.name.as_str();
        let const_id = const_name(title);
        let str_const_id = const_name(format!("{}_STR", title));
        mnemonic_table.insert(const_id.clone(), title.to_string(), info.value);
        assoc_constants.insert(const_id, title.to_string(), info.value, &info.refs[..]);
    }
    quote! {
        use super::SvcbParamKey;

        #assoc_constants

        #mnemonic_table
    }
}

fn get_registry<'a>(element: &'a Element, id: &str) -> &'a Element {
    element
        .find_all((IANA_ASSIGNMENT_NS, "registry"))
        .find(|r| r.get_attr("id") == Some(id))
        .expect(id)
}

fn write_file<T, U>(path: T, content: U)
where
    T: AsRef<Path> + std::fmt::Debug,
    U: AsRef<[u8]>,
{
    let path = path.as_ref();
    if let Ok(mut fh) = File::open(path) {
        let mut permissions = fh.metadata().unwrap().permissions();
        permissions.set_readonly(false);
        fh.set_permissions(permissions).unwrap();
    }
    let mut fh = match File::create(path) {
        Ok(fh) => BufWriter::new(fh),
        Err(err) => panic!("Failed to create {:?}: {:?}", path, err),
    };
    let warning = b"
/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
";
    for s in &[warning, content.as_ref(), warning] {
        if let Err(err) = fh.write_all(s) {
            panic!("Failed to write {:?}: {:?}", path, err);
        }
    }
    if let Err(err) = fh.flush() {
        panic!("Failed to flush {:?}: {:?}", path, err);
    }
    drop(fh);
    if path.extension().and_then(|os| os.to_str()) == Some("rs") {
        let status = Command::new("rustfmt")
            .args(["--edition", "2018"])
            .arg(path)
            .status()
            .expect("failed to execute rustfmt");
        //assert!(status.success(), "rustfmt failed");
        if !status.success() {
            eprintln!("rustfmt failed for {}", path.display());
        }
    }
    let fh = File::open(path).unwrap();
    let mut permissions = fh.metadata().unwrap().permissions();
    permissions.set_readonly(true);
    fh.set_permissions(permissions).unwrap();
}

pub static CODEGEN_PATH: &str = env!("CARGO_MANIFEST_DIR");

fn main() {
    let mut update = false;
    for arg in std::env::args().skip(1) {
        match arg.as_ref() {
            "update" => update = true,
            unknown => panic!("unknown arg: {}", unknown),
        }
    }
    if update && !asset::update() {
        return;
    }

    let codegen_path = Path::new(env!("CARGO_MANIFEST_DIR"));
    let workspace_path = codegen_path.parent().expect("workspace path");
    let yardi_path = workspace_path.join("yardi/src");
    assert!(yardi_path.is_dir());

    let rfc_index = build_rfc_index();
    let params_fh = Asset::DnsParameters.open();
    let params_root = Element::from_reader(params_fh).expect("root");

    let people = iana::rf::People::new(&params_root);

    let ednsoptreg = iana::EdnsOptRegistry::new(
        get_registry(&params_root, EOPT_REGISTRY),
        &rfc_index,
        &people,
    );
    let ednscode_mod = generate_ednscode_mod(&ednsoptreg).to_string();
    write_file(
        yardi_path.join("msg/edns/opt_code/generated.rs"),
        &ednscode_mod,
    );

    let edereg = iana::EdeRegistry::new(
        get_registry(&params_root, EDE_REGISTRY),
        &rfc_index,
        &people,
    );
    {
        let mut assoc_constants = template::AssocNumConstants::new("InfoCode");
        let mut mnemonics = Vec::new();
        for info in &edereg.0[..] {
            let id = const_name(&info.description);
            let debug_str = &info.description;
            mnemonics.push(quote! { (InfoCode::#id, #debug_str) });
            assoc_constants.insert(
                id.clone(),
                info.description.clone(),
                info.value,
                &info.refs[..],
            );
        }
        let ede_mod = quote! {
            use super::InfoCode;

            #assoc_constants
            pub(super) const MNEMONICS: &'static [(InfoCode, &str)] = &[
                #(#mnemonics),*
            ];
        }
        .to_string();
        write_file(yardi_path.join("msg/edns/ede/generated.rs"), &ede_mod);
    }

    let opcodereg = iana::OpcodeRegistry::new(
        get_registry(&params_root, OPCODE_REGISTRY),
        &rfc_index,
        &people,
    );
    let opcode_mod = generate_opcode_constants(&opcodereg).to_string();
    write_file(yardi_path.join("msg/opcode/generated.rs"), &opcode_mod);

    let rcodereg = iana::RcodeRegistry::new(
        get_registry(&params_root, RCODE_REGISTRY),
        &rfc_index,
        &people,
    );
    let rcode_mod = generate_rcode_constants(&rcodereg).to_string();
    write_file(yardi_path.join("msg/rcode/generated.rs"), &rcode_mod);

    let tsigalg_fh = Asset::TsigAlgorithmNames.open();
    let tsigalg_root = Element::from_reader(tsigalg_fh).expect("root");
    let tsigalgreg = iana::TsigAlgRegistry::new(
        get_registry(&tsigalg_root, "tsig-algorithm-names-1"),
        &rfc_index,
        &people,
    );
    let tsig_alg_mod = tsig::generate_tsig_alg(&tsigalgreg).to_string();
    write_file(yardi_path.join("msg/tsig/alg.rs"), &tsig_alg_mod);

    let edns_rcode_mod = generate_edns_rcode_mod(&rcodereg).to_string();
    write_file(
        yardi_path.join("msg/edns/ext_rcode/mod.rs"),
        &edns_rcode_mod,
    );

    let dnssec_fh = Asset::DnsSecAlgNumbers.open();
    let dnssec_root = Element::from_reader(dnssec_fh).expect("root");
    let dnssecalgreg = iana::DnssecAlgRegistry::new(
        get_registry(&dnssec_root, "dns-sec-alg-numbers-1"),
        &rfc_index,
        &people,
    );
    let dnssec_constants_mod = generate_dnssec_sigalg_constants(&dnssecalgreg).to_string();
    write_file(
        yardi_path.join("dnssec/sigalg/generated.rs"),
        &dnssec_constants_mod,
    );

    let dsdigestalg_fh = Asset::DsRrTypes.open();
    let dsdigestalg_root = Element::from_reader(dsdigestalg_fh).expect("root");
    let dsdigestalgreg = iana::DsDigestAlgRegistry::new(
        get_registry(&dsdigestalg_root, "ds-rr-types-1"),
        &rfc_index,
        &people,
    );

    let dnskeyflags_fh = Asset::DnskeyFlags.open();
    let dnskeyflags_root = Element::from_reader(dnskeyflags_fh).expect("root");
    let dnskeyflagsreg = iana::DnskeyFlagRegistry::new(
        get_registry(&dnskeyflags_root, "dnskey-flags-1"),
        &rfc_index,
        &people,
    );

    let dane_fh = Asset::DaneParameters.open();
    let dane_root = Element::from_reader(dane_fh).expect("root");
    let tlsaselector_reg = iana::TlsaSelectorInfoRegistry::new(
        get_registry(&dane_root, "selectors"),
        &rfc_index,
        &people,
    );
    let tlsacertusage_reg = iana::TlsaCertUsageRegistry::new(
        get_registry(&dane_root, "certificate-usages"),
        &rfc_index,
        &people,
    );
    let tlsamatchtype_reg = iana::TlsaMatchingTypeRegistry::new(
        get_registry(&dane_root, "matching-types"),
        &rfc_index,
        &people,
    );

    let certty_fh = Asset::CertRrTypes.open();
    let certty_root = Element::from_reader(certty_fh).expect("root");
    let certtyreg = iana::CertTypeRegistry::new(
        get_registry(&certty_root, "cert-rr-types-2"),
        &rfc_index,
        &people,
    );

    let sshfp_fh = Asset::DnsSshfpRrParameters.open();
    let sshfp_root = Element::from_reader(sshfp_fh).expect("root");
    let sshfpkeyalgreg = iana::SshPubKeyAlgRegistry::new(
        get_registry(&sshfp_root, "dns-sshfp-rr-parameters-1"),
        &rfc_index,
        &people,
    );
    let sshfptyreg = iana::SshFpTypeRegistry::new(
        get_registry(&sshfp_root, "dns-sshfp-rr-parameters-2"),
        &rfc_index,
        &people,
    );

    let nsec3_fh = Asset::DnssecNsec3Parameters.open();
    let nsec3_root = Element::from_reader(nsec3_fh).expect("root");
    let nsec3flags = iana::Nsec3FlagRegistry::new(
        get_registry(&nsec3_root, "dnssec-nsec3-parameters-1"),
        &rfc_index,
        &people,
    );
    let nsec3paramflags = iana::Nsec3paramFlagRegistry::new(
        get_registry(&nsec3_root, "dnssec-nsec3-parameters-2"),
        &rfc_index,
        &people,
    );
    let nsec3hashalgs = iana::Nsec3HashAlgRegistry::new(
        get_registry(&nsec3_root, "dnssec-nsec3-parameters-3"),
        &rfc_index,
        &people,
    );

    let caa_fh = Asset::PkixParameters.open();
    let caa_root = Element::from_reader(caa_fh).expect("root");
    let caaflags =
        iana::CaaFlagRegistry::new(get_registry(&caa_root, "caa-flags"), &rfc_index, &people);

    // TODO: generate a helper to parse a str as either a Class or a Type

    let classreg = iana::ClassRegistry::new(
        get_registry(&params_root, CLASS_REGISTRY),
        &rfc_index,
        &people,
    );
    let class_constants_mod = class_mod::build(&classreg).to_string();
    write_file(
        yardi_path.join("datatypes/class/generated.rs"),
        &class_constants_mod,
    );

    let tyreg = iana::TypeRegistry::new(
        get_registry(&params_root, RRTYPE_REGISTRY),
        &rfc_index,
        &people,
    );
    let ty_constants_mod = ty_mod::build(&tyreg).to_string();
    write_file(
        yardi_path.join("datatypes/ty/generated.rs"),
        &ty_constants_mod,
    );

    let eheaderreg = iana::EheaderRegistry::new(
        get_registry(&params_root, EHEADER_REGISTRY),
        &rfc_index,
        &people,
    );
    let edns_flags_mod = edns_mod::build_flags(&eheaderreg).to_string();
    write_file(
        yardi_path.join("msg/edns/edns_flags/mod.rs"),
        &edns_flags_mod,
    );

    let csyncreg = iana::CsyncFlagRegistry::new(
        get_registry(&params_root, CSYNC_REGISTRY),
        &rfc_index,
        &people,
    );

    let svcb_fh = Asset::DnsServiceBindings.open();
    let svcb_root = Element::from_reader(svcb_fh).expect("root");
    let svcb_param_key_registry = iana::SvcParamKeyRegistry::new(
        get_registry(&svcb_root, "dns-svcparamkeys"),
        &rfc_index,
        // TODO: people?
        &people,
    );
    let svcb_param_key_mod =
        generate_svcb_param_key_constants(&svcb_param_key_registry).to_string();
    write_file(
        yardi_path.join("rdata/svcb/svcb_params/generated.rs"),
        &svcb_param_key_mod,
    );

    {
        let fh = Asset::IpseckeyRrParameters.open();
        let root = Element::from_reader(fh).expect("ipsec params root");
        let reg = iana::IpseckeyAlgRegistry::new(
            get_registry(&root, IPSECKEYALG_REGISTRY),
            &rfc_index,
            &people,
        );
        let mut assoc_constants = template::AssocNumConstants::new("IpseckeyAlg");
        let mut mnemonics = Vec::with_capacity(reg.0.len());
        for info in reg.0.iter() {
            let name = info.name.clone();
            let id = const_name(&name);
            mnemonics.push(quote! { (IpseckeyAlg::#id, #name) });
            assoc_constants.insert(id, info.description.clone(), info.value, &info.refs[..]);
        }
        let tokens = quote! {
            #[doc="IPSECKEY RR Algorithm"]
            #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                    UnpackedLen, WireOrd, Parse, Present)]
            #[yardi(crate="crate")]
            pub struct IpseckeyAlg(pub u8);

            impl core::fmt::Debug for IpseckeyAlg {
                fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                    for &(mt, m) in Self::MNEMONICS {
                        if mt == *self {
                            return write!(f, "IpseckeyAlg({})", m);
                        }
                    }
                    write!(f, "IpseckeyAlg({})", self.0)
                }
            }

            #assoc_constants

            impl IpseckeyAlg {
                const MNEMONICS: &'static [(IpseckeyAlg, &'static str)] = &[
                    #(#mnemonics),*
                ];
            }
        }
        .to_string();
        write_file(yardi_path.join("rdata/ipseckey/alg.rs"), &tokens);
    }

    let mut unimplemented_rr: HashSet<&str> = tyreg
        .by_num
        .values()
        .filter(|ti| ti.is_data)
        .map(|ti| ti.mstr.as_str())
        .collect();

    let rr_defs = rr::build(&classreg, &tyreg);

    let mut rr_mod_include = Vec::new();
    let mut rr_structs = BTreeSet::new();

    for &(m, opt_cl) in MANUAL_RR {
        rr_structs.insert((tyreg.by_mstr[m].num, opt_cl, struct_name(m)));
        unimplemented_rr.remove(m);
    }

    for (&(ty, cl), def) in &rr_defs {
        let struct_name = def.struct_name();
        let mod_name = def.mod_name();
        let base_path = {
            let mut p = yardi_path.join("rdata");
            p.push(&mod_name);
            if p.exists() {
                assert!(p.is_dir());
            } else {
                fs::create_dir(&p).expect("create dir for rr");
            }
            p
        };
        {
            let samples = def.samples();
            let mlower = def.mnemonic().to_lowercase();
            let mut temp_path = base_path.clone();
            for (i, (ascii, wire)) in samples.iter().enumerate() {
                let name = format!("sample_sg_{}_{:02}.bin", mlower, i);
                temp_path.push(&name);
                temp_path.set_extension("bin");
                fs::write(&temp_path, wire).expect("write wire");
                if def.is_data() {
                    temp_path.set_extension("txt");
                    fs::write(&temp_path, ascii).expect("write ascii");
                }
                temp_path.pop();
            }
        }
        let test_mod = template::RdataTestMod::new(def, &base_path);
        let mod_path = base_path.join("mod.rs");
        let test_path = base_path.join("test.rs");
        write_file(&test_path, quote! { #test_mod }.to_string());
        let mut extra = Vec::new();
        match mod_name.as_str() {
            "a" | "aaaa" => a_aaaa::add_extra(&mut extra, &mod_name, &struct_name),
            "caa" => caa::add_extra(&mut extra, &caaflags),
            "cert" => cert::add_extra(&mut extra, &certtyreg, &dnssecalgreg),
            "csync" => csync_mod::add_extra(&mut extra, &csyncreg),
            "ds" => ds::add_extra(&mut extra, &dsdigestalgreg),
            "cdnskey" | "dnskey" | "key" => {
                dnskey::add_extra(&mut extra, &mod_name, &struct_name, &dnskeyflagsreg)
            }
            "nsec" => {
                extra.push(quote!{ mod nsec_type_bitmap; pub use self::nsec_type_bitmap::{NsecTypeBitmap, NsecTypeBitmapIter}; });
            }
            "nsec3" => nsec3::add_extra(&mut extra, &nsec3hashalgs, &nsec3flags),
            "nsec3param" => nsec3::add_extra_param(&mut extra, &nsec3paramflags),
            "rrsig" | "sig" => sig::add_extra(&mut extra, &struct_name),
            "sshfp" => sshfp::add_extra(&mut extra, &sshfpkeyalgreg, &sshfptyreg),
            "svcb" => extra.push(
                quote! { #[cfg(test)] mod rfc_test; pub mod svcb_params; pub use svcb_params::*; },
            ),
            "tlsa" => tlsa::add_extra(
                &mut extra,
                &tlsacertusage_reg,
                &tlsamatchtype_reg,
                &tlsaselector_reg,
            ),
            "tsig" => tsig::add_extra(&mut extra, &rcodereg),
            "txt" => {
                extra.push(quote! { mod txt_data; pub use self::txt_data::{TxtData, TxtIter}; })
            }
            _ => (),
        };
        // TODO: Should Class follow RDATA instead?
        let mod_title = if let Some(cmnemonic) = def.class_mnemonic() {
            format!("Support for {} ({}) RDATA.", def.mnemonic(), cmnemonic)
        } else {
            format!("Support for {} RDATA.", def.mnemonic())
        };
        let tokens = quote! {
            #![doc=#mod_title]
            #![allow(unused_qualifications)]
            #[cfg(test)] mod test;
            #def
            #(#extra)*
        }
        .to_string();
        write_file(&mod_path, &tokens);
        let st_path = format!("{}::{}", mod_name, struct_name);
        rr_structs.insert((ty, cl, struct_name.clone()));
        let pub_mod = {
            let file = syn::parse_str::<syn::File>(&tokens).expect("parse generated file");
            file.items
                .iter()
                .filter(|item| match item {
                    syn::Item::Const(syn::ItemConst { ref vis, .. })
                    | syn::Item::Enum(syn::ItemEnum { ref vis, .. })
                    | syn::Item::Fn(syn::ItemFn { ref vis, .. })
                    | syn::Item::Static(syn::ItemStatic { ref vis, .. })
                    | syn::Item::Struct(syn::ItemStruct { ref vis, .. })
                    | syn::Item::Type(syn::ItemType { ref vis, .. })
                    | syn::Item::Use(syn::ItemUse { ref vis, .. }) => {
                        matches!(vis, &syn::Visibility::Public(_))
                    }
                    _ => false,
                })
                .count()
                > 1
        };
        if pub_mod {
            write!(
                rr_mod_include,
                "pub mod {};\n#[doc(no_inline)] pub use self::{};\n",
                mod_name, &st_path
            )
            .expect("write pub mod");
        } else {
            write!(
                rr_mod_include,
                "mod {};\npub use self::{};\n",
                mod_name, &st_path
            )
            .expect("write mod");
        };
        unimplemented_rr.remove(def.mnemonic());
    }
    let rr_mod_imp_lookup = {
        // TODO: make lookup_imp const?
        let mut out = b"
fn lookup_imp(class: Class, ty: Ty) -> Option<&'static RdataImpTable> {
    match (ty, class) {
"
        .to_vec();
        for &(ty, cl, ref st) in &rr_structs {
            if !tyreg.by_num[&ty].is_data {
                continue;
            }
            let cl_pat = if let Some(cl) = cl {
                cl.to_string()
            } else {
                "_".into()
            };
            writeln!(
                out,
                "        (Ty({}), Class({})) => Some(&<{}>::IMP_TABLE),",
                ty, cl_pat, st
            )
            .expect("write lookup");
        }
        out.extend_from_slice(
            b"        _ => None,
    }
}
",
        );
        out
    };
    // TODO: generate something that allows reflection over the structs?
    let rr_mod_with_rdata_idents = {
        let mut idents: Vec<(&str, Vec<&syn::Ident>)> = Vec::new();
        for &(ty, cl, ref st) in &rr_structs {
            if !tyreg.by_num[&ty].is_data {
                continue;
            }
            let cl_str = cl
                .map(|num| classreg.by_num[&num].mstr.as_str())
                .unwrap_or("ANY");
            match idents.binary_search_by(|probe| probe.0.cmp(cl_str)) {
                Ok(i) => idents[i].1.push(st),
                Err(i) => {
                    idents.insert(i, (cl_str, vec![st]));
                }
            }
        }
        // TODO: need a variant that only matches compressible types;
        //       can we somehow deduplicate if it's combined with
        //       other classes?
        // TODO: needs a method to specify generics to substitute
        let mut out = r#"#[macro_export]
macro_rules! with_rdata_idents {
    ($callback:ident($($v:ident)*),) => {
        $crate::with_rdata_idents!{ $callback($($v)*) }
    };
    ($callback:ident($($v:ident)*)) => {
        $callback!{ $($v),* }
    };
"#
        .to_string();
        for (cl, ids) in &idents {
            write!(
                out,
                "    ($callback:ident($($v:ident)*), {} $($tt:tt)*) => {{
        $crate::with_rdata_idents!{{$callback($($v)*",
                cl
            )
            .unwrap();
            for id in ids {
                write!(out, " {}", id).unwrap();
            }
            out.push_str(
                r#") $($tt)*}
    };
"#,
            );
        }
        out.push_str(
            r#"    ($callback:ident $(, $class:ident)*) => {
        $crate::with_rdata_idents!{ $callback(), $($class),* }
    };
    ($callback:ident $(, $class:ident)*,) => {
        $crate::with_rdata_idents!{ $callback(), $($class),* }
    };
}"#,
        );
        out.into_bytes()
    };
    rr_mod_include.extend_from_slice(&rr_mod_imp_lookup);
    rr_mod_include.extend_from_slice(&rr_mod_with_rdata_idents);
    write_file(yardi_path.join("rdata/mod.in.rs"), rr_mod_include);
    let tuple_impls_mod = tuple_impls::generate().to_string();
    write_file(yardi_path.join("tuple_impls.rs"), tuple_impls_mod);
    let errors_mod = errors::module().to_string();
    write_file(yardi_path.join("errors.rs"), errors_mod);

    let unimplemented_rr_txt = {
        let mut sorted = Vec::with_capacity(unimplemented_rr.len() + 1);
        sorted.extend(unimplemented_rr.iter().cloned());
        sorted.sort();
        sorted.insert(0, "(This file is maintained by codegen)");
        sorted.join("\n")
    };

    write_file(
        yardi_path.join("dnssec/rsa/test.rs"),
        generate_dnssec_tests(),
    );

    fs::write(
        workspace_path.join("unimplemented_rr.txt"),
        unimplemented_rr_txt,
    )
    .expect("write unimplemented rr");
}
