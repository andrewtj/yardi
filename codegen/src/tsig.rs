use proc_macro2::TokenStream;

use super::const_name;
use crate::iana::{RcodeRegistry, TsigAlgRegistry};
use crate::template::{AssocNumConstants, DocRefs};

pub fn add_extra(extra: &mut Vec<TokenStream>, rcode_reg: &RcodeRegistry) {
    extra.push(quote!{
        use core::cmp::Ordering;

        #[cfg(feature = "std")] use std::time::SystemTime;

        use crate::wire::{Pack, PackError, Reader, UnpackError, Unpack, UnpackedLen, WireOrd, WireOrdError, Writer};
    });
    extra.push(tsig_time());
    extra.push(generate_tsig_rcode(rcode_reg));
}

pub fn generate_tsig_alg(r: &TsigAlgRegistry) -> TokenStream {
    let algs = r.0.iter().map(|a| {
        let title = &a.title;
        let name = &a.name;
        let doc_refs = DocRefs(&a.refs);
        let const_id = const_name(title);
        quote! {
            #[doc=#title]
            #[doc="\n"]
            #doc_refs
            pub const #const_id: crate::datatypes::name::Name = name!(#name);
        }
    });
    quote! {
        #(#algs)*
    }
}

fn tsig_time() -> TokenStream {
    // TODO: this shouldn't be in codegen
    quote! {
        /// TSIG Time.
        #[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
        pub struct TsigTime(pub(crate) u64);

        impl TsigTime {
            const U48_MAX: u64 = 0xFFFF_FFFF_FFFF;
            /// Return the 48-bit time as a `u64`.
            pub const fn as_u64(self) -> u64 {
                self.0
            }
            /// Return a `TsigTime` representing the current time.
            #[cfg(feature = "std")]
            pub fn now() -> Self {
                // TODO: gracefully handle a bad clock
                let now = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs();
                TsigTime(now % Self::U48_MAX)
            }
            // TODO: Return Option, and/or implement TryFrom instead
            /// Create a `TsigTime` from a `u64`.
            pub const fn from_u64(n: u64) -> TsigTime {
                TsigTime(n % Self::U48_MAX)
            }
            /// Returns true if this `TsigTime` is within `fudge` seconds of `other`.
            pub fn within_fudge(self, other: Self, fudge: u16) -> bool {
                let (s, g) = match self.0.cmp(&other.0) {
                    Ordering::Equal => return true,
                    Ordering::Less => (self.0, other.0),
                    Ordering::Greater => (other.0, self.0),
                };
                g.checked_sub(s)
                    .map(|d| {
                        if d > u64::from(u16::MAX) {
                            false
                        } else {
                            (d as u16) < fudge
                        }
                    })
                    .unwrap_or(false)
            }
        }

        impl WireOrd for TsigTime {
            fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
                let l = <[u8; 6]>::unpack(left).map_err(WireOrdError::Left)?;
                let r = <[u8; 6]>::unpack(right).map_err(WireOrdError::Right)?;
                Ok(l.cmp(&r))
            }
            fn cmp_as_wire(&self, other: &Self) -> Ordering {
                self.0.to_be_bytes()[2..].cmp(&other.0.to_be_bytes()[2..])
            }
        }

        impl UnpackedLen for TsigTime {
            fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
                <[u8; 6]>::unpacked_len(reader)
            }
        }

        impl Pack for TsigTime {
            fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
                dst.write_bytes(&self.0.to_be_bytes()[2..])
            }
            fn packed_len(&self) -> Result<usize, PackError> {
                Ok(6)
            }
        }

        impl<'a> Unpack<'a> for TsigTime {
            fn unpack(src: &mut Reader<'a>) -> Result<Self, UnpackError> {
                src.get_bytes_map(6, |s| {
                    Ok(TsigTime(
                        u64::from(s[0]) << 40
                            | u64::from(s[1]) << 32
                            | u64::from(s[2]) << 24
                            | u64::from(s[3]) << 16
                            | u64::from(s[4]) << 8
                            | u64::from(s[5]),
                    ))
                })
            }
        }
    }
}

fn generate_tsig_rcode(r: &RcodeRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("TsigRcode");
    let mut mnemonics = Vec::with_capacity(r.tsig.len());
    for info in r.tsig.values() {
        let title = format!("{} - {}", info.mstr, info.desc);
        let const_id = const_name(&info.mstr);
        let debug_str = &info.mstr;
        mnemonics.push(quote! { (TsigRcode::#const_id, #debug_str) });
        assoc_constants.insert(const_id, title, info.num, &info.refs[..]);
    }
    quote! {
        /// TSIG RCODE.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, PartialOrd, Ord, Pack, Unpack,
         UnpackedLen, WireOrd)]
        #[yardi(crate="crate")]
        pub struct TsigRcode(pub u16);

        impl core::fmt::Debug for TsigRcode {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(rc, m) in Self::MNEMONICS {
                    if rc == *self {
                        return write!(f, "TsigRcode({})", m);
                    }
                }
                write!(f, "TsigRcode({})", self.0)
            }
        }

        #assoc_constants

        impl TsigRcode {
            const MNEMONICS: &'static [(TsigRcode, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
