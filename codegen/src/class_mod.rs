use proc_macro2::TokenStream;

use crate::const_name;
use crate::iana::ClassRegistry;
use crate::template::{AssocNumConstants, ToFromMnemonic};

pub fn build(r: &ClassRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("Class");
    let mut mnemonic_table = ToFromMnemonic::new("Class");
    for info in r.by_num.values() {
        let title = format!("{} - {}", info.mstr, info.desc);
        let const_id = const_name(&info.mstr);
        mnemonic_table.insert(const_id.clone(), info.mstr.clone(), info.num);
        assoc_constants.insert(const_id, title, info.num, &info.refs[..]);
    }
    quote! {
        use super::Class;
        #assoc_constants
        #mnemonic_table
    }
}
