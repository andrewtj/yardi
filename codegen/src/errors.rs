use heck::{ToSnakeCase, ToTitleCase, ToUpperCamelCase};
use syn::Ident;

const PRIVATE_VARIANTS: usize = 2;
const VALUE_ERRORS: &[&str] = &[
    "empty label",
    "invalid value",
    "invalid name pointer",
    "large value",
    "long label",
    "no CLASS",
    "no last name",
    "no origin",
    "no token",
    "no TTL",
    "small value",
    "RDATA empty",
    "RDATA oversize",
    "unexpected version",
    "unknown",
    "unknown label header",
    "unspecified",
];
const PARSE_ERRORS: &[&str] = &[
    "bad ascii",
    "dangling escape",
    "dangling newline",
    "dangling quote",
    "disallowed comment",
    "disallowed parenthesis",
    "leading junk",
    "long entry",
    "no token",
    "parenthesis overflow",
    "trailing junk",
    "unbalanced parenthesis",
];
// TODO: remove unspecified
const PACK_ERRORS: &[&str] = &["no space"];
const UNPACK_ERRORS: &[&str] = &["short", "trailing junk"];

fn strip_value(mut s: &str) -> &str {
    if s.ends_with(" value") {
        s = &s[..s.len() - 6];
    }
    assert!(!s.contains("value"));
    s
}

fn to_camel_ident(s: &str) -> syn::Ident {
    let s = strip_value(s).to_upper_camel_case();
    Ident::new(&s, proc_macro2::Span::call_site())
}

fn to_snake_ident(s: &str) -> syn::Ident {
    let s = strip_value(s).to_snake_case();
    Ident::new(&s, proc_macro2::Span::call_site())
}

pub fn module() -> proc_macro2::TokenStream {
    let value_error_variants: Vec<(Ident, Ident, &str)> = VALUE_ERRORS
        .iter()
        .map(|&s| (to_camel_ident(s), to_snake_ident(s), s))
        .chain((1..=PRIVATE_VARIANTS).map(|i| {
            let s = format!("private{}", i);
            (to_camel_ident(&s), to_snake_ident(&s), "unspecified")
        }))
        .collect();
    let parse_error_variants: Vec<(Ident, Ident, &str)> = PARSE_ERRORS
        .iter()
        .map(|&s| (to_camel_ident(s), to_snake_ident(s), s))
        .collect();
    let pack_error_variants: Vec<(Ident, Ident, &str)> = PACK_ERRORS
        .iter()
        .map(|&s| (to_camel_ident(s), to_snake_ident(s), s))
        .collect();
    let unpack_error_variants: Vec<(Ident, Ident, &str)> = UNPACK_ERRORS
        .iter()
        .map(|&s| (to_camel_ident(s), to_snake_ident(s), s))
        .collect();
    let value_constructors: Vec<(Ident, Ident)> = value_error_variants
        .iter()
        .map(|(vn, mn, _)| {
            let s = format!("value_{}", mn);
            (Ident::new(&s, proc_macro2::Span::call_site()), vn.clone())
        })
        .collect();
    let defs: &[(Ident, &[(Ident, Ident, &str)])] = &[
        (
            Ident::new("ParseError", proc_macro2::Span::call_site()),
            &parse_error_variants[..],
        ),
        (
            Ident::new("PresentError", proc_macro2::Span::call_site()),
            &[],
        ),
        (
            Ident::new("PackError", proc_macro2::Span::call_site()),
            &pack_error_variants[..],
        ),
        (
            Ident::new("UnpackError", proc_macro2::Span::call_site()),
            &unpack_error_variants[..],
        ),
    ];
    let error_size_checks = defs.iter().map(|(ty, _)| {
        let test_name = to_snake_ident(&format!("{}_size_check", ty));
        quote! {
            #[test]
            fn #test_name() {
                let target_size = 40;
                let actual_size = size_of::<#ty>();
                assert!(
                    actual_size <= target_size,
                    "{} is {} bytes, target is {} at most",
                    stringify!(#ty),
                    actual_size,
                    target_size,
                );
            }
        }
    });
    let enum_defs_and_impls = defs
        .iter()
        .map(|(name, variants)| {
            let name_kind_str = format!("{}Kind", name);
            let name_kind = Ident::new(&name_kind_str, proc_macro2::Span::call_site());
            let members = variants.iter().map(|(v, _, d)| {
                let doc = d.to_title_case();
                quote! {
                    #[doc=#doc]
                    #v
                }
            });
            let member_constructors = variants.iter().map(|(vn, _, d)| {
                let mn = Ident::new(&d.to_snake_case(), proc_macro2::Span::call_site());
                let extra = if name == "UnpackError" {
                    let mn_at_str = format!("{}_at", mn);
                    let mn_at = Ident::new(&mn_at_str, proc_macro2::Span::call_site());
                    quote! {
                        pub fn #mn_at(index: usize) -> #name {
                            #name::with_kind(#name_kind::#vn).set_index(index)
                        }
                    }
                } else {
                    quote!{}
                };
                quote! {
                    pub fn #mn() -> #name {
                        #name::with_kind(#name_kind::#vn)
                    }
                    #extra
                }
            });
            let value_constructors = value_constructors.iter().map(|(mn, vn)| {
                let extra = if name  == "UnpackError" {
                    let mn_at_str = format!("{}_at", mn);
                    let mn_at = Ident::new(&mn_at_str, proc_macro2::Span::call_site());
                    quote! {
                        pub fn #mn_at(index: usize) -> #name {
                            #name::with_kind(#name_kind::Value(ValueErrorKind::#vn)).set_index(index)
                        }
                    }
                } else {
                    quote!{}
                };
                quote! {
                    pub fn #mn() -> #name {
                        #name::with_kind(#name_kind::Value(ValueErrorKind::#vn))
                    }
                    #extra
                }
            });
            let variant_as_str = variants.iter().map(|(vn, _, d)| {
                quote!{ #name_kind::#vn => #d }
            });
            quote! {
                #[derive(Clone, Copy, Debug, Eq, PartialEq)]
                #[non_exhaustive]
                pub enum #name_kind {
                    #(#members,)*
                    #[doc="Value"]
                    Value(ValueErrorKind),
                }
                impl #name_kind {
                    pub fn as_str(&self) -> &'static str {
                        match self {
                            #(#variant_as_str,)*
                            #name_kind::Value(ve) => ve.as_str(),
                        }
                    }
                }
                impl Display for #name_kind {
                    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                        self.as_str().fmt(f)
                    }
                }
                impl #name {
                    #(#member_constructors)*
                    #(#value_constructors)*
                }
            }
        })
        .chain(std::iter::once({
            let variants = value_error_variants.iter().map(|(vn, mn, d)| {
                let d: &str = d;
                let is_private_use = d.eq("unspecified") && *mn != "unspecified";
                let doc = if is_private_use {
                    "Private Use".into()
                } else {
                    d.to_title_case()
                };
                quote! {
                    #[doc=#doc]
                    #vn
                }
            });
            let variant_as_str = value_error_variants.iter().map(|(vn, _, d)| {
                quote!{ ValueErrorKind::#vn => #d }
            });
            quote! {
                #[derive(Clone, Copy, Debug, Eq, PartialEq)]
                #[non_exhaustive]
                pub enum ValueErrorKind {
                    #(#variants),*
                }
                impl ValueErrorKind {
                    fn as_str(&self) -> &'static str {
                        match self {
                            #(#variant_as_str,)*
                        }
                    }
                }
                impl Display for ValueErrorKind {
                    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                        self.as_str().fmt(f)
                    }
                }
            }
        }));
    quote! {
        // TODO: remove
        #![allow(missing_docs)]
        use core::{error::Error, fmt::{self, Display}};
        use crate::ascii::parse::Position;

        use nonmax::NonMaxUsize;

        #[derive(Clone, Debug, Eq, PartialEq)]
        pub struct ParseError {
            kind: ParseErrorKind,
            subject: Option<&'static &'static str>,
            position: Option<Position>, // TODO: possible to always have a position?
        }

        impl ParseError {
            pub fn with_kind(kind: ParseErrorKind) -> Self {
                Self{kind, subject: None, position: None}
            }
            pub fn kind(&self) -> ParseErrorKind {
                self.kind
            }
            pub fn set_subject(self, subject: &'static &'static str) -> Self {
                Self { subject: Some(subject), .. self }
            }
            pub fn subject(&self) -> Option<&'static &'static str> {
                self.subject
            }
            pub fn set_unset_subject(self, subject: &'static &'static str) -> Self {
                if self.subject.is_none() {
                    self.set_subject(subject)
                } else {
                    self
                }
            }
            pub fn set_position(self, position: Position) -> Self {
                let position = Some(position);
                Self { position, .. self}
            }
            pub fn position(&self) -> Option<&Position> {
                self.position.as_ref()
            }
        }

        impl Display for ParseError {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                if let Some(subject) = self.subject {
                    f.write_fmt(format_args!("{} parsing {}", self.kind.as_str(), subject))?
                } else {
                    f.write_str(self.kind.as_str())?
                }
                if let Some(p) = self.position.as_ref() {
                    f.write_fmt(format_args!(" at {}:{}", p.row, p.col))
                } else {
                    Ok(())
                }
            }
        }

        impl Error for ParseError {}

        #[derive(Clone, Debug, Eq, PartialEq)]
        pub struct PresentError {
            kind: PresentErrorKind,
            subject: Option<&'static &'static str>,
        }

        impl PresentError {
            pub fn with_kind(kind: PresentErrorKind) -> Self {
                Self{kind, subject: None}
            }
            pub fn kind(&self) -> PresentErrorKind {
                self.kind
            }
            pub fn set_subject(self, subject: &'static &'static str) -> Self {
                Self { subject: Some(subject), .. self }
            }
            pub fn set_unset_subject(self, subject: &'static &'static str) -> Self {
                if self.subject.is_none() {
                    self.set_subject(subject)
                } else {
                    self
                }
            }
            pub fn subject(&self) -> Option<&'static &'static str> {
                self.subject
            }
        }

        impl Display for PresentError {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                if let Some(subject) = self.subject {
                    f.write_fmt(format_args!("{} presenting {}", self.kind.as_str(), subject))
                } else {
                    f.write_str(self.kind.as_str())
                }
            }
        }

        impl Error for PresentError {}

        #[derive(Clone, Debug, Eq, PartialEq)]
        pub struct PackError {
            kind: PackErrorKind,
            subject: Option<&'static &'static str>,
        }

        impl PackError {
            pub fn with_kind(kind: PackErrorKind) -> Self {
                Self{kind, subject: None}
            }
            pub fn kind(&self) -> PackErrorKind {
                self.kind
            }
            pub fn set_subject(self, subject: &'static &'static str) -> Self {
                Self { subject: Some(subject), .. self }
            }
            pub fn set_unset_subject(self, subject: &'static &'static str) -> Self {
                if self.subject.is_none() {
                    self.set_subject(subject)
                } else {
                    self
                }
            }
            pub fn subject(&self) -> Option<&'static &'static str> {
                self.subject
            }
            #[doc(hidden)]
            pub fn private_parse_helper(self) -> ParseError {
                let err = match self.kind {
                    PackErrorKind::Value(ValueErrorKind::RdataOversize) => {
                        ParseError::value_rdata_oversize()
                    }
                    other if cfg!(debug_assertions) => {
                        panic!("unexpected PackErrorKind: {:?}", other);
                    }
                    // TODO: debug log?
                    _ => ParseError::value_invalid(),
                };
                if let Some(subject) = self.subject {
                    err.set_subject(subject)
                } else {
                    err
                }
            }
            #[doc(hidden)]
            pub fn private_present_helper(self) -> PresentError {
                let err = match self.kind {
                    PackErrorKind::Value(ValueErrorKind::RdataOversize) => {
                        PresentError::value_rdata_oversize()
                    }
                    other if cfg!(debug_assertions) => {
                        panic!("unexpected PresentErrorKind: {:?}", other);
                    }
                    // TODO: debug log?
                    _ => PresentError::value_invalid(),
                };
                if let Some(subject) = self.subject {
                    err.set_subject(subject)
                } else {
                    err
                }
            }
            #[doc(hidden)]
            pub fn private_unpack_helper(self) -> UnpackError {
                let err = match self.kind {
                    PackErrorKind::Value(ValueErrorKind::RdataOversize) => {
                        UnpackError::value_rdata_oversize()
                    }
                    other if cfg!(debug_assertions) => {
                        panic!("unexpected PresentErrorKind: {:?}", other);
                    }
                    // TODO: debug log?
                    _ => UnpackError::value_invalid(),
                };
                if let Some(subject) = self.subject {
                    err.set_subject(subject)
                } else {
                    err
                }
            }
        }

        impl Display for PackError {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                if let Some(subject) = self.subject {
                    f.write_fmt(format_args!("{} packing {}", self.kind.as_str(), subject))
                } else {
                    f.write_str(self.kind.as_str())
                }
            }
        }

        impl Error for PackError {}

        #[derive(Clone, Debug, Eq, PartialEq)]
        pub struct UnpackError {
            kind: UnpackErrorKind,
            subject: Option<&'static &'static str>,
            index: Option<NonMaxUsize>, // TOOD: make this a u32? unset value
        }

        impl UnpackError {
            pub fn with_kind(kind: UnpackErrorKind) -> Self {
                Self{kind, subject: None, index: None }
            }
            pub fn kind(&self) -> UnpackErrorKind {
                self.kind
            }
            pub fn set_subject(self, subject: &'static &'static str) -> Self {
                Self { subject: Some(subject), .. self }
            }
            pub fn set_unset_subject(self, subject: &'static &'static str) -> Self {
                if self.subject.is_none() {
                    self.set_subject(subject)
                } else {
                    self
                }
            }
            pub fn subject(&self) -> Option<&'static &'static str> {
                self.subject
            }
            pub fn set_index(self, index: usize) -> Self {
                let index = NonMaxUsize::new(index);
                Self { index, .. self }
            }
            pub fn index(&self) -> Option<usize> {
                self.index.as_ref().map(NonMaxUsize::get)
            }
        }

        impl Display for UnpackError {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                if let Some(subject) = self.subject {
                    f.write_fmt(format_args!("{} unpacking {}", self.kind.as_str(), subject))?
                } else {
                    f.write_str(self.kind.as_str())?
                }
                if let Some(index) = self.index {
                    f.write_fmt(format_args!(" at {}", index))
                } else {
                    Ok(())
                }
            }
        }

        impl Error for UnpackError {}

        #(#enum_defs_and_impls)*

        #(#error_size_checks)*
    }
}
