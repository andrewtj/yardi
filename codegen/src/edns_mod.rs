use proc_macro2::TokenStream;
use syn;

use crate::const_name;
use crate::iana::EheaderRegistry;
use crate::template::{AssocPathConstants, FlagTy, GenericFlags};

pub fn build_flags(r: &EheaderRegistry) -> TokenStream {
    let flag_ty = FlagTy::new("EdnsFlags", "EDNS Flags.", "u16");
    let mut assoc_constants = AssocPathConstants::new("EdnsFlags");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in r.0.iter() {
        let title = format!("{} - {}", info.title, info.description);
        let const_id = const_name(&info.title);
        let debug_str = &info.title;
        mnemonics.push(quote! { (EdnsFlags::#const_id, #debug_str) });
        let value = syn::parse_str(format!("EdnsFlags::FLAG_{}", info.value).as_str())
            .expect("edns header flag path");
        assoc_constants.insert(info.value, const_id, title, value, &info.refs[..]);
    }
    let generic_flags = GenericFlags::new("EdnsFlags", "Flag", 16);
    quote! {
        #[cfg(test)]
        mod test;

        #flag_ty

        #assoc_constants

        #generic_flags

        impl EdnsFlags {
            const MNEMONICS: &'static [(EdnsFlags, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
