use proc_macro2::TokenStream;

use super::const_name;
use crate::iana::{TlsaCertUsageRegistry, TlsaMatchingTypeRegistry, TlsaSelectorInfoRegistry};
use crate::template::AssocNumConstants;

pub fn add_extra(
    extra: &mut Vec<TokenStream>,
    certusage_reg: &TlsaCertUsageRegistry,
    matchty_reg: &TlsaMatchingTypeRegistry,
    sel_reg: &TlsaSelectorInfoRegistry,
) {
    extra.push(generate_certusage(certusage_reg));
    extra.push(generate_matchty(matchty_reg));
    extra.push(generate_selector(sel_reg));
}

pub fn generate_certusage(reg: &TlsaCertUsageRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("TlsaCertUsage");
    let mut mnemonics = Vec::with_capacity(reg.0.len());
    for info in reg.0.iter() {
        let title = format!("{} - {}", &info.acronym, &info.description);
        let id = const_name(&info.acronym);
        let debug_str = &info.acronym;
        mnemonics.push(quote! { (TlsaCertUsage::#id, #debug_str) });
        assoc_constants.insert(id, title, info.value, &info.refs[..]);
    }
    quote! {
        /// TLSA Certificate Usage.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct TlsaCertUsage(pub u8);

        impl core::fmt::Debug for TlsaCertUsage {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(cu, m) in Self::MNEMONICS {
                    if cu == *self {
                        return write!(f, "TlsaCertUsage({})", m);
                    }
                }
                write!(f, "TlsaCertUsage({})", self.0)
            }
        }

        #assoc_constants

        impl TlsaCertUsage {
            const MNEMONICS: &'static [(TlsaCertUsage, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}

pub fn generate_matchty(reg: &TlsaMatchingTypeRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("TlsaMatchTy");
    let mut mnemonics = Vec::with_capacity(reg.0.len());
    for info in reg.0.iter() {
        let title = format!("{} - {}", &info.acronym, &info.description);
        let id = const_name(&info.acronym);
        let debug_str = &info.acronym;
        mnemonics.push(quote! { (TlsaMatchTy::#id, #debug_str) });
        assoc_constants.insert(id, title, info.value, &info.refs[..]);
    }
    quote! {
        /// TLSA Match Type.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                 UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct TlsaMatchTy(pub u8);

        impl core::fmt::Debug for TlsaMatchTy {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(mt, m) in Self::MNEMONICS {
                    if mt == *self {
                        return write!(f, "TlsaMatchTy({})", m);
                    }
                }
                write!(f, "TlsaMatchTy({})", self.0)
            }
        }

        #assoc_constants

        impl TlsaMatchTy {
            const MNEMONICS: &'static [(TlsaMatchTy, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}

pub fn generate_selector(reg: &TlsaSelectorInfoRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("TlsaSelector");
    let mut mnemonics = Vec::with_capacity(reg.0.len());
    for info in reg.0.iter() {
        let title = format!("{} - {}", &info.acronym, &info.description);
        let id = const_name(&info.acronym);
        let debug_str = &info.acronym;
        mnemonics.push(quote! { (TlsaSelector::#id, #debug_str) });
        assoc_constants.insert(id, title, info.value, &info.refs[..]);
    }
    quote! {
        /// TLSA Selector.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct TlsaSelector(pub u8);

        impl core::fmt::Debug for TlsaSelector {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(s, m) in Self::MNEMONICS {
                    if s == *self {
                        return write!(f, "TlsaSelector({})", m);
                    }
                }
                write!(f, "TlsaSelector({})", self.0)
            }
        }

        #assoc_constants

        impl TlsaSelector {
            const MNEMONICS: &'static [(TlsaSelector, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
