use proc_macro2::TokenStream;

use super::const_name;
use crate::iana::DsDigestAlgRegistry;
use crate::template::AssocNumConstants;

pub fn add_extra(extra: &mut Vec<TokenStream>, dsdigestalg_reg: &DsDigestAlgRegistry) {
    extra.push(generate_digestalg(dsdigestalg_reg));
}

fn generate_digestalg(r: &DsDigestAlgRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("DsDigestAlg");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in r.0.iter() {
        let title = info.name.clone();
        let const_id = const_name(&title);
        let debug_str = &info.name;
        mnemonics.push(quote! { (DsDigestAlg::#const_id, #debug_str) });
        assoc_constants.insert(const_id, title, info.value, &info.refs[..]);
    }
    quote! {
        /// DS Digest Algorithm.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct DsDigestAlg(pub u8);

        impl core::fmt::Display for DsDigestAlg {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                self.0.fmt(f)
            }
        }

        impl core::fmt::Debug for DsDigestAlg {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(a, m) in Self::MNEMONICS {
                    if a == *self {
                        return write!(f, "DsDigestAlg({})", m);
                    }
                }
                write!(f, "DsDigestAlg({})", self.0)
            }
        }

        #assoc_constants

        impl DsDigestAlg {
            const MNEMONICS: &'static [(DsDigestAlg, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
