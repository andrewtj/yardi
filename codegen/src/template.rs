use std::borrow::Cow;
use std::collections::{BTreeMap, HashMap};
use std::path::Path;
use std::rc::Rc;

use proc_macro2::{Span, TokenStream};
use quote::ToTokens;
use syn::{self, Ident, LitInt};

use crate::iana::rf::Rf;
use crate::rr::Rr;

pub struct Constant<'a, T> {
    name: Ident,
    title: String,
    doc_refs: Cow<'a, [Rf]>,
    value: T,
}

pub struct AssocNumConstants<'a, T>
where
    T: Ord,
{
    name: Ident,
    values: BTreeMap<T, Constant<'a, T>>,
}

impl<'a, T> AssocNumConstants<'a, T>
where
    T: Ord + Copy,
{
    pub fn new(name: &str) -> Self {
        AssocNumConstants {
            name: Ident::new(name, Span::call_site()),
            values: BTreeMap::new(),
        }
    }
    pub fn insert<U>(&mut self, name: Ident, title: String, value: T, doc_refs: U) -> &mut Self
    where
        U: Into<Cow<'a, [Rf]>>,
    {
        let constant = Constant {
            name,
            title,
            doc_refs: doc_refs.into(),
            value,
        };
        assert!(self.values.insert(value, constant).is_none());
        self
    }
}

impl<'a, T> ToTokens for AssocNumConstants<'a, T>
where
    T: Copy + Ord + Into<u64>,
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let ty_ident = &self.name;
        let mut inner = TokenStream::new();
        for c in self.values.values() {
            let name = &c.name;
            let title = format!("{}\n\n", &c.title);
            let doc_refs = DocRefs(&c.doc_refs);
            let ty_num: LitInt =
                LitInt::new(c.value.into().to_string().as_str(), Span::call_site());
            inner.extend(quote! {
                #[doc=#title]
                #doc_refs
                pub const #name: #ty_ident = #ty_ident(#ty_num);
            });
        }
        let inner = inner.into_iter();
        tokens.extend(quote! { impl #ty_ident { #(#inner)* } });
    }
}

pub struct AssocPathConstants<'a, T> {
    name: Ident,
    values: BTreeMap<T, Constant<'a, syn::Path>>,
}

impl<'a, T> AssocPathConstants<'a, T>
where
    T: Ord + Copy,
{
    pub fn new(name: &str) -> Self {
        AssocPathConstants {
            name: Ident::new(name, Span::call_site()),
            values: BTreeMap::new(),
        }
    }
    pub fn insert<U>(
        &mut self,
        key: T,
        name: Ident,
        title: String,
        path: syn::Path,
        doc_refs: U,
    ) -> &mut Self
    where
        U: Into<Cow<'a, [Rf]>>,
    {
        let constant = Constant {
            name,
            title,
            doc_refs: doc_refs.into(),
            value: path,
        };
        assert!(self.values.insert(key, constant).is_none());
        self
    }
}

impl<'a, T> ToTokens for AssocPathConstants<'a, T>
where
    T: Ord + Copy,
{
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let ty_ident = &self.name;
        let mut inner = Vec::with_capacity(self.values.len());
        for c in self.values.values() {
            let name = &c.name;
            let title = format!("{}\n\n", &c.title);
            let doc_refs = DocRefs(&c.doc_refs);
            let path = &c.value;
            inner.push(quote! {
                #[doc=#title]
                #doc_refs
                pub const #name: #ty_ident = #path;
            });
        }
        tokens.extend(quote! { impl #ty_ident { #(#inner)* } });
    }
}

pub struct GenericFlags<'a> {
    ty: Ident,
    title: Cow<'a, str>,
    size: u8,
}

impl<'a> GenericFlags<'a> {
    pub fn new<T>(ty: &str, title: T, size: u8) -> Self
    where
        T: Into<Cow<'a, str>>,
    {
        let ty = Ident::new(ty, Span::call_site());
        let title = title.into();
        assert_eq!(&title, "Flag");
        GenericFlags { ty, title, size }
    }
}

impl<'a> ToTokens for GenericFlags<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        assert!(self.size.is_power_of_two());
        assert!(self.size <= 64);
        let mut value = 1u64 << (self.size - 1);
        let ty = &self.ty;
        let mut inner = Vec::with_capacity(self.size as usize);
        for i in 0.. {
            if value == 0 {
                break;
            }
            let title = format!("{} {}", self.title, i);
            let id: Ident = syn::parse_str(format!("FLAG_{}", i).as_str()).unwrap();
            let value_lit = syn::LitInt::new(value.to_string().as_str(), Span::call_site());
            inner.push(quote! {
                #[doc=#title]
                pub const #id: #ty = #ty(#value_lit);
            });
            value >>= 1;
        }
        tokens.extend(quote! { impl #ty { #(#inner)* } });
    }
}

#[derive(Debug)]
pub struct DocRefs<'a>(pub &'a [Rf]);

impl<'a> quote::ToTokens for DocRefs<'a> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        match self.0.len() {
            0 => (),
            1 => {
                let r = &self.0[0];
                tokens.extend(quote! { #[doc="Reference: "] #[doc=#r] });
            }
            _ => {
                tokens.extend(quote! { #[doc="References:"] });
                for mut s in self.0.iter().map(ToString::to_string) {
                    s.insert_str(0, " * ");
                    tokens.extend(quote! { #[doc=#s] });
                }
            }
        }
    }
}

#[derive(Debug)]
pub struct ToFromMnemonic<T>
where
    T: Ord,
{
    ty: syn::Ident,
    from_str_only: bool,
    from_str_sensistive: bool,
    from_str: HashMap<usize, Vec<(Rc<syn::Ident>, Rc<syn::Ident>)>>,
    to_str: Vec<(T, Rc<syn::Ident>, Rc<syn::Ident>)>,
    consts: Vec<(Rc<syn::Ident>, String)>,
}

impl<T> ToFromMnemonic<T>
where
    T: Ord,
{
    pub fn new(ty: &str) -> Self {
        ToFromMnemonic {
            ty: Ident::new(ty, Span::call_site()),
            from_str_only: false,
            from_str_sensistive: false,
            from_str: Default::default(),
            to_str: Default::default(),
            consts: Default::default(),
        }
    }
    pub fn no_to_str(&mut self) {
        self.from_str_only = true;
    }
    pub fn sensistive_str(&mut self) {
        self.from_str_sensistive = true;
    }
    pub fn insert(&mut self, id: syn::Ident, s: String, val: T) {
        let id = Rc::new(id);
        let str_id = Rc::new(syn::Ident::new(
            format!("{}_STR", id).as_ref(),
            Span::call_site(),
        ));

        let fsv = self.from_str.entry(s.len()).or_default();
        let fs_val = (id.clone(), str_id.clone());
        let fs_idx = fsv.binary_search(&fs_val).unwrap_err();
        fsv.insert(fs_idx, fs_val);

        let ts_val = (val, id, str_id.clone());
        let ts_idx = self.to_str.binary_search(&ts_val).unwrap_err();
        self.to_str.insert(ts_idx, ts_val);

        let c_val = (str_id, s);
        let c_idx = self.consts.binary_search(&c_val).unwrap_err();
        self.consts.insert(c_idx, c_val);
    }
}

impl<T> quote::ToTokens for ToFromMnemonic<T>
where
    T: Ord,
{
    fn to_tokens(&self, tokens: &mut crate::TokenStream) {
        let ty = &self.ty;
        let consts = self.consts.iter().map(|(id, s)| {
            let id: &syn::Ident = id;
            quote! { const #id: &str = #s; }
        });
        let mut from_str_keys: Vec<_> = self.from_str.keys().cloned().collect();
        from_str_keys.sort();
        let from_str_eq_fn = if self.from_str_sensistive {
            quote! { eq }
        } else {
            quote! { eq_ignore_ascii_case }
        };
        let from_str_clauses = from_str_keys.iter().map(|&i| {
            let pairs = self.from_str[&i].iter().map(|(id, str_id)| {
                let id: &syn::Ident = id;
                let str_id: &syn::Ident = str_id;
                quote! {
                    if #str_id.as_bytes().#from_str_eq_fn(s.as_bytes()) {
                        Some(#ty::#id)
                    }
                }
            });
            quote! {
                #i => {
                    #(#pairs else)* { None }
                },
            }
        });
        let maybe_to_str = if self.from_str_only {
            quote! {}
        } else {
            let to_str_clauses = self.to_str.iter().map(|(_, c_id, s_id)| {
                let c_id: &syn::Ident = c_id;
                let s_id: &syn::Ident = s_id;
                quote! { #ty::#c_id => Some(#s_id), }
            });
            quote! {
                pub(crate) fn to_str(ty: #ty) -> Option<&'static str> {
                    match ty {
                        #(#to_str_clauses)*
                        _ => None,
                    }
                }
            }
        };
        tokens.extend(quote! {
            #(#consts)*
            #[allow(clippy::cognitive_complexity)]
            pub(crate) fn from_str(s: &str) -> Option<#ty> {
                match s.len() {
                    #(#from_str_clauses)*
                    _ => None,
                }
            }
            #maybe_to_str
        });
    }
}

#[derive(Debug)]
pub struct FlagTy {
    ty: syn::Ident,
    wrapped_ty: syn::Ident,
    doc: String,
}

impl FlagTy {
    pub fn new<D>(ty: &str, doc: D, wrapped_ty: &str) -> Self
    where
        D: Into<String>,
    {
        FlagTy {
            ty: syn::parse_str(ty).expect("ty must be a valid ident"),
            wrapped_ty: syn::parse_str(wrapped_ty).expect("wrapped_ty must be a valid ident"),
            doc: doc.into(),
        }
    }
}

impl quote::ToTokens for FlagTy {
    fn to_tokens(&self, tokens: &mut crate::TokenStream) {
        let ty = &self.ty;
        let wrapped_ty = &self.wrapped_ty;
        let doc = &self.doc;
        tokens.extend(quote!{
            #[doc=#doc]
            #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Parse, Present, Pack,
                    Unpack, UnpackedLen, WireOrd)]
            #[yardi(crate="crate")]
            pub struct #ty(pub #wrapped_ty);

            impl #ty {
                /// Returns true if all the flags in other are set in self.
                pub fn contains(self, other: Self) -> bool {
                    (self & other) == other
                }
            }

            impl core::fmt::Display for #ty {
                fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                    core::fmt::Display::fmt(&self.0, f)
                }
            }

            impl core::fmt::Debug for #ty {
                fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                    f.write_str(stringify!(#ty))?;
                    f.write_str("(")?;
                    if self.0 == 0 {
                        return f.write_str("0)");
                    }
                    let mut i = !(!0 >> 1);
                    let mut mnemonics = Self::MNEMONICS;
                    let mut first = true;
                    while i != 0 {
                        if i & self.0 != 0 {
                            if first {
                                first = false;
                            } else {
                                f.write_str("|")?;
                            }
                            while mnemonics.first().map(|&(f, _)| f.0 > i).unwrap_or(false) {
                                mnemonics = &mnemonics[1..];
                            }
                            match mnemonics.first() {
                                Some(&(#ty(n), s)) if n == i => f.write_str(s)?,
                                Some(_) | None => write!(f, "{}", i.leading_zeros())?,
                            }
                        }
                        i >>= 1;
                    }
                    f.write_str(")")
                }
            }

            impl core::ops::BitAnd for #ty {
                type Output = Self;
                fn bitand(self, rhs: Self) -> Self {
                    #ty(self.0 & rhs.0)
                }
            }

            impl core::ops::BitAndAssign for #ty {
                fn bitand_assign(&mut self, rhs: Self) {
                    self.0 &= rhs.0
                }
            }

            impl core::ops::BitOr for #ty {
                type Output = Self;
                fn bitor(self, rhs: Self) -> Self {
                    #ty(self.0 | rhs.0)
                }
            }

            impl core::ops::BitOrAssign for #ty {
                fn bitor_assign(&mut self, rhs: Self) {
                    self.0 |= rhs.0;
                }
            }

            impl core::ops::BitXor for #ty {
                type Output = Self;
                fn bitxor(self, rhs: Self) -> Self {
                    #ty(self.0 ^ rhs.0)
                }
            }

            impl core::ops::BitXorAssign for #ty {
                fn bitxor_assign(&mut self, rhs: Self) {
                    self.0 ^= rhs.0
                }
            }

            impl core::ops::Sub for #ty {
                type Output = Self;
                fn sub(self, rhs: Self) -> Self {
                    #ty(self.0 & !rhs.0)
                }
            }

            impl core::ops::SubAssign for #ty {
                fn sub_assign(&mut self, rhs: Self) {
                    self.0 &= !rhs.0;
                }
            }

            impl core::ops::Not for #ty {
                type Output = Self;
                fn not(self) -> Self {
                    #ty(!self.0)
                }
            }

            impl From<#wrapped_ty> for #ty {
                fn from(flags: #wrapped_ty) -> Self {
                    #ty(flags)
                }
            }

            impl From<#ty> for #wrapped_ty {
                fn from(#ty(flags): #ty) -> Self {
                    flags
                }
            }
        });
    }
}

pub struct RdataTestMod {
    struct_name: syn::Ident,
    cases: Vec<(String, (String, Option<String>))>,
}

impl RdataTestMod {
    pub fn new(def: &Rr, base_path: &Path) -> Self {
        let struct_name = def.struct_name();
        let mut cases = Vec::new();
        for entry in base_path.read_dir().expect("failed to read rr dir") {
            let path = entry.expect("failed to read rr dir entry").path();
            if !path.is_file() {
                continue;
            }
            if path.extension().and_then(|ext| ext.to_str()) != Some("bin") {
                continue;
            }
            let test_id = if let Some(s) = path.file_stem().and_then(|f| f.to_str()) {
                let mut components = s.split('_');
                if Some("sample") != components.next() {
                    continue;
                }
                let kind = match components.next() {
                    Some(s) if s == "sg" || s == "hg" => s, // s = synthetic, h = human, g = good
                    _ => continue,
                };
                let mnemonic_matches = components
                    .next()
                    .map(|s| s.eq_ignore_ascii_case(def.mnemonic()))
                    .unwrap_or(false);
                if !mnemonic_matches {
                    continue;
                }
                let id = if let Some(s) = components.next() {
                    s
                } else {
                    continue;
                };
                if components.next().is_some() {
                    continue;
                }
                format!("{}{}", kind, id)
            } else {
                continue;
            };
            let bin_fn = path.file_name().unwrap().to_str().unwrap().to_string();
            let path_txt = path.with_extension("txt");
            let txt_fn = if !def.is_data() || !path_txt.is_file() {
                None
            } else {
                Some(path_txt.file_name().unwrap().to_str().unwrap().to_string())
            };
            let case = (test_id.to_string(), (bin_fn, txt_fn));
            cases.push(case);
        }
        cases.sort();
        RdataTestMod { struct_name, cases }
    }
}

impl quote::ToTokens for RdataTestMod {
    fn to_tokens(&self, tokens: &mut crate::TokenStream) {
        fn new_ident<T: AsRef<str>>(t: T) -> syn::Ident {
            syn::Ident::new(t.as_ref(), Span::call_site())
        }
        let name = &self.struct_name;
        tokens.extend(quote! {
            use crate::rdata::#name;
            #[test]
            fn default() {
                <#name>::default();
            }
        });
        for &(ref id, (ref bin_fn, ref opt_txt_fn)) in &self.cases {
            let id_upper = id.to_uppercase();
            let wire_id = new_ident(format!("CASE_{}_WIRE", id_upper));
            let wire_marshal_id = new_ident(format!("wire_marshal_{}", id));
            let wire_cmp_id = new_ident(format!("wire_cmp_{}", id));
            let wire_cmp_can_id = new_ident(format!("wire_cmp_canonical_{}", id));
            let (wire_marshal, wire_cmp, wire_cmp_canonical) = if opt_txt_fn.is_some() {
                (
                    quote! { wire_marshal_data },
                    quote! { wire_cmp_data },
                    quote! { wire_cmp_canonical_data },
                )
            } else {
                (
                    quote! { wire_marshal },
                    quote! { wire_cmp },
                    quote! { wire_cmp_canonical },
                )
            };
            tokens.extend(quote! {
                const #wire_id: &'static [u8] = include_bytes!(#bin_fn);

                #[test]
                fn #wire_marshal_id() {
                    crate::rdata::test::#wire_marshal::<#name>(#wire_id);
                }
                #[test]
                fn #wire_cmp_id() {
                    crate::rdata::test::#wire_cmp::<#name>(#wire_id);
                }
                #[test]
                fn #wire_cmp_can_id() {
                    crate::rdata::test::#wire_cmp_canonical::<#name>(#wire_id);
                }
            });

            if let Some(txt_fn) = opt_txt_fn {
                let ascii_id = new_ident(format!("CASE_{}_ASCII", id_upper));
                let ascii_rt_id = new_ident(format!("ascii_roundtrip_{}", id));
                let generic_ascii_rt_id = new_ident(format!("generic_ascii_roundtrip_{}", id));
                let ascii_wire_id = new_ident(format!("ascii_to_wire_{}", id));
                let wire_ascii_id = new_ident(format!("wire_to_ascii_{}", id));
                tokens.extend(quote! {
                    const #ascii_id: &str = include_str!(#txt_fn);

                    #[test]
                    fn #ascii_rt_id() {
                        crate::rdata::test::ascii_roundtrip::<#name>(#ascii_id);
                    }

                    #[test]
                    fn #generic_ascii_rt_id() {
                        crate::rdata::test::generic_ascii_roundtrip::<#name>(#ascii_id, #wire_id);
                    }

                    #[test]
                    fn #ascii_wire_id() {
                        crate::rdata::test::ascii_to_wire::<#name>(#ascii_id, #wire_id);
                    }

                    #[test]
                    fn #wire_ascii_id() {
                        crate::rdata::test::wire_to_ascii::<#name>(#ascii_id, #wire_id);
                    }
                });
            }
        }
    }
}
