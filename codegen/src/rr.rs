use std::borrow::Cow;
use std::collections::{BTreeMap, HashSet};
use std::fmt::{Debug, Write as FmtWrite};
use std::io::Write as IoWrite;
use std::process::{Command, Stdio};
use std::sync::{Arc, OnceLock};

use proc_macro2::TokenStream;
use syn::TypeParam;

use crate::template::DocRefs;

fn nsupdate_supports(ty: &str) -> bool {
    static NSUPDATE_TYPES: OnceLock<Vec<String>> = OnceLock::new();
    NSUPDATE_TYPES
        .get_or_init(|| {
            let output = Command::new("nsupdate")
                .arg("-T")
                .output()
                .expect("failed to spawn nsupdate");
            let stderr = String::from_utf8_lossy(&output.stderr);
            assert!(
                output.status.success(),
                "nsupdate failed - stderr:\n{}",
                stderr
            );
            String::from_utf8_lossy(&output.stdout)
                .lines()
                .filter(|s| !s.is_empty())
                .map(Into::into)
                .collect()
        })
        .iter()
        .any(|s| s == ty)
}

#[derive(Debug)]
enum ByteRep {
    Fixed(u16),
    Variable {
        param: Option<TypeParam>,
        min: u16,
        max: u16,
    },
}

impl ByteRep {
    fn min_max(&self) -> (usize, usize) {
        match *self {
            Self::Fixed(l) => (l as usize, l as usize),
            Self::Variable { min, max, .. } => (min as usize, max as usize),
        }
    }
    fn type_param(&self) -> Option<&TypeParam> {
        match self {
            Self::Variable {
                param: p @ Some(_), ..
            } => p.as_ref(),
            _ => None,
        }
    }
}

#[derive(Debug)]
pub struct ByteFmt {
    optional: bool,
    rep: ByteRep,
    wire: ByteWire,
    ascii: ByteAscii,
    pattern: BytePattern,
}

impl ByteFmt {
    fn from_element(e: &elementtree::Element) -> Self {
        let optional = e
            .get_attr("optional")
            .map(|s| s.parse().expect("optional must be a bool"))
            .unwrap_or(false);
        let wire = match e.get_attr("wire") {
            None | Some("raw") => ByteWire::Raw,
            Some("prefix:8") => ByteWire::Prefix8,
            Some("prefix:16") => ByteWire::Prefix16,
            Some(s) if s.starts_with("fixed:") => {
                let n: u16 = s[6..].parse().expect("byte wire fixed u16");
                ByteWire::Fixed(n)
            }
            Some(other) => panic!("don't know about {}", other),
        };
        let min = e
            .get_attr("min")
            .map(|s| s.parse().expect("byte fmt min u16"))
            .unwrap_or_else(|| match wire {
                ByteWire::Fixed(n) => n,
                _ => 0,
            });
        let max = e
            .get_attr("max")
            .map(|s| s.parse().expect("byte fmt max u16"))
            .unwrap_or_else(|| match wire {
                ByteWire::Prefix8 => 0xFF,
                ByteWire::Fixed(n) => n,
                _ => 0xFFFF,
            });
        let rep = if min == max {
            ByteRep::Fixed(min)
        } else {
            ByteRep::Variable {
                param: None,
                min,
                max,
            }
        };
        let ascii = match e.get_attr("ascii") {
            None => ByteAscii::WireOnly,
            Some("lettersdigits") => ByteAscii::LettersDigits,
            Some("base64") => ByteAscii::Base64,
            Some("base64ws") => ByteAscii::Base64Ws,
            Some("base64wsdash") => ByteAscii::Base64WsDash,
            Some("eui48addr") => ByteAscii::Eui48Addr,
            Some("eui64addr") => ByteAscii::Eui64Addr,
            Some("charstring") => ByteAscii::CharString,
            Some("hex") => ByteAscii::Hex,
            Some("hexws") => ByteAscii::HexWs,
            Some("ilnp-locator32") => ByteAscii::IlnpLocator32,
            Some("ilnp-locator64") => ByteAscii::IlnpLocator64,
            Some("ilnp-nodeid") => ByteAscii::IlnpNodeId,
            Some("nsap-hex") => ByteAscii::NsapHex,
            Some("base32hexnopad") => ByteAscii::Base32HexNoPad,
            Some("nsec3-salt") => ByteAscii::Nsec3Salt,
            Some(other) => panic!("don't know about {}", other),
        };
        let pattern = match e.get_attr("pattern") {
            None => BytePattern::None,
            Some(s) if s.starts_with("regex:") => BytePattern::Regex(s[6..].into()),
            Some(s) => BytePattern::Other(s.into()),
        };
        ByteFmt {
            optional,
            rep,
            wire,
            ascii,
            pattern,
        }
    }
    fn parse_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.ascii {
            ByteAscii::LettersDigits => "crate::datatypes::bytes::parse_ascii_ld",
            ByteAscii::Base64 => "crate::datatypes::bytes::parse_base64",
            ByteAscii::Base64Ws => "crate::datatypes::bytes::parse_base64ws",
            ByteAscii::Base64WsDash => "crate::datatypes::bytes::parse_base64wsdash",
            ByteAscii::CharString => "crate::datatypes::bytes::parse_charstr",
            ByteAscii::Hex => "crate::datatypes::bytes::parse_hex",
            ByteAscii::HexWs => "crate::datatypes::bytes::parse_hexws",
            ByteAscii::NsapHex if !self.optional => {
                return quote! { crate::datatypes::bytes::parse_nsap_hex }
            }
            ByteAscii::Nsec3Salt if !self.optional => {
                return quote! { crate::datatypes::bytes::parse_nsec3_salt }
            }
            ByteAscii::Base32HexNoPad if !self.optional => {
                "crate::datatypes::bytes::parse_base32hexnp"
            }
            ByteAscii::IlnpLocator32 if !self.optional => {
                return quote! { crate::datatypes::ilnp::parse_locator32 };
            }
            ByteAscii::IlnpLocator64 if !self.optional => {
                return quote! { crate::datatypes::ilnp::parse64 };
            }
            ByteAscii::IlnpNodeId if !self.optional => {
                return quote! { crate::datatypes::ilnp::parse64 };
            }
            ByteAscii::Eui48Addr if !self.optional => {
                return quote! { crate::datatypes::eui::parse48 };
            }
            ByteAscii::Eui64Addr if !self.optional => {
                return quote! { crate::datatypes::eui::parse64 };
            }
            _ => unimplemented!(),
        })
        .expect("parse_expr path");
        let (min, max) = self.rep.min_max();
        if self.optional {
            quote! {
                |e| {
                    if e.peek()?.is_none() {
                        return Ok(None);
                    }
                    #path(e, #min, #max).map(Some)
                }
            }
        } else {
            quote! { |e| #path(e, #min, #max) }
        }
    }
    fn present_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.ascii {
            ByteAscii::LettersDigits => "crate::datatypes::bytes::present_ascii_ld",
            ByteAscii::Base64 => "crate::datatypes::bytes::present_base64",
            ByteAscii::Base64Ws => "crate::datatypes::bytes::present_base64ws",
            ByteAscii::Base64WsDash => "crate::datatypes::bytes::present_base64wsdash",
            ByteAscii::CharString => "crate::datatypes::bytes::present_charstr",
            ByteAscii::Hex => "crate::datatypes::bytes::present_hex",
            ByteAscii::HexWs => "crate::datatypes::bytes::present_hexws",
            ByteAscii::NsapHex if !self.optional => {
                return quote! { crate::datatypes::bytes::present_nsap_hex }
            }
            ByteAscii::Nsec3Salt if !self.optional => {
                return quote! { crate::datatypes::bytes::present_nsec3_salt }
            }
            ByteAscii::Base32HexNoPad if !self.optional => {
                "crate::datatypes::bytes::present_base32hexnp"
            }
            ByteAscii::IlnpLocator32 if !self.optional => {
                return quote! { crate::datatypes::ilnp::present_locator32 };
            }
            ByteAscii::IlnpLocator64 if !self.optional => {
                return quote! { crate::datatypes::ilnp::present64 };
            }
            ByteAscii::IlnpNodeId if !self.optional => {
                return quote! { crate::datatypes::ilnp::present64 };
            }
            ByteAscii::Eui48Addr if !self.optional => {
                return quote! { crate::datatypes::eui::present };
            }
            ByteAscii::Eui64Addr if !self.optional => {
                return quote! { crate::datatypes::eui::present };
            }
            _ => unimplemented!(),
        })
        .expect("present_expr path");
        let (min, max) = self.rep.min_max();
        if self.optional {
            quote! {
                |m, e| {
                    if let Some(m) = m.as_ref() {
                        #path(m, e, #min, #max)
                    } else {
                        Ok(())
                    }
                }
            }
        } else {
            quote! { |m, e| #path(m, e, #min, #max) }
        }
    }
    fn pack_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.wire {
            ByteWire::Prefix8 if self.ascii == ByteAscii::LettersDigits => {
                "crate::datatypes::bytes::pack8_ld"
            }
            _ if self.ascii == ByteAscii::LettersDigits => unimplemented!(),
            ByteWire::Raw => "crate::datatypes::bytes::pack",
            ByteWire::Prefix8 => "crate::datatypes::bytes::pack8",
            ByteWire::Prefix16 => "crate::datatypes::bytes::pack16",
            _ => unreachable!(),
        })
        .expect("pack_expr path");
        let (min, max) = self.rep.min_max();
        let expr = quote! { #path(m, w, #min, #max) };
        if self.optional {
            quote! {
                |m, w| {
                    if let Some(m) = m.as_ref() {
                        #expr
                    } else {
                        Ok(())
                    }
                }
            }
        } else {
            quote! { |m, w| #expr }
        }
    }
    fn pack_len_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.wire {
            ByteWire::Prefix8 if self.ascii == ByteAscii::LettersDigits => {
                "crate::datatypes::bytes::packed8_len_ld"
            }
            _ if self.ascii == ByteAscii::LettersDigits => unimplemented!(),
            ByteWire::Raw => "crate::datatypes::bytes::packed_len",
            ByteWire::Prefix8 => "crate::datatypes::bytes::packed8_len",
            ByteWire::Prefix16 => "crate::datatypes::bytes::packed16_len",
            _ => unreachable!(),
        })
        .expect("pack_len_expr path");
        let (min, max) = self.rep.min_max();
        let expr = quote! { #path(m, #min, #max) };
        if self.optional {
            quote! {
                |m| {
                    if let Some(m) = m.as_ref() {
                        #expr
                    } else {
                        Ok(0)
                    }
                }
            }
        } else {
            quote! { |m| #expr }
        }
    }
    fn unpack_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.wire {
            ByteWire::Prefix8 if self.ascii == ByteAscii::LettersDigits => {
                "crate::datatypes::bytes::unpack8_ld"
            }
            _ if self.ascii == ByteAscii::LettersDigits => unimplemented!(),
            ByteWire::Raw => "crate::datatypes::bytes::unpack",
            ByteWire::Prefix8 => "crate::datatypes::bytes::unpack8",
            ByteWire::Prefix16 => "crate::datatypes::bytes::unpack16",
            _ => unreachable!(),
        })
        .expect("unpack_expr path");
        let (min, max) = self.rep.min_max();
        let expr = quote! { #path(r, #min, #max) };
        if self.optional {
            quote! {
                |r| {
                    if r.is_empty() {
                        Ok(None)
                    } else {
                        (#expr).map(Some)
                    }
                }
            }
        } else {
            quote! { |r| #expr }
        }
    }
    fn unpacked_len_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.wire {
            ByteWire::Prefix8 if self.ascii == ByteAscii::LettersDigits => {
                "crate::datatypes::bytes::unpacked8_len_ld"
            }
            _ if self.ascii == ByteAscii::LettersDigits => unimplemented!(),
            ByteWire::Raw => "crate::datatypes::bytes::unpacked_len",
            ByteWire::Prefix8 => "crate::datatypes::bytes::unpacked8_len",
            ByteWire::Prefix16 => "crate::datatypes::bytes::unpacked16_len",
            _ => unreachable!(),
        })
        .expect("unpacked_len_expr path");
        let (min, max) = self.rep.min_max();
        let expr = quote! { #path(r, #min, #max) };
        if self.optional {
            quote! {
                |r| {
                    if r.is_empty() {
                        Ok(0)
                    } else {
                        #expr
                    }
                }
            }
        } else {
            quote! { |r| #expr }
        }
    }
    fn cmp_expr(&self) -> TokenStream {
        let path: syn::Path = syn::parse_str(match self.wire {
            ByteWire::Prefix8 if self.ascii == ByteAscii::LettersDigits => {
                "crate::datatypes::bytes::cmp8_ld"
            }
            _ if self.ascii == ByteAscii::LettersDigits => unreachable!(),
            ByteWire::Raw => "crate::datatypes::bytes::cmp",
            ByteWire::Prefix8 => "crate::datatypes::bytes::cmp8",
            ByteWire::Prefix16 => "crate::datatypes::bytes::cmp16",
            ByteWire::Fixed(_) => unreachable!(),
        })
        .expect("cmp_expr path");
        let (min, max) = self.rep.min_max();
        if self.optional {
            quote! {
                |l, r| {
                    match (l.is_empty(), r.is_empty()) {
                        (true, true) => Ok(core::cmp::Ordering::Equal),
                        (true, false) => Ok(core::cmp::Ordering::Less),
                        (false, true) => Ok(core::cmp::Ordering::Greater),
                        (false, false) => #path(l, r, #min, #max),
                    }
                }
            }
        } else {
            quote! { |l, r| #path(l, r, #min, #max) }
        }
    }
    fn cmp_as_wire_expr(&self) -> TokenStream {
        match self.wire {
            ByteWire::Raw if !self.optional => {
                quote! { |l, r| <[u8]>::cmp(l.as_ref(), r.as_ref()) }
            }
            ByteWire::Prefix8 if !self.optional => quote! {
                |l, r| {
                    let l: &[u8] = l.as_ref();
                    let r: &[u8] = r.as_ref();
                    let l_len = l.len() as u8;
                    let r_len = r.len() as u8;
                    match l_len.cmp(&r_len) {
                        core::cmp::Ordering::Equal => <[u8]>::cmp(l, r),
                        other => other,
                    }
                }
            },
            ByteWire::Prefix8 => quote! {
                |l, r| {
                    match (l.as_ref(), r.as_ref()) {
                        (None, None) => core::cmp::Ordering::Equal,
                        (None, Some(_)) => core::cmp::Ordering::Less,
                        (Some(_), None) => core::cmp::Ordering::Greater,
                        (Some(l), Some(r)) => {
                            let l: &[u8] = l.as_ref();
                            let r: &[u8] = r.as_ref();
                            let l_len = l.len() as u8;
                            let r_len = r.len() as u8;
                            match l_len.cmp(&r_len) {
                                core::cmp::Ordering::Equal => <[u8]>::cmp(l, r),
                                other => other,
                            }
                        }
                    }
                }
            },
            ByteWire::Prefix16 if !self.optional => quote! {
                |l, r| {
                    let l = l.as_ref();
                    let r = r.as_ref();
                    let l_len = (l.len() as u16).to_be_bytes();
                    let r_len = (r.len() as u16).to_be_bytes();
                    match l_len.cmp(&r_len) {
                        core::cmp::Ordering::Equal => <[u8]>::cmp(l, r),
                        other => other,
                    }
                }
            },
            ByteWire::Fixed(_) if !self.optional => quote! {
                |l, r| <[u8]>::cmp(l, r)
            },
            _ => unimplemented!(),
        }
    }
    fn attr(&self) -> TokenStream {
        let ascii_helpers = if self.ascii == ByteAscii::WireOnly {
            None
        } else {
            let parse = self.parse_expr().to_string();
            let present = self.present_expr().to_string();
            Some((parse, present))
        };
        assert!(ascii_helpers.is_some() || self.ascii == ByteAscii::WireOnly);
        let ascii_attrs = if let Some((parse, present)) = ascii_helpers {
            if let Some(tp) = self.rep.type_param() {
                let present_bound = format!("{}: AsRef<[u8]>", tp.ident);
                let parse_bound = format!("{}: for<'a> From<&'a [u8]>", tp.ident);
                Some(quote! {
                    present_bound = #present_bound,
                    present = #present,
                    parse_bound = #parse_bound,
                    parse = #parse,
                })
            } else {
                Some(quote! {
                    present = #present,
                    parse = #parse,
                })
            }
        } else {
            None
        };
        if let ByteWire::Fixed(_) = self.wire {
            assert!(!self.optional);
            return quote! { #[yardi(#ascii_attrs)] };
        }
        let pack = self.pack_expr().to_string();
        let packed_len = self.pack_len_expr().to_string();
        let unpack = self.unpack_expr().to_string();
        let unpacked_len = self.unpacked_len_expr().to_string();
        let cmp = self.cmp_expr().to_string();
        let cmp_as_wire = self.cmp_as_wire_expr().to_string();
        if let Some(tp) = self.rep.type_param() {
            let pack_bound = format!("{}: AsRef<[u8]>", tp.ident);
            let unpack_bound = format!("{}: From<&'yr [u8]>", tp.ident);
            let cmp_bound = format!("{}: AsRef<[u8]>", tp.ident);
            quote! {
                #[yardi(#ascii_attrs
                        pack_bound = #pack_bound,
                        pack = #pack,
                        packed_len = #packed_len,
                        unpack_bound = #unpack_bound,
                        unpack = #unpack,
                        unpacked_len = #unpacked_len,
                        cmp_bound = #cmp_bound,
                        cmp_wire = #cmp,
                        cmp_wire_canonical = #cmp,
                        cmp_as_wire = #cmp_as_wire,
                        cmp_as_wire_canonical = #cmp_as_wire,
                )]
            }
        } else {
            quote! {
                #[yardi(#ascii_attrs
                        pack = #pack,
                        packed_len = #packed_len,
                        unpack = #unpack,
                        unpacked_len = #unpacked_len,
                        cmp_wire = #cmp,
                        cmp_wire_canonical = #cmp,
                        cmp_as_wire = #cmp_as_wire,
                        cmp_as_wire_canonical = #cmp_as_wire,
                )]
            }
        }
    }
    fn samples(&self) -> Cow<'static, [(&'static str, &'static [u8])]> {
        let s: &[(&str, &[u8])] = match *self {
            ByteFmt {
                wire: ByteWire::Raw,
                ref ascii,
                ..
            } => match ascii {
                ByteAscii::Base64 | ByteAscii::Base64Ws | ByteAscii::Base64WsDash => {
                    &[("eWFyZGk=", b"yardi")]
                }
                ByteAscii::CharString => &[(r#""x""#, &[b'x'])],
                ByteAscii::Hex | ByteAscii::HexWs => &[("DD", &[0xDD])],
                ByteAscii::NsapHex => &[("0xEE", &[0xEE])],
                _ => panic!("{:?} not implemented for raw", ascii),
            },
            ByteFmt {
                wire: ByteWire::Fixed(4),
                ascii: ByteAscii::IlnpLocator32,
                ..
            } => &[("1.2.3.4", &[1, 2, 3, 4])],
            ByteFmt {
                wire: ByteWire::Fixed(6),
                ascii: ByteAscii::Eui48Addr,
                ..
            } => &[("00-11-22-33-44-55", &[0x00, 0x11, 0x22, 0x33, 0x44, 0x55])],
            ByteFmt {
                wire: ByteWire::Fixed(8),
                ascii: ByteAscii::Eui64Addr,
                ..
            } => &[(
                "00-11-22-33-44-55-66-77",
                &[0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77],
            )],
            ByteFmt {
                wire: ByteWire::Fixed(8),
                ascii: ByteAscii::IlnpLocator64,
                ..
            }
            | ByteFmt {
                wire: ByteWire::Fixed(8),
                ascii: ByteAscii::IlnpNodeId,
                ..
            } => &[(
                "1111:2222:3333:4444",
                &[0x11, 0x11, 0x22, 0x22, 0x33, 0x33, 0x44, 0x44],
            )],
            ByteFmt {
                wire: ByteWire::Fixed(n),
                ref ascii,
                ..
            } => {
                panic!("{:?} not implemented for fixed {}", ascii, n);
            }
            ByteFmt {
                wire: ByteWire::Prefix8,
                ref ascii,
                ..
            } => match ascii {
                ByteAscii::LettersDigits => &[("issue", b"\x05issue")],
                ByteAscii::CharString => &[(r#""y""#, &[1, b'y'])],
                ByteAscii::Nsec3Salt => &[("-", &[0]), ("CC", &[1, 0xCC])],
                ByteAscii::Base32HexNoPad => &[("f5gn4p39", b"\x05yardi")],
                _ => panic!("{:?} not implemented for prefix8", ascii),
            },
            ByteFmt {
                wire: ByteWire::Prefix16,
                ref ascii,
                ..
            } => match ascii {
                ByteAscii::WireOnly => &[("N/A", &[0, 1, 2])],
                _ => panic!("{:?} not implemented for prefix16", ascii),
            },
        };
        if self.optional {
            let mut v = Vec::with_capacity(s.len() + 1);
            v.push(("", &[][..]));
            v.extend_from_slice(s);
            Cow::Owned(v)
        } else {
            Cow::Borrowed(s)
        }
    }
}

impl quote::ToTokens for ByteFmt {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let inner = if let ByteWire::Fixed(n) = self.wire {
            let n = n as usize;
            quote! { [u8; #n] }
        } else {
            let ident = &self.rep.type_param().unwrap().ident;
            quote! { #ident }
        };
        tokens.extend(if self.optional {
            quote! { Option<#inner> }
        } else {
            inner
        })
    }
}

#[derive(Debug)]
enum ByteWire {
    Raw,
    Fixed(u16),
    Prefix8,
    Prefix16,
}

#[derive(Debug, PartialEq)]
enum ByteAscii {
    LettersDigits,
    Base64,
    Base64Ws,
    Base64WsDash,
    CharString,
    Eui48Addr,
    Eui64Addr,
    Hex,
    HexWs,
    IlnpLocator32,
    IlnpLocator64,
    IlnpNodeId,
    NsapHex,
    Base32HexNoPad,
    Nsec3Salt,
    WireOnly,
}

#[derive(Debug)]
enum BytePattern {
    None,
    Regex(String),
    Other(String),
}

#[derive(Debug)]
enum DataTy {
    Bytes(ByteFmt),
    CaaFlags,
    CertAlg,
    CertType,
    CsyncFlags,
    DnskeyFlags,
    DnskeyProtocol,
    DnssecAlg,
    DnssecAlgNum,
    DsDigestAlg,
    Ipv4,
    Ipv6,
    KeyFlags,
    KeyProtocol,
    Name,
    NsecTypeBitmap { param: Option<TypeParam> },
    Nsec3Flags,
    Nsec3HashAlg,
    Nsec3paramFlags,
    RrType,
    SvcbParams { param: Option<TypeParam> },
    SshfpPkAlg,
    SshfpType,
    Timestamp,
    Timestamp14,
    TlsaCertUsage,
    TlsaMatchType,
    TlsaSelector,
    TsigRcode,
    TsigTime,
    Ttl,
    TtlSig,
    Txt { param: Option<TypeParam> },
    U16,
    U16Octal,
    U32,
    U8,
}

impl DataTy {
    fn max_len(&self) -> usize {
        match self {
            DataTy::Bytes(ByteFmt { rep, .. }) => rep.min_max().1,
            DataTy::CaaFlags
            | DataTy::CertAlg
            | DataTy::DnskeyProtocol
            | DataTy::DnssecAlg
            | DataTy::DnssecAlgNum
            | DataTy::DsDigestAlg
            | DataTy::KeyProtocol
            | DataTy::Nsec3Flags
            | DataTy::Nsec3HashAlg
            | DataTy::Nsec3paramFlags
            | DataTy::SshfpType
            | DataTy::SshfpPkAlg
            | DataTy::TlsaCertUsage
            | DataTy::TlsaMatchType
            | DataTy::TlsaSelector
            | DataTy::U8 => 1,
            DataTy::CertType
            | DataTy::CsyncFlags
            | DataTy::DnskeyFlags
            | DataTy::KeyFlags
            | DataTy::RrType
            | DataTy::TsigRcode
            | DataTy::U16
            | DataTy::U16Octal => 2,
            DataTy::Ipv4
            | DataTy::Timestamp
            | DataTy::Timestamp14
            | DataTy::Ttl
            | DataTy::TtlSig
            | DataTy::U32 => 4,
            DataTy::TsigTime => 6,
            DataTy::Ipv6 => 16,
            DataTy::Name => 255,
            DataTy::NsecTypeBitmap { .. } => 8704, // TODO: 8688?
            DataTy::SvcbParams { .. } | DataTy::Txt { .. } => 0xFFFF + 1,
        }
    }
    fn from_element(e: &elementtree::Element) -> Self {
        let optional = e
            .get_attr("optional")
            .map(|s| s.parse().expect("optional must be a bool"))
            .unwrap_or(false);
        match e.get_attr("type").expect("data type") {
            "bytes" => DataTy::Bytes(ByteFmt::from_element(e)),
            "caa-flags" if !optional => DataTy::CaaFlags,
            "cert-alg" if !optional => DataTy::CertAlg,
            "cert-type" if !optional => DataTy::CertType,
            "csync-flags" if !optional => DataTy::CsyncFlags,
            "dnskey-flags" if !optional => DataTy::DnskeyFlags,
            "dnskey-protocol" if !optional => DataTy::DnskeyProtocol,
            "dnssec-alg" if !optional => DataTy::DnssecAlg,
            "dnssec-alg-num" if !optional => DataTy::DnssecAlgNum,
            "ds-digestalg" if !optional => DataTy::DsDigestAlg,
            "ipv4" if !optional => DataTy::Ipv4,
            "ipv6" if !optional => DataTy::Ipv6,
            "key-flags" if !optional => DataTy::KeyFlags,
            "key-protocol" if !optional => DataTy::KeyProtocol,
            "name" if !optional => DataTy::Name,
            "nsec-type-bitmap" if !optional => DataTy::NsecTypeBitmap { param: None },
            "nsec3-flags" if !optional => DataTy::Nsec3Flags,
            "nsec3-hashalg" if !optional => DataTy::Nsec3HashAlg,
            "nsec3param-flags" if !optional => DataTy::Nsec3paramFlags,
            "rrtype" if !optional => DataTy::RrType,
            "svcb-params" if !optional => DataTy::SvcbParams { param: None },
            "sshfp-pkalg" if !optional => DataTy::SshfpPkAlg,
            "sshfp-type" if !optional => DataTy::SshfpType,
            "timestamp" if !optional => DataTy::Timestamp,
            "timestamp14" if !optional => DataTy::Timestamp14,
            "tlsa-certusage" if !optional => DataTy::TlsaCertUsage,
            "tlsa-matchtype" if !optional => DataTy::TlsaMatchType,
            "tlsa-selector" if !optional => DataTy::TlsaSelector,
            "tsig-rcode" if !optional => DataTy::TsigRcode,
            "tsig-time" if !optional => DataTy::TsigTime,
            "ttl" if !optional => DataTy::Ttl,
            "ttl-sig" if !optional => DataTy::TtlSig,
            "txt" if !optional => DataTy::Txt { param: None },
            "u16" if !optional => DataTy::U16,
            "u16-octal" if !optional => DataTy::U16Octal,
            "u32" if !optional => DataTy::U32,
            "u8" if !optional => DataTy::U8,
            other => panic!("what is {} (optional: {})", other, optional),
        }
    }
    fn attr(&self) -> Option<TokenStream> {
        match *self {
            DataTy::Bytes(ref f) => Some(f.attr()),
            DataTy::NsecTypeBitmap { ref param }
            | DataTy::SvcbParams { ref param }
            | DataTy::Txt { ref param } => {
                let tp = param.as_ref().unwrap();
                let present_bound = format!("{}: AsRef<[u8]>", tp.ident);
                let parse_bound = format!("{}: for<'a> From<&'a [u8]>", tp.ident);
                let pack_bound = &present_bound;
                let unpack_bound = format!("{}: From<&'yr [u8]>", tp.ident);
                Some(quote! {
                    #[yardi(
                        present_bound = #present_bound,
                        parse_bound=#parse_bound,
                        pack_bound = #pack_bound,
                        unpack_bound = #unpack_bound,
                    )]
                })
            }
            DataTy::U16Octal => Some(quote! {
                #[yardi(parse = "crate::datatypes::int::parse_u16_octal",
                        present = "|m, e| crate::datatypes::int::present_u16_octal(*m, e)")]
            }),
            _ => None,
        }
    }
    fn can_default(&self) -> bool {
        !matches!(*self, DataTy::Ipv4 | DataTy::Ipv6)
    }
    fn ty_param(&self) -> Option<&TypeParam> {
        match self {
            DataTy::Bytes(ByteFmt { rep, .. }) => rep.type_param(),
            DataTy::SvcbParams { param } => param.as_ref(),
            DataTy::Txt { param } => param.as_ref(),
            DataTy::NsecTypeBitmap { param } => param.as_ref(),
            _ => None,
        }
    }
    fn note(&self) -> Option<&str> {
        match *self {
            DataTy::DnssecAlgNum => Some(
                "Values are supposed to be numbers only however mnemonics are accepted when parsing. Values are always presented as numbers.",
            ),
            DataTy::Timestamp14 => Some(
                "Values are supposed to be in the form `YYYYMMDDHHMMSS` however both this form and UNIX timestamps are accepted when parsing. Time values are always presented in `YYYYMMDDHHMMSS` format.",
            ),
            DataTy::TtlSig => Some(
                "Value is supposed to be optional with the owner TTL being substituted if it is not present. If the owner's TTL is absent the value of this field is supposed to be used.",
            ),
            _ => None,
        }
    }
    fn samples(&self) -> Cow<'static, [(&'static str, &'static [u8])]> {
        let s: &[(&str, &[u8])] = match *self {
            DataTy::Bytes(ref b) => return b.samples(),
            DataTy::CaaFlags => &[("128", &[128])],
            DataTy::CertAlg => &[("1", &[1])],
            DataTy::CertType => &[("257", &[1, 1])],
            DataTy::CsyncFlags => &[("1", &[0, 1])],
            DataTy::DnskeyFlags => &[("256", &[1, 0])],
            DataTy::DnskeyProtocol => &[("3", &[3])],
            DataTy::DnssecAlg | DataTy::DnssecAlgNum => &[("255", &[255])],
            DataTy::DsDigestAlg => &[("254", &[254])],
            DataTy::Ipv4 => &[("127.0.0.1", &[127, 0, 0, 1])],
            DataTy::Ipv6 => &[("::1", &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1])],
            DataTy::KeyFlags => &[("0", &[0, 0])],
            DataTy::KeyProtocol => &[("3", &[3])],
            DataTy::Name => &[("x.", &[1, b'x', 0])],
            DataTy::NsecTypeBitmap { .. } => &[("A NS", &[0, 1, 0b0110_0000])],
            DataTy::Nsec3Flags | DataTy::Nsec3paramFlags | DataTy::Nsec3HashAlg => &[("0", &[0])],
            DataTy::RrType => &[("TYPE65280", &[0xFF, 00])],
            DataTy::SvcbParams { .. } => &[("", &[]), ("key667=hello", b"\x02\x9b\x00\x05hello")],
            DataTy::SshfpPkAlg | DataTy::SshfpType => &[("1", &[1])],
            DataTy::Timestamp | DataTy::Timestamp14 => &[("20180630144515", &[91, 55, 151, 123])],
            DataTy::TlsaCertUsage | DataTy::TlsaMatchType | DataTy::TlsaSelector => &[("0", &[0])],
            DataTy::Ttl | DataTy::TtlSig => &[("3", &[0, 0, 0, 3])],
            DataTy::Txt { .. } => &[(r#""t""#, &[1, b't'])],
            DataTy::U8 => &[("1", &[1])],
            DataTy::U16 => &[("256", &[1, 0])],
            DataTy::U16Octal => &[("10", &[0, 8])],
            DataTy::U32 => &[("16777216", &[1, 0, 0, 0])],
            DataTy::TsigRcode => &[("N/A", &[0, 0])],
            DataTy::TsigTime => &[("N/A", &[0, 0, 91, 55, 151, 123])],
        };
        Cow::Borrowed(s)
    }
}

impl quote::ToTokens for DataTy {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        tokens.extend(match *self {
            DataTy::Bytes(ref b) => quote! { #b },
            DataTy::CaaFlags => quote! { crate::rdata::caa::CaaFlags },
            DataTy::CertAlg => quote! { crate::rdata::cert::CertAlg },
            DataTy::CertType => quote! { crate::rdata::cert::CertTy },
            DataTy::CsyncFlags => quote! { crate::rdata::csync::CsyncFlags },
            DataTy::DnskeyFlags => quote! { crate::rdata::dnskey::DnskeyFlags },
            DataTy::DnskeyProtocol => quote! { crate::rdata::dnskey::DnskeyProtocol },
            DataTy::DnssecAlg | DataTy::DnssecAlgNum => quote! { crate::dnssec::SigAlg },
            DataTy::DsDigestAlg => quote! { crate::rdata::ds::DsDigestAlg },
            DataTy::Ipv4 => quote! { core::net::Ipv4Addr },
            DataTy::Ipv6 => quote! { core::net::Ipv6Addr },
            DataTy::KeyFlags => quote! { crate::rdata::key::KeyFlags },
            DataTy::KeyProtocol => quote! { crate::rdata::key::KeyProtocol },
            DataTy::Name => quote! { crate::datatypes::Name },
            DataTy::NsecTypeBitmap { ref param } => {
                let ident = &param.as_ref().unwrap().ident;
                quote! { crate::rdata::nsec::NsecTypeBitmap < #ident > }
            }
            DataTy::Nsec3Flags => quote! { crate::rdata::nsec3::Nsec3Flags },
            DataTy::Nsec3HashAlg => quote! { crate::rdata::nsec3::Nsec3HashAlg },
            DataTy::Nsec3paramFlags => quote! { crate::rdata::nsec3param::Nsec3paramFlags },
            DataTy::RrType => quote! { crate::datatypes::Ty },
            DataTy::SvcbParams { ref param } => {
                let ident = &param.as_ref().unwrap().ident;
                quote! { crate::rdata::svcb::svcb_params::SvcbParams < #ident > }
            }
            DataTy::SshfpPkAlg => quote! { crate::rdata::sshfp::SshfpPubKeyAlg },
            DataTy::SshfpType => quote! { crate::rdata::sshfp::SshfpFpTy },
            DataTy::Timestamp | DataTy::Timestamp14 => quote! { crate::datatypes::Time },
            DataTy::TlsaCertUsage => quote! { crate::rdata::tlsa::TlsaCertUsage },
            DataTy::TlsaMatchType => quote! { crate::rdata::tlsa::TlsaMatchTy },
            DataTy::TlsaSelector => quote! { crate::rdata::tlsa::TlsaSelector },
            DataTy::TsigRcode => quote! { crate::rdata::tsig::TsigRcode },
            DataTy::TsigTime => quote! { crate::rdata::tsig::TsigTime },
            DataTy::Ttl | DataTy::TtlSig => quote! { crate::datatypes::Ttl },
            DataTy::Txt { ref param } => {
                let ident = &param.as_ref().unwrap().ident;
                quote! { crate::rdata::txt::TxtData < #ident > }
            }
            DataTy::U16 | DataTy::U16Octal => quote! { u16 },
            DataTy::U32 => quote! { u32 },
            DataTy::U8 => quote! { u8 },
        });
    }
}

#[derive(Debug)]
pub struct Field {
    ty: Arc<crate::iana::TypeInfo>,
    name: String,
    desc: Option<String>,
    data_ty: DataTy,
}

impl Field {
    fn from_element(e: &elementtree::Element, ty: Arc<crate::iana::TypeInfo>) -> Self {
        let name = e.find("name").expect("name").text();
        let desc = e.find("description").map(|e| e.text());
        let data = e.find("data").expect("data");
        let data_ty = DataTy::from_element(data);
        Field {
            ty,
            name: name.into(),
            desc: desc.map(Into::into),
            data_ty,
        }
    }
    fn can_default(&self) -> bool {
        self.data_ty.can_default()
    }
    fn needs_ord_eq_bound(&self) -> bool {
        matches!(
            self.data_ty,
            DataTy::NsecTypeBitmap { .. } | DataTy::SvcbParams { .. } | DataTy::Txt { .. }
        )
    }
    fn ty_param(&self) -> Option<&TypeParam> {
        self.data_ty.ty_param()
    }
    fn samples(&self, _class: &str, ty: &str) -> Cow<'static, [(&'static str, &'static [u8])]> {
        match (ty, self.name.as_str()) {
            ("X25", _) => Cow::Borrowed(&[("\"0255503330\"", b"\x0A0255503330")]),
            ("NAPTR", "Regexp") => Cow::Borrowed(&[(
                "\"/^http:\\\\/\\\\/([^\\\\/:]+)/\\\\1/i\"",
                b"\x19/^http:\\/\\/([^\\/:]+)/\\1/i",
            )]),
            ("SSHFP", "Algorithm") | ("SSHFP", "Type") => Cow::Borrowed(&[("254", &[254])]),
            ("NSEC3" | "NSEC3PARAM", "Iterations") => {
                // TODO: shared constant for maximum reasonable iterations?
                Cow::Borrowed(&[("0", &[0, 0])])
            }
            ("RKEY", "Flags") => Cow::Borrowed(&[("0", &[0, 0])]),
            ("ZONEMD", "Hash Algorithm") => Cow::Borrowed(&[("255", &[255])]),
            ("ZONEMD", "Digest") => Cow::Borrowed(&[(
                "000000000000000000000000",
                &[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
            )]),
            _ => self.data_ty.samples(),
        }
    }
}

impl quote::ToTokens for Field {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let error_subject = if self.ty.mstr == self.name {
            self.name.clone()
        } else {
            format!("{} {}", self.ty.mstr, self.name)
        };
        let mut doc = if let Some(ref s) = self.desc {
            format!("{}: {}", self.name, s)
        } else {
            format!("{}.", self.name)
        };
        if let Some(note) = self.data_ty.note() {
            doc.push_str("\n\n**Note**: ");
            doc.push_str(note);
        }
        let member_name = crate::member_name(&self.name);
        let data_ty = &self.data_ty;
        let data_attr = data_ty.attr();
        tokens.extend(quote! {
            #[doc=#doc]
            #data_attr
            #[yardi(error_subject=#error_subject)]
            pub #member_name: #data_ty
        })
    }
}

#[derive(Debug)]
pub struct Rr {
    cl: Option<Arc<crate::iana::ClassInfo>>,
    ty: Arc<crate::iana::TypeInfo>,
    fields: Vec<Field>,
    samples: Vec<(String, Vec<u8>)>,
}

impl Rr {
    pub fn is_data(&self) -> bool {
        self.ty.is_data
    }
    pub fn class(&self) -> Option<&str> {
        self.cl.as_ref().map(|cl| cl.mstr.as_ref())
    }
    pub fn struct_name(&self) -> syn::Ident {
        let maybe_cl = self.cl.as_ref().and_then(|i| match i.mstr.as_str() {
            "ANY" | "IN" => None,
            other => Some(other),
        });
        if let Some(cl) = maybe_cl {
            crate::struct_name(format!("{}-{}", cl, self.ty.mstr).as_str())
        } else {
            crate::struct_name(self.ty.mstr.as_str())
        }
    }
    pub fn mnemonic(&self) -> &str {
        &self.ty.mstr
    }
    pub fn class_mnemonic(&self) -> Option<&str> {
        self.cl.as_ref().map(|cl| cl.mstr.as_str())
    }
    pub fn mod_name(&self) -> String {
        let maybe_cl = self.cl.as_ref().and_then(|i| match i.mstr.as_str() {
            "ANY" | "IN" => None,
            other => Some(other),
        });
        if let Some(cl) = maybe_cl {
            crate::mod_name(format!("{}-{}", cl, self.ty.mstr).as_str())
        } else {
            crate::mod_name(self.ty.mstr.as_str())
        }
    }
    pub fn samples(&self) -> &[(String, Vec<u8>)] {
        &self.samples[..]
    }
    fn max_len(&self) -> usize {
        self.fields.iter().map(|f| f.data_ty.max_len()).sum()
    }
}

impl quote::ToTokens for Rr {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let name = self.struct_name();
        let fields = self.fields.iter();
        let mut title = self.ty.mstr.clone();
        let cl_ident = if let Some(c) = self.cl.as_ref() {
            write!(title, " ({})", c.mstr.as_str()).unwrap();
            Some(crate::const_name(&c.mstr))
        } else {
            None
        };
        if let Some(desc) = self.ty.desc.as_ref() {
            write!(title, " - {}", desc).unwrap();
        }
        title.push('.');
        let mut yardi_attrs = vec![quote! { crate="crate" }];
        let mut derives = vec!["Clone", "Pack", "Unpack", "UnpackedLen", "WireOrd"];
        if self.fields.iter().all(|f| f.can_default()) {
            derives.push("Default");
        }
        if self.ty.is_data {
            derives.extend(&["Parse", "Present", "Rdata"]);
            if let Some(ident) = cl_ident.as_ref() {
                let path = format!("crate::datatypes::Class::{}", ident);
                yardi_attrs.push(quote! { rr_class = #path });
            }
            let rr_ty_path = format!("crate::datatypes::Ty::{}", crate::const_name(&self.ty.mstr));
            yardi_attrs.push(quote! { rr_ty = #rr_ty_path });
            if self.max_len() <= 0xFFFF {
                yardi_attrs.push(quote! { rr_skip_len_check });
            }
        }
        yardi_attrs.push({
            let error_subject = &self.ty.mstr;
            quote! { error_subject = #error_subject }
        });
        let maybe_from_str = self.ty.is_data.then(|| quote! {
            impl core::str::FromStr for #name {
                type Err = crate::ascii::parse::ParseError;
                fn from_str(s: &str) -> Result<Self, Self::Err> {
                    crate::ascii::parse::from_single_entry(s.as_bytes(), crate::ascii::Context::shared_default())
                }
            }
        });
        let doc_refs = DocRefs(&self.ty.refs);
        let extra_impl = {
            let ty_params: Vec<_> = self
                .fields
                .iter()
                .filter_map(|f| f.ty_param().map(|t| &t.ident))
                .collect();
            let where_clause = (!ty_params.is_empty()).then(|| {
                quote::quote! {
                    where #(#ty_params: AsRef<[u8]>),*
                }
            });
            let ff = self.fields.iter().map(|f| {
                let m = crate::member_name(&f.name);
                let m_str = m.to_string();
                let v = match &f.data_ty {
                    DataTy::Bytes(ByteFmt { optional, rep, .. }) if rep.type_param().is_some() => {
                        let get = if *optional {
                            quote! { self.#m.as_ref().map(AsRef::as_ref).unwrap_or_default() }
                        } else {
                            quote! { self.#m.as_ref() }
                        };
                        quote! { &crate::datatypes::bytes::DebugFmt(#get) }
                    }
                    _ => quote! { &self.#m },
                };
                quote! { .field(#m_str, #v) }
            });
            let cf = self.fields.iter().map(|f| {
                let m = crate::member_name(&f.name);
                match f.data_ty {
                    DataTy::Bytes(ByteFmt{ optional: true, .. }) => {
                        quote! {
                            if let order @ Less | order @ Greater = Ord::cmp(
                                self.#m.as_ref().map(AsRef::as_ref),
                                other.#m.as_ref().map(AsRef::as_ref),
                            ) {
                                return order;
                            }
                        }
                    }
                    DataTy::Bytes( .. ) => {
                        quote! {
                            if let order @ Less | order @ Greater = Ord::cmp(self.#m.as_ref(), other.#m.as_ref()) {
                                return order;
                            }
                        }
                    }
                    _ => {
                        quote! {
                            if let order @ Less | order @ Greater = Ord::cmp(&self.#m, &other.#m) {
                                return order;
                            }
                        }
                    }
                }
            });
            let ef = self.fields.iter().map(|f| {
                let m = crate::member_name(&f.name);
                match f.data_ty {
                    DataTy::Bytes(ByteFmt{ optional: true, .. }) => {
                        quote! {
                            if self.#m.as_ref().map(AsRef::as_ref) != other.#m.as_ref().map(AsRef::as_ref) {
                                return false;
                            }
                        }
                    }
                    DataTy::Bytes( .. ) => {
                        quote! {
                            if self.#m.as_ref() != other.#m.as_ref() {
                                return false;
                            }
                        }
                    }
                    _ => {
                        quote! {
                            if self.#m != other.#m {
                                return false;
                            }
                        }
                    }
                }
            });
            let name_str = name.to_string();
            let maybe_ord_eq_impl = if self.fields.iter().any(|f| f.needs_ord_eq_bound()) {
                quote! {
                    impl < #(#ty_params),* > core::cmp::Eq for # name < #(#ty_params),* > #where_clause {}
                    impl < #(#ty_params),* > core::cmp::Ord for # name < #(#ty_params),* >
                        #where_clause
                    {
                        fn cmp(&self, other: &Self) -> core::cmp::Ordering {
                            use core::cmp::{Ord, Ordering::*};
                            #(#cf)*
                            Equal
                        }
                    }
                    impl < #(#ty_params),* > core::cmp::PartialEq for # name < #(#ty_params),* >
                        #where_clause
                    {
                        fn eq(&self, other: &Self) -> bool {
                            #(#ef)*
                            true
                        }
                    }
                    impl < #(#ty_params),* > core::cmp::PartialOrd for # name < #(#ty_params),* >
                         #where_clause
                    {
                        fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
                             Some(self.cmp(other))
                        }
                    }
                }
            } else {
                derives.extend(["Eq", "PartialEq", "Ord", "PartialOrd"]);
                quote! {}
            };
            quote! {
                impl < #(#ty_params),* > core::fmt::Debug for #name < #(#ty_params),* >
                    #where_clause
                {
                    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                        f.debug_struct(#name_str)
                            #(#ff)*
                            .finish()
                    }
                }

                #maybe_ord_eq_impl
            }
        };
        let ty_params = self.fields.iter().filter_map(|f| f.ty_param());
        derives.sort();
        let derives = derives
            .iter()
            .map(|s| syn::Ident::new(s, proc_macro2::Span::call_site()));
        tokens.extend(quote! {
            #[doc=#title]
            #[doc="\n\n"]
            #doc_refs
            #[derive(#(#derives),*)]
            #[yardi(#(#yardi_attrs),*)]
            pub struct #name < #(#ty_params),* > { #(#fields),* }
            #extra_impl
            #maybe_from_str
        });
    }
}

pub fn build(
    cl_reg: &crate::iana::ClassRegistry,
    ty_reg: &crate::iana::TypeRegistry,
) -> BTreeMap<(u16, Option<u16>), Rr> {
    let root =
        elementtree::Element::from_reader(crate::Asset::RdataSpecs.open()).expect("rr def root");
    let mut used_struct_names = HashSet::new();
    root.find_all("rdata")
        .map(|d| {
            let ty = d.get_attr("type").expect("type");
            let ty_info = Arc::clone(&ty_reg.by_mstr[ty]);
            let ty_num = ty_info.num;
            let class_info = d.get_attr("class").map(|s| Arc::clone(&cl_reg.by_mstr[s]));
            let sort_class_num = class_info.as_ref().map(|i| i.num);
            let mut fields: Vec<_> = d
                .find_all("field")
                .map(|e| Field::from_element(e, Arc::clone(&ty_info)))
                .collect();
            let mut used_ty_params = HashSet::new();
            let ty_param_field_name_take = 1 + (2 * (ty_info.mstr == "GPOS") as usize);
            for f in fields.iter_mut() {
                match f.data_ty {
                    DataTy::Bytes(ByteFmt {
                        rep: ByteRep::Variable { ref mut param, .. },
                        ..
                    })
                    | DataTy::NsecTypeBitmap { ref mut param }
                    | DataTy::SvcbParams { ref mut param }
                    | DataTy::Txt { ref mut param } => {
                        let ty_param = f
                            .name
                            .split_whitespace()
                            .map(|s| &s[..ty_param_field_name_take])
                            .chain(std::iter::once(" = alloc::boxed::Box<[u8]>"))
                            .collect::<String>();
                        *param = Some(syn::parse_str(&ty_param).unwrap());
                        assert!(used_ty_params.insert(ty_param));
                    }
                    _ => {}
                }
            }
            let samples = {
                let cl_m = class_info
                    .as_ref()
                    .map(|cl| cl.mstr.as_str())
                    .unwrap_or("IN");
                let ty_m = &ty_info.mstr;
                let mut v = vec![(String::new(), Vec::new())];
                for (i, f) in fields.iter().enumerate() {
                    let field_samples = f.samples(cl_m, ty_m);
                    assert!(!field_samples.is_empty());
                    if field_samples.len() == 1 {
                        for x in &mut v {
                            if i > 0 {
                                x.0.push(' ');
                            }
                            x.0.push_str(field_samples[0].0);
                            x.1.extend_from_slice(field_samples[0].1);
                        }
                        continue;
                    }
                    let mut t = Vec::with_capacity(v.len() * field_samples.len());
                    std::mem::swap(&mut t, &mut v);
                    for (s, w) in &t {
                        for field_sample in field_samples.iter() {
                            let mut new_s = s.clone();
                            if i > 0 && !field_sample.0.is_empty() {
                                new_s.push(' ');
                            }
                            new_s.push_str(field_sample.0);
                            let mut new_w = w.clone();
                            new_w.extend_from_slice(field_sample.1);
                            v.push((new_s, new_w));
                        }
                    }
                }
                if ty_info.is_data {
                    check_samples(cl_m, ty_m, &v);
                }
                v
            };
            let rr = Rr {
                cl: class_info,
                ty: ty_info,
                fields,
                samples,
            };
            let name = rr.struct_name();
            assert!(used_struct_names.insert(name));
            ((ty_num, sort_class_num), rr)
        })
        .collect()
}

fn check_samples(class_m: &str, ty_m: &str, samples: &[(String, Vec<u8>)]) {
    if class_m != "IN" {
        eprintln!("nsupdate check: skipping {}/{} due to class", class_m, ty_m);
        return;
    }
    if !nsupdate_supports(ty_m) {
        eprintln!("nsupdate check: skipping {} as its unsupported", ty_m);
        return;
    }
    for (ascii, wire) in samples {
        let mut wire_hex = String::with_capacity(wire.len() * 2);
        for b in wire {
            write!(wire_hex, "{:02x}", b).unwrap();
        }
        let test = format!(
            "zone . {class}
add . 0 {ty} {ascii}
add . 0 {ty} \\# {rdlen} {wire_hex}
show
quit
",
            class = class_m,
            ty = ty_m,
            ascii = ascii,
            rdlen = wire.len(),
            wire_hex = wire_hex
        );
        let mut cmd = Command::new("nsupdate");
        cmd.stdin(Stdio::piped()).stdout(Stdio::piped());
        let mut child = match cmd.spawn() {
            Ok(child) => child,
            Err(err) => {
                eprintln!("nsupdate check - failed to spawn: {:?}", err);
                return;
            }
        };
        {
            let stdin = child
                .stdin
                .as_mut()
                .expect("nsupdate check: failed to open stdin");
            stdin
                .write_all(test.as_bytes())
                .expect("nsupdate check - failed to write to stdin");
        }
        let output = child
            .wait_with_output()
            .expect("nsupdate check - failed to read stdout");
        let stdout = String::from_utf8_lossy(&output.stdout);
        let rr_lines: Vec<_> = stdout.lines().filter(|l| l.starts_with('.')).collect();
        let ok = rr_lines.len() == 2 && (rr_lines[0] == (rr_lines[1]));
        if !ok {
            eprintln!(
                "nsupdate check - unexpected output. supplied:\n{}returned:\n{}",
                test, stdout
            );
        }
    }
}
