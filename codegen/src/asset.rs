use std::{
    fs::{self, File},
    io::{self, BufReader, Read},
    path::{Path, PathBuf},
    time::Duration,
};

macro_rules! asset_filename {
    ($tag:ident url $url:expr) => {
        stringify!($tag)
    };
    ($tag:ident file $src:expr) => {
        $src
    };
}

macro_rules! asset_url {
    ($tag:ident url $url:expr) => {
        Some($url)
    };
    ($tag:ident file $src:expr) => {
        None
    };
}

macro_rules! assets {
    ( $($tag:ident $ty:ident $src:expr,)* ) => {
        #[derive(Debug)]
        pub enum Asset {
            $($tag,)*
        }
        impl Asset {
            pub fn url(&self) -> Option<&'static str> {
                match *self {
                    $(Asset::$tag => asset_url!($tag $ty $src),)*
                }
            }
            pub fn filename(&self) -> &'static str {
                match *self {
                    $(Asset::$tag => asset_filename!($tag $ty $src),)*
                }
            }
            pub fn all_variants() -> &'static [Self] {
                &[$(Asset::$tag,)*]
            }
        }
    }
}

assets! {
    AddressFamilyNumbers url "https://www.iana.org/assignments/address-family-numbers/address-family-numbers.xml",
    CertRrTypes url "https://www.iana.org/assignments/cert-rr-types/cert-rr-types.xml",
    DaneParameters url "https://www.iana.org/assignments/dane-parameters/dane-parameters.xml",
    DnsParameters url "https://www.iana.org/assignments/dns-parameters/dns-parameters.xml",
    DnsSecAlgNumbers url "https://www.iana.org/assignments/dns-sec-alg-numbers/dns-sec-alg-numbers.xml",
    DnsSshfpRrParameters url "https://www.iana.org/assignments/dns-sshfp-rr-parameters/dns-sshfp-rr-parameters.xml",
    DnskeyFlags url "https://www.iana.org/assignments/dnskey-flags/dnskey-flags.xml",
    DnssecNsec3Parameters url "https://www.iana.org/assignments/dnssec-nsec3-parameters/dnssec-nsec3-parameters.xml",
    DsRrTypes url "https://www.iana.org/assignments/ds-rr-types/ds-rr-types.xml",
    IpseckeyRrParameters url "https://www.iana.org/assignments/ipseckey-rr-parameters/ipseckey-rr-parameters.xml",
    PkixParameters url "https://www.iana.org/assignments/pkix-parameters/pkix-parameters.xml",
    RdataSpecs file "rr_definitions.xml",
    RfcIndex url "https://www.rfc-editor.org/in-notes/rfc-index.xml",
    TsigAlgorithmNames url "https://www.iana.org/assignments/tsig-algorithm-names/tsig-algorithm-names.xml",
    DnsServiceBindings url "https://www.iana.org/assignments/dns-svcb/dns-svcb.xml",
}

impl Asset {
    fn path(&self) -> PathBuf {
        let assets = Path::new(&super::CODEGEN_PATH).join("assets");
        if self.url().is_some() {
            assets.join("cache").join(self.filename())
        } else {
            assets.join(self.filename())
        }
    }
    pub fn open(&self) -> BufReader<File> {
        let path = self.path();
        match File::open(&path) {
            Ok(f) => BufReader::new(f),
            Err(err) => {
                panic!(
                    "failed to open asset {:?} at path {:?}: {:?}",
                    self, &path, err
                );
            }
        }
    }
}

fn compare_files(a: File, b: File) -> bool {
    let a_meta = a.metadata().expect("metadata for a");
    let b_meta = b.metadata().expect("metadata for b");
    if a_meta.len() != b_meta.len() {
        return false;
    }
    for (a, b) in BufReader::new(a).bytes().zip(BufReader::new(b).bytes()) {
        let a = a.expect("read a");
        let b = b.expect("read b");
        if a != b {
            return false;
        }
    }
    true
}

pub fn update() -> bool {
    let agent: ureq::Agent = ureq::Agent::config_builder()
        .user_agent(concat!("yardi-codegen/", env!("CARGO_PKG_VERSION")))
        .timeout_global(Some(Duration::from_secs(10)))
        .build()
        .into();
    let mut saw_err = false;
    let mut changed = false;
    let url_assets = Asset::all_variants()
        .iter()
        .filter_map(|a| a.url().map(|url| (a.path(), url)));
    for (dest_path, url) in url_assets {
        let mut resp = match agent.get(url).call() {
            Ok(resp) => resp,
            Err(err) => {
                eprintln!("Error retrieving {}: {}", url, err);
                saw_err = true;
                continue;
            }
        };
        if resp.status() != 200 {
            eprintln!("Bad status retrieving {}: {}", url, resp.status());
            saw_err = true;
            continue;
        };
        let temp_path = dest_path.with_extension("tmp");
        let download_result = {
            let mut temp = File::create(&temp_path).expect("create temp file");
            let r = io::copy(&mut resp.body_mut().as_reader(), &mut temp);
            temp.sync_all().expect("sync temp file");
            r
        };
        if let Err(err) = download_result {
            fs::remove_file(&temp_path).expect("remove temp file");
            eprintln!("Error retrieving {}: {}", url, err);
            saw_err = true;
            continue;
        }
        let same = if let Ok(existing) = File::open(&dest_path) {
            let new = File::open(&temp_path).expect("reopen temp file");
            compare_files(existing, new)
        } else {
            false
        };
        if same {
            fs::remove_file(&temp_path).expect("remove temp file");
        } else {
            changed = true;
            eprintln!("Updated {}", url);
            fs::rename(&temp_path, &dest_path).expect("move new file");
        }
    }
    assert!(
        !saw_err,
        "one or more errors occurred while updating assets"
    );
    changed
}
