use proc_macro2::{Span, TokenStream};
use syn::{self, Ident};

use crate::iana::DnskeyFlagRegistry;
use crate::template::{AssocPathConstants, FlagTy, GenericFlags};

pub fn add_extra(
    extra: &mut Vec<TokenStream>,
    mod_name: &str,
    ident: &Ident,
    flag_reg: &DnskeyFlagRegistry,
) {
    let tag_doc = format!("Returns the key tag for this `{}`.", ident);
    extra.push(quote! {
        impl #ident {
            #[doc=#tag_doc]
            pub fn tag(&self) -> u16 {
                crate::dnssec::key_tag(self.flags.0, self.protocol.0, self.alg, &self.public_key)
            }
        }
    });
    match mod_name {
        "dnskey" => {
            extra.push(generate_dnskeyflags(flag_reg));
            extra.push(generate_protocol("DNSKEY", ident));
        }
        "key" => {
            extra.push(generate_keyflags());
            extra.push(generate_protocol("KEY", ident));
        }
        _ => (),
    }
}

fn generate_dnskeyflags(r: &DnskeyFlagRegistry) -> TokenStream {
    // TODO: Use "SEP" for Debug fmt too?
    fn desc_to_id(s: &str) -> &str {
        if s == "Secure Entry Point (SEP)" {
            return "SEP";
        }
        assert!(!s.contains('('));
        s
    }
    let flag_ty = FlagTy::new("DnskeyFlags", "DNSKEY Flags.", "u16");
    let mut assoc_constants = AssocPathConstants::new("DnskeyFlags");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in r.0.iter() {
        let title = info.description.clone();
        let const_id = Ident::new(desc_to_id(&title), Span::call_site());
        let debug_str = &info.description;
        mnemonics.push(quote! { (DnskeyFlags::#const_id, #debug_str) });
        let value = syn::parse_str(format!("DnskeyFlags::FLAG_{}", info.value).as_str())
            .expect("dnskey flag path");
        assoc_constants.insert(info.value, const_id, title, value, &info.refs[..]);
    }
    let generic_flags = GenericFlags::new("DnskeyFlags", "Flag", 16);
    quote! {
        #flag_ty

        #assoc_constants

        #generic_flags

        impl DnskeyFlags {
            const MNEMONICS: &'static [(DnskeyFlags, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}

fn generate_keyflags() -> TokenStream {
    // "Limiting the Scope of the KEY Resource Record (RR)"
    // https://tools.ietf.org/html/rfc3445#section-3
    let flag_ty = FlagTy::new("KeyFlags", "KEY Flags.", "u16");
    let generic_flags = GenericFlags::new("KeyFlags", "Flag", 16);
    quote! {
        #flag_ty

        impl KeyFlags {
           /// KEY is a DNS Zone Key.
           pub const ZONE: KeyFlags = KeyFlags::FLAG_7;
           const MNEMONICS: &'static [(KeyFlags, &'static str)] = &[(KeyFlags::ZONE, "ZONE")];
        }

        #generic_flags
    }
}

// TODO: ZST to represent DNSKEY Protocol always being 3. Keep KEY flexible though.
fn generate_protocol(mnemonic: &str, prefix: &Ident) -> TokenStream {
    let doc_str = format!("{} Protocol.", mnemonic);
    let id: Ident = syn::parse_str(format!("{}Protocol", prefix).as_str()).unwrap();
    quote! {
        #[doc = #doc_str]
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd,
                 Pack, Unpack, UnpackedLen, WireOrd,
                 Parse, Present)]
        #[yardi(crate="crate")]
        pub struct #id(pub u8);

        impl #id {
            /// DNSSEC
            pub const DNSSEC: #id = #id(3);
        }

        impl core::fmt::Debug for #id {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                f.write_str(stringify!(#id))?;
                if *self == #id::DNSSEC {
                    f.write_str("(DNSSEC)")
                } else {
                    write!(f, "({})", self.0)
                }
            }
        }

        impl From<u8> for #id {
            fn from(num: u8) -> Self {
                #id(num)
            }
        }

        impl From<#id> for u8 {
            fn from(#id(num): #id) -> Self {
                num
            }
        }
    }
}
