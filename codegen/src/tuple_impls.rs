use proc_macro2::{Span, TokenStream};
use quote::TokenStreamExt;
use syn::{self, Ident, LitInt};

const TUPLE_FIELDS: u64 = 8;

pub fn generate() -> TokenStream {
    let mut field_ty = Vec::with_capacity(TUPLE_FIELDS as usize);
    let mut field_n = Vec::with_capacity(TUPLE_FIELDS as usize);
    let mut out = quote! {
        use crate::ascii::parse::{Parser, Parse, ParseError};
        use crate::ascii::present::{Presenter, Present, PresentError};
        use crate::wire::{Reader, Writer, Pack, PackError, Unpack, UnpackedLen, UnpackError};
    };
    for i in 0..TUPLE_FIELDS {
        let new_ty = Ident::new(
            std::str::from_utf8(&[b'A' + i as u8]).unwrap(),
            Span::call_site(),
        );
        let new_n: LitInt = LitInt::new(i.to_string().as_str(), Span::call_site());
        field_ty.push(new_ty);
        field_n.push(new_n);
        if i == 0 {
            continue;
        }
        let pack = generate_pack(&field_ty, &field_n);
        out.append_all(pack);
        let unpack = generate_unpack(&field_ty, &field_n);
        out.append_all(unpack);
        let unpacked_len = generate_unpacked_len(&field_ty, &field_n);
        out.append_all(unpacked_len);
        let parse = generate_parse(&field_ty, &field_n);
        out.append_all(parse);
        let present = generate_present(&field_ty, &field_n);
        out.append_all(present);
    }
    out
}

fn generate_pack<T>(f_ty: &[Ident], f_n: &[T]) -> TokenStream
where
    T: quote::ToTokens,
{
    assert_eq!(f_ty.len(), f_n.len());
    quote! {
        impl<#(#f_ty),*> Pack for (#(#f_ty),*)
        where
            #(#f_ty: Pack,)*
        {
            fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
                #(self.#f_n.pack(writer)?;)*
                Ok(())
            }
            fn packed_len(&self) -> Result<usize, PackError> {
                #[allow(clippy::identity_op)]
                Ok(0 #(+ self.#f_n.packed_len()?)*)
            }
        }
    }
}

fn generate_unpack<T>(f_ty: &[Ident], f_n: &[T]) -> TokenStream
where
    T: quote::ToTokens,
{
    assert_eq!(f_ty.len(), f_n.len());
    let unpack_id = syn::parse_str::<syn::Path>("Unpack::unpack").unwrap();
    let unpack = std::iter::repeat(&unpack_id).take(f_ty.len());
    quote! {
        impl<'a, #(#f_ty),*> Unpack<'a> for (#(#f_ty),*)
        where
            #(#f_ty: Unpack<'a>,)*
        {
            fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
                Ok((
                    #(#unpack(reader)?,)*
                ))
            }
        }
    }
}

fn generate_unpacked_len<T>(f_ty: &[Ident], f_n: &[T]) -> TokenStream
where
    T: quote::ToTokens,
{
    assert_eq!(f_ty.len(), f_n.len());
    let unpacked_len = f_ty.iter().map(|id| quote! { #id::unpacked_len });
    quote! {
        impl<#(#f_ty),*> UnpackedLen for (#(#f_ty),*)
        where
            #(#f_ty: UnpackedLen,)*
        {
            fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
                #[allow(clippy::identity_op)]
                Ok(0 #(+ #unpacked_len(reader)?)*)
            }
        }
    }
}

fn generate_parse<T>(f_ty: &[Ident], f_n: &[T]) -> TokenStream
where
    T: quote::ToTokens,
{
    assert_eq!(f_ty.len(), f_n.len());
    let parse_id = syn::parse_str::<syn::Path>("Parse::parse").unwrap();
    let parse = std::iter::repeat(&parse_id).take(f_ty.len());
    quote! {
        impl<#(#f_ty),*> Parse for (#(#f_ty),*)
        where
            #(#f_ty: Parse,)*
        {
            fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
                Ok((
                    #(#parse(p)?,)*
                ))
            }
        }
    }
}

fn generate_present<T>(f_ty: &[Ident], f_n: &[T]) -> TokenStream
where
    T: quote::ToTokens,
{
    assert_eq!(f_ty.len(), f_n.len());
    quote! {
        impl<#(#f_ty),*> Present for (#(#f_ty),*)
        where
            #(#f_ty: Present,)*
        {
            fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
                #(self.#f_n.present(p)?;)*
                Ok(())
            }
        }
    }
}
