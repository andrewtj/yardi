use proc_macro2::TokenStream;

use super::const_name;
use crate::iana::{self, CertTypeRegistry, DnssecAlgRegistry};
use crate::template::{AssocNumConstants, ToFromMnemonic};

pub fn add_extra(
    extra: &mut Vec<TokenStream>,
    certtyy_reg: &CertTypeRegistry,
    dnssecalg_reg: &DnssecAlgRegistry,
) {
    extra.push(generate_alg(dnssecalg_reg));
    extra.push(generate_certty(certtyy_reg));
}

pub fn generate_alg(r: &DnssecAlgRegistry) -> TokenStream {
    let unknown_refs: &[_] = &[iana::rf::Rf::Rfc {
        doc: "RFC 4398".into(),
        title: "Storing Certificates in the Domain Name System (DNS)".into(),
        errata: Vec::new(),
    }];
    let mut assoc_constants = AssocNumConstants::new("CertAlg");
    let mut mnemonic_table = ToFromMnemonic::new("CertAlg");
    mnemonic_table.no_to_str();
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in &r.0 {
        let (title, refs) = if info.num == 0 {
            ("UNKNOWN".into(), unknown_refs)
        } else {
            (info.mstr.to_string(), &info.refs[..])
        };
        let id = const_name(&title);
        let str_id = const_name(format!("{} STR", title));
        mnemonics.push(quote! { (CertAlg::#id, #str_id) });
        mnemonic_table.insert(id.clone(), title.clone(), info.num);
        assoc_constants.insert(id, title, info.num, refs);
    }
    quote! {
        /// CERT Algorithm.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Present)]
        #[yardi(crate="crate")]
        pub struct CertAlg(pub u8);

        impl crate::ascii::parse::Parse for CertAlg {
            fn parse(p: &mut crate::ascii::parse::Parser<'_, '_>) -> Result<Self, crate::ascii::parse::ParseError> {
                p.pull(core::str::FromStr::from_str)
            }
        }

        impl core::fmt::Display for CertAlg {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                core::fmt::Display::fmt(&self.0, f)
            }
        }

        impl core::fmt::Debug for CertAlg {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for (a, s) in Self::MNEMONICS {
                    if a == self {
                        return write!(f, "CertAlg({})", s);
                    }
                }
                write!(f, "CertAlg({})", self.0)
            }
        }

        impl From<u8> for CertAlg {
            fn from(alg: u8) -> Self {
                CertAlg(alg)
            }
        }

        impl core::str::FromStr for CertAlg {
            type Err = crate::ascii::parse::ParseError;
            fn from_str(s: &str) -> Result<Self, Self::Err> {
                match from_str(s) {
                    Some(v) => Ok(v),
                    None => crate::datatypes::int::parse_u8(s).map(CertAlg),
                }
            }
        }

        #assoc_constants

        #mnemonic_table

        impl CertAlg {
            const MNEMONICS: &'static [(CertAlg, &'static str)] = &[
                #(#mnemonics),*
            ];
        }

    }
}

pub fn generate_certty(r: &CertTypeRegistry) -> TokenStream {
    let mut assoc_consts = AssocNumConstants::new("CertTy");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in r.0.iter() {
        assert!(!info.ty.is_empty());
        assert!(!info.description.is_empty());
        let title = format!("{} - {}", &info.ty, &info.description);
        let const_id = const_name(&info.ty);
        let debug_str = &info.ty;
        mnemonics.push(quote! { (CertTy::#const_id, #debug_str) });
        assoc_consts.insert(const_id, title, info.num, &info.refs[..]);
    }
    quote! {
        /// CERT Type.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Present)]
        #[yardi(crate="crate")]
        pub struct CertTy(pub u16);

        impl crate::ascii::parse::Parse for CertTy {
            fn parse(entry: &mut crate::ascii::parse::Parser<'_, '_>) -> Result<Self, crate::ascii::parse::ParseError> {
                entry.pull(core::str::FromStr::from_str)
            }
        }

        impl core::fmt::Display for CertTy {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                self.0.fmt(f)
            }
        }

        impl core::fmt::Debug for CertTy {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(t, m) in Self::MNEMONICS.iter() {
                    if t == *self {
                        return write!(f, "CertTy({})", m);
                    }
                }
                write!(f, "CertTy({})", self.0)
            }
        }

        impl From<u16> for CertTy {
            fn from(alg: u16) -> Self {
                CertTy(alg)
            }
        }

        impl core::str::FromStr for CertTy {
            type Err = crate::ascii::parse::ParseError;
            fn from_str(s: &str) -> Result<Self, Self::Err> {
                for &(value, subject) in Self::MNEMONICS.iter() {
                    if subject.eq_ignore_ascii_case(s) {
                        return Ok(value);
                    }
                }
                crate::datatypes::int::parse_u16(s).map(CertTy)
            }
        }

        #assoc_consts

        impl CertTy {
            const MNEMONICS: &'static [(CertTy, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
