use proc_macro2::TokenStream;

use super::const_name;
use crate::iana::{SshFpTypeRegistry, SshPubKeyAlgRegistry};
use crate::template::AssocNumConstants;

pub fn add_extra(
    extra: &mut Vec<TokenStream>,
    pkalg_reg: &SshPubKeyAlgRegistry,
    fpty_reg: &SshFpTypeRegistry,
) {
    extra.push(generate_pkgalg(pkalg_reg));
    extra.push(generate_fpty(fpty_reg));
}

pub fn generate_pkgalg(keyalgreg: &SshPubKeyAlgRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("SshfpPubKeyAlg");
    let mut mnemonics = Vec::with_capacity(keyalgreg.0.len());
    for info in keyalgreg.0.iter() {
        let title = info.description.clone();
        let id = const_name(&title);
        mnemonics.push(quote! { (SshfpPubKeyAlg::#id, #title) });
        assoc_constants.insert(id, title, info.value, &info.refs[..]);
    }
    quote! {
        /// SSHFP Public Key Algorithm.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct SshfpPubKeyAlg(pub u8);

        impl core::fmt::Debug for SshfpPubKeyAlg {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(pka, m) in Self::MNEMONICS {
                    if pka == *self {
                        return write!(f, "SshfpPubKeyAlg({})", m);
                    }
                }
                write!(f, "SshfpPubKeyAlg({})", self.0)
            }
        }

        #assoc_constants

        impl SshfpPubKeyAlg {
            const MNEMONICS: &'static [(SshfpPubKeyAlg, &'static str)] = &[
                #(#mnemonics),*
            ];
        }

    }
}

pub fn generate_fpty(fptyreg: &SshFpTypeRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("SshfpFpTy");
    let mut mnemonics = Vec::with_capacity(fptyreg.0.len());
    for info in fptyreg.0.iter() {
        let title = info.description.clone();
        let id = const_name(&title);
        mnemonics.push(quote! { (SshfpFpTy::#id, #title) });
        assoc_constants.insert(id, title, info.value, &info.refs[..]);
    }
    quote! {
        /// SSHFP Fingerprint Type.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct SshfpFpTy(pub u8);

        impl core::fmt::Debug for SshfpFpTy {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(fpty, m) in Self::MNEMONICS {
                    if fpty == *self {
                        return write!(f, "SshfpFpTy({})", m);
                    }
                }
                write!(f, "SshfpFpTy({})", self.0)
            }
        }

        #assoc_constants

        impl SshfpFpTy {
            const MNEMONICS: &'static [(SshfpFpTy, &'static str)] = &[
                #(#mnemonics),*
            ];
        }

    }
}
