use proc_macro2::TokenStream;
use syn::Ident;

pub fn add_extra(extra: &mut Vec<TokenStream>, ident: &Ident) {
    extra.push(quote! {
        use crate::datatypes::{Ty, Ttl, Time, Name};
        use crate::dnssec::SigAlg;
        use crate::wire::{PackError, Writer, Pack};

        type SiglessSig<'a> = (
            &'a Ty,
            &'a SigAlg,
            &'a u8,
            &'a Ttl,
            &'a Time,
            &'a Time,
            &'a u16,
            &'a Name,
        );

        impl<S> #ident<S> {
            fn data_no_sig(&self) -> SiglessSig<'_> {
                (
                    &self.ty_covered,
                    &self.alg,
                    &self.labels,
                    &self.original_ttl,
                    &self.expire,
                    &self.incept,
                    &self.key_tag,
                    &self.signers_name,
                )
            }
            /// Packed length with signature excluded.
            pub fn packed_len_no_sig(&self) -> Result<usize, PackError> {
                self.data_no_sig().packed_len()
            }
            /// Pack with signature excluded.
            pub fn pack_no_sig(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
                self.data_no_sig().pack(writer)
            }
        }
    });
}
