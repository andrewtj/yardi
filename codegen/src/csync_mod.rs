use proc_macro2::TokenStream;
use syn;

use crate::const_name;
use crate::iana::CsyncFlagRegistry;
use crate::template::{AssocPathConstants, FlagTy, GenericFlags};

pub fn add_extra(extra: &mut Vec<TokenStream>, csyncflag_reg: &CsyncFlagRegistry) {
    extra.push(build_flags(csyncflag_reg));
}

pub fn build_flags(r: &CsyncFlagRegistry) -> TokenStream {
    let flag_ty = FlagTy::new("CsyncFlags", "CSYNC Flags.", "u16");
    let mut assoc_constants = AssocPathConstants::new("CsyncFlags");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in r.0.iter() {
        let title = format!("{} - {}", info.name, info.description);
        let const_id = const_name(&info.name);
        let debug_str = &info.name;
        mnemonics.push(quote! { (CsyncFlags::#const_id, #debug_str) });
        let value = syn::parse_str(format!("CsyncFlags::FLAG_{}", info.value).as_str())
            .expect("csync flag path");
        assoc_constants.insert(info.value, const_id, title, value, &info.refs[..]);
    }
    let generic_flags = GenericFlags::new("CsyncFlags", "Flag", 16);
    quote! {
        #flag_ty

        #assoc_constants

        #generic_flags

        impl CsyncFlags {
            const MNEMONICS: &'static [(CsyncFlags, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
