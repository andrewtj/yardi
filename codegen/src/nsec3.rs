use proc_macro2::TokenStream;
use syn;

use crate::iana::{Nsec3FlagRegistry, Nsec3HashAlgRegistry, Nsec3paramFlagRegistry};
use crate::template::{AssocNumConstants, AssocPathConstants, FlagTy, GenericFlags};
use crate::{const_name, iana};

pub fn add_extra(
    extra: &mut Vec<TokenStream>,
    nsec3hashalgs: &Nsec3HashAlgRegistry,
    nsec3flags: &Nsec3FlagRegistry,
) {
    extra.push(generate_hash_algs(nsec3hashalgs));
    extra.push(generate_flags(nsec3flags));
}

pub fn add_extra_param(extra: &mut Vec<TokenStream>, nsec3paramflags: &Nsec3paramFlagRegistry) {
    extra.push(generate_param_flags(nsec3paramflags));
}

pub fn generate_flags(flagsreg: &iana::Nsec3FlagRegistry) -> TokenStream {
    let flag_ty = FlagTy::new("Nsec3Flags", "NSEC3 Flags.", "u8");
    let mut assoc_constants = AssocPathConstants::new("Nsec3Flags");
    let mut mnemonics = Vec::with_capacity(flagsreg.0.len());
    for info in &flagsreg.0 {
        let title = info.description.clone();
        let const_id = const_name(&title);
        let debug_str = &info.description;
        mnemonics.push(quote! { (Nsec3Flags::#const_id, #debug_str) });
        let value = syn::parse_str(format!("Nsec3Flags::FLAG_{}", info.value).as_str())
            .expect("dnskey flag path");
        assoc_constants.insert(info.value, const_id, title, value, &info.refs[..]);
    }
    let generic_flags = GenericFlags::new("Nsec3Flags", "Flag", 8);
    quote! {
        #flag_ty

        #assoc_constants

        #generic_flags

        impl Nsec3Flags {
            const MNEMONICS: &'static [(Nsec3Flags, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}

pub fn generate_param_flags(pflagsreg: &iana::Nsec3paramFlagRegistry) -> TokenStream {
    let flag_ty = FlagTy::new("Nsec3paramFlags", "NSEC3PARAM Flags", "u8");
    let mut assoc_constants = AssocPathConstants::new("Nsec3paramFlags");
    let mut mnemonics = Vec::with_capacity(pflagsreg.0.len());
    for info in &pflagsreg.0 {
        let title = info.description.clone();
        let const_id = const_name(&title);
        let debug_str = &info.description;
        mnemonics.push(quote! { (Nsec3paramFlags::#const_id, #debug_str) });
        let value = syn::parse_str(format!("Nsec3paramFlags::FLAG_{}", info.value).as_str())
            .expect("dnskey flag path");
        assoc_constants.insert(info.value, const_id, title, value, &info.refs[..]);
    }
    let generic_flags = GenericFlags::new("Nsec3paramFlags", "Flag", 8);
    quote! {
        #flag_ty

        #assoc_constants

        #generic_flags

        impl Nsec3paramFlags {
            const MNEMONICS: &'static [(Nsec3paramFlags, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}

pub fn generate_hash_algs(hashalgsreg: &iana::Nsec3HashAlgRegistry) -> TokenStream {
    let mut assoc_constants = AssocNumConstants::new("Nsec3HashAlg");
    let mut mnemonics = Vec::with_capacity(hashalgsreg.0.len());
    for info in &hashalgsreg.0 {
        let title = &info.description;
        let id = const_name(title);
        mnemonics.push(quote! { (Nsec3HashAlg::#id, #title) });
        assoc_constants.insert(id, title.clone(), info.value, &info.refs[..]);
    }
    quote! {
        /// NSEC3 Hash Algorithm.
        #[derive(Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack,
                UnpackedLen, WireOrd, Parse, Present)]
        #[yardi(crate="crate")]
        pub struct Nsec3HashAlg(pub u8);

        impl core::fmt::Debug for Nsec3HashAlg {
            fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
                for &(h, m) in Self::MNEMONICS {
                    if h == *self {
                        return write!(f, "Nsec3HashAlg({})", m);
                    }
                }
                write!(f, "Nsec3HashAlg({})", self.0)
            }
        }

        #assoc_constants

        impl Nsec3HashAlg {
            const MNEMONICS: &'static [(Nsec3HashAlg, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
