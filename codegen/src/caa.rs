use proc_macro2::TokenStream;
use syn;

use super::const_name;
use crate::iana::CaaFlagRegistry;
use crate::template::{AssocPathConstants, FlagTy, GenericFlags};

pub fn add_extra(extra: &mut Vec<TokenStream>, reg: &CaaFlagRegistry) {
    extra.push(generate_flag_constants(reg));
}

pub fn generate_flag_constants(r: &CaaFlagRegistry) -> TokenStream {
    let flag_ty = FlagTy::new("CaaFlags", "CAA Flags.", "u8");
    let mut assoc_constants = AssocPathConstants::new("CaaFlags");
    let mut mnemonics = Vec::with_capacity(r.0.len());
    for info in &r.0 {
        let title = info.description.clone();
        let const_id = const_name(&title);
        let debug_str = &info.description;
        mnemonics.push(quote! { (CaaFlags::#const_id, #debug_str) });
        let value = syn::parse_str(format!("CaaFlags::FLAG_{}", info.value).as_str())
            .expect("caa flag path");
        assoc_constants.insert(info.value, const_id, title, value, &info.refs[..]);
    }
    let generic_flags = GenericFlags::new("CaaFlags", "Flag", 8);
    quote! {
        #flag_ty

        #assoc_constants

        #generic_flags

        impl CaaFlags {
            const MNEMONICS: &'static [(CaaFlags, &'static str)] = &[
                #(#mnemonics),*
            ];
        }
    }
}
