use std::collections::HashMap;
use std::fmt::{self, Write};

use elementtree::Element;
use proc_macro2::TokenStream;
use quote::ToTokens;

use super::ASSIGNMENTS_NS;

#[derive(Clone, Debug, Eq, PartialEq, PartialOrd, Ord)]
pub enum Rf {
    Text(String),
    Rfc {
        doc: String,
        title: String,
        errata: Vec<String>,
    },
    Uri {
        title: Option<String>,
        uri: String,
    },
    Person(Person),
}

impl fmt::Display for Rf {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fn escape(s: &str) -> String {
            let mut out = String::with_capacity(s.len());
            for &c in s.as_bytes() {
                match c {
                    b'a'..=b'z'
                    | b'A'..=b'Z'
                    | b'0'..=b'9'
                    | b' '
                    | b'('
                    | b')'
                    | b','
                    | b'-'
                    | b'.'
                    | b'/'
                    | b':'
                    | b'=' => out.push(c as char),
                    b'&' => out.push_str("&amp;"),
                    b'"' => out.push_str("&quot;"),
                    b'\'' => out.push_str("&apos;"),
                    _ => panic!("didn't handle {}", c as char),
                }
            }
            out
        }
        let s = match *self {
            Rf::Person(Person {
                ref name, ref uri, ..
            }) => {
                format!(r#"<a href="{}">{}</a>"#, uri, escape(name))
            }
            Rf::Rfc {
                ref doc,
                ref title,
                ref errata,
            } => {
                let doc_fragment = if doc.starts_with("RFC") {
                    let mut t = doc.to_lowercase();
                    if t.as_bytes()[3] == b' ' {
                        t.remove(3);
                    }
                    let ends_with_digits = t[3..].as_bytes().iter().all(|&b| b.is_ascii_digit());
                    assert!(ends_with_digits, "unexpected title: {:?}", t);
                    t
                } else {
                    doc.clone()
                };
                let link_name = if let Some(stripped) = doc.strip_prefix("RFC") {
                    format!("RFC {}: {}", stripped, title)
                } else {
                    title.clone()
                };
                let url = format!("https://tools.ietf.org/html/{}", doc_fragment);
                let mut fragment = format!(r#"<a href="{}">{}</a>"#, url, escape(&link_name));
                if !errata.is_empty() {
                    assert!(doc.as_bytes()[3..].iter().all(|b| b.is_ascii_digit()));
                }
                for (i, e) in errata.iter().enumerate() {
                    fragment.push_str(match i {
                        0 => " (Errata: ",
                        i if i == errata.len() - 1 => " and ",
                        _ => ", ",
                    });
                    write!(
                        fragment,
                        "<a href=\"https://www.rfc-editor.org/errata_search.php?rfc={rfc}&eid={eid}\">{eid}</a>",
                        rfc = &doc[3..],
                        eid = e
                    ).expect("errata fragment");
                }
                if !errata.is_empty() {
                    fragment.push(')');
                }
                fragment
            }
            Rf::Text(ref text) => escape(text),
            Rf::Uri { ref title, ref uri } => {
                if let Some(ref title) = title {
                    format!(r#"<a href="{}">{}</a>"#, uri, title)
                } else {
                    format!("<{}>", uri)
                }
            }
        };
        f.write_str(&s)
    }
}

impl ToTokens for Rf {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let s = self.to_string();
        tokens.extend(quote! { #s })
    }
}

#[derive(Clone, Debug)]
pub struct People(HashMap<String, Person>);

impl People {
    pub fn new(registry: &Element) -> Self {
        let people = registry
            .find((ASSIGNMENTS_NS, "people"))
            .expect("people")
            .find_all((ASSIGNMENTS_NS, "person"))
            .map(|e| {
                let id = e.get_attr("id").expect("id").to_string();
                let name = e
                    .find((ASSIGNMENTS_NS, "name"))
                    .expect("name")
                    .text()
                    .to_string();
                let uri = e
                    .find((ASSIGNMENTS_NS, "uri"))
                    .expect("uri")
                    .text()
                    .to_string();
                let updated = e
                    .find((ASSIGNMENTS_NS, "updated"))
                    .expect("updated")
                    .text()
                    .to_string();
                (id, Person { name, uri, updated })
            })
            .collect();
        People(people)
    }
}

impl std::ops::Deref for People {
    type Target = HashMap<String, Person>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

#[derive(Clone, Debug, Eq, PartialEq, Ord, PartialOrd)]
pub struct Person {
    pub name: String,
    pub uri: String,
    pub updated: String,
}
