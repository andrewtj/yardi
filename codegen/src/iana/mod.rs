pub mod rf;

use std::collections::{BTreeMap, HashMap};
use std::sync::Arc;

use elementtree::Element;

use self::rf::{People, Rf};

const ASSIGNMENTS_NS: &str = "http://www.iana.org/assignments";

type RfcIndex = HashMap<String, String>;

#[derive(Clone, Debug)]
pub struct ClassInfo {
    pub is_q_or_meta: bool,
    pub num: u16,
    pub mstr: String,
    pub desc: String,
    pub refs: Vec<Rf>,
}

#[derive(Clone, Debug)]
pub struct ClassRegistry {
    pub by_num: BTreeMap<u16, Arc<ClassInfo>>,
    pub by_mstr: BTreeMap<String, Arc<ClassInfo>>,
}

impl ClassRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut by_num = BTreeMap::new();
        let mut by_mstr = BTreeMap::new();
        for r in registry.find_all((ASSIGNMENTS_NS, "record")) {
            let desc = match r
                .find((ASSIGNMENTS_NS, "description"))
                .expect("description")
                .text()
            {
                "Reserved" | "Unassigned" | "Reserved for Private Use" => continue,
                other => other.to_string(),
            };
            let value_str = r.find((ASSIGNMENTS_NS, "value")).expect("value").text();
            if value_str.contains('-') {
                panic!("did not expect Class range with description: {}", desc);
            }
            let num: u16 = value_str.parse().expect("u16 value");
            let mstr = desc
                .split_whitespace()
                .last()
                .expect("mnemonic")
                .trim_matches(|c| c == '(' || c == ')')
                .to_string();
            let refs = extract_references(r, rfc_index, people, None);
            let is_q_or_meta = match num {
                128..=253 | 254 /* NONE */ | 255 /* ANY */ | 57344..=65279 => true,
                _ => false,
            };
            let info = Arc::new(ClassInfo {
                is_q_or_meta,
                num,
                mstr,
                desc,
                refs,
            });
            assert!(by_num.insert(num, Arc::clone(&info)).is_none());
            assert!(by_mstr.insert(info.mstr.clone(), info).is_none());
        }
        ClassRegistry { by_num, by_mstr }
    }
}

#[derive(Clone, Debug)]
pub struct TypeInfo {
    pub is_data: bool,
    pub num: u16,
    pub mstr: String,
    pub desc: Option<String>,
    pub refs: Vec<Rf>,
}

#[derive(Clone, Debug)]
pub struct TypeRegistry {
    pub by_num: BTreeMap<u16, Arc<TypeInfo>>,
    pub by_mstr: BTreeMap<String, Arc<TypeInfo>>,
}

impl TypeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut by_num = BTreeMap::new();
        let mut by_mstr = BTreeMap::new();
        for r in registry.find_all((ASSIGNMENTS_NS, "record")) {
            let mstr = match r.find((ASSIGNMENTS_NS, "type")).expect("type").text() {
                "Unassigned" | "Private use" | "Reserved" => continue,
                "*" => "ANY".to_string(),
                other => other.to_string(),
            };
            // Reference: https://tools.ietf.org/html/draft-lewis-dns-undocumented-types
            let extra_refs = match mstr.as_str() {
                "EID" | "NIMLOC" => {
                    let rfc_docs = ["RFC1753", "RFC1992", "RFC2102", "RFC2103"];
                    let mut v = Vec::with_capacity(2 + rfc_docs.len());
                    v.extend_from_slice(&[
                        Rf::Uri {
                            title: Some("draft-ietf-nimrod-dns-00".into()),
                            uri: "https://tools.ietf.org/html/draft-ietf-nimrod-dns-00".into(),
                        },
                        Rf::Uri{
                            title: Some("Endpoints and Endpoint Names: A Proposed Enhancement to the Internet Architecture".into()),
                            uri: "http://mercury.lcs.mit.edu/~jnc/tech/endpoints.txt".into()
                        },
                    ]);
                    v.extend(rfc_docs.iter().map(|doc| {
                        let doc = doc.to_string();
                        let title = rfc_index[&doc].clone();
                        Rf::Rfc {
                            doc,
                            title,
                            errata: Vec::new(),
                        }
                    }));
                    v
                }
                "KEY" => {
                    let doc = "RFC3445".into();
                    let title = rfc_index[&doc].clone();
                    vec![Rf::Rfc {
                        doc,
                        title,
                        errata: Vec::new(),
                    }]
                }
                "NINFO" => vec![
                    Rf::Text("Originally proposed as the ZS RR".into()),
                    Rf::Uri {
                        title: Some("draft-reid-dnsext-zs-01".into()),
                        uri: "https://tools.ietf.org/html/draft-reid-dnsext-zs-01".into(),
                    },
                ],
                "RKEY" => vec![Rf::Uri {
                    title: Some("draft-reid-dnsext-rkey-00".into()),
                    uri: "https://tools.ietf.org/html/draft-reid-dnsext-rkey-00".into(),
                }],
                "TALINK" => vec![Rf::Uri {
                    title: Some("draft-wijngaards-dnsop-trust-history".into()),
                    uri: "https://tools.ietf.org/html/draft-wijngaards-dnsop-trust-history".into(),
                }],
                _ => vec![],
            };
            let refs = extract_references(r, rfc_index, people, extra_refs);
            let desc =
                r.find((ASSIGNMENTS_NS, "description"))
                    .and_then(|e| match e.text().trim() {
                        "" => None,
                        desc if desc == mstr => None,
                        desc if desc.starts_with(&mstr) => {
                            let desc = desc[mstr.len()..].trim();
                            if desc.is_empty() {
                                panic!("reached");
                            } else {
                                Some(desc.to_string())
                            }
                        }
                        desc => Some(desc.to_string()),
                    });
            let num: u16 = r
                .find((ASSIGNMENTS_NS, "value"))
                .expect("value")
                .text()
                .parse()
                .expect("value");
            let is_data = match num {
                41 /* OPT */ | 128..=255 => false,
                _ => true,
            };
            let info = Arc::new(TypeInfo {
                is_data,
                num,
                mstr,
                desc,
                refs,
            });
            assert!(by_num.insert(num, Arc::clone(&info)).is_none());
            assert!(by_mstr.insert(info.mstr.clone(), info).is_none());
        }
        TypeRegistry { by_num, by_mstr }
    }
}

#[derive(Clone, Debug)]
pub struct DnssecAlgInfo {
    pub num: u8,
    pub mstr: String,
    pub desc: String,
    pub refs: Vec<Rf>,
}

#[derive(Clone, Debug)]
pub struct DnssecAlgRegistry(pub Vec<DnssecAlgInfo>);

impl DnssecAlgRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let desc = match r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("description")
                    .text()
                {
                    "Reserved" | "Unassigned" => return None,
                    other => other.replace(", see 5", ""), // for RSA/MD5
                };
                let num: u8 = r
                    .find((ASSIGNMENTS_NS, "number"))
                    .expect("DNSSEC alg number")
                    .text()
                    .parse()
                    .expect("DNSSEC alg number");
                let mstr = r
                    .find((ASSIGNMENTS_NS, "mnemonic"))
                    .expect("mnemonic")
                    .text()
                    .to_string();
                let refs = extract_references(r, rfc_index, people, None);
                Some(DnssecAlgInfo {
                    num,
                    mstr,
                    desc,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.num);
        DnssecAlgRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct OpcodeInfo {
    pub num: u8,
    pub mstr: String,
    pub desc: Option<String>,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct OpcodeRegistry(pub Vec<OpcodeInfo>);

impl OpcodeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let desc_raw = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .map(|e| e.text())
                    .expect("description");
                let (mstr, desc) = match desc_raw {
                    "Unassigned" => return None,
                    "IQuery  (Inverse Query, OBSOLETE)" => {
                        ("IQuery".into(), Some("Inverse Query (Obsolete)".into()))
                    }
                    "DNS Stateful Operations (DSO)" => {
                        ("DSO".into(), Some("DNS Stateful Operations".into()))
                    }
                    other => {
                        let all_letters = other.as_bytes().iter().all(|&b| b.is_ascii_alphabetic());
                        assert!(all_letters, "opcode desc isn't all letters: {:?}", other);
                        (other.to_string(), None)
                    }
                };
                let num: u8 = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("opcode value")
                    .text()
                    .parse()
                    .expect("opcode value");
                let refs = extract_references(r, rfc_index, people, None);
                Some(OpcodeInfo {
                    num,
                    mstr,
                    desc,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.num);
        OpcodeRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct RcodeInfo {
    pub num: u16,
    pub mstr: String,
    pub desc: String,
    pub refs: Vec<Rf>,
}

impl RcodeInfo {
    pub fn tsig(&self) -> bool {
        self.refs.iter().any(|r| match *r {
            Rf::Rfc { ref doc, .. } => matches!(doc.as_str(), "RFC2845" | "RFC2930" | "RFC8945"),
            _ => false,
        })
    }
}

#[derive(Debug, Clone)]
pub struct RcodeRegistry {
    pub std: BTreeMap<u16, Arc<RcodeInfo>>,
    pub tsig: BTreeMap<u16, Arc<RcodeInfo>>,
    pub edns: BTreeMap<u16, Arc<RcodeInfo>>,
}

impl RcodeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut std = BTreeMap::new();
        let mut tsig = BTreeMap::new();
        let mut edns = BTreeMap::new();
        let mut seen_notauth = 0;
        for r in registry.find_all((ASSIGNMENTS_NS, "record")) {
            let mstr = match r.find((ASSIGNMENTS_NS, "name")).expect("value").text() {
                "Unassigned"
                | "Reserved, can be allocated by Standards Action"
                | "Reserved for Private Use" => continue,
                other => other.to_string(),
            };
            let mut desc = r
                .find((ASSIGNMENTS_NS, "description"))
                .expect("description")
                .text()
                .to_string();
            if !desc.ends_with('.') {
                desc.push('.');
            }
            let num = r
                .find((ASSIGNMENTS_NS, "value"))
                .expect("value")
                .text()
                .parse()
                .expect("value");
            let refs = extract_references(r, rfc_index, people, None);
            let info = if num == 9 && seen_notauth > 0 {
                assert_eq!(seen_notauth, 1);
                seen_notauth += 1;
                let mut first: Arc<RcodeInfo> = std.remove(&9).expect("previous entry");
                tsig.remove(&9);
                edns.remove(&9);
                {
                    let first = Arc::get_mut(&mut first).expect("only reference");
                    first.refs.extend_from_slice(&refs[..]);
                    first.desc.pop();
                    for s in &[" or ", &desc, " (RCODE assigned twice)"] {
                        first.desc.push_str(s);
                    }
                }
                first
            } else {
                if num == 9 {
                    seen_notauth += 1;
                }
                Arc::new(RcodeInfo {
                    num,
                    mstr,
                    desc,
                    refs,
                })
            };
            if num <= 15 {
                assert!(std.insert(num, Arc::clone(&info)).is_none());
                assert!(tsig.insert(num, Arc::clone(&info)).is_none());
                assert!(edns.insert(num, info).is_none());
            } else if info.tsig() {
                assert!(tsig.insert(num, info).is_none());
            } else {
                assert!(edns.insert(num, info).is_none());
            }
        }
        assert_eq!(seen_notauth, 2);
        RcodeRegistry { std, tsig, edns }
    }
}

#[derive(Debug, Clone)]
pub struct TsigAlgInfo {
    pub name: String,
    pub title: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct TsigAlgRegistry(pub Vec<TsigAlgInfo>);

impl TsigAlgRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .map(|r| {
                let name = match r
                    .find((ASSIGNMENTS_NS, "name"))
                    .expect("tsig alg name")
                    .text()
                {
                    other if other.ends_with('.') => other.to_lowercase(),
                    other => {
                        let mut t = other.to_lowercase();
                        t.push('.');
                        t
                    }
                };
                let title = name
                    .split('.')
                    .next()
                    .expect("first label of tsig alg")
                    .to_uppercase();
                let refs = extract_references(r, rfc_index, people, None);
                TsigAlgInfo { name, title, refs }
            })
            .collect();
        v.sort_by(|a, b| a.name.cmp(&b.name));
        TsigAlgRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct EdnsOptInfo {
    pub name: String,
    pub value: u16,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct EdnsOptRegistry(pub Vec<EdnsOptInfo>);

impl EdnsOptRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("edns value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u16 = value.parse().expect("edns value should be a u16");
                if value == 0 || value == 0xFFFF {
                    return None;
                }
                let name = match r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("edns desc")
                    .text()
                {
                    "Reserved" if value == 4 => "Owner".into(),
                    "Unassigned" => return None,
                    other => {
                        let mut t = String::with_capacity(other.len());
                        let parts = other.split(|c: char| c.is_whitespace() || c == '-');
                        for s in parts {
                            assert!(!s.is_empty());
                            if s.eq_ignore_ascii_case("edns") {
                                continue;
                            }
                            if !t.is_empty() {
                                t.push(' ');
                            }
                            if s == "tcp" {
                                t.push_str("TCP");
                                continue;
                            }
                            let mut chars = s.chars();
                            t.extend(chars.next().unwrap().to_uppercase());
                            t.extend(chars);
                        }
                        t
                    }
                };
                let refs = extract_references(r, rfc_index, people, None);
                Some(EdnsOptInfo { name, value, refs })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        EdnsOptRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct DsDigestAlgInfo {
    pub name: String,
    pub value: u8,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct DsDigestAlgRegistry(pub Vec<DsDigestAlgInfo>);

impl DsDigestAlgRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("ds digest value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("ds digest value should be a u8");
                if value == 0 {
                    return None;
                }
                let name = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("ds digest desc")
                    .text()
                    .to_string();
                let refs = extract_references(r, rfc_index, people, None);
                Some(DsDigestAlgInfo { name, value, refs })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        DsDigestAlgRegistry(v)
    }
}

#[derive(Clone, Debug)]
pub struct DnskeyFlagInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct DnskeyFlagRegistry(pub Vec<DnskeyFlagInfo>);

impl DnskeyFlagRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("dnskey flag value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("dnskey flag value should be a u8");
                let description = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("dnskey flag desc")
                    .text()
                    .to_string();
                let refs = extract_references(r, rfc_index, people, None);
                Some(DnskeyFlagInfo {
                    value,
                    description,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        DnskeyFlagRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct TlsaCertUsageInfo {
    pub acronym: String,
    pub description: String,
    pub value: u8,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct TlsaCertUsageRegistry(pub Vec<TlsaCertUsageInfo>);

impl TlsaCertUsageRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("tlsa cert usage value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("tlsa cert usage value should be a u8");
                let acronym = r
                    .find((ASSIGNMENTS_NS, "acronym"))
                    .expect("tlsa cert usage acronym")
                    .text()
                    .to_string();
                let description = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("tlsa cert usage description")
                    .text()
                    .to_string();
                let refs = extract_references(r, rfc_index, people, None);
                Some(TlsaCertUsageInfo {
                    acronym,
                    description,
                    value,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        TlsaCertUsageRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct TlsaSelectorInfo {
    pub acronym: String,
    pub description: String,
    pub value: u8,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct TlsaSelectorInfoRegistry(pub Vec<TlsaSelectorInfo>);

impl TlsaSelectorInfoRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("tlsa cert usage value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("tlsa cert usage value should be a u8");
                let acronym = r
                    .find((ASSIGNMENTS_NS, "acronym"))
                    .expect("tlsa cert usage acronym")
                    .text()
                    .to_string();
                let description = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("tlsa cert usage description")
                    .text()
                    .to_string();
                let refs = extract_references(r, rfc_index, people, None);
                Some(TlsaSelectorInfo {
                    acronym,
                    description,
                    value,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        TlsaSelectorInfoRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct TlsaMatchingTypeInfo {
    pub acronym: String,
    pub description: String,
    pub value: u8,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct TlsaMatchingTypeRegistry(pub Vec<TlsaMatchingTypeInfo>);

impl TlsaMatchingTypeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("tlsa matching type value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value
                    .parse()
                    .expect("tlsa matching type value should be a u8");
                let acronym = r
                    .find((ASSIGNMENTS_NS, "acronym"))
                    .expect("tlsa matching type acronym")
                    .text()
                    .to_string();
                let description = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("tlsa matching type description")
                    .text()
                    .to_string();
                let refs = extract_references(r, rfc_index, people, None);
                Some(TlsaMatchingTypeInfo {
                    acronym,
                    description,
                    value,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        TlsaMatchingTypeRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct SshPubKeyAlgInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct SshPubKeyAlgRegistry(pub Vec<SshPubKeyAlgInfo>);

impl SshPubKeyAlgRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        SshPubKeyAlgRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "value"))
                        .expect("ssh pub key value")
                        .text();
                    if value.contains('-') {
                        return None;
                    }
                    let value: u8 = value.parse().expect("ssh pub key value should be a u8");
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("ssh pub key description")
                        .text();
                    if description == "Reserved" || description == "Unassigned" {
                        return None;
                    }
                    Some(SshPubKeyAlgInfo {
                        value,
                        description: description.to_string(),
                        refs: extract_references(r, rfc_index, people, None),
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct SshFpTypeInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct SshFpTypeRegistry(pub Vec<SshFpTypeInfo>);

impl SshFpTypeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        SshFpTypeRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "value"))
                        .expect("ssh pub key value")
                        .text();
                    if value.contains('-') {
                        return None;
                    }
                    let value: u8 = value.parse().expect("ssh pub key value should be a u8");
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("ssh pub key description")
                        .text();
                    if description == "Reserved" {
                        return None;
                    }
                    Some(SshFpTypeInfo {
                        value,
                        description: description.to_string(),
                        refs: extract_references(r, rfc_index, people, None),
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct Nsec3FlagInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct Nsec3FlagRegistry(pub Vec<Nsec3FlagInfo>);

impl Nsec3FlagRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        Nsec3FlagRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "value"))
                        .expect("nsec3 flag value")
                        .text();
                    if value.contains('-') {
                        return None;
                    }
                    let value: u8 = value.parse().expect("nsec3 flag value should be a u8");
                    let description = match r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("nsec3 flag desc")
                        .text()
                    {
                        "Reserved" => return None,
                        s => s.to_string(),
                    };
                    let refs = extract_references(r, rfc_index, people, None);
                    Some(Nsec3FlagInfo {
                        value,
                        description,
                        refs,
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct Nsec3paramFlagInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct Nsec3paramFlagRegistry(pub Vec<Nsec3paramFlagInfo>);

impl Nsec3paramFlagRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("nsec3 flag value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("nsec3 flag value should be a u8");
                let description = match r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("nsec3 flag desc")
                    .text()
                {
                    "Reserved" => return None,
                    s => s.to_string(),
                };
                let refs = extract_references(r, rfc_index, people, None);
                Some(Nsec3paramFlagInfo {
                    value,
                    description,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        Nsec3paramFlagRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct Nsec3HashAlgInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct Nsec3HashAlgRegistry(pub Vec<Nsec3HashAlgInfo>);

impl Nsec3HashAlgRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("nsec3 flag value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("nsec3 flag value should be a u8");
                let description = match r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("nsec3 flag desc")
                    .text()
                {
                    "Reserved" => return None,
                    s => s.to_string(),
                };
                let refs = extract_references(r, rfc_index, people, None);
                Some(Nsec3HashAlgInfo {
                    value,
                    description,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        Nsec3HashAlgRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct CertTypeInfo {
    pub num: u16,
    pub ty: String,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct CertTypeRegistry(pub Vec<CertTypeInfo>);

impl CertTypeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        CertTypeRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let num = r
                        .find((ASSIGNMENTS_NS, "number"))
                        .expect("cert type num")
                        .text();
                    if num.contains('-') {
                        return None;
                    }
                    let num: u16 = num.parse().expect("cert type num should be a u16");
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("cert type description")
                        .text();
                    if description == "Reserved" {
                        return None;
                    }
                    let ty = r
                        .find((ASSIGNMENTS_NS, "type"))
                        .expect("cert type")
                        .text()
                        .to_string();
                    Some(CertTypeInfo {
                        num,
                        ty,
                        description: description.to_string(),
                        refs: extract_references(r, rfc_index, people, None),
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct CaaFlagInfo {
    pub value: u8,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct CaaFlagRegistry(pub Vec<CaaFlagInfo>);

impl CaaFlagRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        CaaFlagRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "value"))
                        .expect("caa flag value")
                        .text();
                    if value.contains('-') {
                        return None;
                    }
                    let value: u8 = value.parse().expect("caa flag value should be a u8");
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("caa flag desc")
                        .text()
                        .to_string();
                    let refs = extract_references(r, rfc_index, people, None);
                    Some(CaaFlagInfo {
                        value,
                        description,
                        refs,
                    })
                })
                .collect(),
        )
    }
}

fn extract_references<T>(e: &Element, rfc: &RfcIndex, people: &People, extra: T) -> Vec<Rf>
where
    T: IntoIterator<Item = Rf>,
{
    fn strip_bis(mut s: &str) -> Option<String> {
        let bis_offset = s.rfind("bis")?;
        s = s[..bis_offset].trim_end_matches('-');
        let rfc_offset = s.find("rfc")?;
        s = &s[rfc_offset + 3..];
        if s.chars().all(|c| c.is_ascii_digit()) {
            Some(format!("RFC{}", s))
        } else {
            None
        }
    }
    let mut v = Vec::new();
    for r in e.find_all((ASSIGNMENTS_NS, "xref")) {
        match r.get_attr("type").expect("type") {
            "note" => {
                if r.get_attr("data") != Some("1") {
                    panic!("unexpected note");
                }
                eprintln!("ignored xref note with attr type=1 - assuming its for one of the two RFC1002 clashes");
            }
            "rfc" => {
                let data = r.get_attr("data").expect("data").to_uppercase();
                let title = rfc[&data].clone();
                v.push(Rf::Rfc {
                    doc: data,
                    title,
                    errata: Vec::new(),
                });
            }
            "person" => {
                let data = r.get_attr("data").expect("data");
                let person = people[data].clone();
                v.push(Rf::Person(person));
            }
            "text" => match r.text() {
                // TODO: capturing document type/status could be useful
                "informational" | "proposed standard" | "standards track" | "IANA-Reserved" => (),
                text => v.push(Rf::Text(text.replace(char::is_control, " "))),
            },
            "uri" => {
                let data = r.get_attr("data").expect("data");
                let uri: String = match data {
                    "http://files.dns-sd.org/draft-sekar-dns-llq.txt" => {
                        "https://tools.ietf.org/html/draft-sekar-dns-llq".into()
                    }
                    "http://files.dns-sd.org/draft-sekar-dns-ul.txt" => {
                        "https://tools.ietf.org/html/draft-sekar-dns-ul".into()
                    }
                    other => other.into(),
                };
                let title = match uri.as_str() {
                    "http://ana-3.lcs.mit.edu/~jnc/nimrod/dns.txt" => {
                        Some("DNS Resource Records for Nimrod Routing Architecture".into())
                    }
                    "https://docs.umbrella.com/developer/networkdevices-api/identifying-dns-traffic2" => {
                        Some("Cisco Umbrella - Identifying DNS traffic".into())
                    }
                    "http://tools.ietf.org/html/draft-eastlake-kitchen-sink" => {
                        Some("The Kitchen Sink Resource Record".into())
                    }
                    "http://www.broadband-forum.org/ftp/pub/approved-specs/af-dans-0152.000.pdf" => {
                        Some("ATM Name System V2.0 (PDF)".into())
                    }
                    "http://www.watson.org/~weiler/INI1999-19.pdf" => {
                        Some("Deploying DNSSEC Without a Signed Root (PDF)".into())
                    }
                    other if other.starts_with("https://tools.ietf.org/") => {
                        uri.rsplit('/').map(Into::into).next()
                    }
                    _ => None,
                };
                if title.is_none() {
                    eprintln!("no title for uri: {}", data);
                }
                v.push(Rf::Uri { title, uri })
            }
            "draft" => {
                fn clean_doc_name(s: &str) -> std::borrow::Cow<'_, str> {
                    let head_len = if s.starts_with("RFC-") {
                        4
                    } else if s.starts_with("draft-") {
                        6
                    } else {
                        return s.into();
                    };
                    let num_tail = s
                        .rsplit('-')
                        .take(1)
                        .filter_map(|s| {
                            if !s.is_empty() && s.as_bytes().iter().all(|&b| b.is_ascii_digit()) {
                                Some(1 + s.len())
                            } else {
                                None
                            }
                        })
                        .next();
                    if let Some(len) = num_tail {
                        format!("draft-{}", &s[head_len..s.len() - len]).into()
                    } else {
                        s.into()
                    }
                }
                let data = clean_doc_name(r.get_attr("data").expect("data"));
                if let Some(doc) = strip_bis(&data).map(Into::into) {
                    let title = rfc[&doc].clone();
                    v.push(Rf::Rfc {
                        doc,
                        title,
                        errata: Vec::new(),
                    });
                }
                let uri = format!("https://tools.ietf.org/html/{}", data);
                let title = Some(data.into());
                v.push(Rf::Uri { title, uri });
            }
            "rfc-errata" => {
                let data = r.get_attr("data").expect("data").to_string();
                if let Some(&mut Rf::Rfc { ref mut errata, .. }) = v.last_mut() {
                    errata.push(data);
                } else {
                    panic!("rfc-errata should only follow rfc");
                }
            }
            other => {
                panic!("not sure how to handle xref type: {}", other);
            }
        }
    }
    v.extend(extra);
    v.sort();
    v.dedup();
    v
}

#[derive(Debug, Clone)]
pub struct EdeInfo {
    pub value: u16,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct EdeRegistry(pub Vec<EdeInfo>);

impl EdeRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        EdeRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value_str = r.find((ASSIGNMENTS_NS, "value")).expect("ede value").text();
                    if value_str.contains('-') {
                        return None;
                    }
                    let value: u16 = value_str.parse().unwrap();
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("ede description")
                        .text()
                        .to_string();
                    let refs = extract_references(r, rfc_index, people, None);
                    Some(EdeInfo {
                        value,
                        description,
                        refs,
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct EheaderInfo {
    pub value: u8,
    pub title: String,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct EheaderRegistry(pub Vec<EheaderInfo>);

impl EheaderRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        EheaderRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "bit"))
                        .expect("edns header bit")
                        .text()
                        .trim_start_matches("Bit ");
                    if value.contains('-') {
                        return None;
                    }
                    let value: u8 = value.parse().expect("edns header flag should be a u8");
                    let title = r
                        .find((ASSIGNMENTS_NS, "flag"))
                        .expect("edns header title")
                        .text()
                        .to_string();
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("edns header desc")
                        .text()
                        .to_string();
                    let refs = extract_references(r, rfc_index, people, None);
                    Some(EheaderInfo {
                        value,
                        title,
                        description,
                        refs,
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct CsyncFlagInfo {
    pub value: u8,
    pub name: String,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct CsyncFlagRegistry(pub Vec<CsyncFlagInfo>);

impl CsyncFlagRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        CsyncFlagRegistry(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "value"))
                        .expect("csync flag value")
                        .text();
                    if value.contains('-') {
                        return None;
                    }
                    let value: u8 = value.parse().expect("csync flag value should be a u8");
                    let name = r
                        .find((ASSIGNMENTS_NS, "name"))
                        .expect("csync flag name")
                        .text()
                        .to_string();
                    let description = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("csync flag desc")
                        .text()
                        .to_string();
                    let refs = extract_references(r, rfc_index, people, None);
                    Some(CsyncFlagInfo {
                        value,
                        name,
                        description,
                        refs,
                    })
                })
                .collect(),
        )
    }
}

#[derive(Debug, Clone)]
pub struct IpseckeyAlgInfo {
    pub value: u8,
    pub name: String,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct IpseckeyAlgRegistry(pub Vec<IpseckeyAlgInfo>);

impl IpseckeyAlgRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        let mut saw_key_is_present = false;
        let mut v: Vec<_> = registry
            .find_all((ASSIGNMENTS_NS, "record"))
            .filter_map(|r| {
                let value = r
                    .find((ASSIGNMENTS_NS, "value"))
                    .expect("ipseckey alg value")
                    .text();
                if value.contains('-') {
                    return None;
                }
                let value: u8 = value.parse().expect("ipseckey alg value should be a u8");
                let desc_el = r
                    .find((ASSIGNMENTS_NS, "description"))
                    .expect("ipseckey alg desc");
                let mut description = desc_el.text().to_string();
                let desc_refs = extract_references(desc_el, rfc_index, people, None);
                match desc_refs.len() {
                    0 => (),
                    1 => {
                        let format_link = desc_refs[0].to_string();
                        description.push_str(&format_link);
                    }
                    n => panic!("didn't expect {} refs in ipseckey alg description", n),
                }
                description.push('.');
                let mut name = desc_el.text().to_string();
                if let Some(i) = name.find(" key is present") {
                    name.truncate(i);
                    saw_key_is_present = true;
                }
                for s in &["A ", "An "] {
                    if name.starts_with(s) {
                        name.replace_range(0..s.len(), "")
                    }
                }
                if name == "No" {
                    name.push_str("ne");
                }
                let refs = extract_references(r, rfc_index, people, desc_refs[..].iter().cloned());
                Some(IpseckeyAlgInfo {
                    value,
                    name,
                    description,
                    refs,
                })
            })
            .collect();
        v.sort_by_key(|i| i.value);
        if !saw_key_is_present {
            eprintln!(
                "`key is present check` no longer relevant {}:{}",
                file!(),
                line!()
            );
        }
        IpseckeyAlgRegistry(v)
    }
}

#[derive(Debug, Clone)]
pub struct SvcParamKeyInfo {
    pub value: u16,
    pub name: String,
    pub description: String,
    pub refs: Vec<Rf>,
}

#[derive(Debug, Clone)]
pub struct SvcParamKeyRegistry(pub Vec<SvcParamKeyInfo>);

impl SvcParamKeyRegistry {
    pub fn new(registry: &Element, rfc_index: &RfcIndex, people: &People) -> Self {
        Self(
            registry
                .find_all((ASSIGNMENTS_NS, "record"))
                .filter_map(|r| {
                    let value = r
                        .find((ASSIGNMENTS_NS, "value"))
                        .expect("dns svcb param value")
                        .text();
                    if value.contains('-') {
                        return None;
                    }
                    let value: u16 = value.parse().expect("dns svcb param value should be a u16");
                    if value == 0xFFFF {
                        return None;
                    }
                    let name = r
                        .find((ASSIGNMENTS_NS, "name"))
                        .expect("dns svcb param name")
                        .text()
                        .to_string();
                    let desc_el = r
                        .find((ASSIGNMENTS_NS, "description"))
                        .expect("dns svcb param desc");
                    let mut description = desc_el.text().to_string();
                    let desc_refs = extract_references(desc_el, rfc_index, people, None);
                    match desc_refs.len() {
                        0 => (),
                        1 => {
                            let format_link = desc_refs[0].to_string();
                            description.push_str(&format_link);
                        }
                        n => panic!(
                            "didn't expect {} refs in dns svcb param value description",
                            n
                        ),
                    }
                    description.push('.');
                    let refs =
                        extract_references(r, rfc_index, people, desc_refs[..].iter().cloned());
                    Some(SvcParamKeyInfo {
                        value,
                        name,
                        description,
                        refs,
                    })
                })
                .collect(),
        )
    }
}
