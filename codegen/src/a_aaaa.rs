use proc_macro2::TokenStream;
use syn::Ident;

pub fn add_extra(extra: &mut Vec<TokenStream>, mod_name: &str, struct_name: &Ident) {
    let (size, ip_ty) = match mod_name {
        "a" => (4usize, quote! { core::net::Ipv4Addr }),
        "aaaa" => (16, quote! { core::net::Ipv6Addr }),
        _ => unreachable!(),
    };
    extra.push(quote! {
        impl Default for #struct_name {
            fn default() -> Self {
                #struct_name {
                    ip: [0u8; #size].into(),
                }
            }
        }

        impl From<#ip_ty> for #struct_name {
            fn from(ip: #ip_ty) -> Self {
                #struct_name { ip }
            }
        }

        impl<'a> From<&'a #ip_ty> for #struct_name {
            fn from(ip: &#ip_ty) -> Self {
                #struct_name { ip: *ip }
            }
        }

        impl From<#struct_name> for #ip_ty {
            fn from(#struct_name{ip}: #struct_name) -> Self {
                ip
            }
        }

        impl<'a> From<&'a #struct_name> for #ip_ty {
            fn from(&#struct_name{ip}: &#struct_name) -> Self {
                ip
            }
        }
    });
}
