#!/bin/sh
set -euxo pipefail
for a in rsasha1 rsasha256 rsasha512 ecdsap256sha256 ecdsap384sha384 ed25519; do
    dnssec-keygen -C -a ${a} -b 2048 -TKEY -n HOST ${a}.sig0.example.
done

