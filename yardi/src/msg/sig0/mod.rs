//! Support for [RFC 2931: DNS Request and Transaction Signatures ( SIG(0)s )](https://tools.ietf.org/html/rfc2931).
//!
//! SIG(0) messages use the following signature input:
//! <table>
//! <thead><tr><th>Message Type</th><th>Input</th></tr></thead>
//! <tbody>
//! <tr><td>Request</td><td>RDATA | Unsigned Request</td></tr>
//! <tr><td>Response</td><td>RDATA | Signed Request | Unsigned Response</td></tr>
//! <tr><td>Subsequent Response</td><td>RDATA | Unsigned Response | Previous Response</td></tr>
//! </tbody>
//! <tfooter><td colspan="2">Where RDATA is the SIG record data without the signature and | indicates concatentation.</td></tfooter>
//! </table>
//! The verify functions take a partial message that does not include the SIG(0) RR though
//! with its presence still included in the message's ARCOUNT.
#[cfg(test)]
mod test;

use crate::{
    datatypes::{Class, Name, Time, Ttl, Ty},
    dnssec::{SigAlg, Sign, Verify},
    msg::{self, Header},
    rdata::Sig,
    wire::{Format, Pack, PackError, Reader, Unpack, UnpackError, Writer},
};

const RR_HEADER: &[u8] = &[
    0, // "."
    0, 24, // Ty::SIG
    0, 255, // Class::ANY
    0, 0, 0, 0, // Ttl(0)
];

/// Retrieve a SIG0 signature from a `Reader`.
///
/// Returns the message truncated before the SIG RR and the SIG data, if any.
/// The `Reader`'s index is preserved.
pub fn seek<'a>(reader: &mut Reader<'a>) -> Result<Option<(&'a [u8], Sig)>, UnpackError> {
    let start = reader.index();
    let r = seek_impl(reader);
    reader.set_index(start);
    r
}

fn seek_impl<'a>(reader: &mut Reader<'a>) -> Result<Option<(&'a [u8], Sig)>, UnpackError> {
    if !msg::seek_last(reader)? {
        return Ok(None);
    }
    let partial_message = reader.get_bytes_seen()?;
    if !Name::unpack(reader)?.is_root() {
        return Ok(None);
    }
    if Ty::unpack(reader)? != Ty::SIG {
        return Ok(None);
    }
    if Class::unpack(reader)? != Class::ANY {
        return Ok(None);
    }
    if Ttl::unpack(reader)? != Ttl(0) {
        return Ok(None);
    }
    let sig = Sig::unpack16(reader)?;
    if sig.ty_covered != Ty(0) {
        return Ok(None);
    }
    if !reader.is_empty() {
        return Err(UnpackError::trailing_junk_at(reader.index()));
    }
    Ok(Some((partial_message, sig)))
}

/// An error returned from signing a message.
#[derive(Debug)]
pub enum SignError {
    /// A pack error occurred.
    Pack(PackError),
    /// An unspecified error occurred.
    Unspecified,
}

impl From<PackError> for SignError {
    fn from(err: PackError) -> Self {
        SignError::Pack(err)
    }
}

/// Sign a request.
pub fn sign_request(
    signer: &Name,
    alg: SigAlg,
    key_tag: u16,
    key: &mut dyn Sign,
    now: Time,
    writer: &mut Writer<'_>,
) -> Result<(), SignError> {
    // data = RDATA | request - SIG(0)
    sign_response(signer, alg, key_tag, key, b"", now, writer)
}

/// Sign a response.
pub fn sign_response(
    signer: &Name,
    alg: SigAlg,
    key_tag: u16,
    key: &mut dyn Sign,
    request: &[u8],
    now: Time,
    response: &mut Writer<'_>,
) -> Result<(), SignError> {
    // data = RDATA | full query | response - SIG(0)
    if response.len() < 12 || Header::from_bytes(response.written()).arcount() == 0xFFFF {
        return Err(SignError::Pack(PackError::value_invalid()));
    }
    let incept = Time(now.0.wrapping_sub(60));
    let expire = Time(now.0.wrapping_add(60));
    let mut data: Sig<&[u8]> = Sig {
        ty_covered: Ty(0),
        alg,
        labels: 0,
        original_ttl: Ttl(0),
        expire,
        incept,
        key_tag,
        signers_name: signer.into(),
        signature: &[0],
    };
    let sig_rd_no_sig_len = data.packed_len_no_sig()?;
    let sig_rr_len = RR_HEADER.len() + 2 + data.packed_len()? - 1 + key.sig_len();
    let expected_len = response.len() + sig_rr_len;
    if response.remaining() < sig_rr_len {
        return Err(SignError::Pack(PackError::no_space()));
    }
    // TODO: SmallVec?
    let mut temp_buf = vec![0; sig_rd_no_sig_len + request.len() + response.len()];
    let mut temp_w = Writer::new(&mut temp_buf);
    temp_w.set_format(Format::Plain);
    data.pack_no_sig(&mut temp_w).unwrap();
    temp_w.write_bytes(request).unwrap();
    temp_w.write_bytes(response.written()).unwrap();
    assert_eq!(temp_w.remaining(), 0);
    let sig_buf = key
        .sign(temp_w.written())
        .map_err(|()| SignError::Unspecified)?;
    debug_assert_eq!(sig_buf.as_ref().len(), key.sig_len());
    // TODO: avoid this alloc?
    // TODO: this could fail if dyn Sign does something crazy
    data.signature = sig_buf.as_ref();

    let response_format = response.format();
    response.set_format(Format::Plain);
    let sig_rr_pack_result = response
        .write_bytes(RR_HEADER)
        .and_then(|_| data.pack16(response));
    response.set_format(response_format);
    sig_rr_pack_result.unwrap();

    assert_eq!(expected_len, response.len());
    let h = Header::from_bytes_mut(response.written_mut());
    let arcount = h.arcount() + 1;
    h.set_arcount(arcount);
    Ok(())
}

/// Verify a request.
///
/// `partial_message` is expected to contain the signed response truncated
/// before the SIG(0) RR and without ARCOUNT adjusted.
// TODO: Error - Unspecified? Unproven? Bogus? Bogus is probably the right terminology? RFC4033
pub fn verify_request(partial_msg: &[u8], sig_rdata: &Sig, key: &dyn Verify) -> Result<(), ()> {
    // data = RDATA | request - SIG(0)
    verify_response(b"", partial_msg, sig_rdata, key)
}

/// Verify a response.
///
/// `partial_message` is expected to contain the signed response truncated
/// before the SIG(0) RR and without ARCOUNT adjusted.
/// TODO: Error - same as verify_request
pub fn verify_response(
    request: &[u8],
    partial_msg: &[u8],
    sig_rdata: &Sig,
    key: &dyn Verify,
) -> Result<(), ()> {
    // data = RDATA | full query | response - SIG(0)
    if partial_msg.len() < 12 {
        return Err(());
    }
    let sig_rd_no_sig_len = sig_rdata.packed_len_no_sig().unwrap();
    // TODO: SmallVec?
    let mut temp_buf = vec![0; sig_rd_no_sig_len + request.len() + partial_msg.len()];
    let mut temp_w = Writer::new(&mut temp_buf[..]);
    temp_w.set_format(Format::Plain);
    sig_rdata.pack_no_sig(&mut temp_w).unwrap();
    temp_w.write_bytes(request).unwrap();
    let (partial_header, partial_body) = partial_msg.split_at(12);
    let h_start = temp_w.len();
    temp_w.write_bytes(partial_header).unwrap();
    let h = Header::from_bytes_mut(&mut temp_w.written_mut()[h_start..]);
    let arcount = h.arcount();
    if arcount == 0 {
        return Err(());
    }
    h.set_arcount(arcount - 1);
    temp_w.write_bytes(partial_body).unwrap();
    assert_eq!(temp_w.remaining(), 0);
    key.verify(temp_w.written(), &sig_rdata.signature[..])
}

/// Sign a subsequent response.
pub fn sign_subsequent(
    signer: &Name,
    alg: SigAlg,
    key_tag: u16,
    key: &mut dyn Sign,
    previous: &[u8],
    now: Time,
    current: &mut Writer<'_>,
) -> Result<(), SignError> {
    // data = RDATA | DNS payload - SIG(0) | previous packet
    if current.len() < 12 || Header::from_bytes(current.written()).arcount() == 0xFFFF {
        return Err(SignError::Pack(PackError::value_invalid()));
    }
    // TODO: should window be configurable?
    let (incept, expire) = now.window(60);
    let mut data: Sig<&[u8]> = Sig {
        ty_covered: Ty(0),
        alg,
        labels: 0,
        original_ttl: Ttl(0),
        expire,
        incept,
        key_tag,
        signers_name: signer.into(),
        signature: &[0],
    };
    let sig_rd_no_sig_len = data.packed_len_no_sig()?;
    let sig_rr_len = RR_HEADER.len() + 2 + data.packed_len()? - 1 + key.sig_len();
    let expected_len = current.len() + sig_rr_len;
    if current.remaining() < sig_rr_len {
        return Err(SignError::Pack(PackError::no_space()));
    }
    let mut temp_buf = vec![0; sig_rd_no_sig_len + current.len() + previous.len()];
    let mut temp_w = Writer::new(&mut temp_buf);
    temp_w.set_format(Format::Plain);
    data.pack_no_sig(&mut temp_w).unwrap();
    temp_w.write_bytes(current.written()).unwrap();
    temp_w.write_bytes(previous).unwrap();
    assert_eq!(temp_w.remaining(), 0);
    let sig_buf = key
        .sign(temp_w.written())
        .map_err(|()| SignError::Unspecified)?;
    assert_eq!(sig_buf.as_ref().len(), key.sig_len());
    // TODO: avoid this alloc?
    // TODO: this could fail if dyn Sig returns something crazy..
    data.signature = sig_buf.as_ref();

    let current_format = current.format();
    current.set_format(Format::Plain);
    let sig_rr_pack_result = current
        .write_bytes(RR_HEADER)
        .and_then(|_| data.pack16(current));
    current.set_format(current_format);
    sig_rr_pack_result.unwrap();

    assert_eq!(expected_len, current.len());
    let h = Header::from_bytes_mut(current.written_mut());
    let arcount = h.arcount() + 1;
    h.set_arcount(arcount);
    Ok(())
}

/// Verify a subsequent response.
///
/// `partial_message` is expected to contain the signed response truncated
/// before the SIG(0) RR and without ARCOUNT adjusted.
/// TODO: Error - same as verify_request
pub fn verify_subsequent(
    previous: &[u8],
    partial_msg: &[u8],
    sig_rdata: &Sig,
    key: &dyn Verify,
) -> Result<(), ()> {
    // data = RDATA | DNS payload - SIG(0) | previous packet
    if partial_msg.len() < 12 {
        return Err(());
    }
    let sig_rd_no_sig_len = sig_rdata.packed_len_no_sig().unwrap();
    let mut buf = vec![0; sig_rd_no_sig_len + partial_msg.len() + previous.len()];
    let mut w = Writer::new(&mut buf);
    w.set_format(Format::Plain);
    sig_rdata.pack_no_sig(&mut w).unwrap();
    let (partial_header, partial_body) = partial_msg.split_at(12);
    let h_start = w.len();
    w.write_bytes(partial_header).unwrap();
    let h = Header::from_bytes_mut(&mut w.written_mut()[h_start..]);
    let arcount = h.arcount();
    if arcount == 0 {
        return Err(());
    }
    h.set_arcount(arcount - 1);
    w.write_bytes(partial_body).unwrap();
    w.write_bytes(previous).unwrap();
    assert_eq!(w.remaining(), 0);
    key.verify(w.written(), &sig_rdata.signature[..])
}
