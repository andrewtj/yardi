use crate::{
    ascii::{parse, Context},
    datatypes::{Class, Time, Ttl, Ty},
    dnssec::{self, Sign},
    msg::{self, sig0, Header, MsgSig, Opcode, Question, Rr},
    rdata::{Key, Sig},
    wire::{Pack, Reader, Unpack, Writer},
};
#[cfg(not(feature = "std"))]
use alloc::boxed::Box;

const NOW: Time = Time(1736896588);

fn test_key() -> (Box<dyn Sign>, Rr<Key>) {
    let mut c = Context::default();
    c.set_default_ttl(Ttl(0));
    let rr: Rr<Key> =
        parse::from_single_entry(include_bytes!("Ktest.+015+22215.key"), &c).expect("Rr<Key>");
    let pkmap = dnssec::PrivateKeyMap::new(include_str!("Ktest.+015+22215.private"))
        .expect("private key map");
    let signer = dnssec::boxed_sign(&pkmap, &rr.data.public_key).expect("signer");
    (signer, rr)
}

fn split_message(msg: &[u8]) -> (&[u8], Sig) {
    let mut r = Reader::new(msg);
    sig0::seek(&mut r).expect("Option<_>").expect("Sig")
}

#[test]
fn check_rr_header() {
    let expect = (name!("."), Ty::SIG, Class::ANY, Ttl(0));
    let mut r = Reader::new(super::RR_HEADER.into());
    let actual = Unpack::unpack(&mut r).expect("header");
    assert_eq!(expect, actual);
    assert!(r.is_empty());
}

#[test]
#[cfg_attr(miri, ignore)]
fn exercise() {
    let (mut signer, keyrr) = test_key();

    let question = Question {
        name: name!("example.").into(),
        ty: Ty::SOA,
        class: Class::IN,
    };

    let mut query_buf = [0u8; 512];
    let query_buf = {
        let mut w = Writer::new(&mut query_buf[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header"))
            .set_opcode(Opcode::UPDATE)
            .set_qdcount(1);
        question.pack(&mut w).expect("pack question");
        sig0::sign_request(
            &keyrr.name,
            keyrr.data.alg,
            keyrr.data.tag(),
            signer.as_mut(),
            NOW,
            &mut w,
        )
        .expect("sign");
        w.into_written()
    };

    let verifier = dnssec::boxed_verify(keyrr.data.alg, &keyrr.data.public_key).expect("verifier");

    let (query_partial, query_sig_rd) = {
        let mut r = Reader::new(&query_buf);
        match msg::seek_sig(&mut r) {
            Ok(Some((pm, MsgSig::Sig0(sig)))) => (pm, sig),
            x => panic!("{:?}", x),
        }
    };

    assert!(sig0::verify_request(&query_partial, &query_sig_rd, verifier.as_ref()).is_ok());

    let mut response_buf = [0u8; 512];
    let response_buf = {
        let mut w = Writer::new(&mut response_buf[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header"))
            .set_qr(true)
            .set_opcode(Opcode::UPDATE)
            .set_qdcount(1);
        question.pack(&mut w).expect("pack question");
        sig0::sign_response(
            &keyrr.name,
            keyrr.data.alg,
            keyrr.data.tag(),
            signer.as_mut(),
            &query_buf,
            NOW,
            &mut w,
        )
        .expect("sign");
        w.into_written()
    };

    let (response_partial, response_sig_rd) = split_message(&response_buf);

    assert!(sig0::verify_response(
        &query_buf,
        &response_partial,
        &response_sig_rd,
        verifier.as_ref()
    )
    .is_ok());

    let mut subresponse_buf = [0u8; 512];
    let subresponse_buf = {
        let mut w = Writer::new(&mut subresponse_buf[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header"))
            .set_qr(true)
            .set_opcode(Opcode::UPDATE)
            .set_qdcount(1);
        question.pack(&mut w).expect("pack question");
        sig0::sign_subsequent(
            &keyrr.name,
            keyrr.data.alg,
            keyrr.data.tag(),
            signer.as_mut(),
            &response_buf,
            NOW,
            &mut w,
        )
        .expect("sign");
        w.into_written()
    };

    let (subresponse_partial, subresponse_sig_rd) = split_message(&subresponse_buf);

    assert!(sig0::verify_subsequent(
        &response_buf,
        &subresponse_partial,
        &subresponse_sig_rd,
        verifier.as_ref()
    )
    .is_ok());
}
