mod generated;

use core::fmt::{self, Debug};

/// DNS message RCODE.
#[derive(Clone, Copy, Hash, Eq, PartialEq)]
pub struct Rcode(u8);

impl Rcode {
    // TODO: Implement TryFrom instead
    /// Construct an `Rcode` from a u8.
    pub fn from_u8(rcode: u8) -> Option<Rcode> {
        if rcode <= 0b0000_1111 {
            Some(Rcode(rcode))
        } else {
            None
        }
    }
    // TODO: Replace with a From implementation
    /// Convert an `Rcode` to a u8.
    pub fn to_u8(self) -> u8 {
        self.0
    }
}

impl Debug for Rcode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for &(r, m) in generated::MNEMONICS {
            if r == *self {
                return write!(f, "Rcode({})", m);
            }
        }
        // TODO: Use fmt's helpers instead
        write!(f, "Rcode({})", self.0)
    }
}
