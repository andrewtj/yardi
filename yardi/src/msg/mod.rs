//! Support for working with DNS messages.
mod header;
mod opcode;
mod question;
mod rcode;
mod rr;

pub mod edns;
pub mod sig0;
pub mod tsig;

pub use self::header::Header;
pub use self::opcode::Opcode;
pub use self::question::Question;
pub use self::rcode::Rcode;
pub use self::rr::{Rr, RrHeader};

use core::marker::PhantomData;

use self::tsig::TsigRr;
use crate::datatypes::{name::LabelHeader, Class, Name, Ttl, Ty};
use crate::rdata::{Generic, Rdata, Sig, StaticRdata, Tsig};
use crate::wire::{Reader, Unpack, UnpackError};

/// Maximum size for a message transported over UDP without using EDNS.
pub const MAX_UDP: usize = 512;
/// Maximum size for a message transported over TCP.
pub const MAX_TCP: usize = 0xFFFF;
/// Maximum size for a message transported using [mDNS](https://tools.ietf.org/html/rfc6762#section-17).
pub const MAX_MDNS: usize = 9000;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum RrSection {
    An,
    Au,
    Ad,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
enum RrIterState {
    Setup(RrSection),
    Remaining(u16),
}

// TODO: Is this actually useful?
/// Iterator over RRs in a section of a message.
///
/// May be configured to silently skip non-data RR or return an error.
/// No further items will be returned once an error has occurred.
#[derive(Debug)]
pub struct RrIter<'a> {
    state: RrIterState,
    reader: &'a mut Reader<'a>,
    reject_non_data: bool,
}

impl<'a> RrIter<'a> {
    fn next_rr(&mut self) -> Result<Option<Rr<Generic>>, UnpackError> {
        let (name, ty, class, ttl): (Name, Ty, Class, Ttl) = Unpack::unpack(self.reader)?;
        let data = if ty.is_data() && class.is_data() {
            Generic::unpack16(class, ty, self.reader)?
        } else if self.reject_non_data {
            return Err(UnpackError::value_invalid());
        } else {
            let rdlen = u16::unpack(self.reader)? as usize;
            self.reader.skip(rdlen)?;
            return Ok(None);
        };
        let rr = Rr {
            name,
            class,
            ttl,
            data,
        };
        Ok(Some(rr))
    }
    /// Returns a wrapper for this `RrIter` that returns only RRs of a particularly type.
    pub fn rdata<T>(self) -> RrIterT<'a, T>
    where
        T: Rdata + Unpack<'a>,
    {
        RrIterT {
            inner: self,
            marker: PhantomData,
        }
    }
    /// Configure this `RrIter` to silently skip non-data RRs.
    pub fn skip_non_data(&mut self) -> &mut Self {
        self.reject_non_data = false;
        self
    }
    /// Configure this `RrIter` to return an error if a non-data RR is encountered.
    pub fn reject_non_data(&mut self) -> &mut Self {
        self.reject_non_data = true;
        self
    }
}

impl<'a> Iterator for RrIter<'a> {
    type Item = Result<Rr<Generic>, UnpackError>;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.state {
                RrIterState::Remaining(0) => return None,
                RrIterState::Remaining(left) => match self.next_rr() {
                    Ok(maybe_rr) => {
                        self.state = RrIterState::Remaining(left - 1);
                        if let Some(rr) = maybe_rr {
                            return Some(Ok(rr));
                        }
                    }
                    Err(err) => {
                        self.state = RrIterState::Remaining(0);
                        return Some(Err(err));
                    }
                },
                RrIterState::Setup(section) => match setup(self.reader, section) {
                    Ok(remaining) => {
                        self.state = RrIterState::Remaining(remaining);
                    }
                    Err(err) => {
                        self.state = RrIterState::Remaining(0);
                        return Some(Err(err));
                    }
                },
            }
        }
        fn setup(reader: &mut Reader<'_>, section: RrSection) -> Result<u16, UnpackError> {
            reader.set_index(0);
            let (qdcount, skip, remaining) = reader.get_bytes_map(12, |h| {
                let h = Header::from_bytes(h);
                let qdc = h.qdcount();
                Ok(match section {
                    RrSection::An => (qdc, 0, h.ancount()),
                    RrSection::Au => (qdc, u32::from(h.ancount()), h.nscount()),
                    RrSection::Ad => (
                        qdc,
                        u32::from(h.ancount()) + u32::from(h.nscount()),
                        h.arcount(),
                    ),
                })
            })?;
            for _ in 0..qdcount {
                skip_q(reader)?;
            }
            for _ in 0..skip {
                skip_rr(reader)?;
            }
            Ok(remaining)
        }
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let max = if let RrIterState::Remaining(r) = self.state {
            Some(r as usize)
        } else {
            None
        };
        (0, max)
    }
}

macro_rules! section_iters {
    (
        $($fn:ident $section_s:tt $reject_s:tt $reject_b:tt $section_v:tt,)+
    ) => {
        $(
            #[doc = "Return an iterator over RR in the "]
            #[doc = $section_s]
            #[doc = " section of a message. Non-data RR are "]
            #[doc = $reject_s ]
            #[doc = " by default."]
            pub fn $fn<'a>(reader: &'a mut Reader<'a>) -> RrIter<'a> {
                let state = RrIterState::Setup(RrSection::$section_v);
                let reject_non_data = $reject_b;
                RrIter{ state, reader, reject_non_data }
            }
        )+
    }
}
section_iters!(
    answer_rr "Answer" "rejected" true An,
    authority_rr "Authority" "rejected" true Au,
    additional_rr "Additional" "skipped" false Ad,
);

/// Iterator over RRs of a specific type in a section of a message.
///
/// Handling of non-data RR is inherited from the `RrIter` used to
/// construct this iterator.
#[derive(Debug)]
pub struct RrIterT<'a, T> {
    inner: RrIter<'a>,
    marker: PhantomData<T>,
}

impl<'a, T> Iterator for RrIterT<'a, T>
where
    T: StaticRdata + for<'b> Unpack<'b>,
{
    type Item = Result<Rr<T>, UnpackError>;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            match self.inner.next()? {
                Err(err) => return Some(Err(err)),
                Ok(rr) => {
                    if rr.data.ty() == T::TY {
                        let data = match T::unpack(&mut rr.data.reader()) {
                            Err(err) => {
                                self.inner.state = RrIterState::Remaining(0);
                                return Some(Err(err));
                            }
                            Ok(t) => t,
                        };
                        let Rr {
                            name, class, ttl, ..
                        } = rr;
                        let rr = Rr {
                            name,
                            class,
                            ttl,
                            data,
                        };
                        return Some(Ok(rr));
                    }
                }
            }
        }
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        self.inner.size_hint()
    }
}

/// A message signature returned from `seek_sig`.
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum MsgSig {
    /// SIG(0) message signature.
    Sig0(Sig),
    /// TSIG message signature.
    Tsig(TsigRr),
}

fn seek_last(reader: &mut Reader<'_>) -> Result<bool, UnpackError> {
    reader.set_index(0);
    let h = Header::unpack(reader)?;
    if h.arcount() == 0 {
        return Ok(false);
    }
    for _ in 0..h.qdcount() {
        skip_q(reader)?;
    }
    for _ in 1..h.ancount() + h.nscount() + h.arcount() {
        skip_rr(reader)?;
    }
    Ok(true)
}

/// Skip over a question.
///
/// The question's Name is only unpacked as far as the first pointer. Do not
/// assume a question is valid because this function has succeeded.
pub fn skip_q(reader: &mut Reader<'_>) -> Result<(), UnpackError> {
    let initial_index = reader.index();
    let mut name_len = 0u8;
    while let LabelHeader::Len(l) = LabelHeader::unpack(reader)? {
        name_len = name_len
            .checked_add(1 + l)
            .ok_or_else(|| UnpackError::value_invalid_at(initial_index))?;
        if l == 0 {
            break;
        }
        reader.skip(l as usize)?;
    }
    reader.skip(2 + 2)
}

/// Skip over an RR.
///
/// The RR's name is only unpacked as far as the first pointer and RDATA is not
/// validated in any way. Do not assume an RR is valid because this function
/// has succeeded.
pub fn skip_rr(reader: &mut Reader<'_>) -> Result<(), UnpackError> {
    skip_q(reader)?;
    reader.skip(4)?;
    let rdlen = u16::unpack(reader)? as usize;
    reader.skip(rdlen)?;
    Ok(())
}

// TODO: is thise useful?
/// Retrieve a TSIG or SIG(0) message signature if present.
pub fn seek_sig<'a>(reader: &mut Reader<'a>) -> Result<Option<(&'a [u8], MsgSig)>, UnpackError> {
    let start = reader.index();
    let r = seek_sig_impl(reader);
    reader.set_index(start);
    r
}

fn seek_sig_impl<'a>(reader: &mut Reader<'a>) -> Result<Option<(&'a [u8], MsgSig)>, UnpackError> {
    if !seek_last(reader)? {
        return Ok(None);
    }
    let partial_message = reader.get_bytes_seen()?;
    let (name, ty): (Name, Ty) = Unpack::unpack(reader)?;
    let maybe_sig0 = name.is_root() && ty == Ty::SIG;
    if ty != Ty::TSIG && !maybe_sig0 {
        return Ok(None);
    }
    if Class::unpack(reader)? != Class::ANY {
        return Ok(None);
    }
    if Ttl::unpack(reader)? != Ttl(0) {
        return Ok(None);
    }
    let msg_sig = if maybe_sig0 {
        let sig = Sig::unpack16(reader)?;
        if sig.ty_covered != Ty(0) {
            return Ok(None);
        }
        MsgSig::Sig0(sig)
    } else {
        let data = Tsig::unpack16(reader)?;
        MsgSig::Tsig(TsigRr { name, data })
    };
    if !reader.is_empty() {
        return Err(UnpackError::trailing_junk_at(reader.index()));
    }
    Ok(Some((partial_message, msg_sig)))
}
