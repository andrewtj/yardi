mod generated;

use core::fmt::{self, Debug};

/// DNS message OPCODE.
#[derive(Clone, Copy, Hash, Eq, PartialEq)]
pub struct Opcode(u8);
impl Opcode {
    /// Construct an `Opcode` from a `u8`.
    pub fn from_u8(opcode: u8) -> Option<Opcode> {
        if opcode <= 0b0000_1111 {
            Some(Opcode(opcode))
        } else {
            None
        }
    }
    /// Convert an `Opcode` to a `u8`.
    pub fn to_u8(self) -> u8 {
        self.0
    }
}
impl Debug for Opcode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for &(o, m) in generated::MNEMONICS {
            if o == *self {
                return write!(f, "Opcode({})", m);
            }
        }
        write!(f, "Opcode({})", self.0)
    }
}
