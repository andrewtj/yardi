//! Support for the [LLQ Extended Option][1]
//!
//! [1]: https://tools.ietf.org/html/rfc8764

#[cfg(test)]
mod test;

use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};

const VERSION: u16 = 1;

macro_rules! impl_u16_enums {
    (
        $(
            $(#[$e_attr:meta])*
            $e_name:ident {
                $(
                    $(#[$v_attr:meta])*
                    $v_name:ident = $value:expr,
                )+
            },
        )+
    ) => {
        $(
            $(#[$e_attr])*
            #[derive(Clone, Copy, Debug, Default, PartialEq, Eq)]
            #[repr(u16)]
            pub enum $e_name {
                $(
                    $(#[$v_attr])*
                    $v_name = $value,
                )+
            }
            impl Pack for $e_name {
                fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
                    (*self as u16).pack(writer)
                }
                fn packed_len(&self) -> Result<usize, PackError> {
                    Ok(2)
                }
            }
            impl<'a> Unpack<'a> for $e_name {
                fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
                    match u16::unpack(reader)? {
                        $(
                            $value => Ok($e_name::$v_name),
                        )+
                        _ => Err(UnpackError::value_unknown_at(reader.index() - 2)),
                    }
                }
            }
        )+
    }
}
impl_u16_enums! {
    /// LLQ OpCode
    LlqOpCode {
        /// Setup
        #[default]
        Setup = 1,
        /// Refresh
        Refresh = 2,
        /// Change event
        Event = 3,
    },
    /// LLQ Error
    LlqError {
        /// No error.
        #[default]
        None = 0,
        /// Server full.
        ServFull = 1,
        /// Queried name and type isn't expected to change frequently.
        Static = 2,
        /// LLQ was improperly formatted.
        Format = 3,
        /// LLQ is expired or non-existent.
        NoSuchLlq = 4,
        /// Server does not support the client's LLQ version.
        BadVers = 5,
        /// Request not granted for an unknown reason.
        Unknown = 6,
    },
}

#[doc = "LLQ.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc8764\">RFC 8764: Apple&apos;s DNS Long-Lived Queries Protocol</a>"]
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct Llq {
    /// Identifies LLQ operation.
    pub opcode: LlqOpCode,
    /// Identifies LLQ errors.
    pub error: LlqError,
    /// Identifier for an LLQ.
    pub id: u64,
    /// Requested or granted life of an LLQ in seconds.
    pub lease_life: u32,
}

impl Opt for Llq {
    const CODE: OptCode = OptCode::LLQ;
}

impl<'a> Unpack<'a> for Llq {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        u16::unpack(reader).and_then(|n| {
            if n != VERSION {
                Err(UnpackError::value_unexpected_version_at(reader.index() - 2))
            } else {
                Ok(())
            }
        })?;
        Ok(Llq {
            opcode: Unpack::unpack(reader)?,
            error: Unpack::unpack(reader)?,
            id: Unpack::unpack(reader)?,
            lease_life: Unpack::unpack(reader)?,
        })
    }
}

impl Pack for Llq {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        VERSION.pack(writer)?;
        self.opcode.pack(writer)?;
        self.error.pack(writer)?;
        self.id.pack(writer)?;
        self.lease_life.pack(writer)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(18)
    }
}
