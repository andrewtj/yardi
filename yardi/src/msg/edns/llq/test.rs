use super::{Llq, VERSION};
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn marshal() {
    assert_eq!(super::LlqOpCode::Setup as u16, 1);
    let opcode_cases_iter = (1u16..=3)
        .into_iter()
        .map(|n| (n, false))
        .chain(Some((0, true)));
    let err_cases_iter = (0u16..=6)
        .into_iter()
        .map(|n| (n, false))
        .chain(Some((7, true)));
    let cases = opcode_cases_iter.flat_map(|opcode_case| {
        err_cases_iter
            .clone()
            .map(move |err_case| (opcode_case, err_case))
    });
    for ((opcode, bad_opcode), (err, bad_err)) in cases {
        let unpack_should_fail = bad_opcode || bad_err;
        let mut w1_buf = [0u8; 0xFF];
        let mut w1 = Writer::new(&mut w1_buf[..]);
        (VERSION, opcode, err, !0u64, 0u32)
            .pack(&mut w1)
            .expect("write case");
        let w1 = w1.written();
        let mut r = Reader::new(w1);
        match Llq::unpack(&mut r) {
            Ok(ref t) if unpack_should_fail => {
                panic!(
                    "unpacked a bad opt: {}({})/{}({}) {:?}",
                    opcode, bad_opcode, err, bad_err, t
                );
            }
            Ok(a) => {
                assert_eq!(a, Llq::unpack(r.set_index(0)).expect("unpack"));
                assert_eq!(a.packed_len(), Ok(w1.len()));
                let mut w2_buf = [0u8; 0xFF];
                let mut w2 = Writer::new(&mut w2_buf[..]);
                a.pack(&mut w2).expect("pack");
                assert_eq!(w1, w2.written());
            }
            Err(_) if unpack_should_fail => {
                assert!(Llq::unpack(r.set_index(0)).is_err());
            }
            Err(_) => {
                panic!(
                    "failed to unpack a good opt {}({})/{}({})",
                    opcode, bad_opcode, err, bad_err
                );
            }
        }
    }
}
