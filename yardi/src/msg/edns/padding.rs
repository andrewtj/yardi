use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};
/// Support for the [Padding Extended Option][1].
///
/// [1]: https://tools.ietf.org/html/rfc7830

/// Padding.
///
/// Reference:
/// <a href="https://tools.ietf.org/html/rfc7830">RFC 7830: The EDNS(0) Padding Option</a>

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Padding<T> {
    pub bytes: T,
}

impl<T> Opt for Padding<T> {
    const CODE: OptCode = OptCode::PADDING;
}

impl<T: AsRef<[u8]>> Pack for Padding<T> {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        let bytes = self.bytes.as_ref();
        if bytes.len() > 0xFFFF {
            return Err(PackError::value_large());
        }
        writer.write_bytes(bytes)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        match self.bytes.as_ref().len() {
            len @ 0..=0xFFFF => Ok(len),
            _ => Err(PackError::value_large()),
        }
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for Padding<T> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let initial_index = reader.index();
        let bytes = reader.get_bytes_rest()?;
        if bytes.len() > 0xFFFF {
            return Err(UnpackError::value_large_at(initial_index));
        }
        let bytes = bytes.into();
        Ok(Self { bytes })
    }
}
