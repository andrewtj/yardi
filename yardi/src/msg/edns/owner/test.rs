use super::{Owner, VERSION};
use crate::wire::{Pack, Reader, Unpack, Writer};
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

const PRIMARY_MAC: [u8; 6] = [0xAA; 6];
const WAKEUP_MAC: [u8; 6] = [0xBB; 6];
const PASSWORD4: [u8; 4] = [0xCC; 4];
const PASSWORD6: [u8; 6] = [0xDD; 6];

macro_rules! concat_bytes {
    ($($bytes:expr),+) => {{
        let len = 0 $(+ { $bytes.len() })+;
        let mut v = <Vec<u8>>::with_capacity(len);
        $({
            let t = $bytes;
            v.extend_from_slice(t.as_ref());
        })+
        assert_eq!(v.len(), len);
        v
    }}
}

macro_rules! test_case {
    ($name:ident, $struct:expr, $wire:expr) => {
        #[test]
        fn $name() {
            let wire = $wire;
            let expect = $struct;

            let mut bad_wire = vec![0; wire.len() + 1];
            bad_wire[..wire.len()].copy_from_slice(&wire[..]);

            let mut r = Reader::new(&wire);
            assert_eq!(expect.packed_len(), Ok(wire.len()));
            assert_eq!(expect, Owner::unpack(&mut r).expect("unpack"));
            let mut buf = [0u8; 0xFF];
            let mut w = Writer::new(&mut buf[..]);
            expect.pack(&mut w).expect("pack");
            assert_eq!(&wire[..], w.written());

            assert!(Owner::unpack(&mut Reader::new(&bad_wire)).is_err());
            bad_wire.pop();
            bad_wire[0] += 1;
            assert!(Owner::unpack(&mut Reader::new(&bad_wire)).is_err());
            bad_wire[0] -= 1;
            bad_wire.pop();
            assert!(Owner::unpack(&mut Reader::new(&bad_wire)).is_err());
        }
    };
}

test_case!(
    full,
    Owner::Full {
        seq: 1,
        primary_mac: PRIMARY_MAC,
        wakeup_mac: WAKEUP_MAC,
        password: PASSWORD6,
    },
    concat_bytes!([VERSION, 1], PRIMARY_MAC, WAKEUP_MAC, PASSWORD6)
);

test_case!(
    short_password,
    Owner::ShortPassword {
        seq: 2,
        primary_mac: PRIMARY_MAC,
        wakeup_mac: WAKEUP_MAC,
        password: PASSWORD4,
    },
    concat_bytes!([VERSION, 2], PRIMARY_MAC, WAKEUP_MAC, PASSWORD4)
);

test_case!(
    no_password,
    Owner::NoPassword {
        seq: 3,
        primary_mac: PRIMARY_MAC,
        wakeup_mac: WAKEUP_MAC,
    },
    concat_bytes!([VERSION, 3], PRIMARY_MAC, WAKEUP_MAC)
);

test_case!(
    single_mac,
    Owner::SingleMac {
        seq: 4,
        mac: PRIMARY_MAC,
    },
    concat_bytes!([VERSION, 4], PRIMARY_MAC)
);
