//! Support for the [Owner Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/draft-cheshire-edns0-owner-option

#[cfg(test)]
mod test;

use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};

const VERSION: u8 = 0;

#[doc = "Owner.\n\n"]
#[doc = "Reference: "]
#[doc = "<https://tools.ietf.org/html/draft-cheshire-edns0-owner-option>"]
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum Owner {
    /// Full Owner
    Full {
        /// Sequence number.
        seq: u8,
        /// Primary MAC identifier.
        primary_mac: [u8; 6],
        /// MAC to use in the wakeup packet.
        wakeup_mac: [u8; 6],
        /// Six byte "password" to use in the wakeup packet.
        password: [u8; 6],
    },
    /// Short password
    ShortPassword {
        /// Sequence number.
        seq: u8,
        /// Primary MAC identifier.
        primary_mac: [u8; 6],
        /// MAC to use in the wakeup packet.
        wakeup_mac: [u8; 6],
        /// Four byte "password" to use in the wakeup packet.
        password: [u8; 4],
    },
    /// No password
    NoPassword {
        /// Sequence number.
        seq: u8,
        /// Primary MAC identifier.
        primary_mac: [u8; 6],
        /// MAC to use in the wakeup packet.
        wakeup_mac: [u8; 6],
    },
    /// Single MAC
    SingleMac {
        /// Sequence number.
        seq: u8,
        /// MAC identifier.
        mac: [u8; 6],
    },
}

impl Opt for Owner {
    const CODE: OptCode = OptCode::OWNER;
}

impl Pack for Owner {
    fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        VERSION.pack(w)?;
        match *self {
            Owner::Full {
                seq,
                ref primary_mac,
                ref wakeup_mac,
                ref password,
            } => {
                seq.pack(w)?;
                w.write_bytes(&primary_mac[..])?;
                w.write_bytes(&wakeup_mac[..])?;
                w.write_bytes(&password[..])
            }
            Owner::ShortPassword {
                seq,
                ref primary_mac,
                ref wakeup_mac,
                ref password,
            } => {
                seq.pack(w)?;
                w.write_bytes(&primary_mac[..])?;
                w.write_bytes(&wakeup_mac[..])?;
                w.write_bytes(&password[..])
            }
            Owner::NoPassword {
                seq,
                ref primary_mac,
                ref wakeup_mac,
            } => {
                seq.pack(w)?;
                w.write_bytes(&primary_mac[..])?;
                w.write_bytes(&wakeup_mac[..])
            }
            Owner::SingleMac { seq, ref mac } => {
                seq.pack(w)?;
                w.write_bytes(&mac[..])
            }
        }
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(1 + match *self {
            Owner::Full { .. } => 1 + (3 * 6),
            Owner::ShortPassword { .. } => 1 + (2 * 6) + 4,
            Owner::NoPassword { .. } => 1 + (2 * 6),
            Owner::SingleMac { .. } => 1 + 6,
        })
    }
}

impl<'a> Unpack<'a> for Owner {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let initial_index = reader.index();
        u8::unpack(reader).and_then(|n| {
            if n != VERSION {
                Err(UnpackError::value_unexpected_version_at(reader.index() - 1))
            } else {
                Ok(())
            }
        })?;
        let seq = reader.get_byte()?;
        match reader.unread() {
            18 => Ok(Owner::Full {
                seq,
                primary_mac: Unpack::unpack(reader)?,
                wakeup_mac: Unpack::unpack(reader)?,
                password: Unpack::unpack(reader)?,
            }),
            16 => Ok(Owner::ShortPassword {
                seq,
                primary_mac: Unpack::unpack(reader)?,
                wakeup_mac: Unpack::unpack(reader)?,
                password: Unpack::unpack(reader)?,
            }),
            12 => Ok(Owner::NoPassword {
                seq,
                primary_mac: Unpack::unpack(reader)?,
                wakeup_mac: Unpack::unpack(reader)?,
            }),
            6 => Ok(Owner::SingleMac {
                seq,
                mac: Unpack::unpack(reader)?,
            }),
            _ => Err(UnpackError::value_invalid_at(initial_index)),
        }
    }
}
