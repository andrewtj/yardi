//! Support for the [DHU Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc6975

#[cfg(test)]
mod test;

use super::{Opt, OptCode};
use crate::{
    rdata::ds::DsDigestAlg,
    wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer},
};

#[doc = "DHU.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc6975\">RFC 6975: Signaling Cryptographic Algorithm Understanding in DNS Security Extensions (DNSSEC)</a>"]
#[derive(Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct Dhu<T> {
    /// Delegation Signer (DS) Hash Algorithms a client wants to signal it understands.
    /// Reserved algorithms are packed and unpacked contrary to the RFC.
    #[yardi(
        pack_bound = "T: AsRef<[DsDigestAlg]>",
        pack = "pack",
        packed_len = "packed_len",
        unpack_bound = "T: From<&'yr [DsDigestAlg]>",
        unpack = "unpack"
    )]
    pub understood: T,
}

impl<T> Opt for Dhu<T> {
    const CODE: OptCode = OptCode::DHU;
}

fn pack(s: impl AsRef<[DsDigestAlg]>, w: &mut Writer<'_>) -> Result<(), PackError> {
    let s = s.as_ref();
    if s.len() > 0xFFFF {
        Err(PackError::value_large())
    } else {
        let s = unsafe { &*(s as *const [DsDigestAlg] as *const [u8]) };
        w.write_bytes(s)
    }
}

fn packed_len(s: impl AsRef<[DsDigestAlg]>) -> Result<usize, PackError> {
    let s = s.as_ref();
    match s.as_ref().len() {
        len @ 0..=0xFFFF => Ok(len),
        _ => Err(PackError::value_large()),
    }
}

fn unpack<'a, T>(r: &mut Reader<'a>) -> Result<T, UnpackError>
where
    T: From<&'a [DsDigestAlg]>,
{
    let s = r.get_bytes_rest()?;
    if s.len() > 0xFFFF {
        Err(UnpackError::value_large())
    } else {
        let s = unsafe { &*(s as *const [u8] as *const [DsDigestAlg]) };
        Ok(s.into())
    }
}
