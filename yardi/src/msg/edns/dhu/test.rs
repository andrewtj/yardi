use super::{Dhu, DsDigestAlg};
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn size_assert() {
    assert_eq!(size_of::<u8>(), size_of::<DsDigestAlg>())
}

#[test]
fn wire_marshal() {
    let input: &[u8] = &[DsDigestAlg::SHA_256.0];
    let mut reader = Reader::new(input);
    let dhu = <Dhu<&[_]>>::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    assert_eq!(&dhu.understood, &[DsDigestAlg::SHA_256]);
    assert_eq!(Ok(1), dhu.packed_len());
    let mut buf = [0u8; 1];
    let mut w = Writer::new(&mut buf[..]);
    dhu.pack(&mut w).expect("pack");
    assert_eq!(w.written(), input);
}

#[test]
fn size_limit() {
    let long_bytes: &[u8] = &[DsDigestAlg::SHA_256.0; 0xFFFF + 1][..];
    assert!(<Dhu<&[_]>>::unpack(&mut Reader::new(long_bytes)).is_err());
}
