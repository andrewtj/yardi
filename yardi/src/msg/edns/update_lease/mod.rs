//! Support for the [UL Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/draft-sekar-dns-ul

#[cfg(test)]
mod test;

use super::{Opt, OptCode};

#[doc = "UL.\n\n"]
#[doc = "Reference: "]
#[doc = "<https://tools.ietf.org/html/draft-sekar-dns-ul>"]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct UpdateLease {
    /// Desired lease (request) or granted lease (response) in seconds.
    pub lease: u32,
    /// Optional desired or granted lease for KEY records in seconds.
    pub key_lease: Option<u32>,
}

impl Opt for UpdateLease {
    const CODE: OptCode = OptCode::UPDATE_LEASE;
}
