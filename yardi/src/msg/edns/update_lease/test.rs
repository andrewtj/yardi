use super::UpdateLease;
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn marshal_lease_only() {
    let input: &[u8] = &[0xFF; 4];
    let mut reader = Reader::new(input.into());
    let ul = UpdateLease::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    assert_eq!(ul.lease, !0u32);
    assert!(ul.key_lease.is_none());
    assert_eq!(Ok(4), ul.packed_len());
    let mut buf = [0u8; 4];
    let mut w = Writer::new(&mut buf[..]);
    ul.pack(&mut w).expect("pack");
    assert_eq!(w.written(), input);
}

#[test]
fn marshal_lease_and_key_lease() {
    let input: &[u8] = &[0xFF; 8];
    let mut reader = Reader::new(input.into());
    let ul = UpdateLease::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    assert_eq!(ul.lease, !0u32);
    assert_eq!(ul.key_lease, Some(!0u32));
    assert_eq!(Ok(8), ul.packed_len());
    let mut buf = [0u8; 8];
    let mut w = Writer::new(&mut buf[..]);
    ul.pack(&mut w).expect("pack");
    assert_eq!(w.written(), input);
}
