//! Support for the [NSID Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc5001
use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};

#[doc = "NSID.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc5001\">RFC 5001: DNS Name Server Identifier (NSID) Option</a>"]
#[derive(Clone, Debug, PartialEq, Eq, Hash)]
pub struct Nsid<T> {
    /// Opaque binary data.
    pub payload: T,
}

impl<T> Opt for Nsid<T> {
    const CODE: OptCode = OptCode::NSID;
}

impl<T: AsRef<[u8]>> Pack for Nsid<T> {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        let payload = self.payload.as_ref();
        if payload.len() > 0xFFFF {
            return Err(PackError::value_large());
        }
        writer.write_bytes(payload)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let payload = self.payload.as_ref();
        if payload.len() > 0xFFFF {
            return Err(PackError::value_large());
        }
        Ok(payload.len())
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for Nsid<T> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        // TODO: is initial_index used consistently in UnpackError?
        let initial_index = reader.index();
        let bytes = reader.get_bytes_rest()?;
        if bytes.len() > 0xFFFF {
            return Err(UnpackError::value_large_at(initial_index));
        }
        let payload = bytes.into();
        Ok(Nsid { payload })
    }
}
