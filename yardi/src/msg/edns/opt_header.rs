use super::EdnsFlags;

/// Requestor's UDP payload size, extended RCODE bits, and flags.
///
/// This represents the parts of an OPT RR that repurpose the RR Class and
/// TTL fields. See the [wire format section of RFC 6891][1] for context.
///
/// [1]: https://tools.ietf.org/html/rfc6891#section-6.1.2
#[derive(Clone, Copy, Debug, PartialEq, Eq, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct OptHeader {
    /// Requestor's UDP payload size
    pub udp_payload_size: u16,
    /// Extended RCODE bits.
    ///
    /// See also the `ExtRcode` type.
    pub rcode_bits: u8,
    /// Implementation level of the sender. See [RFC 6891 Section 6.1.3][1]
    /// for correct use of this field.
    ///
    /// [1]: https://tools.ietf.org/html/rfc6891#section-6.1.3
    pub version: u8,
    /// Extended Flags.
    pub flags: EdnsFlags,
}

impl Default for OptHeader {
    fn default() -> Self {
        OptHeader {
            udp_payload_size: 512,
            rcode_bits: 0,
            version: 0,
            flags: EdnsFlags::default(),
        }
    }
}
