mod generated;

use core::fmt::{self, Debug};

/// EDNS Option Code.
#[derive(Clone, Copy, Hash, Eq, PartialEq, Pack, Unpack, UnpackedLen, WireOrd)]
#[yardi(crate = "crate")]
pub struct OptCode(pub u16);

impl Debug for OptCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for &(o, m) in generated::MNEMONICS {
            if o == *self {
                return write!(f, "OptCode({})", m);
            }
        }
        write!(f, "OptCode({})", self.0)
    }
}
