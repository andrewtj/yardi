mod generated;

use crate::wire::{Reader, UnpackError};
use alloc::borrow::Cow;
#[cfg(not(feature = "std"))]
use alloc::string::ToString;
use core::fmt::{self, Debug};

const MAX_EXTRA_TEXT_LEN: usize = 0xFFFF - 12 - 1 - 2 - 2 - 4 - 2 - 2 - 2 - 2;

// TODO: Implement Ord?
#[derive(Clone, Copy, Eq, Ord, Hash, PartialEq, PartialOrd, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct InfoCode(pub u16);

impl Debug for InfoCode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: all impls like this should be the same
        if let Some((_, s)) = generated::MNEMONICS.iter().find(|(ic, _)| ic == self) {
            f.write_str(s)
        } else {
            f.write_fmt(format_args!("INFOCODE{}", self.0))
        }
    }
}

// TODO: do we have the same derives everywhere?
#[derive(Clone, Debug, Eq, Ord, Hash, PartialEq, PartialOrd, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct Ede<'a> {
    pub info_code: InfoCode,
    #[yardi(
        pack = "|m, w| crate::datatypes::bytes::pack(m.as_bytes(), w, 0, MAX_EXTRA_TEXT_LEN)",
        packed_len = "|m| crate::datatypes::bytes::packed_len(m.as_bytes(), 0, MAX_EXTRA_TEXT_LEN)",
        unpack = "unpack_extra_text",
        unpacked_len = "unpacked_len_extra_text"
    )]
    pub extra_text: Cow<'a, str>,
}

fn unpack_extra_text(r: &mut Reader<'_>) -> Result<Cow<'static, str>, UnpackError> {
    let b = r.get_bytes_rest()?;
    if b.len() > MAX_EXTRA_TEXT_LEN {
        return Err(UnpackError::value_large());
    }
    core::str::from_utf8(b)
        .map(|s| Cow::Owned(s.to_string()))
        .map_err(|_| UnpackError::value_invalid())
}
