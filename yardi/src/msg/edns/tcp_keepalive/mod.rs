//! Support for the [TCP Keepalive Extended Opt][1].
//!
//! [1]: https://tools.ietf.org/html/rfc7828

#[cfg(test)]
mod test;

use core::time::Duration;

use super::{Opt, OptCode};

const NANOS_PER_HUNDREDMILLI: u64 = 100_000_000;
const HUNDREDMILLI_PER_SEC: u64 = 10;

#[doc = "TCP Keepalive.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc7828\">RFC 7828: The edns-tcp-keepalive EDNS0 Option</a>"]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct TcpKeepalive {
    /// Idle timeout value for the TCP connection in units of 100 milliseconds.
    pub timeout: u16,
}

impl Opt for TcpKeepalive {
    const CODE: OptCode = OptCode::TCP_KEEPALIVE;
}

/// A `Duration` larger than the maximum value (6,553,500 milliseconds) will
/// be truncated to the maximum value.
impl From<Duration> for TcpKeepalive {
    fn from(duration: Duration) -> TcpKeepalive {
        let nanos_part_in_hm = u64::from(duration.subsec_nanos()) / NANOS_PER_HUNDREDMILLI;
        let secs_part_in_hm = duration.as_secs().saturating_mul(HUNDREDMILLI_PER_SEC);
        let hm = secs_part_in_hm.saturating_add(nanos_part_in_hm);
        if hm > 0xFFFF {
            TcpKeepalive { timeout: 0xFFFF }
        } else {
            TcpKeepalive { timeout: hm as u16 }
        }
    }
}

impl From<TcpKeepalive> for Duration {
    fn from(tcp_keepalive: TcpKeepalive) -> Duration {
        Duration::from_millis(u64::from(tcp_keepalive.timeout) * 100)
    }
}
