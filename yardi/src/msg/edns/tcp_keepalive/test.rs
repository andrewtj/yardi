use core::time::Duration;

use super::TcpKeepalive;

#[test]
fn convert() {
    for &timeout in &[0, 0xF, 0xFF, 0xFFF, 0xFFFF] {
        let initial = TcpKeepalive { timeout };
        let duration = Duration::from(initial);
        let roundtripped = TcpKeepalive::from(duration);
        assert_eq!(roundtripped, initial);
    }
}

#[test]
fn saturating_conversion() {
    let over_max = Duration::from_millis((0xFFFF * 100) + 1);
    assert_eq!(TcpKeepalive { timeout: 0xFFFF }, over_max.into());
}
