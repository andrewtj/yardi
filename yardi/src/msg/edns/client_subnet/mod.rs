//! Support for the [Client Subnet Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc7871

#[cfg(test)]
mod test;

use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};

// TODO: Better representation?
#[doc = "Client Subnet.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc7871\">RFC 7871: Client Subnet in DNS Queries</a>"]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct ClientSubnet<T> {
    /// Address Family Number
    pub family: u16,
    /// The leftmost number of significant bits of `addr`.
    pub source_prefix_len: u8,
    /// The leftmost number of significant bits of `addr` that the response covers.
    pub scope_prefix_len: u8,
    /// Contains an IPv4 or IPv6 address (dependant on `family`) truncated at
    /// `source_prefix_len` bits and padded with zero bits to the last octet.
    pub addr: T,
}

impl<T> Opt for ClientSubnet<T> {
    const CODE: OptCode = OptCode::CLIENT_SUBNET;
}

impl<T: AsRef<[u8]>> Pack for ClientSubnet<T> {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        _ = self.packed_len()?;
        self.family.pack(dst)?;
        self.source_prefix_len.pack(dst)?;
        self.scope_prefix_len.pack(dst)?;
        dst.write_bytes(self.addr.as_ref())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let addr_len = (self.source_prefix_len as usize + 7) / 8;
        // TODO: is this necessary? and correct?
        let addr = self.addr.as_ref();
        if addr.len() != addr_len {
            return Err(PackError::value_invalid());
        }
        if self.scope_prefix_len as usize > addr_len {
            return Err(PackError::value_invalid());
        }
        Ok(2 + 1 + 1 + addr_len)
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for ClientSubnet<T> {
    // TODO: check buffer is empty after unpack?
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let family = u16::unpack(reader)?;
        let source_prefix_len = reader.get_byte()?;
        let scope_prefix_len = reader.get_byte()?;
        let addr_len = (source_prefix_len as usize + 7) / 8;
        let addr = reader.get_bytes(addr_len)?.into();
        Ok(ClientSubnet {
            family,
            source_prefix_len,
            scope_prefix_len,
            addr,
        })
    }
}
