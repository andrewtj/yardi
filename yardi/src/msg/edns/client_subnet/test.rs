use super::ClientSubnet;
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn marshal() {
    for source_prefix_len in 15..=25 {
        let byte_len = (source_prefix_len as usize + 7) / 8;
        let mut cs = ClientSubnet {
            family: 0,
            source_prefix_len,
            scope_prefix_len: 0,
            addr: vec![0xAA; byte_len],
        };
        assert_eq!(cs.addr.len(), byte_len);
        let mut buf = [0u8; 0xFF];
        let mut w = Writer::new(&mut buf[..]);
        cs.pack(&mut w).expect("pack");
        assert_eq!(Ok(w.written().len()), cs.packed_len());
        let mut r = Reader::new(w.written());
        let csu = ClientSubnet::unpack(&mut r).expect("unpack");
        assert!(r.is_empty());
        assert_eq!(csu, cs);
        cs.source_prefix_len += 8;
        assert!(cs.packed_len().is_err());
        let mut buf = [0u8; 0xFF];
        let mut w = Writer::new(&mut buf[..]);
        assert!(cs.pack(&mut w).is_err());
        w.truncate(0).expect("truncate");
        cs.source_prefix_len -= 16;
        assert!(cs.packed_len().is_err());
        assert!(cs.pack(&mut w).is_err());
    }
}
