//! Support for the [Key Tag Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc8145

#[cfg(test)]
mod test;

#[cfg(not(feature = "std"))]
use alloc::vec::Vec;
use core::{
    fmt::{self, Debug},
    hash::Hash,
};

use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};

const MAX_TAGS: usize = 0xFFFF / 2;

#[doc = "Key Tag with storage in native byte order.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc8145\">RFC 8145: Signaling Trust Anchor Knowledge in DNS Security Extensions (DNSSEC)</a>"]
#[derive(Clone, Default, Hash, PartialEq, Eq)]
pub struct KeyTagNative<T> {
    pub tags: T,
}

impl<T> Opt for KeyTagNative<T> {
    const CODE: OptCode = OptCode::KEY_TAG;
}

impl<T: AsRef<[u16]>> KeyTagNative<T> {
    pub fn iter(&self) -> impl Iterator<Item = u16> + '_ {
        self.tags.as_ref().iter().copied()
    }
}

impl<T: AsRef<[u16]>> Debug for KeyTagNative<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl KeyTagNative<Vec<u16>> {
    /// Insert a tag if it is not already present.
    pub fn insert(&mut self, tag: u16) {
        if self.tags.iter().all(|existing| *existing != tag) {
            self.push(tag);
        }
    }
    /// Append a tag without checking if it is already present.
    pub fn push(&mut self, tag: u16) {
        self.tags.reserve_exact(1);
        self.tags.push(tag);
    }
}

impl<T: AsRef<[u16]>> Pack for KeyTagNative<T> {
    fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        let s = self.tags.as_ref();
        if s.len() > MAX_TAGS {
            return Err(PackError::value_large());
        }
        for kt in s {
            kt.pack(w)?;
        }
        Ok(())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let s = self.tags.as_ref();
        match s.len() {
            len @ 0..=MAX_TAGS => Ok(len * 2),
            _ => Err(PackError::value_large()),
        }
    }
}

impl<T: for<'a> From<&'a [u16]>> Unpack<'_> for KeyTagNative<T> {
    fn unpack(r: &mut Reader<'_>) -> Result<Self, UnpackError> {
        let s = r.get_bytes_rest()?;
        match s.len() {
            len if len % 2 != 0 => Err(UnpackError::value_invalid()),
            len if len > 0xFFFF => Err(UnpackError::value_large()),
            len => {
                let tags = &mut [0u16; MAX_TAGS];
                let tags = &mut tags[..len / 2];
                for (bytes, tag) in s.chunks_exact(2).zip(tags.iter_mut()) {
                    *tag = u16::from_be_bytes([bytes[0], bytes[1]]);
                }
                let tags = tags[..].into();
                Ok(Self { tags })
            }
        }
    }
}

#[doc = "Key Tag with storage in network byte order.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc8145\">RFC 8145: Signaling Trust Anchor Knowledge in DNS Security Extensions (DNSSEC)</a>"]
#[derive(Clone, Default, Hash, PartialEq, Eq)]
pub struct KeyTagNetwork<T> {
    pub tags: T,
}

impl<T> Opt for KeyTagNetwork<T> {
    const CODE: OptCode = OptCode::KEY_TAG;
}

impl<T: AsRef<[u8]>> KeyTagNetwork<T> {
    pub fn iter(&self) -> impl Iterator<Item = u16> + '_ {
        self.tags
            .as_ref()
            .chunks_exact(2)
            .map(|bytes| u16::from_be_bytes([bytes[0], bytes[1]]))
    }
}

impl<T: AsRef<[u8]>> Debug for KeyTagNetwork<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl KeyTagNetwork<Vec<u8>> {
    /// Insert a tag if it is not already present.
    pub fn insert(&mut self, tag: u16) {
        let tag = tag.to_be_bytes();
        if self.tags.chunks_exact(2).all(|existing| existing != tag) {
            self.tags.reserve_exact(2);
            self.tags.extend_from_slice(&tag);
        }
    }
    /// Append a tag without checking if it is already present.
    pub fn push(&mut self, tag: u16) {
        self.tags.reserve_exact(2);
        self.tags.extend_from_slice(&tag.to_be_bytes());
    }
}

impl<T: AsRef<[u8]>> Pack for KeyTagNetwork<T> {
    fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        let s = self.tags.as_ref();
        match s.len() {
            len if len % 2 != 0 => Err(PackError::value_invalid()),
            len if len > 0xFFFF => Err(PackError::value_large()),
            _ => w.write_bytes(s),
        }
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let s = self.tags.as_ref();
        match s.len() {
            len if len % 2 != 0 => Err(PackError::value_invalid()),
            len if len > 0xFFFF => Err(PackError::value_large()),
            len => Ok(len),
        }
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for KeyTagNetwork<T> {
    fn unpack(r: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let s = r.get_bytes_rest()?;
        match s.len() {
            len if len % 2 != 0 => Err(UnpackError::value_invalid()),
            len if len > 0xFFFF => Err(UnpackError::value_large()),
            _ => {
                let tags = s.into();
                Ok(Self { tags })
            }
        }
    }
}
