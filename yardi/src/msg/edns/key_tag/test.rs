use super::{KeyTagNative, KeyTagNetwork};
use crate::wire::{Reader, Unpack};

#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

#[test]
fn unpack_err_odd_len() {
    let mut r = Reader::new(b"12345");
    assert!(<KeyTagNative<Vec<u16>>>::unpack(&mut r).is_err());
    assert!(<KeyTagNetwork<&[u8]>>::unpack(r.set_index(0)).is_err());
}

#[test]
fn unpack_ok() {
    let r = &mut Reader::new(b"1234");
    let nat = <KeyTagNative<Vec<u16>>>::unpack(r).unwrap();
    let net = <KeyTagNetwork<&[u8]>>::unpack(r.set_index(0)).unwrap();
    let mut nat_iter = nat.iter();
    let mut net_iter = net.iter();
    loop {
        let nat_item = nat_iter.next();
        let net_item = net_iter.next();
        assert_eq!(nat_item, net_item);
        if net_item.is_none() {
            break;
        }
    }
}
