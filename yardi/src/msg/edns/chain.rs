//! Support for the [CHAIN Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc7901
use super::{Opt, OptCode};
use crate::datatypes::Name;
use crate::wire::{Pack, PackError, Writer};

#[doc = "CHAIN.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc7901\">RFC 7901: CHAIN Query Requests in DNS</a>"]
#[derive(Clone, Debug, PartialEq, Eq, Unpack)]
#[yardi(crate = "crate")]
pub struct Chain {
    /// The start part point of the requested chain of trust which ends at the
    /// containing message's query.
    pub closest_trust_point: Name,
}

impl Opt for Chain {
    const CODE: OptCode = OptCode::CHAIN;
}

impl Pack for Chain {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        self.closest_trust_point.pack(writer.plain().as_mut())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        self.closest_trust_point.packed_len()
    }
}
