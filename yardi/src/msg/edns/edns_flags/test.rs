use super::EdnsFlags;

#[test]
fn flags_contains() {
    assert!((EdnsFlags::FLAG_0 | EdnsFlags::FLAG_1).contains(EdnsFlags::FLAG_0));
    assert!(
        (EdnsFlags::FLAG_0 | EdnsFlags::FLAG_1).contains(EdnsFlags::FLAG_0 | EdnsFlags::FLAG_1,)
    );
    assert!(!EdnsFlags::FLAG_0.contains(EdnsFlags::FLAG_0 | EdnsFlags::FLAG_1));
}

#[test]
fn flags_debug() {
    assert_eq!("EdnsFlags(0)", format!("{:?}", EdnsFlags(0)));
    assert_eq!("EdnsFlags(DO)", format!("{:?}", EdnsFlags::DO));
    assert_eq!(
        "EdnsFlags(DO|1)",
        format!("{:?}", EdnsFlags::DO | EdnsFlags::FLAG_1)
    );
}
