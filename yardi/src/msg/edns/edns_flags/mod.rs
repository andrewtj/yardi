/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
#[cfg(test)]
mod test;
#[doc = "EDNS Flags."]
#[derive(
    Clone,
    Copy,
    Default,
    Hash,
    Eq,
    PartialEq,
    Ord,
    PartialOrd,
    Parse,
    Present,
    Pack,
    Unpack,
    UnpackedLen,
    WireOrd,
)]
#[yardi(crate = "crate")]
pub struct EdnsFlags(pub u16);
impl EdnsFlags {
    #[doc = r" Returns true if all the flags in other are set in self."]
    pub fn contains(self, other: Self) -> bool {
        (self & other) == other
    }
}
impl core::fmt::Display for EdnsFlags {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        core::fmt::Display::fmt(&self.0, f)
    }
}
impl core::fmt::Debug for EdnsFlags {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str(stringify!(EdnsFlags))?;
        f.write_str("(")?;
        if self.0 == 0 {
            return f.write_str("0)");
        }
        let mut i = !(!0 >> 1);
        let mut mnemonics = Self::MNEMONICS;
        let mut first = true;
        while i != 0 {
            if i & self.0 != 0 {
                if first {
                    first = false;
                } else {
                    f.write_str("|")?;
                }
                while mnemonics.first().map(|&(f, _)| f.0 > i).unwrap_or(false) {
                    mnemonics = &mnemonics[1..];
                }
                match mnemonics.first() {
                    Some(&(EdnsFlags(n), s)) if n == i => f.write_str(s)?,
                    Some(_) | None => write!(f, "{}", i.leading_zeros())?,
                }
            }
            i >>= 1;
        }
        f.write_str(")")
    }
}
impl core::ops::BitAnd for EdnsFlags {
    type Output = Self;
    fn bitand(self, rhs: Self) -> Self {
        EdnsFlags(self.0 & rhs.0)
    }
}
impl core::ops::BitAndAssign for EdnsFlags {
    fn bitand_assign(&mut self, rhs: Self) {
        self.0 &= rhs.0
    }
}
impl core::ops::BitOr for EdnsFlags {
    type Output = Self;
    fn bitor(self, rhs: Self) -> Self {
        EdnsFlags(self.0 | rhs.0)
    }
}
impl core::ops::BitOrAssign for EdnsFlags {
    fn bitor_assign(&mut self, rhs: Self) {
        self.0 |= rhs.0;
    }
}
impl core::ops::BitXor for EdnsFlags {
    type Output = Self;
    fn bitxor(self, rhs: Self) -> Self {
        EdnsFlags(self.0 ^ rhs.0)
    }
}
impl core::ops::BitXorAssign for EdnsFlags {
    fn bitxor_assign(&mut self, rhs: Self) {
        self.0 ^= rhs.0
    }
}
impl core::ops::Sub for EdnsFlags {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        EdnsFlags(self.0 & !rhs.0)
    }
}
impl core::ops::SubAssign for EdnsFlags {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 &= !rhs.0;
    }
}
impl core::ops::Not for EdnsFlags {
    type Output = Self;
    fn not(self) -> Self {
        EdnsFlags(!self.0)
    }
}
impl From<u16> for EdnsFlags {
    fn from(flags: u16) -> Self {
        EdnsFlags(flags)
    }
}
impl From<EdnsFlags> for u16 {
    fn from(EdnsFlags(flags): EdnsFlags) -> Self {
        flags
    }
}
impl EdnsFlags {
    #[doc = "DO - DNSSEC answer OK\n\n"]
    #[doc = "References:"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc3225\">RFC 3225: Indicating Resolver Support of DNSSEC</a>"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc4035\">RFC 4035: Protocol Modifications for the DNS Security Extensions</a>"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc6840\">RFC 6840: Clarifications and Implementation Notes for DNS Security (DNSSEC)</a> (Errata: <a href=\"https://www.rfc-editor.org/errata_search.php?rfc=6840&eid=4928\">4928</a>)"]
    pub const DO: EdnsFlags = EdnsFlags::FLAG_0;
}
impl EdnsFlags {
    #[doc = "Flag 0"]
    pub const FLAG_0: EdnsFlags = EdnsFlags(32768);
    #[doc = "Flag 1"]
    pub const FLAG_1: EdnsFlags = EdnsFlags(16384);
    #[doc = "Flag 2"]
    pub const FLAG_2: EdnsFlags = EdnsFlags(8192);
    #[doc = "Flag 3"]
    pub const FLAG_3: EdnsFlags = EdnsFlags(4096);
    #[doc = "Flag 4"]
    pub const FLAG_4: EdnsFlags = EdnsFlags(2048);
    #[doc = "Flag 5"]
    pub const FLAG_5: EdnsFlags = EdnsFlags(1024);
    #[doc = "Flag 6"]
    pub const FLAG_6: EdnsFlags = EdnsFlags(512);
    #[doc = "Flag 7"]
    pub const FLAG_7: EdnsFlags = EdnsFlags(256);
    #[doc = "Flag 8"]
    pub const FLAG_8: EdnsFlags = EdnsFlags(128);
    #[doc = "Flag 9"]
    pub const FLAG_9: EdnsFlags = EdnsFlags(64);
    #[doc = "Flag 10"]
    pub const FLAG_10: EdnsFlags = EdnsFlags(32);
    #[doc = "Flag 11"]
    pub const FLAG_11: EdnsFlags = EdnsFlags(16);
    #[doc = "Flag 12"]
    pub const FLAG_12: EdnsFlags = EdnsFlags(8);
    #[doc = "Flag 13"]
    pub const FLAG_13: EdnsFlags = EdnsFlags(4);
    #[doc = "Flag 14"]
    pub const FLAG_14: EdnsFlags = EdnsFlags(2);
    #[doc = "Flag 15"]
    pub const FLAG_15: EdnsFlags = EdnsFlags(1);
}
impl EdnsFlags {
    const MNEMONICS: &'static [(EdnsFlags, &'static str)] = &[(EdnsFlags::DO, "DO")];
}
/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
