//! Support for the [N3U Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc6975

#[cfg(test)]
mod test;

use super::{Opt, OptCode};
use crate::{
    rdata::nsec3::Nsec3HashAlg,
    wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer},
};

#[doc = "N3U.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc6975\">RFC 6975: Signaling Cryptographic Algorithm Understanding in DNS Security Extensions (DNSSEC)</a>"]
#[derive(Debug, Clone, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct N3u<T> {
    /// NSEC3 Hash Algorithms a client wants to signal it understands.
    /// Reserved algorithms are packed and unpacked contrary to the RFC.
    #[yardi(
        pack_bound = "T: AsRef<[Nsec3HashAlg]>",
        pack = "pack",
        packed_len = "packed_len",
        unpack_bound = "T: From<&'yr [Nsec3HashAlg]>",
        unpack = "unpack"
    )]
    pub understood: T,
}

impl<T> Opt for N3u<T> {
    const CODE: OptCode = OptCode::N3U;
}

fn pack(s: impl AsRef<[Nsec3HashAlg]>, w: &mut Writer<'_>) -> Result<(), PackError> {
    let s = s.as_ref();
    if s.len() > 0xFFFF {
        Err(PackError::value_large())
    } else {
        let s = unsafe { &*(s as *const [Nsec3HashAlg] as *const [u8]) };
        w.write_bytes(s)
    }
}

fn packed_len(s: impl AsRef<[Nsec3HashAlg]>) -> Result<usize, PackError> {
    let s = s.as_ref();
    match s.as_ref().len() {
        len @ 0..=0xFFFF => Ok(len),
        _ => Err(PackError::value_large()),
    }
}

fn unpack<'a, T>(r: &mut Reader<'a>) -> Result<T, UnpackError>
where
    T: From<&'a [Nsec3HashAlg]>,
{
    let s = r.get_bytes_rest()?;
    if s.len() > 0xFFFF {
        Err(UnpackError::value_large())
    } else {
        let s = unsafe { &*(s as *const [u8] as *const [Nsec3HashAlg]) };
        Ok(s.into())
    }
}
