use super::{N3u, Nsec3HashAlg};
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn size_assert() {
    assert_eq!(size_of::<u8>(), size_of::<Nsec3HashAlg>());
}

#[test]
fn wire_marshal() {
    let n3ha = Nsec3HashAlg::SHA_1;
    let input: &[u8] = &[Nsec3HashAlg::SHA_1.0];
    let mut reader = Reader::new(input);
    let n3u = <N3u<&[_]>>::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    assert_eq!(&n3u.understood, &[n3ha]);
    assert_eq!(Ok(1), n3u.packed_len());
    let mut buf = [0u8; 1];
    let mut w = Writer::new(&mut buf[..]);
    n3u.pack(&mut w).expect("pack");
    assert_eq!(w.written(), input);
}

#[test]
fn size_limit() {
    let long_bytes: &[u8] = &[Nsec3HashAlg::SHA_1.0; 0xFFFF + 1][..];
    assert!(<N3u<&[_]>>::unpack(&mut Reader::new(long_bytes)).is_err());
}
