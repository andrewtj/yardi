//! Support for [RFC 6891: Extension Mechanisms for DNS (EDNS(0))][1].
//!
//! [1]: https://tools.ietf.org/html/rfc6891
mod edns_flags;
mod ext_rcode;
mod opt_code;
mod opt_header;

pub mod chain;
pub mod client_subnet;
pub mod cookie;
pub mod dau;
pub mod dhu;
pub mod ede;
pub mod expire;
pub mod key_tag;
pub mod llq;
pub mod n3u;
pub mod nsid;
pub mod owner;
pub mod padding;
pub mod tcp_keepalive;
pub mod update_lease;

pub use self::chain::Chain;
pub use self::client_subnet::ClientSubnet;
pub use self::cookie::Cookie;
pub use self::dau::Dau;
pub use self::dhu::Dhu;
pub use self::edns_flags::EdnsFlags;
pub use self::expire::Expire;
pub use self::ext_rcode::ExtRcode;
pub use self::key_tag::{KeyTagNative, KeyTagNetwork};
pub use self::llq::Llq;
pub use self::n3u::N3u;
pub use self::nsid::Nsid;
pub use self::opt_code::OptCode;
pub use self::opt_header::OptHeader;
pub use self::owner::Owner;
pub use self::padding::Padding;
pub use self::tcp_keepalive::TcpKeepalive;
pub use self::update_lease::UpdateLease;

use core::marker::PhantomData;

use crate::datatypes::{Class, Name, Ttl, Ty};
use crate::errors::ValueErrorKind;
use crate::msg;
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, UnpackErrorKind, Writer};

/// Retrieve an OptHeader and OptReader if present.
pub fn seek_opt<'a>(
    reader: &mut Reader<'a>,
) -> Result<Option<(OptHeader, OptReader<'a>)>, UnpackError> {
    let start = reader.index();
    let r = seek_opt_impl(reader);
    reader.set_index(start);
    r
}

fn seek_opt_impl<'a>(
    reader: &mut Reader<'a>,
) -> Result<Option<(OptHeader, OptReader<'a>)>, UnpackError> {
    reader.set_index(0);
    let h = msg::Header::unpack(reader)?;
    let arcount = h.arcount();
    if arcount == 0 {
        return Ok(None);
    }
    for _ in 0..h.qdcount() {
        msg::skip_q(reader)?;
    }
    for _ in 0..h.ancount() + h.nscount() {
        msg::skip_rr(reader)?;
    }
    for _ in 0..arcount {
        let (name, ty): (Name, Ty) = Unpack::unpack(reader)?;
        if name.is_root() && ty == Ty::OPT {
            let oh = OptHeader::unpack(reader)?;
            let or = OptReader::unpack(reader)?;
            return Ok(Some((oh, or)));
        }
        let _ = <(Class, Ttl)>::unpack(reader)?;
        let rdlen = u16::unpack(reader)? as usize;
        reader.skip(rdlen)?;
    }
    Ok(None)
}

/// Result of reading EDNS option data.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum OptRead<T> {
    /// Code was present but with no option data.
    Empty,
    /// Option data.
    Opt(T),
    /// Code and data present, but a data is from an unknown version.
    UnknownVersion,
}

/// An error returned from reading an EDNS option.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum OptReadError {
    /// An option that was expected to appear once was found multiple times.
    Duplicate,
    /// An unpack error occurred.
    Unpack(UnpackError),
}

impl From<UnpackError> for OptReadError {
    fn from(err: UnpackError) -> Self {
        OptReadError::Unpack(err)
    }
}

/// A trait implemented by types representing EDNS options.
pub trait Opt {
    /// Option Code
    const CODE: OptCode;
    /// Pack the option preceded by a two-byte length prefix.
    fn pack_len(&self, writer: &mut Writer<'_>) -> Result<(), PackError>
    where
        Self: Pack,
    {
        self.pack(writer.writer16()?.as_mut())
    }
    /// Pack the option's code and then call `pack_len`.
    fn pack_code(&self, writer: &mut Writer<'_>) -> Result<(), PackError>
    where
        Self: Pack,
    {
        Self::CODE.pack(writer)?;
        self.pack_len(writer)
    }
}

/// Retrieves options from an OPT RR's data.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct OptReader<'a> {
    buf: &'a [u8],
}

impl<'a> OptReader<'a> {
    /// Get a single option `T` that can only appear once.
    pub fn get<T>(&self) -> Result<Option<OptRead<T>>, OptReadError>
    where
        T: Opt + Unpack<'a>,
    {
        let mut iter = self.get_all::<T>();
        let maybe_t = match iter.next() {
            Some(r) => Some(r?),
            None => None,
        };
        match iter.next() {
            None => Ok(maybe_t),
            Some(Err(err)) => Err(err.into()),
            Some(Ok(_)) => Err(OptReadError::Duplicate),
        }
    }
    /// Returns an iterator over all the `T`'s contained in this `OptReader`.
    pub fn get_all<T>(&self) -> OptReaderIter<'a, T>
    where
        T: Opt + Unpack<'a>,
    {
        OptReaderIter {
            phantom: PhantomData,
            reader: Reader::new(self.buf),
        }
    }
}

impl<'a> Unpack<'a> for OptReader<'a> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let buf = reader.get_bytes16()?;
        Ok(OptReader { buf })
    }
}

/// Iterate over all the `T` options in an `OptReader`.
#[derive(Debug)]
pub struct OptReaderIter<'a, T> {
    phantom: PhantomData<T>,
    reader: Reader<'a>,
}

impl<'a, T> OptReaderIter<'a, T>
where
    T: Opt + Unpack<'a>,
{
    fn next_opt(&mut self) -> Result<Option<OptRead<T>>, UnpackError> {
        let code = OptCode::unpack(&mut self.reader)?;
        let mut reader = self.reader.reader16()?;
        if code != T::CODE {
            return Ok(None);
        } else if reader.is_empty() {
            return Ok(Some(OptRead::Empty));
        }
        match T::unpack(&mut reader) {
            Ok(t) => {
                // TODO: Opt impls should be checking for trailing junk
                if !reader.is_empty() {
                    return Err(UnpackError::trailing_junk_at(reader.index()));
                }
                Ok(Some(OptRead::Opt(t)))
            }
            Err(err) => {
                if err.kind() == UnpackErrorKind::Value(ValueErrorKind::UnexpectedVersion) {
                    Ok(Some(OptRead::UnknownVersion))
                } else {
                    Err(err)
                }
            }
        }
    }
}

impl<'a, T> Iterator for OptReaderIter<'a, T>
where
    T: Opt + Unpack<'a>,
{
    type Item = Result<OptRead<T>, UnpackError>;
    fn next(&mut self) -> Option<Self::Item> {
        while !self.reader.is_empty() {
            match self.next_opt() {
                Ok(Some(item)) => return Some(Ok(item)),
                Ok(None) => (),
                Err(err) => {
                    self.reader.set_index(!0);
                    return Some(Err(err));
                }
            }
        }
        None
    }
}
