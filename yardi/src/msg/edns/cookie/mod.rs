//! Support for the [COOKIE Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc7873

#[cfg(test)]
mod test;

use core::fmt::{self, Debug};

use super::{Opt, OptCode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer};

/// Length of a client cookie.
pub const CLIENT_LEN: usize = 8;
/// Minimum length of a server cookie.
pub const SERVER_MIN_LEN: usize = 8;
/// Maximum length of a server cookie.
pub const SERVER_MAX_LEN: usize = 32;

#[doc = "COOKIE.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc7873\">RFC 7873: Domain Name System (DNS) Cookies</a>"]
#[derive(Clone, Default, PartialEq, Eq)]
pub struct Cookie {
    client: [u8; CLIENT_LEN],
    server: [u8; SERVER_MAX_LEN],
    server_len: u8,
}

impl Cookie {
    /// Return the client part of the `Cookie`.
    pub fn client(&self) -> &[u8] {
        &self.client[..]
    }
    /// Set the client part of the `Cookie`.
    pub fn set_client(&mut self, cookie: &[u8; CLIENT_LEN]) -> &mut Self {
        self.client[..].copy_from_slice(cookie.as_ref());
        self
    }
    /// Return the server part of the `Cookie`, if any.
    pub fn server(&self) -> Option<&[u8]> {
        if self.server_len == 0 {
            return None;
        }
        Some(&self.server[..self.server_len as usize])
    }
    /// Set the server part of the `Cookie`. `cookie` may be empty
    /// or 8 to 32 bytes.
    // TODO: Error::{Short, Long}
    pub fn set_server<T>(&mut self, cookie: T) -> Result<&mut Self, ()>
    where
        T: AsRef<[u8]>,
    {
        let cookie = cookie.as_ref();
        self.server_len = match cookie.len() {
            0 => 0,
            len if (SERVER_MIN_LEN..=SERVER_MAX_LEN).contains(&len) => {
                self.server[..len].copy_from_slice(cookie);
                len as u8
            }
            _ => return Err(()),
        };
        Ok(self)
    }
}

impl Debug for Cookie {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Cookie")
            .field("client", &&self.client[..])
            .field("server", &&self.server())
            .finish()
    }
}

impl Opt for Cookie {
    const CODE: OptCode = OptCode::COOKIE;
}

impl Pack for Cookie {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        dst.write_bytes(&self.client[..])?;
        dst.write_bytes(&self.server[..self.server_len as usize])
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(CLIENT_LEN + self.server_len as usize)
    }
}

impl<'a> Unpack<'a> for Cookie {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let mut c = Cookie::default();
        c.client[..].copy_from_slice(reader.get_bytes(CLIENT_LEN)?);
        match reader.unread() {
            0 => (),
            n if n < SERVER_MIN_LEN => {
                return Err(UnpackError::value_small_at(reader.index()));
            }
            n if n > SERVER_MAX_LEN => {
                return Err(UnpackError::value_large_at(reader.index()));
            }
            n => {
                c.server[..n].copy_from_slice(reader.get_bytes(n)?);
                c.server_len = n as u8;
            }
        }
        Ok(c)
    }
}
