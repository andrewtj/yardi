use super::{Cookie, CLIENT_LEN, SERVER_MAX_LEN, SERVER_MIN_LEN};
use crate::wire::{Pack, Reader, Unpack, Writer};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ExampleTy {
    Bad,
    Client,
    ClientServer,
}

#[test]
fn marshal() {
    let cases = &[
        (CLIENT_LEN - 1, ExampleTy::Bad),
        (CLIENT_LEN, ExampleTy::Client),
        (CLIENT_LEN + SERVER_MIN_LEN, ExampleTy::ClientServer),
        (CLIENT_LEN + SERVER_MAX_LEN, ExampleTy::ClientServer),
        (CLIENT_LEN + SERVER_MAX_LEN + 1, ExampleTy::Bad),
    ];
    let dds: &[u8] = &[0xDD; CLIENT_LEN + SERVER_MAX_LEN + 1];
    for &(len, ty) in cases {
        let mut r = Reader::new(dds[..len].into());
        match Cookie::unpack(&mut r) {
            Ok(_) if ty == ExampleTy::Bad => {
                panic!("unpacked a bad opt");
            }
            Ok(a) => {
                assert!(r.is_empty());
                assert_eq!(a, Cookie::unpack(r.set_index(0)).expect("unpack owned"));
                assert_eq!(a.packed_len(), Ok(len));
                if ty == ExampleTy::ClientServer {
                    assert!(a.server().is_some());
                } else {
                    assert!(a.server().is_none());
                }
                let mut buf = [0u8; CLIENT_LEN + SERVER_MAX_LEN];
                let mut w = Writer::new(&mut buf[..]);
                a.pack(&mut w).expect("pack");
                assert_eq!(&dds[..len], w.written());
            }
            Err(_) if ty == ExampleTy::Bad => {
                assert!(Cookie::unpack(r.set_index(0)).is_err());
            }
            Err(_) => {
                panic!("failed to unpack a good opt");
            }
        }
    }
}
