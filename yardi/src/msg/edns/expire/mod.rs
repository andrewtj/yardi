//! Support for the [EXPIRE Extended Option][1].
//!
//! [1]: https://tools.ietf.org/html/rfc7314

#[cfg(test)]
mod test;

use super::{Opt, OptCode};

#[doc = "EXPIRE.\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc7314\">RFC 7314: Extension Mechanisms for DNS (EDNS) EXPIRE Option</a>"]
#[derive(Clone, Copy, Debug, PartialEq, Eq, Pack, Unpack)]
#[yardi(crate = "crate")]
pub struct Expire(pub u32);

impl Opt for Expire {
    const CODE: OptCode = OptCode::EXPIRE;
}
