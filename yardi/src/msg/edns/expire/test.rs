use super::Expire;
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn marshal() {
    let input: &[u8] = &[0xFF; 4];
    let mut reader = Reader::new(input.into());
    let expire = Expire::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    assert_eq!(expire.0, !0u32);
    assert_eq!(Ok(4), expire.packed_len());
    let mut buf = [0u8; 4];
    let mut w = Writer::new(&mut buf[..]);
    expire.pack(&mut w).expect("pack");
    assert_eq!(w.written(), input);
}
