use super::{Dau, SigAlg};
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn size_assert() {
    assert_eq!(size_of::<u8>(), size_of::<SigAlg>())
}

#[test]
fn wire_marshal() {
    let input: &[u8] = &[SigAlg::ED25519.0];
    let mut reader = Reader::new(input);
    let dau = <Dau<&[_]>>::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    assert_eq!(&dau.understood, &[SigAlg::ED25519]);
    assert_eq!(Ok(1), dau.packed_len());
    let mut buf = [0u8; 1];
    let mut w = Writer::new(&mut buf[..]);
    dau.pack(&mut w).expect("pack");
    assert_eq!(w.written(), input);
}

#[test]
fn size_limit() {
    let long_bytes: &[u8] = &[SigAlg::ED25519.0; 0xFFFF + 1][..];
    assert!(<Dau<&[_]>>::unpack(&mut Reader::new(long_bytes)).is_err());
}
