use crate::{
    datatypes::TsigTime,
    msg::{self, tsig, Header, MsgSig},
    wire::{Reader, Unpack, Writer},
};

const NOW: TsigTime = TsigTime::from_u64(1736896588);

#[test]
#[cfg_attr(miri, ignore)]
fn exercise() {
    let key_name = name!("example.");
    let secret = b"secret";

    let mut client_context;
    let mut query = [0u8; 512];
    let query = {
        let mut w = Writer::new(&mut query[..]);
        w.write_forward(12).expect("skip header");
        client_context = tsig::sign_request(&mut w, &tsig::HMAC_SHA1, &key_name, secret, NOW)
            .expect("sign request");
        w.into_written()
    };

    let (query_partial_msg, query_tsig_rr) = {
        let mut r = Reader::new(&query);
        let h = Header::unpack(&mut r).unwrap();
        assert_eq!(h.arcount(), 1);
        match msg::seek_sig(&mut r) {
            Ok(Some((pm, MsgSig::Tsig(tsig)))) => (pm, tsig),
            x => panic!("{:?}", x),
        }
    };
    let mut server_sign = tsig::verify_request(&query_partial_msg, &query_tsig_rr, secret, NOW)
        .expect("verify request");

    let mut response1 = [0u8; 512];
    let response1 = {
        let mut w = Writer::new(&mut response1[..]);
        w.write_forward(12).expect("skip header 1");
        server_sign.sign(&mut w, NOW).expect("sign response 1");
        w.into_written()
    };

    let (response1_partial_msg, response1_tsig_rr) = {
        let mut r = Reader::new(&response1);
        let h = Header::unpack(&mut r).unwrap();
        assert_eq!(h.arcount(), 1);
        tsig::seek(&mut r).expect("Option<_>").expect("TsigRr")
    };
    client_context
        .update(&response1_partial_msg, NOW, &response1_tsig_rr)
        .expect("verify response 1");

    let mut response2 = [0u8; 512];
    let response2 = {
        let mut w = Writer::new(&mut response2[..]);
        w.write_forward(12).expect("skip header 2");
        server_sign.sign(&mut w, NOW).expect("sign response 2");
        w.into_written()
    };

    let (response2_partial_msg, response2_tsig_rr) = {
        let mut r = Reader::new(&response2);
        let h = Header::unpack(&mut r).unwrap();
        assert_eq!(h.arcount(), 1);
        tsig::seek(&mut r).expect("Option<_>").expect("TsigRr")
    };
    client_context
        .update(&response2_partial_msg, NOW, &response2_tsig_rr)
        .expect("verify response 2");
}
