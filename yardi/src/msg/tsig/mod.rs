//! Support for [RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)][1].
//!
//! This module provides helpers for two use cases:
//! * A client that signs a request and then verifies one or more responses
//!   to that initial request.
//! * A server that verifies a request and then signs one or more responses
//!   to that initial request.
//!
//! The algorithms supported are HMAC-SHA1, HMAC-SHA256, HMAC-SHA384 and
//! HMAC-SHA512.
//!
//! [TSIG Truncation][2] can be enabled via `Options`. When enabled it is
//! only applied when a TSIG would not otherwise fit into the space available.
//!
//! [1]: https://tools.ietf.org/html/rfc8945
//! [2]: https://datatracker.ietf.org/doc/html/rfc8945#section-5.2.2.1

// TODO: hmac-sha256-128, hmac-sha384-192, and hmac-sha512-256.

#[cfg(test)]
mod test;

mod alg;

pub use self::alg::*;
#[doc(no_inline)]
pub use crate::rdata::tsig::{Tsig, TsigRcode, TsigTime};
use crate::{
    datatypes::{Class, Name, Ttl, Ty},
    dnssec::SigAlg,
    msg::{self, header::HEADER_LOC_ARCOUNT, Header, Rcode},
    wire::{Pack, PackError, Reader, Unpack, UnpackError, Writer},
};
use core::fmt::{self, Debug};

/// BIND's pseudo algorithm for HMAC-MD5.
pub const BIND_HMAC_MD5: SigAlg = SigAlg(157);
/// BIND's pseudo algorithm for HMAC-SHA1.
pub const BIND_HMAC_SHA1: SigAlg = SigAlg(161);
/// BIND's pseudo algorithm for HMAC-SHA224.
pub const BIND_HMAC_SHA224: SigAlg = SigAlg(162);
/// BIND's pseudo algorithm for HMAC-SHA256.
pub const BIND_HMAC_SHA256: SigAlg = SigAlg(163);
/// BIND's pseudo algorithm for HMAC-SHA384.
pub const BIND_HMAC_SHA384: SigAlg = SigAlg(164);
/// BIND's pseudo algorithm for HMAC-SHA512.
pub const BIND_HMAC_SHA512: SigAlg = SigAlg(165);

/// Default seconds of error permitted.
// From https://tools.ietf.org/html/rfc2845#section-6
pub const DEFAULT_FUDGE: u16 = 300;

/// Returns the MAC length for supported HMAC algorithms.
pub fn mac_len(name: &Name) -> Option<usize> {
    Hmac::from_name(name).ok().map(|h| h.full_len())
}

/// Retrieve a TSIG signature from a `Reader`.
///
/// Returns the message truncated before the TSIG RR and the TSIG RR, if any.
/// The `Reader`'s index is preserved.
pub fn seek<'a>(reader: &mut Reader<'a>) -> Result<Option<(&'a [u8], TsigRr)>, UnpackError> {
    let start = reader.index();
    let r = seek_impl(reader);
    reader.set_index(start);
    r
}

fn seek_impl<'a>(reader: &mut Reader<'a>) -> Result<Option<(&'a [u8], TsigRr)>, UnpackError> {
    if !msg::seek_last(reader)? {
        return Ok(None);
    }
    let partial_message = reader.get_bytes_seen()?;
    let name = Name::unpack(reader)?;
    if Ty::unpack(reader)? != Ty::TSIG {
        return Ok(None);
    }
    if Class::unpack(reader)? != Class::ANY {
        return Ok(None);
    }
    if Ttl::unpack(reader)? != Ttl(0) {
        return Ok(None);
    }
    let data = Tsig::unpack16(reader)?;
    if !reader.is_empty() {
        return Err(UnpackError::trailing_junk_at(reader.index()));
    }
    Ok(Some((partial_message, TsigRr { name, data })))
}

/// TSIG RR.
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TsigRr {
    /// TSIG key name.
    pub name: Name,
    /// TSIG data.
    pub data: Tsig,
}

impl Pack for TsigRr {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        let _ = self.packed_len()?;
        self.name.pack(writer)?;
        Ty::TSIG.pack(writer)?;
        Class::ANY.pack(writer)?;
        Ttl(0).pack(writer)?;
        self.data.pack16(writer)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let rdlen = self.data.packed_len()?;
        (self.name.len() + 2 + 2 + 4 + 2)
            .checked_add(rdlen)
            .ok_or_else(PackError::value_large)
            .and_then(|l| {
                if l > 0xFFFF {
                    Err(PackError::value_large())
                } else {
                    Ok(l)
                }
            })
    }
}

#[derive(Debug, Clone, Copy)]
struct Unspecified;

impl From<Unspecified> for SignError {
    fn from(Unspecified: Unspecified) -> Self {
        SignError::Unspecified
    }
}

impl From<Unspecified> for VerifyError {
    fn from(Unspecified: Unspecified) -> Self {
        VerifyError::Unspecified
    }
}

impl From<Unspecified> for RequestError {
    fn from(Unspecified: Unspecified) -> Self {
        RequestError::Unspecified
    }
}

struct Context {
    inner: ring::hmac::Context,
}

impl Context {
    fn new(key: &ring::hmac::Key) -> Self {
        let inner = ring::hmac::Context::with_key(key);
        Context { inner }
    }
    fn finish(self) -> Mac {
        let tag = self.inner.sign();
        let tag: &[u8] = tag.as_ref();
        let mut mac = Mac::new(tag.len());
        mac.as_mut()[..].copy_from_slice(tag);
        mac
    }
    fn update(&mut self, bytes: &[u8]) {
        self.inner.update(bytes)
    }
    fn update16(&mut self, bytes: &[u8]) {
        debug_assert!(bytes.len() < 0xFFFF);
        self.update_u16(bytes.len() as u16);
        self.update(bytes);
    }
    fn update_u16(&mut self, n: u16) {
        self.update(&n.to_be_bytes()[..]);
    }
    fn update_u32(&mut self, n: u32) {
        self.update(&n.to_be_bytes()[..]);
    }
    fn update_u48(&mut self, n: u64) {
        self.update(&n.to_be_bytes()[2..]);
    }
    fn update_name(&mut self, name: &Name) {
        let mut temp = [0u8; 64];
        for l in name.labels().rev() {
            temp[0] = l.len() as u8;
            let lb = l.iter().map(u8::to_ascii_lowercase);
            for (d, b) in temp[1..].iter_mut().zip(lb) {
                *d = b;
            }
            self.update(&temp[..1 + l.len()]);
        }
        self.update(&[0]);
    }
    fn update_tsig_vars(&mut self, tsig_rr: &TsigRr) {
        self.update_name(&tsig_rr.name);
        self.update_u16(Class::ANY.0);
        self.update_u32(0);
        self.update_name(&tsig_rr.data.alg_name);
        self.update_u48(tsig_rr.data.time_signed.0);
        self.update_u16(tsig_rr.data.fudge);
        self.update_u16(tsig_rr.data.error.0);
        self.update16(&tsig_rr.data.other);
    }
    fn update_tsig_timers(&mut self, tsig_rr: &TsigRr) {
        self.update_u48(tsig_rr.data.time_signed.0);
        self.update_u16(tsig_rr.data.fudge);
    }
}

fn pack_rr_meta(rr: &TsigRr, writer: &mut Writer<'_>) -> Result<usize, PackError> {
    let initial = writer.len();
    let r = rr
        .name
        .pack(writer)
        .and_then(|()| Ty::TSIG.pack(writer))
        .and_then(|()| Class::ANY.pack(writer))
        .and_then(|()| Ttl(0).pack(writer));
    if r.is_err() {
        writer.truncate(initial).unwrap();
    }
    r.map(|()| initial)
}

fn pack_rr_data(
    rr: &TsigRr,
    mac: &[u8],
    other: &[u8],
    writer: &mut Writer<'_>,
) -> Result<(), PackError> {
    let mut writer = writer.writer16()?;
    rr.data.alg_name.pack(&mut writer)?;
    rr.data.time_signed.pack(&mut writer)?;
    rr.data.fudge.pack(&mut writer)?;
    writer.write_bytes16(mac)?;
    rr.data.original_id.pack(&mut writer)?;
    rr.data.error.pack(&mut writer)?;
    writer.write_bytes16(other)
}

#[derive(Debug, Clone, Copy)]
struct UnsupportedAlg;

impl From<UnsupportedAlg> for SignError {
    fn from(UnsupportedAlg: UnsupportedAlg) -> Self {
        SignError::UnsupportedAlg
    }
}

impl From<UnsupportedAlg> for VerifyError {
    fn from(UnsupportedAlg: UnsupportedAlg) -> Self {
        VerifyError::UnsupportedAlg
    }
}

impl From<UnsupportedAlg> for RequestError {
    fn from(UnsupportedAlg: UnsupportedAlg) -> Self {
        RequestError::BadKey
    }
}

#[derive(Debug, Clone, Copy)]
struct BadTruncPolicy;

impl From<BadTruncPolicy> for SignError {
    fn from(BadTruncPolicy: BadTruncPolicy) -> Self {
        SignError::BadTruncPolicy
    }
}

impl From<BadTruncPolicy> for VerifyError {
    fn from(BadTruncPolicy: BadTruncPolicy) -> Self {
        VerifyError::BadTruncPolicy
    }
}

impl From<BadTruncPolicy> for RequestError {
    fn from(BadTruncPolicy: BadTruncPolicy) -> Self {
        RequestError::BadTruncPolicy
    }
}

macro_rules! impl_hmac_ty {
    ($($id:ident => ($name: ident, $algorithm:ident, $len:literal),)*) => {
        #[derive(Debug, Clone, Copy, PartialEq, Eq)]
        #[allow(clippy::enum_variant_names)]
        enum Hmac {
            $($id),*
        }

        impl Hmac {
            fn from_name(name: &Name) -> Result<Self, UnsupportedAlg> {
                match name {
                    $(a if a == $name => Ok(Hmac::$id),)*
                    _ => Err(UnsupportedAlg),
                }
            }
            fn name(self) -> Name {
                match self {
                    $(Hmac::$id => $name.clone()),*
                }
            }
            fn algorithm(self) -> ring::hmac::Algorithm {
                match self {
                    $(Hmac::$id => ring::hmac::$algorithm,)*
                }
            }
            fn full_len(self) -> usize {
                match self {
                    $(Hmac::$id => $len,)*
                }
            }
            fn min_len(self) -> usize {
                match self {
                    $(Hmac::$id => core::cmp::max(10, $len / 2),)*
                }
            }
            fn policy_len(self, policy: u16) -> Result<(usize, usize), BadTruncPolicy> {
                let min = self.min_len();
                let full = self.full_len();
                match policy as usize {
                    0 => Ok((min, full)),
                    l if l < min || l > full => Err(BadTruncPolicy),
                    l => Ok((l, full)),
                }
            }
        }
    }
}

impl_hmac_ty! {
    Sha1 => (HMAC_SHA1, HMAC_SHA1_FOR_LEGACY_USE_ONLY, 20),
    Sha256 => (HMAC_SHA256, HMAC_SHA256, 32),
    Sha384 => (HMAC_SHA384, HMAC_SHA384, 48),
    Sha512 => (HMAC_SHA512, HMAC_SHA512, 64),
}

#[derive(Clone, Copy)]
struct Mac {
    len: u8,
    buf: [u8; Self::MAX],
}

impl Mac {
    const MAX: usize = 64;
    fn new(len: usize) -> Self {
        assert!(len <= Self::MAX);
        Mac {
            len: len as u8,
            buf: [0u8; Self::MAX],
        }
    }
    fn len(&self) -> usize {
        self.len as usize
    }
    fn truncate(&mut self, len: usize) {
        assert!(len <= self.len());
        self.len = len as u8;
    }
}

impl AsRef<[u8]> for Mac {
    fn as_ref(&self) -> &[u8] {
        &self.buf[..self.len as usize]
    }
}

impl AsMut<[u8]> for Mac {
    fn as_mut(&mut self) -> &mut [u8] {
        &mut self.buf[..self.len as usize]
    }
}

/// Error returned from signing a message.
#[derive(Debug)]
pub enum SignError {
    /// Invalid message provided
    BadMsg,
    /// Local truncation policy is not a valid size for the algorithm
    BadTruncPolicy,
    /// A failure occurred appending the TSIG RR to the message
    PackError(PackError),
    /// Unsupported HMAC algorithm
    UnsupportedAlg,
    /// Unspecified failure occurred in the underlying crypto library.
    Unspecified,
}

impl From<PackError> for SignError {
    fn from(err: PackError) -> Self {
        SignError::PackError(err)
    }
}

/// An error returned from verifying a message.
#[derive(Debug)]
pub enum VerifyError {
    /// Calculated MAC does not match the recieved MAC (BADSIG)
    BadMac,
    /// Recieved MAC is too large or too small (FORMERR)
    BadMacSize,
    /// Invalid messasge provided
    BadMsg,
    /// Too large a discrepancy between the time signed and the current time (BADTIME)
    BadTime,
    /// Recieved MAC is truncated to a value that is standard compliant but too small
    /// for the local policy (BADTRUNC)
    BadTrunc,
    /// Local truncation policy is not a valid size for the algorithm
    BadTruncPolicy,
    /// Unsupported HMAC algorithm (BADKEY)
    UnsupportedAlg,
    /// Initial response message was unsigned or too many responses have passed
    /// without a signature. Unrecoverable.
    Unsigned,
    /// Unspecified failure occurred in the underlying crypto library.
    Unspecified,
}

/// TSIG Options.
#[derive(Clone, Debug)]
pub struct Options {
    fudge: u16,
    trunc: u16,
}

impl Options {
    const DEFAULT: Self = Options {
        fudge: DEFAULT_FUDGE,
        trunc: 0,
    };
    /// Set seconds of error permitted.
    pub fn fudge(&mut self, fudge: u16) -> &mut Self {
        self.fudge = fudge;
        self
    }
    /// Set minimum MAC size. Must be at least the greater of ten bytes or half
    /// the algorithm's output. Set to zero to use full size.
    pub fn trunc(&mut self, bytes: u16) -> &mut Self {
        self.trunc = bytes;
        self
    }
}

impl Default for Options {
    fn default() -> Self {
        Self::DEFAULT.clone()
    }
}

/// Equivalent to calling `sign_request_options` with default options.
pub fn sign_request(
    request: &mut Writer<'_>,
    alg: &Name,
    key: &Name,
    secret: &[u8],
    now: TsigTime,
) -> Result<ClientContext, SignError> {
    sign_request_options(request, alg, key, secret, now, &Options::DEFAULT)
}

/// Sign a request and return a context for verifying responses.
pub fn sign_request_options(
    request: &mut Writer<'_>,
    alg: &Name,
    key: &Name,
    secret: &[u8],
    now: TsigTime,
    options: &Options,
) -> Result<ClientContext, SignError> {
    let hmac = Hmac::from_name(alg)?;
    let (mac_len_min, mac_len_max) = hmac.policy_len(options.trunc)?;
    let mut mac;
    let fudge = options.fudge;
    let original_id =
        Header::from_bytes(request.written().get(0..12).ok_or(SignError::BadMsg)?).id();
    let rr = TsigRr {
        name: key.into(),
        data: Tsig {
            alg_name: hmac.name(),
            time_signed: now,
            fudge,
            error: TsigRcode::NOERROR,
            mac: Default::default(),
            original_id,
            other: Default::default(),
        },
    };
    let mac_less_tsig_rd_len = 2 + rr.data.packed_len()?;
    let rr_start = pack_rr_meta(&rr, request)?;
    let mac_len_actual = request
        .remaining()
        .checked_sub(mac_less_tsig_rd_len)
        .and_then(|n| match n {
            n if n < mac_len_min => None,
            n if n > mac_len_max => Some(mac_len_max),
            n => Some(n),
        })
        .ok_or_else(|| {
            request.truncate(rr_start).unwrap();
            PackError::no_space()
        })?;
    let key = ring::hmac::Key::new(hmac.algorithm(), secret);
    let mut context = Context::new(&key);
    context.update(&request.written()[..rr_start]);
    context.update_tsig_vars(&rr);
    mac = context.finish();
    mac.truncate(mac_len_actual);
    {
        let h = Header::from_bytes_mut(request.written_mut());
        let arcount = h.arcount();
        h.set_arcount(
            arcount
                .checked_add(1)
                .ok_or_else(PackError::value_invalid)?,
        );
    }
    pack_rr_data(&rr, mac.as_ref(), &[], request.for_ty(Ty::TSIG).as_mut())
        .expect("tsig size miscalculated");
    Ok(ClientContext::new(hmac, key, mac))
}

/// Stores state needed to verify responses.
///
/// The first and at least every hundredth response must be signed. The time
/// signed must not go backwards.
pub struct ClientContext {
    hmac: Hmac,
    key: ring::hmac::Key,
    last_mac: Mac,
    last_signed_time: Option<TsigTime>,
}

impl Debug for ClientContext {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ClientContext").finish()
    }
}

impl ClientContext {
    fn new(hmac: Hmac, key: ring::hmac::Key, last_mac: Mac) -> Self {
        ClientContext {
            hmac,
            key,
            last_mac,
            last_signed_time: None,
        }
    }
    /// Verify a response. Equivalent to calling `update_options` with default options.
    pub fn update(
        &mut self,
        partial_message: &[u8],
        now: TsigTime,
        tsig_rr: &TsigRr,
    ) -> Result<(), VerifyError> {
        self.update_options(partial_message, tsig_rr, now, &Options::DEFAULT)
    }
    /// Verify a response. Equivalent to calling `update_unsigned` with an empty iterator.
    pub fn update_options(
        &mut self,
        partial_message: &[u8],
        tsig_rr: &TsigRr,
        now: TsigTime,
        options: &Options,
    ) -> Result<(), VerifyError> {
        self.update_unsigned(partial_message, tsig_rr, now, options, None)
    }
    /// Verify a response.
    ///
    /// `partial_message` is expected to contain the signed response truncated
    /// at the TSIG RR and without ARCOUNT adjusted.
    ///
    /// `tsig_rr`'s name is not checked as it's expected to have already been
    /// done already as part of extracting it from the message.
    ///
    /// `unsigned` must iterate over any unsigned messages between the last
    /// signed message and `partial_msg`.
    pub fn update_unsigned<'a, T>(
        &mut self,
        partial_message: &[u8],
        tsig_rr: &TsigRr,
        now: TsigTime,
        options: &Options,
        unsigned: T,
    ) -> Result<(), VerifyError>
    where
        T: IntoIterator<Item = &'a [u8]>,
    {
        if partial_message.len() < 12 {
            return Err(VerifyError::BadMsg);
        }
        let mut context = Context::new(&self.key);
        context.update16(self.last_mac.as_ref());
        for (i, msg) in unsigned.into_iter().enumerate() {
            if msg.len() < 12 {
                return Err(VerifyError::BadMsg);
            }
            if self.last_signed_time.is_none() {
                return Err(VerifyError::Unsigned);
            }
            if i > 99 {
                return Err(VerifyError::Unsigned);
            }
            context.update(msg);
        }
        let (mac_len_min, mac_len_max) = self.hmac.policy_len(options.trunc)?;
        let response_mac = &tsig_rr.data.mac[..];
        if response_mac.len() < mac_len_min {
            return Err(VerifyError::BadMacSize);
        }
        if response_mac.len() > mac_len_max {
            return Err(VerifyError::BadMac);
        }
        let (header, body) = partial_message.split_at(12);
        context.update_u16(tsig_rr.data.original_id);
        context.update(&header[2..HEADER_LOC_ARCOUNT]);
        let arcount =
            u16::from_be_bytes([header[HEADER_LOC_ARCOUNT], header[HEADER_LOC_ARCOUNT + 1]]);
        if arcount == 0 {
            return Err(VerifyError::BadMsg);
        }
        context.update_u16(arcount - 1);
        context.update(body);
        if self.last_signed_time.is_some() {
            context.update_tsig_timers(tsig_rr);
        } else {
            context.update_tsig_vars(tsig_rr);
        }
        let mut mac = context.finish();
        mac.truncate(response_mac.len());
        if ring::constant_time::verify_slices_are_equal(mac.as_ref(), response_mac).is_err() {
            return Err(VerifyError::BadMac);
        }
        if !tsig_rr.data.time_signed.within_fudge(now, options.fudge)
            || self
                .last_signed_time
                .map(|t| tsig_rr.data.time_signed < t)
                .unwrap_or(false)
        {
            return Err(VerifyError::BadTime);
        }
        self.last_signed_time = Some(tsig_rr.data.time_signed);
        self.last_mac = mac;
        Ok(())
    }
}

/// Error returned from verifying a request.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum RequestError {
    /// Message is too short to be valid.
    ShortMsg,
    /// Format Error.
    FormErr,
    /// Bad signature.
    BadSig,
    /// Bad time signed.
    BadTime,
    /// Bad truncation.
    BadTrunc,
    /// Local truncation policy is invalid.
    BadTruncPolicy,
    /// Invalid key.
    BadKey,
    /// Unspecified failure occurred in the underlying crypto library.
    Unspecified,
}

impl RequestError {
    /// Suggested response codes for a given error, if any.
    pub fn rcodes(self) -> Option<(Rcode, Option<TsigRcode>)> {
        match self {
            RequestError::ShortMsg => None,
            RequestError::FormErr => Some((Rcode::FORMERR, None)),
            RequestError::BadSig => Some((Rcode::NOTAUTH, Some(TsigRcode::BADSIG))),
            RequestError::BadTime => Some((Rcode::NOTAUTH, Some(TsigRcode::BADTIME))),
            RequestError::BadTrunc => Some((Rcode::NOTAUTH, Some(TsigRcode::BADTRUNC))),
            RequestError::BadTruncPolicy => Some((Rcode::SERVFAIL, None)),
            RequestError::BadKey => Some((Rcode::NOTAUTH, Some(TsigRcode::BADKEY))),
            RequestError::Unspecified => Some((Rcode::SERVFAIL, None)),
        }
    }
}

/// Equivalent to calling `write_error_response_options` with default options.
pub fn write_error_response(
    response: &mut Writer<'_>,
    request_tsig_rr: &TsigRr,
    secret: &[u8],
    error: TsigRcode,
    now: TsigTime,
) -> Result<(), SignError> {
    write_error_response_options(
        response,
        request_tsig_rr,
        secret,
        error,
        now,
        &Options::DEFAULT,
    )
}

/// Write a TSIG error response.
///
/// If the error is `TsigRcode::BADTIME`, the response will be signed with
/// the time signed taken from `request_tsig_rr` and the local time written
/// to the TSIG RR's other field. For other errors the response will not be
/// signed and `secret` will go unused.
pub fn write_error_response_options(
    response: &mut Writer<'_>,
    request_tsig_rr: &TsigRr,
    secret: &[u8],
    error: TsigRcode,
    now: TsigTime,
    options: &Options,
) -> Result<(), SignError> {
    let fudge = options.fudge;
    let time_signed;
    let other_buf;
    let other;
    let local_time = now;
    if error == TsigRcode::BADTIME {
        other_buf = local_time.0.to_be_bytes();
        other = &other_buf[2..];
        time_signed = request_tsig_rr.data.time_signed;
    } else {
        other = &[];
        time_signed = local_time;
    };
    let original_id = {
        let h = Header::from_bytes(response.written().get(0..12).ok_or(SignError::BadMsg)?);
        if h.arcount() == 0xFFFF {
            return Err(SignError::BadMsg);
        }
        h.id()
    };
    let hmac = Hmac::from_name(&request_tsig_rr.data.alg_name)?;
    let mut mac;
    let mut response_tsig_rr = TsigRr {
        name: request_tsig_rr.name.clone(),
        data: Tsig {
            alg_name: request_tsig_rr.data.alg_name.clone(),
            time_signed,
            fudge,
            error,
            mac: Default::default(),
            original_id,
            other: Default::default(),
        },
    };
    if error != TsigRcode::BADTIME {
        let initial = response.len();
        if let Err(err) = response_tsig_rr.pack(response) {
            response.truncate(initial).unwrap();
            return Err(err.into());
        }
        let h = Header::from_bytes_mut(response.written_mut());
        let arcount = h.arcount();
        h.set_arcount(arcount + 1);
        return Ok(());
    }
    let (mac_len_min, mac_len_max) = hmac.policy_len(options.trunc)?;
    let mac_less_tsig_rd_len = 2 + response_tsig_rr.data.packed_len()?;
    let rr_start = pack_rr_meta(&response_tsig_rr, response)?;
    let mac_len_actual = response
        .remaining()
        .checked_sub(mac_less_tsig_rd_len)
        .and_then(|n| match n {
            n if n < mac_len_min => None,
            n if n > mac_len_max => Some(mac_len_max),
            n => Some(n),
        })
        .ok_or_else(|| {
            response.truncate(rr_start).unwrap();
            PackError::no_space()
        })?;
    response_tsig_rr.data.alg_name = hmac.name();
    let key = ring::hmac::Key::new(hmac.algorithm(), secret);
    let mut context = Context::new(&key);
    context.update16(&request_tsig_rr.data.mac[..]);
    context.update(&response.written()[..rr_start]);
    context.update_tsig_vars(&response_tsig_rr);
    mac = context.finish();
    mac.truncate(mac_len_actual);
    {
        let h = Header::from_bytes_mut(response.written_mut());
        let arcount = h.arcount();
        h.set_arcount(arcount + 1);
    }
    pack_rr_data(
        &response_tsig_rr,
        mac.as_ref(),
        other,
        response.for_ty(Ty::TSIG).as_mut(),
    )
    .expect("tsig size miscalculated");
    Ok(())
}

/// Equivalent to calling `verify_request_options` with default options.
pub fn verify_request(
    partial_message: &[u8],
    tsig_rr: &TsigRr,
    secret: &[u8],
    now: TsigTime,
) -> Result<ServerContext, RequestError> {
    verify_request_options(partial_message, tsig_rr, secret, now, &Options::DEFAULT)
}

/// Verify a request and return a context for signing responses.
///
/// `partial_message` is expected to contain the signed response truncated
/// at the TSIG RR and without ARCOUNT adjusted.
pub fn verify_request_options(
    partial_message: &[u8],
    tsig_rr: &TsigRr,
    secret: &[u8],
    now: TsigTime,
    options: &Options,
) -> Result<ServerContext, RequestError> {
    if partial_message.len() < 12 {
        return Err(RequestError::ShortMsg);
    }
    let hmac = Hmac::from_name(&tsig_rr.data.alg_name)?;
    let (mac_len_min, mac_len_max) = hmac.policy_len(options.trunc)?;
    let request_mac = &tsig_rr.data.mac[..];
    if request_mac.len() < mac_len_min {
        return Err(RequestError::BadTrunc);
    }
    if request_mac.len() > mac_len_max {
        return Err(RequestError::FormErr);
    }
    let key = ring::hmac::Key::new(hmac.algorithm(), secret);
    let mut context = Context::new(&key);
    let (header, body) = partial_message.split_at(12);
    context.update(&header[..HEADER_LOC_ARCOUNT]);
    let c = u16::from_be_bytes([header[HEADER_LOC_ARCOUNT], header[HEADER_LOC_ARCOUNT + 1]])
        .saturating_sub(1);
    context.update_u16(c);
    context.update(body);
    context.update_tsig_vars(tsig_rr);
    let mut mac = context.finish();
    mac.truncate(request_mac.len());
    if ring::constant_time::verify_slices_are_equal(mac.as_ref(), request_mac).is_err() {
        return Err(RequestError::BadSig);
    }
    if !tsig_rr.data.time_signed.within_fudge(now, options.fudge) {
        return Err(RequestError::BadTime);
    }
    Ok(ServerContext {
        is_first: true,
        name: tsig_rr.name.clone(),
        hmac,
        key,
        last_mac: mac,
    })
}

/// Stores state needed to sign one or more responses.
///
/// Unsigned intermediate responses are not supported.
pub struct ServerContext {
    is_first: bool,
    name: Name,
    hmac: Hmac,
    key: ring::hmac::Key,
    last_mac: Mac,
}

impl Debug for ServerContext {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("ServerContext").finish()
    }
}

impl ServerContext {
    /// Equivalent to calling `sign_options` with default options.
    pub fn sign(&mut self, response: &mut Writer<'_>, now: TsigTime) -> Result<(), SignError> {
        self.sign_options(response, now, &Options::DEFAULT)
    }
    /// Sign a response.
    pub fn sign_options(
        &mut self,
        response: &mut Writer<'_>,
        now: TsigTime,
        options: &Options,
    ) -> Result<(), SignError> {
        let (mac_len_min, mac_len_max) = self.hmac.policy_len(options.trunc)?;
        let fudge = options.fudge;
        let original_id = {
            let h = Header::from_bytes(response.written().get(0..12).ok_or(SignError::BadMsg)?);
            if h.arcount() == 0xFFFF {
                return Err(SignError::BadMsg);
            }
            h.id()
        };
        let rr = TsigRr {
            name: self.name.clone(),
            data: Tsig {
                alg_name: self.hmac.name(),
                time_signed: now,
                fudge,
                error: TsigRcode::NOERROR,
                mac: Default::default(),
                original_id,
                other: Default::default(),
            },
        };
        let mac_less_tsig_rd_len = 2 + rr.data.packed_len()?;
        let rr_start = pack_rr_meta(&rr, response)?;
        let mac_len_actual = response
            .remaining()
            .checked_sub(mac_less_tsig_rd_len)
            .and_then(|n| match n {
                n if n < mac_len_min => None,
                n if n > mac_len_max => Some(mac_len_max),
                n => Some(n),
            })
            .ok_or_else(PackError::no_space)?;
        let mut context = Context::new(&self.key);
        context.update16(self.last_mac.as_ref());
        context.update(&response.written()[..rr_start]);
        if self.is_first {
            context.update_tsig_vars(&rr);
        } else {
            context.update_tsig_timers(&rr);
        }
        let mut mac = context.finish();
        mac.truncate(mac_len_actual);
        {
            let h = Header::from_bytes_mut(response.written_mut());
            let arcount = h.arcount();
            h.set_arcount(arcount + 1);
        }
        pack_rr_data(&rr, mac.as_ref(), &[], response.for_ty(Ty::TSIG).as_mut())
            .expect("tsig size miscalculated");
        self.is_first = false;
        self.last_mac = mac;
        Ok(())
    }
}
