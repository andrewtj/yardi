use core::{cmp::Ordering, str::FromStr};

use crate::ascii::parse::Pull;
use crate::ascii::{Context, Parse, ParseError, Parser, Present, PresentError, Presenter};
use crate::datatypes::{Class, Name, Ttl, Ty};
use crate::rdata::{Generic, Rdata, StaticRdata};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, WireOrdError, Writer};

macro_rules! impl_cl_ty_helpers {
    ($ty:ty, $cmp:ident, $pack:ident, $packed_len:ident, $unpack:ident, $unpacked_len:ident, $present:ident) => {
        fn $cmp(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
            let l_initial = left.index();
            let r_initial = right.index();
            let _ = $unpack(left).map_err(WireOrdError::Left)?;
            let _ = $unpack(right).map_err(WireOrdError::Right)?;
            crate::datatypes::bytes::cmp_fixed(
                left.set_index(l_initial),
                right.set_index(r_initial),
                2,
            )
        }
        fn $pack(t: &$ty, w: &mut Writer<'_>) -> Result<(), PackError> {
            if t.is_data() {
                t.pack(w)
            } else {
                Err(PackError::value_invalid())
            }
        }
        fn $packed_len(t: &$ty) -> Result<usize, PackError> {
            if t.is_data() {
                t.packed_len()
            } else {
                Err(PackError::value_invalid())
            }
        }
        fn $unpack(r: &mut Reader<'_>) -> Result<$ty, UnpackError> {
            <$ty>::unpack(r).and_then(|t| {
                if t.is_data() {
                    Ok(t)
                } else {
                    Err(UnpackError::value_invalid_at(r.index() - 2))
                }
            })
        }
        fn $unpacked_len(r: &mut Reader<'_>) -> Result<usize, UnpackError> {
            $unpack(r).map(|_| 2)
        }
        fn $present(t: &$ty, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
            if t.is_data() {
                t.present(p)
            } else {
                Err(PresentError::value_invalid())
            }
        }
    };
}

impl_cl_ty_helpers!(
    Class,
    cmp_dcl,
    pack_dcl,
    packed_len_dcl,
    unpack_dcl,
    unpacked_len_dcl,
    present_dcl
);
impl_cl_ty_helpers!(
    Ty,
    cmp_dty,
    pack_dty,
    packed_len_dty,
    unpack_dty,
    unpacked_len_dty,
    present_dty
);

// TODO: this doesn't seem like the right place for Rr* to live
#[doc = r#"
RrHeader is primarily intended to provide reuseable
[RFC 1035](https://tools.ietf.org/html/rfc1035#section-5) RR parsing logic.
```
use yardi::{ascii::{Parse, Parser, ParseError}, msg::RrHeader};

struct MyT;

impl Parse for MyT {
    fn parse(parser: &mut Parser) -> Result<Self, ParseError> {
        let RrHeader{name, ty, class, ttl} = Parse::parse(parser)?;
        # let _ = (name, ty, class);
        unimplemented!("custom logic");
    }
}
```
"#]
#[derive(
    Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Present, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
pub struct RrHeader {
    /// Owner name
    #[yardi(error_subject = "RR Name")]
    pub name: Name,
    /// Type
    #[yardi(
        pack = "pack_dty",
        packed_len = "packed_len_dty",
        unpack = "unpack_dty",
        unpacked_len = "unpacked_len_dty",
        present = "present_dty",
        cmp_wire = "cmp_dty",
        cmp_wire_canonical = "cmp_dty",
        error_subject = "RR Type"
    )]
    pub ty: Ty,
    /// Class
    #[yardi(
        pack = "pack_dcl",
        packed_len = "packed_len_dcl",
        unpack = "unpack_dcl",
        unpacked_len = "unpacked_len_dcl",
        present = "present_dcl",
        cmp_wire = "cmp_dcl",
        cmp_wire_canonical = "cmp_dcl",
        error_subject = "RR Class"
    )]
    pub class: Class,
    /// TTL
    #[yardi(error_subject = "RR TTL")]
    pub ttl: Ttl,
}

impl RrHeader {
    /// Parse an `RrHeader` with a specific class required.
    pub fn parse_with_class(parser: &mut Parser<'_, '_>, class: Class) -> Result<Self, ParseError> {
        Self::parse_impl(parser, Some(class))
    }
    fn parse_impl(p: &mut Parser<'_, '_>, expected_cl: Option<Class>) -> Result<Self, ParseError> {
        let context = p.context();
        let name = if p.leading_whitespace()? {
            context
                .last_owner()
                .ok_or_else(ParseError::value_no_last_name)?
                .into()
        } else {
            Name::parse(p)?
        };
        let mut ttl = None;
        let mut class = None;
        loop {
            if ttl.is_none() {
                ttl = p.pull(|s| match Ttl::from_str(s) {
                    Ok(ttl) => Pull::Accept(Some(ttl)),
                    Err(_) => Pull::Decline(None),
                })?;
            }
            if class.is_none() {
                class = p.pull(|s| match Class::from_str(s) {
                    Ok(class) => {
                        let match_expected = expected_cl.map(|t| t == class).unwrap_or(true);
                        if class.is_data() && match_expected {
                            Pull::Accept(Some(class))
                        } else {
                            Pull::Reject(ParseError::value_invalid())
                        }
                    }
                    Err(_) => Pull::Decline(None),
                })?;
                if class.is_some() && ttl.is_none() {
                    continue;
                }
            }
            break;
        }
        let ttl = if let Some(ttl) = ttl {
            ttl
        } else {
            context.infer_ttl().ok_or_else(ParseError::value_no_ttl)?
        };
        let class = if let Some(class) = class {
            class
        } else {
            context
                .last_class()
                .ok_or_else(ParseError::value_no_class)?
        };
        if expected_cl.is_some() && Some(class) != expected_cl {
            return Err(ParseError::value_invalid());
        }
        let ty = Ty::parse(p)?;
        if !ty.is_data() {
            return Err(ParseError::value_invalid());
        }
        Ok(RrHeader {
            name,
            ty,
            class,
            ttl,
        })
    }
    fn into_rr<T>(self, data: T) -> Rr<T> {
        let RrHeader {
            name, class, ttl, ..
        } = self;
        Rr {
            name,
            class,
            ttl,
            data,
        }
    }
}

impl Parse for RrHeader {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        Self::parse_impl(p, None)
    }
}

/// Data Resource Record.
///
/// This struct represents a concrete data resource record. It is an error
/// to use it with a non-data type or class.
// TODO: error subject
#[derive(Clone, Debug)]
pub struct Rr<T> {
    /// Owner name
    pub name: Name,
    /// Class
    pub class: Class,
    /// TTL
    pub ttl: Ttl,
    /// Data
    pub data: T,
}

impl<T> Rr<T> {
    fn present_header(
        &self,
        data_cl: Option<Class>,
        data_ty: Ty,
        p: &mut Presenter<'_, '_>,
    ) -> Result<(), PresentError> {
        if data_cl.map(|cl| cl != self.class).unwrap_or(false) {
            return Err(PresentError::value_invalid());
        }
        self.name.present(p)?;
        self.ttl.present(p)?;
        present_dcl(&self.class, p)?;
        present_dty(&data_ty, p)
    }
    fn pack_header(
        &self,
        data_cl: Option<Class>,
        data_ty: Ty,
        dst: &mut Writer<'_>,
    ) -> Result<(), PackError> {
        if data_cl.map(|cl| cl != self.class).unwrap_or(false) {
            return Err(PackError::value_invalid());
        }
        self.name.pack(dst)?;
        pack_dty(&data_ty, dst)?;
        pack_dcl(&self.class, dst)?;
        self.ttl.pack(dst)
    }
    fn packed_header_len(&self, data_cl: Option<Class>, data_ty: Ty) -> Result<usize, PackError> {
        if data_cl.map(|cl| cl != self.class).unwrap_or(false) {
            return Err(PackError::value_invalid());
        }
        let _ = packed_len_dty(&data_ty)?;
        let _ = packed_len_dcl(&self.class)?;
        Ok(self.name.len() + 2 + 2 + 4 + 2)
    }
    fn cmp_header(&self, o: &Self, s_ty: Ty, o_ty: Ty) -> Ordering {
        match self.name.cmp(&o.name) {
            Ordering::Equal => (),
            other => return other,
        }
        match s_ty.cmp(&o_ty) {
            Ordering::Equal => (),
            other => return other,
        }
        match self.class.cmp(&o.class) {
            Ordering::Equal => (),
            other => return other,
        }
        self.ttl.cmp(&o.ttl)
    }
}

impl<T> Default for Rr<T>
where
    T: StaticRdata + Default,
{
    fn default() -> Self {
        let name = Default::default();
        let class = T::CLASS.unwrap_or(Class::IN);
        let ttl = Default::default();
        let data = Default::default();
        Rr {
            name,
            class,
            ttl,
            data,
        }
    }
}

// TODO: FromStr for Rr<Generic>
impl<T> FromStr for Rr<T>
where
    T: StaticRdata + Parse + Present,
{
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(s.as_bytes(), Context::shared_default())
    }
}

impl<T> Eq for Rr<T> where T: Rdata + Eq {}

impl<T> PartialEq for Rr<T>
where
    T: Rdata + PartialEq,
{
    fn eq(&self, other: &Self) -> bool {
        self.name == other.name
            && self.class == other.class
            && self.ttl == other.ttl
            && self.data == other.data
    }
}

impl<T> PartialOrd for Rr<T>
where
    T: Rdata + PartialOrd,
{
    fn partial_cmp(&self, o: &Self) -> Option<Ordering> {
        match self.cmp_header(o, self.data.ty(), o.data.ty()) {
            Ordering::Equal => self.data.partial_cmp(&o.data),
            other => Some(other),
        }
    }
}

impl<T> Ord for Rr<T>
where
    T: Rdata + Ord,
{
    fn cmp(&self, o: &Self) -> Ordering {
        match self.cmp_header(o, self.data.ty(), o.data.ty()) {
            Ordering::Equal => self.data.cmp(&o.data),
            other => other,
        }
    }
}

impl<T> Pack for Rr<T>
where
    T: Rdata + Pack,
{
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        // TODO: check self.class == self.data.class ?
        self.pack_header(self.data.class(), self.data.ty(), dst)?;
        self.data.pack16(dst)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        // TODO: check self.class == self.data.class ?
        self.packed_header_len(self.data.class(), self.data.ty())
            .and_then(|hl| self.data.packed_len().map(|dl| hl + dl))
    }
}

impl<'a, T> Unpack<'a> for Rr<T>
where
    T: StaticRdata + Unpack<'a>,
{
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let (name, ty): (_, Ty) = Unpack::unpack(reader)?;
        if ty != T::TY {
            return Err(UnpackError::value_invalid_at(reader.index() - 2));
        }
        let (class, ttl) = Unpack::unpack(reader)?;
        let data = T::unpack16(reader)?;
        Ok(Rr {
            name,
            class,
            ttl,
            data,
        })
    }
}

impl<'a> Unpack<'a> for Rr<Generic> {
    fn unpack(r: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let h = RrHeader::unpack(r)?;
        let data = Generic::unpack16(h.class, h.ty, r)?;
        Ok(h.into_rr(data))
    }
}

impl<T> Present for Rr<T>
where
    T: Rdata + Present,
{
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        // TODO: check self.data.class == self.class ?
        self.present_header(self.data.class(), self.data.ty(), p)?;
        self.data.present(p)
    }
}

impl<T> Parse for Rr<T>
where
    T: StaticRdata + Parse,
{
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError>
    where
        Self: Sized,
    {
        let h = RrHeader::parse_impl(p, T::CLASS)?;
        if h.ty != T::TY {
            return Err(ParseError::value_invalid());
        }
        // TODO: class check?
        let data = T::parse(p)?;
        Ok(h.into_rr(data))
    }
}

impl Parse for Rr<Generic> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let h = RrHeader::parse(p)?;
        let data = Generic::parse(h.class, h.ty, p)?;
        Ok(h.into_rr(data))
    }
}
