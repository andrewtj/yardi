use crate::datatypes::{Class, Name, Ty};

/// DNS Question.
#[derive(
    Clone, Debug, Default, Eq, Ord, PartialEq, PartialOrd, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
pub struct Question {
    /// Query Name
    #[yardi(error_subject = "Question Name")]
    pub name: Name,
    /// Query Type
    #[yardi(error_subject = "Question Type")]
    pub ty: Ty,
    /// Query Class
    #[yardi(error_subject = "Question Class")]
    pub class: Class,
}
