use super::Header;

#[test]
fn bools() {
    let mut h = Header([0u8; 12]);
    let getters = &[
        Header::qr,
        Header::aa,
        Header::tc,
        Header::rd,
        Header::ra,
        Header::z,
        Header::ad,
        Header::cd,
    ];
    let setters = &[
        Header::set_qr,
        Header::set_aa,
        Header::set_tc,
        Header::set_rd,
        Header::set_ra,
        Header::set_z,
        Header::set_ad,
        Header::set_cd,
    ];
    let n = getters.len();
    for o in 0..n {
        for i in 0..n {
            assert!(!getters[i](&h));
        }
        setters[o](&mut h, true);
        assert!(getters[o](&h));
        for i in 0..n {
            if i == o {
                continue;
            }
            assert!(!getters[i](&h));
        }
        setters[o](&mut h, false);
        for i in 0..n {
            assert!(!getters[i](&h));
        }
    }
}

#[test]
fn u16s() {
    let mut h = Header([0u8; 12]);
    let getters = &[
        Header::id,
        Header::qdcount,
        Header::ancount,
        Header::nscount,
        Header::arcount,
    ];
    let setters = &[
        Header::set_id,
        Header::set_qdcount,
        Header::set_ancount,
        Header::set_nscount,
        Header::set_arcount,
    ];
    let n = getters.len();
    for o in 0..n {
        for i in 0..n {
            assert_eq!(getters[i](&h), 0);
        }
        setters[o](&mut h, 0xFFFF);
        assert_eq!(getters[o](&h), 0xFFFF);
        assert_eq!(10, h.0[..].iter().filter(|&&n| n == 0).count());
        for i in 0..n {
            if i == o {
                continue;
            }
            assert_eq!(getters[i](&h), 0);
        }
        setters[o](&mut h, 0);
        assert_eq!(getters[o](&h), 0);
        for i in 0..n {
            assert_eq!(getters[i](&h), 0);
        }
        assert_eq!(12, h.0[..].iter().filter(|&&n| n == 0).count());
    }
}
