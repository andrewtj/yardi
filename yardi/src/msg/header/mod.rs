#[cfg(test)]
mod test;

use core::fmt::{self, Debug};

use super::{Opcode, Rcode};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, Writer};

#[derive(Clone, Copy, Debug)]
struct HeaderBit {
    index: usize,
    mask: u8,
}

/// QR - Query Response
const HEADER_BIT_QR: HeaderBit = HeaderBit {
    index: 2,
    mask: 0b1000_0000,
};
/// AA - Authoritative Answer
const HEADER_BIT_AA: HeaderBit = HeaderBit {
    index: 2,
    mask: 0b0000_0100,
};
/// TC - Truncated
const HEADER_BIT_TC: HeaderBit = HeaderBit {
    index: 2,
    mask: 0b0000_0010,
};
/// RD - Recursion Desired
const HEADER_BIT_RD: HeaderBit = HeaderBit {
    index: 2,
    mask: 0b0000_0001,
};
/// RA - Recursion Available
const HEADER_BIT_RA: HeaderBit = HeaderBit {
    index: 3,
    mask: 0b1000_0000,
};
/// Z - Reserved
const HEADER_BIT_Z: HeaderBit = HeaderBit {
    index: 3,
    mask: 0b0100_0000,
};
/// AD - Authentic Data
const HEADER_BIT_AD: HeaderBit = HeaderBit {
    index: 3,
    mask: 0b0010_0000,
};
/// CD - Checking Disabled
const HEADER_BIT_CD: HeaderBit = HeaderBit {
    index: 3,
    mask: 0b0001_0000,
};

/// ID - Location of ID
pub(crate) const HEADER_LOC_ID: usize = 0;
/// QDCOUNT - Location of Question section
pub(crate) const HEADER_LOC_QDCOUNT: usize = 4;
/// ANCOUNT - Location of Answer section
pub(crate) const HEADER_LOC_ANCOUNT: usize = 6;
/// NSCOUNT - Location of Authority section
pub(crate) const HEADER_LOC_NSCOUNT: usize = 8;
/// ARCOUNT - Location of Additional section
pub(crate) const HEADER_LOC_ARCOUNT: usize = 10;

/// DNS message header.
#[derive(Clone, Copy, Default, PartialEq, Eq)]
#[repr(transparent)]
pub struct Header([u8; Self::LEN]);

impl Header {
    /// DNS message header length.
    pub const LEN: usize = 12;
    fn header_u16(&self, index: usize) -> u16 {
        u16::from_be_bytes([self.0[index], self.0[index + 1]])
    }
    fn set_header_u16(&mut self, index: usize, v: u16) -> &mut Self {
        self.0[index..index + 2].copy_from_slice(&v.to_be_bytes()[..]);
        self
    }
    fn header_bit(&self, f: HeaderBit) -> bool {
        self.0[f.index] & f.mask != 0
    }
    fn set_header_bit(&mut self, f: HeaderBit, v: bool) -> &mut Self {
        if v {
            self.0[f.index] |= f.mask;
        } else {
            self.0[f.index] &= !f.mask;
        }
        self
    }
    /// Zero this `Header`.
    pub fn clear(&mut self) -> &mut Self {
        for b in &mut self.0 {
            *b = 0;
        }
        self
    }
    /// Construct a `Header` from a slice of at least 12 bytes. This will panic if a short slice is supplied.
    pub fn from_bytes(b: &[u8]) -> &Header {
        assert!(b.len() >= Self::LEN);
        unsafe { &*(b.as_ptr() as *const Header) }
    }
    /// Construct a mutable `Header` from a slice of at least 12 bytes. This will panic if a short slice is supplied.
    pub fn from_bytes_mut(b: &mut [u8]) -> &mut Header {
        assert!(b.len() >= Self::LEN);
        unsafe { &mut *(b.as_mut_ptr() as *mut Header) }
    }
    /// Returns the message ID
    pub fn id(&self) -> u16 {
        self.header_u16(0)
    }
    /// Sets the message ID
    pub fn set_id(&mut self, id: u16) -> &mut Self {
        self.set_header_u16(HEADER_LOC_ID, id)
    }
    /// Returns the QR (Query Response) bit
    pub fn qr(&self) -> bool {
        self.header_bit(HEADER_BIT_QR)
    }
    /// Set the QR (Query Response) bit
    pub fn set_qr(&mut self, qr: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_QR, qr)
    }
    /// Returns the message OPCODE
    pub fn opcode(&self) -> Opcode {
        Opcode::from_u8((self.0[2] & 0b0111_1000) >> 3).unwrap()
    }
    /// Sets the message OPCODE
    pub fn set_opcode(&mut self, opcode: Opcode) -> &mut Self {
        self.0[2] &= 0b1000_0111;
        self.0[2] |= opcode.to_u8() << 3;
        self
    }
    /// Returns the AA (Authoritative Answer) bit
    pub fn aa(&self) -> bool {
        self.header_bit(HEADER_BIT_AA)
    }
    /// Sets the AA (Authoritative Answer) bit
    pub fn set_aa(&mut self, aa: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_AA, aa)
    }
    /// Returns the TC (Truncated) bit
    pub fn tc(&self) -> bool {
        self.header_bit(HEADER_BIT_TC)
    }
    /// Sets the TC (Truncated) bit
    pub fn set_tc(&mut self, tc: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_TC, tc)
    }
    /// Returns the RD (Recursion Desired) bit
    pub fn rd(&self) -> bool {
        self.header_bit(HEADER_BIT_RD)
    }
    /// Sets the RD (Recursion Desired) bit
    pub fn set_rd(&mut self, rd: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_RD, rd)
    }
    /// Returns the RA (Recursion Available) bit
    pub fn ra(&self) -> bool {
        self.header_bit(HEADER_BIT_RA)
    }
    /// Sets the RA (Recursion Available) bit
    pub fn set_ra(&mut self, ra: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_RA, ra)
    }
    /// Returns the Z (Reserved) bit
    pub fn z(&self) -> bool {
        self.header_bit(HEADER_BIT_Z)
    }
    /// Sets the Z (Reserved) bit
    pub fn set_z(&mut self, z: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_Z, z)
    }
    /// Returns the message RCODE
    pub fn rcode(&self) -> Rcode {
        Rcode::from_u8(0b0000_1111 & self.0[3]).unwrap()
    }
    /// Sets the message RCODE
    pub fn set_rcode(&mut self, rcode: Rcode) -> &mut Self {
        self.0[3] &= 0b1111_0000;
        self.0[3] |= rcode.to_u8();
        self
    }
    /// Returns the AD (Authentic Data) bit
    pub fn ad(&self) -> bool {
        self.header_bit(HEADER_BIT_AD)
    }
    /// Sets the AD (Authentic Data) bit
    pub fn set_ad(&mut self, ad: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_AD, ad)
    }
    /// Returns the CD (Checking Disabled) bit
    pub fn cd(&self) -> bool {
        self.header_bit(HEADER_BIT_CD)
    }
    /// Sets the CD (Checking Disabled) bit
    pub fn set_cd(&mut self, cd: bool) -> &mut Self {
        self.set_header_bit(HEADER_BIT_CD, cd)
    }
    /// Returns the QDCOUNT (Question section)
    pub fn qdcount(&self) -> u16 {
        self.header_u16(HEADER_LOC_QDCOUNT)
    }
    /// Sets the QDCOUNT (Question section)
    pub fn set_qdcount(&mut self, count: u16) -> &mut Self {
        self.set_header_u16(HEADER_LOC_QDCOUNT, count)
    }
    /// Returns the ANCOUNT (Answer section)
    pub fn ancount(&self) -> u16 {
        self.header_u16(HEADER_LOC_ANCOUNT)
    }
    /// Sets the ANCOUNT (Answer section)
    pub fn set_ancount(&mut self, count: u16) -> &mut Self {
        self.set_header_u16(HEADER_LOC_ANCOUNT, count)
    }
    /// Returns the NSCOUNT (Authority section)
    pub fn nscount(&self) -> u16 {
        self.header_u16(HEADER_LOC_NSCOUNT)
    }
    /// Sets the NSCOUNT (Authority section)
    pub fn set_nscount(&mut self, count: u16) -> &mut Self {
        self.set_header_u16(HEADER_LOC_NSCOUNT, count)
    }
    /// Returns the ARCOUNT (Additional section)
    pub fn arcount(&self) -> u16 {
        self.header_u16(HEADER_LOC_ARCOUNT)
    }
    /// Sets the ARCOUNT (Additional section)
    pub fn set_arcount(&mut self, count: u16) -> &mut Self {
        self.set_header_u16(HEADER_LOC_ARCOUNT, count)
    }
    /// Returns the header as a byte slice
    pub fn as_bytes(&self) -> &[u8] {
        &self.0[..]
    }
}

impl Debug for Header {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Header")
            .field("id", &self.id())
            .field("qr", &self.qr())
            .field("opcode", &self.opcode())
            .field("aa", &self.aa())
            .field("tc", &self.tc())
            .field("rd", &self.rd())
            .field("ra", &self.ra())
            .field("z", &self.z())
            .field("ad", &self.ad())
            .field("cd", &self.cd())
            .field("rcode", &self.rcode())
            .field("qdcount", &self.qdcount())
            .field("ancount", &self.ancount())
            .field("nscount", &self.nscount())
            .field("arcount", &self.arcount())
            .finish()
    }
}

// TODO: if it works out, do an impl for &'a Header
impl<'a> Unpack<'a> for Header {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        <[u8; Self::LEN]>::unpack(reader).map(Header)
    }
}

impl UnpackedLen for Header {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        <[u8; Self::LEN]>::unpacked_len(reader)
    }
}

impl Pack for Header {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        dst.write_bytes(self.as_bytes())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(12)
    }
}
