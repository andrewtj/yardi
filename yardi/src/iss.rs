use core::{
    cmp::Ordering,
    hash::{Hash, Hasher},
};

use crate::ascii::{Present, PresentError, Presenter};

// TODO: should this be pub?
#[derive(Debug, Clone, Copy)]
pub struct ISS<'a>(pub &'a str);

impl<'a> From<&'a str> for ISS<'a> {
    fn from(s: &'a str) -> Self {
        ISS(s)
    }
}

impl<'a> Hash for ISS<'a> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        for c in self.0.chars().flat_map(|c| c.to_lowercase()) {
            c.hash(state);
        }
    }
}

impl<'a> Eq for ISS<'a> {}

impl<'a> PartialEq for ISS<'a> {
    fn eq(&self, other: &Self) -> bool {
        self.0.as_bytes().eq_ignore_ascii_case(other.0.as_bytes())
    }
}

impl<'a> PartialEq<[u8]> for ISS<'a> {
    fn eq(&self, other: &[u8]) -> bool {
        self.0.as_bytes().eq_ignore_ascii_case(other)
    }
}

impl<'a> PartialEq<ISS<'a>> for &'a [u8] {
    fn eq(&self, other: &ISS<'_>) -> bool {
        other.eq(*self)
    }
}

impl<'a> PartialEq<str> for ISS<'a> {
    fn eq(&self, other: &str) -> bool {
        self.eq(other.as_bytes())
    }
}

impl<'a> PartialEq<ISS<'a>> for &'a str {
    fn eq(&self, other: &ISS<'_>) -> bool {
        other.eq(*self)
    }
}

impl<'a> Ord for ISS<'a> {
    fn cmp(&self, other: &Self) -> Ordering {
        let mut a_iter = self.0.as_bytes().iter().map(|b| b.to_ascii_lowercase());
        let mut b_iter = other.0.as_bytes().iter().map(|b| b.to_ascii_lowercase());
        loop {
            let a = a_iter.next();
            let b = b_iter.next();
            match a.cmp(&b) {
                Ordering::Equal => {
                    if a.is_none() {
                        return Ordering::Equal;
                    }
                }
                other => return other,
            }
        }
    }
}

impl<'a> PartialOrd for ISS<'a> {
    fn partial_cmp(&self, other: &ISS<'_>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Present for ISS<'a> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        self.0.present(p)
    }
}
