#![cfg_attr(not(feature = "std"), no_std)]
// TODO: can we do a Name collect from an Iterator of strs/[u8] that returns Result<Name, _>?
// TODO: check for off-by-ones in buffer sizes
// TODO: codegen, alloc, inline-bytes, fallible alloc
// TODO: repr(transparent) like uncased: https://docs.rs/uncased/latest/src/uncased/borrowed.rs.html
// TODO: use uncased instead of iss
// TODO: use uncased + phf for mnemonic parsing
// TODO: dynamic field accessors for rdata
// TODO: DnskeyFlags and probably others don't benefit from the struct name appearing in Debug;
//       also DnskeyFlags in particular should have SEP shortened.
// TODO: NameCache that stores last 100 lru2`name: (last_used, next_to_last_used)` and some common
//       static names that can be supplied to Reader/Parser to reduce allocations.
// TODO: revisit errors - make it possible to give good rustc-like errors with position,
//       underlining, etc
// TODO: revisit unpacking - makt it possible to introspect the unpack process, so that a GUI
//       packet dissection is possible
// TODO: core/alloc/std features; Ipv{4,6}Addr are in core now
// TODO: posix re https://pubs.opengroup.org/onlinepubs/9699919799.2016edition/
// TODO: what would alloc free unpack/parse impls look like?
// TODO: make ring optional and substitutable
// TODO: errors should be guarded on the size of common Results
// TODO: would a present_len be useful?
// TODO: standalone validate?
// TODO: custom formatting for parenthesis/whitespace?
// TODO: non-ascii parse modes: ascii-only, idna names, utf8 (idna names + best effort utf8 in
//       charstrings/etc)
// TODO: ascii pull w/ Option<enum Connected { Dot /* Period? */, Equal }>
// TODO: look at where From<[u8]> might better be From<Vec<u8>>; stop leaning
//       on the stack so hard, and try to work out what an alloc free
//       impl (without dnssec/etc) might look like.
// TODO: draft/experimental stuff should be behind a feature
// TODO: make use of split_at_checked where it'd help
// TODO: Helpers for byte marshalling in tuples? (CharStr<MIN=0,MAX=255<Prefix8<MIN=0, MAX=255, &[u8]>>>, ..)
// TODO: look at moving datatypes contained in rdata::* to datatypes module
// TODO: breadcrumb for errors: linked-list on stack
// TODO: setters/getters for svcb
#![warn(
    missing_debug_implementations,
    bare_trait_objects,
    unused_lifetimes,
    unused_qualifications,
//    missing_docs,
)]
// TODO: Clippy.
// TODO: Warns about associated constants which don't default to static lifetimes (yet?).
#![allow(clippy::redundant_static_lifetimes)]
//! *yardi* is a DNS toolbox.

#[macro_use]
extern crate yardi_derive;
#[macro_use]
pub(crate) extern crate alloc;
#[doc(hidden)]
pub use yardi_derive::*;

#[macro_use]
mod macros;

mod iss;
mod tuple_impls;
#[macro_use]
pub mod datatypes;
// TODO: rename text?
pub mod ascii;
pub mod dnssec;
// TODO: what to expose or not expose?
pub mod encoding;
pub mod errors;
pub mod msg;
pub mod nametree;
pub mod rdata;
pub mod ssa;
pub(crate) mod vec16;
pub mod wire;
pub mod zone;

// TODO: is this useful?
pub const MEDIA_TYPE: &str = "text/dns";

// TOOD: move this somewhere more appropriate
#[cfg(feature = "false")]
#[test]
fn unpack_error_message() {
    use crate::ascii::{self, Context, Parse};
    use crate::wire::{Pack, Reader, Unpack, UnpackedLen, Writer};
    use rdata::{Caa, Srv, Txt, Uri};
    let bytes: &[u8] = &[0x00, 0xFF, 0xFF >> 1];
    for b in bytes {
        let temp: &[u8] = &[*b; 8];
        let mut r = Reader::new(bytes::Bytes::copy_from_slice(temp));
        if let Err(err) = Srv::unpack(&mut r) {
            println!("{:?} => {}", err, err);
        } else {
            println!(" !! {} succeeded", b);
        }
        r.set_index(0);
        if let Err(err) = Srv::unpacked_len(&mut r) {
            println!("{:?} => {}", err, err);
        } else {
            println!(" !! {} succeeded", b);
        }
    }
    let overlarge_txt: &'static [u8] = &[0u8; 0xFFFF + 1];
    let mut r = Reader::new(bytes::Bytes::from_static(overlarge_txt));
    if let Err(err) = Txt::unpack(&mut r) {
        println!("{:?} => {}", err, err);
    } else {
        println!(" !! TXT succeeded");
    }
    let mut r = Reader::new(bytes::Bytes::from_static(&[0]));
    if let Err(err) = datatypes::Ty::unpack(&mut r) {
        println!("{:?} => {}", err, err);
    } else {
        println!(" !! TY succeeded");
    }
    let context = Context::default();
    let samples = &[
        "0 0 80 example..com.",
        "0 0 80 a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.",
        r"\# 8 0000 1111 2222  F F F F",
        r"\# 8 0000 1111 2222 02 1 1",
        r"\# 8 0000 1111 2222 00 11",
        r"\# 8 0000 1111 2222 0011  ",
        r"\# 8 0000 1111 2222 00   11  ",
    ];
    for s in samples {
        // TODO: should be a parse_str short hand or something
        if let Err(err) = ascii::parse::from_single_entry::<Srv>(s.as_bytes(), &context) {
            let off = err.position().unwrap().byte as usize;
            println!("{}\n{:?} => {} ({:?})", s, err, err, s.get(off..));
        } else {
            println!("!! PARSE succeeded");
        }
    }
    dbg!(Txt::default().pack(&mut Writer::with_capacity(0)));
    {
        let mut uri = Uri::default();
        uri.target = vec![0; 0xFFFF].into();
        dbg!(uri.pack(&mut Writer::with_capacity(0xFFFF * 2)));
    }
    {
        let mut caa = Caa::default();
        caa.tag = bytes::Bytes::from_static(&[0]);
        dbg!(caa.pack(&mut Writer::with_capacity(0xFFFF * 2)));
    }

    panic!();
}

// TODO: the fuzz fn should be shared with the fuzz harness directly
#[cfg(feature = "std")]
#[test]
fn hf_rdata_wire_replay() {
    use crate::{
        ascii,
        datatypes::{Class, Ty},
        rdata::{self, Generic},
        wire::{Reader, Unpack, Writer},
    };
    use std::fs;
    let dir = "../fuzz/hfuzz_workspace/hf_rdata_wire";
    for entry in fs::read_dir(dir).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        let Some(extension) = path.extension() else {
            continue;
        };
        if extension != "fuzz" {
            continue;
        }
        let input = fs::read(path).unwrap();
        fuzz(&input);
    }
    pub fn fuzz(input: &[u8]) {
        let mut r = Reader::new(input);
        let (cl, ty): (Class, Ty) = match Unpack::unpack(&mut r) {
            Ok(tup) => tup,
            Err(_) => return,
        };
        let rdata_bytes = match r.get_bytes_rest() {
            Ok(bytes) => bytes,
            Err(_) => return,
        };
        let mut temp_string = String::with_capacity(0xFFFF);
        let mut temp_bytes = vec![0u8; 0xFFFF];
        let r = Reader::new(rdata_bytes);
        let unpacked = match rdata::unpacked_len(cl, ty, &mut r.clone()) {
            Ok(rdlen) => {
                assert!(
                    cl.is_data() && ty.is_data(),
                    "unpacked_len succeded for non data ty or cl: {}/{}\n{:?}",
                    ty,
                    cl,
                    rdata_bytes
                );
                assert!(rdlen <= 0xFFFF, "rdlen > 0xFFFF");
                let mut w = Writer::new(&mut temp_bytes);
                if let Err(err) = rdata::copy(cl, ty, &mut r.clone(), &mut w) {
                    panic!(
                        "copy failed after unpacked_len succeeded for {}/{}: {:?}\n{:?}",
                        cl, ty, err, rdata_bytes
                    );
                }
                true
            }
            Err(_) => {
                let mut w = Writer::new(&mut temp_bytes);
                if rdata::copy(cl, ty, &mut r.clone(), &mut w).is_ok() {
                    panic!(
                        "copy succeeded after unpacked_len failed for {}/{}\n{:?}",
                        cl, ty, rdata_bytes
                    );
                }
                false
            }
        };
        match <Generic>::unpack(cl, ty, &mut r.clone()) {
            Ok(ref rdata) if unpacked => {
                let mut w = Writer::new(&mut temp_bytes);
                if let Err(err) = rdata.pack(&mut w) {
                    panic!(
                        "Generic pack failed for {}/{} {:?}\n{:?}",
                        cl, ty, err, rdata_bytes
                    );
                }
                if let Err(err) = ascii::present::to_string(&rdata, &mut temp_string) {
                    panic!(
                        "Failed to present for {}/{} {:?}\n{:?}",
                        cl, ty, err, rdata_bytes
                    );
                }
                match Generic::from_str(cl, ty, &temp_string) {
                    Ok(rdata2) => {
                        assert_eq!(
                            *rdata, rdata2,
                            "Parse/present mismatch for {}/{}\n{:?}",
                            cl, ty, rdata_bytes
                        );
                    }
                    Err(err) => {
                        panic!(
                            "Failed to parse what was presented for {}/{}: {:?}\n{:?}\n{:?}",
                            cl, ty, err, rdata_bytes, temp_string,
                        );
                    }
                }
            }
            Ok(_) => {
                panic!(
                    "Generic::unpack succeeded when it shouldn't have {}/{}\n{:?}",
                    cl, ty, rdata_bytes
                );
            }
            Err(ref err) if unpacked => {
                panic!(
                    "Generic::unpack failed when it shouldn't have {}/{}: {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            Err(_) => (),
        }
    }
}

// TODO: needs a lookover
#[cfg(feature = "std")]
#[test]
fn hf_ascii_replay() {
    use crate::{
        ascii::{
            parse::{self, Entry},
            Context,
        },
        msg::Rr,
        zone::{OriginDirective, TtlDirective, ZoneEntry},
    };
    use std::fs;
    let dir = "../fuzz/hfuzz_workspace/hf_ascii";
    for entry in fs::read_dir(dir).unwrap() {
        let entry = entry.unwrap();
        let path = entry.path();
        let Some(extension) = path.extension() else {
            continue;
        };
        if extension != "fuzz" {
            continue;
        }
        let input = fs::read(path).unwrap();
        match std::str::from_utf8(&input) {
            Ok(s) => eprintln!("fuzz input is utf8: {s:?}"),
            Err(err) => {
                let (g, b) = input.split_at(err.valid_up_to());
                let g = std::str::from_utf8(g).unwrap();
                eprintln!("fuzz input is not utf8: {g:?} {b:?}");
            }
        };
        fuzz(&input);
    }
    pub fn fuzz(mut input: &[u8]) {
        let mut c = Context::default();
        let mut cur_pos = parse::Position::default();
        while !input.is_empty() {
            let value = match parse::from_entry::<ZoneEntry<'_>>(input, cur_pos, &c, true) {
                Ok(Entry::End) => return,
                Ok(Entry::Value { read, pos, value }) => {
                    input = &input[read..];
                    cur_pos = pos;
                    value
                }
                Ok(Entry::Empty { read, pos }) => {
                    input = &input[read..];
                    cur_pos = pos;
                    continue;
                }
                Ok(Entry::Short) => unreachable!(),
                Err(_) => return,
            };
            match value {
                ZoneEntry::Origin(OriginDirective(origin)) => {
                    c.set_origin(origin);
                }
                ZoneEntry::Include(_) => (),
                ZoneEntry::Rr(Rr { name, .. }) => {
                    c.set_last_owner(name);
                }
                ZoneEntry::Ttl(TtlDirective(ttl)) => {
                    c.set_default_ttl(ttl);
                }
            }
        }
    }
}
