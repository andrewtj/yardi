//! RSA Support.
//!
//! This module provides support for [RSA/SHA-1] and [RSA/SHA-2] signatures.
//!
//! RSASHA1, RSASHA1-NSEC3-SHA1, and RSASHA256 with 1,024 to 4,096 bit keys
//! and RSASHA512 with 2,048 to 4,096 bit keys are are supported for
//! verification.
//!
//!
//! RSASHA256 and RSASHA512 with 2,048 to 4,096 bit keys are supported for
//! signing.
//!
//! [RSA/SHA-1]: https://tools.ietf.org/html/rfc3110
//! [RSA/SHA-2]: https://tools.ietf.org/html/rfc5702
#[cfg(test)]
mod test;

use alloc::borrow::Cow;
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

use ring::signature::{
    RSA_PKCS1_1024_8192_SHA1_FOR_LEGACY_USE_ONLY, RSA_PKCS1_1024_8192_SHA256_FOR_LEGACY_USE_ONLY,
    RSA_PKCS1_1024_8192_SHA512_FOR_LEGACY_USE_ONLY,
};

use crate::dnssec::{PrivKeyError, PrivateKeyMap, PubKeyError, Verify};
use crate::wire::{Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, Writer};

use core::{fmt, fmt::Debug};

use crate::dnssec::{SigAlg, Sign, Signature};

const MOD_KEY: &str = "Modulus";
const PUBEXP_KEY: &str = "PublicExponent";
const PRIVEXP_KEY: &str = "PrivateExponent";
const PRIME1_KEY: &str = "Prime1";
const PRIME2_KEY: &str = "Prime2";
const EXP1_KEY: &str = "Exponent1";
const EXP2_KEY: &str = "Exponent2";
const COEFF_KEY: &str = "Coefficient";

// "For interoperability, the exponent and modulus are each limited to 4096 bits in
// length."
// "The size of n ... MUST be not less than 512 bits and not more than 4096 bits."
const N_MIN: usize = 512 / 8;
const N_MAX: usize = 4096 / 8;
const E_MIN: usize = 1;
const E_MAX: usize = 4096 / 8;

fn check_e<T, E>(t: T) -> Result<T, E>
where
    T: AsRef<[u8]>,
    E: From<BadComponent>,
{
    match t.as_ref() {
        s if s.len() < E_MIN || s.len() > E_MAX => Err(BadComponent),
        &[0, ..] => Err(BadComponent),
        // LSB must be 1
        &[.., last] if last.trailing_zeros() > 0 => Err(BadComponent),
        // must be at least two bits
        &[1] => Err(BadComponent),
        _ => Ok(t),
    }
    .map_err(Into::into)
}

fn check_n<T, E>(t: T) -> Result<T, E>
where
    T: AsRef<[u8]>,
    E: From<BadComponent>,
{
    match t.as_ref() {
        s if s.len() < N_MIN || s.len() > N_MAX => return Err(BadComponent.into()),
        s if s[0] == 0 => return Err(BadComponent.into()),
        // LSB must be 1
        s if s[s.len() - 1].trailing_zeros() > 0 => return Err(BadComponent.into()),
        _ => (),
    }
    Ok(t)
}

#[derive(Debug)]
struct BadComponent;

impl From<BadComponent> for PackError {
    fn from(BadComponent: BadComponent) -> Self {
        PackError::value_invalid()
    }
}

impl From<BadComponent> for UnpackError {
    fn from(BadComponent: BadComponent) -> Self {
        UnpackError::value_invalid()
    }
}

impl From<BadComponent> for PubKeyError {
    fn from(BadComponent: BadComponent) -> Self {
        PubKeyError::Bad
    }
}

/// Components of an RSA Public Key.
///
/// This is a helper for marshaling to and from the public key format
/// originally defined in [RFC 2537 Section 2][1].
///
/// [1]: https://tools.ietf.org/html/rfc2537#section-2
// TODO: Type params.
#[derive(Clone, Debug)]
pub struct RsaPublicComponents {
    /// Modulus.
    pub n: Vec<u8>,
    /// Public Exponent.
    pub e: Vec<u8>,
}

impl RsaPublicComponents {
    /// Unpack an `RsaPublicComponents`.
    pub fn from_bytes(bytes: &[u8]) -> Result<Self, UnpackError> {
        Self::unpack(&mut Reader::new(bytes))
    }
}

impl Pack for RsaPublicComponents {
    fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        if self.e.len() <= 0xFF {
            crate::datatypes::bytes::pack8(&self.e, w, E_MIN, E_MAX)?;
        } else {
            crate::datatypes::bytes::pack16(&self.e, w, E_MIN, E_MAX)?;
        }
        crate::datatypes::bytes::pack(&self.n, w, N_MIN, N_MAX)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        check_e::<_, PackError>(&self.e)?;
        check_n::<_, PackError>(&self.n)?;
        let e_prefix = match self.e.len() {
            l if l <= 0xFF => 1,
            _ => 2,
        };
        Ok(e_prefix + self.e.len() + self.n.len())
    }
}

impl UnpackedLen for RsaPublicComponents {
    fn unpacked_len(r: &mut Reader<'_>) -> Result<usize, UnpackError> {
        let initial = r.index();
        let e_len = match r.get_byte()? {
            0 => u16::unpack(r)? as usize,
            l => l as usize,
        };
        r.get_bytes_map(e_len, |s| check_e(s).map(drop))?;
        r.get_bytes_map(r.unread(), |s| check_n(s).map(drop))?;
        Ok(r.index() - initial)
    }
}

// TODO: this is a candidate for a borrowing impl
impl<'a> Unpack<'a> for RsaPublicComponents {
    fn unpack(r: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let e_len = match r.get_byte()? {
            0 => u16::unpack(r)? as usize,
            l => l as usize,
        };
        let e: Vec<u8> = r.get_bytes(e_len).and_then(check_e)?.to_vec();
        let n: Vec<u8> = r.get_bytes_rest().and_then(check_n)?.to_vec();
        Ok(RsaPublicComponents { n, e })
    }
}

/// Components of an RSA Private Key.
#[derive(Clone, Debug)]
pub struct RsaPrivateComponents<'a> {
    /// Modulus.
    pub n: Cow<'a, [u8]>,
    /// Public Exponent.
    pub e: Cow<'a, [u8]>,
    /// Private Exponent.
    pub d: Cow<'a, [u8]>,
    /// Prime 1.
    pub p: Cow<'a, [u8]>,
    /// Prime 2.
    pub q: Cow<'a, [u8]>,
    /// Exponent 1.
    pub dp: Cow<'a, [u8]>,
    /// Exponent 2.
    pub dq: Cow<'a, [u8]>,
    /// Coefficient.
    pub qinv: Cow<'a, [u8]>,
}

impl<'a> RsaPrivateComponents<'a> {
    /// Create a `RsaPrivateComponents` from a `PrivateKeyMap`.
    ///
    /// Values are not validated beyond checking that they're present.
    pub fn from_map(map: &PrivateKeyMap<'_>) -> Result<Self, PrivKeyError> {
        if map.format.0 != 1 || map.format.1 < 2 {
            return Err(PrivKeyError::UnsupportedFormat);
        }
        Ok(RsaPrivateComponents {
            n: map.get_b64(MOD_KEY)?.into(),
            e: map.get_b64(PUBEXP_KEY)?.into(),
            d: map.get_b64(PRIVEXP_KEY)?.into(),
            p: map.get_b64(PRIME1_KEY)?.into(),
            q: map.get_b64(PRIME2_KEY)?.into(),
            dp: map.get_b64(EXP1_KEY)?.into(),
            dq: map.get_b64(EXP2_KEY)?.into(),
            qinv: map.get_b64(COEFF_KEY)?.into(),
        })
    }
}

macro_rules! impl_verifier {
    ($struct:ident, $structs:expr, $algs:expr, $alg:ident, $ring_param:ident) => {
        #[doc = "Verify"]
        #[doc=$algs]
        #[doc = "signatures."]
        #[derive(Clone)]
        pub struct $struct {
            components: RsaPublicComponents,
        }
        impl Debug for $struct {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.debug_struct(stringify!($struct))
                    .field("components", &self.components)
                    .finish()
            }
        }
        impl $struct {
            #[doc = "Create an `"]
            #[doc = $structs]
            #[doc = "` from an `RsaPublicComponents`."]
            pub fn from_components(c: &RsaPublicComponents) -> Result<Self, PubKeyError> {
                let n = check_n::<_, PubKeyError>(c.n.clone())?;
                let e = check_e::<_, PubKeyError>(c.e.clone())?;
                // *ring* supports up to 33-bits in e
                let e_bits = (8 - e[0].leading_zeros() as usize) + (e.len() - 1) * 8;
                if e_bits > 33 {
                    return Err(PubKeyError::Unsupported);
                }
                // *ring* supports 1024 to 8192 bit keys
                // (check_n has checked `n` is <= 4096)
                if n.len() < 1024 / 8 {
                    return Err(PubKeyError::Unsupported);
                }
                Ok($struct {
                    components: RsaPublicComponents { n, e },
                })
            }
        }
        impl Verify for $struct {
            fn new(bytes: &[u8]) -> Result<$struct, PubKeyError> {
                let c = RsaPublicComponents::from_bytes(bytes).map_err(|_| PubKeyError::Bad)?;
                $struct::from_components(&c)
            }
            fn verify(&self, data: &[u8], sig: &[u8]) -> Result<(), ()> {
                ring::signature::RsaPublicKeyComponents {
                    n: &self.components.n[..],
                    e: &self.components.e[..],
                }
                .verify(&$ring_param, data, sig)
                .map_err(|_| ())
            }
        }
    };
    ($struct:ident, $alg:ident, $ring_param:ident) => {
        impl_verifier!(
            $struct,
            stringify!($struct),
            stringify!($alg),
            $alg,
            $ring_param
        );
    };
}
impl_verifier!(
    RsaSha1Verifier,
    RSASHA1,
    RSA_PKCS1_1024_8192_SHA1_FOR_LEGACY_USE_ONLY
);
impl_verifier!(
    RsaSha256Verifier,
    RSASHA256,
    RSA_PKCS1_1024_8192_SHA256_FOR_LEGACY_USE_ONLY
);
impl_verifier!(
    RsaSha512Verifier,
    RSASHA512,
    RSA_PKCS1_1024_8192_SHA512_FOR_LEGACY_USE_ONLY
);

macro_rules! impl_signer {
    ($struct:ident, $structs:expr, $algs:expr, $alg:ident, $ring_enc:ident) => {
        #[doc = "Create"]
        #[doc=$algs]
        #[doc = "signatures."]
        pub struct $struct {
            key_pair: ::ring::signature::RsaKeyPair,
        }
        impl Debug for $struct {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.write_str($structs)
            }
        }
        impl $struct {
            #[doc = "Construct an `"]
            #[doc=$structs]
            #[doc = "` from an `RsaPrivateComponents`."]
            pub fn from_components(
                components: &RsaPrivateComponents<'_>,
            ) -> Result<Self, PrivKeyError> {
                let components = ring::rsa::KeyPairComponents {
                    public_key: ring::rsa::PublicKeyComponents {
                        n: &components.n,
                        e: &components.e,
                    },
                    d: &components.d,
                    p: &components.p,
                    q: &components.q,
                    dP: &components.dp,
                    dQ: &components.dq,
                    qInv: &components.qinv,
                };
                let key_pair = ring::rsa::KeyPair::from_components(&components)
                    .map_err(|_| PrivKeyError::BadKey)?;
                return Ok($struct { key_pair });
            }
        }
        impl Sign for $struct {
            fn new(map: &PrivateKeyMap<'_>, _pub_key: &[u8]) -> Result<Self, PrivKeyError> {
                if map.alg() != SigAlg::$alg {
                    return Err(PrivKeyError::UnknownKey);
                }
                let components = RsaPrivateComponents::from_map(map)?;
                $struct::from_components(&components)
            }
            fn sig_len(&self) -> usize {
                self.key_pair.public().modulus_len()
            }
            fn sign(&mut self, data: &[u8]) -> Result<Signature, ()> {
                let rng = ::ring::rand::SystemRandom::new();
                Signature::new(self.sig_len(), |s| {
                    self.key_pair
                        .sign(&::ring::signature::$ring_enc, &rng, data, s)
                        .map_err(|::ring::error::Unspecified| ())
                })
            }
        }
    };
    ($struct:ident, $alg:ident, $ring_enc:ident) => {
        impl_signer!(
            $struct,
            stringify!($struct),
            stringify!($alg),
            $alg,
            $ring_enc
        );
    };
}
impl_signer!(RsaSha256Signer, RSASHA256, RSA_PKCS1_SHA256);
impl_signer!(RsaSha512Signer, RSASHA512, RSA_PKCS1_SHA512);
