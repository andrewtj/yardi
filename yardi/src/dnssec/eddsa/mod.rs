//! EdDSA Support.
//!
//! [Edwards-Curve Digital Security Algorithm (EdDSA)][1] support. At present
//! only ED25519 is supported.
//!
//! [1]: https://tools.ietf.org/html/rfc8080
#[cfg(test)]
mod test;

use core::fmt::{self, Debug};

use super::{PrivKeyError, PrivateKeyMap, PubKeyError, Sign, Signature, Verify};
use crate::dnssec::sigalg::SigAlg;

use ring::signature::Ed25519KeyPair;

const PRIVKEY_KEY: &str = "PrivateKey";

/// Verify ED25519 signatures.
#[derive(Clone, Debug)]
pub struct Ed25519Verifier {
    key: [u8; 32],
}

impl Verify for Ed25519Verifier {
    fn new(data: &[u8]) -> Result<Self, PubKeyError> {
        if data.len() != 32 {
            return Err(PubKeyError::Bad);
        }
        let mut key = [0u8; 32];
        key[..].copy_from_slice(data);
        Ok(Ed25519Verifier { key })
    }
    fn verify(&self, data: &[u8], sig: &[u8]) -> Result<(), ()> {
        ring::signature::UnparsedPublicKey::new(&::ring::signature::ED25519, &self.key[..])
            .verify(data, sig)
            .map_err(|_| ())
    }
}

/// Create ED25519 signatures.
pub struct Ed25519Signer {
    keypair: Ed25519KeyPair,
}

impl Ed25519Signer {
    /// Construct an `Ed25519Signer` from `private` and `public` components.
    pub fn from_components(private: &[u8], public: &[u8]) -> Result<Self, PrivKeyError> {
        if private.len() != 32 || public.len() != 32 {
            return Err(PrivKeyError::BadKey);
        }
        Ed25519KeyPair::from_seed_and_public_key(private, public)
            .map(|keypair| Ed25519Signer { keypair })
            .map_err(|_| PrivKeyError::BadKey)
    }
}

impl Debug for Ed25519Signer {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Signer(eddsa)").finish()
    }
}

impl Sign for Ed25519Signer {
    fn new(map: &PrivateKeyMap<'_>, public: &[u8]) -> Result<Self, PrivKeyError> {
        if map.format.0 != 1 || map.format.1 < 2 {
            return Err(PrivKeyError::UnsupportedFormat);
        }
        if map.alg != SigAlg::ED25519 {
            return Err(PrivKeyError::UnsupportedKey);
        }
        let private = map.get_b64(PRIVKEY_KEY)?;
        Self::from_components(&private[..], public)
    }
    fn sig_len(&self) -> usize {
        64
    }
    fn sign(&mut self, data: &[u8]) -> Result<Signature, ()> {
        Ok(Signature::from_ring_sig(self.keypair.sign(data)))
    }
}
