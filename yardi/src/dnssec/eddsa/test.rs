#[test]
#[cfg_attr(miri, ignore)]
fn verify_ed25519_256() {
    dnssec_verify_test!("ed25519_256_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn verify_ed25519_256_rfc_a() {
    dnssec_verify_test!("ed25519_rfc8080a_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn verify_ed25519_256_rfc_b() {
    dnssec_verify_test!("ed25519_rfc8080b_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn sign_ed25519_256() {
    dnssec_sign_test!("ed25519_256_rr.txt", "ed25519_256_private.txt");
}
