#[cfg(not(feature = "std"))]
use alloc::string::String;

use core::{cmp::Ordering, str::FromStr};

use super::*;
use crate::ascii;
use crate::datatypes::Name;
use crate::msg::Rr;
use crate::rdata::{Cname, Dnskey, Ds, Generic, Nsec3param, Rrsig, StaticRdata, Txt};
use crate::wire::{Pack, Reader, Unpack, WireOrd, Writer};

#[test]
fn key_tag_rsamd5() {
    let key_str = "256 3 1 AwEAActd+olTXD0R8WTN9YlHpBUWJ7LWnTMzZKLesR/TKCU+kIym5cZH YmeQPgBTZE247AnYkGhCC4Sl+1PXvCyPbBk=";
    let key = Dnskey::from_str(key_str).expect("Dnskey");
    assert_eq!(key.tag(), 36716);
}

#[test]
fn key_tag_ecdsap256sha256() {
    let key_str = "257 3 13 GojIhhXUN/u4v54ZQqGSnyhWJwaubCvTmeexv7bR6edbkrSqQpF64cYbcB7wNcP+e+MAnLr+Wi9xMWyQLc8NAA==";
    let key = Dnskey::from_str(key_str).expect("Dnskey");
    assert_eq!(key.tag(), 55648);
}

#[test]
fn canonical_name_equality() {
    let a: &[u8] = b"\x07example\x03com\x00";
    let b: &[u8] = b"\x07EXAMPLE\x03COM\x00";
    for &(a, b) in [(a, b), (b, a)].iter() {
        let mut a = Reader::new(a.into());
        let mut b = Reader::new(b.into());
        assert_eq!(
            Ordering::Equal,
            Cname::cmp_wire_canonical(&mut a, &mut b).unwrap()
        );
    }
}

#[test]
fn left_justified() {
    let a: &[u8] = b"\x01a\x02ac\x00";
    let b: &[u8] = b"\x01a\x02ca\x00";
    for &(a, b, o) in [(a, b, Ordering::Less), (b, a, Ordering::Greater)].iter() {
        let mut a = Reader::new(a.into());
        let mut b = Reader::new(b.into());
        assert_eq!(o, <Txt>::cmp_wire_canonical(&mut a, &mut b).unwrap());
    }
}

#[test]
fn absence_before_zero() {
    let a: &[u8] = b"\x01a";
    let b: &[u8] = b"\x01a\x00";
    for &(a, b, o) in [(a, b, Ordering::Less), (b, a, Ordering::Greater)].iter() {
        let mut a = Reader::new(a.into());
        let mut b = Reader::new(b.into());
        assert_eq!(o, <Txt>::cmp_wire_canonical(&mut a, &mut b).unwrap());
    }
}

macro_rules! dnssec_verify_test {
    ($filename:expr) => {{
        let rr_str = include_str!($filename);
        crate::dnssec::test::verify_sample(rr_str)
    }};
}

macro_rules! dnssec_sign_test {
    ($rr_filename:expr, $private_filename:expr) => {{
        let rr_str = include_str!($rr_filename);
        let private_str = include_str!($private_filename);
        crate::dnssec::test::exercise_signer(rr_str, private_str);
    }};
}

fn get_rr<T: StaticRdata + for<'a> Unpack<'a>>(rrs: &[Rr<Generic>]) -> Rr<T> {
    rrs.iter()
        .filter_map(|rr| {
            if rr.data.ty() == T::TY {
                let data = T::unpack(&mut rr.data.reader()).expect("dnskey");
                Some(Rr {
                    name: rr.name.clone(),
                    class: rr.class,
                    ttl: rr.ttl,
                    data: data,
                })
            } else {
                None
            }
        })
        .next()
        .expect("rr")
}

fn rrs_from_ascii(mut ascii: &[u8]) -> Result<Vec<Rr<Generic>>, ascii::ParseError> {
    let mut v = Vec::new();
    let context = ascii::Context::shared_default();
    let mut pos = ascii::parse::Position::default();
    while !ascii.is_empty() {
        let (read, new_pos) = match ascii::parse::from_entry(ascii, pos, context, true)? {
            ascii::parse::Entry::End => break,
            ascii::parse::Entry::Value { read, pos, value } => {
                v.push(value);
                (read, pos)
            }
            ascii::parse::Entry::Empty { read, pos } => (read, pos),
            ascii::parse::Entry::Short => unreachable!(),
        };
        ascii = &ascii[read..];
        pos = new_pos;
    }
    Ok(v)
}

pub fn verify_sample(rrs_str: &str) {
    let rrs = rrs_from_ascii(rrs_str.as_bytes()).expect("rrs");
    let dk_rr = get_rr::<Dnskey>(&rrs);
    let ds_rr = get_rr::<Ds>(&rrs);
    let rrsig_rr = get_rr::<Rrsig>(&rrs);
    let signed_rr = rrs
        .iter()
        .filter(|rr| rr.data.ty() == rrsig_rr.data.ty_covered)
        .next()
        .expect("signed rr");
    assert_eq!(ds_rr.data.key_tag, dk_rr.data.tag());
    let mut buf = [0u8; 4096];
    let buf = {
        let mut w = Writer::new(&mut buf[..]);
        w.set_format(crate::wire::Format::Dnssec);
        rrsig_rr
            .data
            .pack_no_sig(&mut w)
            .expect("write digest data");
        rrsig_rr.name.pack(&mut w).expect("write owner name");
        rrsig_rr.data.ty_covered.pack(&mut w).expect("write ty");
        rrsig_rr.class.pack(&mut w).expect("write class");
        rrsig_rr.data.original_ttl.pack(&mut w).expect("write ttl");
        signed_rr.data.pack16(&mut w).expect("write data");
        w.into_written()
    };
    let verifier = boxed_verify(dk_rr.data.alg, &dk_rr.data.public_key).expect("verifier");
    assert!(verifier.verify(&buf, &rrsig_rr.data.signature).is_ok());
}

pub fn exercise_signer(rr_str: &str, priv_str: &str) {
    let rrs = rrs_from_ascii(rr_str.as_bytes()).expect("rrs");
    let dk_rr = get_rr::<Dnskey>(&rrs);
    let pkmap = PrivateKeyMap::new(&priv_str).expect("pkmap");
    let mut signer = boxed_sign(&pkmap, &dk_rr.data.public_key).expect("signer");
    let sig = signer.sign(b"test").expect("sign");
    let verifier = boxed_verify(dk_rr.data.alg, &dk_rr.data.public_key).expect("verifier");
    assert!(verifier.verify(b"test", sig.as_ref()).is_ok());
}

#[test]
#[cfg_attr(miri, ignore)]
fn rfc5011_hash_names() {
    let name_to_hash: &[(Name, &'static str)] = &[
        (name!("example."), "0p9mhaveqvm6t7vbl5lop2u3t2rp3tom"),
        (name!("a.example."), "35mthgpgcu1qg68fab165klnsnk3dpvl"),
        (name!("ai.example."), "gjeqe526plbf1g8mklp59enfd789njgi"),
        (name!("ns1.example."), "2t7b4g4vsa5smi47k61mv5bv1a22bojr"),
        (name!("ns2.example."), "q04jkcevqvmu85r014c7dkba38o0ji5r"),
        (name!("w.example."), "k8udemvp1j2f7eg6jebps17vp3n8i58h"),
        (name!("*.w.example."), "r53bq7cc2uvmubfu5ocmm6pers9tk9en"),
        (name!("x.w.example."), "b4um86eghhds6nea196smvmlo4ors995"),
        (name!("y.w.example."), "ji6neoaepv8b5o6k4ev33abha8ht9fgc"),
        (name!("x.y.w.example."), "2vptu5timamqttgl4luu9kg21e0aor3s"),
        (name!("xx.example."), "t644ebqk9bibcna874givr6joj62mlhv"),
        (
            name!("2t7b4g4vsa5smi47k61mv5bv1a22bojr.example."),
            "kohar7mbb8dc2ce8a9qvl8hon4k53uhi",
        ),
    ];
    let n3p = Nsec3param::from_str("1 0 12 aabbccdd").expect("parse param");
    for &(ref owner, hash) in name_to_hash.iter() {
        let digest = nsec3_hash(n3p.hash_alg, owner, &n3p.salt, n3p.iterations).expect("hash name");
        let mut digest_b32 = String::new();
        ascii::present::to_string(Base32Helper(digest.as_ref()), &mut digest_b32).unwrap();
        assert_eq!(digest_b32, hash);
    }
    struct Base32Helper<'a>(&'a [u8]);
    impl ascii::Present for Base32Helper<'_> {
        fn present(&self, p: &mut ascii::Presenter<'_, '_>) -> Result<(), ascii::PresentError> {
            crate::datatypes::bytes::present_base32hexnp(self.0, p, 0, !0)
        }
    }
}
