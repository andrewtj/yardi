//! DNSSEC support.
#[cfg(test)]
#[macro_use]
mod test;

#[cfg(not(feature = "std"))]
use alloc::{boxed::Box, vec::Vec};
use core::fmt::{self, Debug};
use core::str::FromStr;

pub use self::sigalg::SigAlg;

use crate::datatypes::{Name, Ty};
use crate::rdata::nsec3::Nsec3HashAlg;

pub mod ecdsa;
pub mod eddsa;
pub mod rsa;
mod sigalg;

use crate::iss::ISS;

const FMT_KEY: &str = "Private-key-format";
const ALG_KEY: &str = "Algorithm";

const CREATED_KEY: &str = "Created";
const PUBLISH_KEY: &str = "Publish";
const ACTIVATE_KEY: &str = "Activate";
const REVOKE_KEY: &str = "Revoke";
const INACTIVE_KEY: &str = "Inactive";
const DELETE_KEY: &str = "Delete";

/// Returns true if a type should be verified in canonical format.
///
/// Sourced from and [RFC 4034 Section 6.2] and [RFC 6840 Section 5.1].
///
/// [RFC 4034 Section 6.2]:
///   https://tools.ietf.org/html/rfc4034#section-6.2
///   "RFC 4034: Resource Records for the DNS Security Extensions"
/// [RFC 6840 Section 5.1]:
///   https://tools.ietf.org/html/rfc6840#section-5.1
///   "Clarifications and Implementation Notes for DNS Security (DNSSEC)"
pub fn canonical(ty: Ty) -> bool {
    matches!(
        ty,
        Ty::A6
            | Ty::AFSDB
            | Ty::CNAME
            | Ty::DNAME
            | Ty::KX
            | Ty::MB
            | Ty::MD
            | Ty::MF
            | Ty::MG
            | Ty::MINFO
            | Ty::MR
            | Ty::MX
            | Ty::NAPTR
            | Ty::NS
            | Ty::NXT
            | Ty::PTR
            | Ty::PX
            | Ty::RP
            | Ty::RRSIG
            | Ty::RT
            | Ty::SIG
            | Ty::SOA
    )
}

/// Calculates a key tag for a public key.
pub fn key_tag(flags: u16, protocol: u8, alg: SigAlg, public_key: &[u8]) -> u16 {
    // defined in RFC 4034 - Appendix B (and errata)
    if alg == SigAlg::RSAMD5 {
        // For RSA/MD5, third to last and second to last octets of public modulus
        // (last in public_key).
        let pub_len = public_key.len();
        return if pub_len > 3 {
            u16::from_be_bytes([public_key[pub_len - 3], public_key[pub_len - 2]])
        } else {
            // not correct but neither is the key
            0
        };
    }
    let acc = flags
        .to_be_bytes()
        .iter()
        .cloned()
        .chain(Some(protocol))
        .chain(Some(alg.0))
        .chain(public_key.iter().cloned())
        .enumerate()
        .map(|(i, b)| {
            if i & 1 != 0 {
                u32::from(b)
            } else {
                u32::from(b) << 8
            }
        })
        .sum::<u32>();
    let (a, b) = ((acc >> 16) as u16, acc as u16);
    a.wrapping_add(b)
}

/// Construct an implementation of `Sign` behind a `Box`.
// TODO: Better type for public_key?
pub fn boxed_sign(
    map: &PrivateKeyMap<'_>,
    public_key: &[u8],
) -> Result<Box<dyn Sign>, PrivKeyError> {
    fn as_box<S: 'static + Sign>(s: S) -> Box<dyn Sign> {
        Box::new(s) as Box<dyn Sign>
    }
    match map.alg {
        SigAlg::RSASHA256 => rsa::RsaSha256Signer::new(map, public_key).map(as_box),
        SigAlg::RSASHA512 => rsa::RsaSha512Signer::new(map, public_key).map(as_box),
        SigAlg::ECDSAP256SHA256 => ecdsa::EcdsaP256Sha256Signer::new(map, public_key).map(as_box),
        SigAlg::ECDSAP384SHA384 => ecdsa::EcdsaP384Sha384Signer::new(map, public_key).map(as_box),
        SigAlg::ED25519 => eddsa::Ed25519Signer::new(map, public_key).map(as_box),
        _ => Err(PrivKeyError::UnsupportedKey),
    }
}

/// Construct an implementation of `Verify` behind a `Box`.
// TODO: better type for public_key
pub fn boxed_verify(alg: SigAlg, public_key: &[u8]) -> Result<Box<dyn Verify>, PubKeyError> {
    fn as_box<V: 'static + Verify>(v: V) -> Box<dyn Verify> {
        Box::new(v) as Box<dyn Verify>
    }
    match alg {
        SigAlg::RSASHA1 | SigAlg::RSASHA1_NSEC3_SHA1 => {
            rsa::RsaSha1Verifier::new(public_key).map(as_box)
        }
        SigAlg::RSASHA256 => rsa::RsaSha256Verifier::new(public_key).map(as_box),
        SigAlg::RSASHA512 => rsa::RsaSha512Verifier::new(public_key).map(as_box),
        SigAlg::ECDSAP256SHA256 => ecdsa::EcdsaP256Sha256Verifier::new(public_key).map(as_box),
        SigAlg::ECDSAP384SHA384 => ecdsa::EcdsaP384Sha384Verifier::new(public_key).map(as_box),
        SigAlg::ED25519 => {
            eddsa::Ed25519Verifier::new(public_key).map(|k| Box::new(k) as Box<dyn Verify>)
        }
        _ => Err(PubKeyError::Unknown),
    }
}

/// Contains a digest.
#[derive(Debug)]
pub struct Digest {
    inner: ::ring::digest::Digest,
}

impl Digest {
    fn from_ring_digest(digest: ::ring::digest::Digest) -> Self {
        Digest { inner: digest }
    }
}

impl AsRef<[u8]> for Digest {
    fn as_ref(&self) -> &[u8] {
        self.inner.as_ref()
    }
}

/// Contains a signature.
#[derive(Debug)]
pub struct Signature {
    inner: SignatureInner,
}

impl Signature {
    fn from_ring_sig(sig: ::ring::signature::Signature) -> Self {
        Signature {
            inner: SignatureInner::RingSig(sig),
        }
    }
    fn new<E, F>(len: usize, f: F) -> Result<Signature, E>
    where
        F: FnOnce(&mut [u8]) -> Result<(), E>,
    {
        let mut inner = SignatureInner::new(len);
        f(inner.as_bytes_mut()).map(|_| Signature { inner })
    }
}

impl From<Vec<u8>> for Signature {
    fn from(sig: Vec<u8>) -> Self {
        Signature {
            inner: SignatureInner::Vec(sig),
        }
    }
}

impl<'a> From<&'a [u8]> for Signature {
    fn from(sig: &[u8]) -> Self {
        let mut inner = SignatureInner::new(sig.len());
        inner.as_bytes_mut().copy_from_slice(sig);
        Signature { inner }
    }
}

impl AsRef<[u8]> for Signature {
    fn as_ref(&self) -> &[u8] {
        self.inner.as_ref()
    }
}

enum SignatureInner {
    Array {
        len: u8,
        buf: [u8; SignatureInner::ARRAY_LEN],
    },
    Vec(Vec<u8>),
    RingSig(::ring::signature::Signature),
}

impl SignatureInner {
    // TODO: this looks hacky
    const ARRAY_LEN: usize = size_of::<::ring::signature::Signature>() - 1;
    fn new(len: usize) -> SignatureInner {
        if len > SignatureInner::ARRAY_LEN {
            SignatureInner::Vec(vec![0u8; len])
        } else {
            SignatureInner::Array {
                len: len as u8,
                buf: [0u8; Self::ARRAY_LEN],
            }
        }
    }
    fn as_bytes_mut(&mut self) -> &mut [u8] {
        match *self {
            SignatureInner::Array { len, ref mut buf } => &mut buf[..len as usize],
            SignatureInner::Vec(ref mut sig) => &mut sig[..],
            SignatureInner::RingSig(_) => unreachable!(),
        }
    }
}

impl Debug for SignatureInner {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        self.as_ref().fmt(f)
    }
}

impl AsRef<[u8]> for SignatureInner {
    fn as_ref(&self) -> &[u8] {
        match *self {
            SignatureInner::Array { len, ref buf } => &buf[..len as usize],
            SignatureInner::Vec(ref sig) => &sig[..],
            SignatureInner::RingSig(ref sig) => sig.as_ref(),
        }
    }
}

/// Create a public key signature.
pub trait Sign {
    /// Construct `Self` from a `PrivateKeyMap` and a public key in DNS wire format.
    fn new(map: &PrivateKeyMap<'_>, pub_key: &[u8]) -> Result<Self, PrivKeyError>
    where
        Self: Sized;
    /// Return the maximum length of a signature.
    fn sig_len(&self) -> usize;
    /// Create a signature.
    fn sign(&mut self, data: &[u8]) -> Result<Signature, ()>;
}

/// Verify a public key signature.
pub trait Verify {
    /// Construct from a public key in DNS wire format.
    fn new(data: &[u8]) -> Result<Self, PubKeyError>
    where
        Self: Sized;
    /// Verify a signature.
    fn verify(&self, data: &[u8], sig: &[u8]) -> Result<(), ()>;
}

/// A helper for accessing fields in DNS private key text format.
///
/// Field names are assumed to be case insensitive. The value for the format
/// field is assumed to be in the form "major.minor". Only the numeric part
/// of the "Algorithm" field is intrepreted.
#[derive(Debug)]
pub struct PrivateKeyMap<'a> {
    format: (u8, u8),
    alg: SigAlg,
    kv: Vec<(ISS<'a>, &'a str)>,
}

impl<'a> PrivateKeyMap<'a> {
    /// Create a new `PrivateKeyMap` from a string.
    pub fn new(s: &'a str) -> Result<Self, PrivKeyError> {
        let mut kv: Vec<(ISS<'a>, &'a str)> = Vec::new();
        let mut format: Option<(u8, u8)> = None;
        let mut alg: Option<SigAlg> = None;
        for l in s.lines() {
            if l.is_empty() {
                continue;
            }
            let sep = l.chars().take_while(|&c| c != ':').count();
            let key = ISS::from(l[..sep].trim());
            let value = l[sep + 1..].trim();
            if key == ISS(FMT_KEY) {
                if format.is_some() {
                    return Err(PrivKeyError::DuplicateKey);
                }
                let mut fmt_iter = value.as_bytes().iter().cloned();
                let fmt_err = PrivKeyError::NoValue { key: FMT_KEY };
                if Some(b'v') != fmt_iter.next() {
                    return Err(fmt_err);
                }
                let mut major = 0u8;
                let mut minor = 0u8;
                for (i, t) in [&mut major, &mut minor][..].iter_mut().enumerate() {
                    loop {
                        match fmt_iter.next() {
                            Some(b'.') if i == 0 => break,
                            Some(b @ b'0'..=b'9') => {
                                **t = (*t)
                                    .checked_mul(10)
                                    .and_then(|n| n.checked_add(b - b'0'))
                                    .ok_or(fmt_err)?;
                            }
                            None => break,
                            _ => return Err(fmt_err),
                        }
                    }
                }
                format = Some((major, minor));
            } else if key == ISS(ALG_KEY) {
                if alg.is_some() {
                    return Err(PrivKeyError::DuplicateKey);
                }
                let alg_err = PrivKeyError::BadValue { key: ALG_KEY };
                if value.is_empty() {
                    return Err(alg_err);
                }
                let mut temp = 0u8;
                for &b in value.as_bytes() {
                    match b {
                        b' ' => break,
                        b @ b'0'..=b'9' => {
                            temp = temp
                                .checked_mul(10)
                                .and_then(|a| a.checked_add(b - b'0'))
                                .ok_or(alg_err)?;
                        }
                        _ => return Err(alg_err),
                    }
                }
                alg = Some(SigAlg(temp));
            } else {
                let Err(i) = kv.binary_search_by(|probe| probe.0.cmp(&key)) else {
                    return Err(PrivKeyError::DuplicateKey);
                };
                kv.insert(i, (key, value));
            }
        }
        Ok(PrivateKeyMap {
            format: format.ok_or(PrivKeyError::NoValue { key: FMT_KEY })?,
            alg: alg.ok_or(PrivKeyError::NoValue { key: ALG_KEY })?,
            kv,
        })
    }
    /// Return format as a tuple `(major, minor)`.
    pub fn format(&self) -> (u8, u8) {
        self.format
    }
    /// Return algorithm as a `SigAlg`.
    pub fn alg(&self) -> SigAlg {
        self.alg
    }
    /// Get the content of a field as a `str`.
    pub fn get(&self, key: &'static str) -> Result<&'a str, PrivKeyError> {
        self.kv
            .iter()
            .find_map(|probe| (probe.0 == ISS(key)).then_some(probe.1))
            .ok_or(PrivKeyError::NoValue { key })
    }
    // TODO: not sure this should be public API?
    /// Get the content of a field containing Base64 as a `Vec<u8>`.
    pub fn get_b64(&self, key: &'static str) -> Result<Vec<u8>, PrivKeyError> {
        use base64::engine::{general_purpose::STANDARD, Engine};
        STANDARD
            .decode(self.get(key)?)
            .map_err(|_| PrivKeyError::BadValue { key })
    }
}

/// Result of verifying a signature.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum VerifyResult {
    /// Valid signature.
    Valid,
    /// Invalid signature.
    Invalid,
    /// Invalid key.
    InvalidKey,
    /// Unsupported key.
    UnsupportedKey,
}

/// Verify a signature.
pub fn verify(alg: SigAlg, public: &[u8], data: &[u8], sig: &[u8]) -> VerifyResult {
    match alg {
        SigAlg::RSASHA1 | SigAlg::RSASHA1_NSEC3_SHA1 => {
            verify_imp::<rsa::RsaSha1Verifier>(public, data, sig)
        }
        SigAlg::RSASHA256 => verify_imp::<rsa::RsaSha256Verifier>(public, data, sig),
        SigAlg::RSASHA512 => verify_imp::<rsa::RsaSha512Verifier>(public, data, sig),
        SigAlg::ECDSAP256SHA256 => verify_imp::<ecdsa::EcdsaP256Sha256Verifier>(public, data, sig),
        SigAlg::ECDSAP384SHA384 => verify_imp::<ecdsa::EcdsaP384Sha384Verifier>(public, data, sig),
        SigAlg::ED25519 => verify_imp::<eddsa::Ed25519Verifier>(public, data, sig),
        _ => VerifyResult::UnsupportedKey,
    }
}

fn verify_imp<T: Verify>(public: &[u8], data: &[u8], sig: &[u8]) -> VerifyResult {
    let verifier = match T::new(public) {
        Ok(v) => v,
        _ => return VerifyResult::UnsupportedKey,
    };
    if verifier.verify(data, sig).is_ok() {
        VerifyResult::Valid
    } else {
        VerifyResult::Invalid
    }
}

/// An error returned from substantiating a public key.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum PubKeyError {
    /// Invalid key.
    Bad,
    /// Key's algorithm is unknown.
    Unknown,
    /// Key's algorithm is known but this key is not supported.
    Unsupported,
}

/// An error returned from substantiating a private key.
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum PrivKeyError {
    /// A required value is missing.
    NoValue {
        /// Missing key
        key: &'static str,
    },
    /// A value is invalid.
    BadValue {
        /// Key with bad value
        key: &'static str,
    },
    /// Key is invalid.
    BadKey,
    /// Key is a duplicate,
    DuplicateKey,
    /// Key's algorithm is unknown.
    UnknownKey,
    /// Key's algorithm is known but this key is not supported.
    UnsupportedKey,
    /// Key is in a format this is not understood.
    UnsupportedFormat,
}

/// Private key timing metadata
#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct TimingMetadata {
    /// Time created
    pub created: Option<u32>,
    /// Time published
    pub publish: Option<u32>,
    /// Time of activation
    pub activate: Option<u32>,
    /// Time of revocation
    pub revoke: Option<u32>,
    /// Time of inactivation
    pub inactive: Option<u32>,
    /// Time of deletion
    pub delete: Option<u32>,
}

impl TimingMetadata {
    /// Construct a `TimingMetadata` from a `PrivateKeyMap`.
    pub fn from_map(map: &PrivateKeyMap<'_>) -> Result<Self, PrivKeyError> {
        fn get_ts(map: &PrivateKeyMap<'_>, key: &'static str) -> Result<Option<u32>, PrivKeyError> {
            let s = match map.get(key) {
                Ok(s) => s,
                Err(PrivKeyError::NoValue { .. }) => return Ok(None),
                Err(err) => return Err(err),
            };
            let ts = u32::from_str(s).map_err(|_| PrivKeyError::BadValue { key })?;
            Ok(Some(ts))
        }
        Ok(TimingMetadata {
            created: get_ts(map, CREATED_KEY)?,
            publish: get_ts(map, PUBLISH_KEY)?,
            activate: get_ts(map, ACTIVATE_KEY)?,
            revoke: get_ts(map, REVOKE_KEY)?,
            inactive: get_ts(map, INACTIVE_KEY)?,
            delete: get_ts(map, DELETE_KEY)?,
        })
    }
}

/// Calculate an [NSEC3][1] hash.
///
/// Returns `None` if `alg` is unsupported or iterations is greater than 2,500.
/// See `nsec3_hash_expensive` for an unlimited variant.
///
/// [1]: https://tools.ietf.org/html/rfc5155
pub fn nsec3_hash(alg: Nsec3HashAlg, name: &Name, salt: &[u8], iterations: u16) -> Option<Digest> {
    if iterations > 2500 {
        None
    } else {
        nsec3_hash_expensive(alg, name, salt, iterations)
    }
}

/// Calculate an expensive [NSEC3][1] hash.
///
/// The number of iterations in [calculating a hash][2] is represented by a
/// `u16` so there is potential for this to be an expensive call.
/// [RFC 5155 Section 10.3][3] discusses limiting the maximum number of
/// iterations. See `nsec3_hash` for a capped variant.
///
/// `None` is returned if `alg` is unsupported.
///
/// [1]: https://tools.ietf.org/html/rfc5155
/// [2]: https://tools.ietf.org/html/rfc5155#section-5
/// [3]: https://tools.ietf.org/html/rfc5155#section-10.3
pub fn nsec3_hash_expensive(
    alg: Nsec3HashAlg,
    name: &Name,
    salt: &[u8],
    iterations: u16,
) -> Option<Digest> {
    let digest_alg = match alg {
        Nsec3HashAlg::SHA_1 => &::ring::digest::SHA1_FOR_LEGACY_USE_ONLY,
        _ => return None,
    };
    let mut digest = {
        let mut context = ::ring::digest::Context::new(digest_alg);
        let mut temp = [0u8; 64];
        for l in name.labels().rev() {
            temp[0] = l.len() as u8;
            let lb = l.iter().map(u8::to_ascii_lowercase);
            for (d, b) in temp[1..].iter_mut().zip(lb) {
                *d = b;
            }
            context.update(&temp[..1 + l.len()]);
        }
        context.update(&[0]);
        context.update(salt);
        context.finish()
    };
    for _ in 0..iterations {
        let mut context = ::ring::digest::Context::new(digest_alg);
        context.update(digest.as_ref());
        context.update(salt);
        digest = context.finish();
    }
    Some(Digest::from_ring_digest(digest))
}
