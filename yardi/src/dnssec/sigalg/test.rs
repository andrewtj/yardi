#[cfg(not(feature = "std"))]
use alloc::string::ToString;
use core::str::FromStr;

use super::SigAlg;

#[test]
fn str_marshal() {
    assert_eq!("13", SigAlg::ECDSAP256SHA256.to_string());
    assert_eq!(
        SigAlg::ECDSAP256SHA256,
        FromStr::from_str("ECDSAP256SHA256").unwrap()
    );
    assert_eq!(
        SigAlg::ECDSAP256SHA256,
        FromStr::from_str("ecdsap256sha256").unwrap()
    );
    assert_eq!(SigAlg::ECDSAP256SHA256, FromStr::from_str("13").unwrap());
}
