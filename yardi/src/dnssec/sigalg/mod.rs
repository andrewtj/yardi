mod generated;
#[cfg(test)]
mod test;

use core::{
    fmt::{self, Debug, Display},
    str::FromStr,
};

use crate::ascii::{Parse, ParseError, Parser};

impl From<u8> for SigAlg {
    fn from(alg: u8) -> Self {
        SigAlg(alg)
    }
}

/// DNSSEC Signature Algorithm.
///
/// `SigAlg` is always presented as a decimal integer. A mnemonic or decimal
/// integer string can be parsed into an `SigAlg`. DS, CDS and DLV records
/// require `SigAlg` to be specified as a decimal integer but this is not
/// enforced at present.
#[derive(
    Clone,
    Copy,
    Default,
    Hash,
    Eq,
    PartialEq,
    Ord,
    PartialOrd,
    Pack,
    Unpack,
    UnpackedLen,
    WireOrd,
    Present,
)]
#[yardi(crate = "crate")]
pub struct SigAlg(pub u8);

impl Parse for SigAlg {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(Self::from_str)
    }
}

impl Display for SigAlg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl Debug for SigAlg {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for (a, s) in generated::MNEMONICS {
            if a == self {
                return write!(f, "SigAlg({})", s);
            }
        }
        write!(f, "SigAlg({})", self.0)
    }
}

impl FromStr for SigAlg {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match generated::from_str(s) {
            Some(v) => Ok(v),
            None => crate::datatypes::int::parse_u8(s).map(SigAlg),
        }
    }
}
