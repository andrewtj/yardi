use crate::{msg::Rr, rdata::Key};
use core::str::FromStr;

#[test]
#[cfg_attr(miri, ignore)]
fn verify_ecdsap256sha256_256() {
    dnssec_verify_test!("ecdsap256sha256_256_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn verify_ecdsap256sha256_256_rfc() {
    dnssec_verify_test!("ecdsap256sha256_rfc6605_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn sign_ecdsap256sha256_256() {
    dnssec_sign_test!(
        "ecdsap256sha256_256_rr.txt",
        "ecdsap256sha256_256_private.txt"
    );
}

#[test]
#[cfg_attr(miri, ignore)]
fn verify_ecdsap384sha384_384() {
    dnssec_verify_test!("ecdsap384sha384_384_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn verify_ecdsap384sha384_384_rfc() {
    dnssec_verify_test!("ecdsap384sha384_rfc6605_rr.txt");
}

#[test]
#[cfg_attr(miri, ignore)]
fn sign_ecdsap384sha384_384() {
    dnssec_sign_test!(
        "ecdsap384sha384_384_rr.txt",
        "ecdsap384sha384_384_private.txt"
    );
}

#[test]
#[cfg_attr(miri, ignore)]
fn ecdsap256sha256_small_private() {
    let key_str = "sig0-ecdsap256sha256.example. 0 IN KEY 512 3 13 IlIG2oTDdcyT2pXxQlbCHy0Fx1wLdLExABhJfcVXPMsgoIvBBAAOktmE 8D3bX4aHnHLDae7lTyFo/Nz9lAOYnQ==";
    let private_str = r#"
Private-key-format: v1.3
Algorithm: 13 (ECDSAP256SHA256)
PrivateKey: 4nx0JBrLAhLJTMgi6/beqA2liM9PZND8KZCVexcM4g==
Created: 20180622020742
Publish: 20180622020712
Activate: 20180622020712"#;
    let key_rr: Rr<Key> = FromStr::from_str(key_str).expect("key rr");
    let pkey_map = crate::dnssec::PrivateKeyMap::new(private_str).expect("pkey map");
    let verifier =
        crate::dnssec::boxed_verify(key_rr.data.alg, &key_rr.data.public_key).expect("verifier");
    let mut signer = crate::dnssec::boxed_sign(&pkey_map, &key_rr.data.public_key).expect("signer");
    let sig_input = b"abc123";
    let sig = signer.sign(sig_input).expect("sign");
    verifier.verify(sig_input, sig.as_ref()).expect("verify");
}
