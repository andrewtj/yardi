//! ECDSA Support.
//!
//! [Elliptic Curve Digital Signature Algorithm (DSA)][1] support. Both
//! ECDSAP256SHA256 and ECDSAP384SHA384 are supported.
//!
//! [1]: https://tools.ietf.org/html/rfc6605
#[cfg(test)]
mod test;

use alloc::borrow::Cow;
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;
use core::{fmt, fmt::Debug};

use crate::dnssec::{PrivKeyError, PrivateKeyMap, PubKeyError, SigAlg, Sign, Signature, Verify};

use ring::signature::EcdsaKeyPair;

const PRIVKEY_KEY: &str = "PrivateKey";

macro_rules! impl_verifier {
    ($struct:ident, $algs:expr, $alg:ident, $size:expr, $ring_va:ident) => {
        #[doc = "Verify"]
        #[doc=$algs]
        #[doc = "signatures."]
        #[derive(Clone)]
        pub struct $struct {
            key: [u8; 1 + $size],
        }
        impl $struct {
            // TODO: redundant?
            fn new_slice(data: &[u8]) -> Result<Self, PubKeyError> {
                if data.len() > $size {
                    return Err(PubKeyError::Bad);
                }
                let mut key = [0u8; 1 + $size];
                key[0] = 4;
                key[1 + $size - data.len()..].copy_from_slice(data);
                Ok($struct { key })
            }
        }
        impl Debug for $struct {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                f.debug_struct(stringify!($struct))
                    .field("key", &&self.key[..])
                    .finish()
            }
        }
        impl Verify for $struct {
            fn new(data: &[u8]) -> Result<Self, PubKeyError> {
                $struct::new_slice(&data[..])
            }
            fn verify(&self, data: &[u8], sig: &[u8]) -> Result<(), ()> {
                ring::signature::UnparsedPublicKey::new(&ring::signature::$ring_va, &self.key[..])
                    .verify(data, sig)
                    .map_err(|_| ())
            }
        }
    };
    ($struct:ident, $alg:ident, $size:expr, $ring_va:ident) => {
        impl_verifier!($struct, stringify!($alg), $alg, $size, $ring_va);
    };
}

impl_verifier!(
    EcdsaP256Sha256Verifier,
    ECDSAP256SHA256,
    64,
    ECDSA_P256_SHA256_FIXED
);
impl_verifier!(
    EcdsaP384Sha384Verifier,
    ECDSAP384SHA384,
    96,
    ECDSA_P384_SHA384_FIXED
);

// TODO: do this on the stack instead
fn zero_pad_private(v: &mut Vec<u8>, size: usize) {
    if v.len() < size {
        let need = size - v.len();
        v.reserve(need);
        unsafe {
            let start = v.as_mut_ptr();
            let dest = start.add(need);
            let l = v.len();
            core::ptr::copy(start, dest, l);
            v.set_len(l + need);
        }
        for b in v[..need].iter_mut() {
            *b = 0;
        }
    }
}

macro_rules! impl_signer {
    ($struct:ident, $structs:expr, $vstruct:ident, $algs:expr, $alg:ident, $size:expr, $ring_alg:ident) => {
        #[doc = "Create"]
        #[doc=$algs]
        #[doc = "signatures."]
        #[derive(Debug)]
        pub struct $struct {
            keypair: EcdsaKeyPair,
        }
        impl $struct {
            const PUBLIC_LEN: usize = $size;
            const PRIVATE_LEN: usize = Self::PUBLIC_LEN / 2;
            #[doc = "Construct an `"]
            #[doc=$structs]
            #[doc = "` from `private` and `public` components."]
            pub fn from_components(private: &[u8], public: &[u8]) -> Result<Self, PrivKeyError> {
                let mut private = Cow::Borrowed(private);
                if private.len() > Self::PRIVATE_LEN {
                    return Err(PrivKeyError::BadKey);
                }
                let verifier = $vstruct::new_slice(public).map_err(|_| PrivKeyError::BadKey)?;
                if private.len() < Self::PRIVATE_LEN {
                    zero_pad_private(private.to_mut(), Self::PRIVATE_LEN);
                }
                debug_assert_eq!(private.len(), Self::PRIVATE_LEN);
                let ring_alg = &::ring::signature::$ring_alg;
                let rand = ring::rand::SystemRandom::new();
                EcdsaKeyPair::from_private_key_and_public_key(
                    ring_alg,
                    &private,
                    &verifier.key[..],
                    &rand,
                )
                .map(|keypair| $struct { keypair })
                .map_err(|_| PrivKeyError::BadKey)
            }
        }
        impl Sign for $struct {
            fn new(map: &PrivateKeyMap<'_>, public: &[u8]) -> Result<Self, PrivKeyError> {
                if map.format.0 != 1 || map.format.1 < 2 {
                    return Err(PrivKeyError::UnsupportedFormat);
                }
                if map.alg() != SigAlg::$alg {
                    return Err(PrivKeyError::UnknownKey);
                }
                let private = map.get_b64(PRIVKEY_KEY).map(|mut v| {
                    zero_pad_private(&mut v, Self::PRIVATE_LEN);
                    v
                })?;
                debug_assert_eq!(private.len(), Self::PRIVATE_LEN);
                Self::from_components(&private[..], &public[..])
            }
            fn sig_len(&self) -> usize {
                Self::PUBLIC_LEN
            }
            fn sign(&mut self, data: &[u8]) -> Result<Signature, ()> {
                let rng = ::ring::rand::SystemRandom::new();
                self.keypair
                    .sign(&rng, data)
                    .map(Signature::from_ring_sig)
                    .map_err(|::ring::error::Unspecified| ())
            }
        }
    };
    ($struct:ident, $vstruct:ident, $alg:ident, $size:expr, $ring_alg:ident) => {
        impl_signer!(
            $struct,
            stringify!($struct),
            $vstruct,
            stringify!($alg),
            $alg,
            $size,
            $ring_alg
        );
    };
}
impl_signer!(
    EcdsaP256Sha256Signer,
    EcdsaP256Sha256Verifier,
    ECDSAP256SHA256,
    64,
    ECDSA_P256_SHA256_FIXED_SIGNING
);
impl_signer!(
    EcdsaP384Sha384Signer,
    EcdsaP384Sha384Verifier,
    ECDSAP384SHA384,
    96,
    ECDSA_P384_SHA384_FIXED_SIGNING
);
