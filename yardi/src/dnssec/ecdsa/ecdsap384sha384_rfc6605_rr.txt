example.net. 3600 IN DNSKEY 257 3 14 (
       xKYaNhWdGOfJ+nPrL8/arkwf2EY3MDJ+SErKivBVSum1
       w/egsXvSADtNJhyem5RCOpgQ6K8X1DRSEkrbYQ+OB+v8
       /uX45NBwY8rp65F6Glur8I/mlVNgF6W/qTI37m40 )

example.net. 3600 IN DS 10771 14 4 (
       72d7b62976ce06438e9c0bf319013cf801f09ecc84b8
       d7e9495f27e305c6a9b0563a9b5f4d288405c3008a94
       6df983d6 )

www.example.net. 3600 IN A 192.0.2.1
www.example.net. 3600 IN RRSIG A 14 3 3600 (
       20100909102025 20100812102025 10771 example.net.
       /L5hDKIvGDyI1fcARX3z65qrmPsVz73QD1Mr5CEqOiLP
       95hxQouuroGCeZOvzFaxsT8Glr74hbavRKayJNuydCuz
       WTSSPdz7wnqXL5bdcJzusdnI0RSMROxxwGipWcJm )