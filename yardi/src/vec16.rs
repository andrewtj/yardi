#[cfg(not(feature = "std"))]
use alloc::vec::Vec;
use core::{fmt, mem, ops, ptr, slice};

trait Checked16 {
    fn checked_u16(self) -> u16;
}

impl Checked16 for usize {
    fn checked_u16(self) -> u16 {
        assert!(self <= 0xFFFF);
        self as u16
    }
}

pub(crate) struct Vec16<T> {
    ptr: *mut T,
    cap: u16,
    len: u16,
}

unsafe impl<T> Send for Vec16<T> where T: Send {}
unsafe impl<T> Sync for Vec16<T> where T: Sync {}

impl<T> Vec16<T> {
    pub fn new() -> Self {
        let mut v = <Vec<T>>::new();
        let ptr = v.as_mut_ptr();
        let cap = v.capacity().checked_u16();
        let len = v.len().checked_u16();
        mem::forget(v);
        Vec16 { ptr, cap, len }
    }
    pub fn with_capacity(capacity: usize) -> Self {
        assert!(capacity <= 0xFFFF);
        let mut v = <Vec<T>>::with_capacity(capacity);
        let ptr = v.as_mut_ptr();
        let cap = v.capacity().checked_u16();
        let len = v.len().checked_u16();
        mem::forget(v);
        Vec16 { ptr, cap, len }
    }
    pub fn capacity(&self) -> usize {
        self.cap as usize
    }
    pub fn as_slice(&self) -> &[T] {
        unsafe { slice::from_raw_parts(self.ptr, self.len as usize) }
    }
    pub fn as_mut_slice(&mut self) -> &mut [T] {
        unsafe { slice::from_raw_parts_mut(self.ptr, self.len as usize) }
    }
    pub unsafe fn set_len(&mut self, len: usize) {
        assert!(len <= self.cap as usize);
        self.len = len.checked_u16();
    }
    fn maybe_reserve(&mut self) {
        if self.cap > self.len {
            return;
        }
        let next = (self.cap as usize + 1).next_power_of_two();
        assert!(next < 0xFFFF);
        unsafe {
            let Vec16 {
                ptr: old_ptr,
                cap: old_cap,
                ..
            } = *self;
            let mut v = <Vec<T>>::with_capacity(next);
            self.ptr = v.as_mut_ptr();
            self.cap = v.capacity().checked_u16();
            mem::forget(v);
            ptr::copy_nonoverlapping(old_ptr, self.ptr, self.len as usize);
            drop(<Vec<T>>::from_raw_parts(old_ptr, 0, old_cap as usize));
        }
    }
    pub fn insert(&mut self, index: usize, element: T) {
        assert!(index.checked_u16() <= self.len);
        unsafe {
            self.maybe_reserve();
            let mut v = <Vec<T>>::from_raw_parts(self.ptr, self.len as usize, self.cap as usize);
            v.insert(index, element);
            debug_assert_eq!(v.as_mut_ptr(), self.ptr);
            self.len = v.len().checked_u16();
            mem::forget(v);
        }
    }
    pub fn push(&mut self, element: T) {
        assert!(self.len != 0xFFFF);
        unsafe {
            self.maybe_reserve();
            let mut v = mem::ManuallyDrop::new(<Vec<T>>::from_raw_parts(
                self.ptr,
                self.len as usize,
                self.cap as usize,
            ));
            v.push(element);
            debug_assert_eq!(v.as_mut_ptr(), self.ptr);
            self.len = v.len().checked_u16();
        }
    }
    pub fn clear(&mut self) {
        unsafe {
            let mut v = mem::ManuallyDrop::new(<Vec<T>>::from_raw_parts(
                self.ptr,
                self.len as usize,
                self.cap as usize,
            ));
            v.clear();
            self.ptr = v.as_mut_ptr();
            self.len = v.len().checked_u16();
            self.cap = v.capacity().checked_u16();
        }
    }
    pub fn len(&self) -> usize {
        self.len as usize
    }
    pub fn remove(&mut self, index: usize) -> T {
        unsafe {
            let mut v = mem::ManuallyDrop::new(<Vec<T>>::from_raw_parts(
                self.ptr,
                self.len as usize,
                self.cap as usize,
            ));
            let t = v.remove(index);
            debug_assert_eq!(v.as_mut_ptr(), self.ptr);
            self.len = v.len().checked_u16();
            t
        }
    }
    pub fn pop(&mut self) -> Option<T> {
        unsafe {
            let mut v = mem::ManuallyDrop::new(<Vec<T>>::from_raw_parts(
                self.ptr,
                self.len as usize,
                self.cap as usize,
            ));
            let t = v.pop();
            debug_assert_eq!(v.as_mut_ptr(), self.ptr);
            self.len = v.len().checked_u16();
            t
        }
    }
    pub fn first_mut(&mut self) -> Option<&mut T> {
        if self.len == 0 {
            None
        } else {
            Some(unsafe { &mut (*self.ptr) })
        }
    }
    pub fn extend_from_slice(&mut self, other: &[T])
    where
        T: Clone,
    {
        let new_len = match other.len().checked_add(self.len as usize) {
            Some(len @ 0..=0xFFFF) => len,
            _ => unreachable!(),
        };
        if new_len > self.capacity() {
            let mut new = <Vec16<T>>::with_capacity(new_len);
            unsafe {
                ptr::copy_nonoverlapping(self.as_ptr(), new.as_mut_ptr(), self.len());
                new.len = self.len;
                self.len = 0;
            }
            mem::swap(self, &mut new);
        }
        unsafe {
            let mut v = mem::ManuallyDrop::new(<Vec<T>>::from_raw_parts(
                self.ptr,
                self.len as usize,
                self.cap as usize,
            ));
            v.extend_from_slice(other);
            debug_assert_eq!(v.as_mut_ptr(), self.ptr);
            self.len = v.len().checked_u16();
        }
    }
}

impl<T> Clone for Vec16<T>
where
    T: Clone,
{
    fn clone(&self) -> Self {
        let mut v = <Vec<T>>::from(self.as_slice());
        let ptr = v.as_mut_ptr();
        let cap = v.capacity().checked_u16();
        let len = v.len().checked_u16();
        mem::forget(v);
        Vec16 { ptr, cap, len }
    }
}

impl<T> Default for Vec16<T> {
    fn default() -> Self {
        Vec16::new()
    }
}

impl<T> ops::Deref for Vec16<T> {
    type Target = [T];
    fn deref(&self) -> &Self::Target {
        self.as_slice()
    }
}

impl<T> ops::DerefMut for Vec16<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_mut_slice()
    }
}

impl<T> fmt::Debug for Vec16<T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Debug::fmt(&**self, f)
    }
}

impl<T> Drop for Vec16<T> {
    fn drop(&mut self) {
        unsafe {
            drop(<Vec<T>>::from_raw_parts(
                self.ptr,
                self.len as usize,
                self.cap as usize,
            ));
        }
    }
}
