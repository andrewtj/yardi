#![allow(missing_docs)]
#[cfg(test)]
mod test;

use crate::ascii::{ParseError, PresentError, Presenter};

pub fn parse_charstr<'a>(s: &str, dst: &'a mut [u8]) -> Result<&'a [u8], ParseError> {
    let mut bytes = crate::ascii::parse::pop_quotes(s)
        .as_bytes()
        .iter()
        .cloned();
    let mut dst_len = dst.len();
    {
        let mut dst_iter = dst.iter_mut().enumerate();
        while let Some(b) = bytes.next() {
            let b = match b {
                b'\\' => parse_escape(&mut bytes)?,
                b => b,
            };
            match dst_iter.next() {
                Some((_, d)) => *d = b,
                None => return Err(ParseError::value_large()),
            }
        }
        if let Some((len, _)) = dst_iter.next() {
            dst_len = len;
        }
    }
    Ok(&dst[..dst_len])
}

pub fn present_charstr(s: &[u8], p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    let mut ip = p.incremental();
    ip.write("\"")?;
    for b in s.iter().cloned() {
        unsafe {
            #[allow(clippy::match_overlapping_arm)]
            match b {
                b'\\' | b'"' => ip.write(core::str::from_utf8_unchecked(&[b'\\', b])),
                b' '..=b'~' => ip.write(core::str::from_utf8_unchecked(&[b])),
                _ => ip.write(core::str::from_utf8_unchecked(&byte_to_ddd(b)[..])),
            }?
        }
    }
    ip.write("\"")
}

pub fn parse_escape<T: Iterator<Item = u8>>(iter: &mut T) -> Result<u8, ParseError> {
    match iter.next() {
        Some(a @ b'0'..=b'9') => {
            let b = iter.next().ok_or_else(ParseError::dangling_escape)?;
            let c = iter.next().ok_or_else(ParseError::dangling_escape)?;
            ddd_to_byte(a, b, c)
        }
        Some(b @ b' '..=b'~') => Ok(b),
        Some(_) => Err(ParseError::bad_ascii()),
        None => Err(ParseError::dangling_escape()),
    }
}

#[inline(always)]
pub fn ddd_to_byte(a: u8, b: u8, c: u8) -> Result<u8, ParseError> {
    fn a_to_u(a: u8) -> Result<u8, ParseError> {
        match a.wrapping_sub(b'0') {
            u if u <= 9 => Ok(u),
            _ => Err(ParseError::value_invalid()),
        }
    }
    ((a_to_u(a)? * 10) + a_to_u(b)?)
        .checked_mul(10)
        .ok_or_else(ParseError::value_large)?
        .checked_add(a_to_u(c)?)
        .ok_or_else(ParseError::value_large)
}

#[inline(always)]
pub fn byte_to_ddd(b: u8) -> [u8; 4] {
    [b'\\', b'0' + b / 100, b'0' + b % 100 / 10, b'0' + b % 10]
}
