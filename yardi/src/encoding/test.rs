#![cfg(feature = "std")]

use std::io::{Cursor, Write};

use super::*;

#[test]
fn ddd_marshal() {
    for i in 0..=0xFFu8 {
        let [_, a, b, c] = byte_to_ddd(i);
        assert_eq!(Ok(i), ddd_to_byte(a, b, c));
    }
}

#[test]
fn ddd_invalid() {
    let mut tmp = [0u8; 3];
    for i in 0xFF + 1..=999 {
        write!(Cursor::new(&mut tmp[..]), "{:03}", i).unwrap();
        assert!(ddd_to_byte(tmp[0], tmp[1], tmp[2]).is_err());
    }
}
