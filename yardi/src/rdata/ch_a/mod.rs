/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
#![doc = "Support for A (CH) RDATA."]
#![allow(unused_qualifications)]
#[cfg(test)]
mod test;
#[doc = "A (CH) - a host address."]
#[doc = "\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
#[derive(
    Clone,
    Default,
    Eq,
    Ord,
    Pack,
    Parse,
    PartialEq,
    PartialOrd,
    Present,
    Rdata,
    Unpack,
    UnpackedLen,
    WireOrd,
)]
#[yardi(
    crate = "crate",
    rr_class = "crate::datatypes::Class::CH",
    rr_ty = "crate::datatypes::Ty::A",
    rr_skip_len_check,
    error_subject = "A"
)]
pub struct ChA {
    #[doc = "domain."]
    #[yardi(error_subject = "A domain")]
    pub domain: crate::datatypes::Name,
    #[doc = "address."]
    #[yardi(
        parse = "crate::datatypes::int::parse_u16_octal",
        present = "|m, e| crate::datatypes::int::present_u16_octal(*m, e)"
    )]
    #[yardi(error_subject = "A address")]
    pub address: u16,
}
impl core::fmt::Debug for ChA {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("ChA")
            .field("domain", &self.domain)
            .field("address", &self.address)
            .finish()
    }
}
impl core::str::FromStr for ChA {
    type Err = crate::ascii::parse::ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(
            s.as_bytes(),
            crate::ascii::Context::shared_default(),
        )
    }
}
/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
