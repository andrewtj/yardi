macro_rules! tests {
    ($($mod:ident : $present:literal $generic:literal)*) => {
        $(
        mod $mod {
            use crate::{
                ascii,
                rdata::svcb::Svcb,
                wire::{Pack, Reader, Unpack, Writer},
            };
            #[cfg(not(feature = "std"))]
            use alloc::string::String;

            fn parse(s: &str) -> Result<Svcb, ascii::ParseError> {
                ascii::parse::from_single_entry::<Svcb>(s.as_bytes(), Default::default())
            }

            #[test]
            fn present_eq_generic() {
                let present = parse($present).expect("present");
                let generic = parse($generic).expect("generic");
                assert_eq!(present, generic);
            }

            #[test]
            fn roundtrip_present() {
                let first = parse($present).expect("first");
                let mut out = String::new();
                ascii::present::to_string(&first, &mut out).expect("present");
                let second = parse(&out).expect("second");
                assert_eq!(first, second);
            }

            #[test]
            fn roundtrip_wire() {
                let first = parse($present).unwrap();
                let w = &mut [0u8; 1024];
                let w = &mut Writer::new(w);
                first.pack(w).unwrap();
                let r = &mut Reader::new(w.written());
                let second = Svcb::unpack(r).unwrap();
                assert_eq!(first, second);
            }
        })*
    };
}

tests! {
    figure2:
    "0 foo.example.com."
    r#"\# 19 (
00 00                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 63 6f 6d 00 ; target
)"#
    figure3:
    r#"1 ."#
    r#"\# 3 (
00 01      ; priority
00         ; target (root label)
)"#
    figure4:
    "16 foo.example.com. port=53"
    r#"\# 25 (
00 10                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 63 6f 6d 00 ; target
00 03                                              ; key 3
00 02                                              ; length 2
00 35                                              ; value
)"#
    figure5:
    "1 foo.example.com. key667=hello"
    r#"\# 28 (
00 01                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 63 6f 6d 00 ; target
02 9b                                              ; key 667
00 05                                              ; length 5
68 65 6c 6c 6f                                     ; value
)"#
    figure6:
    r#"1 foo.example.com. key667="hello\210qoo""#
    r#"\# 32 (
00 01                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 63 6f 6d 00 ; target
02 9b                                              ; key 667
00 09                                              ; length 9
68 65 6c 6c 6f d2 71 6f 6f                         ; value
)"#
    figure7:
    r#"1 foo.example.com. (
                      ipv6hint="2001:db8::1,2001:db8::53:1"
                      )"#
    r#"\# 55 (
00 01                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 63 6f 6d 00 ; target
00 06                                              ; key 6
00 20                                              ; length 32
20 01 0d b8 00 00 00 00 00 00 00 00 00 00 00 01    ; first address
20 01 0d b8 00 00 00 00 00 00 00 00 00 53 00 01    ; second address
)"#
    figure8:
    r#"1 example.com. (
                        ipv6hint="2001:db8:122:344::192.0.2.33"
                        )"#
    r#"\# 35 (
00 01                                              ; priority
07 65 78 61 6d 70 6c 65 03 63 6f 6d 00             ; target
00 06                                              ; key 6
00 10                                              ; length 16
20 01 0d b8 01 22 03 44 00 00 00 00 c0 00 02 21    ; address
)"#
    figure9:
    r#"16 foo.example.org. (
                      alpn=h2,h3-19 mandatory=ipv4hint,alpn
                      ipv4hint=192.0.2.1
                      )"#
    r#"\# 48 (
00 10                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 6f 72 67 00 ; target
00 00                                              ; key 0
00 04                                              ; param length 4
00 01                                              ; value: key 1
00 04                                              ; value: key 4
00 01                                              ; key 1
00 09                                              ; param length 9
02                                                 ; alpn length 2
68 32                                              ; alpn value
05                                                 ; alpn length 5
68 33 2d 31 39                                     ; alpn value
00 04                                              ; key 4
00 04                                              ; param length 4
c0 00 02 01                                        ; param value
)"#
    figure10a:
    r#"16 foo.example.org. alpn="f\\\\oo\\,bar,h2""#
    r#"\# 35 (
00 10                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 6f 72 67 00 ; target
00 01                                              ; key 1
00 0c                                              ; param length 12
08                                                 ; alpn length 8
66 5c 6f 6f 2c 62 61 72                            ; alpn value
02                                                 ; alpn length 2
68 32                                              ; alpn value
)"#
    figure10b:
    r#"16 foo.example.org. alpn=f\\\092oo\092,bar,h2"#
    r#"\# 35 (
00 10                                              ; priority
03 66 6f 6f 07 65 78 61 6d 70 6c 65 03 6f 72 67 00 ; target
00 01                                              ; key 1
00 0c                                              ; param length 12
08                                                 ; alpn length 8
66 5c 6f 6f 2c 62 61 72                            ; alpn value
02                                                 ; alpn length 2
68 32                                              ; alpn value
)"#
}
