mod generated;
use crate::{
    ascii::{parse::Pull, Parse, ParseError, Parser, Present, PresentError, Presenter},
    encoding::byte_to_ddd,
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};
#[cfg(not(feature = "std"))]
use alloc::{boxed::Box, vec::Vec};
use core::{
    cmp::Ordering,
    fmt::{self, Debug, Display},
    hash::{Hash, Hasher},
    net::{Ipv4Addr, Ipv6Addr},
    str::FromStr,
};

#[derive(
    Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
/// Represents an SVCB Parameter Key.
pub struct SvcbParamKey(pub u16);

impl From<u16> for SvcbParamKey {
    fn from(ty: u16) -> Self {
        Self(ty)
    }
}

impl Display for SvcbParamKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if let Some(s) = generated::to_str(*self) {
            Display::fmt(s, f)
        } else {
            write!(f, "key{}", self.0)
        }
    }
}

impl Debug for SvcbParamKey {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(self, f)
    }
}

impl FromStr for SvcbParamKey {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(v) = generated::from_str(s) {
            return Ok(v);
        }
        let Some(("key", maybe_int)) = s.split_at_checked(3) else {
            return Err(ParseError::value_invalid());
        };
        crate::datatypes::int::parse_u16(maybe_int).map(Self)
    }
}

impl Parse for SvcbParamKey {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(SvcbParamKey::from_str)
    }
}

impl Present for SvcbParamKey {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        if let Some(s) = generated::to_str(*self) {
            p.write(s)
        } else {
            write!(p, "key{}", self.0)
        }
    }
}

#[derive(Clone, Default)]
pub struct SvcbParams<P = Box<[u8]>> {
    wire: P,
}

impl<P: AsRef<[u8]>> SvcbParams<P> {
    // TODO: We're guaranteeing P always has valid wire format here, make sure that's the case...
    fn wire_params(&self) -> impl Iterator<Item = (SvcbParamKey, &[u8])> {
        let mut wire = self.wire.as_ref();
        core::iter::from_fn(move || {
            let (key, rest) = wire.split_first_chunk::<2>()?;
            let key = SvcbParamKey(u16::from_be_bytes(*key));
            let (val_len, rest) = rest.split_first_chunk::<2>()?;
            let val_len = u16::from_be_bytes(*val_len);
            let (value, rest) = rest.split_at_checked(usize::from(val_len))?;
            wire = rest;
            Some((key, value))
        })
    }
    fn wire_param(&self, key: SvcbParamKey) -> Option<&[u8]> {
        for (p_key, value) in self.wire_params() {
            match p_key.cmp(&key) {
                Ordering::Less => continue,
                Ordering::Equal => return Some(value),
                Ordering::Greater => return None,
            }
        }
        None
    }
    pub fn contains(&self, key: SvcbParamKey) -> bool {
        self.wire_param(key).is_some()
    }
    // TODO: is this useful?
    pub fn ohttp(self) -> bool {
        self.contains(SvcbParamKey::OHTTP)
    }
    pub fn dohpath(&self) -> Option<&str> {
        let path = self.wire_param(SvcbParamKey::DOHPATH)?;
        let path = core::str::from_utf8(path).ok()?;
        if path.contains("{dns}") {
            return Some(path);
        }
        None
    }
    pub fn ipv4_hints(&self) -> impl Iterator<Item = Ipv4Addr> + '_ {
        let mut wire = self.wire_param(SvcbParamKey::IPV4HINT).unwrap_or_default();
        core::iter::from_fn(move || {
            let (addr, rest) = wire.split_first_chunk::<4>()?;
            wire = rest;
            Some(Ipv4Addr::from(*addr))
        })
    }
    pub fn ipv6_hints(&self) -> impl Iterator<Item = Ipv6Addr> + '_ {
        let mut wire = self.wire_param(SvcbParamKey::IPV6HINT).unwrap_or_default();
        core::iter::from_fn(move || {
            let (addr, rest) = wire.split_first_chunk::<16>()?;
            wire = rest;
            Some(Ipv6Addr::from(*addr))
        })
    }
    pub fn port(&self) -> Option<u16> {
        self.wire_param(SvcbParamKey::PORT)
            .and_then(|s| s.first_chunk::<2>().copied())
            .map(u16::from_be_bytes)
    }
    pub fn no_default_alpn(&self) -> bool {
        self.contains(SvcbParamKey::NO_DEFAULT_ALPN)
    }
    pub fn alpn(&self) -> impl Iterator<Item = &[u8]> {
        let mut wire = self.wire_param(SvcbParamKey::ALPN).unwrap_or_default();
        core::iter::from_fn(move || {
            let (&len, rest) = wire.split_first()?;
            let (id, rest) = rest.split_at_checked(usize::from(len))?;
            wire = rest;
            Some(id)
        })
    }
    pub fn mandatory(&self) -> impl Iterator<Item = SvcbParamKey> + '_ {
        let mut wire = self.wire_param(SvcbParamKey::MANDATORY).unwrap_or_default();
        core::iter::from_fn(move || {
            let (key, rest) = wire.split_first_chunk::<2>()?;
            wire = rest;
            Some(SvcbParamKey(u16::from_be_bytes(*key)))
        })
    }
}

impl<P> SvcbParams<P>
where
    P: AsRef<[u8]> + From<Vec<u8>>,
{
    fn adjacent_wire(&self, key: SvcbParamKey) -> (bool, &[u8], &[u8]) {
        let mut i = 0_usize;
        let wire = self.wire.as_ref();
        while i < wire.len() {
            let Some((pkey, rest)) = wire[i..].split_first_chunk::<2>() else {
                break;
            };
            let pkey = SvcbParamKey(u16::from_be_bytes(*pkey));
            let len = usize::from(u16::from_be_bytes(*rest.first_chunk::<2>().unwrap()));
            match key.cmp(&pkey) {
                Ordering::Less => {
                    let (before, after) = wire.split_at(i);
                    return (false, before, after);
                }
                Ordering::Equal => {
                    let (before, rest) = wire.split_at(i);
                    let after = &rest[2 + len..];
                    return (true, before, after);
                }
                Ordering::Greater => {
                    i += 2 + len;
                }
            }
        }
        (false, wire, &[])
    }
    pub fn set_wire(&mut self, key: SvcbParamKey, value: &[u8]) -> Result<(), WireError> {
        validate_wire(key, value)?;
        let (_present, before, after) = self.adjacent_wire(key);
        let new_len = (before.len() + after.len())
            .checked_add(4)
            .and_then(|len| len.checked_add(value.len()))
            .ok_or(WireError::Oversize)?;
        let mut new_wire = Vec::with_capacity(new_len);
        new_wire.extend_from_slice(before);
        new_wire.extend_from_slice(&key.0.to_be_bytes());
        new_wire.extend_from_slice(&(value.len() as u16).to_be_bytes());
        new_wire.extend_from_slice(after);
        self.wire = new_wire.into();
        Ok(())
    }
    pub fn remove_wire(&mut self, key: SvcbParamKey) {
        if let (true, before, after) = self.adjacent_wire(key) {
            let mut v = Vec::with_capacity(before.len() + after.len());
            v.extend_from_slice(before);
            v.extend_from_slice(after);
            self.wire = v.into();
        }
    }
    // TODO: too large error
    fn set_bool_param(&mut self, key: SvcbParamKey, present: bool) -> Result<(), ()> {
        if present {
            self.set_wire(key, &[]).map_err(drop)
        } else {
            self.remove_wire(key);
            Ok(())
        }
    }
    pub fn set_ohttp(&mut self, present: bool) -> Result<(), ()> {
        self.set_bool_param(SvcbParamKey::OHTTP, present)
    }
    pub fn set_dohpath(&mut self, value: &str) -> Result<(), WireError> {
        validate_wire(SvcbParamKey::DOHPATH, value.as_bytes())?;
        self.set_wire(SvcbParamKey::DOHPATH, value.as_bytes())
    }
    pub fn set_ipv4hints(&mut self, hints: &[Ipv4Addr]) -> Result<(), WireError> {
        if hints.is_empty() {
            self.remove_wire(SvcbParamKey::IPV4HINT);
            return Ok(());
        }
        let (_, before, after) = self.adjacent_wire(SvcbParamKey::IPV4HINT);
        let value_len = hints
            .len()
            .checked_mul(4)
            .filter(|&l| l <= 0xFFFF)
            .ok_or(WireError::Oversize)?;
        let new_len = (before.len() + after.len())
            .checked_add(4 + value_len)
            .ok_or(WireError::Oversize)?;
        let mut new_wire = Vec::with_capacity(new_len);
        new_wire.extend_from_slice(before);
        new_wire.extend_from_slice(&SvcbParamKey::IPV4HINT.0.to_be_bytes());
        new_wire.extend_from_slice(&(value_len as u16).to_be_bytes());
        for addr in hints {
            new_wire.extend_from_slice(&addr.octets());
        }
        new_wire.extend_from_slice(after);
        self.wire = new_wire.into();
        Ok(())
    }
    pub fn remove_ipv4hints(&mut self) {
        self.remove_wire(SvcbParamKey::IPV4HINT);
    }
    // TODO: this could take an Iterator and discard the allocation?
    pub fn set_ipv6hints(&mut self, hints: &[Ipv6Addr]) -> Result<(), WireError> {
        if hints.is_empty() {
            self.remove_wire(SvcbParamKey::IPV6HINT);
            return Ok(());
        }
        let (_, before, after) = self.adjacent_wire(SvcbParamKey::IPV6HINT);
        let value_len = hints
            .len()
            .checked_mul(16)
            .filter(|&l| l <= 0xFFFF)
            .ok_or(WireError::Oversize)?;
        let new_len = (before.len() + after.len())
            .checked_add(4 + value_len)
            .ok_or(WireError::Oversize)?;
        let mut new_wire = Vec::with_capacity(new_len);
        new_wire.extend_from_slice(before);
        new_wire.extend_from_slice(&SvcbParamKey::IPV6HINT.0.to_be_bytes());
        new_wire.extend_from_slice(&(value_len as u16).to_be_bytes());
        for addr in hints {
            new_wire.extend_from_slice(&addr.octets());
        }
        new_wire.extend_from_slice(after);
        self.wire = new_wire.into();
        Ok(())
    }
    pub fn remove_ipv6hints(&mut self) {
        self.remove_wire(SvcbParamKey::IPV6HINT);
    }
    pub fn set_port(&mut self, port: u16) -> Result<(), WireError> {
        self.set_wire(SvcbParamKey::PORT, &port.to_be_bytes())
    }
    pub fn remove_port(&mut self) {
        self.remove_wire(SvcbParamKey::PORT);
    }
    pub fn set_no_default_alpn(&mut self, present: bool) -> Result<(), ()> {
        self.set_bool_param(SvcbParamKey::NO_DEFAULT_ALPN, present)
    }
    // TOOD: not sure taking an Iterator is ergonomic?
    pub fn set_alpn<'a>(
        &mut self,
        alpnids: impl Iterator<Item = &'a [u8]>,
    ) -> Result<(), WireError> {
        let (_, before, after) = self.adjacent_wire(SvcbParamKey::ALPN);
        let value_len_guess = match alpnids.size_hint() {
            (_, Some(upper)) => upper.saturating_mul(2),
            (lower, None) => lower.saturating_mul(2),
        }
        .max(0xFFFF);
        let param_len_guess = 4 + value_len_guess;
        let capacity_guess = (before.len() + after.len()).saturating_add(param_len_guess);
        let mut new_wire = Vec::with_capacity(capacity_guess);
        new_wire.extend_from_slice(before);
        new_wire.extend_from_slice(&SvcbParamKey::ALPN.0.to_be_bytes());
        let len_offset = new_wire.len();
        new_wire.extend_from_slice(&[0, 0]);
        let mut param_len = 0_u16;
        for id in alpnids {
            if id.is_empty() || id.len() > 0xFF {
                return Err(WireError::ValueInvalid);
            }
            param_len = param_len
                .checked_add(id.len() as u16 + 1)
                .ok_or(WireError::Oversize)?;
            new_wire.reserve(1 + id.len());
            new_wire.push(id.len() as u8);
            new_wire.extend_from_slice(id);
            if new_wire.len() > 0xFFFF {
                return Err(WireError::Oversize);
            }
        }
        if param_len == 0 {
            self.remove_wire(SvcbParamKey::ALPN);
            return Ok(());
        }
        if new_wire.len().saturating_add(after.len()) > 0xFFFF {
            return Err(WireError::Oversize);
        }
        new_wire[len_offset..len_offset + 2].copy_from_slice(&param_len.to_be_bytes());
        new_wire.extend_from_slice(after);
        Ok(())
    }
    pub fn remove_alpn(&mut self) {
        self.remove_wire(SvcbParamKey::ALPN);
    }
    // TOOD: not sure taking an Iterator is ergonomic?
    pub fn set_mandatory(
        &mut self,
        mandatory: impl Iterator<Item = SvcbParamKey>,
    ) -> Result<(), ()> {
        let mut set = U16Set::default();
        for m in mandatory {
            if m == SvcbParamKey::MANDATORY {
                // TODO: is this correct?
                return Err(());
            }
            set.insert(m.0);
        }
        if set.len() > (0xFFFF / 2) {
            return Err(());
        }
        if set.is_empty() {
            self.remove_wire(SvcbParamKey::MANDATORY);
            return Ok(());
        }
        let (_, before, after) = self.adjacent_wire(SvcbParamKey::MANDATORY);
        let new_len = (before.len() + after.len())
            .checked_add(4 + (set.len() * 2))
            .ok_or(())?;
        let mut new = Vec::with_capacity(new_len);
        new.extend_from_slice(before);
        new.extend_from_slice(&SvcbParamKey::MANDATORY.0.to_be_bytes());
        new.extend_from_slice(&(set.len() as u16).to_be_bytes()[..]);
        for n in set.iter() {
            new.extend_from_slice(&n.to_be_bytes()[..]);
        }
        new.extend_from_slice(after);
        debug_assert_eq!(new_len, new.len());
        self.wire = new.into();
        Ok(())
    }
}

impl<P: AsRef<[u8]>> Eq for SvcbParams<P> {}

impl<P: AsRef<[u8]>> PartialEq for SvcbParams<P> {
    fn eq(&self, other: &Self) -> bool {
        self.wire.as_ref() == other.wire.as_ref()
    }
}

impl<P: AsRef<[u8]>> Ord for SvcbParams<P> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.wire.as_ref().cmp(other.wire.as_ref())
    }
}

impl<P: AsRef<[u8]>> PartialOrd for SvcbParams<P> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<P: AsRef<[u8]>> Hash for SvcbParams<P> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.wire.as_ref().hash(state)
    }
}

impl<P: AsRef<[u8]>> Debug for SvcbParams<P> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.debug_set().entries(self.wire_params()).finish()
    }
}

impl<P: AsRef<[u8]>> Pack for SvcbParams<P> {
    fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        w.write_bytes(self.wire.as_ref())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(self.wire.as_ref().len())
    }
}

impl<'a, P: From<&'a [u8]>> Unpack<'a> for SvcbParams<P> {
    fn unpack(src: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let mut last = None;
        let start = src.index();
        while !src.is_empty() {
            let key = SvcbParamKey::unpack(src)?;
            if Some(key) <= last {
                return Err(UnpackError::value_invalid());
            }
            let value = src.get_bytes16()?;
            validate_wire(key, value)?;
            last = Some(key);
        }
        src.set_index(start);
        let wire = src.get_bytes_rest().map(P::from)?;
        Ok(Self { wire })
    }
}

impl<P> UnpackedLen for SvcbParams<P> {
    fn unpacked_len(src: &mut Reader<'_>) -> Result<usize, UnpackError> {
        <SvcbParams<&[u8]>>::unpack(src).map(|me| me.wire.len())
    }
}

impl<P: AsRef<[u8]>> WireOrd for SvcbParams<P> {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let left = <SvcbParams<&[u8]>>::unpack(left).map_err(WireOrdError::Left)?;
        let right = <SvcbParams<&[u8]>>::unpack(right).map_err(WireOrdError::Right)?;
        Ok(left.wire.as_ref().cmp(right.wire.as_ref()))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        self.wire.as_ref().cmp(other.wire.as_ref())
    }
}

impl<P: for<'a> From<&'a [u8]>> Parse for SvcbParams<P> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let mut seen_keys = U16Set::default();
        let mut parsed_charstr = [0u8; 0xFFFF];
        let mut params: Vec<(SvcbParamKey, Vec<u8>)> = Vec::new();
        let mut rdlen = 0_usize;
        while p.peek()?.is_some() {
            let (key, parsed_charstr) = parse_key_value(p, &mut parsed_charstr)?;
            if !seen_keys.insert(key.0) {
                return Err(ParseError::value_invalid());
            }
            let mut value = Vec::new();
            match key {
                SvcbParamKey::MANDATORY => {
                    // one or more SvcbParamKey, comma separated,
                    // any order but no duplicates. ordered on the wire.
                    let s = core::str::from_utf8(parsed_charstr)
                        .map_err(|_| ParseError::value_invalid())?;
                    let mut set = U16Set::default();
                    for r in s.split(',').map(SvcbParamKey::from_str) {
                        let k = r?;
                        if k == SvcbParamKey::MANDATORY || !set.insert(k.0) {
                            return Err(ParseError::value_invalid());
                        }
                        if set.len() > (0xFFFF / 2) {
                            return Err(ParseError::value_large());
                        };
                    }
                    if set.is_empty() {
                        return Err(ParseError::value_small());
                    }
                    value.reserve_exact(set.len() * 2);
                    for k in set.iter().map(u16::to_be_bytes) {
                        value.extend_from_slice(&k);
                    }
                }
                SvcbParamKey::ALPN => {
                    // one or more ALPN, comma seperated; alpnid
                    // are arbitrary octets so need to watch for
                    // escapes.
                    let mut escaped = false;
                    let mut alpnid = [0u8; 0xFF];
                    let mut len = 0;
                    for b in parsed_charstr.iter().copied() {
                        match b {
                            b'\\' if !escaped => escaped = true,
                            b',' if !escaped => {
                                if len == 0 {
                                    return Err(ParseError::value_invalid());
                                }
                                let need = 1 + len;
                                if need + value.len() > 0xFFFF {
                                    return Err(ParseError::value_invalid());
                                }
                                value.reserve(need);
                                value.push(len as u8);
                                value.extend(&alpnid[..len]);
                                len = 0;
                            }
                            b => {
                                escaped = false;
                                *alpnid.get_mut(len).ok_or_else(ParseError::value_invalid)? = b;
                                len += 1;
                            }
                        }
                    }
                    if escaped || len == 0 {
                        return Err(ParseError::value_invalid());
                    }
                    let need = 1 + len;
                    if need + value.len() > 0xFFFF {
                        return Err(ParseError::value_invalid());
                    }
                    value.reserve(need);
                    value.push(len as u8);
                    value.extend(&alpnid[..len]);
                }
                SvcbParamKey::NO_DEFAULT_ALPN => {
                    // no value
                    if !parsed_charstr.is_empty() {
                        return Err(ParseError::value_invalid());
                    }
                }
                SvcbParamKey::PORT => {
                    // u16
                    let u16 = crate::datatypes::int::parse_u16(parsed_charstr)?;
                    value.extend_from_slice(&u16.to_be_bytes()[..]);
                }
                SvcbParamKey::IPV4HINT => {
                    // one or more addresses, comma seperated.
                    // duplicates allowed.
                    let s = core::str::from_utf8(parsed_charstr)
                        .map_err(|_| ParseError::value_invalid())?;
                    for r in s.split(',').map(Ipv4Addr::from_str) {
                        if let Ok(addr) = r {
                            value.extend_from_slice(&addr.octets());
                        } else {
                            return Err(ParseError::value_invalid());
                        }
                    }
                    if value.is_empty() {
                        return Err(ParseError::value_small());
                    }
                }
                SvcbParamKey::ECH => {
                    // reserved... presumably whatever goes
                    // here won't be just octets? so safer to err?
                    return Err(ParseError::value_unknown());
                }
                SvcbParamKey::IPV6HINT => {
                    // one or more addresses, comma seperated.
                    // duplicates allowed.
                    let s = core::str::from_utf8(parsed_charstr)
                        .map_err(|_| ParseError::value_invalid())?;
                    for r in s.split(',').map(Ipv6Addr::from_str) {
                        if let Ok(addr) = r {
                            value.extend_from_slice(&addr.octets());
                        } else {
                            return Err(ParseError::value_invalid());
                        }
                    }
                    if value.is_empty() {
                        return Err(ParseError::value_small());
                    }
                }
                SvcbParamKey::DOHPATH => {
                    // utf8 template containing "{dns}"
                    // TODO: better validation
                    if validate_wire(SvcbParamKey::DOHPATH, parsed_charstr).is_err() {
                        return Err(ParseError::value_invalid());
                    }
                    value.extend_from_slice(parsed_charstr);
                }
                SvcbParamKey::OHTTP => {
                    // no value
                    if !parsed_charstr.is_empty() {
                        return Err(ParseError::value_invalid());
                    }
                }
                SvcbParamKey::TLS_SUPPORTED_GROUPS => {
                    // one or more u16, comma seperated, any order
                    // but no duplicates.
                    let s = core::str::from_utf8(parsed_charstr)
                        .map_err(|_| ParseError::value_invalid())?;
                    let mut set = U16Set::default();
                    for r in s.split(',').map(crate::datatypes::int::parse_u16) {
                        let v = r?;
                        if !set.insert(v) || (set.len() > (0xFFFF / 2)) {
                            return Err(ParseError::value_invalid());
                        }
                        value.extend_from_slice(&v.to_be_bytes());
                    }
                    if value.is_empty() {
                        return Err(ParseError::value_invalid());
                    }
                }
                _ => {
                    // value is arbitrary bytes
                    value.extend_from_slice(parsed_charstr);
                }
            };
            rdlen += 4 + value.len();
            if rdlen > 0xFFFF {
                return Err(ParseError::value_invalid());
            }
            let i = params.partition_point(|probe| probe.0 < key);
            params.insert(i, (key, value));
        }
        let w = &mut [0u8; 0xFFFF];
        let w = &mut Writer::new(w);
        for (key, value) in params {
            debug_assert!(value.len() <= 0xFFFF);
            let r = key.pack(w).and_then(|()| w.write_bytes16(&value));
            debug_assert_eq!(r, Ok(()));
            if r.is_err() {
                return Err(ParseError::value_invalid());
            }
        }
        debug_assert_eq!(rdlen, w.written().len());
        let wire = P::from(w.written());
        Ok(Self { wire })
    }
}

impl<P: AsRef<[u8]>> Present for SvcbParams<P> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        for (key, value) in self.wire_params() {
            let mut p = p.incremental();
            if value.is_empty() {
                write!(p, "{}", key)?;
                continue;
            }
            write!(p, "{}=", key)?;
            match key {
                SvcbParamKey::MANDATORY => {
                    let mut first = true;
                    let mut keys = value;
                    while let Some((key, rest)) = keys.split_first_chunk::<2>() {
                        keys = rest;
                        let key = SvcbParamKey(u16::from_be_bytes(*key));
                        if core::mem::take(&mut first) {
                            write!(p, "{key}")?;
                        } else {
                            write!(p, ",{key}")?;
                        }
                    }
                }
                SvcbParamKey::ALPN => {
                    let mut first = true;
                    let mut alpnids = value;
                    while let Some((alpnid, rest)) = alpnids
                        .split_first()
                        .and_then(|(&len, rest)| rest.split_at_checked(len as usize))
                    {
                        alpnids = rest;
                        if !core::mem::take(&mut first) {
                            p.write(",")?;
                        }
                        // TODO: could be better. escaping is probably overzealous, slow,
                        //       and unsafe might be avoidable.
                        for b in alpnid {
                            let escape;
                            let s = if matches!(b, b'0'..=b'9' | b'a'..=b'z' | b'A'..=b'Z' | b'-' | b'/' | b'.')
                            {
                                core::slice::from_ref(b)
                            } else {
                                let [_, a, b, c] = byte_to_ddd(*b);
                                escape = [b'\\', b'\\', b'\\', a, b, c];
                                &escape[..]
                            };
                            p.write(unsafe { core::str::from_utf8_unchecked(s) })?
                        }
                    }
                }
                #[cfg(debug_assertions)]
                SvcbParamKey::NO_DEFAULT_ALPN => unreachable!(),
                SvcbParamKey::PORT | SvcbParamKey::TLS_SUPPORTED_GROUPS => {
                    let mut first = true;
                    let mut u16s = value;
                    while let Some((bytes, rest)) = u16s.split_first_chunk::<2>() {
                        u16s = rest;
                        let n = u16::from_be_bytes(*bytes);
                        if core::mem::take(&mut first) {
                            write!(p, "{n}")?;
                        } else {
                            write!(p, ",{n}")?;
                        }
                    }
                }
                SvcbParamKey::IPV4HINT => {
                    let mut first = true;
                    let mut addrs = value;
                    while let Some((addr, rest)) = addrs.split_first_chunk::<4>() {
                        addrs = rest;
                        let addr = Ipv4Addr::from(*addr);
                        if core::mem::take(&mut first) {
                            write!(p, "{addr}")?;
                        } else {
                            write!(p, ",{addr}")?;
                        }
                    }
                }
                #[cfg(debug_assertions)]
                SvcbParamKey::ECH => unreachable!(),
                SvcbParamKey::IPV6HINT => {
                    let mut first = true;
                    let mut addrs = value;
                    while let Some((addr, rest)) = addrs.split_first_chunk::<16>() {
                        addrs = rest;
                        let addr = Ipv6Addr::from(*addr);
                        if core::mem::take(&mut first) {
                            write!(p, "{addr}")?;
                        } else {
                            write!(p, ",{addr}")?;
                        }
                    }
                }
                #[cfg(debug_assertions)]
                SvcbParamKey::OHTTP => unreachable!(),
                SvcbParamKey::DOHPATH | SvcbParamKey(_) => {
                    for b in value.iter().copied() {
                        unsafe {
                            match b {
                                b'\\' | b'"' => {
                                    p.write(core::str::from_utf8_unchecked(&[b'\\', b]))
                                }
                                b' '..=b'~' => p.write(core::str::from_utf8_unchecked(&[b])),
                                _ => p.write(core::str::from_utf8_unchecked(&byte_to_ddd(b)[..])),
                            }?
                        }
                    }
                }
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
pub enum WireError {
    ValueInvalid,
    ValueUnknown,
    Oversize,
}

impl From<WireError> for UnpackError {
    fn from(err: WireError) -> Self {
        match err {
            WireError::Oversize => UnpackError::value_large(),
            WireError::ValueInvalid => UnpackError::value_invalid(),
            WireError::ValueUnknown => UnpackError::value_unknown(),
        }
    }
}

fn validate_wire(key: SvcbParamKey, value: &[u8]) -> Result<(), WireError> {
    let ok = match key {
        SvcbParamKey::MANDATORY => {
            // keys may appear in any order but cannot appear more than once
            let mut set = U16Set::default();
            !value.is_empty()
                && (value.len() % 2 == 0)
                && value.chunks(2).all(|w| {
                    let value = u16::from_be_bytes([w[0], w[1]]);
                    value != key.0 && set.insert(value)
                })
        }
        SvcbParamKey::ALPN => {
            // one or more u8 prefixed byte sequences
            let mut alpnids = value;
            loop {
                match alpnids.split_first() {
                    Some((&0, _)) => break false,
                    Some((&len, rest)) => {
                        let Some((_, rest)) = rest.split_at_checked(usize::from(len)) else {
                            break false;
                        };
                        alpnids = rest;
                    }
                    None => break !value.is_empty(),
                }
            }
        }
        SvcbParamKey::NO_DEFAULT_ALPN => value.is_empty(),
        SvcbParamKey::PORT => value.len() == 2,
        SvcbParamKey::IPV4HINT => !value.is_empty() && (value.len() % 4 == 0),
        SvcbParamKey::ECH => return Err(WireError::ValueUnknown),
        SvcbParamKey::IPV6HINT => !value.is_empty() && (value.len() % 16 == 0),
        SvcbParamKey::DOHPATH => {
            // TODO: this could be more comprehensive
            let s = core::str::from_utf8(value).map_err(|_| WireError::ValueInvalid)?;
            s.contains("{dns}")
        }
        SvcbParamKey::OHTTP => value.is_empty(),
        SvcbParamKey::TLS_SUPPORTED_GROUPS => {
            let mut set = U16Set::default();
            !value.is_empty()
                && (value.len() % 2 == 0)
                && value
                    .chunks(2)
                    .all(|w| set.insert(u16::from_be_bytes([w[0], w[1]])))
        }
        _ => true,
    };
    ok.then_some(()).ok_or(WireError::ValueInvalid)
}

struct U16Set {
    bmp: [u8; 8192],
    len: usize,
}

impl Default for U16Set {
    fn default() -> Self {
        Self {
            bmp: [0u8; 8192],
            len: 0,
        }
    }
}

impl U16Set {
    fn insert(&mut self, key: u16) -> bool {
        let i = usize::from(key) / 8;
        let b = 1 << (key % 8);
        let out = self.bmp[i] & b == 0;
        self.bmp[i] |= b;
        self.len += out as usize;
        out
    }
    fn iter(&self) -> impl Iterator<Item = u16> + '_ {
        let bmp = &self.bmp;
        let mut value = 0_usize;
        core::iter::from_fn(move || loop {
            let i = value / 8;
            let t = *bmp.get(i)?;
            if t == 0 {
                value += 8;
                continue;
            }
            let b = 1 << (value % 8);
            let present = t & b != 0;
            let old_value = value;
            value += 1;
            if present {
                return Some(old_value as u16);
            }
        })
    }
    fn len(&self) -> usize {
        self.len
    }
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

#[test]
fn u16_set_works() {
    let mut set = U16Set::default();
    for i in 0..=u16::MAX {
        assert!(set.insert(i));
        assert_eq!(set.len(), i as usize + 1);
    }
    for i in 0..=u16::MAX {
        assert!(!set.insert(i));
    }
    let mut iter = set.iter();
    for i in 0..=u16::MAX {
        assert_eq!(iter.next(), Some(i));
    }
    assert!(iter.next().is_none());
}

#[test]
fn wire_params_works() {
    let temp = &mut [0u8; 128];
    let w = &mut Writer::new(temp);
    let range = u16::MAX - 3..u16::MAX;
    for i in range.clone() {
        i.pack(w).unwrap();
        i.pack(&mut w.writer16().unwrap()).unwrap();
    }
    let svcb_params = SvcbParams { wire: w.written() };
    let mut range_iter = range.into_iter();
    let mut params_iter = svcb_params.wire_params();
    loop {
        match (range_iter.next(), params_iter.next()) {
            (Some(i), Some((key, value))) => {
                assert_eq!(i, key.0);
                assert_eq!(2, value.len());
                assert_eq!(i.to_be_bytes(), [value[0], value[1]]);
            }
            (None, None) => break,
            other => panic!("{other:?}"),
        }
    }
}

fn parse_key_value<'a>(
    parser: &mut Parser<'_, '_>,
    temp: &'a mut [u8; 0xFFFF],
) -> Result<(SvcbParamKey, &'a [u8]), ParseError> {
    #[derive(Debug)]
    enum FirstPass<'a> {
        Done(SvcbParamKey, &'a [u8]),
        ValueMayFollow(SvcbParamKey, usize, &'a mut [u8]),
    }
    let ph: FirstPass = parser.pull(move |s| {
        let mut bytes_iter = s.as_bytes().iter().enumerate();
        let r = loop {
            match bytes_iter.next() {
                Some((0..63, b'-' | b'0'..=b'9' | b'a'..=b'z')) => {}
                Some((i, eq @ &b'=')) => match SvcbParamKey::from_str(&s[..i]) {
                    Ok(key) => match s.get(i + 1..) {
                        Some(charstr) if !charstr.is_empty() => {
                            break crate::encoding::parse_charstr(charstr, temp)
                                .map(|value| FirstPass::Done(key, value));
                        }
                        _ => {
                            break Ok(FirstPass::ValueMayFollow(
                                key,
                                eq as *const u8 as usize + 1,
                                temp,
                            ));
                        }
                    },
                    Err(err) => break Err(err),
                },
                None => match SvcbParamKey::from_str(s) {
                    Ok(key) => break Ok(FirstPass::Done(key, &temp[..0])),
                    Err(err) => break Err(err),
                },
                _ => break Err(ParseError::value_invalid()),
            }
        };
        match r {
            Ok(accept) => Pull::Accept(accept),
            Err(err) => Pull::Reject(err),
        }
    })?;
    match ph {
        FirstPass::Done(key, value) => Ok((key, value)),
        FirstPass::ValueMayFollow(key, after, temp) => {
            let value = parser.pull(|s| match s.as_bytes().first() {
                Some(b) if b as *const u8 as usize == after => {
                    match crate::encoding::parse_charstr(s, temp) {
                        Ok(value) => Pull::Accept(value),
                        Err(err) => Pull::Reject(err),
                    }
                }
                _ => Pull::Decline(&temp[..0]),
            })?;
            Ok((key, value))
        }
    }
}
