#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

use super::WksBitmap;

#[test]
fn bitmapbuf() {
    let mut b = WksBitmap::default();
    let subjects = &[0, 23, 53, 80, 443, 0xFFFF];
    for &p in subjects {
        b.insert(p);
    }
    assert_eq!(b.iter().collect::<Vec<_>>().as_slice(), subjects);
}
