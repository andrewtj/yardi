#[cfg(test)]
mod test;

use core::{
    cmp::Ordering,
    fmt::{self, Debug},
};

use crate::{
    ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter},
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

/// An owned WKS bitmap.
#[derive(Clone, Default, Eq, Hash, Ord, PartialEq, PartialOrd)]
// TODO: make this a regular struct with an inner?
pub struct WksBitmap<T>(T);

impl WksBitmap<Vec<u8>> {
    /// Insert a service into this `WksBitmap`.
    pub fn insert(&mut self, port: u16) {
        let port = port as usize;
        let off = port / 8;
        let maybe_need = (off + 1).checked_sub(self.0.len());
        if let Some(need) = maybe_need {
            // TODO: fallible alloc
            self.0.resize(self.0.len() + need, 0);
        };
        self.0[off] |= 0b_1000_0000 >> (port % 8);
    }
}

impl<T: for<'a> From<&'a [u8]>> WksBitmap<T> {
    /// Create a new `WksBitmap` by parsing services of the specified protocol.
    pub fn parse_with_protocol(
        p: &mut Parser<'_, '_>,
        protocol: Option<u8>,
    ) -> Result<Self, ParseError> {
        let f = p.context().parse_wks_service();
        let temp = &mut [0u8; 8192];
        let mut len = 0;
        p.pull_rest(|s| {
            let port = f(protocol, s)?;
            let off = (port as usize) / 8;
            temp[off] |= 0b_1000_0000 >> (port % 8);
            len = len.max(off + 1);
            Ok(())
        })?;
        Ok(Self(temp[..len].into()))
    }
}

impl<T: AsRef<[u8]>> WksBitmap<T> {
    /// True if there are no services in this `WksBitmap`.
    pub fn is_empty(&self) -> bool {
        self.0.as_ref().iter().all(|&b| b == 0)
    }
    /// Returns an iterator over the services in this `WksBitmap`.
    pub fn iter(&self) -> WksBmpIter<'_> {
        WksBmpIter::new(self.0.as_ref())
    }
    /// Returns the (byte) length of this `WksBitmap`.
    pub fn len(&self) -> usize {
        self.0.as_ref().len()
    }
}

impl<'a, T: AsRef<[u8]>> IntoIterator for &'a WksBitmap<T> {
    type Item = u16;
    type IntoIter = WksBmpIter<'a>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T: AsRef<[u8]>> Debug for WksBitmap<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl<T: for<'a> From<&'a [u8]>> Parse for WksBitmap<T> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        WksBitmap::parse_with_protocol(p, None)
    }
}

impl<T: AsRef<[u8]>> Present for WksBitmap<T> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        for port in self {
            port.present(p)?;
        }
        Ok(())
    }
}

impl<T: AsRef<[u8]>> Pack for WksBitmap<T> {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        dst.write_bytes(self.0.as_ref())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(self.0.as_ref().len())
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for WksBitmap<T> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        unpack_bytes(reader).map(Into::into).map(Self)
    }
}

impl<T> UnpackedLen for WksBitmap<T> {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        let initial = reader.index();
        unpack_bytes(reader)?;
        Ok(reader.index() - initial)
    }
}

impl<T: AsRef<[u8]>> WireOrd for WksBitmap<T> {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let l = unpack_bytes(left).map_err(WireOrdError::Left)?;
        let r = unpack_bytes(right).map_err(WireOrdError::Right)?;
        Ok(l.cmp(r))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        <[u8]>::cmp(self.0.as_ref(), other.0.as_ref())
    }
}

fn unpack_bytes<'a>(r: &mut Reader<'a>) -> Result<&'a [u8], UnpackError> {
    let initial_index = r.index();
    let bytes = r.get_bytes_rest()?;
    if bytes.len() > 8192 || bytes.last() == Some(&0) {
        Err(UnpackError::value_invalid_at(initial_index))
    } else {
        Ok(bytes)
    }
}

/// An iterator over the services contained in a `WksBitmap`.
#[derive(Debug)]
pub struct WksBmpIter<'a> {
    subject: u8,
    base: u16,
    rest: &'a [u8],
}

impl<'a> WksBmpIter<'a> {
    fn new(buf: &'a [u8]) -> Self {
        let (first, rest) = buf
            .split_first()
            .map(|(&first, rest)| (first, rest))
            .unwrap_or_else(|| (0, &[]));
        WksBmpIter {
            subject: first,
            base: 0,
            rest,
        }
    }
}

impl<'a> Iterator for WksBmpIter<'a> {
    type Item = u16;
    fn next(&mut self) -> Option<u16> {
        while self.subject == 0 {
            match self.rest.split_first() {
                Some((&next, rest)) => {
                    self.base += 8;
                    self.subject = next;
                    self.rest = rest;
                }
                None => return None,
            }
        }
        let lz = self.subject.leading_zeros();
        self.subject ^= 1 << (7 - lz);
        Some(self.base + lz as u16)
    }
}
