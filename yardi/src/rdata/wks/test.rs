use crate::rdata::Wks;

#[test]
fn default() {
    <Wks>::default();
}

const CASE_HG00_WIRE: &'static [u8] = include_bytes!("sample_hg_wks_00.bin");

#[test]
fn wire_marshal_hg00() {
    crate::rdata::test::wire_marshal::<Wks>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_hg00() {
    crate::rdata::test::wire_cmp::<Wks>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_canonical_hg00() {
    crate::rdata::test::wire_cmp_canonical::<Wks>(CASE_HG00_WIRE);
}

const CASE_HG00_ASCII: &str = include_str!("sample_hg_wks_00.txt");

#[test]
fn ascii_roundtrip_hg00() {
    crate::rdata::test::ascii_roundtrip::<Wks>(CASE_HG00_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg00() {
    crate::rdata::test::generic_ascii_roundtrip::<Wks>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn ascii_to_wire_hg00() {
    crate::rdata::test::ascii_to_wire::<Wks>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn wire_to_ascii_hg00() {
    crate::rdata::test::wire_to_ascii::<Wks>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}
