//! Support for WKS (IN) RDATA

#[cfg(test)]
mod test;

mod wks_bitmap;

pub use self::wks_bitmap::{WksBitmap, WksBmpIter};
use crate::{
    ascii::{Context, Parse, ParseError, Parser},
    wire::Pack,
};
#[cfg(not(feature = "std"))]
use alloc::boxed::Box;
use core::{
    fmt::{self, Debug},
    str::FromStr,
};

#[doc = "WKS (IN) - a well known service description\n\n"]
#[doc = "Parsing protocol and services mnemonics is not implemented by default. If you need support for mnemonics provide helper functions via `Context`.\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
#[derive(
    Clone, PartialEq, Eq, PartialOrd, Ord, Present, Pack, Unpack, UnpackedLen, Rdata, WireOrd,
)]
#[yardi(
    crate = "crate",
    rr_class = "crate::datatypes::Class::IN",
    rr_ty = "crate::datatypes::Ty::WKS"
)]
pub struct Wks<S = Box<[u8]>> {
    #[doc = "Address: IPv4 address."]
    pub address: core::net::Ipv4Addr,
    #[doc = "Protocol: 8 bit IP protocol number.\n\n**Note**: Parsing mnemonics is not implemented by default. If you need support for mnemonics provide a helper function via `Context`."]
    pub protocol: u8,
    #[doc = "Services: Services listening at address.\n\n**Note**: Parsing mnemonics is not implemented by default. If you need support for mnemonics provide a helper function via `Context`."]
    #[yardi(
        parse_bound = "S: From<[u8]>",
        present_bound = "S: AsRef<[u8]>",
        pack_bound = "S: AsRef<[u8]>",
        unpack_bound = "S: From<&'yr [u8]>"
    )]
    pub services: WksBitmap<S>,
}

impl<S: AsRef<[u8]>> Debug for Wks<S> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Wks")
            .field("address", &self.address)
            .field("protocol", &self.protocol)
            .field("services", &self.services)
            .finish()
    }
}

impl<S> FromStr for Wks<S>
where
    Wks<S>: Parse,
{
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(s.as_bytes(), Context::shared_default())
    }
}

impl<S: Default> Default for Wks<S> {
    fn default() -> Self {
        Wks {
            address: [0u8; 4].into(),
            protocol: 0,
            services: WksBitmap::default(),
        }
    }
}

impl<S: for<'a> From<&'a [u8]> + AsRef<[u8]>> Parse for Wks<S> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        if let Some(wks) = crate::rdata::parse_known_generic::<Self>(p)? {
            return Ok(wks);
        }
        let address = Parse::parse(p)?;
        let parse_protocol_f = p.context().parse_wks_ip_protocol();
        let protocol = p.pull(parse_protocol_f)?;
        let services = WksBitmap::parse_with_protocol(p, Some(protocol))?;
        let wks = Wks {
            address,
            protocol,
            services,
        };
        let _ = wks.packed_len().map_err(|_| ParseError::value_large())?;
        Ok(wks)
    }
}
