use super::Hip;
use crate::{
    msg::Rr,
    wire::{Pack, Reader, Unpack, WireOrd, Writer},
};
#[cfg(not(feature = "std"))]
use alloc::string::String;
use core::{cmp::Ordering, str::FromStr};

#[test]
fn rfc8005_a() {
    let sample = r#"www.example.com. 0 IN HIP 2 200100107B1A74DF365639CC39F1D578 AwEAAbdxyhNuSutc5EMzxTs9LBPCIkOFH8cIvM4p9+LrV4e19WzK00+CI6zBCQTdtWsuxKbWIy87UOoJTwkUs7lBu+Upr1gsNrut79ryra+bSRGQb1slImA8YVJyuIDsj7kwzG7jnERNqnWxZ48AWkskmdHaVDP4BcelrTI3rMXdXF5D"#;
    rfc8005_test(sample);
}

#[test]
fn rfc8005_b() {
    let sample = r#"www.example.com. 0 IN HIP 2 200100107B1A74DF365639CC39F1D578 AwEAAbdxyhNuSutc5EMzxTs9LBPCIkOFH8cIvM4p9+LrV4e19WzK00+CI6zBCQTdtWsuxKbWIy87UOoJTwkUs7lBu+Upr1gsNrut79ryra+bSRGQb1slImA8YVJyuIDsj7kwzG7jnERNqnWxZ48AWkskmdHaVDP4BcelrTI3rMXdXF5D rvs.example.com."#;
    rfc8005_test(sample);
}

#[test]
fn rfc8005_c() {
    let sample = r#"www.example.com. 0 IN HIP 2 200100107B1A74DF365639CC39F1D578 AwEAAbdxyhNuSutc5EMzxTs9LBPCIkOFH8cIvM4p9+LrV4e19WzK00+CI6zBCQTdtWsuxKbWIy87UOoJTwkUs7lBu+Upr1gsNrut79ryra+bSRGQb1slImA8YVJyuIDsj7kwzG7jnERNqnWxZ48AWkskmdHaVDP4BcelrTI3rMXdXF5D rvs1.example.com. rvs2.example.com."#;
    rfc8005_test(sample);
}

fn rfc8005_test(sample: &str) {
    let mut actual = String::with_capacity(sample.len());
    let rr = <Rr<Hip>>::from_str(sample).expect("from_str");
    crate::ascii::present::to_string(&rr, &mut actual).expect("present");
    assert_eq!(sample, actual.as_str());
    let mut buf = vec![0; rr.data.packed_len().expect("packed_len")];
    let mut w = Writer::new(&mut buf[..]);
    rr.data.pack(&mut w).expect("pack");
    assert_eq!(w.remaining(), 0);
    let bytes = w.written();
    let mut r = Reader::new(bytes);
    let rt = Hip::unpack(&mut r).expect("unpack");
    assert!(r.is_empty());
    assert_eq!(rt, rr.data);
    assert_eq!(
        Ok(Ordering::Equal),
        Hip::cmp_wire(&mut Reader::new(bytes), &mut Reader::new(bytes),)
    );
}

const CASE_HG00_WIRE: &'static [u8] = include_bytes!("sample_hg_hip_00.bin");

#[test]
fn wire_marshal_hg00() {
    crate::rdata::test::wire_marshal::<Hip>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_hg00() {
    crate::rdata::test::wire_cmp::<Hip>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_canonical_hg00() {
    crate::rdata::test::wire_cmp_canonical::<Hip>(CASE_HG00_WIRE);
}

const CASE_HG00_ASCII: &str = include_str!("sample_hg_hip_00.txt");

#[test]
fn ascii_roundtrip_hg00() {
    crate::rdata::test::ascii_roundtrip::<Hip>(CASE_HG00_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg00() {
    crate::rdata::test::generic_ascii_roundtrip::<Hip>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn ascii_to_wire_hg00() {
    crate::rdata::test::ascii_to_wire::<Hip>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn wire_to_ascii_hg00() {
    crate::rdata::test::wire_to_ascii::<Hip>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

const CASE_HG01_WIRE: &'static [u8] = include_bytes!("sample_hg_hip_01.bin");

#[test]
fn wire_marshal_hg01() {
    crate::rdata::test::wire_marshal::<Hip>(CASE_HG01_WIRE);
}

#[test]
fn wire_cmp_hg01() {
    crate::rdata::test::wire_cmp::<Hip>(CASE_HG01_WIRE);
}

#[test]
fn wire_cmp_canonical_hg01() {
    crate::rdata::test::wire_cmp_canonical::<Hip>(CASE_HG01_WIRE);
}

const CASE_HG01_ASCII: &str = include_str!("sample_hg_hip_01.txt");

#[test]
fn ascii_roundtrip_hg01() {
    crate::rdata::test::ascii_roundtrip::<Hip>(CASE_HG01_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg01() {
    crate::rdata::test::generic_ascii_roundtrip::<Hip>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

#[test]
fn ascii_to_wire_hg01() {
    crate::rdata::test::ascii_to_wire::<Hip>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

#[test]
fn wire_to_ascii_hg01() {
    crate::rdata::test::wire_to_ascii::<Hip>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

const CASE_HG02_WIRE: &'static [u8] = include_bytes!("sample_hg_hip_02.bin");

#[test]
fn wire_marshal_hg02() {
    crate::rdata::test::wire_marshal::<Hip>(CASE_HG02_WIRE);
}

#[test]
fn wire_cmp_hg02() {
    crate::rdata::test::wire_cmp::<Hip>(CASE_HG02_WIRE);
}

#[test]
fn wire_cmp_canonical_hg02() {
    crate::rdata::test::wire_cmp_canonical::<Hip>(CASE_HG02_WIRE);
}

const CASE_HG02_ASCII: &str = include_str!("sample_hg_hip_02.txt");

#[test]
fn ascii_roundtrip_hg02() {
    crate::rdata::test::ascii_roundtrip::<Hip>(CASE_HG02_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg02() {
    crate::rdata::test::generic_ascii_roundtrip::<Hip>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

#[test]
fn ascii_to_wire_hg02() {
    crate::rdata::test::ascii_to_wire::<Hip>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

#[test]
fn wire_to_ascii_hg02() {
    crate::rdata::test::wire_to_ascii::<Hip>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}
