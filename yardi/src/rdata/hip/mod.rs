//! Support for HIP RDATA
#[cfg(not(feature = "std"))]
use alloc::{boxed::Box, vec::Vec};

#[cfg(test)]
mod test;

use core::{cmp::Ordering, str::FromStr};

use crate::{
    ascii::{Context, ParseError, Parser, Present, PresentError, Presenter},
    datatypes::{IpseckeyAlg, Name, Ty},
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};

fn parse_servers(p: &mut Parser<'_, '_>) -> Result<Vec<Name>, ParseError> {
    let mut v = Vec::new();
    let origin = p.context().origin();
    p.pull_rest(|s| {
        v.push(Name::from_str_origin(s, origin)?);
        Ok(())
    })?;
    Ok(v)
}

fn present_servers(servers: &[Name], p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    for n in servers {
        n.present(p)?;
    }
    Ok(())
}

#[doc = "HIP - Host Identity Protocol\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc8005\">RFC 8005: Host Identity Protocol (HIP) Domain Name System (DNS) Extension</a>"]
#[derive(Clone, Debug, PartialOrd, Ord, PartialEq, Eq, Parse, Present, Rdata)]
#[yardi(
    crate = "crate",
    rr_ty = "crate::datatypes::Ty::HIP",
    error_subject = "HIP"
)]
// TODO: hit and public key should be generic.... servers too?
pub struct Hip {
    /// Public key cryptographic algorithm
    pub alg: IpseckeyAlg,
    #[yardi(
        present = "|m , e| crate::datatypes::bytes::present_hex(m, e, 1, 255)",
        parse = "|e| crate::datatypes::bytes::parse_hex(e, 1, 255)",
        error_subject = "HIT"
    )]
    /// Host Identity Tag
    pub hit: Box<[u8]>,
    #[yardi(
        present = "|m, e| crate::datatypes::bytes::present_base64(m, e, 1, 65535)",
        parse = "|e| crate::datatypes::bytes::parse_base64(e, 1, 65535)",
        error_subject = "Public Key"
    )]
    /// Public key
    pub public_key: Box<[u8]>,
    /// Rendezvous servers
    #[yardi(
        present = "present_servers",
        parse = "parse_servers",
        error_subject = "Rendezvous Servers"
    )]
    pub servers: Vec<Name>,
}

impl FromStr for Hip {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(s.as_bytes(), Context::shared_default())
    }
}

impl Hip {
    fn wire_header(&self) -> WireHeader {
        WireHeader {
            hit_len: self.hit.len(),
            pk_alg: self.alg,
            pk_len: self.public_key.len(),
        }
    }
}

#[derive(Pack, Unpack)]
#[yardi(crate = "crate")]
struct WireHeader {
    #[yardi(
        pack = "pack_len::<u8>",
        packed_len = "packed_len::<u8>",
        unpack = "unpack_len::<u8>",
        error_subject = "HIT length"
    )]
    hit_len: usize,
    #[yardi(error_subject = "PK algorithm")]
    pk_alg: IpseckeyAlg,
    #[yardi(
        pack = "pack_len::<u16>",
        packed_len = "packed_len::<u16>",
        unpack = "unpack_len::<u16>",
        error_subject = "PK length"
    )]
    pk_len: usize,
}

fn pack_len<T: TryFrom<usize> + Pack>(len: &usize, w: &mut Writer<'_>) -> Result<(), PackError> {
    if *len == 0 {
        return Err(PackError::value_small());
    }
    match T::try_from(*len) {
        Ok(len) => len.pack(w),
        Err(_) => Err(PackError::value_large()),
    }
}

fn packed_len<T: TryFrom<usize> + Pack>(len: &usize) -> Result<usize, PackError> {
    if *len == 0 {
        return Err(PackError::value_small());
    }
    match T::try_from(*len) {
        Ok(len) => len.packed_len(),
        Err(_) => Err(PackError::value_large()),
    }
}

fn unpack_len<'a, T: Into<usize> + Unpack<'a>>(r: &mut Reader<'a>) -> Result<usize, UnpackError> {
    match T::unpack(r).map(Into::into)? {
        0 => Err(UnpackError::value_small()),
        len => Ok(len),
    }
}

impl<'a> Unpack<'a> for Hip {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let header = WireHeader::unpack(reader)?;
        let hit = reader
            .get_bytes(header.hit_len)
            .map_err(|err| err.set_unset_subject(&"HIT"))?;
        let pk = reader
            .get_bytes(header.pk_len)
            .map_err(|err| err.set_unset_subject(&"Public Key"))?;
        let mut len = 4 + header.hit_len + header.pk_len;
        if len > 0xFFFF {
            return Err(UnpackError::value_rdata_oversize().set_unset_subject(&"HIP"));
        }
        let mut servers = Vec::new();
        while !reader.is_empty() {
            let name = Name::unpack(reader)?;
            len = len.saturating_add(name.len());
            if len > 0xFFFF {
                return Err(UnpackError::value_rdata_oversize().set_unset_subject(&"HIP"));
            }
            servers.push(name);
        }
        Ok(Hip {
            alg: header.pk_alg,
            hit: hit.into(),
            public_key: pk.into(),
            servers,
        })
    }
}

impl UnpackedLen for Hip {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        let header = WireHeader::unpack(reader)?;
        reader
            .skip(header.hit_len)
            .map_err(|err| err.set_unset_subject(&"HIT"))?;
        let mut len = 4 + header.hit_len + header.pk_len;
        if len > 0xFFFF {
            return Err(UnpackError::value_rdata_oversize().set_unset_subject(&"HIP"));
        }
        reader
            .skip(header.pk_len)
            .map_err(|err| err.set_unset_subject(&"Public Key"))?;
        while !reader.is_empty() {
            len = len.saturating_add(Name::unpacked_len(reader)?);
            if len > 0xFFFF {
                return Err(UnpackError::value_rdata_oversize().set_unset_subject(&"HIP"));
            }
        }
        Ok(len)
    }
}

impl Pack for Hip {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        let mut ty_writer = writer.for_ty(Ty::HIP);
        let writer = ty_writer.as_mut();
        let header = self.wire_header();
        let mut len = header.packed_len()? + header.hit_len;
        header.pack(writer)?;
        writer.write_bytes(&self.hit[..])?;
        len = len.saturating_add(header.pk_len);
        if len > 0xFFFF {
            return Err(PackError::value_rdata_oversize().set_unset_subject(&"HIP"));
        }
        writer.write_bytes(&self.public_key[..])?;
        for n in &self.servers {
            len = len.saturating_add(n.packed_len()?);
            if len > 0xFFFF {
                return Err(PackError::value_rdata_oversize().set_unset_subject(&"HIP"));
            }
            n.pack(writer)?;
        }
        Ok(())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let header = WireHeader {
            hit_len: self.hit.len(),
            pk_alg: self.alg,
            pk_len: self.public_key.len(),
        };
        let mut len = header.packed_len()?;
        len += header.hit_len;
        len = len.saturating_add(header.pk_len);
        for n in &self.servers {
            len = len.saturating_add(n.packed_len()?);
        }
        if len > 0xFFFF {
            Err(PackError::value_rdata_oversize())
        } else {
            Ok(len)
        }
    }
}

fn cmp_wire(
    name_compare: fn(&mut Reader<'_>, &mut Reader<'_>) -> Result<Ordering, WireOrdError>,
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
) -> Result<Ordering, WireOrdError> {
    let left_initial = left.index();
    let hit_len_l = u8::unpack(left).map_err(WireOrdError::Left)?;
    if hit_len_l == 0 {
        return Err(WireOrdError::Left(UnpackError::value_small()));
    }
    let hit_len_r = u8::unpack(right).map_err(WireOrdError::Right)?;
    if hit_len_r == 0 {
        return Err(WireOrdError::Right(UnpackError::value_small()));
    }
    match hit_len_l.cmp(&hit_len_r) {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    match IpseckeyAlg::cmp_wire(left, right)? {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    let pk_len_l_index = left.index();
    let pk_len_l = <[u8; 2]>::unpack(left).map_err(WireOrdError::Left)?;
    if pk_len_l == [0u8; 2] {
        return Err(WireOrdError::Left(UnpackError::value_small_at(
            pk_len_l_index,
        )));
    }
    let pk_len_r_index = right.index();
    let pk_len_r = <[u8; 2]>::unpack(right).map_err(WireOrdError::Right)?;
    if pk_len_r == [0u8; 2] {
        return Err(WireOrdError::Right(UnpackError::value_small_at(
            pk_len_r_index,
        )));
    }
    match pk_len_l.cmp(&pk_len_r) {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    let hit_pk_len = usize::from(hit_len_l) + usize::from(u16::from_be_bytes(pk_len_l));
    match crate::datatypes::bytes::cmp_fixed(left, right, hit_pk_len)? {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    // TODO: this needs a rethink
    let mut len = left.index() - left_initial;
    while !left.is_empty() && !right.is_empty() && len <= 0xFFFF {
        let left_name_index = left.index();
        match name_compare(left, right)? {
            Ordering::Equal => (),
            other => return Ok(other),
        }
        len += Name::unpack(left.set_index(left_name_index))
            .unwrap()
            .packed_len()
            .unwrap();
    }
    if len > 0xFFFF {
        return Err(WireOrdError::Left(UnpackError::value_large_at(
            left_initial,
        )));
    }
    match (left.is_empty(), right.is_empty()) {
        (true, true) | (false, false) => Ok(Ordering::Equal),
        (true, false) => Ok(Ordering::Less),
        (false, true) => Ok(Ordering::Greater),
    }
}

fn cmp_as_wire(name_compare: fn(&Name, &Name) -> Ordering, left: &Hip, right: &Hip) -> Ordering {
    let left_hit_len = left.hit.len() as u8;
    let right_hit_len = right.hit.len() as u8;
    match left_hit_len.cmp(&right_hit_len) {
        Ordering::Equal => (),
        right => return right,
    }
    match left.alg.cmp_as_wire(&right.alg) {
        Ordering::Equal => (),
        right => return right,
    }
    match left.hit[..].cmp(&right.hit) {
        Ordering::Equal => (),
        right => return right,
    }
    match left.public_key[..].cmp(&right.public_key) {
        Ordering::Equal => (),
        right => return right,
    }
    let mut lservers = left.servers.iter();
    let mut rservers = right.servers.iter();
    loop {
        match (lservers.next(), rservers.next()) {
            (None, None) => break Ordering::Equal,
            (None, Some(_)) => break Ordering::Less,
            (Some(_), None) => break Ordering::Greater,
            (Some(l), Some(r)) => match name_compare(l, r) {
                Ordering::Equal => (),
                right => break right,
            },
        }
    }
}

impl WireOrd for Hip {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        cmp_wire(Name::cmp_wire, left, right)
    }
    fn cmp_wire_canonical(
        left: &mut Reader<'_>,
        right: &mut Reader<'_>,
    ) -> Result<Ordering, WireOrdError> {
        cmp_wire(Name::cmp_wire_canonical, left, right)
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        cmp_as_wire(Name::cmp_as_wire, self, other)
    }
    fn cmp_as_wire_canonical(&self, other: &Self) -> Ordering {
        cmp_as_wire(Name::cmp_as_wire_canonical, self, other)
    }
}
