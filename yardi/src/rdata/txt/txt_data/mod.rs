#[cfg(test)]
mod test;

use crate::{
    ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter},
    encoding::{parse_charstr, present_charstr},
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;
use core::{
    cmp::Ordering,
    fmt::{self, Debug},
    hash::{Hash, Hasher},
};

// TODO: DNS-SD k-v helpers?

/// Maximum content size (Max RDLEN less length prefix-bytes)
const MAX_PAYLOAD: usize = 65_279;

/// Contains one or more character strings.
#[derive(Clone, Default)]
// TODO: make this a regular struct with an inner?
pub struct TxtData<T>(T);

impl TxtData<Vec<u8>> {
    /// Create an empty `TxtData` with room for `capacity` bytes.
    pub fn with_capacity(capacity: usize) -> Self {
        TxtData(<Vec<u8>>::with_capacity(capacity))
    }
    /// Reserve capacity for at least `additional` more bytes.
    pub fn reserve(&mut self, additional: usize) {
        let max_add = 0xFFFF_usize.saturating_sub(self.len());
        self.0.reserve(additional.min(max_add));
    }
    /// Add a character string to the end of this `TxtData`.
    // TODO: Error::{InvalidCharStr, Overflow}
    pub fn push(&mut self, char_str: &[u8]) -> Result<(), ()> {
        if char_str.len() > 0xFF {
            return Err(());
        }
        if self.0.len() + 1 + char_str.len() > 0xFFFF {
            return Err(());
        }
        self.0.reserve(1 + char_str.len());
        self.0.push(char_str.len() as u8);
        self.0.extend_from_slice(char_str);
        Ok(())
    }
    /// Inserts a character string at position `index` within this `TxtData`.
    // TODO: Error::{InvalidCharStr, Overflow}
    pub fn insert(&mut self, index: usize, char_str: &[u8]) -> Result<(), ()> {
        if char_str.len() > 255 {
            return Err(());
        }
        let plen = char_str.len().saturating_add(1);
        let olen = self.0.len();
        let new_len = olen.saturating_add(plen);
        if new_len > 0xFFFF {
            return Err(());
        }
        let mut off = 0;
        for _ in 0..index {
            if off >= olen {
                return Err(());
            }
            off += self.0[off] as usize + 1;
        }
        if off > olen {
            return Err(());
        }
        self.0.reserve(plen);
        if off == olen {
            self.0.push(char_str.len() as u8);
            self.0.extend_from_slice(char_str);
            return Ok(());
        }
        self.0.resize(new_len, 0);
        let keep = off..olen;
        self.0.copy_within(keep, off + plen);
        self.0[off] = char_str.len() as u8;
        self.0[off + 1..off + plen].copy_from_slice(char_str);
        Ok(())
    }
    /// Replace the character string at position `index`.
    // TODO: Error::{InvalidCharStr, Overflow}
    pub fn replace(&mut self, index: usize, char_str: &[u8]) -> Result<(), ()> {
        if char_str.len() > 255 {
            return Err(());
        }
        let plen = char_str.len() + 1;
        let olen = self.0.len();
        let mut off = 0;
        for _ in 0..index {
            if off >= olen {
                return Err(());
            }
            off += self.0[off] as usize + 1;
        }
        if off >= olen {
            return Err(());
        }
        let cur_c_len = self.0[off] as usize;
        let new_len = olen - cur_c_len + char_str.len();
        if new_len > 0xFFFF {
            return Err(());
        }
        self.0.reserve(char_str.len().saturating_sub(cur_c_len));
        if self.0.len() != new_len {
            if new_len > olen {
                self.0.resize(new_len, 0);
            }
            let keep = off + 1 + cur_c_len..olen;
            self.0.copy_within(keep, off + plen);
            self.0.truncate(new_len);
        }
        self.0[off] = char_str.len() as u8;
        self.0[off + 1..off + plen].copy_from_slice(char_str);
        Ok(())
    }
    /// Remove the character string at position `index`.
    // TODO: Error::{InvalidCharStr, Overflow}
    pub fn remove(&mut self, index: usize) -> Result<(), ()> {
        let mut off = 0;
        for _ in 0..index {
            if off >= self.0.len() {
                return Err(());
            }
            off += self.0[off] as usize + 1;
        }
        if off >= self.0.len() {
            return Err(());
        }
        let cur_c_len = self.0[off] as usize;
        let cur_c_plen = cur_c_len + 1;
        let new_len = self.0.len() - cur_c_plen;
        let next_c_off = off + cur_c_plen;
        if next_c_off < self.0.len() {
            let keep = next_c_off..self.0.len();
            self.0.copy_within(keep, off);
            self.0.truncate(new_len);
        } else {
            self.0.truncate(new_len);
        }
        Ok(())
    }
}

impl<T: for<'a> From<&'a [u8]>> TxtData<T> {
    /// Create a `TxtData` by splitting `bytes` at 255 byte intervals.
    /// If `bytes` is empty, a `TxtData` containing a single empty
    /// character string will be constructed.
    // TODO: Error::Overflow
    pub fn from_splitting<B: AsRef<[u8]>>(bytes: B) -> Result<Self, ()> {
        Self::from_splitting_imp(bytes.as_ref())
    }
    fn from_splitting_imp(s: &[u8]) -> Result<Self, ()> {
        if s.is_empty() {
            return Ok(TxtData([0u8][..].into()));
        }
        if s.len() > MAX_PAYLOAD {
            return Err(());
        }
        let temp = &mut [0u8; 0xFFFF];
        let mut len = 0;
        {
            let mut tail = &mut temp[..];
            for c in s.chunks(0xFF) {
                let (head, rest) = tail.split_at_mut(c.len() + 1);
                tail = rest;
                head[0] = c.len() as u8;
                head[1..].copy_from_slice(c);
                len += head.len();
            }
        }
        Ok(TxtData(temp[..len].into()))
    }
}

impl<T: AsRef<[u8]>> TxtData<T> {
    /// Return an iterator over the character strings contained in this `TxtData`.
    pub fn iter(&self) -> TxtIter<'_> {
        TxtIter {
            buf: self.0.as_ref(),
        }
    }
    /// Length of this `TxtData`.
    pub fn len(&self) -> usize {
        self.0.as_ref().len()
    }
    /// Returns true if this `TxtData` is empty.
    pub fn is_empty(&self) -> bool {
        self.0.as_ref().is_empty()
    }
}

impl<T: AsRef<[u8]>> Eq for TxtData<T> {}

impl<T: AsRef<[u8]>> PartialEq for TxtData<T> {
    fn eq(&self, other: &Self) -> bool {
        self.0.as_ref() == other.0.as_ref()
    }
}

impl<T: AsRef<[u8]>> Ord for TxtData<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        self.0.as_ref().cmp(other.0.as_ref())
    }
}

impl<T: AsRef<[u8]>> PartialOrd for TxtData<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<T: AsRef<[u8]>> Hash for TxtData<T> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.0.as_ref().hash(state)
    }
}

impl<T: AsRef<[u8]>> Debug for TxtData<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        // TODO: character-string debug/display helper struct
        let mut temp = [0u8; (0xFF * 4)];
        let mut f = f.debug_list();
        for cstring in self.iter() {
            let mut dest_iter = temp[..].iter_mut().enumerate();
            for &b in cstring {
                let (_, target) = dest_iter.next().unwrap();
                // Debug formatter will escape '\' and '"' for us.
                match b {
                    b' '..=b'~' => {
                        *target = b;
                    }
                    _ => {
                        let (_, t1) = dest_iter.next().unwrap();
                        let (_, t2) = dest_iter.next().unwrap();
                        let (_, t3) = dest_iter.next().unwrap();
                        let ddd = crate::encoding::byte_to_ddd(b);
                        *target = ddd[0];
                        *t1 = ddd[1];
                        *t2 = ddd[2];
                        *t3 = ddd[3];
                    }
                }
            }
            let len = match dest_iter.next() {
                Some((len, _)) => len,
                None => temp.len(),
            };
            f.entry(&core::str::from_utf8(&temp[..len]).unwrap());
        }
        f.finish()
    }
}

impl<T: for<'a> From<&'a [u8]>> Parse for TxtData<T> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let temp = &mut [0u8; 0xFFFF];
        let mut len = 0;
        p.pull_rest(|s| {
            let temp = &mut temp[len..];
            let (prefix, rest) = temp.split_first_mut().ok_or_else(ParseError::value_large)?;
            let cap = rest.len().min(0xFF);
            let parsed = parse_charstr(s, &mut rest[..cap]).map(|s| s.len())?;
            *prefix = parsed as u8;
            len += 1 + parsed;
            Ok(())
        })?;
        if len > 0 {
            Ok(TxtData(temp[..len].into()))
        } else {
            Err(ParseError::no_token())
        }
    }
}

impl<T: AsRef<[u8]>> Present for TxtData<T> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        if self.0.as_ref().is_empty() {
            return Err(PresentError::value_small());
        }
        for s in self.iter() {
            present_charstr(s, p)?;
        }
        Ok(())
    }
}

impl<T: AsRef<[u8]>> Pack for TxtData<T> {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        let bytes = self.0.as_ref();
        if bytes.is_empty() {
            return Err(PackError::value_small());
        }
        dst.write_bytes(bytes)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let bytes = self.0.as_ref();
        if bytes.is_empty() {
            return Err(PackError::value_small());
        }
        Ok(bytes.len())
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for TxtData<T> {
    fn unpack(src: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let initial_index = src.index();
        let bytes = src.get_bytes_rest()?;
        if bytes.is_empty() {
            return Err(UnpackError::value_small_at(initial_index));
        }
        if bytes.len() > 0xFFFF {
            return Err(UnpackError::value_large_at(initial_index));
        }
        let mut temp = bytes;
        while !temp.is_empty() {
            let len = temp[0] as usize;
            temp = temp
                .get(1 + len..)
                .ok_or_else(|| UnpackError::value_invalid_at(initial_index))?;
        }
        Ok(TxtData(bytes.into()))
    }
}

impl<T> UnpackedLen for TxtData<T> {
    fn unpacked_len(src: &mut Reader<'_>) -> Result<usize, UnpackError> {
        <TxtData<&[u8]>>::unpack(src).map(|me| me.len())
    }
}

impl<T: AsRef<[u8]>> WireOrd for TxtData<T> {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        // TODO: shouldn't be empty
        loop {
            match (left.is_empty(), right.is_empty()) {
                (true, true) => return Ok(Ordering::Equal),
                (true, _) => return Ok(Ordering::Less),
                (_, true) => return Ok(Ordering::Greater),
                (false, false) => match crate::datatypes::bytes::cmp8(left, right, 0, 0xFF)? {
                    Ordering::Equal => (),
                    other => return Ok(other),
                },
            }
        }
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        self.0.as_ref().cmp(other.0.as_ref())
    }
}

/// An iterator over the contents of a `TxtData`.
#[derive(Debug)]
pub struct TxtIter<'a> {
    buf: &'a [u8],
}

impl<'a> Iterator for TxtIter<'a> {
    type Item = &'a [u8];
    fn next(&mut self) -> Option<Self::Item> {
        let (&len, rest) = self.buf.split_first()?;
        let (item, rest) = rest.split_at(usize::from(len));
        self.buf = rest;
        Some(item)
    }
}
