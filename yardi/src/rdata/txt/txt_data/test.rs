use super::{TxtData, MAX_PAYLOAD};
use crate::wire::{Pack, Writer};
#[cfg(not(feature = "std"))]
use alloc::{boxed::Box, vec};

#[test]
fn from_bytes_min() {
    assert_eq!(
        <TxtData<Box<[u8]>>>::from_splitting_imp(&[])
            .expect("ok")
            .len(),
        1
    );
}

#[test]
fn from_bytes_max() {
    let src = vec![0u8; MAX_PAYLOAD];
    let txt = <TxtData<Box<[u8]>>>::from_splitting_imp(&src[..]).expect("txt");
    assert_eq!(txt.len(), 0xFFFF);
}

#[test]
fn from_bytes_overflow() {
    let src = vec![0u8; MAX_PAYLOAD + 1];
    <TxtData<Box<[u8]>>>::from_splitting_imp(&src[..]).expect_err("txt");
}

#[test]
fn insert() {
    let mut buf = TxtData::default();
    buf.insert(0, b"123").expect("insert empty");
    assert_eq!(&buf.0[..], b"\x03123");
    buf.insert(0, b"abc").expect("insert start");
    assert_eq!(&buf.0[..], b"\x03abc\x03123");
    buf.insert(1, b"def").expect("insert middle");
    assert_eq!(&buf.0[..], b"\x03abc\x03def\x03123");
    buf.insert(3, b"456").expect("insert end");
    assert_eq!(&buf.0[..], b"\x03abc\x03def\x03123\x03456");
    buf.insert(5, b"").expect_err("insert beyond end");
    buf.insert(0, &[0u8; 0xFF + 1])
        .expect_err("insert too large char str");
    buf.0 = vec![0; 0xFFFF - 1];
    buf.insert(0xFFFF - 1, &[]).expect("insert to capacity");
    buf.insert(0, &[]).expect_err("insert too large txt buf");
}

#[test]
fn replace() {
    let mut buf = TxtData(b"\x01a\x00\x01z".into());
    buf.replace(1, b"123").expect("replace with larger");
    assert_eq!(&buf.0[..], b"\x01a\x03123\x01z");
    buf.replace(1, b"321").expect("replace with same");
    assert_eq!(&buf.0[..], b"\x01a\x03321\x01z");
    buf.replace(1, b"").expect("replace with smaller");
    assert_eq!(&buf.0[..], b"\x01a\x00\x01z");
    buf.replace(1, &[0u8; 0xFF + 1])
        .expect_err("replace with too large char str");
    buf.0 = vec![0; 0xFFFF - 1];
    buf.replace(0xFFFF - 2, &[0]).expect("replace to capacity");
    assert_eq!(buf.0.len(), 0xFFFF);
    let r = buf.replace(0xFFFF - 2, &[1, 2]);
    assert!(buf.0.len() <= 0xFFFF);
    r.expect_err("insert too large txt buf");
}

#[test]
fn remove() {
    let mut buf = TxtData(b"\x011\x012\x00\x013".into());
    buf.remove(2).expect("remove empty");
    assert_eq!(&buf.0[..], b"\x011\x012\x013");
    buf.remove(1).expect("remove 2");
    assert_eq!(&buf.0[..], b"\x011\x013");
    buf.remove(1).expect("remove 3");
    assert_eq!(&buf.0[..], b"\x011");
    buf.remove(1).expect_err("remove bad index");
}

#[test]
fn no_pack_empty() {
    let txt = <TxtData<&[u8]>>::default();
    let mut w = Writer::new(&mut []);
    assert!(txt.packed_len().is_err());
    assert!(txt.pack(&mut w).is_err());
}
