//! Support for IPSECKEY (IN) RDATA
#[cfg(test)]
mod test;

mod alg;

#[cfg(not(feature = "std"))]
use alloc::boxed::Box;
use core::{
    cmp::Ordering,
    net::{Ipv4Addr, Ipv6Addr},
    str::FromStr,
};

pub use self::alg::IpseckeyAlg;

use crate::{
    ascii::{Context, Parse, ParseError, Parser, Present, PresentError, Presenter},
    datatypes::{Class, Name, Ty},
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};

const GW_NONE: u8 = 0;
const GW_IPV4: u8 = 1;
const GW_IPV6: u8 = 2;
const GW_NAME: u8 = 3;

fn unpack_gateway(reader: &mut Reader<'_>) -> Result<u8, UnpackError> {
    let initial_index = reader.index();
    u8::unpack(reader).and_then(|n| {
        if n > 3 {
            Err(UnpackError::value_invalid_at(initial_index))
        } else {
            Ok(n)
        }
    })
}

/// Gateway
#[derive(Clone, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub enum IpseckeyGateway {
    /// None
    #[default]
    None,
    /// IPv4 address
    Ipv4(Ipv4Addr),
    /// IPv6 address
    Ipv6(Ipv6Addr),
    /// Host name
    Name(Name),
}

impl IpseckeyGateway {
    fn ty(&self) -> u8 {
        match *self {
            IpseckeyGateway::None => GW_NONE,
            IpseckeyGateway::Ipv4(_) => GW_IPV4,
            IpseckeyGateway::Ipv6(_) => GW_IPV6,
            IpseckeyGateway::Name(_) => GW_NAME,
        }
    }
}

#[doc = "IPSECKEY\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc4025\">RFC 4025: A Method for Storing IPsec Keying Material in DNS</a>"]
#[derive(Clone, Debug, Default, Eq, Ord, PartialEq, PartialOrd, Rdata)]
#[yardi(crate = "crate", rr_class = "Class::IN", rr_ty = "Ty::IPSECKEY")]
// TODO: public_key should be generic
pub struct Ipseckey {
    /// Preference among IPSECKEY RR at the same owner name. Lower preferred.
    pub precedence: u8,
    /// Public key's cryptographic algorithm.
    pub alg: IpseckeyAlg,
    /// Gateway to which an IPsec tunnel may be created.
    pub gateway: IpseckeyGateway,
    /// Public key.
    pub public_key: Box<[u8]>,
}

impl Parse for Ipseckey {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let me = if let Some(me) = crate::rdata::parse_known_generic(p)? {
            me
        } else {
            let precedence = u8::parse(p)?;
            let gateway_ty = u8::parse(p)?;
            if gateway_ty > 3 {
                return Err(ParseError::value_invalid());
            }
            let alg = IpseckeyAlg::parse(p)?;
            let gateway = match gateway_ty {
                GW_NONE => p.pull(|s| {
                    if s == "." {
                        Ok(IpseckeyGateway::None)
                    } else {
                        Err(ParseError::value_invalid())
                    }
                })?,
                GW_IPV4 => Ipv4Addr::parse(p).map(IpseckeyGateway::Ipv4)?,
                GW_IPV6 => Ipv6Addr::parse(p).map(IpseckeyGateway::Ipv6)?,
                GW_NAME => Name::parse(p).map(IpseckeyGateway::Name)?,
                _ => unreachable!(),
            };
            let public_key = crate::datatypes::bytes::parse_base64ws(p, 0, 0xFFFF)?;
            Ipseckey {
                precedence,
                alg,
                gateway,
                public_key,
            }
        };
        if me.packed_len().unwrap_or(!0) > 0xFFFF {
            return Err(ParseError::value_large());
        }
        Ok(me)
    }
}

impl Present for Ipseckey {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        // TODO: unwrap_or seems nqr
        if self.packed_len().unwrap_or(!0) > 0xFFFF {
            return Err(PresentError::value_invalid());
        }
        self.precedence.present(p)?;
        self.gateway.ty().present(p)?;
        self.alg.present(p)?;
        match self.gateway {
            IpseckeyGateway::None => p.write(".")?,
            IpseckeyGateway::Ipv4(ref gw) => gw.present(p)?,
            IpseckeyGateway::Ipv6(ref gw) => gw.present(p)?,
            IpseckeyGateway::Name(ref gw) => gw.present(p)?,
        };
        crate::datatypes::bytes::present_base64(&self.public_key, p, 0, 0xFFFF)
    }
}

impl Pack for Ipseckey {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        let mut ty_writer = writer.for_ty(Ty::IPSECKEY);
        let writer = ty_writer.as_mut();
        let _ = self.packed_len()?;
        self.precedence.pack(writer)?;
        self.gateway.ty().pack(writer)?;
        self.alg.pack(writer)?;
        match self.gateway {
            IpseckeyGateway::None => (),
            IpseckeyGateway::Ipv4(ref gw) => gw.pack(writer)?,
            IpseckeyGateway::Ipv6(ref gw) => gw.pack(writer)?,
            IpseckeyGateway::Name(ref gw) => gw.pack(writer)?,
        };
        writer.write_bytes(&self.public_key)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        let gw_len = match self.gateway {
            IpseckeyGateway::None => 0,
            IpseckeyGateway::Ipv4(_) => 4,
            IpseckeyGateway::Ipv6(_) => 16,
            IpseckeyGateway::Name(ref n) => n.len(),
        };
        let len = 3 + gw_len + self.public_key.len();
        if len > 0xFFFF {
            Err(PackError::value_large())
        } else {
            Ok(len)
        }
    }
}

impl<'a> Unpack<'a> for Ipseckey {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let precedence = u8::unpack(reader)?;
        let gateway_ty = unpack_gateway(reader)?;
        let alg = IpseckeyAlg::unpack(reader)?;
        let gateway = match gateway_ty {
            GW_NONE => IpseckeyGateway::None,
            GW_IPV4 => Ipv4Addr::unpack(reader).map(IpseckeyGateway::Ipv4)?,
            GW_IPV6 => Ipv6Addr::unpack(reader).map(IpseckeyGateway::Ipv6)?,
            GW_NAME => Name::unpack(reader).map(IpseckeyGateway::Name)?,
            _ => unreachable!(),
        };
        let public_key_index = reader.index();
        let public_key = reader.get_bytes_rest()?.into();
        let me = Ipseckey {
            precedence,
            alg,
            gateway,
            public_key,
        };
        if me.packed_len().unwrap_or(!0) > 0xFFFF {
            return Err(UnpackError::value_large_at(public_key_index));
        }
        Ok(me)
    }
}

impl UnpackedLen for Ipseckey {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        Ipseckey::unpack(reader).map(|t| t.packed_len().unwrap())
    }
}

fn cmp_wire(
    name_compare: fn(&mut Reader<'_>, &mut Reader<'_>) -> Result<Ordering, WireOrdError>,
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
) -> Result<Ordering, WireOrdError> {
    let left_initial = left.index();
    let right_initial = right.index();
    let _ = Ipseckey::unpacked_len(left).map_err(WireOrdError::Left)?;
    let _ = Ipseckey::unpacked_len(right).map_err(WireOrdError::Right)?;
    left.set_index(left_initial);
    right.set_index(right_initial);
    match u8::cmp_wire(left, right)? {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    let gw_ty_l = unpack_gateway(left).map_err(WireOrdError::Left)?;
    let gw_ty_r = unpack_gateway(right).map_err(WireOrdError::Left)?;
    match gw_ty_l.cmp(&gw_ty_r) {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    match u8::cmp_wire(left, right)? {
        Ordering::Equal => (),
        other => return Ok(other),
    }
    let gw_o = match gw_ty_l {
        GW_NONE => Ordering::Equal,
        GW_IPV4 => Ipv4Addr::cmp_wire(left, right)?,
        GW_IPV6 => Ipv6Addr::cmp_wire(left, right)?,
        GW_NAME => name_compare(left, right)?,
        _ => unreachable!(),
    };
    if gw_o != Ordering::Equal {
        return Ok(gw_o);
    }
    crate::datatypes::bytes::cmp(left, right, 0, 0xFFFF)
}

fn cmp_as_wire(
    name_compare: fn(&Name, &Name) -> Ordering,
    left: &Ipseckey,
    right: &Ipseckey,
) -> Ordering {
    match left.precedence.cmp_as_wire(&right.precedence) {
        Ordering::Equal => (),
        right => return right,
    }
    let gw_ordering = match (&left.gateway, &right.gateway) {
        (IpseckeyGateway::None, IpseckeyGateway::None) => Ordering::Equal,
        (IpseckeyGateway::Ipv4(l), IpseckeyGateway::Ipv4(r)) => l.cmp_as_wire(r),
        (IpseckeyGateway::Ipv6(l), IpseckeyGateway::Ipv6(r)) => l.cmp_as_wire(r),
        (IpseckeyGateway::Name(l), IpseckeyGateway::Name(r)) => name_compare(l, r),
        _ => return left.gateway.ty().cmp(&right.gateway.ty()),
    };
    match left.alg.cmp_as_wire(&right.alg) {
        Ordering::Equal => (),
        right => return right,
    }
    if gw_ordering == Ordering::Equal {
        left.public_key.cmp(&right.public_key)
    } else {
        gw_ordering
    }
}

impl WireOrd for Ipseckey {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        cmp_wire(Name::cmp_wire, left, right)
    }
    fn cmp_wire_canonical(
        left: &mut Reader<'_>,
        right: &mut Reader<'_>,
    ) -> Result<Ordering, WireOrdError> {
        cmp_wire(Name::cmp_wire_canonical, left, right)
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        cmp_as_wire(Name::cmp_as_wire, self, other)
    }
    fn cmp_as_wire_canonical(&self, other: &Self) -> Ordering {
        cmp_as_wire(Name::cmp_as_wire_canonical, self, other)
    }
}

impl FromStr for Ipseckey {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(s.as_bytes(), Context::shared_default())
    }
}
