use super::Ipseckey;
use crate::{
    msg::Rr,
    wire::{Pack, Reader, Unpack, WireOrd, Writer},
};
#[cfg(not(feature = "std"))]
use alloc::string::String;
use core::{cmp::Ordering, str::FromStr};

#[test]
fn rfc4025_a() {
    let s = "38.2.0.192.in-addr.arpa. 7200 IN IPSECKEY 10 1 2 192.0.2.38 AQNRU3mG7TVTO2BkR47usntb102uFJtugbo6BSGvgqt4AQ==";
    rfc4025_test(s);
}

#[test]
fn rfc4025_b() {
    let s = "38.2.0.192.in-addr.arpa. 7200 IN IPSECKEY 10 0 2 . AQNRU3mG7TVTO2BkR47usntb102uFJtugbo6BSGvgqt4AQ==";
    rfc4025_test(s);
}

#[test]
fn rfc4025_c() {
    let s = "38.2.0.192.in-addr.arpa. 7200 IN IPSECKEY 10 1 2 192.0.2.3 AQNRU3mG7TVTO2BkR47usntb102uFJtugbo6BSGvgqt4AQ==";
    rfc4025_test(s);
}

#[test]
fn rfc4025_d() {
    let s = "38.1.0.192.in-addr.arpa. 7200 IN IPSECKEY 10 3 2 mygateway.example.com. AQNRU3mG7TVTO2BkR47usntb102uFJtugbo6BSGvgqt4AQ==";
    rfc4025_test(s);
}

#[test]
fn rfc4025_e() {
    let s = "0.d.4.0.3.0.e.f.f.f.3.f.0.1.2.0.1.0.0.0.0.0.2.8.B.D.0.1.0.0.2.ip6.arpa. 7200 IN IPSECKEY 10 2 2 2001:db8:0:8002::2000:1 AQNRU3mG7TVTO2BkR47usntb102uFJtugbo6BSGvgqt4AQ==";
    rfc4025_test(s);
}

fn rfc4025_test(sample: &str) {
    let mut actual = String::with_capacity(sample.len());
    let rr = <Rr<Ipseckey>>::from_str(sample).expect("from_str");
    crate::ascii::present::to_string(&rr, &mut actual).expect("present");
    assert_eq!(sample, actual.as_str());
    let mut buf = vec![0; rr.data.packed_len().expect("packed_len")];
    let mut w = Writer::new(&mut buf[..]);
    rr.data.pack(&mut w).expect("pack");
    assert_eq!(w.remaining(), 0);
    let bytes = w.written();
    let mut r = Reader::new(bytes);
    let rt = Ipseckey::unpack(&mut r).expect("unpack");
    assert!(r.is_empty());
    assert_eq!(rt, rr.data);
    assert_eq!(
        Ok(Ordering::Equal),
        Ipseckey::cmp_wire(&mut Reader::new(bytes), &mut Reader::new(bytes),)
    );
}

#[test]
fn default() {
    Ipseckey::default();
}

const CASE_HG00_WIRE: &'static [u8] = include_bytes!("sample_hg_ipseckey_00.bin");

#[test]
fn wire_marshal_hg00() {
    crate::rdata::test::wire_marshal::<Ipseckey>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_hg00() {
    crate::rdata::test::wire_cmp::<Ipseckey>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_canonical_hg00() {
    crate::rdata::test::wire_cmp_canonical::<Ipseckey>(CASE_HG00_WIRE);
}

const CASE_HG00_ASCII: &str = include_str!("sample_hg_ipseckey_00.txt");

#[test]
fn ascii_roundtrip_hg00() {
    crate::rdata::test::ascii_roundtrip::<Ipseckey>(CASE_HG00_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg00() {
    crate::rdata::test::generic_ascii_roundtrip::<Ipseckey>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn ascii_to_wire_hg00() {
    crate::rdata::test::ascii_to_wire::<Ipseckey>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn wire_to_ascii_hg00() {
    crate::rdata::test::wire_to_ascii::<Ipseckey>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

const CASE_HG01_WIRE: &'static [u8] = include_bytes!("sample_hg_ipseckey_01.bin");

#[test]
fn wire_marshal_hg01() {
    crate::rdata::test::wire_marshal::<Ipseckey>(CASE_HG01_WIRE);
}

#[test]
fn wire_cmp_hg01() {
    crate::rdata::test::wire_cmp::<Ipseckey>(CASE_HG01_WIRE);
}

#[test]
fn wire_cmp_canonical_hg01() {
    crate::rdata::test::wire_cmp_canonical::<Ipseckey>(CASE_HG01_WIRE);
}

const CASE_HG01_ASCII: &str = include_str!("sample_hg_ipseckey_01.txt");

#[test]
fn ascii_roundtrip_hg01() {
    crate::rdata::test::ascii_roundtrip::<Ipseckey>(CASE_HG01_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg01() {
    crate::rdata::test::generic_ascii_roundtrip::<Ipseckey>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

#[test]
fn ascii_to_wire_hg01() {
    crate::rdata::test::ascii_to_wire::<Ipseckey>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

#[test]
fn wire_to_ascii_hg01() {
    crate::rdata::test::wire_to_ascii::<Ipseckey>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

const CASE_HG02_WIRE: &'static [u8] = include_bytes!("sample_hg_ipseckey_02.bin");

#[test]
fn wire_marshal_hg02() {
    crate::rdata::test::wire_marshal::<Ipseckey>(CASE_HG02_WIRE);
}

#[test]
fn wire_cmp_hg02() {
    crate::rdata::test::wire_cmp::<Ipseckey>(CASE_HG02_WIRE);
}

#[test]
fn wire_cmp_canonical_hg02() {
    crate::rdata::test::wire_cmp_canonical::<Ipseckey>(CASE_HG02_WIRE);
}

const CASE_HG02_ASCII: &str = include_str!("sample_hg_ipseckey_02.txt");

#[test]
fn ascii_roundtrip_hg02() {
    crate::rdata::test::ascii_roundtrip::<Ipseckey>(CASE_HG02_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg02() {
    crate::rdata::test::generic_ascii_roundtrip::<Ipseckey>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

#[test]
fn ascii_to_wire_hg02() {
    crate::rdata::test::ascii_to_wire::<Ipseckey>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

#[test]
fn wire_to_ascii_hg02() {
    crate::rdata::test::wire_to_ascii::<Ipseckey>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

const CASE_HG03_WIRE: &'static [u8] = include_bytes!("sample_hg_ipseckey_03.bin");

#[test]
fn wire_marshal_hg03() {
    crate::rdata::test::wire_marshal::<Ipseckey>(CASE_HG03_WIRE);
}

#[test]
fn wire_cmp_hg03() {
    crate::rdata::test::wire_cmp::<Ipseckey>(CASE_HG03_WIRE);
}

#[test]
fn wire_cmp_canonical_hg03() {
    crate::rdata::test::wire_cmp_canonical::<Ipseckey>(CASE_HG03_WIRE);
}

const CASE_HG03_ASCII: &str = include_str!("sample_hg_ipseckey_03.txt");

#[test]
fn ascii_roundtrip_hg03() {
    crate::rdata::test::ascii_roundtrip::<Ipseckey>(CASE_HG03_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg03() {
    crate::rdata::test::generic_ascii_roundtrip::<Ipseckey>(CASE_HG03_ASCII, CASE_HG03_WIRE);
}

#[test]
fn ascii_to_wire_hg03() {
    crate::rdata::test::ascii_to_wire::<Ipseckey>(CASE_HG03_ASCII, CASE_HG03_WIRE);
}

#[test]
fn wire_to_ascii_hg03() {
    crate::rdata::test::wire_to_ascii::<Ipseckey>(CASE_HG03_ASCII, CASE_HG03_WIRE);
}

const CASE_HG04_WIRE: &'static [u8] = include_bytes!("sample_hg_ipseckey_04.bin");

#[test]
fn wire_marshal_hg04() {
    crate::rdata::test::wire_marshal::<Ipseckey>(CASE_HG04_WIRE);
}

#[test]
fn wire_cmp_hg04() {
    crate::rdata::test::wire_cmp::<Ipseckey>(CASE_HG04_WIRE);
}

#[test]
fn wire_cmp_canonical_hg04() {
    crate::rdata::test::wire_cmp_canonical::<Ipseckey>(CASE_HG04_WIRE);
}

const CASE_HG04_ASCII: &str = include_str!("sample_hg_ipseckey_04.txt");

#[test]
fn ascii_roundtrip_hg04() {
    crate::rdata::test::ascii_roundtrip::<Ipseckey>(CASE_HG04_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg04() {
    crate::rdata::test::generic_ascii_roundtrip::<Ipseckey>(CASE_HG04_ASCII, CASE_HG04_WIRE);
}

#[test]
fn ascii_to_wire_hg04() {
    crate::rdata::test::ascii_to_wire::<Ipseckey>(CASE_HG04_ASCII, CASE_HG04_WIRE);
}

#[test]
fn wire_to_ascii_hg04() {
    crate::rdata::test::wire_to_ascii::<Ipseckey>(CASE_HG04_ASCII, CASE_HG04_WIRE);
}
