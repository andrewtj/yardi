//! Support for LOC RDATA
#[cfg(test)]
mod test;

use core::{cmp::Ordering, iter, str::FromStr};

use crate::ascii::parse::Pull;
use crate::ascii::{Context, Parse, ParseError, Parser, Present, PresentError, Presenter};
use crate::datatypes::int::{parse_u32, parse_u8};
use crate::datatypes::Ty;
use crate::wire::{
    Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
};

fn split_dec(s: &[u8], max: usize) -> (&[u8], &[u8]) {
    if let Some(index) = s
        .iter()
        .rev()
        .take(max)
        .position(|&b| b == b'.')
        .map(|n| n + 1)
        .map(|n| s.len() - n)
    {
        (&s[..index], &s[index + 1..])
    } else {
        (s, &[])
    }
}

#[doc = "LOC - Location Information\n\n"]
#[doc = "Unpacking a LOC with a version other than zero will return `UnpackError::UnknownVersion`.\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc1876\">RFC 1876: A Means for Expressing Location Information in the Domain Name System</a>"]
#[derive(
    Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord, Pack, Unpack, UnpackedLen, Rdata,
)]
#[yardi(crate = "crate", rr_ty = "Ty::LOC")]
pub struct Loc {
    ver: V0,
    size: Size,
    hp: HorizPre,
    vp: VertPre,
    lat: Lat,
    lon: Lon,
    alt: Alt,
}

impl Loc {
    /// Size a tuple of `(base, exponent)`.
    pub fn size_base_exp(&self) -> (u8, u8) {
        self.size.base_exp()
    }
    /// Size in cm.
    pub fn size_as_cm(&self) -> u64 {
        self.size.as_cm()
    }
    /// Set size. Neither `base` or `exp` can be larger than nine.
    /// If `base` is zero `exp` must also be zero.
    // TODO: Error::{Overflow, Invalid}
    pub fn set_size(&mut self, base: u8, exp: u8) -> Result<(), ()> {
        self.size.set(base, exp)
    }
    /// Horizontal precision as a tuple of `(base, exponent)`.
    pub fn hp_base_exp(&self) -> (u8, u8) {
        self.hp.base_exp()
    }
    /// Horizonal precision in cm.
    pub fn hp_as_cm(&self) -> u64 {
        self.hp.as_cm()
    }
    /// Set horizonal precision. Neither `base` or `exp` can be larger than nine.
    /// If `base` is zero `exp` must also be zero.
    // TODO: Error::{Overflow, Invalid}
    pub fn set_hp(&mut self, base: u8, exponent: u8) -> Result<(), ()> {
        self.hp.set(base, exponent)
    }
    /// Vertical precision as a tuple of `(base, exponent)`.
    pub fn vp_base_exp(&self) -> (u8, u8) {
        self.vp.base_exp()
    }
    /// Vertical precision in cm.
    pub fn vp_as_cm(&self) -> u64 {
        self.vp.as_cm()
    }
    /// Set vertical precision. Neither `base` or `exp` can be larger than nine.
    /// If `base` is zero `exp` must also be zero.
    // TODO: Error::{Overflow, Invalid}
    pub fn set_vp(&mut self, base: u8, exponent: u8) -> Result<(), ()> {
        self.vp.set(base, exponent)
    }
    /// Latitude in milliarcseconds.
    pub fn lat_as_ams(&self) -> i32 {
        self.lat.as_mas()
    }
    /// Set latitude in milliarcseconds.
    // TODO: Error::Invalid
    pub fn set_lat(&mut self, mas: i32) -> Result<(), ()> {
        self.lat.set(mas)
    }
    /// Longitude in milliarcseconds.
    pub fn lon_as_ams(&self) -> i32 {
        self.lon.as_mas()
    }
    /// Set longitude in milliarcseconds.
    // TODO: Error::Invalid
    pub fn set_lon(&mut self, mas: i32) -> Result<(), ()> {
        self.lon.set(mas)
    }
    /// Altitude in cm.
    pub fn alt_as_cm(&self) -> i64 {
        self.alt.as_cm()
    }
    /// Set altitude in cm.
    // TODO: Error::Invalid
    pub fn set_alt(&mut self, cm: i64) -> Result<(), ()> {
        self.alt.set(cm)
    }
}

impl Parse for Loc {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        if let Some(loc) = crate::rdata::parse_known_generic(p)? {
            return Ok(loc);
        }
        let lat = Parse::parse(p)?;
        let lon = Parse::parse(p)?;
        let alt = Parse::parse(p)?;
        let size = Parse::parse(p)?;
        let hp = Parse::parse(p)?;
        let vp = Parse::parse(p)?;
        Ok(Loc {
            ver: V0,
            size,
            hp,
            vp,
            lat,
            lon,
            alt,
        })
    }
}

impl Present for Loc {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        self.lat.present(p)?;
        self.lon.present(p)?;
        self.alt.present(p)?;
        self.size.present(p)?;
        self.hp.present(p)?;
        self.vp.present(p)
    }
}

impl WireOrd for Loc {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        V0::unpack(left).map_err(WireOrdError::Left)?;
        V0::unpack(right).map_err(WireOrdError::Left)?;
        let l = left.peek(15).map_err(WireOrdError::Left)?;
        let r = right.peek(15).map_err(WireOrdError::Right)?;
        let order = l.cmp(r);
        left.skip(15).unwrap();
        right.skip(15).unwrap();
        Ok(order)
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        match self.size.cmp_as_wire(&other.size) {
            Ordering::Equal => (),
            other => return other,
        }
        match self.hp.cmp_as_wire(&other.hp) {
            Ordering::Equal => (),
            other => return other,
        }
        match self.vp.cmp_as_wire(&other.vp) {
            Ordering::Equal => (),
            other => return other,
        }
        match self.lat.cmp_as_wire(&other.lat) {
            Ordering::Equal => (),
            other => return other,
        }
        match self.lon.cmp_as_wire(&other.lon) {
            Ordering::Equal => (),
            other => return other,
        }
        self.alt.cmp_as_wire(&other.alt)
    }
}

impl FromStr for Loc {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(s.as_bytes(), Context::shared_default())
    }
}

#[derive(Clone, Copy, Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
struct V0;

impl Pack for V0 {
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        0u8.pack(writer)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(1)
    }
}

impl<'a> Unpack<'a> for V0 {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        reader.get_byte().and_then(|v| {
            if v == 0 {
                Ok(V0)
            } else {
                Err(UnpackError::value_unexpected_version_at(reader.index() - 1))
            }
        })
    }
}

impl UnpackedLen for V0 {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        Self::unpack(reader)?;
        Ok(1)
    }
}

const DEG_MAS: u32 = AM_PER_DEG * AM_MAS;
const AM_MAS: u32 = AS_PER_AM * AS_MAS;
const AS_MAS: u32 = MAS_PER_AS * MAS;
const MAS: u32 = 1;

const AM_PER_DEG: u32 = 60;
const AS_PER_AM: u32 = 60;
const MAS_PER_AS: u32 = 1000;

// 0 .. 90 | 180
fn parse_deg(p: &mut Parser<'_, '_>, max: u32) -> Result<u32, ParseError> {
    p.pull(|s| {
        let t = u32::from(parse_u8(s)?) * DEG_MAS;
        if t > max {
            Err(ParseError::value_large())
        } else {
            Ok(t)
        }
    })
}

// 0 .. 59
fn parse_min(p: &mut Parser<'_, '_>) -> Result<u32, ParseError> {
    p.pull(|s| {
        let s = s.as_bytes();
        match s[0] {
            b'0'..=b'9' => {
                let n = parse_u8(s).ok().map(u32::from).unwrap_or(u32::MAX);
                if n > 59 {
                    Pull::Reject(ParseError::value_large())
                } else {
                    Pull::Accept(n * AM_MAS)
                }
            }
            _ => Pull::Decline(0),
        }
    })
}

// 0 .. 59.999
fn parse_sec(p: &mut Parser<'_, '_>) -> Result<u32, ParseError> {
    p.pull(|s| {
        let (sec_s, fracsec_s) = split_dec(s.as_bytes(), 4);
        let mut mas = match sec_s.first().cloned() {
            Some(b'0'..=b'9') => match parse_u32(sec_s) {
                Ok(n) if n < 60 => n * AS_MAS,
                Ok(_) => return Pull::Reject(ParseError::value_large()),
                Err(err) => return Pull::Reject(err),
            },
            _ => return Pull::Decline(0),
        };
        for (b, m) in fracsec_s.iter().cloned().zip([100, 10, 1].iter()) {
            match b.saturating_sub(b'0') {
                n @ 0..=9 => mas += u32::from(n) * m,
                _ => return Pull::Reject(ParseError::value_invalid()),
            }
        }
        Pull::Accept(mas)
    })
}

const DEG_BASE: u32 = 1 << 31;
/*
macro_rules! impl_deg_ty {
    ($ty:ident, $max_deg:expr, $pth:expr, $ptl:expr) => {
        #[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Pack)]
        #[yardi(crate = "crate")]
        struct $ty(u32);

        impl $ty {
            const MAX_MAS: u32 = $max_deg * 60 * 60 * 1000;
            fn as_mas(&self) -> i32 {
                if self.0 > DEG_BASE {
                    (self.0 - DEG_BASE) as i32
                } else {
                    -((DEG_BASE - self.0) as i32)
                }
            }
            fn set(&mut self, mas: i32) -> Result<(), ()> {
                if mas.is_positive() {
                    let offset = mas as u32;
                    if offset > $ty::MAX_MAS {
                        return Err(());
                    }
                    self.0 = DEG_BASE + offset;
                    Ok(())
                } else {
                    let offset = -mas as u32;
                    if offset > $ty::MAX_MAS {
                        return Err(());
                    }
                    self.0 = DEG_BASE - offset;
                    Ok(())
                }
            }
        }

        impl Default for $ty {
            fn default() -> Self {
                $ty(DEG_BASE)
            }
        }

        impl Parse for $ty {
            fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
                let offset = parse_deg(p, $ty::MAX_MAS)? + parse_min(p)? + parse_sec(p)?;
                let high = p.pull(|s| {
                    let s = s.as_bytes();
                    if s.len() != 1 {
                        return Err(ParseError::value_invalid());
                    }
                    match !0x20 & s[0] {
                        $pth => Ok(true),
                        $ptl => Ok(false),
                        _ => Err(ParseError::value_invalid()),
                    }
                })?;
                if offset > $ty::MAX_MAS {
                    return Err(ParseError::value_large());
                }
                let value = if high {
                    DEG_BASE + offset
                } else {
                    DEG_BASE - offset
                };
                Ok($ty(value))
            }
        }

        impl Present for $ty {
            fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
                let point;
                let mut v;
                if self.0 >= DEG_BASE {
                    v = self.0 - DEG_BASE;
                    point = $pth;
                } else {
                    v = DEG_BASE - self.0;
                    point = $ptl;
                }
                let fracsec = v % 1000;
                v /= 1000;
                let sec = v % 60;
                v /= 60;
                let min = v % 60;
                v /= 60;
                let deg = v;
                write!(
                    p,
                    "{} {} {}.{:03} {}",
                    deg, min, sec, fracsec, point as char
                )
            }
        }

        impl Unpack for $ty {
            fn unpack(reader: &mut Reader<'_>) -> Result<Self, UnpackError> {
                let initial_index = reader.index();
                u32::unpack(reader).and_then(|n| {
                    let v = if n > DEG_BASE {
                        n - DEG_BASE
                    } else {
                        DEG_BASE - n
                    };
                    if v > $ty::MAX_MAS {
                        Err(UnpackError::value_large_at(initial_index))
                    } else {
                        Ok($ty(n))
                    }
                })
            }
        }

        impl UnpackedLen for $ty {
            fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
                $ty::unpack(reader).map(|_| 4)
            }
        }

        impl WireOrd for $ty {
            fn cmp_wire(
                left: &mut Reader<'_>,
                right: &mut Reader<'_>,
            ) -> Result<Ordering, WireOrdError> {
                let (l_initial, r_initial) = (left.index(), right.index());
                let _ = $ty::unpack(left).map_err(WireOrdError::Left)?;
                let _ = $ty::unpack(right).map_err(WireOrdError::Right)?;
                left.set_index(l_initial);
                right.set_index(r_initial);
                <u32 as WireOrd>::cmp_wire(left, right)
            }
            fn cmp_as_wire(&self, other: &Self) -> Ordering {
                u32::cmp_as_wire(&self.0, &other.0)
            }
        }
    };
}

impl_deg_ty!(Lat, 90, b'N', b'S');
impl_deg_ty!(Lon, 180, b'E', b'W');
*/

type Lat = DegTy<90, b'N', b'S'>;
type Lon = DegTy<180, b'E', b'W'>;

#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Pack)]
#[yardi(crate = "crate")]
struct DegTy<const MAX_DEG: u32, const PTH: u8, const PTL: u8>(u32);

impl<const MAX_DEG: u32, const PTH: u8, const PTL: u8> DegTy<MAX_DEG, PTH, PTL> {
    const MAX_MAS: u32 = MAX_DEG * 60 * 60 * 1000;
    fn as_mas(&self) -> i32 {
        if self.0 > DEG_BASE {
            (self.0 - DEG_BASE) as i32
        } else {
            -((DEG_BASE - self.0) as i32)
        }
    }
    fn set(&mut self, mas: i32) -> Result<(), ()> {
        if mas.is_positive() {
            let offset = mas as u32;
            if offset > Self::MAX_MAS {
                return Err(());
            }
            self.0 = DEG_BASE + offset;
            Ok(())
        } else {
            let offset = -mas as u32;
            if offset > Self::MAX_MAS {
                return Err(());
            }
            self.0 = DEG_BASE - offset;
            Ok(())
        }
    }
}

impl<const MAX_DEG: u32, const PTH: u8, const PTL: u8> Default for DegTy<MAX_DEG, PTH, PTL> {
    fn default() -> Self {
        DegTy(DEG_BASE)
    }
}

impl<const MAX_DEG: u32, const PTH: u8, const PTL: u8> Parse for DegTy<MAX_DEG, PTH, PTL> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let offset = parse_deg(p, Self::MAX_MAS)? + parse_min(p)? + parse_sec(p)?;
        let high = p.pull(|s| {
            let s = s.as_bytes();
            if s.len() != 1 {
                return Err(ParseError::value_invalid());
            }
            match !0x20 & s[0] {
                b if b == PTH => Ok(true),
                b if b == PTL => Ok(false),
                _ => Err(ParseError::value_invalid()),
            }
        })?;
        if offset > Self::MAX_MAS {
            return Err(ParseError::value_large());
        }
        let value = if high {
            DEG_BASE + offset
        } else {
            DEG_BASE - offset
        };
        Ok(Self(value))
    }
}

impl<const MAX_DEG: u32, const PTH: u8, const PTL: u8> Present for DegTy<MAX_DEG, PTH, PTL> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        let point;
        let mut v;
        if self.0 >= DEG_BASE {
            v = self.0 - DEG_BASE;
            point = PTH;
        } else {
            v = DEG_BASE - self.0;
            point = PTL;
        }
        let fracsec = v % 1000;
        v /= 1000;
        let sec = v % 60;
        v /= 60;
        let min = v % 60;
        v /= 60;
        let deg = v;
        write!(
            p,
            "{} {} {}.{:03} {}",
            deg, min, sec, fracsec, point as char
        )
    }
}

impl<'a, const MAX_DEG: u32, const PTH: u8, const PTL: u8> Unpack<'a> for DegTy<MAX_DEG, PTH, PTL> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let initial_index = reader.index();
        u32::unpack(reader).and_then(|n| {
            let v = if n > DEG_BASE {
                n - DEG_BASE
            } else {
                DEG_BASE - n
            };
            if v > Self::MAX_MAS {
                Err(UnpackError::value_large_at(initial_index))
            } else {
                Ok(Self(n))
            }
        })
    }
}

impl<const MAX_DEG: u32, const PTH: u8, const PTL: u8> UnpackedLen for DegTy<MAX_DEG, PTH, PTL> {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        Self::unpack(reader).map(|_| 4)
    }
}

impl<const MAX_DEG: u32, const PTH: u8, const PTL: u8> WireOrd for DegTy<MAX_DEG, PTH, PTL> {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let (l_initial, r_initial) = (left.index(), right.index());
        let _ = Self::unpack(left).map_err(WireOrdError::Left)?;
        let _ = Self::unpack(right).map_err(WireOrdError::Right)?;
        left.set_index(l_initial);
        right.set_index(r_initial);
        <u32 as WireOrd>::cmp_wire(left, right)
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        u32::cmp_as_wire(&self.0, &other.0)
    }
}

#[derive(
    Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
struct Alt(u32);

#[allow(clippy::inconsistent_digit_grouping)]
const ALT_FLOOR: u32 = 100_000_00;
const ALT_NEGMAX: u32 = ALT_FLOOR;
#[allow(clippy::inconsistent_digit_grouping)]
const ALT_POSMAX: u32 = 42_849_672_95;

impl Alt {
    fn as_cm(self) -> i64 {
        i64::from(self.0) - i64::from(ALT_FLOOR)
    }
    fn set(&mut self, cm: i64) -> Result<(), ()> {
        if cm > i64::from(ALT_POSMAX) {
            return Err(());
        }
        let cm = cm + i64::from(ALT_FLOOR);
        if cm.is_negative() {
            return Err(());
        }
        self.0 = cm as u32;
        Ok(())
    }
}

impl Default for Alt {
    fn default() -> Self {
        Alt(ALT_FLOOR)
    }
}

impl Parse for Alt {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        // [-100000.00 .. 42849672.95] BY .01
        // presentation is meters
        // wire rep is cm above -100,000m
        if p.peek()?.is_none() {
            return Ok(Alt(0));
        }
        p.pull(|s| {
            let s = s.as_bytes();
            match s.last().cloned() {
                Some(b'm') | Some(b'M') => (),
                _ => return Err(ParseError::value_invalid()),
            }
            let (m, cm) = split_dec(&s[..s.len() - 1], 3);
            let (mut alt, neg) = match m.first().cloned() {
                Some(b'-') => (parse_u32(&m[1..])?, true),
                Some(b'+') => (parse_u32(&m[1..])?, false),
                Some(b'0'..=b'9') => (parse_u32(m)?, false),
                _ => return Err(ParseError::value_invalid()),
            };
            alt = alt.checked_mul(100).ok_or_else(ParseError::value_invalid)?;
            let mut rest = cm.iter().cloned();
            for mul in [10, 1].iter() {
                alt = rest
                    .next()
                    .unwrap_or(b'0')
                    .checked_sub(b'0')
                    .and_then(|n| {
                        if n > 9 {
                            None
                        } else {
                            u32::from(n * mul).checked_add(alt)
                        }
                    })
                    .ok_or_else(ParseError::value_invalid)?;
            }
            if rest.next().is_some() {
                return Err(ParseError::value_invalid());
            }
            if neg {
                if alt <= ALT_NEGMAX {
                    return Ok(Alt(ALT_FLOOR - alt));
                }
            } else if alt <= ALT_POSMAX {
                return Ok(Alt(ALT_FLOOR + alt));
            }
            Err(ParseError::value_invalid())
        })
    }
}

impl Present for Alt {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        let (sign, all_cm) = if self.0 < ALT_FLOOR {
            ("-", ALT_FLOOR - self.0)
        } else {
            ("", self.0 - ALT_FLOOR)
        };
        let (m, rem_cm) = (all_cm / 100, all_cm % 100);
        write!(p, "{}{}.{:02}m", sign, m, rem_cm)
    }
}

// [0 .. 90000000.00]m
fn parse_size(p: &mut Parser<'_, '_>) -> Result<u8, ParseError> {
    p.pull(|s| {
        let (m_s, cm_s) = match s.as_bytes().split_last() {
            Some((&b'm', rest)) | Some((&b'M', rest)) => split_dec(rest, 3),
            _ => return Err(ParseError::value_invalid()),
        };
        if m_s.len() > 8 || cm_s.len() > 2 {
            return Err(ParseError::value_invalid());
        }
        let iter = m_s
            .iter()
            .cloned()
            .chain(cm_s.iter().cloned().chain(iter::repeat(b'0')).take(2));
        let mut exp = 0u8;
        let mut base = 0u8;
        for b in iter {
            match b {
                b'0' if exp < 9 => exp += 1,
                b'1'..=b'9' if base == 0 => {
                    base = (b - b'0') << 4;
                    exp = 0;
                }
                // TODO: other impls seem to do this... it's not in the spec though
                b'1'..=b'9' => exp += 1,
                _ => return Err(ParseError::value_invalid()),
            }
        }
        if base == 0 {
            Ok(0)
        } else {
            Ok(base | exp)
        }
    })
}

fn is_valid_size(v: u8) -> bool {
    let base = v >> 4;
    let pot = 0xF & v;
    if base == 0 {
        pot == 0
    } else {
        base < 10 && pot < 10
    }
}

fn unpack_size(reader: &mut Reader<'_>) -> Result<u8, UnpackError> {
    let initial_index = reader.index();
    let v = reader.get_byte()?;
    if is_valid_size(v) {
        Ok(v)
    } else {
        Err(UnpackError::value_invalid_at(initial_index))
    }
}

fn cmp_wire_size(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
    let a = unpack_size(left).map_err(WireOrdError::Left)?;
    let b = unpack_size(right).map_err(WireOrdError::Right)?;
    Ok(a.cmp(&b))
}

fn present_size(v: u8, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    if !is_valid_size(v) {
        return Err(PresentError::value_invalid());
    }
    let base = v >> 4;
    let pot = 0xF & v;
    match pot {
        0 => write!(p, "0.0{}m", base),
        1 => write!(p, "0.{}0m", base),
        _ => write!(p, "{}m", 10u32.pow(u32::from(pot) - 2) * u32::from(base)),
    }
}

// TODO: Rename SizeTy ... just implement Default manually?
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Pack)]
#[yardi(crate = "crate")]
struct SizeImpl<const DEFAULT: u8>(u8);

impl<const DEFAULT: u8> SizeImpl<DEFAULT> {
    fn base_exp(&self) -> (u8, u8) {
        (self.0 >> 4, 0xF & self.0)
    }
    fn as_cm(&self) -> u64 {
        let (base, exp) = self.base_exp();
        10u64.pow(u32::from(exp)) * u64::from(base)
    }
    fn set(&mut self, base: u8, exp: u8) -> Result<(), ()> {
        if base < 10 && exp < 10 {
            let v = (base << 4) | exp;
            if is_valid_size(v) {
                self.0 = v;
                return Ok(());
            }
        }
        Err(())
    }
}

impl<const DEFAULT: u8> Default for SizeImpl<DEFAULT> {
    fn default() -> Self {
        SizeImpl(DEFAULT)
    }
}

impl<const DEFAULT: u8> Parse for SizeImpl<DEFAULT> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        if p.peek()?.is_none() {
            return Ok(SizeImpl::default());
        }
        Ok(SizeImpl(parse_size(p)?))
    }
}

impl<const DEFAULT: u8> Present for SizeImpl<DEFAULT> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        present_size(self.0, p)
    }
}

impl<'a, const DEFAULT: u8> Unpack<'a> for SizeImpl<DEFAULT> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        unpack_size(reader).map(SizeImpl)
    }
}

impl<const DEFAULT: u8> UnpackedLen for SizeImpl<DEFAULT> {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        unpack_size(reader).map(|_| 1)
    }
}

impl<const DEFAULT: u8> WireOrd for SizeImpl<DEFAULT> {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        cmp_wire_size(left, right)
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        self.0.cmp(&other.0)
    }
}

type Size = SizeImpl<0x12>;
type VertPre = SizeImpl<0x13>;
type HorizPre = SizeImpl<0x16>;

#[test]
fn test_size() {
    // TODO: these samples come from https://github.com/miekg/dns/pull/1132/
    //       other impls seem happy with 16m (nsupdate, bind?)
    let samples: &[(&str, u8)] = &[
        ("0.01M", 0x10),
        ("0.1m", 0x11),
        ("1m", 0x12),
        ("10m", 0x13),
        ("0.03m", 0x30),
        ("0.3m", 0x31),
        ("90000000m", 0x99),
        ("16m", 0x13),
        ("0.16m", 0x11),
    ];
    let c = Default::default();
    for (s, n) in samples.iter().cloned() {
        assert_eq!(
            crate::ascii::parse::from_single_entry::<Size>(s.as_bytes(), &c).map(|t| t.0),
            Ok(n)
        );
    }
}
