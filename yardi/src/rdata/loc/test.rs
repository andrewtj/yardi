#[cfg(not(feature = "std"))]
use alloc::string::String;

use core::{cmp::Ordering, str::FromStr};

use super::{Alt, HorizPre, Loc, Size, VertPre, ALT_POSMAX};
use crate::msg::Rr;
use crate::wire::{Pack, Reader, Unpack, WireOrd, Writer};

#[test]
fn test_alt() {
    let mut t = Alt::default();
    assert_eq!(t.as_cm(), 0);
    assert!(t.set(-100_000_01).is_err());
    assert!(t.set(-100_000_00).is_ok());
    assert_eq!(t.as_cm(), -100_000_00);
    let pos_max = i64::from(ALT_POSMAX);
    assert!(t.set(pos_max + 1).is_err());
    assert!(t.set(pos_max).is_ok());
    assert_eq!(t.as_cm(), pos_max);
}

macro_rules! size_test {
    ($test_name:ident, $ty:ident, $def_cm:expr) => {
        #[test]
        fn $test_name() {
            let mut s = $ty::default();
            assert_eq!(s.as_cm(), $def_cm);
            assert!(s.set(0, 9).is_err());
            assert!(s.set(9, 9).is_ok());
            assert_eq!(s.as_cm(), 90000000_00);
            assert!(s.set(0, 0).is_ok());
            assert_eq!(s.as_cm(), 0);
        }
    };
}
size_test!(test_size, Size, 100);
size_test!(test_horiz_pre, HorizPre, 1000000);
size_test!(test_vert_pre, VertPre, 1000);

// Test samples from RFC 1876 have been run through nsupdate to normalise them.

#[test]
fn rfc1876_a() {
    let input = "cambridge-net.kei.com. 0 IN LOC 42 21 54 N 71 06 18 W -24m 30m";
    let expect =
        "cambridge-net.kei.com. 0 IN LOC 42 21 54.000 N 71 6 18.000 W -24.00m 30m 10000m 10m";
    rfc1876_test(input, expect);
}

#[test]
fn rfc1876_b() {
    let input = "loiosh.kei.com. 0 IN LOC 42 21 43.952 N 71 5 6.344 W -24m 1m 200m";
    let expect = "loiosh.kei.com. 0 IN LOC 42 21 43.952 N 71 5 6.344 W -24.00m 1m 200m 10m";
    rfc1876_test(input, expect);
}

#[test]
fn rfc1876_c() {
    let input = "pipex.net. 0 IN LOC 52 14 05 N 00 08 50 E 10m";
    let expect = "pipex.net. 0 IN LOC 52 14 5.000 N 0 8 50.000 E 10.00m 1m 10000m 10m";
    rfc1876_test(input, expect);
}

#[test]
fn rfc1876_d() {
    let input = "curtin.edu.au. 0 IN LOC 32 7 19 S 116 2 25 E 10m";
    let expect = "curtin.edu.au. 0 IN LOC 32 7 19.000 S 116 2 25.000 E 10.00m 1m 10000m 10m";
    rfc1876_test(input, expect);
}

#[test]
fn rfc1876_e() {
    let input = "rwy04L.logan-airport.boston. 0 IN LOC 42 21 28.764 N 71 00 51.617 W -44m 2000m";
    let expect = "rwy04L.logan-airport.boston. 0 IN LOC 42 21 28.764 N 71 0 51.617 W -44.00m 2000m 10000m 10m";
    rfc1876_test(input, expect);
}

fn rfc1876_test(input: &str, expect: &str) {
    let mut actual = String::with_capacity(expect.len());
    let rr = <Rr<Loc>>::from_str(input).expect("from_str");
    crate::ascii::present::to_string(&rr, &mut actual).expect("present");
    assert_eq!(expect, actual.as_str());
    let mut buf = vec![0; rr.data.packed_len().expect("packed_len")];
    let mut w = Writer::new(&mut buf[..]);
    rr.data.pack(&mut w).expect("pack");
    assert_eq!(w.remaining(), 0);
    let bytes = w.written();
    let mut r = Reader::new(bytes);
    let rt = Loc::unpack(&mut r).expect("unpack");
    assert!(r.is_empty());
    assert_eq!(rt, rr.data);
    assert_eq!(
        Ok(Ordering::Equal),
        Loc::cmp_wire(&mut Reader::new(bytes), &mut Reader::new(bytes))
    );
}

const CASE_HG00_WIRE: &'static [u8] = include_bytes!("sample_hg_loc_00.bin");

#[test]
fn wire_marshal_hg00() {
    crate::rdata::test::wire_marshal::<Loc>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_hg00() {
    crate::rdata::test::wire_cmp::<Loc>(CASE_HG00_WIRE);
}

#[test]
fn wire_cmp_canonical_hg00() {
    crate::rdata::test::wire_cmp_canonical::<Loc>(CASE_HG00_WIRE);
}

const CASE_HG00_ASCII: &str = include_str!("sample_hg_loc_00.txt");

#[test]
fn ascii_roundtrip_hg00() {
    crate::rdata::test::ascii_roundtrip::<Loc>(CASE_HG00_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg00() {
    crate::rdata::test::generic_ascii_roundtrip::<Loc>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn ascii_to_wire_hg00() {
    crate::rdata::test::ascii_to_wire::<Loc>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

#[test]
fn wire_to_ascii_hg00() {
    crate::rdata::test::wire_to_ascii::<Loc>(CASE_HG00_ASCII, CASE_HG00_WIRE);
}

const CASE_HG01_WIRE: &'static [u8] = include_bytes!("sample_hg_loc_01.bin");

#[test]
fn wire_marshal_hg01() {
    crate::rdata::test::wire_marshal::<Loc>(CASE_HG01_WIRE);
}

#[test]
fn wire_cmp_hg01() {
    crate::rdata::test::wire_cmp::<Loc>(CASE_HG01_WIRE);
}

#[test]
fn wire_cmp_canonical_hg01() {
    crate::rdata::test::wire_cmp_canonical::<Loc>(CASE_HG01_WIRE);
}

const CASE_HG01_ASCII: &str = include_str!("sample_hg_loc_01.txt");

#[test]
fn ascii_roundtrip_hg01() {
    crate::rdata::test::ascii_roundtrip::<Loc>(CASE_HG01_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg01() {
    crate::rdata::test::generic_ascii_roundtrip::<Loc>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

#[test]
fn ascii_to_wire_hg01() {
    crate::rdata::test::ascii_to_wire::<Loc>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

#[test]
fn wire_to_ascii_hg01() {
    crate::rdata::test::wire_to_ascii::<Loc>(CASE_HG01_ASCII, CASE_HG01_WIRE);
}

const CASE_HG02_WIRE: &'static [u8] = include_bytes!("sample_hg_loc_02.bin");

#[test]
fn wire_marshal_hg02() {
    crate::rdata::test::wire_marshal::<Loc>(CASE_HG02_WIRE);
}

#[test]
fn wire_cmp_hg02() {
    crate::rdata::test::wire_cmp::<Loc>(CASE_HG02_WIRE);
}

#[test]
fn wire_cmp_canonical_hg02() {
    crate::rdata::test::wire_cmp_canonical::<Loc>(CASE_HG02_WIRE);
}

const CASE_HG02_ASCII: &str = include_str!("sample_hg_loc_02.txt");

#[test]
fn ascii_roundtrip_hg02() {
    crate::rdata::test::ascii_roundtrip::<Loc>(CASE_HG02_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg02() {
    crate::rdata::test::generic_ascii_roundtrip::<Loc>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

#[test]
fn ascii_to_wire_hg02() {
    crate::rdata::test::ascii_to_wire::<Loc>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

#[test]
fn wire_to_ascii_hg02() {
    crate::rdata::test::wire_to_ascii::<Loc>(CASE_HG02_ASCII, CASE_HG02_WIRE);
}

const CASE_HG03_WIRE: &'static [u8] = include_bytes!("sample_hg_loc_03.bin");

#[test]
fn wire_marshal_hg03() {
    crate::rdata::test::wire_marshal::<Loc>(CASE_HG03_WIRE);
}

#[test]
fn wire_cmp_hg03() {
    crate::rdata::test::wire_cmp::<Loc>(CASE_HG03_WIRE);
}

#[test]
fn wire_cmp_canonical_hg03() {
    crate::rdata::test::wire_cmp_canonical::<Loc>(CASE_HG03_WIRE);
}

const CASE_HG03_ASCII: &str = include_str!("sample_hg_loc_03.txt");

#[test]
fn ascii_roundtrip_hg03() {
    crate::rdata::test::ascii_roundtrip::<Loc>(CASE_HG03_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg03() {
    crate::rdata::test::generic_ascii_roundtrip::<Loc>(CASE_HG03_ASCII, CASE_HG03_WIRE);
}

#[test]
fn ascii_to_wire_hg03() {
    crate::rdata::test::ascii_to_wire::<Loc>(CASE_HG03_ASCII, CASE_HG03_WIRE);
}

#[test]
fn wire_to_ascii_hg03() {
    crate::rdata::test::wire_to_ascii::<Loc>(CASE_HG03_ASCII, CASE_HG03_WIRE);
}

const CASE_HG04_WIRE: &'static [u8] = include_bytes!("sample_hg_loc_04.bin");

#[test]
fn wire_marshal_hg04() {
    crate::rdata::test::wire_marshal::<Loc>(CASE_HG04_WIRE);
}

#[test]
fn wire_cmp_hg04() {
    crate::rdata::test::wire_cmp::<Loc>(CASE_HG04_WIRE);
}

#[test]
fn wire_cmp_canonical_hg04() {
    crate::rdata::test::wire_cmp_canonical::<Loc>(CASE_HG04_WIRE);
}

const CASE_HG04_ASCII: &str = include_str!("sample_hg_loc_04.txt");

#[test]
fn ascii_roundtrip_hg04() {
    crate::rdata::test::ascii_roundtrip::<Loc>(CASE_HG04_ASCII);
}

#[test]
fn generic_ascii_roundtrip_hg04() {
    crate::rdata::test::generic_ascii_roundtrip::<Loc>(CASE_HG04_ASCII, CASE_HG04_WIRE);
}

#[test]
fn ascii_to_wire_hg04() {
    crate::rdata::test::ascii_to_wire::<Loc>(CASE_HG04_ASCII, CASE_HG04_WIRE);
}

#[test]
fn wire_to_ascii_hg04() {
    crate::rdata::test::wire_to_ascii::<Loc>(CASE_HG04_ASCII, CASE_HG04_WIRE);
}
