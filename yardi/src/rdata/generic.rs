use crate::{
    ascii::{Context, ParseError, Parser, Present, PresentError, Presenter},
    datatypes::{Class, Ty},
    rdata::{self, CopyError, PresentWireError, Rdata, StaticRdata},
    wire::{Pack, PackError, Reader, Unpack, UnpackError, WireOrd, WireOrdError, Writer},
};
#[cfg(not(feature = "std"))]
use alloc::{boxed::Box, vec::Vec};
use core::cmp::Ordering;

/// A generic container of wire format record data.
#[derive(Clone, Debug)]
pub struct Generic<B = Box<[u8]>> {
    cl: Class,
    ty: Ty,
    buf: B,
    len: usize,
}

impl<B> Generic<B> {
    /// Consumes self returning the wrapped buffer.
    pub fn into_bytes(self) -> B {
        self.buf
    }
    /// Gets a reference to the wrapped buffer.
    pub fn get_ref(&self) -> &B {
        &self.buf
    }
    fn wire_class_ty_check(cl: Class, ty: Ty) -> Result<(), UnpackError> {
        // TODO: likely needs to be inlined to get index to line up
        // TODO: more specific error?
        if !cl.is_data() || !ty.is_data() {
            Err(UnpackError::value_invalid())
        } else {
            Ok(())
        }
    }
}

impl<B> Generic<B>
where
    B: for<'a> From<&'a [u8]>,
{
    /// Unpack data.
    // TODO: another helper to support zero-copy when possible
    pub fn unpack(cl: Class, ty: Ty, r: &mut Reader<'_>) -> Result<Self, UnpackError> {
        Self::wire_class_ty_check(cl, ty)?;
        let mut buf = [0u8; 0xFFFF];
        let mut w = Writer::new(&mut buf[..]);
        let start = r.index();
        rdata::copy(cl, ty, r, &mut w).map_err(|err| {
            if let CopyError::Unpack(err) = err {
                return err;
            }
            // PackError could theoretically occur if the rdata decompressed
            // to a larger value, though that's not possible with any current
            // RDATA format. Meta Class/Type check would have already caught
            // those variants of CopyError.
            UnpackError::value_large_at(start)
        })?;
        let buf = w.into_written();
        let len = buf.len();
        let buf = buf.into();
        Ok(Generic { cl, ty, buf, len })
    }
    /// Unpacks two byte length prefixed data that references the `Reader` for storage.
    pub fn unpack16(cl: Class, ty: Ty, r: &mut Reader<'_>) -> Result<Self, UnpackError> {
        Self::wire_class_ty_check(cl, ty)?;
        let initial_index = r.index();
        let mut r16 = r.reader16()?;
        let me = Self::unpack(cl, ty, &mut r16)?;
        if r16.is_empty() {
            Ok(me)
        } else {
            Err(UnpackError::trailing_junk_at(initial_index))
        }
    }
    /// Create a `Generic` from a `&[u8]`.
    pub fn from_bytes(cl: Class, ty: Ty, bytes: &[u8]) -> Result<Self, UnpackError> {
        Self::wire_class_ty_check(cl, ty)?;
        let len_result = {
            let mut r = Reader::new(bytes);
            rdata::unpacked_len(cl, ty, &mut r).and_then(|len| {
                if r.is_empty() {
                    Ok(len)
                } else {
                    Err(UnpackError::trailing_junk())
                }
            })
        };
        match len_result {
            Ok(len) => Ok(Generic {
                cl,
                ty,
                buf: bytes.into(),
                len,
            }),
            Err(err) => Err(err),
        }
    }
}

impl<B> Generic<B>
where
    B: From<Vec<u8>>,
{
    /// Create a `Generic` by parsing a string.
    pub fn from_str(cl: Class, ty: Ty, s: &str) -> Result<Self, ParseError> {
        Self::from_str_context(cl, ty, s, Context::shared_default())
    }
    /// Create a `Generic` by parsing a string with a `Context`.
    pub fn from_str_context(cl: Class, ty: Ty, s: &str, c: &Context) -> Result<Self, ParseError> {
        let buf = crate::ascii::parse::from_single_entry_f(s.as_bytes(), c, |parser| {
            rdata::parse(cl, ty, parser)
        })?;
        let len = buf.len();
        let buf = buf.into();
        Ok(Generic { cl, ty, buf, len })
    }
    /// Create a `Generic` from a `Parser`.
    pub fn parse(cl: Class, ty: Ty, p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        if !cl.is_data() || !ty.is_data() {
            return Err(ParseError::value_invalid());
        }
        let buf = rdata::parse(cl, ty, p)?;
        let len = buf.len();
        let buf = buf.into();
        Ok(Generic { cl, ty, buf, len })
    }
}

impl<B> Generic<B>
where
    B: AsRef<[u8]>,
{
    /// Pack data to the `Writer`.
    pub fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        rdata::copy(self.cl, self.ty, &mut self.reader(), w).map_err(|err| {
            if let CopyError::Pack(err) = err {
                err
            } else {
                unreachable!()
            }
        })
    }
    /// Pack data to the `Writer` with a two-byte length prefix.
    pub fn pack16(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        self.pack(w.writer16()?.as_mut())
    }
    /// Return this `Generic`'s type.
    pub fn ty(&self) -> Ty {
        self.ty
    }
    /// Return this `Generic`'s class.
    pub fn class(&self) -> Class {
        self.cl
    }
    /// Return this `Generic`'s length.
    #[allow(clippy::len_without_is_empty)]
    pub fn len(&self) -> usize {
        self.len
    }
    /// Return a `Reader` over this generic's data.
    pub fn reader(&self) -> Reader<'_> {
        Reader::new_index(self.buf.as_ref(), 0)
    }
    pub fn try_into_rdata<T: StaticRdata + for<'a> Unpack<'a>>(&self) -> Result<T, UnpackError> {
        if let Some(tclass) = T::CLASS {
            if self.class() != tclass {
                // TODO: more specific error?
                return Err(UnpackError::value_invalid());
            }
        }
        if self.ty() != T::TY {
            // TODO: more specific error?
            return Err(UnpackError::value_invalid());
        }
        let mut r = self.reader();
        let t = T::unpack(&mut r)?;
        if r.is_empty() {
            Ok(t)
        } else {
            // TODO: better error
            Err(UnpackError::trailing_junk_at(r.index()))
        }
    }
}

impl<B> Eq for Generic<B> where B: AsRef<[u8]> {}

impl<B> PartialEq for Generic<B>
where
    B: AsRef<[u8]>,
{
    fn eq(&self, other: &Self) -> bool {
        self.cl == other.cl
            && self.ty == other.ty
            && rdata::eq(self.cl, self.ty, &mut self.reader(), &mut other.reader()).unwrap_or(false)
    }
}

impl<B> PartialOrd for Generic<B>
where
    B: AsRef<[u8]>,
{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<B> Ord for Generic<B>
where
    B: AsRef<[u8]>,
{
    fn cmp(&self, other: &Self) -> Ordering {
        self.cl
            .cmp(&other.cl)
            .then_with(|| self.ty.cmp(&other.ty))
            .then_with(|| {
                let mut a = self.reader();
                let mut b = other.reader();
                rdata::cmp(self.cl, self.ty, &mut a, &mut b).expect("valid data")
            })
    }
}

impl<B> Rdata for Generic<B> {
    // TODO: should this try to filter on Ty?
    fn class(&self) -> Option<Class> {
        Some(self.cl)
    }
    fn ty(&self) -> Ty {
        self.ty
    }
}

impl<B> Present for Generic<B>
where
    B: AsRef<[u8]>,
{
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        rdata::present(self.cl, self.ty, &mut self.reader(), p).map_err(|err| {
            if let PresentWireError::Present(err) = err {
                err
            } else {
                unreachable!()
            }
        })
    }
}

impl<B> Pack for Generic<B>
where
    B: AsRef<[u8]>,
{
    fn pack(&self, writer: &mut Writer<'_>) -> Result<(), PackError> {
        rdata::copy(
            self.cl,
            self.ty,
            &mut self.reader(),
            writer.for_ty(self.ty).as_mut(),
        )
        .map_err(|err| {
            if let CopyError::Pack(err) = err {
                err
            } else {
                unreachable!()
            }
        })
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(self.len())
    }
}

// TODO: this is a bad abstraction
impl<B> WireOrd for Generic<B>
where
    B: AsRef<[u8]>,
{
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        // TODO: this is a bit useless
        let left = left.get_bytes_rest().map_err(WireOrdError::Left)?;
        let right = right.get_bytes_rest().map_err(WireOrdError::Right)?;
        Ok(left.cmp(right))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        // TODO: should this panic if class and ty don't match?
        // TODO: what's the right thing to do with an error?
        rdata::cmp(self.cl, self.ty, &mut self.reader(), &mut other.reader())
            .unwrap_or_else(|_err| self.buf.as_ref().cmp(other.buf.as_ref()))
    }
}
