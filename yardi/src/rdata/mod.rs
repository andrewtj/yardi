//! Resource Record Datas
//! # Canonical RR Data Construction
#![doc = r###"
This example walks through constructing canonically sorted CNAME datas from
non-canonical format CNAME datas.
```
use std::borrow::Cow;
use std::cmp::Ordering;

use yardi::ascii;
use yardi::datatypes::{name, Class, Ty};
use yardi::dnssec;
use yardi::rdata::{self, Cname, Generic, Rdata};
use yardi::wire::{self, Reader, Writer, Pack};

// These will be the values of the CNAMEs. A properly behaved authoritative
// would only include one of the first two because in canonical format they
// are equivalent.
let cname_targets = &[
    name!("example.com."),
    name!("EXAMPLE.COM."),
    name!("WWW.EXAMPLE.COM."),
];

// Construct a buffer containing length-prefixed data and an index
// of those datas:
let mut index = Vec::new();
let mut buf = [0u8; 512];
let mut w = Writer::new(&mut buf);
w.set_format(wire::Format::DnsSensitive);
for target in cname_targets {
    index.push(w.len());
    Cname {
        cname: target.clone(),
    }.pack16(&mut w).expect("pack data");
}
let wire = w.written();

// From here on we'll refer to the sets class and type via variables since
// that's more representative.
let (cl, ty) = (Class::IN, Ty::CNAME);

// Not all types require canonicalisation. You'd check if a type needs
// canonicalisation in real code with `dnssec::canonical`:
assert!(dnssec::canonical(ty));

// This is where this example gets a little more realistic. We've got a
// an index of datas we want in a canonical set. Lets sort it and dedupe
// it with a canonical comparison:
index.sort_by(|&a, &b| {
    let mut a = Reader::new_index(&wire, a);
    let mut b = Reader::new_index(&wire, b);
    rdata::cmp_canonical16(cl, ty, &mut a, &mut b).expect("rdata is known good")
});
index.dedup_by(|&mut a, &mut b| {
    let mut a = Reader::new_index(&wire, a);
    let mut b = Reader::new_index(&wire, b);
    rdata::cmp_canonical16(cl, ty, &mut a, &mut b)
        .map(|order| order == Ordering::Equal)
        .expect("rdata is known good")
});

// And now copy the relevant datas to a new buffer in canonical format.
let mut c_b = [0u8; 512];
let mut c_w = Writer::new(&mut c_b);
c_w.set_format(wire::Format::Dnssec);
for &i in &index {
    let mut r = Reader::new_index(wire.clone(), i);
    rdata::copy16(cl, ty, &mut r, &mut c_w).expect("rdata is known good");
}
let c_wire = c_w.written();

// `c_wire` now contains the set in canonical format. For the sake of example
// we'll now present it:
let mut s = String::new();
let mut c_r = Reader::new(c_wire);
while !c_r.is_empty() {
    if !s.is_empty() {
        s.push(' ');
    }
    let data = <Generic>::unpack16(cl, ty, &mut c_r).expect("rdata is known good");
    ascii::present::to_string(&data, &mut s).expect("rdata is known good");
}
assert_eq!("www.example.com. example.com.", &s);
```
"###]
#[cfg(test)]
mod test;

mod generic;
mod hip;
pub mod ipseckey;
mod loc;
pub mod wks;

#[cfg(not(feature = "std"))]
use alloc::vec::Vec;
pub use yardi_derive::Rdata;

// TODO: add a From like trait that allows converting rdata structures
//       (can't use From because the blanket impl conflicts.)
// TODO: revisit now that this file is generated too.
// TODO: manually maintain mods and imports, move lookup_imp to module
include!("mod.in.rs");

use core::{cmp::Ordering, marker::PhantomData};

use crate::ascii::parse::Pull;
use crate::ascii::{Parse, ParseError, ParseErrorKind, Parser, Present, PresentError, Presenter};
use crate::datatypes::{Class, Ty};
use crate::wire::{Pack, PackError, Unpack, UnpackError, UnpackErrorKind, UnpackedLen};
use crate::wire::{Reader, WireOrd, WireOrdError, Writer};

pub use crate::rdata::generic::Generic;
pub use crate::rdata::hip::Hip;
#[doc(no_inline)]
pub use crate::rdata::ipseckey::Ipseckey;
pub use crate::rdata::loc::Loc;
#[doc(no_inline)]
pub use crate::rdata::wks::Wks;

/// Returns true for TYPEs that can be compressed in standard (unicast)
/// messages.
///
/// [RFC 3597](https://tools.ietf.org/html/rfc3597#section-4)
/// limits compression to the TYPEs in
/// [RFC 1035](https://tools.ietf.org/html/rfc1035). This crate further
/// whittles down that list to the TYPEs which aren't listed as
/// EXPERIMENTAL or Obsolete as support for those TYPEs has waned in
/// many DNS implementations.
///
/// Succinctly, this returns true for `Ty::NS`, `Ty::CNAME`, `Ty::SOA`,
/// `Ty::PTR` and `Ty::MX`.
pub fn compress(ty: Ty) -> bool {
    // MB, MD, MF, MG, MINFO and MR could be compressed but given
    // they've all but disappeared it's probably better not to.
    matches!(ty, Ty::NS | Ty::CNAME | Ty::SOA | Ty::PTR | Ty::MX)
}

/// Returns true for TYPEs that can be compressed in
/// [Multicast DNS (RFC 6762)](https://tools.ietf.org/html/rfc6762#section-18.14)
/// messages.
pub fn compress_mdns(ty: Ty) -> bool {
    matches!(
        ty,
        Ty::NS
            | Ty::CNAME
            | Ty::SOA
            | Ty::PTR
            | Ty::MX
            | Ty::RT
            | Ty::RP
            | Ty::AFSDB
            | Ty::PX
            | Ty::SRV
            | Ty::KX
            | Ty::DNAME
            | Ty::NSEC
    )
}

/// Implemented by types that represent a single RDATA.
pub trait StaticRdata {
    /// Class if the RDATA is Class-specific.
    const CLASS: Option<Class> = None;
    /// Type.
    const TY: Ty;
}

/// Implemented by types representing RDATA.
pub trait Rdata {
    /// Class if the RDATA is Class-specific.
    fn class(&self) -> Option<Class>;
    /// Type.
    fn ty(&self) -> Ty;
}

impl<T: StaticRdata> Rdata for T {
    fn class(&self) -> Option<Class> {
        T::CLASS
    }
    fn ty(&self) -> Ty {
        T::TY
    }
}

trait RdataImpTableExt {
    const IMP_TABLE: RdataImpTable;
}

impl<T> RdataImpTableExt for T
where
    T: StaticRdata + for<'a> Unpack<'a> + UnpackedLen + Pack + Parse + Present + WireOrd,
{
    const IMP_TABLE: RdataImpTable = RdataImpTable {
        copy: |r, w| {
            Self::unpack(r)
                .map_err(CopyError::Unpack)
                .and_then(|t| t.pack(w).map_err(CopyError::Pack))
        },
        unpacked_len: Self::unpacked_len,
        cmp_wire: Self::cmp_wire,
        cmp_wire_canonical: Self::cmp_wire_canonical,
        parse_bytes: |r| {
            let t = Self::parse(r)?;
            let mut buf = vec![0; t.packed_len().expect("Parsed unpackable")];
            let mut w = Writer::new(&mut buf);
            w.set_format(crate::wire::Format::Plain);
            t.pack(&mut w).expect("Parsed unpackable");
            let len = w.written().len();
            buf.truncate(len);
            Ok(buf)
        },
        present: |r, p| {
            let t = Self::unpack(r).map_err(PresentWireError::Unpack)?;
            t.present(p).map_err(PresentWireError::Present)
        },
    };
}

#[derive(Clone, Copy)]
struct RdataImpTable {
    copy: fn(&mut Reader<'_>, &mut Writer<'_>) -> Result<(), CopyError>,
    unpacked_len: fn(&mut Reader<'_>) -> Result<usize, UnpackError>,
    cmp_wire: fn(&mut Reader<'_>, &mut Reader<'_>) -> Result<Ordering, WireOrdError>,
    cmp_wire_canonical: fn(&mut Reader<'_>, &mut Reader<'_>) -> Result<Ordering, WireOrdError>,
    // TODO: Rename parse_to_wire?
    parse_bytes: fn(&mut Parser<'_, '_>) -> Result<Vec<u8>, ParseError>,
    present: fn(&mut Reader<'_>, &mut Presenter<'_, '_>) -> Result<(), PresentWireError>,
}

impl core::fmt::Debug for RdataImpTable {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("RdataImpTable").finish()
    }
}

/// An error returned from a copy function.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum CopyError {
    /// The class provided was not a data class.
    MetaClass,
    /// The type provided was not a data type.
    MetaTy,
    /// An error occured during unpacking.
    Unpack(UnpackError),
    /// An error occured during packing.
    Pack(PackError),
}

impl From<UnpackError> for CopyError {
    fn from(err: UnpackError) -> Self {
        CopyError::Unpack(err)
    }
}

impl From<PackError> for CopyError {
    fn from(err: PackError) -> Self {
        CopyError::Pack(err)
    }
}

/// An error returned when comparing data in wire format.
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum CmpWireError {
    /// The class provided was not a data class.
    MetaClass,
    /// The type provided was not a data type.
    MetaTy,
    /// The "left" reader encountered an unpack error.
    Left(UnpackError),
    /// The "right" reader encountered an unpack error.
    Right(UnpackError),
}

/// An error returned when parsing to wire format.
#[derive(Debug)]
pub enum ParseWireError {
    /// A parse error occurred.
    Parse(ParseError),
    /// A pack error occurred.
    Pack(PackError),
}

/// An error returned when presenting wire data.
#[derive(Debug)]
pub enum PresentWireError {
    /// The class provided was not a data class.
    MetaClass,
    /// The type provided was not a data type.
    MetaTy,
    /// An unpack error occurred.
    Unpack(UnpackError),
    /// A present error occurred.
    Present(PresentError),
}

impl From<UnpackError> for PresentWireError {
    fn from(err: UnpackError) -> Self {
        PresentWireError::Unpack(err)
    }
}

impl From<PresentError> for PresentWireError {
    fn from(err: PresentError) -> Self {
        PresentWireError::Present(err)
    }
}

/// Copy rdata from a `Reader` to a `Writer`.
pub fn copy(cl: Class, ty: Ty, r: &mut Reader<'_>, w: &mut Writer<'_>) -> Result<(), CopyError> {
    if !ty.is_data() {
        return Err(CopyError::MetaTy);
    }
    if !cl.is_data() {
        return Err(CopyError::MetaClass);
    }
    if let Some(imp) = lookup_imp(cl, ty) {
        (imp.copy)(r, w)
    } else {
        crate::datatypes::bytes::unpack::<&[u8]>(r, 0, 0xFFFF)
            .map_err(CopyError::Unpack)
            .and_then(|s| w.write_bytes(s).map_err(CopyError::Pack))
    }
}

/// A variant of `copy` that expects a length-prefixed data and writes length-prefixed data.
pub fn copy16(cl: Class, ty: Ty, r: &mut Reader<'_>, w: &mut Writer<'_>) -> Result<(), CopyError> {
    let mut r16 = r.reader16()?;
    let mut w16 = w.writer16()?;
    copy(cl, ty, &mut r16, &mut w16)
}

/// Returns the (decompressed) unpacked length of a data.
///
/// `Class` must be a data Class and `Ty` must be a data Type.
pub fn unpacked_len(cl: Class, ty: Ty, r: &mut Reader<'_>) -> Result<usize, UnpackError> {
    if !ty.is_data() {
        return Err(UnpackError::value_invalid());
    }
    if !cl.is_data() {
        return Err(UnpackError::value_invalid());
    }
    if let Some(imp) = lookup_imp(cl, ty) {
        (imp.unpacked_len)(r)
    } else {
        crate::datatypes::bytes::unpacked_len(r, 0, 0xFFFF)
    }
}

/// A variant of `unpacked_len` that expects a length-prefixed data.
pub fn unpacked_len16(cl: Class, ty: Ty, r: &mut Reader<'_>) -> Result<usize, UnpackError> {
    let mut r16 = r.reader16()?;
    unpacked_len(cl, ty, &mut r16)
}

/// Check if rdata in wire format is equal.
// TODO: This checks names sensitively - could do with a better name or docs that indicate this
pub fn eq(cl: Class, ty: Ty, l: &mut Reader<'_>, r: &mut Reader<'_>) -> Result<bool, CmpWireError> {
    cmp(cl, ty, l, r).map(|o| o == Ordering::Equal)
}

/// A variant of `eq` that expects length-prefixed data.
pub fn eq16(
    cl: Class,
    ty: Ty,
    l: &mut Reader<'_>,
    r: &mut Reader<'_>,
) -> Result<bool, CmpWireError> {
    cmp16(cl, ty, l, r).map(|o| o == Ordering::Equal)
}

/// Compare rdata in wire format with names decompressed.
///
/// `Reader`s must be positioned at the start of the data and must end at the end of the data.
///
/// ```
/// # use std::cmp::Ordering;
/// # use yardi::wire::Reader;
/// # use yardi::datatypes::{Class, Ty};
/// # use yardi::rdata::cmp;
/// let mut a = Reader::new(b"\x07example\x03com\x00");
/// let mut b = Reader::new(b"\x07EXAMPLE\x03COM\x00");
/// assert_eq!(Ok(Ordering::Greater), cmp(Class::IN, Ty::CNAME, &mut a, &mut b));
/// ```
/// ```
/// # use std::cmp::Ordering;
/// # use yardi::wire::Reader;
/// # use yardi::datatypes::{Class, Ty};
/// # use yardi::rdata::cmp;
/// let mut a = Reader::new(b"\x04www1\x07example\x03com\x00");
/// let mut b = Reader::new(b"\x04WWWW2\x07EXAMPLE\x03COM\x00");
/// assert_eq!(Ok(Ordering::Greater), cmp(Class::IN, Ty::CNAME, &mut a, &mut b));
/// ```
pub fn cmp(
    cl: Class,
    ty: Ty,
    l: &mut Reader<'_>,
    r: &mut Reader<'_>,
) -> Result<Ordering, CmpWireError> {
    if !ty.is_data() {
        return Err(CmpWireError::MetaTy);
    }
    if !cl.is_data() {
        return Err(CmpWireError::MetaClass);
    }
    if let Some(imp) = lookup_imp(cl, ty) {
        (imp.cmp_wire)(l, r).map_err(|err| match err {
            WireOrdError::Left(err) => CmpWireError::Left(err),
            WireOrdError::Right(err) => CmpWireError::Right(err),
        })
    } else {
        let left =
            crate::datatypes::bytes::unpack::<&[u8]>(l, 0, 0xFFFF).map_err(CmpWireError::Left)?;
        let right =
            crate::datatypes::bytes::unpack::<&[u8]>(r, 0, 0xFFFF).map_err(CmpWireError::Right)?;
        Ok(left.cmp(right))
    }
}

/// A variant of `cmp` that compares length-prefixed datas.
pub fn cmp16(
    cl: Class,
    ty: Ty,
    l: &mut Reader<'_>,
    r: &mut Reader<'_>,
) -> Result<Ordering, CmpWireError> {
    let l_initial_index = l.index();
    let mut l16 = l.reader16().map_err(CmpWireError::Left)?;
    let r_initial_index = r.index();
    let mut r16 = r.reader16().map_err(CmpWireError::Right)?;
    cmp(cl, ty, &mut l16, &mut r16).and_then(|order| {
        if order == Ordering::Equal {
            if !l16.is_empty() {
                let err = UnpackError::trailing_junk_at(l_initial_index);
                return Err(CmpWireError::Left(err));
            } else if !r16.is_empty() {
                let err = UnpackError::trailing_junk_at(r_initial_index);
                return Err(CmpWireError::Right(err));
            }
        }
        Ok(order)
    })
}

/// Compare rdata in wire format with names decompressed and labels in canonical form.
///
/// `Reader`s must be positioned at the start of the data and must end at the end of the data.
///
/// ```
/// # use std::cmp::Ordering;
/// # use yardi::wire::Reader;
/// # use yardi::datatypes::{Class, Ty};
/// # use yardi::rdata::cmp_canonical;
/// let mut a = Reader::new(b"\x07example\x03com\x00");
/// let mut b = Reader::new(b"\x07EXAMPLE\x03COM\x00");
/// assert_eq!(Ok(Ordering::Equal), cmp_canonical(Class::IN, Ty::CNAME, &mut a, &mut b));
/// ```
/// ```
/// # use std::cmp::Ordering;
/// # use yardi::wire::Reader;
/// # use yardi::datatypes::{Class, Ty};
/// # use yardi::rdata::cmp_canonical;
/// let mut a = Reader::new(b"\x04www1\x07example\x03com\x00");
/// let mut b = Reader::new(b"\x04WWWW2\x07EXAMPLE\x03COM\x00");
/// assert_eq!(Ok(Ordering::Less), cmp_canonical(Class::IN, Ty::CNAME, &mut a, &mut b));
/// ```
pub fn cmp_canonical(
    cl: Class,
    ty: Ty,
    l: &mut Reader<'_>,
    r: &mut Reader<'_>,
) -> Result<Ordering, CmpWireError> {
    if !ty.is_data() {
        return Err(CmpWireError::MetaTy);
    }
    if !cl.is_data() {
        return Err(CmpWireError::MetaClass);
    }
    if let Some(imp) = lookup_imp(cl, ty) {
        (imp.cmp_wire_canonical)(l, r).map_err(|err| match err {
            WireOrdError::Left(err) => CmpWireError::Left(err),
            WireOrdError::Right(err) => CmpWireError::Right(err),
        })
    } else {
        let left =
            crate::datatypes::bytes::unpack::<&[u8]>(l, 0, 0xFFFF).map_err(CmpWireError::Left)?;
        let right =
            crate::datatypes::bytes::unpack::<&[u8]>(r, 0, 0xFFFF).map_err(CmpWireError::Right)?;
        Ok(left.cmp(right))
    }
}

/// A variant of `cmp_canonical` that compares length-prefixed rdata.
pub fn cmp_canonical16(
    cl: Class,
    ty: Ty,
    l: &mut Reader<'_>,
    r: &mut Reader<'_>,
) -> Result<Ordering, CmpWireError> {
    let l_initial_index = l.index();
    let mut l16 = l.reader16().map_err(CmpWireError::Left)?;
    let r_initial_index = r.index();
    let mut r16 = r.reader16().map_err(CmpWireError::Right)?;
    cmp_canonical(cl, ty, &mut l16, &mut r16).and_then(|order| {
        if order == Ordering::Equal {
            if !l16.is_empty() {
                let err = UnpackError::trailing_junk_at(l_initial_index);
                return Err(CmpWireError::Left(err));
            } else if !r16.is_empty() {
                let err = UnpackError::trailing_junk_at(r_initial_index);
                return Err(CmpWireError::Right(err));
            }
        }
        Ok(order)
    })
}

/// Unpack a data from a `Reader` and present it directly to a `Presenter`.
pub fn present(
    cl: Class,
    ty: Ty,
    r: &mut Reader<'_>,
    p: &mut Presenter<'_, '_>,
) -> Result<(), PresentWireError> {
    if !ty.is_data() {
        return Err(PresentWireError::MetaTy);
    }
    if !cl.is_data() {
        return Err(PresentWireError::MetaClass);
    }
    if let Some(imp) = lookup_imp(cl, ty) {
        (imp.present)(r, p)
    } else {
        crate::datatypes::bytes::unpack::<&[u8]>(r, 0, 0xFFFF)
            .map_err(PresentWireError::Unpack)
            .and_then(|s| present_generic(s, p).map_err(PresentWireError::Present))
    }
}

#[doc = r#"
Present a byte slice in generic data presentation format.
```
use yardi::{ascii::{self, Present}, rdata};
#[derive(yardi::Present)]
struct CustomRdata<'a>{
    #[yardi(present = "rdata::present_generic")]
    data: &'a [u8],
}
let mut s = String::new();
let data = CustomRdata{data: &[0xC0, 0xFF, 0xEE]};
ascii::present::to_string(&data, &mut s).expect("present");
assert_eq!(r"\# 3 C0FFEE", &s);
```
"#]

pub fn present_generic(src: &[u8], p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    if src.len() > 0xFFFF {
        return Err(PresentError::value_large());
    }
    p.write("\\#")?;
    (src.len() as u16).present(p)?;
    if !src.is_empty() {
        crate::datatypes::bytes::present_hexws(src, p, src.len(), src.len())?;
    }
    Ok(())
}

/// Parse RDATA from a `Parser` and return it as bytes.
pub fn parse(cl: Class, ty: Ty, p: &mut Parser<'_, '_>) -> Result<Vec<u8>, ParseError> {
    if !ty.is_data() {
        return Err(ParseError::value_invalid());
    }
    if !cl.is_data() {
        return Err(ParseError::value_invalid());
    }
    if let Some(imp) = lookup_imp(cl, ty) {
        (imp.parse_bytes)(p)
    } else {
        parse_generic_impl(p)
            .and_then(|maybe_bytes| maybe_bytes.ok_or_else(ParseError::value_unknown))
    }
}

/// Parse type `T` in generic presentation form.
///
/// If the input doesn't start with "\\#" followed by a number it's assumed to
/// not be in generic format and the parser is restored to its initial state.
pub fn parse_known_generic<T>(p: &mut Parser<'_, '_>) -> Result<Option<T>, ParseError>
where
    T: for<'a> Unpack<'a>,
{
    let snapshot = p.snapshot();
    if let Some(bytes) = parse_generic_impl(p)? {
        let mut reader = Reader::new(&bytes);
        match T::unpack(&mut reader) {
            Ok(t) => Ok(Some(t)),
            Err(err) => {
                p.restore(&snapshot);
                p.pull(|s| {
                    debug_assert_eq!(s, "\\#");
                    Ok(Pull::Accept(()))
                })
                .and_then(|_| u16::parse(p).map(usize::from))
                .unwrap();
                let mut nibs = err
                    .index()
                    .and_then(|i| i.checked_mul(2))
                    .unwrap_or_default();
                let mut token_parse_err = None;
                while token_parse_err.is_none() {
                    token_parse_err = p
                        .pull(|s| {
                            if s.len() > nibs {
                                Err(ParseError::value_private1())
                            } else {
                                nibs -= s.len();
                                Ok(())
                            }
                        })
                        .err();
                }
                let mut parse_err = match err.kind() {
                    UnpackErrorKind::Short => ParseError::value_small(),
                    UnpackErrorKind::TrailingJunk => ParseError::trailing_junk(),
                    UnpackErrorKind::Value(ve) => ParseError::with_kind(ParseErrorKind::Value(ve)),
                };
                let maybe_pos = token_parse_err.unwrap().position().copied();
                if let Some(mut pos) = maybe_pos {
                    pos.col = pos.col.saturating_add(nibs as u32);
                    pos.byte = pos.byte.saturating_add(nibs as u64);
                    parse_err = parse_err.set_position(pos);
                }
                if let Some(subject) = err.subject() {
                    parse_err = parse_err.set_subject(subject)
                }
                Err(parse_err)
            }
        }
    } else {
        p.restore(&snapshot);
        Ok(None)
    }
}

fn parse_generic_impl(p: &mut Parser<'_, '_>) -> Result<Option<Vec<u8>>, ParseError> {
    let preamble_result = p
        .pull(|s| {
            if s == "\\#" {
                Pull::Accept(())
            } else {
                Pull::Reject(ParseError::value_invalid())
            }
        })
        .and_then(|_| u16::parse(p).map(usize::from));
    if let Ok(len) = preamble_result {
        // TODO: should this be returned as octets?
        crate::datatypes::bytes::parse_hexws(p, len, len).map(Some)
    } else {
        Ok(None)
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Impl<T>
where
    T: StaticRdata + Present + Parse,
{
    marker: PhantomData<T>,
}

impl<T> Default for Impl<T>
where
    T: StaticRdata + Present + Parse,
{
    fn default() -> Self {
        Self {
            marker: PhantomData,
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct ImplGeneric {
    class: Class,
    ty: Ty,
}

impl ImplGeneric {
    pub fn new(class: Class, ty: Ty) -> Result<Self, ()> {
        if class.is_data() && ty.is_data() {
            Ok(ImplGeneric { class, ty })
        } else {
            Err(())
        }
    }
}

// TODO: should this be sealed?
pub trait Constructor {
    type Data: 'static + Pack + Present;
    fn unpack(&self, reader: &mut Reader<'_>) -> Result<Self::Data, UnpackError>;
    fn parse(&self, p: &mut Parser<'_, '_>) -> Result<Self::Data, ParseError>;
    // a user supplied T in Impl<T> may provide a non-data Class or Ty... can that be a problem?
    fn ty(&self) -> Ty;
    fn class(&self) -> Option<Class>;
}

impl<T> Constructor for Impl<T>
where
    T: 'static + Present + Parse + StaticRdata + for<'a> Unpack<'a> + Pack,
{
    type Data = T;
    fn unpack(&self, reader: &mut Reader<'_>) -> Result<Self::Data, UnpackError> {
        T::unpack(reader)
    }
    fn parse(&self, p: &mut Parser<'_, '_>) -> Result<Self::Data, ParseError> {
        <T as Parse>::parse(p)
    }
    fn ty(&self) -> Ty {
        T::TY
    }
    fn class(&self) -> Option<Class> {
        T::CLASS
    }
}

impl Constructor for ImplGeneric {
    type Data = Generic;
    fn unpack(&self, reader: &mut Reader<'_>) -> Result<Self::Data, UnpackError> {
        Generic::unpack(self.class, self.ty, reader)
    }
    fn parse(&self, p: &mut Parser<'_, '_>) -> Result<Self::Data, ParseError> {
        Generic::parse(self.class, self.ty, p)
    }
    fn ty(&self) -> Ty {
        self.ty
    }
    // TODO: this is a bit off - we need a class to provide to unpack and parse though
    fn class(&self) -> Option<Class> {
        Some(self.class)
    }
}

pub trait AssocConstructor: Sized + Pack + Present {
    type Impl: Constructor<Data = Self>;
    fn rdata_impl(&self) -> Self::Impl;
}

impl<T> AssocConstructor for T
where
    T: 'static + StaticRdata + Present + Parse + for<'a> Unpack<'a> + Pack,
{
    type Impl = Impl<T>;
    fn rdata_impl(&self) -> Self::Impl {
        Impl::default()
    }
}

impl AssocConstructor for Generic {
    type Impl = ImplGeneric;
    fn rdata_impl(&self) -> Self::Impl {
        ImplGeneric {
            class: self.class(),
            ty: self.ty(),
        }
    }
}

#[test]
fn constructor_test() {
    fn p<T: Constructor>(t: &T, s: &str) -> T::Data {
        if let crate::ascii::parse::Entry::Value { value, .. } = crate::ascii::parse::from_entry_f(
            s.as_bytes(),
            Default::default(),
            &Default::default(),
            true,
            |p| t.parse(p),
        )
        .unwrap()
        {
            value
        } else {
            panic!();
        }
    }
    let c_mx: Impl<Mx> = Impl::default();
    let uc_mx = ImplGeneric::new(Class::IN, Ty::MX).unwrap();
    let input: &str = "10 example.";
    let c_mx_d = p(&c_mx, input);
    assert_eq!(c_mx, c_mx_d.rdata_impl());
    let uc_mx_d = p(&uc_mx, input);
    assert_eq!(uc_mx, uc_mx_d.rdata_impl());
    // TODO: is this finished?
    //panic!("{:?}", (p(&c_mx, input), p(&uc_mx, input)));
}
