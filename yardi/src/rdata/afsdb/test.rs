/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
use crate::rdata::Afsdb;
#[test]
fn default() {
    <Afsdb>::default();
}
const CASE_SG00_WIRE: &'static [u8] = include_bytes!("sample_sg_afsdb_00.bin");
#[test]
fn wire_marshal_sg00() {
    crate::rdata::test::wire_marshal_data::<Afsdb>(CASE_SG00_WIRE);
}
#[test]
fn wire_cmp_sg00() {
    crate::rdata::test::wire_cmp_data::<Afsdb>(CASE_SG00_WIRE);
}
#[test]
fn wire_cmp_canonical_sg00() {
    crate::rdata::test::wire_cmp_canonical_data::<Afsdb>(CASE_SG00_WIRE);
}
const CASE_SG00_ASCII: &str = include_str!("sample_sg_afsdb_00.txt");
#[test]
fn ascii_roundtrip_sg00() {
    crate::rdata::test::ascii_roundtrip::<Afsdb>(CASE_SG00_ASCII);
}
#[test]
fn generic_ascii_roundtrip_sg00() {
    crate::rdata::test::generic_ascii_roundtrip::<Afsdb>(CASE_SG00_ASCII, CASE_SG00_WIRE);
}
#[test]
fn ascii_to_wire_sg00() {
    crate::rdata::test::ascii_to_wire::<Afsdb>(CASE_SG00_ASCII, CASE_SG00_WIRE);
}
#[test]
fn wire_to_ascii_sg00() {
    crate::rdata::test::wire_to_ascii::<Afsdb>(CASE_SG00_ASCII, CASE_SG00_WIRE);
}
/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
