#[cfg(test)]
mod test;

#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

use core::{
    cmp::Ordering,
    fmt::{self, Debug},
    str::FromStr,
};

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};
use crate::datatypes::Ty;
use crate::wire::{
    Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
};

/// An NSEC Type bitmap.
#[derive(Clone, Default)]
// TODO: make this a regular struct with an inner?
// TODO: max length is 8688 bytes, custom rep?
pub struct NsecTypeBitmap<T>(T);

impl NsecTypeBitmap<Vec<u8>> {
    /// Add a Type to this `NsecTypeBitmap`.
    // TODO: rename insert? <-- assume it'd be insert writing yr3
    // TODO: Error::Invalid?
    // TODO: supposed to accept but ignore non data types from the wire, so accept them here too?
    pub fn add(&mut self, ty: Ty) -> Result<(), ()> {
        if !ty.is_data() {
            return Err(());
        }
        let window = ty.0 / 256;
        let bit = ty.0 - (window * 256);
        let len = (bit / 8) + 1;
        assert!(len <= 32);
        let mask = 0b_1000_0000_u8 >> (bit % 8);
        let mut offset = 0;
        while offset < self.0.len() {
            assert!(self.0.len() < 0xFFFF);
            let cur_window = u16::from(self.0[offset]);
            let cur_len = self.0[offset + 1];
            match window.cmp(&cur_window) {
                Ordering::Equal if len as u8 > cur_len => {
                    let extend_by = len as usize - cur_len as usize;
                    let old_len = self.0.len();
                    // TODO: fallible alloc?
                    self.0.resize(old_len + extend_by, 0);
                    let insert_at = offset + 2 + cur_len as usize;
                    self.0
                        .copy_within(insert_at..old_len, insert_at + extend_by);
                    for b in self.0[insert_at..].iter_mut().take(extend_by) {
                        *b = 0;
                    }
                    self.0[offset + 1] = len as u8;
                    self.0[offset + 1 + len as usize] |= mask;
                    return Ok(());
                }
                Ordering::Equal => {
                    self.0[offset + 1 + len as usize] |= mask;
                    return Ok(());
                }
                Ordering::Less => {
                    // “Blocks are present in the NSEC3 RR RDATA in increasing numerical order.”
                    let extend_by = 2 + len as usize;
                    let old_len = self.0.len();
                    // TODO: fallible alloc
                    self.0.resize(old_len + extend_by, 0);
                    self.0.copy_within(offset..old_len, offset + extend_by);
                    for b in self.0[offset..].iter_mut().take(extend_by) {
                        *b = 0;
                    }
                    self.0[offset] = window as u8;
                    self.0[offset + 1] = len as u8;
                    self.0[offset + 1 + (len as usize)] |= mask;
                    return Ok(());
                }
                _ => {}
            }
            offset += 2 + cur_len as usize;
        }
        // TODO: fallible allocs
        self.0.push(window as u8);
        self.0.push(len as u8);
        self.0.resize(self.0.len() + len as usize, 0);
        self.0[offset + 1 + len as usize] = mask;
        Ok(())
    }
    /// Remove a Type from this `NsecTypeBitmap`.
    pub fn remove(&mut self, ty: Ty) {
        let window = ty.0 / 256;
        let bit = ty.0 - (window * 256);
        let len = (bit / 8) + 1;
        assert!(len <= 32);
        let mask = !(0b_1000_0000_u8 >> (bit % 8));
        let mut offset = 0;
        while offset < self.0.len() {
            assert!(self.0.len() < 0xFFFF);
            let cur_window = u16::from(self.0[offset]);
            let cur_len = self.0[offset + 1];
            match window.cmp(&cur_window) {
                Ordering::Equal if len as u8 > cur_len => {
                    return;
                }
                Ordering::Equal => {
                    self.0[offset + 1 + len as usize] &= mask;
                    if self.0[offset + 1 + len as usize] != 0 {
                        return;
                    }
                    let empty_window = self.0[offset + 2..offset + 1 + cur_len as usize]
                        .iter()
                        .all(|&b| b == 0);
                    if empty_window {
                        let remove = 2 + cur_len as usize;
                        let old_len = self.0.len();
                        self.0.copy_within(offset + remove..old_len, offset);
                        self.0.truncate(old_len - remove);
                    }
                    return;
                }
                Ordering::Less => return,
                _ => {}
            }
            offset += 2 + cur_len as usize;
        }
    }
}

impl<T: AsRef<[u8]>> NsecTypeBitmap<T> {
    /// Returns an iterator over the types in this `NsecTypeBitmap`.
    pub fn iter(&self) -> NsecTypeBitmapIter<'_> {
        NsecTypeBitmapIter::new(self.0.as_ref())
    }
}

impl<T: AsRef<[u8]>> Debug for NsecTypeBitmap<T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_list().entries(self.iter()).finish()
    }
}

impl<T: AsRef<[u8]>> Eq for NsecTypeBitmap<T> {}

impl<T: AsRef<[u8]>> PartialEq for NsecTypeBitmap<T> {
    fn eq(&self, other: &Self) -> bool {
        let mut a = self.iter();
        let mut b = other.iter();
        loop {
            match (a.next(), b.next()) {
                (None, None) => return true,
                (Some(a), Some(b)) if a == b => (),
                _ => return false,
            }
        }
    }
}

impl<T: AsRef<[u8]>> Ord for NsecTypeBitmap<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        let mut a = self.iter();
        let mut b = other.iter();
        loop {
            match (a.next(), b.next()) {
                (None, None) => return Ordering::Equal,
                (Some(a), Some(b)) => match a.cmp(&b) {
                    Ordering::Equal => (),
                    other => return other,
                },
                (None, Some(_)) => return Ordering::Less,
                (Some(_), None) => return Ordering::Greater,
            }
        }
    }
}

impl<T: AsRef<[u8]>> PartialOrd for NsecTypeBitmap<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(<Self as Ord>::cmp(self, other))
    }
}

impl<'a, T: AsRef<[u8]>> IntoIterator for &'a NsecTypeBitmap<T> {
    type Item = Ty;
    type IntoIter = NsecTypeBitmapIter<'a>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<T: for<'a> From<&'a [u8]>> Parse for NsecTypeBitmap<T> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        // TODO: build on stack, then allocate
        let mut bmp = <NsecTypeBitmap<Vec<u8>>>::default();
        p.pull_rest(|s| {
            let ty = Ty::from_str(s)?;
            bmp.add(ty).map_err(|_| ParseError::value_invalid())?;
            Ok(())
        })?;
        let bmp = Self(bmp.0[..].into());
        Ok(bmp)
    }
}

impl<T: AsRef<[u8]>> Present for NsecTypeBitmap<T> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        for ty in self {
            ty.present(p)?;
        }
        Ok(())
    }
}

impl<T: AsRef<[u8]>> Pack for NsecTypeBitmap<T> {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        dst.write_bytes(self.0.as_ref())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(self.0.as_ref().len())
    }
}

impl<'a, T: From<&'a [u8]>> Unpack<'a> for NsecTypeBitmap<T> {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        // TODO: RFC 3845 and RFC 4034 say a meta or qtype should be ignored upon reading?
        let data = unpack_bytes(reader).map(Into::into)?;
        Ok(NsecTypeBitmap(data))
    }
}

impl<T> UnpackedLen for NsecTypeBitmap<T> {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        unpack_bytes(reader).map(|s| s.len())
    }
}

impl<T: AsRef<[u8]>> WireOrd for NsecTypeBitmap<T> {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let l = unpack_bytes(left).map_err(WireOrdError::Left)?;
        let r = unpack_bytes(right).map_err(WireOrdError::Right)?;
        Ok(l.cmp(r))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        <[u8]>::cmp(self.0.as_ref(), other.0.as_ref())
    }
}

/// An iterator over the types in an `NsecTypeBitmap`.
#[derive(Debug)]
pub struct NsecTypeBitmapIter<'a> {
    bytes: &'a [u8],
    offset: usize,
    bit_offset: usize,
}

impl<'a> NsecTypeBitmapIter<'a> {
    fn new(bytes: &'a [u8]) -> Self {
        NsecTypeBitmapIter {
            bytes,
            offset: 0,
            bit_offset: 0,
        }
    }
}

impl<'a> Iterator for NsecTypeBitmapIter<'a> {
    type Item = Ty;
    fn next(&mut self) -> Option<Self::Item> {
        while self.offset < self.bytes.len() {
            let window = u16::from(self.bytes[self.offset]);
            let len = self.bytes[self.offset + 1] as usize;
            assert!(len > 0 && len <= 32);
            for i in self.bit_offset..(len * 8) {
                let byte_i = i / 8;
                let bit_i = i % 8;
                if self.bytes[self.offset + 2 + byte_i] & (0b_1000_0000_u8 >> bit_i) != 0 {
                    if i == (len * 8) - 1 {
                        self.bit_offset = 0;
                        self.offset += 2 + len;
                    } else {
                        self.bit_offset = i + 1
                    }
                    return Some(Ty((window * 256) + i as u16));
                }
            }
            self.bit_offset = 0;
            self.offset += 2 + len;
        }
        None
    }
}

fn unpack_bytes<'a>(src: &mut Reader<'a>) -> Result<&'a [u8], UnpackError> {
    let initial_index = src.index();
    let bytes = src.get_bytes_rest()?;
    let mut last_window = None;
    let mut temp = bytes;
    while !temp.is_empty() {
        let window = temp[0];
        if let Some(last) = last_window {
            if last >= window {
                return Err(UnpackError::value_invalid_at(initial_index));
            }
        }
        last_window = Some(window);
        let len = *temp
            .get(1)
            .ok_or_else(|| UnpackError::short_at(initial_index))? as usize;
        if len == 0 || len > 32 {
            return Err(UnpackError::value_invalid_at(initial_index));
        }
        let end = 1 + len;
        let last = temp
            .get(end)
            .ok_or_else(|| UnpackError::short_at(initial_index))?;
        if *last == 0 {
            return Err(UnpackError::value_invalid_at(initial_index));
        }
        let (block, rest) = temp.split_at(end + 1);
        if !NsecTypeBitmapIter::new(block).all(Ty::is_data) {
            return Err(UnpackError::value_invalid_at(initial_index));
        }
        temp = rest;
    }
    Ok(bytes)
}
