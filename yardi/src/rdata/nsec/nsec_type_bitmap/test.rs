#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

use crate::datatypes::Ty;
use crate::rdata::nsec::{Nsec, NsecTypeBitmap};
use crate::wire::{Reader, Unpack};

#[test]
fn add_remove() {
    let mut b = <NsecTypeBitmap<Vec<u8>>>::default();
    assert!(b.0.is_empty());
    assert!(b.add(Ty(0)).is_err());
    assert!(b.0.is_empty());
    let tys: &[Ty] = &[Ty::A, Ty::AAAA, Ty::NS, Ty::SRV, Ty(65280)];
    for &ty in tys {
        b.add(ty).expect("add");
    }
    let mut check: Vec<Ty> = tys.to_vec();
    check.sort();
    check.reverse();
    for ty in b.iter() {
        assert_eq!(ty, check.pop().unwrap());
    }
    assert!(check.is_empty());
    for &ty in tys {
        b.remove(ty);
    }
    assert!(b.0.is_empty());
}

#[test]
fn rfc_4034_example() {
    let input: &[u8] = &[
        0x04, b'h', b'o', b's', b't', 0x07, b'e', b'x', b'a', b'm', b'p', b'l', b'e', 0x03, b'c',
        b'o', b'm', 0x00, 0x00, 0x06, 0x40, 0x01, 0x00, 0x00, 0x00, 0x03, 0x04, 0x1b, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20,
    ];
    let mut r = Reader::new(input);
    let d: Nsec = Unpack::unpack(&mut r).expect("nsec");
    assert!(r.is_empty());
    assert_eq!(d.next_domain, name!("host.example.com."));
    assert_eq!(
        d.types.iter().collect::<Vec<Ty>>().as_slice(),
        &[Ty::A, Ty::MX, Ty::RRSIG, Ty::NSEC, Ty(1234)]
    );
}
