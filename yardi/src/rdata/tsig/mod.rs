/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
#![doc = "Support for TSIG (ANY) RDATA."]
#![allow(unused_qualifications)]
#[cfg(test)]
mod test;
#[doc = "TSIG (ANY) - Transaction Signature."]
#[doc = "\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc8945\">RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)</a>"]
#[derive(Clone, Default, Eq, Ord, Pack, PartialEq, PartialOrd, Unpack, UnpackedLen, WireOrd)]
#[yardi(crate = "crate", error_subject = "TSIG")]
pub struct Tsig<M = alloc::boxed::Box<[u8]>, O = alloc::boxed::Box<[u8]>> {
    #[doc = "Algorithm Name: Name of the algorithm."]
    #[yardi(error_subject = "TSIG Algorithm Name")]
    pub alg_name: crate::datatypes::Name,
    #[doc = "Time Signed: Seconds since 1 January UTC."]
    #[yardi(error_subject = "TSIG Time Signed")]
    pub time_signed: crate::rdata::tsig::TsigTime,
    #[doc = "Fudge: Seconds of error permitted in Time Signed."]
    #[yardi(error_subject = "TSIG Fudge")]
    pub fudge: u16,
    #[doc = "MAC: MAC."]
    #[yardi(
        pack_bound = "M: AsRef<[u8]>",
        pack = "| m , w | crate :: datatypes :: bytes :: pack16 (m , w , 0usize , 65535usize)",
        packed_len = "| m | crate :: datatypes :: bytes :: packed16_len (m , 0usize , 65535usize)",
        unpack_bound = "M: From<&'yr [u8]>",
        unpack = "| r | crate :: datatypes :: bytes :: unpack16 (r , 0usize , 65535usize)",
        unpacked_len = "| r | crate :: datatypes :: bytes :: unpacked16_len (r , 0usize , 65535usize)",
        cmp_bound = "M: AsRef<[u8]>",
        cmp_wire = "| l , r | crate :: datatypes :: bytes :: cmp16 (l , r , 0usize , 65535usize)",
        cmp_wire_canonical = "| l , r | crate :: datatypes :: bytes :: cmp16 (l , r , 0usize , 65535usize)",
        cmp_as_wire = "| l , r | { let l = l . as_ref () ; let r = r . as_ref () ; let l_len = (l . len () as u16) . to_be_bytes () ; let r_len = (r . len () as u16) . to_be_bytes () ; match l_len . cmp (& r_len) { core :: cmp :: Ordering :: Equal => < [u8] > :: cmp (l , r) , other => other , } }",
        cmp_as_wire_canonical = "| l , r | { let l = l . as_ref () ; let r = r . as_ref () ; let l_len = (l . len () as u16) . to_be_bytes () ; let r_len = (r . len () as u16) . to_be_bytes () ; match l_len . cmp (& r_len) { core :: cmp :: Ordering :: Equal => < [u8] > :: cmp (l , r) , other => other , } }"
    )]
    #[yardi(error_subject = "TSIG MAC")]
    pub mac: M,
    #[doc = "Original ID: Original message ID."]
    #[yardi(error_subject = "TSIG Original ID")]
    pub original_id: u16,
    #[doc = "Error: Expanded RCODE covering TSIG processing."]
    #[yardi(error_subject = "TSIG Error")]
    pub error: crate::rdata::tsig::TsigRcode,
    #[doc = "Other: Other."]
    #[yardi(
        pack_bound = "O: AsRef<[u8]>",
        pack = "| m , w | crate :: datatypes :: bytes :: pack16 (m , w , 0usize , 65535usize)",
        packed_len = "| m | crate :: datatypes :: bytes :: packed16_len (m , 0usize , 65535usize)",
        unpack_bound = "O: From<&'yr [u8]>",
        unpack = "| r | crate :: datatypes :: bytes :: unpack16 (r , 0usize , 65535usize)",
        unpacked_len = "| r | crate :: datatypes :: bytes :: unpacked16_len (r , 0usize , 65535usize)",
        cmp_bound = "O: AsRef<[u8]>",
        cmp_wire = "| l , r | crate :: datatypes :: bytes :: cmp16 (l , r , 0usize , 65535usize)",
        cmp_wire_canonical = "| l , r | crate :: datatypes :: bytes :: cmp16 (l , r , 0usize , 65535usize)",
        cmp_as_wire = "| l , r | { let l = l . as_ref () ; let r = r . as_ref () ; let l_len = (l . len () as u16) . to_be_bytes () ; let r_len = (r . len () as u16) . to_be_bytes () ; match l_len . cmp (& r_len) { core :: cmp :: Ordering :: Equal => < [u8] > :: cmp (l , r) , other => other , } }",
        cmp_as_wire_canonical = "| l , r | { let l = l . as_ref () ; let r = r . as_ref () ; let l_len = (l . len () as u16) . to_be_bytes () ; let r_len = (r . len () as u16) . to_be_bytes () ; match l_len . cmp (& r_len) { core :: cmp :: Ordering :: Equal => < [u8] > :: cmp (l , r) , other => other , } }"
    )]
    #[yardi(error_subject = "TSIG Other")]
    pub other: O,
}
impl<M, O> core::fmt::Debug for Tsig<M, O>
where
    M: AsRef<[u8]>,
    O: AsRef<[u8]>,
{
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Tsig")
            .field("alg_name", &self.alg_name)
            .field("time_signed", &self.time_signed)
            .field("fudge", &self.fudge)
            .field("mac", &crate::datatypes::bytes::DebugFmt(self.mac.as_ref()))
            .field("original_id", &self.original_id)
            .field("error", &self.error)
            .field(
                "other",
                &crate::datatypes::bytes::DebugFmt(self.other.as_ref()),
            )
            .finish()
    }
}
use crate::wire::{
    Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
};
use core::cmp::Ordering;
#[cfg(feature = "std")]
use std::time::SystemTime;
#[doc = r" TSIG Time."]
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct TsigTime(pub(crate) u64);
impl TsigTime {
    const U48_MAX: u64 = 0xFFFF_FFFF_FFFF;
    #[doc = r" Return the 48-bit time as a `u64`."]
    pub const fn as_u64(self) -> u64 {
        self.0
    }
    #[doc = r" Return a `TsigTime` representing the current time."]
    #[cfg(feature = "std")]
    pub fn now() -> Self {
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        TsigTime(now % Self::U48_MAX)
    }
    #[doc = r" Create a `TsigTime` from a `u64`."]
    pub const fn from_u64(n: u64) -> TsigTime {
        TsigTime(n % Self::U48_MAX)
    }
    #[doc = r" Returns true if this `TsigTime` is within `fudge` seconds of `other`."]
    pub fn within_fudge(self, other: Self, fudge: u16) -> bool {
        let (s, g) = match self.0.cmp(&other.0) {
            Ordering::Equal => return true,
            Ordering::Less => (self.0, other.0),
            Ordering::Greater => (other.0, self.0),
        };
        g.checked_sub(s)
            .map(|d| {
                if d > u64::from(u16::MAX) {
                    false
                } else {
                    (d as u16) < fudge
                }
            })
            .unwrap_or(false)
    }
}
impl WireOrd for TsigTime {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let l = <[u8; 6]>::unpack(left).map_err(WireOrdError::Left)?;
        let r = <[u8; 6]>::unpack(right).map_err(WireOrdError::Right)?;
        Ok(l.cmp(&r))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        self.0.to_be_bytes()[2..].cmp(&other.0.to_be_bytes()[2..])
    }
}
impl UnpackedLen for TsigTime {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        <[u8; 6]>::unpacked_len(reader)
    }
}
impl Pack for TsigTime {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        dst.write_bytes(&self.0.to_be_bytes()[2..])
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(6)
    }
}
impl<'a> Unpack<'a> for TsigTime {
    fn unpack(src: &mut Reader<'a>) -> Result<Self, UnpackError> {
        src.get_bytes_map(6, |s| {
            Ok(TsigTime(
                u64::from(s[0]) << 40
                    | u64::from(s[1]) << 32
                    | u64::from(s[2]) << 24
                    | u64::from(s[3]) << 16
                    | u64::from(s[4]) << 8
                    | u64::from(s[5]),
            ))
        })
    }
}
#[doc = r" TSIG RCODE."]
#[derive(
    Clone, Copy, Default, Hash, Eq, PartialEq, PartialOrd, Ord, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
pub struct TsigRcode(pub u16);
impl core::fmt::Debug for TsigRcode {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        for &(rc, m) in Self::MNEMONICS {
            if rc == *self {
                return write!(f, "TsigRcode({})", m);
            }
        }
        write!(f, "TsigRcode({})", self.0)
    }
}
impl TsigRcode {
    #[doc = "NoError - No Error.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
    pub const NOERROR: TsigRcode = TsigRcode(0);
    #[doc = "FormErr - Format Error.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
    pub const FORMERR: TsigRcode = TsigRcode(1);
    #[doc = "ServFail - Server Failure.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
    pub const SERVFAIL: TsigRcode = TsigRcode(2);
    #[doc = "NXDomain - Non-Existent Domain.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
    pub const NXDOMAIN: TsigRcode = TsigRcode(3);
    #[doc = "NotImp - Not Implemented.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
    pub const NOTIMP: TsigRcode = TsigRcode(4);
    #[doc = "Refused - Query Refused.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
    pub const REFUSED: TsigRcode = TsigRcode(5);
    #[doc = "YXDomain - Name Exists when it should not.\n\n"]
    #[doc = "References:"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc2136\">RFC 2136: Dynamic Updates in the Domain Name System (DNS UPDATE)</a>"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc6672\">RFC 6672: DNAME Redirection in the DNS</a>"]
    pub const YXDOMAIN: TsigRcode = TsigRcode(6);
    #[doc = "YXRRSet - RR Set Exists when it should not.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc2136\">RFC 2136: Dynamic Updates in the Domain Name System (DNS UPDATE)</a>"]
    pub const YXRRSET: TsigRcode = TsigRcode(7);
    #[doc = "NXRRSet - RR Set that should exist does not.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc2136\">RFC 2136: Dynamic Updates in the Domain Name System (DNS UPDATE)</a>"]
    pub const NXRRSET: TsigRcode = TsigRcode(8);
    #[doc = "NotAuth - Server Not Authoritative for zone or Not Authorized. (RCODE assigned twice)\n\n"]
    #[doc = "References:"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc2136\">RFC 2136: Dynamic Updates in the Domain Name System (DNS UPDATE)</a>"]
    #[doc = " * <a href=\"https://tools.ietf.org/html/rfc8945\">RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)</a>"]
    pub const NOTAUTH: TsigRcode = TsigRcode(9);
    #[doc = "NotZone - Name not contained in zone.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc2136\">RFC 2136: Dynamic Updates in the Domain Name System (DNS UPDATE)</a>"]
    pub const NOTZONE: TsigRcode = TsigRcode(10);
    #[doc = "DSOTYPENI - DSO-TYPE Not Implemented.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc8490\">RFC 8490: DNS Stateful Operations</a>"]
    pub const DSOTYPENI: TsigRcode = TsigRcode(11);
    #[doc = "BADSIG - TSIG Signature Failure.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc8945\">RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)</a>"]
    pub const BADSIG: TsigRcode = TsigRcode(16);
    #[doc = "BADKEY - Key not recognized.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc8945\">RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)</a>"]
    pub const BADKEY: TsigRcode = TsigRcode(17);
    #[doc = "BADTIME - Signature out of time window.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc8945\">RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)</a>"]
    pub const BADTIME: TsigRcode = TsigRcode(18);
    #[doc = "BADMODE - Bad TKEY Mode.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc2930\">RFC 2930: Secret Key Establishment for DNS (TKEY RR)</a>"]
    pub const BADMODE: TsigRcode = TsigRcode(19);
    #[doc = "BADNAME - Duplicate key name.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc2930\">RFC 2930: Secret Key Establishment for DNS (TKEY RR)</a>"]
    pub const BADNAME: TsigRcode = TsigRcode(20);
    #[doc = "BADALG - Algorithm not supported.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc2930\">RFC 2930: Secret Key Establishment for DNS (TKEY RR)</a>"]
    pub const BADALG: TsigRcode = TsigRcode(21);
    #[doc = "BADTRUNC - Bad Truncation.\n\n"]
    #[doc = "Reference: "]
    #[doc = "<a href=\"https://tools.ietf.org/html/rfc8945\">RFC 8945: Secret Key Transaction Authentication for DNS (TSIG)</a>"]
    pub const BADTRUNC: TsigRcode = TsigRcode(22);
}
impl TsigRcode {
    const MNEMONICS: &'static [(TsigRcode, &'static str)] = &[
        (TsigRcode::NOERROR, "NoError"),
        (TsigRcode::FORMERR, "FormErr"),
        (TsigRcode::SERVFAIL, "ServFail"),
        (TsigRcode::NXDOMAIN, "NXDomain"),
        (TsigRcode::NOTIMP, "NotImp"),
        (TsigRcode::REFUSED, "Refused"),
        (TsigRcode::YXDOMAIN, "YXDomain"),
        (TsigRcode::YXRRSET, "YXRRSet"),
        (TsigRcode::NXRRSET, "NXRRSet"),
        (TsigRcode::NOTAUTH, "NotAuth"),
        (TsigRcode::NOTZONE, "NotZone"),
        (TsigRcode::DSOTYPENI, "DSOTYPENI"),
        (TsigRcode::BADSIG, "BADSIG"),
        (TsigRcode::BADKEY, "BADKEY"),
        (TsigRcode::BADTIME, "BADTIME"),
        (TsigRcode::BADMODE, "BADMODE"),
        (TsigRcode::BADNAME, "BADNAME"),
        (TsigRcode::BADALG, "BADALG"),
        (TsigRcode::BADTRUNC, "BADTRUNC"),
    ];
}
/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
