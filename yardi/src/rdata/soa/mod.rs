/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
#![doc = "Support for SOA RDATA."]
#![allow(unused_qualifications)]
#[cfg(test)]
mod test;
#[doc = "SOA - marks the start of a zone of authority."]
#[doc = "\n\n"]
#[doc = "Reference: "]
#[doc = "<a href=\"https://tools.ietf.org/html/rfc1035\">RFC 1035: Domain names - implementation and specification</a>"]
#[derive(
    Clone,
    Default,
    Eq,
    Ord,
    Pack,
    Parse,
    PartialEq,
    PartialOrd,
    Present,
    Rdata,
    Unpack,
    UnpackedLen,
    WireOrd,
)]
#[yardi(
    crate = "crate",
    rr_ty = "crate::datatypes::Ty::SOA",
    rr_skip_len_check,
    error_subject = "SOA"
)]
pub struct Soa {
    #[doc = "MNAME: Primary name server for this zone."]
    #[yardi(error_subject = "SOA MNAME")]
    pub mname: crate::datatypes::Name,
    #[doc = "RNAME: Mailbox of person responsible for this zone."]
    #[yardi(error_subject = "SOA RNAME")]
    pub rname: crate::datatypes::Name,
    #[doc = "Serial: Zone serial number."]
    #[yardi(error_subject = "SOA Serial")]
    pub serial: u32,
    #[doc = "Refresh: Time interval before the zone should be refreshed by secondary servers."]
    #[yardi(error_subject = "SOA Refresh")]
    pub refresh: crate::datatypes::Ttl,
    #[doc = "Retry: Time interval before a failed refresh should be retried by secondary servers."]
    #[yardi(error_subject = "SOA Retry")]
    pub retry: crate::datatypes::Ttl,
    #[doc = "Expire: Time interval that can elapse before secondary servers are no longer authoritative."]
    #[yardi(error_subject = "SOA Expire")]
    pub expire: crate::datatypes::Ttl,
    #[doc = "Minimum: TTL for negative responses."]
    #[yardi(error_subject = "SOA Minimum")]
    pub minimum: crate::datatypes::Ttl,
}
impl core::fmt::Debug for Soa {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.debug_struct("Soa")
            .field("mname", &self.mname)
            .field("rname", &self.rname)
            .field("serial", &self.serial)
            .field("refresh", &self.refresh)
            .field("retry", &self.retry)
            .field("expire", &self.expire)
            .field("minimum", &self.minimum)
            .finish()
    }
}
impl core::str::FromStr for Soa {
    type Err = crate::ascii::parse::ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::ascii::parse::from_single_entry(
            s.as_bytes(),
            crate::ascii::Context::shared_default(),
        )
    }
}
/**************************************************************************
* DO NOT EDIT THIS FILE. YOUR CHANGES WILL BE LOST WHEN IT IS REGENERATED! *
**************************************************************************/
