use crate::{
    ascii::{present, ParseError, Present, PresentError, Presenter},
    datatypes::{Class, Ty},
    rdata::{self, Generic, Rdata, Srv, StaticRdata},
    wire::{Format, Pack, Reader, Unpack, UnpackedLen, WireOrd, Writer},
};
#[cfg(not(feature = "std"))]
use alloc::string::String;
use core::{
    cmp::Ordering,
    fmt::{Debug, Write},
    str::FromStr,
};

pub fn bin_to_generic(s: &[u8]) -> String {
    let mut out = String::with_capacity(7 + (s.len() * 2));
    write!(out, "\\# {} ", s.len()).unwrap();
    for &b in s {
        write!(out, "{:02x}", b).unwrap();
    }
    out
}

pub fn ascii_roundtrip<T>(ascii: &str)
where
    T: Debug + FromStr<Err = ParseError> + Rdata + Present,
{
    let ty_struct = T::from_str(ascii).expect("ty_struct");
    let mut actual = String::with_capacity(ascii.len());
    present::to_string(&ty_struct, &mut actual).expect("present");
    assert_eq!(actual.as_str(), ascii);
}

pub fn generic_ascii_roundtrip<T>(ascii: &str, wire: &[u8])
where
    T: Debug + FromStr<Err = ParseError> + Rdata + Present,
{
    let generic_wire = bin_to_generic(wire);
    let ty_struct = T::from_str(&generic_wire).expect("ty struct");
    let mut actual = String::with_capacity(ascii.len());
    present::to_string(&ty_struct, &mut actual).expect("present");
    assert_eq!(actual.as_str(), ascii);
}

pub fn ascii_to_wire<T>(ascii: &str, wire: &[u8])
where
    T: Debug + FromStr<Err = ParseError> + Rdata + Pack,
{
    let ty_struct = T::from_str(ascii).expect("ty_struct");
    let mut buf = vec![0; wire.len()];
    let mut writer = Writer::new(&mut buf[..]);
    writer.set_format(Format::Plain);
    ty_struct.pack(&mut writer).expect("pack");
    assert_eq!(0, writer.remaining());
    let actual = writer.written();
    assert_eq!(actual, wire);
}

pub fn wire_to_ascii<T>(ascii: &str, wire: &[u8])
where
    T: Debug + Rdata + for<'a> Unpack<'a> + Present,
{
    let mut reader = Reader::new(wire);
    let ty_struct = T::unpack(&mut reader).expect("unpack");
    assert!(reader.is_empty());
    let mut actual = String::with_capacity(ascii.len());
    present::to_string(&ty_struct, &mut actual).expect("present");
    assert_eq!(actual.as_str(), ascii);
}

pub fn wire_marshal<T>(wire: &[u8])
where
    T: Debug + PartialEq + Pack + for<'a> Unpack<'a> + UnpackedLen,
{
    let ty_struct = {
        let mut r = Reader::new(wire);
        let t = T::unpack(&mut r).expect("unpack");
        assert!(r.is_empty());
        t
    };
    {
        let mut buf = vec![0; wire.len()];
        let mut w = Writer::new(&mut buf[..]);
        w.set_format(Format::Plain);
        ty_struct.pack(&mut w).expect("pack");
        assert_eq!(w.remaining(), 0);
        assert_eq!(w.written(), wire);
    }
    let unpacked_len = T::unpacked_len(&mut Reader::new(wire)).expect("unpacked len");
    assert_eq!(wire.len(), unpacked_len);
}

pub fn wire_marshal_data<T>(wire: &[u8])
where
    T: Debug + PartialEq + StaticRdata + Pack + for<'a> Unpack<'a> + UnpackedLen,
{
    wire_marshal::<T>(wire);
    let cl = T::CLASS.unwrap_or(Class::IN);
    let mut buf = vec![0; wire.len()];
    let mut w = Writer::new(&mut buf[..]);
    w.set_format(Format::Plain);
    let r = &mut Reader::new(wire);
    rdata::copy(cl, T::TY, r, &mut w).expect("copy");
    assert!(r.is_empty());
    assert_eq!(w.remaining(), 0);
    assert_eq!(w.written(), wire);
}

pub fn wire_cmp<T>(wire: &[u8])
where
    T: WireOrd,
{
    let mut a = Reader::new(wire);
    let mut b = Reader::new(wire);
    assert_eq!(Ordering::Equal, T::cmp_wire(&mut a, &mut b).unwrap());
}

pub fn wire_cmp_data<T>(wire: &[u8])
where
    T: StaticRdata + WireOrd,
{
    let mut a = Reader::new(wire);
    let mut b = Reader::new(wire);
    assert_eq!(Ordering::Equal, T::cmp_wire(&mut a, &mut b).unwrap());
    a.set_index(0);
    b.set_index(0);
    let cl = T::CLASS.unwrap_or(Class::IN);
    assert_eq!(
        Ordering::Equal,
        rdata::cmp(cl, T::TY, &mut a, &mut b).unwrap()
    );
}

pub fn wire_cmp_canonical<T>(wire: &[u8])
where
    T: WireOrd,
{
    let mut a = Reader::new(wire);
    let mut b = Reader::new(wire);
    assert_eq!(
        Ordering::Equal,
        T::cmp_wire_canonical(&mut a, &mut b).unwrap()
    );
}

pub fn wire_cmp_canonical_data<T>(wire: &[u8])
where
    T: StaticRdata + WireOrd,
{
    let mut a = Reader::new(wire);
    let mut b = Reader::new(wire);
    assert_eq!(
        Ordering::Equal,
        T::cmp_wire_canonical(&mut a, &mut b).unwrap()
    );
    a.set_index(0);
    b.set_index(0);
    let cl = T::CLASS.unwrap_or(Class::IN);
    assert_eq!(
        Ordering::Equal,
        rdata::cmp_canonical(cl, T::TY, &mut a, &mut b).unwrap()
    );
}

#[test]
fn txt_generic_valid() {
    let octets = <Generic>::from_str(Class::IN, Ty::TXT, "\\# 1 00")
        .map(Generic::into_bytes)
        .unwrap();
    assert_eq!(&octets[..], &[0]);
}

#[test]
fn txt_generic_short_rdlen() {
    assert!(<Generic>::from_str(Class::IN, Ty::TXT, "\\# 2 00").is_err());
}

#[test]
fn txt_generic_short_charstr() {
    assert!(<Generic>::from_str(Class::IN, Ty::TXT, "\\# 2 03 61").is_err());
}

#[test]
fn txt_generic_start_but_not_rdlen() {
    let octets = <Generic>::from_str(Class::IN, Ty::TXT, "\\# A 00")
        .map(Generic::into_bytes)
        .unwrap();
    assert_eq!(&octets[..], &[1, b'#', 1, b'A', 2, b'0', b'0']);
}

#[test]
fn txt_generic_start_only() {
    let octets = <Generic>::from_str(Class::IN, Ty::TXT, "\\#")
        .map(Generic::into_bytes)
        .unwrap();
    assert_eq!(&octets[..], &[1, b'#']);
}

struct GenericHelper<'a> {
    data: &'a [u8],
}
impl<'a> Present for GenericHelper<'a> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        rdata::present_generic(self.data, p)
    }
}

#[test]
fn txt_generic_present() {
    let mut out = String::new();
    present::to_string(
        &GenericHelper {
            data: b"\x03abc\x03123",
        },
        &mut out,
    )
    .expect("present");
    let expect = "\\# 8 0361626303313233";
    assert_eq!(out, expect);
}

#[test]
fn generic_no_data() {
    let empty_generic = "\\# 0";
    let octets = <Generic>::from_str(Class::IN, Ty::NULL, empty_generic)
        .map(Generic::into_bytes)
        .unwrap();
    assert!(octets.is_empty());
    let mut out = String::new();
    present::to_string(&GenericHelper { data: b"" }, &mut out).expect("present");
    assert_eq!(out, empty_generic);
}

#[test]
fn srv_from_str() {
    let expect = Srv {
        priority: 0,
        weight: 0,
        port: 80,
        target: name!("example.com."),
    };
    let output = Srv::from_str("0 0 80 example.com.").expect("parse srv");
    assert_eq!(expect, output);
}

// TODO: eh?
#[cfg(feature = "false")]
#[test]
fn fuzz() {
    //let input = include_bytes!("id:000000,sig:06,src:001433,op:int32,pos:2,val:+4096");
    //let input = include_bytes!("id:000001,sig:06,src:000377+001429,op:splice,rep:16");
    //let input = include_bytes!("id:000002,sig:06,src:000077+001432,op:splice,rep:4");
    //let input = include_bytes!("id:000003,sig:06,src:000124+001140,op:splice,rep:4");
    let input = include_bytes!("id:000004,sig:06,src:000449+001145,op:splice,rep:8");
    let mut r = Reader::new(input);
    let (cl, ty): (Class, Ty) = match Unpack::unpack(&mut r) {
        Ok(tup) => tup,
        Err(_) => return,
    };
    let rdata_bytes = match r.get_bytes_rest() {
        Ok(bytes) => bytes,
        Err(_) => return,
    };
    let mut temp_bytes = vec![0u8; 0xFFFF];
    let mut temp_string = String::with_capacity(0xFFFF);
    let r = Reader::new(rdata_bytes);
    let unpacked = match rdata::unpacked_len(cl, ty, &mut r.clone()) {
        Ok(rdlen) => {
            assert!(
                cl.is_data() && ty.is_data(),
                "unpacked_len succeded for non data ty or cl: {}/{}\n{:?}",
                ty,
                cl,
                rdata_bytes
            );
            assert!(rdlen <= 0xFFFF, "rdlen > 0xFFFF");
            let mut w = Writer::new(&mut temp_bytes[..rdlen]);
            if let Err(err) = rdata::copy(cl, ty, &mut r.clone(), &mut w) {
                panic!(
                    "copy failed after unpacked_len succeeded for {}/{}: {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            true
        }
        Err(_) => {
            let mut w = Writer::new(&mut temp_bytes[..]);
            if rdata::copy(cl, ty, &mut r.clone(), &mut w).is_ok() {
                panic!(
                    "copy succeeded after unpacked_len failed for {}/{}\n{:?}",
                    cl, ty, rdata_bytes
                );
            }
            false
        }
    };
    match Generic::unpack(cl, ty, &mut r.clone()) {
        Ok(ref rdata) if unpacked => {
            let mut w = Writer::new(&mut temp_bytes[..]);
            if let Err(err) = rdata.pack(&mut w) {
                panic!(
                    "Generic pack failed for {}/{} {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            if let Err(err) = crate::ascii::present::to_string(&rdata, &mut temp_string) {
                panic!(
                    "Failed to present for {}/{} {:?}\n{:?}",
                    cl, ty, err, rdata_bytes
                );
            }
            match Generic::from_str(cl, ty, &temp_string) {
                Ok(rdata2) => {
                    assert_eq!(
                        *rdata, rdata2,
                        "Parse/present mismatch for {}/{}\n{:?}",
                        cl, ty, rdata_bytes
                    );
                }
                Err(err) => {
                    panic!(
                        "Failed to parse what was presented for {}/{}: {:?}\n{:?}",
                        cl, ty, err, rdata_bytes
                    );
                }
            }
        }
        Ok(_) => {
            panic!(
                "Generic::unpack succeeded when it shouldn't have {}/{}\n{:?}",
                cl, ty, rdata_bytes
            );
        }
        Err(ref err) if unpacked => {
            panic!(
                "Generic::unpack failed when it shouldn't have {}/{}: {:?}\n{:?}",
                cl, ty, err, rdata_bytes
            );
        }
        Err(_) => (),
    }
    panic!("{}/{}", cl, ty);
}
