#![cfg(feature = "std")]

use std::sync::{Arc, Mutex};

use super::{MatchDown, NameTree, Node};
use crate::datatypes::{name, Name};

#[test]
fn remove() {
    let names: &[Name] = &[
        name!(r"example."),
        name!(r"b.a.example."),
        name!(r"x.c.b.a.example."),
        name!(r"d.c.b.a.example."),
        name!(r"x.d.c.b.a.example."),
        name!(r"d.\000.b.a.example."),
        name!(r"d.\255.b.a.example."),
    ];
    let mut last: Option<&Name> = None;
    let mut a: NameTree<&Name> = NameTree::new(name!("example."));
    for n in names.iter() {
        let before = format!("{:#?}", a);
        let _ = a.insert(n, n).unwrap();
        let inserted = a.get(n);
        if inserted != Ok(Some(&n)) {
            panic!(
                "lost current value - expect: {:?} actual: {:?} tree\n{:#?}\nbefore:\n{}",
                Some(&n),
                inserted,
                a,
                before
            );
        }
        if let Some(last) = last {
            let expect = Ok(Some(&last));
            let actual = a.get(last);
            if actual != expect {
                panic!(
                    "lost previous value - expect: {:?} actual: {:?} tree:\n{:#?}\nbefore:\n{}",
                    expect, actual, a, before
                );
            }
        }
        last = Some(n);
    }
    for name in names {
        assert_eq!(a.get(name), Ok(Some(&name)));
    }
    for name in names.iter().rev() {
        assert_eq!(a.get(name), Ok(Some(&name)));
        a.remove(name);
        assert_eq!(a.get(name), Ok(None));
    }
}

#[test]
fn root() {
    let root = Name::new();
    let names: &[Name] = &[name!("example."), name!("invalid.")];
    let mut a: NameTree<&Name> = NameTree::new(&root);
    let mut last: Option<&Name> = None;
    for n in names.iter() {
        let before = format!("{:#?}", a);
        let _ = a.insert(n, n).unwrap();
        let inserted = a.get(n);
        if inserted != Ok(Some(&n)) {
            panic!(
                "lost current value - expect: {:?} actual: {:?} tree\n{:#?}\nbefore:\n{}",
                Some(&n),
                inserted,
                a,
                before
            );
        }
        if let Some(last) = last {
            let expect = Ok(Some(&last));
            let actual = a.get(last);
            if actual != expect {
                panic!(
                    "lost previous value - expect: {:?} actual: {:?} tree:\n{:#?}\nbefore:\n{}",
                    expect, actual, a, before
                );
            }
        }
        last = Some(n);
    }
    for name in names {
        assert_eq!(a.get(name), Ok(Some(&name)));
    }
    for name in names.iter().rev() {
        assert_eq!(a.get(name), Ok(Some(&name)));
        a.remove(name);
        assert_eq!(a.get(name), Ok(None));
    }
}

#[test]
fn rfc5155_mut() {
    let names: &[Name] = &[
        name!("example."),
        name!("a.example."),
        name!("ai.example."),
        name!("ns1.example."),
        name!("ns2.example."),
        name!("w.example."),
        name!("*.w.example."),
        name!("x.w.example."),
        name!("y.w.example."),
        name!("x.y.w.example."),
        name!("xx.example."),
    ];
    let mut nt: NameTree<usize> = NameTree::new(name!("example."));
    for n in names {
        nt.insert(n, 0).expect("insert name");
    }
    for n in names {
        assert_eq!(nt.get(n), Ok(Some(&0)));
    }
    let mut name_chk_iter = names.iter();
    for node in nt.iter_mut() {
        if let Node::Occupied(entry) = node {
            let (name, value) = entry.name_and_value_mut();
            assert_eq!(Some(name), name_chk_iter.next());
            *value += 1;
        } else {
            panic!("{}: no value!?", node.name());
        }
    }
    for n in names {
        assert_eq!(nt.get(n), Ok(Some(&1)), "match_down {}", n);
    }
    let mut name_chk_iter = names.iter();
    for (name, value) in nt.iter() {
        assert_eq!(Some(name), name_chk_iter.next());
        if let Some(value) = value {
            assert_eq!(value, &1);
        } else {
            panic!("{}: no value!?", name);
        }
    }
    {
        let filtered_names: &[Name] = &[
            name!("example."),
            name!("a.example."),
            name!("ai.example."),
            name!("ns1.example."),
            name!("ns2.example."),
            name!("w.example."),
            name!("xx.example."),
        ];
        let skip_name = name!("w.example.");
        {
            let mut iter = nt.iter_mut();
            let mut expect_iter = filtered_names.iter();
            while let Some(node) = iter.next() {
                let name = node.name();
                assert_eq!(Some(name), expect_iter.next());
                if name == skip_name {
                    iter.skip_below();
                }
            }
            assert!(expect_iter.next().is_none());
        }
        {
            let mut iter = nt.iter();
            let mut expect_iter = filtered_names.iter();
            while let Some((name, _)) = iter.next() {
                assert_eq!(Some(name), expect_iter.next());
                if name == skip_name {
                    iter.skip_below();
                }
            }
            assert!(expect_iter.next().is_none());
        }
    }
}

#[test]
fn descend() {
    let example_com = name!("example.com.");
    let http_tcp_example_com = name!("_http._tcp.example.com.");
    let mut nt = NameTree::new(example_com.clone());
    nt.insert(&example_com, 0usize).unwrap();
    nt.insert(&http_tcp_example_com, 1).unwrap();
    let expectations: &[(Name, usize)] = &[
        (name!("example.com."), 0),
        (name!("_http._tcp.example.com."), 1),
    ];
    {
        let mut expect_iter = expectations.iter().take(1).cloned();

        let mut descend_iter = nt.descend(&example_com).map(|(name, &u)| (name, u));
        loop {
            let expect: Option<(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual);
            if expect.is_none() {
                break;
            }
        }
    }
    {
        let mut expect_iter = expectations.iter().cloned();
        let mut descend_iter = nt
            .descend(&http_tcp_example_com)
            .map(|(name, &u)| (name, u));
        loop {
            let expect: Option<(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual);
            if expect.is_none() {
                break;
            }
        }
    }
}

#[test]
fn descend_mut() {
    let example_com = name!("example.com.");
    let http_tcp_example_com = name!("_http._tcp.example.com.");
    let mut nt = NameTree::new(example_com.clone());
    nt.insert(&example_com, 0usize).unwrap();
    nt.insert(&http_tcp_example_com, 1).unwrap();
    let expectations: &[(Name, usize)] =
        &[(example_com.clone(), 0), (http_tcp_example_com.clone(), 1)];
    {
        let mut expect_iter = expectations.iter().take(1);
        let mut descend_iter = nt.descend_mut(&example_com).map(|(name, &mut u)| (name, u));
        loop {
            let expect: Option<&(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual.as_ref());
            if expect.is_none() {
                break;
            }
        }
    }
    {
        let mut expect_iter = expectations.iter().cloned();
        let mut descend_iter = nt
            .descend_mut(&http_tcp_example_com)
            .map(|(name, &mut u)| (name, u));
        loop {
            let expect: Option<(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual);
            if expect.is_none() {
                break;
            }
        }
    }
}

#[test]
fn descend_root() {
    let root = Name::new();
    let http_tcp_example_com = name!("_http._tcp.example.com.");
    let mut nt = NameTree::new(root.clone());
    nt.insert(&root, 0usize).unwrap();
    nt.insert(&http_tcp_example_com, 1).unwrap();
    let expectations: &[(Name, usize)] = &[(root.clone(), 0), (http_tcp_example_com.clone(), 1)];
    {
        let mut expect_iter = expectations.iter().take(1).cloned();
        let mut descend_iter = nt.descend(&root).map(|(name, &u)| (name, u));
        loop {
            let expect: Option<(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual);
            if expect.is_none() {
                break;
            }
        }
    }
    {
        let mut expect_iter = expectations.iter().cloned();
        let mut descend_iter = nt
            .descend(&http_tcp_example_com)
            .map(|(name, &u)| (name, u));
        loop {
            let expect: Option<(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual);
            if expect.is_none() {
                break;
            }
        }
    }
}

#[test]
fn test_descend_root_mut() {
    let root = Name::new();
    let http_tcp_example_com = name!("_http._tcp.example.com.");
    let mut nt = NameTree::new(name!("."));
    nt.insert(&root, 0usize).unwrap();
    nt.insert(&http_tcp_example_com, 1).unwrap();
    let expectations: &[(Name, usize)] = &[(root.clone(), 0), (http_tcp_example_com.clone(), 1)];
    {
        let mut expect_iter = expectations.iter().take(1);
        let mut descend_iter = nt.descend_mut(&root).map(|(name, &mut u)| (name, u));
        loop {
            let expect: Option<&(Name, usize)> = expect_iter.next();
            let actual: Option<(Name, usize)> = descend_iter.next();
            assert_eq!(expect, actual.as_ref());
            if expect.is_none() {
                break;
            }
        }
    }
    {
        let mut expect_iter = expectations.iter();
        let mut descend_iter = nt
            .descend_mut(&http_tcp_example_com)
            .map(|(name, &mut u)| (name, u));
        loop {
            let expect = expect_iter.next();
            let actual = descend_iter.next();
            assert_eq!(expect, actual.as_ref());
            if expect.is_none() {
                break;
            }
        }
    }
}

#[test]
fn rfc4034_insert_and_iterate() {
    let names: &[Name] = &[
        name!(r"example."),
        name!(r"a.example."),
        name!(r"yljkjljk.a.example."),
        name!(r"Z.a.example."),
        name!(r"zABC.a.EXAMPLE."),
        name!(r"z.example."),
        name!(r"\001.z.example."),
        name!(r"*.z.example."),
        name!(r"\200.z.example."),
    ];
    let mut last: Option<&Name> = None;
    let mut a: NameTree<&Name> = NameTree::new(name!("example."));
    for n in names.iter() {
        let before = format!("{:#?}", a);
        let _ = a.insert(n, n).expect("insert");
        let inserted = a.get(n);
        if inserted != Ok(Some(&n)) {
            panic!(
                "lost current value - expect: {:?} actual: {:?} tree\n{:#?}\nbefore:\n{}",
                Some(&n),
                inserted,
                a,
                before
            );
        }
        if let Some(last) = last {
            let expect = Ok(Some(&last));
            let actual = a.get(last);
            if actual != expect {
                panic!(
                    "lost previous value - expect: {:?} actual: {:?} tree:\n{:#?}\nbefore:\n{}",
                    expect, actual, a, before
                );
            }
        }
        last = Some(&n);
    }
    for name in names {
        assert_eq!(a.get(name), Ok(Some(&name)));
    }
}

#[test]
fn get_or_insert_with_panic_doesnt_dangle() {
    let root = Name::new();
    let nt: Arc<Mutex<NameTree<()>>> = Arc::new(Mutex::new(NameTree::new(&root)));
    let dangling_name = name!("example.");
    {
        let nt = Arc::clone(&nt);
        let dangling_name = dangling_name.clone();
        assert!(std::panic::catch_unwind(move || {
            let _ = nt
                .lock()
                .expect("grab lock")
                .get_or_insert_with(&dangling_name, || panic!("oh no"));
        })
        .is_err());
    }
    let nt = nt.lock().unwrap_err().into_inner();
    assert_eq!(nt.match_down(&dangling_name), MatchDown::NX);
}
