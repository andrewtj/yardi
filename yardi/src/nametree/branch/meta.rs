use core::fmt;

#[derive(Clone, Copy, PartialEq)]
pub(in crate::nametree) struct Meta(u8);

impl Meta {
    const POINT_BIT: u8 = 0b1000_0000;
    const LOW_BIT: u8 = 0b0100_0000;
    const CHOICE_MASK: u8 = 0b0011_1111;
    pub fn new(choice: usize) -> Self {
        assert!(choice <= Self::CHOICE_MASK as usize);
        let choice = choice as u8;
        assert_eq!(choice & Self::CHOICE_MASK, choice);
        Meta(choice)
    }
    pub fn choice(self) -> usize {
        (Self::CHOICE_MASK & self.0) as usize
    }
    pub fn point_bit(self) -> bool {
        self.0 & Self::POINT_BIT == Self::POINT_BIT
    }
    pub fn set_point_bit(&mut self) -> &mut Self {
        self.0 |= Self::POINT_BIT;
        self
    }
    pub fn clear_point_bit(&mut self) -> &mut Self {
        self.0 &= !Self::POINT_BIT;
        self
    }
    pub fn low_bit(self) -> bool {
        self.0 & Self::LOW_BIT == Self::LOW_BIT
    }
    pub fn clear_low_bit(&mut self) -> &mut Self {
        self.0 &= !Self::LOW_BIT;
        self
    }
    pub fn set_low_bit(&mut self) -> &mut Self {
        self.0 |= Self::LOW_BIT;
        self
    }
}

impl fmt::Debug for Meta {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Meta")
            .field("point_bit", &self.point_bit())
            .field("low_bit", &self.low_bit())
            .field("choice", &self.choice())
            .finish()
    }
}
