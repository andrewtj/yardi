mod branch256;
mod branch64;
mod key;
mod meta;
mod pop;

#[cfg(not(feature = "std"))]
use alloc::boxed::Box;

pub(in crate::nametree) use self::{
    branch256::Branch256, branch64::Branch64, key::Key, meta::Meta, pop::Pop,
};
use super::{BranchOrPointBox, Point};
use crate::vec16::Vec16;
use core::{fmt, marker::PhantomData, mem, num::NonZeroUsize};

pub(super) const TAG_MASK: usize = 0b11;
pub(super) const TAG_BRANCH64: usize = 1;
pub(super) const TAG_BRANCH256: usize = 2;

pub(in crate::nametree) struct Branch<T> {
    p: PhantomData<T>,
    ptr: NonZeroUsize,
}

impl<T> Branch<T> {
    fn untagged_ptr(&self) -> usize {
        self.ptr.get() & !TAG_MASK
    }
}

macro_rules! boilerplate {
    ( $( ($ty:tt, $tag:tt) ),+) => {
        impl<T> Clone for Branch<T> where T: Clone {
            fn clone(&self) -> Self {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &*(self.untagged_ptr() as *const $ty<T>);
                        b.clone().into()
                    })+
                    _ => unreachable!(),
                }
            }
        }
        impl<T> Drop for Branch<T> {
            fn drop(&mut self) {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let raw = self.untagged_ptr() as *mut $ty<T>;
                        drop(Box::from_raw(raw));
                    })+
                    _ => unreachable!(),
                }
            }
        }

        impl<T> fmt::Debug for Branch<T> where T: fmt::Debug {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &*(self.untagged_ptr() as *const $ty<T>);
                        b.fmt(f)
                    })+
                    _ => unreachable!(),
                }
            }
        }

        impl<T> Branch<T> {
            pub fn new(choice: usize) -> Self {
                Branch64::new(choice).into()
            }
            pub fn new_width_hint(choice: usize, width: u8) -> Self {
                if width < 64 {
                    Branch64::new(choice).into()
                } else {
                    Branch256::new(choice).into()
                }
            }
            pub fn choice(&self) -> usize {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &*(self.untagged_ptr() as *const $ty<T>);
                        b.meta.choice()
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn set_point(&mut self, point: BranchOrPointBox<T>) -> &mut Point<T> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.set_point(point)
                    })+
                    _ => unreachable!(),
                }
            }
            #[allow(unused)]
            pub fn take_point(&mut self) -> Option<BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.take_point()
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn point(&self) -> Option<&Point<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &*(self.untagged_ptr() as *const $ty<T>);
                        b.point()
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn point_mut(&mut self) -> Option<&mut Point<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.point_mut()
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn take_last(&mut self) -> Option<BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.take_last()
                    })+
                    _ => unreachable!(),
                }
            }
            #[allow(unused)]
            pub fn member_len(&self) -> usize {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &*(self.untagged_ptr() as *const $ty<T>);
                        b.pop.len()
                    })+
                    _ => unreachable!(),
                }
            }
            fn insert_label_precheck(&mut self, label: &[u8]) -> bool {
                let fits = match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &*(self.untagged_ptr() as *const $ty<T>);
                        b.can_insert_label(label).expect("label should contain choice")
                    })+
                    _ => unreachable!(),
                };
                if fits {
                    return false;
                }
                let mut replacement = match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                        let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                        b.next_size_up()
                    })+
                    _ => unreachable!(),
                };
                mem::swap(&mut replacement, self);
                true
            }
            pub fn insert_label(
                &mut self,
                label: &[u8],
                value: BranchOrPointBox<T>,
            ) -> &mut BranchOrPointBox<T> {
                while self.insert_label_precheck(label) {}
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.insert_label(label, value)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn remove_immediate_point(&mut self, label: &[u8]) -> bool {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.remove_immediate_point(label)
                    })+
                    _ => unreachable!(),
                }
            }
            #[allow(unused)]
            pub fn remove_label(&mut self, label: &[u8]) {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.remove_label(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn first(&self) -> Option<&BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &*(self.untagged_ptr() as *const $ty<T>);
                            b.vec.first()
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn first_mut(&mut self) -> Option<&mut BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.vec.first_mut()
                    })+
                    _ => unreachable!(),
                }
            }
            #[allow(unused)]
            pub fn has_member_point(&self, label: &[u8]) -> bool {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &*(self.untagged_ptr() as *const $ty<T>);
                            b.has_member_point(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn is_last(&self, label: &[u8]) -> bool {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &*(self.untagged_ptr() as *const $ty<T>);
                            b.is_last(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn next_after(&self, label: &[u8]) -> Option<&BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &*(self.untagged_ptr() as *const $ty<T>);
                            b.next_after(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn next_after_mut(&mut self, label: &[u8]) -> Option<&mut BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.next_after_mut(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn get_label(&self, label: &[u8]) -> Option<&BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &*(self.untagged_ptr() as *const $ty<T>);
                            b.get_label(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn get_label_mut(&mut self, label: &[u8]) -> Option<&mut BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.get_label_mut(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn get_for_insert_chase(&mut self, label: &[u8]) -> &mut BranchOrPointBox<T> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.get_for_insert_chase(label)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn key_for_insert_mutate(&self, label: &[u8], max: usize) -> Option<Key> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &*(self.untagged_ptr() as *const $ty<T>);
                            b.key_for_insert_mutate(label, max)
                    })+
                    _ => unreachable!(),
                }
            }
            pub fn get_key_mut(&mut self, key: Key) -> Option<&mut BranchOrPointBox<T>> {
                match TAG_MASK & self.ptr.get() {
                    $($tag => unsafe {
                            let b = &mut *(self.untagged_ptr() as *mut $ty<T>);
                            b.get_key_mut(key)
                    })+
                    _ => unreachable!(),
                }
            }
        }

        $(
            impl<T> From<$ty<T>> for Branch<T> {
                fn from(branch: $ty<T>) -> Self {
                    Box::new(branch).into()
                }
            }
            impl<T> From<Box<$ty<T>>> for Branch<T> {
                fn from(b: Box<$ty<T>>) -> Self {
                    unsafe {
                        assert_eq!((b.as_ref() as *const $ty<T> as usize) & TAG_MASK, 0);
                        let ptr = NonZeroUsize::new_unchecked(Box::into_raw(b) as usize | $tag);
                        Branch {
                            p: PhantomData,
                            ptr,
                        }
                    }
                }
            }
        )+
    };
}
boilerplate![(Branch64, TAG_BRANCH64), (Branch256, TAG_BRANCH256)];
