pub(in crate::nametree) trait Pop {
    const MAX: u8;
    fn clear(&mut self);
    fn contains(&self, value: u8) -> bool;
    fn low(&self) -> Option<u8>;
    fn next_after(&self, value: u8) -> Option<u8>;
    fn insert(&mut self, value: u8);
    fn remove(&mut self, value: u8);
    fn index(&self, value: u8) -> usize;
    fn len(&self) -> usize;
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
}

macro_rules! impl_pop_for_uint {
    ($ty: ty) => {
        impl Pop for $ty {
            const MAX: u8 = (size_of::<$ty>() as u8 * 8) - 1;
            fn clear(&mut self) {
                *self = 0;
            }
            fn contains(&self, value: u8) -> bool {
                if value <= <Self as Pop>::MAX {
                    let bit = 1 << value;
                    *self & bit == bit
                } else {
                    false
                }
            }
            fn low(&self) -> Option<u8> {
                if *self != 0 {
                    Some(self.trailing_zeros() as u8)
                } else {
                    None
                }
            }
            fn next_after(&self, value: u8) -> Option<u8> {
                if value <= <Self as Pop>::MAX {
                    let mask = !0 << value + 1;
                    if let next @ 0..=<Self as Pop>::MAX = (mask & self).trailing_zeros() as u8 {
                        assert!(value < next, "index: {} next: {}", value, next);
                        return Some(next);
                    }
                }
                None
            }
            fn insert(&mut self, value: u8) {
                assert!(value <= <Self as Pop>::MAX);
                let bit = 1 << value;
                *self |= bit;
            }
            fn remove(&mut self, value: u8) {
                assert!(value <= <Self as Pop>::MAX);
                let bit = 1 << value;
                *self &= !bit;
            }
            fn index(&self, value: u8) -> usize {
                assert!(value <= <Self as Pop>::MAX);
                (self.count_ones() - ((!0 << value) & *self).count_ones()) as usize
            }
            fn len(&self) -> usize {
                self.count_ones() as usize
            }
        }
    };
}

impl_pop_for_uint!(u8);
impl_pop_for_uint!(u16);
impl_pop_for_uint!(u32);
impl_pop_for_uint!(u64);
impl_pop_for_uint!(u128);
