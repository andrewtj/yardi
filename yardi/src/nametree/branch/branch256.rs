use super::{Branch, Branch64, BranchOrPointBox, Key, Meta, Point, Pop, Vec16};
#[cfg(not(feature = "std"))]
use alloc::boxed::Box;
use core::{fmt, mem};

#[derive(Clone, Copy, Default)]
pub(super) struct Pop256 {
    low: u128,
    high: u128,
}

impl Pop for Pop256 {
    const MAX: u8 = 0xFF;
    fn clear(&mut self) {
        self.low = 0;
        self.high = 0;
    }
    fn contains(&self, value: u8) -> bool {
        if 0x80 & value == 0 {
            let bit = 1 << value;
            self.low & bit == bit
        } else {
            let bit = 1 << (0x7F & value);
            self.high & bit == bit
        }
    }
    fn low(&self) -> Option<u8> {
        if self.low != 0 {
            Some(self.low.trailing_zeros() as u8)
        } else if self.high != 0 {
            Some(self.high.trailing_zeros() as u8 + 128)
        } else {
            None
        }
    }
    fn next_after(&self, value: u8) -> Option<u8> {
        let mask = if value < 0x80 {
            let mask = !0 << (value + 1);
            if let next @ 0..=127 = (mask & self.low).trailing_zeros() as u8 {
                assert!(value < next, "index: {} next: {}", value, next);
                return Some(next);
            }
            !0
        } else {
            !0 << 0x7f & (u128::from(value) + 1)
        };
        if let next @ 0..=127 = (mask & self.high).trailing_zeros() as u8 {
            let next = next + 128;
            assert!(value < next, "index: {} next: {}", value, next);
            Some(next)
        } else {
            None
        }
    }
    fn insert(&mut self, value: u8) {
        if 0x80 & value == 0 {
            let bit = 1 << value;
            self.low |= bit;
        } else {
            let bit = 1 << (0x7F & value);
            self.high |= bit;
        }
    }
    fn remove(&mut self, value: u8) {
        if 0x80 & value == 0 {
            let bit = 1 << value;
            self.low &= !bit;
        } else {
            let bit = 1 << (0x7F & value);
            self.high &= !bit;
        }
    }
    fn index(&self, value: u8) -> usize {
        if value & 0x80 == 0 {
            (self.low.count_ones() - ((!0 << value) & self.low).count_ones()) as usize
        } else {
            let bit_index = 0x7F & value;
            (self.low.count_ones()
                + (self.high.count_ones() - ((!0 << bit_index) & self.high).count_ones()))
                as usize
        }
    }
    fn len(&self) -> usize {
        (self.high.count_ones() + self.low.count_ones()) as usize
    }
}

#[derive(Clone)]
pub(in crate::nametree) struct Branch256<T> {
    pub(super) meta: Meta,
    pub(super) pop: Pop256,
    pub(super) vec: Vec16<BranchOrPointBox<T>>,
}

impl<T> Branch256<T> {
    pub fn next_size_up(&mut self) -> Branch<T> {
        unreachable!();
    }
    pub fn new(choice: usize) -> Self {
        Branch256 {
            meta: Meta::new(choice),
            pop: Pop256::default(),
            vec: Default::default(),
        }
    }
    pub fn boxed_new_from_branch64(other: &mut Branch64<T>) -> Box<Self> {
        let mut new = Box::new(Branch256::new(other.meta.choice()));
        mem::swap(&mut new.vec, &mut other.vec);
        mem::swap(&mut new.meta, &mut other.meta);
        if new.meta.low_bit() {
            match other.low.0 {
                min if min >= 128 => {
                    new.pop.high = u128::from(other.pop) << (128 - min);
                }
                0 => {
                    new.pop.low = other.pop.into();
                }
                min => {
                    let pop = u128::from(other.pop);
                    new.pop.low = pop << min;
                    new.pop.high = pop >> (128 - min);
                }
            }
        }
        new.meta.clear_low_bit();
        other.pop = 0;
        new
    }
    pub fn set_point(&mut self, point: BranchOrPointBox<T>) -> &mut Point<T> {
        assert!(!self.meta.point_bit());
        assert!(point.is_point());
        self.vec.push(point);
        self.meta.set_point_bit();
        unsafe { self.vec.last_mut().unwrap().as_point_mut_unchecked() }
    }
    pub fn take_point(&mut self) -> Option<BranchOrPointBox<T>> {
        if self.meta.point_bit() {
            let point = self.vec.pop();
            self.meta.clear_point_bit();
            point
        } else {
            None
        }
    }
    pub fn point(&self) -> Option<&Point<T>> {
        if self.meta.point_bit() {
            Some(
                self.vec
                    .last()
                    .expect("empty with point set")
                    .as_point()
                    .expect("last isn't a point with point set"),
            )
        } else {
            None
        }
    }
    pub fn point_mut(&mut self) -> Option<&mut Point<T>> {
        if self.meta.point_bit() {
            Some(
                self.vec
                    .last_mut()
                    .expect("empty with point set")
                    .as_point_mut()
                    .expect("last isn't a point with point set"),
            )
        } else {
            None
        }
    }
    pub fn take_last(&mut self) -> Option<BranchOrPointBox<T>> {
        if self.vec.len() != 1 {
            return None;
        }
        self.pop.clear();
        self.meta.clear_point_bit();
        self.vec.pop()
    }
    // TODO: Error - code path to here looks odd
    pub fn can_insert_label(&self, label: &[u8]) -> Result<bool, ()> {
        if label.get(self.meta.choice()).is_some() {
            Ok(true)
        } else {
            Err(())
        }
    }
    pub fn insert_label(
        &mut self,
        label: &[u8],
        value: BranchOrPointBox<T>,
    ) -> &mut BranchOrPointBox<T> {
        let key = self
            .label_to_key(label)
            .expect("this label can't be inserted");
        debug_assert!(!self.pop.contains(key.0));
        let index = self.pop.index(key.0);
        self.vec.insert(index, value);
        self.pop.insert(key.0);
        &mut self.vec[index]
    }
    pub fn remove_immediate_point(&mut self, label: &[u8]) -> bool {
        if self.point().map(|p| p.label() == label).unwrap_or(false) {
            let _ = self.take_point();
            self.meta.clear_point_bit();
            return true;
        }
        let has_member_point = self
            .get_label(label)
            .and_then(BranchOrPointBox::as_point)
            .map(|p| p.label() == label)
            .unwrap_or(false);
        if has_member_point {
            self.remove_label(label);
            true
        } else {
            false
        }
    }
    pub fn remove_label(&mut self, label: &[u8]) {
        let key = self.label_to_key(label).unwrap();
        if !self.pop.contains(key.0) {
            return;
        }
        let index = self.pop.index(key.0);
        self.vec.remove(index).as_point();
        self.pop.remove(key.0);
    }
    pub fn has_member_point(&self, label: &[u8]) -> bool {
        self.get_label(label)
            .and_then(BranchOrPointBox::as_point)
            .map(|p| p.label() == label)
            .unwrap_or(false)
    }
    pub fn is_last(&self, label: &[u8]) -> bool {
        if let Some(key) = self.label_to_key(label) {
            self.pop.next_after(key.0).is_none()
        } else {
            true
        }
    }
    pub fn next_after(&self, label: &[u8]) -> Option<&BranchOrPointBox<T>> {
        let index = self
            .label_to_key(label)
            .and_then(|k| self.pop.next_after(k.0))
            .map(|k| self.pop.index(k))?;
        Some(&self.vec[index])
    }
    pub fn next_after_mut(&mut self, label: &[u8]) -> Option<&mut BranchOrPointBox<T>> {
        let index = self
            .label_to_key(label)
            .and_then(|k| self.pop.next_after(k.0))
            .map(|k| self.pop.index(k))?;
        Some(&mut self.vec[index])
    }
    pub fn get_label(&self, label: &[u8]) -> Option<&BranchOrPointBox<T>> {
        let key = self.label_to_key(label)?;
        if !self.pop.contains(key.0) {
            return None;
        }
        let index = self.pop.index(key.0);
        Some(&self.vec[index])
    }
    pub fn get_label_mut(&mut self, label: &[u8]) -> Option<&mut BranchOrPointBox<T>> {
        let key = self.label_to_key(label)?;
        if !self.pop.contains(key.0) {
            return None;
        }
        let index = self.pop.index(key.0);
        Some(&mut self.vec[index])
    }
    pub fn get_for_insert_chase(&mut self, label: &[u8]) -> &mut BranchOrPointBox<T> {
        if let Some(key) = self.label_to_key(label).filter(|k| self.has_key(*k)) {
            self.get_key_mut(key).unwrap()
        } else if self.meta.point_bit() {
            return self.vec.last_mut().unwrap();
        } else {
            // If we don't have an exact match, plow on to get a label so we
            // can work out where we diverge.
            let i = self
                .vec
                .iter_mut()
                .position(|bop| bop.is_point())
                .unwrap_or(0);
            &mut self.vec[i]
        }
    }
    pub fn key_for_insert_mutate(&self, label: &[u8], max: usize) -> Option<Key> {
        let choice = self.meta.choice();
        if choice >= max {
            None
        } else {
            self.label_to_key(label)
        }
    }
    pub fn get_key_mut(&mut self, key: Key) -> Option<&mut BranchOrPointBox<T>> {
        if !self.pop.contains(key.0) {
            return None;
        }
        let index = self.pop.index(key.0);
        Some(&mut self.vec[index])
    }
    fn label_to_key(&self, label: &[u8]) -> Option<Key> {
        label.get(self.meta.choice()).cloned().map(Key::from)
    }
    fn has_key(&self, key: Key) -> bool {
        self.pop.contains(key.0)
    }
    fn member_iter(&self) -> Branch256MembersIter<'_, T> {
        Branch256MembersIter {
            branch: self,
            last: None,
        }
    }
}

impl<T> fmt::Debug for Branch256<T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let members = MemberDebugHelper {
            member_iter: self.member_iter(),
        };
        f.debug_struct("Branch256")
            .field("choice", &self.meta.choice())
            .field("point", &self.point())
            .field("members", &members)
            .finish()
    }
}

#[derive(Copy)]
struct Branch256MembersIter<'a, T> {
    branch: &'a Branch256<T>,
    last: Option<u8>,
}

impl<'a, T> Clone for Branch256MembersIter<'a, T> {
    fn clone(&self) -> Self {
        Branch256MembersIter {
            branch: self.branch,
            last: self.last,
        }
    }
}

impl<'a, T> Iterator for Branch256MembersIter<'a, T> {
    type Item = (Key, &'a BranchOrPointBox<T>);
    fn next(&mut self) -> Option<Self::Item> {
        match self.last {
            None => {
                let bit_index = self.branch.pop.low()?;
                self.last = Some(bit_index);
                Some((Key(bit_index), &self.branch.vec[0]))
            }
            Some(last) => {
                if let Some(bit_index) = self.branch.pop.next_after(last) {
                    let key = Key(bit_index);
                    let index = self.branch.pop.index(bit_index);
                    self.last = Some(bit_index);
                    Some((key, &self.branch.vec[index]))
                } else {
                    None
                }
            }
        }
    }
}

struct MemberDebugHelper<'a, T> {
    member_iter: Branch256MembersIter<'a, T>,
}

impl<'a, T> fmt::Debug for MemberDebugHelper<'a, T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map().entries(self.member_iter.clone()).finish()
    }
}

#[test]
fn insert_order() {
    use crate::datatypes::Name;
    let mut branch = Branch256::new(0);
    let a: &[u8] = b"a";
    let b: &[u8] = b"b";
    let c: &[u8] = b"c";
    fn bop(l: &[u8]) -> BranchOrPointBox<&[u8]> {
        let mut n = Name::new();
        n.push(l).expect("push label");
        let p = Point::new(n, l);
        p.into()
    }
    let _ = branch.insert_label(a, bop(a));
    assert_eq!(
        branch
            .get_label(a)
            .unwrap()
            .as_point()
            .unwrap()
            .value
            .as_ref(),
        Some(&a)
    );
    let _ = branch.insert_label(c, bop(c));
    for l in &[a, c] {
        assert_eq!(
            branch
                .get_label(l)
                .unwrap()
                .as_point()
                .unwrap()
                .value
                .as_ref(),
            Some(l)
        );
    }
    let _ = branch.insert_label(b, bop(b));
    for l in &[a, b, c] {
        assert_eq!(
            branch
                .get_label(l)
                .unwrap()
                .as_point()
                .unwrap()
                .value
                .as_ref(),
            Some(l)
        );
    }
}
