use core::cmp::Ordering;

/// Key contains a canonical label byte. Since upper case letters do not appear
/// in canonical form the range b'A'..=b'Z' is skipped in the internal
/// representation. That is, any ASCII value larger than b'Z' has 26 subtracted
/// from it. This keeps canonical order while hopefully getting a little bit
/// more out of the smaller maps.
#[derive(Clone, Copy, Debug, PartialEq)]
pub(in crate::nametree) struct Key(pub u8);

impl Key {
    const UNCHANGED_END: u8 = b'A' - 1;
}

impl Key {
    pub fn diff(self, other: Self) -> (Ordering, u8) {
        match self.0.cmp(&other.0) {
            o @ Ordering::Equal => (o, 0),
            o @ Ordering::Less => (o, other.0 - self.0),
            o @ Ordering::Greater => (o, self.0 - other.0),
        }
    }
}

impl From<u8> for Key {
    fn from(b: u8) -> Self {
        Key(match b {
            0..=Self::UNCHANGED_END => b,
            b'A'..=b'Z' => b + (b'a' - b'['),
            _ => b - 26,
        })
    }
}

impl From<Key> for u8 {
    fn from(Key(b): Key) -> Self {
        if b > Key::UNCHANGED_END {
            b + 26
        } else {
            b
        }
    }
}

#[test]
fn key_range() {
    use core::cmp;
    let (mut min, mut max) = (!0, 0);
    let common = (b'a'..=b'z')
        .into_iter()
        .chain(b'0'..=b'9')
        .chain(b"-_*".into_iter().cloned());
    for b in common {
        let v = Key::from(b).0;
        min = cmp::min(min, v);
        max = cmp::max(max, v);
    }
    assert!(64 >= max - min);
}
