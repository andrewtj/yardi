use super::{Branch, Branch256, BranchOrPointBox, Key, Meta, Point, Pop, Vec16};
use core::{cmp::Ordering, fmt};

#[derive(Clone)]
pub(in crate::nametree) struct Branch64<T> {
    pub(super) meta: Meta,
    pub(super) low: Key,
    pub(super) pop: u64,
    pub(super) vec: Vec16<BranchOrPointBox<T>>,
}

impl<T> Branch64<T> {
    pub fn next_size_up(&mut self) -> Branch<T> {
        Branch256::boxed_new_from_branch64(self).into()
    }
    pub fn new(choice: usize) -> Self {
        Branch64 {
            meta: Meta::new(choice),
            low: Key(0),
            pop: 0,
            vec: Default::default(),
        }
    }
    pub fn set_point(&mut self, point: BranchOrPointBox<T>) -> &mut Point<T> {
        assert!(!self.meta.point_bit());
        assert!(point.is_point());
        self.vec.push(point);
        self.meta.set_point_bit();
        unsafe { self.vec.last_mut().unwrap().as_point_mut_unchecked() }
    }
    pub fn take_point(&mut self) -> Option<BranchOrPointBox<T>> {
        if self.meta.point_bit() {
            let point = self.vec.pop();
            self.meta.clear_point_bit();
            point
        } else {
            None
        }
    }
    pub fn point(&self) -> Option<&Point<T>> {
        if self.meta.point_bit() {
            Some(
                self.vec
                    .last()
                    .expect("empty with point set")
                    .as_point()
                    .expect("last isn't a point with point set"),
            )
        } else {
            None
        }
    }
    pub fn point_mut(&mut self) -> Option<&mut Point<T>> {
        if self.meta.point_bit() {
            Some(
                self.vec
                    .last_mut()
                    .expect("empty with point set")
                    .as_point_mut()
                    .expect("last isn't a point with point set"),
            )
        } else {
            None
        }
    }
    pub fn take_last(&mut self) -> Option<BranchOrPointBox<T>> {
        if self.vec.len() != 1 {
            return None;
        }
        self.pop.clear();
        self.meta.clear_low_bit();
        self.meta.clear_point_bit();
        self.vec.pop()
    }
    // TODO: Error - code path to here looks odd
    pub fn can_insert_label(&self, label: &[u8]) -> Result<bool, ()> {
        let key: Key = match self.label_to_key(label) {
            Some(k) => k,
            None => return Err(()),
        };
        if !self.meta.low_bit() {
            return Ok(true);
        }
        match self.low.diff(key) {
            (Ordering::Equal, _) => Ok(true),
            (Ordering::Less, diff) => Ok(diff < 64),
            (Ordering::Greater, diff) => Ok(diff <= self.pop.leading_zeros() as u8),
        }
    }
    pub fn insert_label(
        &mut self,
        label: &[u8],
        value: BranchOrPointBox<T>,
    ) -> &mut BranchOrPointBox<T> {
        let key = self
            .label_to_key(label)
            .expect("this label can't be inserted");
        if !self.meta.low_bit() {
            assert_eq!(self.pop, 0);
            self.vec.insert(0, value);
            self.low = key;
            self.meta.set_low_bit();
            self.pop.insert(0);
            return &mut self.vec[0];
        }
        match self.low.0.cmp(&key.0) {
            Ordering::Equal => unreachable!("insert over existing"),
            Ordering::Less => {
                let diff = key.0 - self.low.0;
                debug_assert!(!self.pop.contains(diff));
                let index = self.pop.index(diff);
                self.vec.insert(index, value);
                self.pop.insert(diff);
                &mut self.vec[index]
            }
            Ordering::Greater => {
                let diff = self.low.0 - key.0;
                let lz = self.pop.leading_zeros() as u8;
                assert!(diff < lz, "{} < {}", diff, lz);
                self.vec.insert(0, value);
                self.pop <<= diff;
                self.pop |= 1;
                self.low = key;
                &mut self.vec[0]
            }
        }
    }
    pub fn remove_immediate_point(&mut self, label: &[u8]) -> bool {
        if self.point().map(|p| p.label() == label).unwrap_or(false) {
            let _ = self.take_point();
            self.meta.clear_point_bit();
            return true;
        }
        let has_member_point = self
            .get_label(label)
            .and_then(BranchOrPointBox::as_point)
            .map(|p| p.label() == label)
            .unwrap_or(false);
        if has_member_point {
            self.remove_label(label);
            true
        } else {
            false
        }
    }
    pub fn remove_label(&mut self, label: &[u8]) {
        if !self.meta.low_bit() {
            return;
        }
        let key = self
            .label_to_key(label)
            .filter(|&k| self.low.0 <= k.0)
            .unwrap();
        let diff = key.0 - self.low.0;
        if diff >= 64 {
            return;
        }
        if !self.pop.contains(diff) {
            return;
        }
        let index = self.pop.index(diff);
        self.vec.remove(index);
        self.pop.remove(diff);
        if diff == 0 {
            if self.pop.is_empty() {
                self.meta.clear_low_bit();
            } else {
                let tz = self.pop.trailing_zeros() as u8;
                self.pop >>= tz;
                self.low.0 += tz;
            }
        }
    }
    pub fn has_member_point(&self, label: &[u8]) -> bool {
        self.get_label(label)
            .and_then(BranchOrPointBox::as_point)
            .map(|p| p.label() == label)
            .unwrap_or(false)
    }
    pub fn is_last(&self, label: &[u8]) -> bool {
        if !self.meta.low_bit() {
            return true;
        }
        if let Some(diff) = self
            .label_to_key(label)
            .and_then(|k| k.0.checked_sub(self.low.0))
        {
            self.pop.next_after(diff).is_none()
        } else {
            true
        }
    }
    pub fn next_after(&self, label: &[u8]) -> Option<&BranchOrPointBox<T>> {
        let index = self
            .label_to_key(label)
            .and_then(|k| k.0.checked_sub(self.low.0))
            .and_then(|k| self.pop.next_after(k))
            .map(|k| self.pop.index(k))?;
        Some(&self.vec[index])
    }
    pub fn next_after_mut(&mut self, label: &[u8]) -> Option<&mut BranchOrPointBox<T>> {
        let index = self
            .label_to_key(label)
            .and_then(|k| k.0.checked_sub(self.low.0))
            .and_then(|k| self.pop.next_after(k))
            .map(|k| self.pop.index(k))?;
        Some(&mut self.vec[index])
    }
    pub fn get_label(&self, label: &[u8]) -> Option<&BranchOrPointBox<T>> {
        if !self.meta.low_bit() {
            return None;
        }
        let diff = self
            .label_to_key(label)
            .and_then(|k| k.0.checked_sub(self.low.0))?;
        if !self.pop.contains(diff) {
            return None;
        }
        let index = self.pop.index(diff);
        Some(&self.vec[index])
    }
    pub fn get_label_mut(&mut self, label: &[u8]) -> Option<&mut BranchOrPointBox<T>> {
        if !self.meta.low_bit() {
            return None;
        }
        let diff = self
            .label_to_key(label)
            .and_then(|k| k.0.checked_sub(self.low.0))?;
        if !self.pop.contains(diff) {
            return None;
        }
        let index = self.pop.index(diff);
        Some(&mut self.vec[index])
    }
    pub fn get_for_insert_chase(&mut self, label: &[u8]) -> &mut BranchOrPointBox<T> {
        if let Some(key) = self.label_to_key(label).filter(|k| self.has_key(*k)) {
            self.get_key_mut(key).unwrap()
        } else if self.meta.point_bit() {
            return self.vec.last_mut().unwrap();
        } else {
            // If we don't have an exact match, plow on to get a label so we
            // can work out where we diverge.
            let i = self
                .vec
                .iter_mut()
                .position(|bop| bop.is_point())
                .unwrap_or(0);
            &mut self.vec[i]
        }
    }
    pub fn key_for_insert_mutate(&self, label: &[u8], max: usize) -> Option<Key> {
        let choice = self.meta.choice();
        if choice >= max {
            None
        } else {
            self.label_to_key(label)
        }
    }
    pub fn get_key_mut(&mut self, key: Key) -> Option<&mut BranchOrPointBox<T>> {
        if !self.meta.low_bit() {
            return None;
        }
        if key.0 < self.low.0 {
            return None;
        }
        let diff = key.0 - self.low.0;
        if !self.pop.contains(diff) {
            return None;
        }
        let index = self.pop.index(diff);
        Some(&mut self.vec[index])
    }
    fn label_to_key(&self, label: &[u8]) -> Option<Key> {
        label.get(self.meta.choice()).cloned().map(Key::from)
    }
    fn has_key(&self, key: Key) -> bool {
        if !self.meta.low_bit() || self.low.0 > key.0 {
            return false;
        }
        let diff = key.0 - self.low.0;
        self.pop.contains(diff)
    }
    fn member_iter(&self) -> Branch64MembersIter<'_, T> {
        Branch64MembersIter {
            branch: self,
            last: None,
        }
    }
}

impl<T> fmt::Debug for Branch64<T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let members = MemberDebugHelper {
            member_iter: self.member_iter(),
        };
        f.debug_struct("Branch64")
            .field("choice", &self.meta.choice())
            .field("point", &self.point())
            .field("members", &members)
            .finish()
    }
}

#[derive(Copy)]
struct Branch64MembersIter<'a, T> {
    branch: &'a Branch64<T>,
    last: Option<u8>,
}

impl<'a, T> Clone for Branch64MembersIter<'a, T> {
    fn clone(&self) -> Self {
        Branch64MembersIter {
            branch: self.branch,
            last: self.last,
        }
    }
}

impl<'a, T> Iterator for Branch64MembersIter<'a, T> {
    type Item = (Key, &'a BranchOrPointBox<T>);
    fn next(&mut self) -> Option<Self::Item> {
        match self.last {
            None => {
                if !self.branch.meta.low_bit() {
                    return None;
                }
                debug_assert_ne!(self.branch.pop, 0);
                self.last = Some(0);
                let key = self.branch.low;
                Some((key, &self.branch.vec[0]))
            }
            Some(last) => {
                if let Some(bit_index) = self.branch.pop.next_after(last) {
                    let key = Key(self.branch.low.0 + bit_index);
                    let index = self.branch.pop.index(bit_index);
                    self.last = Some(bit_index);
                    Some((key, &self.branch.vec[index]))
                } else {
                    None
                }
            }
        }
    }
}

struct MemberDebugHelper<'a, T> {
    member_iter: Branch64MembersIter<'a, T>,
}

impl<'a, T> fmt::Debug for MemberDebugHelper<'a, T>
where
    T: fmt::Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_map().entries(self.member_iter.clone()).finish()
    }
}
