use super::{Branch, BranchOrPointBox};
use crate::datatypes::Name;

#[derive(Clone, Debug)]
pub(in crate::nametree) struct Point<T> {
    pub name: Name,
    pub value: Option<T>,
    pub children: Option<BranchOrPointBox<T>>,
}

impl<T> Point<T> {
    pub fn new<N, V>(name: N, value: V) -> Self
    where
        N: Into<Name>,
        V: Into<Option<T>>,
    {
        Point {
            name: name.into(),
            value: value.into(),
            children: None,
        }
    }
    pub fn get(&self, l: &[u8]) -> Option<&Point<T>> {
        self.children.as_ref().and_then(|bop| bop.get(l))
    }
    pub fn get_mut(&mut self, l: &[u8]) -> Result<&mut Point<T>, &mut Point<T>> {
        let me: *mut Self = self;
        self.children
            .as_mut()
            .and_then(|bop| bop.get_mut(l))
            .ok_or_else(|| unsafe { &mut *me })
    }
    pub fn get_or_insert(&mut self, l: &[u8]) -> &mut Point<T> {
        let Point {
            ref name,
            ref mut children,
            ..
        } = self;
        match children {
            None => {
                let mut n = name.clone();
                n.push(l).unwrap();
                let point = Point::new(n, None);
                *children = Some(point.into());
                children
                    .as_mut()
                    .expect("just added children")
                    .as_point_mut()
                    .expect("just added point")
            }
            Some(ref mut bop) => bop.get_or_insert(name, l),
        }
    }
    pub fn label(&self) -> &[u8] {
        self.name.labels().last().expect("never called on root")
    }
    pub fn is_empty_single_point(&self) -> bool {
        if self.value.is_none() {
            self.children
                .as_ref()
                .map(|bop| bop.is_point())
                .unwrap_or(false)
        } else {
            false
        }
    }
    pub fn seek<'b>(&self, target: &'b Name) -> Option<&Self> {
        if !self.name.is_suffix(target) {
            return None;
        }
        let skip = self.name.labels().count();
        let mut cursor = self;
        for l in target.labels().skip(skip) {
            cursor = cursor.get(l)?;
        }
        Some(cursor)
    }
    pub fn seek_mut<'b>(&mut self, target: &'b Name) -> Option<&mut Self> {
        if !self.name.is_suffix(target) {
            return None;
        }
        let skip = self.name.labels().count();
        let mut cursor = self;
        for l in target.labels().skip(skip) {
            cursor = cursor.get_mut(l).ok()?;
        }
        Some(cursor)
    }
    pub fn get_after<'b>(&self, label: &'b [u8]) -> Option<&Self> {
        let mut cursor = self
            .children
            .as_ref()
            .and_then(BranchOrPointBox::as_branch)?;
        let mut lmb: Option<&Branch<T>> = None;
        loop {
            if cursor.point().map(|p| p.label() == label).unwrap_or(false) {
                return cursor.first()?.first();
            } else if !cursor.is_last(label) {
                lmb = Some(cursor);
            }
            if let Some(next) = cursor
                .get_label(label)
                .and_then(BranchOrPointBox::as_branch)
            {
                cursor = next;
            } else {
                break;
            }
        }
        lmb?.next_after(label)?.first()
    }
    pub fn get_after_mut(&mut self, label: &[u8]) -> Option<&mut Self> {
        unsafe {
            let mut cursor = self
                .children
                .as_mut()
                .and_then(BranchOrPointBox::as_branch_mut)?;
            let mut lmb: Option<*mut Branch<T>> = None;
            loop {
                if cursor.point().map(|p| p.label() == label).unwrap_or(false) {
                    return cursor.first_mut()?.first_mut();
                } else if !cursor.is_last(label) {
                    lmb = Some(cursor);
                }
                if let Some(next) = cursor
                    .get_label_mut(label)
                    .and_then(BranchOrPointBox::as_branch_mut)
                {
                    cursor = next;
                } else {
                    break;
                }
            }
            (*lmb?).next_after_mut(label)?.first_mut()
        }
    }
}
