//! A tree mapping `Names` to values.
// TODO: This whold module needs some love. Internals were built pre-NLL,
//       and &Name isn't that useful anymore since they're RC'd. Then
//       again maybe this whole thing should go?
#[cfg(test)]
mod test;

mod branch;
mod point;

#[cfg(not(feature = "std"))]
use alloc::boxed::Box;
use core::{
    cmp,
    cmp::Ordering,
    fmt::{self, Debug},
    marker::PhantomData,
    mem,
    num::NonZeroUsize,
    ptr,
};

use self::branch::{Branch, TAG_BRANCH256, TAG_BRANCH64, TAG_MASK};
use self::point::Point;
use crate::datatypes::Name;

#[doc = r#"
An error indicating the name provided isn't within the scope of a `NameTree`.
```
use yardi::{name, nametree::{NameTree, NotTree}};
let mut nt: NameTree<()> = NameTree::new(&name!("example.com."));
assert_eq!(nt.get(&name!("com.")), Err(NotTree));
```
"#]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct NotTree;

const TAG_POINT: usize = 0;

/// References a node in a `NameTree` that contains a value.
pub struct OccupiedNode<'a, T> {
    point: &'a mut Point<T>,
}

impl<'a, T> OccupiedNode<'a, T> {
    /// Returns a reference to the node `Name`.
    pub fn name(&self) -> &Name {
        &self.point.name
    }
    /// Consume `self` returning a reference to the node value.
    pub fn value(self) -> &'a T {
        match self.point.value.as_ref() {
            Some(t) => t,
            None => unreachable!(),
        }
    }
    /// Consume `self` returning a mutable reference to the node value.
    pub fn value_mut(self) -> &'a mut T {
        match self.point.value.as_mut() {
            Some(t) => t,
            None => unreachable!(),
        }
    }
    /// Consume `self` returning references to the node name and value.
    pub fn name_and_value(self) -> (&'a Name, &'a T) {
        let Point {
            ref value,
            ref name,
            ..
        } = *self.point;
        match value.as_ref() {
            Some(t) => (name, t),
            None => unreachable!(),
        }
    }
    /// Consume `self` returning a reference to the node name and a mutable
    /// reference to the node value.
    pub fn name_and_value_mut(self) -> (&'a Name, &'a mut T) {
        let Point {
            ref mut value,
            ref name,
            ..
        } = *self.point;
        match value.as_mut() {
            Some(t) => (name, t),
            None => unreachable!(),
        }
    }
    /// Returns a reference to the node value.
    pub fn get(&self) -> &T {
        match self.point.value.as_ref() {
            Some(t) => t,
            None => unreachable!(),
        }
    }
    /// Returns a mutable reference to the node value.
    pub fn get_mut(&mut self) -> &mut T {
        match self.point.value.as_mut() {
            Some(t) => t,
            None => unreachable!(),
        }
    }
}

impl<'a, T> Debug for OccupiedNode<'a, T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name = &self.point.name;
        let value = self.point.value.as_ref().unwrap();
        f.debug_struct("OccupiedNode")
            .field("name", name)
            .field("value", value)
            .finish()
    }
}

/// References a node in a `NameTree` that does not contain a value.
pub struct EmptyNode<'a, T> {
    point: &'a mut Point<T>,
}

impl<'a, T> EmptyNode<'a, T> {
    /// Returns a reference to the node `Name`.
    pub fn name(&self) -> &Name {
        &self.point.name
    }
    /// Sets a value at the node and returns a mutable reference to it.
    pub fn insert(self, value: T) -> &'a mut T {
        debug_assert!(self.point.value.is_none());
        self.point.value.get_or_insert(value)
    }
}

impl<'a, T> Debug for EmptyNode<'a, T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name = &self.point.name;
        f.debug_struct("OccupiedNode").field("name", name).finish()
    }
}

/// References a node in a `NameTree` which may be occupied or empty.
#[derive(Debug)]
pub enum Node<'a, T> {
    /// An occupied node.
    Occupied(OccupiedNode<'a, T>),
    /// An empty node.
    Empty(EmptyNode<'a, T>),
}

impl<'a, T> Node<'a, T> {
    /// Returns a reference to the node `Name`.
    pub fn name(&self) -> &Name {
        match *self {
            Node::Occupied(ref entry) => entry.name(),
            Node::Empty(ref entry) => entry.name(),
        }
    }
    /// Inserts `default` if the node is empty, and returns a mutable reference
    /// to the value in the node.
    pub fn or_insert(self, default: T) -> &'a mut T {
        match self {
            Node::Occupied(entry) => entry.value_mut(),
            Node::Empty(entry) => entry.insert(default),
        }
    }
    /// Inserts the value produced by `default` if the node is empty, and
    /// returns a mutable reference to the value in the node.
    pub fn or_insert_with<F>(self, default: F) -> &'a mut T
    where
        F: FnOnce() -> T,
    {
        match self {
            Node::Occupied(entry) => entry.value_mut(),
            Node::Empty(entry) => entry.insert(default()),
        }
    }
    /// Allows modification of the node value, if present.
    pub fn and_modify<F>(self, f: F) -> Self
    where
        F: FnOnce(&mut T),
    {
        match self {
            Node::Occupied(mut entry) => {
                f(entry.get_mut());
                Node::Occupied(entry)
            }
            Node::Empty(entry) => Node::Empty(entry),
        }
    }
    /// Consume `self` returning a reference to the node `Name` and a
    /// reference to the node value, if any.
    pub fn name_and_value(self) -> (&'a Name, Option<&'a T>) {
        match self {
            Node::Occupied(entry) => {
                let (name, val) = entry.name_and_value();
                (name, Some(val))
            }
            Node::Empty(entry) => (&entry.point.name, None),
        }
    }
    /// Consume `self` returning a reference to the node `Name` and a
    /// mutable reference to the node value, if any.
    pub fn name_and_value_mut(self) -> (&'a Name, Option<&'a mut T>) {
        match self {
            Node::Occupied(entry) => {
                let (name, val) = entry.name_and_value_mut();
                (name, Some(val))
            }
            Node::Empty(entry) => (&entry.point.name, None),
        }
    }
    /// Consume `self` returning a reference to the node value, if any.
    pub fn value(self) -> Option<&'a T> {
        self.into()
    }
    /// Consume `self` returning a mutable reference to the node value, if any.
    pub fn value_mut(self) -> Option<&'a T> {
        self.into()
    }
}

impl<'a, T> Into<Option<&'a T>> for Node<'a, T> {
    fn into(self) -> Option<&'a T> {
        match self {
            Node::Occupied(entry) => Some(entry.value()),
            Node::Empty(_) => None,
        }
    }
}

impl<'a, T> Into<Option<&'a mut T>> for Node<'a, T> {
    fn into(self) -> Option<&'a mut T> {
        match self {
            Node::Occupied(entry) => Some(entry.value_mut()),
            Node::Empty(_) => None,
        }
    }
}

impl<'a, T> From<&'a mut Point<T>> for Node<'a, T> {
    fn from(point: &'a mut Point<T>) -> Self {
        if point.value.is_some() {
            Node::Occupied(OccupiedNode { point })
        } else {
            Node::Empty(EmptyNode { point })
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum LBC {
    Equal,
    SelfPrefix,
    OtherPrefix,
    Common,
}

fn lbc(a: &[u8], b: &[u8]) -> (LBC, usize) {
    let shortest_len = cmp::min(a.len(), b.len());
    for i in 0..shortest_len {
        if a[i].eq_ignore_ascii_case(&b[i]) {
            continue;
        } else {
            return (LBC::Common, i);
        }
    }
    match a.len().cmp(&b.len()) {
        Ordering::Equal => (LBC::Equal, b.len()),
        Ordering::Less => (LBC::SelfPrefix, a.len()),
        Ordering::Greater => (LBC::OtherPrefix, b.len()),
    }
}

/// Returned by `NameTree::match_down`.
#[derive(Clone, Debug, PartialEq)]
pub enum MatchDown<'a, T> {
    /// `Name` isn't in scope.
    NotTree,
    /// `Name` does not exist.
    NX,
    /// `Name` exists but does not have a value.
    Empty,
    /// `Name` exists and has a value.
    Value(T),
    /// `Name` does not exist, a wildcard expansion returned an empty node.
    WildEmpty {
        /// Name of the wildcard node.
        name: &'a Name,
    },
    /// `Name` does not exist, a wildcard expansion returned a a name with a value.
    WildValue {
        /// Name of the wildcard node.
        name: &'a Name,
        /// Value of the wildcard node.
        value: T,
    },
}

#[doc = r#"
A tree mapping `Name`s to values.

`NameTree` is intended to be used in implementing zones and zone catalogs.

Empty nodes are implicitly inserted and removed:
```
use yardi::{name, nametree::{NameTree, MatchDown}};
let zone_apex = name!("example.com.");
let http_srv_owner = name!("_http._tcp.example.com.");
let intermediate_name = name!("_tcp.example.com.");
let mut nt: NameTree<()> = NameTree::new(zone_apex);
assert_eq!(nt.match_down(&intermediate_name), MatchDown::NX);
nt.insert(&http_srv_owner, ()).expect("in balliwick");
assert_eq!(nt.match_down(&intermediate_name), MatchDown::Empty);
```

Wildcards are followed in `match_down`:
```
use yardi::{name, nametree::{NameTree, MatchDown}};
let zone_apex = name!("example.com.");
let tame = name!("tame.example.com.");
let wild = name!("*.example.com.");
let www = name!("www.example.com.");
let mut nt: NameTree<&str> = NameTree::new(zone_apex);
nt.insert(&tame, "tame").expect("in balliwick");
nt.insert(&wild, "wild").expect("in balliwick");
assert_eq!(nt.match_down(&tame), MatchDown::Value(&"tame"));
assert_eq!(nt.match_down(&www), MatchDown::WildValue{name: &wild, value: &"wild"});
```

Iteration down a name's non-empty nodes is available via `descend`:
```
use yardi::{datatypes::{name, Name}, nametree::NameTree};
let zones = &[name!("example.com."),
              name!("mel.example.com."),
              name!("internal.staff.mel.example.com.")];
let qname = name!("www.internal.staff.mel.example.com.");
let mut nt: NameTree<()> = NameTree::new(Name::new());
for n in zones {
    nt.insert(n, ()).expect("in balliwick");
}
let visited = nt.descend(&qname)
                .map(|(n, ())| n)
                .collect::<Vec<_>>();
assert_eq!(visited, zones);
"#]
#[derive(Clone, Debug)]
pub struct NameTree<T> {
    inner: Point<T>,
}

impl<T> NameTree<T> {
    /// Construct a `NameTree` with `name` as the apex.
    pub fn new<N>(name: N) -> Self
    where
        N: Into<Name>,
    {
        let inner = Point::new(name.into(), None);
        NameTree { inner }
    }
    #[doc = r#"
Match down a name within the `NameTree`, expanding a single wildcard
if appropriate.

Wildcard expansion follows the rules outlined in [RFC 4592: The Role of Wildcards
in the Domain Name System](https://tools.ietf.org/html/rfc4592). The following
examples come from that RFC.

```
use yardi::{name, nametree::{NameTree, MatchDown}};
let owners = &[name!("example."),
               name!("*.example."),
               name!("sub.*.example."),
               name!("host1.example."),
               name!("_ssh._tcp.host1.example."),
               name!("_ssh._tcp.host2.example."),
               name!("subdel.example.")];
let mut nt: NameTree<()> = NameTree::new(name!("example."));
for o in owners {
    nt.insert(o, ()).expect("in balliwick");
}
// From section 2.2.1
assert_eq!(nt.match_down(&name!("host3.example.")),
           MatchDown::WildValue{name: &name!("*.example."), value: &()});
assert_eq!(nt.match_down(&name!("foo.bar.example.")),
           MatchDown::WildValue{name: &name!("*.example."), value: &()});
assert_eq!(nt.match_down(&name!("host1.example.")), MatchDown::Value(&()));
assert_eq!(nt.match_down(&name!("sub.*.example.")), MatchDown::Value(&()));
assert_eq!(nt.match_down(&name!("_telnet._tcp.host1.example.")), MatchDown::NX);
assert_eq!(nt.match_down(&name!("host.subdel.example.")), MatchDown::NX);
assert_eq!(nt.match_down(&name!("ghost.*.example.")), MatchDown::NX);
// From section 3.3.2
assert_eq!(nt.match_down(&name!("host3.example.")),
           MatchDown::WildValue{name: &name!("*.example."), value: &()});
assert_eq!(nt.match_down(&name!("_telnet._tcp.host1.example.")), MatchDown::NX);
assert_eq!(nt.match_down(&name!("_dns._udp.host2.example.")), MatchDown::NX);
assert_eq!(nt.match_down(&name!("_telnet._tcp.host3.example.")),
           MatchDown::WildValue{name: &name!("*.example."), value: &()});
assert_eq!(nt.match_down(&name!("_chat._udp.host3.example.")),
           MatchDown::WildValue{name: &name!("*.example."), value: &()});
assert_eq!(nt.match_down(&name!("foobar.*.example.")), MatchDown::NX);
```
"#]
    pub fn match_down(&self, name: &Name) -> MatchDown<'_, &T> {
        if !self.inner.name.is_suffix(name) {
            return MatchDown::NotTree;
        }
        let mut cursor = &self.inner;
        for l in name.labels().skip(self.inner.name.labels().count()) {
            if let Some(next) = cursor.get(l) {
                cursor = next;
            } else if let Some(wild) = cursor.get(b"*") {
                let name = &wild.name;
                return wild
                    .value
                    .as_ref()
                    .map(|value| MatchDown::WildValue { name, value })
                    .unwrap_or_else(|| MatchDown::WildEmpty { name });
            } else {
                return MatchDown::NX;
            }
        }
        debug_assert_eq!(&cursor.name, name);
        cursor
            .value
            .as_ref()
            .map(MatchDown::Value)
            .unwrap_or(MatchDown::Empty)
    }
    /// Get a reference to the value for `name`, if any.
    pub fn get(&self, name: &Name) -> Result<Option<&T>, NotTree> {
        if !self.inner.name.is_suffix(name) {
            return Err(NotTree);
        }
        let skip = self.inner.name.labels().count();
        let mut cursor = &self.inner;
        for l in name.labels().skip(skip) {
            if let Some(next) = cursor.get(l) {
                cursor = next;
            } else {
                return Ok(None);
            }
        }
        debug_assert_eq!(&cursor.name, name);
        Ok(cursor.value.as_ref())
    }
    /// Get a mutable reference to the value for `name`, if any.
    pub fn get_mut(&mut self, name: &Name) -> Result<Option<&mut T>, NotTree> {
        if !self.inner.name.is_suffix(name) {
            return Err(NotTree);
        }
        let skip = self.inner.name.labels().count();
        let mut cursor = &mut self.inner;
        for l in name.labels().skip(skip) {
            if let Ok(next) = cursor.get_mut(l) {
                cursor = next;
            } else {
                return Ok(None);
            }
        }
        debug_assert_eq!(&cursor.name, name);
        Ok(cursor.value.as_mut())
    }
    /// Inserts a node for `name` with the provided value, replacing any
    /// existing value.
    pub fn insert(&mut self, name: &Name, value: T) -> Result<&mut T, NotTree> {
        if !self.inner.name.is_suffix(name) {
            return Err(NotTree);
        }
        let labels = name.labels().skip(self.inner.name.labels().count());
        let mut cursor = &mut self.inner;
        for l in labels {
            cursor = cursor.get_or_insert(&l);
        }
        cursor.value = Some(value);
        Ok(cursor.value.as_mut().unwrap())
    }
    /// Gets a mutable reference to the value for `name`, inserting `value` if
    /// there is no existing value.
    pub fn get_or_insert(&mut self, name: &Name, value: T) -> Result<&mut T, NotTree> {
        if !self.inner.name.is_suffix(name) {
            return Err(NotTree);
        }
        let labels = name.labels().skip(self.inner.name.labels().count());
        let mut cursor = &mut self.inner;
        for l in labels {
            cursor = cursor.get_or_insert(&l);
        }
        Ok(cursor.value.get_or_insert(value))
    }
    /// Gets a mutable reference to the value for `name`, inserting the value
    /// produced by `f` if there is no existing value.
    pub fn get_or_insert_with<F>(&mut self, name: &Name, f: F) -> Result<&mut T, NotTree>
    where
        F: FnOnce() -> T,
    {
        if !self.inner.name.is_suffix(name) {
            return Err(NotTree);
        }
        let mut labels = name.labels().skip(self.inner.name.labels().count());
        let mut cursor = &mut self.inner;
        let value = loop {
            if let Some(l) = labels.next() {
                match cursor.get_mut(l) {
                    Ok(next) => cursor = next,
                    Err(cur) => {
                        let value = f();
                        cursor = cur.get_or_insert(l);
                        break value;
                    }
                }
            } else {
                return Ok(cursor.value.get_or_insert_with(f));
            }
        };
        for l in labels {
            cursor = cursor.get_or_insert(&l);
        }
        cursor.value = Some(value);
        Ok(cursor.value.as_mut().unwrap())
    }
    /// Removes the value for `name`.
    pub fn remove(&mut self, name: &Name) {
        if !self.inner.name.is_suffix(name) {
            return;
        }
        if self.inner.name == name {
            self.inner.value = None;
            return;
        }
        if let Some(ancestor) = self.remove_impl(name) {
            self.remove_cut(&ancestor);
        }
    }
    fn remove_impl<'a>(&mut self, name: &'a Name) -> Option<Name> {
        let target_label = name.labels().last()?;
        let parent = name.parent()?;
        let labels = parent
            .labels()
            .enumerate()
            .skip(self.inner.name.labels().count());
        let mut cut_at = None;
        let mut cursor = &mut self.inner;
        for (i, l) in labels {
            if !cursor.is_empty_single_point() {
                cut_at = Some(i + 1);
            }
            cursor = cursor.get_mut(l).ok()?;
        }
        if cursor.value.is_some() {
            cut_at = None;
        }
        let (has_single_child, does_match) = cursor
            .children
            .as_ref()
            .and_then(BranchOrPointBox::as_point)
            .map(|p| (true, p.label() == target_label))
            .unwrap_or((false, false));
        if has_single_child {
            if !does_match {
                return None;
            }
            if let Some(ancestor) = cut_at.and_then(|cut| name.cut(cut)) {
                return Some(ancestor);
            }
            cursor.children = None;
            return None;
        }
        cursor.children = cursor
            .children
            .take()
            .and_then(|bop| bop.prune(target_label));
        None
    }
    fn remove_cut(&mut self, name: &Name) {
        let mut cursor = &mut self.inner;
        let target_label = name.labels().last().expect("name shouldn't be root");
        let parent = name.parent().expect("name shouldn't be root");
        for l in parent.labels().skip(cursor.name.labels().count()) {
            if let Ok(next) = cursor.get_mut(l) {
                cursor = next;
            } else {
                unreachable!();
            }
        }
        cursor.children = cursor
            .children
            .take()
            .and_then(|bop| bop.prune(target_label));
    }
    /// Returns an iterator over all nodes.
    pub fn iter(&self) -> Iter<'_, T> {
        Iter::new(self)
    }
    /// Returns a mutable iterator over all nodes.
    pub fn iter_mut(&mut self) -> IterMut<'_, T> {
        IterMut::new(self)
    }
    /// Returns an iterator over the nodes between the `NameTree`'s root and the supplied name.
    pub fn descend<'b>(&self, name: &'b Name) -> Descend<'_, 'b, T> {
        let cut = self.inner.name.labels().count();
        let cursor = if self.inner.name.is_suffix(name) {
            Some(&self.inner)
        } else {
            None
        };
        Descend { cursor, name, cut }
    }
    /// Returns an iterator over mutable nodes between the `NameTree`'s root and the supplied name.
    pub fn descend_mut<'b>(&mut self, name: &'b Name) -> DescendMut<'_, 'b, T> {
        let cut = self.inner.name.labels().count();
        let cursor = if self.inner.name.is_suffix(name) {
            &mut self.inner as *mut Point<T>
        } else {
            ptr::null_mut()
        };
        DescendMut {
            p: PhantomData,
            cursor,
            name,
            cut,
        }
    }
}

/// An iterator over the non-empty nodes down a name in a `NameTree`.
pub struct Descend<'a, 'b, T> {
    cursor: Option<&'a Point<T>>,
    name: &'b Name,
    cut: usize,
}

impl<'a, 'b, T> Debug for Descend<'a, 'b, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let cursor = self.cursor.as_ref().map(|p| &p.name);
        let target = self.name;
        f.debug_struct("Descend")
            .field("cursor", &cursor)
            .field("target", &target)
            .finish()
    }
}

impl<'a, 'b, T> Iterator for Descend<'a, 'b, T> {
    type Item = (Name, &'a T);
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let cursor = self.cursor.take()?;
            let name = self.name.cut(self.cut)?;
            self.cursor = self.name.labels().nth(self.cut).and_then(|l| cursor.get(l));
            self.cut += 1;
            if let Some(value) = cursor.value.as_ref() {
                return Some((name, value));
            }
        }
    }
}

/// A mutable iterator over the non-empty nodes down a name in a `NameTree`.
pub struct DescendMut<'a, 'b, T> {
    p: PhantomData<&'a mut NameTree<T>>,
    cursor: *mut Point<T>,
    name: &'b Name,
    cut: usize,
}

impl<'a, 'b, T> Debug for DescendMut<'a, 'b, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let cursor = unsafe { self.cursor.as_ref().map(|p| &p.name) };
        let target = self.name;
        f.debug_struct("DescendMut")
            .field("cursor", &cursor)
            .field("target", &target)
            .finish()
    }
}

impl<'a, 'b, T> Iterator for DescendMut<'a, 'b, T> {
    type Item = (Name, &'a mut T);
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let name = self.name.cut(self.cut)?;
            let cursor = unsafe { self.cursor.as_mut()? };
            self.cursor = self
                .name
                .labels()
                .nth(self.cut)
                .and_then(|l| cursor.get_mut(l).ok().map(|p| p as *mut Point<T>))
                .unwrap_or_else(ptr::null_mut);
            self.cut += 1;
            if let Some(value) = cursor.value.as_mut() {
                return Some((name, value));
            }
        }
    }
}

/// An iterator over the nodes of a `NameTree`.
pub struct Iter<'a, T> {
    root: &'a NameTree<T>,
    cursor: Option<&'a Point<T>>,
    skip_below: bool,
}

impl<'a, T> Iter<'a, T> {
    fn new(root: &'a NameTree<T>) -> Self {
        Iter {
            root: root as _,
            cursor: None,
            skip_below: false,
        }
    }
    /// Skip nodes below the current node.
    pub fn skip_below(&mut self) {
        self.skip_below = true;
    }
}

impl<'a, T> Debug for Iter<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name: &Name = &self.root.inner.name;
        f.debug_struct("Iter").field("root", &name).finish()
    }
}

impl<'a, T> Iterator for Iter<'a, T> {
    type Item = (&'a Name, Option<&'a T>);
    fn next(&mut self) -> Option<Self::Item> {
        let cursor = if let Some(cursor) = self.cursor {
            cursor
        } else {
            let cursor = &self.root.inner;
            self.cursor = Some(cursor);
            let n: &Name = &cursor.name;
            let v = cursor.value.as_ref();
            return Some((n, v));
        };
        if self.skip_below {
            self.skip_below = false;
        } else if let Some(child) = cursor.children.as_ref().and_then(|p| p.first()) {
            self.cursor = Some(child);
            let n: &Name = &child.name;
            let v = child.value.as_ref();
            return Some((n, v));
        }
        let mut leaf_name: Name = cursor.name.clone();
        loop {
            if leaf_name == self.root.inner.name {
                return None;
            }
            let leaf_label = leaf_name.labels().last()?;
            let leaf_parent = leaf_name.parent()?;
            if let Some(sibling) = self
                .root
                .inner
                .seek(&leaf_parent)
                .and_then(|p| p.get_after(leaf_label))
            {
                self.cursor = Some(sibling);
                let n: &Name = &sibling.name;
                let v = sibling.value.as_ref();
                return Some((n, v));
            }
            leaf_name = leaf_parent;
        }
    }
}

/// A mutable iterator over the nodes of a `NameTree`.
pub struct IterMut<'a, T> {
    p: PhantomData<&'a mut NameTree<T>>,
    root: *mut NameTree<T>,
    cursor: *mut Point<T>,
    skip_below: bool,
}

impl<'a, T> IterMut<'a, T> {
    fn new(root: &'a mut NameTree<T>) -> Self {
        IterMut {
            p: PhantomData,
            root: root as _,
            cursor: ptr::null_mut(),
            skip_below: false,
        }
    }
    /// Skip nodes below the current node.
    pub fn skip_below(&mut self) {
        self.skip_below = true;
    }
}

impl<'a, T> Debug for IterMut<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let name: &Name = unsafe { &(*self.root).inner.name };
        f.debug_struct("IterMut").field("root", &name).finish()
    }
}

impl<'a, T> Iterator for IterMut<'a, T> {
    type Item = Node<'a, T>;
    fn next(&mut self) -> Option<Self::Item> {
        unsafe {
            if self.cursor.is_null() {
                let cursor = &mut (*self.root).inner;
                self.cursor = cursor as _;
                let item = cursor.into();
                return Some(item);
            }
            if self.skip_below {
                self.skip_below = false;
            } else if let Some(child) = (*self.cursor).children.as_mut().and_then(|p| p.first_mut())
            {
                self.cursor = child as _;
                let item = child.into();
                return Some(item);
            }
            let cursor = &mut (*self.cursor);
            let mut leaf_name: Name = cursor.name.clone();
            loop {
                if leaf_name == (*self.root).inner.name {
                    return None;
                }
                let leaf_label = leaf_name.labels().last()?;
                let leaf_parent = leaf_name.parent()?;
                if let Some(sibling) = (*self.root)
                    .inner
                    .seek_mut(&leaf_parent)
                    .and_then(|p| p.get_after_mut(leaf_label))
                {
                    self.cursor = sibling as _;
                    let item = sibling.into();
                    return Some(item);
                }
                leaf_name = leaf_name.parent()?;
            }
        }
    }
}

#[derive(Clone, Debug)]
enum BranchOrPointRef<'a, T> {
    Branch(&'a Branch<T>),
    Point(&'a Point<T>),
}

#[derive(Debug)]
enum BranchOrPointRefMut<'a, T> {
    Branch(&'a mut Branch<T>),
    Point(&'a mut Point<T>),
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum BranchOrPointTy {
    Branch,
    Point,
}

struct BranchOrPointBox<T> {
    p: PhantomData<T>,
    ptr: NonZeroUsize,
}

impl<T> BranchOrPointBox<T> {
    fn ty(&self) -> BranchOrPointTy {
        match TAG_MASK & self.ptr.get() {
            TAG_POINT => BranchOrPointTy::Point,
            TAG_BRANCH64 => BranchOrPointTy::Branch,
            TAG_BRANCH256 => BranchOrPointTy::Branch,
            _ => unreachable!(),
        }
    }
    fn from_branch(branch: Branch<T>) -> Self {
        unsafe { mem::transmute::<Branch<T>, BranchOrPointBox<T>>(branch) }
    }
    fn from_point(point: Point<T>) -> Self {
        unsafe {
            let b = Box::new(point);
            assert_eq!((b.as_ref() as *const Point<T> as usize) & TAG_MASK, 0);
            let raw = Box::into_raw(b) as usize;
            BranchOrPointBox {
                p: PhantomData,
                ptr: NonZeroUsize::new_unchecked(raw | TAG_POINT),
            }
        }
    }
    fn is_branch(&self) -> bool {
        match self.ptr.get() & TAG_MASK {
            TAG_POINT => false,
            TAG_BRANCH64 | TAG_BRANCH256 => true,
            _ => unreachable!(),
        }
    }
    fn is_point(&self) -> bool {
        self.ptr.get() & TAG_MASK == TAG_POINT
    }
    fn untagged_ptr(&self) -> usize {
        self.ptr.get() & !TAG_MASK
    }
    fn as_branch(&self) -> Option<&Branch<T>> {
        if self.is_branch() {
            unsafe { Some(self.as_branch_unchecked()) }
        } else {
            None
        }
    }
    unsafe fn as_branch_unchecked(&self) -> &Branch<T> {
        debug_assert!(self.is_branch());
        &*(self as *const BranchOrPointBox<T> as *const Branch<T>)
    }
    fn as_branch_mut(&mut self) -> Option<&mut Branch<T>> {
        if self.is_branch() {
            unsafe { Some(self.as_branch_mut_unchecked()) }
        } else {
            None
        }
    }
    unsafe fn as_branch_mut_unchecked(&mut self) -> &mut Branch<T> {
        debug_assert!(self.is_branch());
        &mut *(self as *mut BranchOrPointBox<T> as *mut Branch<T>)
    }
    fn as_point(&self) -> Option<&Point<T>> {
        if self.is_point() {
            Some(unsafe { self.as_point_unchecked() })
        } else {
            None
        }
    }
    unsafe fn as_point_unchecked(&self) -> &Point<T> {
        debug_assert!(self.is_point());
        &*(self.untagged_ptr() as *const Point<T>)
    }
    fn as_point_mut(&mut self) -> Option<&mut Point<T>> {
        if self.is_point() {
            Some(unsafe { self.as_point_mut_unchecked() })
        } else {
            None
        }
    }
    unsafe fn as_point_mut_unchecked(&mut self) -> &mut Point<T> {
        debug_assert!(self.is_point());
        &mut *(self.untagged_ptr() as *mut Point<T>)
    }
    pub fn first(&self) -> Option<&Point<T>> {
        let mut cursor = self;
        loop {
            match cursor.as_ref() {
                BranchOrPointRef::Point(point) => return Some(point),
                BranchOrPointRef::Branch(branch) => {
                    if let Some(point) = branch.point() {
                        return Some(point);
                    }
                    cursor = branch.first()?;
                }
            }
        }
    }
    pub fn first_mut(&mut self) -> Option<&mut Point<T>> {
        let mut cursor = self;
        loop {
            match cursor.as_mut() {
                BranchOrPointRefMut::Point(point) => return Some(point),
                BranchOrPointRefMut::Branch(branch) => {
                    if branch.point().is_some() {
                        return branch.point_mut();
                    }
                    cursor = branch.first_mut()?;
                }
            }
        }
    }
    fn get(&self, label: &[u8]) -> Option<&Point<T>> {
        let mut cur = self;
        loop {
            match cur.as_ref() {
                BranchOrPointRef::Branch(branch) => {
                    if let Some(next) = branch.get_label(label) {
                        cur = next;
                    } else {
                        return branch.point().filter(|p| p.label() == label);
                    }
                }
                BranchOrPointRef::Point(point) => {
                    if point.label() == label {
                        return Some(point);
                    }
                    return None;
                }
            }
        }
    }
    fn get_mut(&mut self, label: &[u8]) -> Option<&mut Point<T>> {
        let mut cursor = self;
        loop {
            cursor = match cursor.as_mut() {
                BranchOrPointRefMut::Branch(b) => {
                    if b.point().map(|p| p.label()) == Some(label) {
                        return b.point_mut();
                    }
                    b.get_label_mut(label)?
                }
                BranchOrPointRefMut::Point(p) => {
                    if p.label() == label {
                        return Some(p);
                    }
                    return None;
                }
            };
        }
    }
    fn prune(mut self, label: &[u8]) -> Option<Self> {
        if self.as_point().map(|p| p.label() == label).unwrap_or(false) {
            return None;
        }
        {
            let mut cursor = &mut self;
            loop {
                if let Some(mut replacement) = cursor.prune_replace(label) {
                    mem::swap(&mut replacement, cursor);
                    break;
                }
                match cursor.as_mut() {
                    BranchOrPointRefMut::Point(p) => {
                        assert_ne!(p.label(), label);
                        break;
                    }
                    BranchOrPointRefMut::Branch(b) => {
                        if let Some(next) = b.get_label_mut(label) {
                            cursor = next;
                        } else {
                            break;
                        }
                    }
                }
            }
        }
        Some(self)
    }
    fn prune_replace(&mut self, label: &[u8]) -> Option<Self> {
        match self.as_mut() {
            BranchOrPointRefMut::Point(p) => assert_ne!(p.label(), label),
            BranchOrPointRefMut::Branch(b) => {
                if b.remove_immediate_point(label) {
                    return b.take_last();
                }
            }
        }
        None
    }
    fn get_or_insert(&mut self, parent: &Name, label: &[u8]) -> &mut Point<T> {
        let (byte_cmp, byte_common, deep_label) = {
            let mut cursor = unsafe { &mut *(self as *mut BranchOrPointBox<T>) };
            loop {
                cursor = match cursor.as_mut() {
                    BranchOrPointRefMut::Branch(b) => b.get_for_insert_chase(label),
                    BranchOrPointRefMut::Point(p) => {
                        let (b_cmp, b_common) = lbc(label, p.label());
                        if b_cmp == LBC::Equal {
                            return p;
                        }
                        break (b_cmp, b_common, p.label());
                    }
                }
            }
        };
        let mut cursor = self;
        while let BranchOrPointTy::Branch = cursor.ty() {
            if let Some(key) = unsafe {
                cursor
                    .as_branch_unchecked()
                    .key_for_insert_mutate(label, byte_common)
            } {
                cursor = unsafe { cursor.as_branch_mut_unchecked().get_key_mut(key).unwrap() };
            } else {
                break;
            }
        }
        let new_point = {
            let mut n = parent.clone();
            n.push(label).unwrap();
            Point::new(n, None).into()
        };
        if cursor.ty() == BranchOrPointTy::Point {
            match byte_cmp {
                LBC::Equal => unreachable!(),
                LBC::Common => {
                    let dl_byte = deep_label.get(byte_common).cloned().unwrap();
                    let l_byte = label.get(byte_common).cloned().unwrap();
                    let diff = if dl_byte > l_byte {
                        dl_byte - l_byte
                    } else {
                        l_byte - dl_byte
                    };
                    let mut temp = Branch::new_width_hint(byte_common, diff).into();
                    mem::swap(&mut temp, cursor);
                    unsafe {
                        let branch = cursor.as_branch_mut_unchecked();
                        branch.insert_label(deep_label, temp);
                        branch
                            .insert_label(label, new_point)
                            .as_point_mut_unchecked()
                    }
                }
                LBC::OtherPrefix => {
                    let mut temp = Branch::new(byte_common).into();
                    mem::swap(&mut temp, cursor);
                    unsafe {
                        let branch = cursor.as_branch_mut_unchecked();
                        branch.set_point(temp);
                        branch
                            .insert_label(label, new_point)
                            .as_point_mut_unchecked()
                    }
                }
                LBC::SelfPrefix => {
                    let mut temp = Branch::new(byte_common).into();
                    mem::swap(&mut temp, cursor);
                    let branch = unsafe { cursor.as_branch_mut_unchecked() };
                    branch.insert_label(deep_label, temp);
                    branch.set_point(new_point)
                }
            }
        } else {
            let branch = unsafe { cursor.as_branch_mut_unchecked() };
            if branch.choice() != byte_common {
                let mut temp = Branch::new(byte_common);
                mem::swap(&mut temp, branch);
                branch.insert_label(deep_label, temp.into());
            }
            if label.get(byte_common).is_some() {
                return unsafe {
                    branch
                        .insert_label(label, new_point)
                        .as_point_mut_unchecked()
                };
            }
            branch.set_point(new_point)
        }
    }
    fn as_ref(&self) -> BranchOrPointRef<'_, T> {
        unsafe {
            match self.ty() {
                BranchOrPointTy::Branch => BranchOrPointRef::Branch(self.as_branch_unchecked()),
                BranchOrPointTy::Point => BranchOrPointRef::Point(self.as_point_unchecked()),
            }
        }
    }
    fn as_mut(&mut self) -> BranchOrPointRefMut<'_, T> {
        unsafe {
            match self.ty() {
                BranchOrPointTy::Branch => {
                    BranchOrPointRefMut::Branch(self.as_branch_mut_unchecked())
                }
                BranchOrPointTy::Point => BranchOrPointRefMut::Point(self.as_point_mut_unchecked()),
            }
        }
    }
}

impl<T> Clone for BranchOrPointBox<T>
where
    T: Clone,
{
    fn clone(&self) -> Self {
        match self.as_ref() {
            BranchOrPointRef::Branch(b) => b.clone().into(),
            BranchOrPointRef::Point(p) => p.clone().into(),
        }
    }
}

impl<T> Drop for BranchOrPointBox<T> {
    fn drop(&mut self) {
        unsafe {
            match self.ty() {
                BranchOrPointTy::Branch => {
                    mem::transmute::<usize, Branch<T>>(self.ptr.get());
                }
                BranchOrPointTy::Point => {
                    let raw = self.untagged_ptr() as *mut Point<T>;
                    drop(Box::from_raw(raw))
                }
            }
        }
    }
}

impl<T> Debug for BranchOrPointBox<T>
where
    T: Debug,
{
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.as_ref() {
            BranchOrPointRef::Branch(branch) => f.debug_tuple("Branch").field(branch).finish(),
            BranchOrPointRef::Point(point) => f.debug_tuple("Point").field(point).finish(),
        }
    }
}

impl<T> From<Branch<T>> for BranchOrPointBox<T> {
    fn from(branch: Branch<T>) -> Self {
        BranchOrPointBox::from_branch(branch)
    }
}

impl<T> From<Point<T>> for BranchOrPointBox<T> {
    fn from(point: Point<T>) -> Self {
        BranchOrPointBox::from_point(point)
    }
}
