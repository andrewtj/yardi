/// A macro for constructing a Name from a string at compile-time.
///
/// The string supplied must be a fully-qualified domain name
/// containing only ASCII bytes.
///
/// ```
/// # use yardi::name;
/// name!("example.com.");
/// ```
///
/// Non-ASCII values can be expressed using [RFC 1035](https://www.rfc-editor.org/rfc/rfc1035.html#section-5.1)'s `\DDD` syntax:
///
/// ```
/// # use yardi::name;
/// name!("\\123.");
/// // or with a raw literal
/// name!(r#"\123."#);
/// ```
///
/// ## Invalid input
///
/// Macro must be supplied a string:
/// ```compile_fail
/// # use yardi::name;
/// name!(b"example.com.");
/// ```
/// Unqualified:
/// ```compile_fail
/// # use yardi::name;
/// name!("example.com");
/// ```
/// Non-ASCII:
/// ```compile_fail
/// # use yardi::name;
/// name!("🤯.");
/// ```
/// DDD overflow:
/// ```compile_fail
/// # use yardi::name;
/// name!("\\256.");
/// ```
/// Invalid DDD:
/// ```compile_fail
/// # use yardi::name;
/// name!("\\1.");
/// ```
/// Label too long:
/// ```compile_fail
/// # use yardi::name;
/// name!("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijkl.");
/// ```
/// Name too long:
/// ```compile_fail
/// # use yardi::name;
/// name!("a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.y.z.a.b.c.d.e.f.g.h.i.j.k.l.m.n.o.p.q.r.s.t.u.v.w.x.");
/// ```
#[macro_export]
macro_rules! name {
    ($t:literal) => {{
        const BUF_LEN: usize = $crate::datatypes::name::nmh_len($t);
        const BUF: &'static [u8] = &{
            let mut buf = [0u8; BUF_LEN];
            let b = $t.as_bytes();
            let mut i = 0;
            let mut j = 0;
            let mut lab_off = j;
            j += 1;
            while i < b.len() && j < buf.len() {
                match b[i] {
                    b'.' => {
                        i += 1;
                        lab_off = j;
                        j += 1;
                    }
                    b'\\' => {
                        i += 1;
                        let (v, new_i) = $crate::datatypes::name::nmh_parse_escape(b, i);
                        buf[j] = v;
                        j += 1;
                        buf[lab_off] += 1;
                        i = new_i;
                    }
                    v @ b' '..=b'~' => {
                        buf[j] = v;
                        j += 1;
                        buf[lab_off] += 1;
                        i += 1;
                    }
                    _ => i = !0,
                }
            }
            buf
        };
        unsafe { $crate::datatypes::name::Name::from_static(BUF) }
    }};
}
