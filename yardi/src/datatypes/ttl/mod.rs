#[cfg(test)]
mod test;

use core::{
    cmp::Ordering,
    convert::TryInto,
    str::{self, FromStr},
    time::Duration,
};
#[cfg(feature = "std")]
use {
    core::ops::{Add, AddAssign, Sub, SubAssign},
    std::time::{Instant, SystemTime},
};

use crate::{
    ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter},
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};

/// Time to Live
///
/// [RFC 2181](https://tools.ietf.org/html/rfc2181#section-8
///            "RFC 2181: Clarifications to the DNS Specification")
/// defines Time to Live (TTL) as an unsigned 31-bit number. It goes on to
/// suggest that implementations treat TTL values with the most-significant bit
/// set as though the value recieved was zero. This implementation considers
/// these values invalid.
// TODO: RFC 8767 suggests treating them as unsigned, and using a max TTL of 7 days.
// TODO: ::lossy_from(_: Duration) ?
#[derive(Clone, Copy, Default, PartialEq, Eq, PartialOrd, Ord)]
pub struct Ttl(pub u32);

impl core::fmt::Debug for Ttl {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        self.0.fmt(f)
    }
}

// TODO: Associated constant on TTL instead.
const MAX: u32 = !0 >> 1;

impl From<Ttl> for Duration {
    fn from(ttl: Ttl) -> Self {
        Duration::from_secs(ttl.0.into())
    }
}

fn unpack_as_bytes(r: &mut Reader<'_>) -> Result<[u8; 4], UnpackError> {
    r.get_bytes_map(4, |s| {
        if s[0] & 0b1000_0000 != 0 {
            Err(UnpackError::value_invalid())
        } else {
            Ok(s.try_into().unwrap())
        }
    })
}

impl Parse for Ttl {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(Ttl::from_str)
    }
}

impl Present for Ttl {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        if self.0 > MAX {
            Err(PresentError::value_invalid())
        } else {
            self.0.present(p)
        }
    }
}

impl Pack for Ttl {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        if self.0 > MAX {
            Err(PackError::value_invalid())
        } else {
            self.0.pack(dst)
        }
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        if self.0 > MAX {
            Err(PackError::value_invalid())
        } else {
            self.0.packed_len()
        }
    }
}

impl<'a> Unpack<'a> for Ttl {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let t = u32::unpack(reader)?;
        if t > MAX {
            Err(UnpackError::value_invalid_at(reader.index() - 4))
        } else {
            Ok(Ttl(t))
        }
    }
}

impl UnpackedLen for Ttl {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        unpack_as_bytes(reader).map(|s| s.len())
    }
}

impl WireOrd for Ttl {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let a = unpack_as_bytes(left).map_err(WireOrdError::Left)?;
        let b = unpack_as_bytes(right).map_err(WireOrdError::Right)?;
        Ok(a.cmp(&b))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        u32::cmp_as_wire(&self.0, &other.0)
    }
}

impl FromStr for Ttl {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        crate::datatypes::int::parse_u32(s).and_then(|u| {
            if u > MAX {
                Err(ParseError::value_large())
            } else {
                Ok(Ttl(u))
            }
        })
    }
}

#[cfg(feature = "std")]
impl Add<Ttl> for Instant {
    type Output = <Instant as Add<Duration>>::Output;
    fn add(self, ttl: Ttl) -> Self::Output {
        self.add(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl AddAssign<Ttl> for Instant {
    fn add_assign(&mut self, ttl: Ttl) {
        self.add_assign(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl Sub<Ttl> for Instant {
    type Output = <Instant as Sub<Duration>>::Output;
    fn sub(self, ttl: Ttl) -> Self::Output {
        self.sub(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl SubAssign<Ttl> for Instant {
    fn sub_assign(&mut self, ttl: Ttl) {
        self.sub_assign(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl Add<Ttl> for SystemTime {
    type Output = <SystemTime as Add<Duration>>::Output;
    fn add(self, ttl: Ttl) -> Self::Output {
        self.add(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl AddAssign<Ttl> for SystemTime {
    fn add_assign(&mut self, ttl: Ttl) {
        self.add_assign(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl Sub<Ttl> for SystemTime {
    type Output = <SystemTime as Sub<Duration>>::Output;
    fn sub(self, ttl: Ttl) -> Self::Output {
        self.sub(Duration::from(ttl))
    }
}

#[cfg(feature = "std")]
impl SubAssign<Ttl> for SystemTime {
    fn sub_assign(&mut self, ttl: Ttl) {
        self.sub_assign(Duration::from(ttl))
    }
}
