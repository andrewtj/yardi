use super::Ttl;
use crate::wire::{Pack, Reader, Unpack, Writer};
#[cfg(not(feature = "std"))]
use alloc::string::{String, ToString};
use core::str::FromStr;

const BAD_TTL: Ttl = Ttl(1 << 31);

#[test]
fn from_str() {
    assert!(Ttl::from_str("").is_err());
    assert!(Ttl::from_str("1a").is_err());
}

#[test]
fn bad_wire() {
    {
        let mut r = Reader::new([0, 0, 0, 0][..].into());
        assert!(Ttl::unpack(&mut r).is_ok());
    }
    {
        let mut buf = [0u8; 4];
        let mut w = Writer::new(&mut buf[..]);
        assert!(Ttl(0).pack(&mut w).is_ok());
    }
    {
        let mut r = Reader::new([0b1000_0000, 0, 0, 0][..].into());
        assert!(Ttl::unpack(&mut r).is_err());
    }
    {
        let mut buf = [0u8; 4];
        let mut w = Writer::new(&mut buf[..]);
        assert!(BAD_TTL.pack(&mut w).is_err());
    }
}

#[test]
fn bad_ascii() {
    let bad_ttl_str = BAD_TTL.0.to_string();
    assert!(Ttl::from_str(&bad_ttl_str).is_err());
    let mut temp = String::new();
    assert!(crate::ascii::present::to_string(&BAD_TTL, &mut temp).is_err());
}
