use crate::ascii::present;
use crate::rdata::Nid;
#[cfg(not(feature = "std"))]
use alloc::string::String;
use core::str::FromStr;

#[test]
fn ilnp_64_ascii() {
    let cases: &[&str] = &["0 94:198:152:169", "0 14:4FFF:FF20:EE64"];
    for &input in cases {
        let nid = Nid::from_str(input).expect("nid");
        let mut output = String::with_capacity(input.len());
        present::to_string(&nid, &mut output).expect("present");
        assert_eq!(input, output);
    }
}
