#[cfg(test)]
mod test;

use core::net::Ipv4Addr;

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};

pub(crate) fn parse_locator32(p: &mut Parser<'_, '_>) -> Result<[u8; 4], ParseError> {
    Ipv4Addr::parse(p).map(|ip| ip.octets())
}

pub(crate) fn present_locator32(
    l32: &[u8; 4],
    p: &mut Presenter<'_, '_>,
) -> Result<(), PresentError> {
    Ipv4Addr::from(*l32).present(p)
}

pub(crate) fn parse64(p: &mut Parser<'_, '_>) -> Result<[u8; 8], ParseError> {
    p.pull(|s| {
        let mut node_id = [0u8; 8];
        if s.len() > 19 {
            return Err(ParseError::value_invalid());
        }
        let mut g_bytes_iter = node_id.chunks_mut(2);
        for g_str in s.as_bytes().split(|&b| b == b':') {
            let g_bytes = g_bytes_iter.next().ok_or_else(ParseError::value_invalid)?;
            if g_str.len() > 4 {
                return Err(ParseError::value_invalid());
            }
            let mut g_str_filled = [b'0'; 4];
            for (s, t) in g_str
                .iter()
                .copied()
                .rev()
                .zip(g_str_filled.iter_mut().rev())
            {
                *t = s;
            }
            hex::decode_to_slice(g_str_filled, g_bytes).map_err(|_| ParseError::value_invalid())?;
        }
        if g_bytes_iter.next().is_some() {
            Err(ParseError::value_invalid())
        } else {
            Ok(node_id)
        }
    })
}

pub(crate) fn present64(s: &[u8; 8], p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    let mut ip = p.incremental();
    for (i, g) in s.chunks(2).enumerate() {
        let p = (i > 0).then_some(":").unwrap_or_default();
        let g = u16::from_be_bytes([g[0], g[1]]);
        ip.write_fmt(format_args!("{p}{g:X}"))?;
    }
    Ok(())
}
