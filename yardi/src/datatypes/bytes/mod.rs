#[cfg(test)]
mod test;

use crate::{
    ascii::{
        parse::Pull,
        {ParseError, Parser, PresentError, Presenter},
    },
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};
use base64::{
    engine::general_purpose::{STANDARD as B64_STD, STANDARD_NO_PAD as B64_NO_PAD},
    Engine as _,
};
use core::{
    cmp::Ordering,
    convert::TryInto,
    fmt::{self, Debug},
};

pub(crate) struct DebugFmt<'a>(pub(crate) &'a [u8]);

impl<'a> Debug for DebugFmt<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "b\"")?;
        for &b in self.0 {
            match b {
                b'\t' => write!(f, "\\t")?,
                b'\n' => write!(f, "\\n")?,
                b'\r' => write!(f, "\\r")?,
                b'\\' | b'"' => write!(f, "\\{}", b as char)?,
                b' '..=b'~' => write!(f, "{}", b as char)?,
                _ => write!(f, "\\x{b:02X}")?,
            }
        }
        write!(f, "\"")
    }
}

fn is_ascii_ld(b: u8) -> bool {
    matches!(b,
        b'0'..=b'9' | b'A'..=b'Z' | b'a'..=b'z')
}

pub(crate) fn parse_ascii_ld<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    p.pull(|s| {
        let s = s.as_bytes();
        if s.len() < min {
            Err(ParseError::value_small())
        } else if s.len() > max {
            Err(ParseError::value_large())
        } else if s.iter().cloned().all(is_ascii_ld) {
            Ok(s.into())
        } else {
            Err(ParseError::value_invalid())
        }
    })
}

pub(crate) fn present_ascii_ld<T: AsRef<[u8]>>(
    src: T,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    match src.len() {
        len if len < min => Err(PresentError::value_small()),
        len if len > max => Err(PresentError::value_large()),
        _ if src.iter().cloned().all(is_ascii_ld) => {
            let s = unsafe { core::str::from_utf8_unchecked(src) };
            p.write(s)
        }
        _ => Err(PresentError::value_invalid()),
    }
}

pub(crate) fn parse_charstr<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    let mut tmp = [0u8; 0xFFFF];
    p.pull(|s| {
        crate::encoding::parse_charstr(s, &mut tmp).and_then(|s| {
            if s.len() < min {
                Err(ParseError::value_small())
            } else if s.len() > max {
                Err(ParseError::value_large())
            } else {
                Ok(s.into())
            }
        })
    })
}

pub(crate) fn present_charstr<T: AsRef<[u8]>>(
    src: T,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    match src.len() {
        len if len < min => Err(PresentError::value_small()),
        len if len > max => Err(PresentError::value_large()),
        _ => crate::encoding::present_charstr(src, p),
    }
}

pub(crate) fn parse_base32hexnp<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    fn decode_byte(b: u8) -> Result<u8, ParseError> {
        match b {
            b'0'..=b'9' => Ok(b - b'0'),
            b'A'..=b'V' => Ok(b - b'A' + 10),
            b'a'..=b'v' => Ok(b - b'a' + 10),
            _ => Err(ParseError::value_invalid())?,
        }
    }
    p.pull(|s| {
        let tmp = &mut [0u8; 0xFFFF][..0xFFFF.min(max)];
        // Reject strings containing bytes that aren't used.
        if let 1 | 3 | 6 = s.len() % 8 {
            return Err(ParseError::value_invalid());
        }
        let mut rest = &mut tmp[..];
        for s in s.as_bytes().chunks(8) {
            let mut i = 0;
            if s.len() >= 2 {
                let tmp_chunk = rest.get_mut(..2).ok_or_else(ParseError::value_large)?;
                // 0b1111_1000
                tmp_chunk[0] = decode_byte(s[0])? << 3;
                let b = decode_byte(s[1])?;
                // 0b1111_1111
                tmp_chunk[0] |= b >> 2;
                // 0b1100_0000
                tmp_chunk[1] = b << 6;
                i += 1;
            }
            if s.len() >= 4 {
                let tmp_chunk = rest.get_mut(..3).ok_or_else(ParseError::value_large)?;
                // 0b1111_1110
                tmp_chunk[1] |= decode_byte(s[2])? << 1;
                let b = decode_byte(s[3])?;
                // 0b1111_1111
                tmp_chunk[1] |= b >> 4;
                // 0b1111_0000
                tmp_chunk[2] = b << 4;
                i += 1;
            }
            if s.len() >= 5 {
                let tmp_chunk = rest.get_mut(..4).ok_or_else(ParseError::value_large)?;
                let b = decode_byte(s[4])?;
                // 0b1111_1111
                tmp_chunk[2] |= b >> 1;
                // 0b1000_0000
                tmp_chunk[3] = b << 7;
                i += 1;
            }
            if s.len() >= 7 {
                let tmp_chunk = rest.get_mut(..5).ok_or_else(ParseError::value_large)?;
                // 0b1111_1100
                tmp_chunk[3] |= decode_byte(s[5])? << 2;
                let b = decode_byte(s[6])?;
                // 0b1111_1111
                tmp_chunk[3] |= b >> 3;
                // 0b1110_0000
                tmp_chunk[4] |= b << 5;
                i += 1;
            }
            if s.len() >= 8 {
                let tmp_chunk = rest.get_mut(..5).ok_or_else(ParseError::value_large)?;
                // 0b1111_1111
                tmp_chunk[4] |= decode_byte(s[7])?;
                i += 1;
            }
            rest = &mut rest[i..];
        }
        let rest = rest.len();
        let tmp = &tmp[..tmp.len() - rest];
        if tmp.len() < min {
            Err(ParseError::value_small())
        } else {
            Ok(tmp.into())
        }
    })
}

pub(crate) fn present_base32hexnp(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    match src.len() {
        len if len < min => return Err(PresentError::value_small()),
        len if len > max => return Err(PresentError::value_large()),
        _ => {}
    }
    let mut ip = p.incremental();
    for c in src.chunks(5) {
        let mut temp = [0u8; 8];
        let mut w = 0;
        if c.len() >= 1 {
            // 0b11111
            temp[0] = c[0] >> 3;
            // 0b11100
            temp[1] = (0b111 & c[0]) << 2;
            w = 2;
        }
        if c.len() >= 2 {
            // 0bPPP11
            temp[1] |= c[1] >> 6;
            // 0b11111
            temp[2] = (0b0011_1110 & c[1]) >> 1;
            // 0b10000
            temp[3] = (0b1 & c[1]) << 4;
            w = 4;
        }
        if c.len() >= 3 {
            // 0bP1111
            temp[3] |= c[2] >> 4;
            // 0b11110
            temp[4] = (0b1111 & c[2]) << 1;
            w = 5;
        }
        if c.len() >= 4 {
            // 0bPPPP1
            temp[4] |= c[3] >> 7;
            // 0b11111
            temp[5] = (0b0111_1100 & c[3]) >> 2;
            // 0b11000
            temp[6] = (0b0000_0011 & c[3]) << 3;
            w = 7;
        }
        if c.len() >= 5 {
            // 0bPP111
            temp[6] |= c[4] >> 5;
            // 0b11111
            temp[7] = 0b11111 & c[4];
            w = 8;
        }
        for b in temp[..w].iter_mut() {
            *b += [b'0', b'a' - 10][usize::from(*b > 9)];
        }
        let s = if cfg!(debug_assertions) {
            core::str::from_utf8(&temp[..w]).unwrap()
        } else {
            unsafe { core::str::from_utf8_unchecked(&temp[..w]) }
        };
        ip.write(s)?;
    }
    Ok(())
}

pub(crate) fn parse_nsec3_salt<T>(p: &mut Parser<'_, '_>) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    p.pull(|s| {
        if s == "-" {
            return Ok([][..].into());
        }
        if s.len() > 2 * 0xFF {
            let hex = s.as_bytes().iter().all(u8::is_ascii_hexdigit);
            return Err(if hex {
                ParseError::value_large()
            } else {
                ParseError::value_invalid()
            });
        }
        let mut tmp = [0u8; 0xFF];
        let tmp = &mut tmp[..s.len() / 2];
        hex::decode_to_slice(s, tmp).map_err(|_| ParseError::value_invalid())?;
        Ok(tmp[..].into())
    })
}

pub(crate) fn present_nsec3_salt(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    if src.is_empty() {
        p.write("-")
    } else {
        present_hex(src, p, 0, 0xFF)
    }
}

pub(crate) fn parse_nsap_hex<T>(p: &mut Parser<'_, '_>) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    p.pull(|s| {
        let s = s.as_bytes();
        if s.len() < 2 || s[0] != b'0' || !matches!(s[1], b'x' | b'X') {
            return Err(ParseError::value_invalid());
        }
        let temp = &mut [0u8; 0xFFFF];
        let mut octets = &mut temp[..];
        let mut left_over = None;
        for s in s[2..].split(|&b| b == b'.') {
            let s = if let Some(lob) = left_over {
                let Some((&b, rest)) = s.split_first() else {
                    continue;
                };
                let input = [lob, b];
                let (head, tail) = octets
                    .split_first_mut()
                    .ok_or_else(ParseError::value_large)?;
                hex::decode_to_slice(input, core::slice::from_mut(head))
                    .map_err(|_| ParseError::value_invalid())?;
                octets = tail;
                rest
            } else {
                s
            };
            let take = s.len() & !1;
            let decoded = take / 2;
            if octets.len() < decoded {
                return Err(ParseError::value_large());
            }
            let (head, tail) = octets.split_at_mut(decoded);
            octets = tail;
            hex::decode_to_slice(&s[..take], head).map_err(|_| ParseError::value_invalid())?;
            left_over = s.get(take).copied();
        }
        if left_over.is_some() {
            return Err(ParseError::value_invalid());
        }
        let octets = octets.len();
        let temp = &temp[..temp.len() - octets];
        if temp.is_empty() {
            Err(ParseError::value_small())
        } else {
            Ok(temp.into())
        }
    })
}

pub(crate) fn present_nsap_hex(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    match src.len() {
        0 => return Err(PresentError::value_small()),
        len if len > 0xFFFF => return Err(PresentError::value_large()),
        _ => (),
    }
    let mut ip = p.incremental();
    ip.write("0x")?;
    let mut output = [0u8; 4096];
    for input in src.chunks(output.len() / 2) {
        let output = &mut output[..input.len() * 2];
        hex::encode_to_slice(input, output).unwrap();
        for b in output.iter_mut() {
            *b = b.to_ascii_uppercase();
        }
        ip.write(unsafe { core::str::from_utf8_unchecked(output) })?;
    }
    Ok(())
}

pub(crate) fn parse_base64<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    p.pull(|s| {
        let temp = &mut [0u8; 0xFFFF];
        let temp_cap = temp.len().min(max);
        let temp = &mut temp[..temp_cap];
        match B64_STD.decode_slice(s.as_bytes(), temp) {
            Ok(len) if len < min => Err(ParseError::value_small()),
            Ok(len) => Ok(temp[..len].into()),
            Err(base64::DecodeSliceError::DecodeError(_)) => Err(ParseError::value_invalid()),
            Err(base64::DecodeSliceError::OutputSliceTooSmall) => Err(ParseError::value_large()),
        }
    })
}

struct Base64WhitespaceDecoder<'a> {
    buf: [u8; Base64WhitespaceDecoder::BUF_CAP],
    buf_len: usize,
    out: &'a mut [u8],
    out_len: usize,
}
impl<'a> Base64WhitespaceDecoder<'a> {
    const BUF_CAP: usize = 170 * 24;
    fn new(out: &'a mut [u8]) -> Self {
        Self {
            buf: [0u8; Base64WhitespaceDecoder::BUF_CAP],
            buf_len: 0,
            out,
            out_len: 0,
        }
    }
    fn push(&mut self, input: &str) -> Result<(), ParseError> {
        let mut input = input.as_bytes();
        while !input.is_empty() {
            let (buf_used, buf_rest) = self.buf.split_at_mut(self.buf_len);
            if buf_rest.is_empty() {
                let out_rest = &mut self.out[self.out_len..];
                if out_rest.is_empty() {
                    return Err(ParseError::value_large());
                }
                self.out_len += B64_NO_PAD
                    .decode_slice(buf_used, out_rest)
                    .map_err(|_| ParseError::value_invalid())?;
                self.buf_len = 0;
            } else {
                let (input_consume, input_rest) = input.split_at(input.len().min(buf_rest.len()));
                buf_rest[..input_consume.len()].copy_from_slice(input_consume);
                self.buf_len += input_consume.len();
                input = input_rest;
            }
        }
        Ok(())
    }
    fn finish(mut self) -> Result<&'a [u8], ParseError> {
        if self.buf_len > 0 {
            let out_rest = &mut self.out[self.out_len..];
            if out_rest.is_empty() {
                return Err(ParseError::value_large());
            }
            self.out_len += B64_STD
                .decode_slice(&self.buf[..self.buf_len], out_rest)
                .map_err(|_| ParseError::value_invalid())?;
        }
        Ok(&self.out[..self.out_len])
    }
}

pub(crate) fn parse_base64ws<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    let out_buf = &mut [0u8; 0xFFFF];
    let out_cap = out_buf.len().min(max);
    let mut adapter = Base64WhitespaceDecoder::new(&mut out_buf[..out_cap]);
    p.pull_rest(|s| adapter.push(s))?;
    let out_buf = adapter.finish()?;
    if out_buf.len() < min {
        Err(ParseError::value_small())
    } else {
        Ok(out_buf.into())
    }
}

pub(crate) fn present_base64(
    src: &[u8],
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    match src.len() {
        len if len < min => Err(PresentError::value_small()),
        len if len > max => Err(PresentError::value_large()),
        _ => {
            let display = base64::display::Base64Display::new(src, &B64_STD);
            p.write_fmt(format_args!("{}", display))
        }
    }
}

pub(crate) fn present_base64ws(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    present_base64(src.as_ref(), p, min, max)
}

pub(crate) fn parse_base64wsdash<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    let empty = p.pull(|s| {
        if s == "-" {
            Pull::Accept(true)
        } else {
            Pull::Decline(false)
        }
    })?;
    if empty {
        return Ok([][..].into());
    }
    parse_base64ws(p, min, max)
}

pub(crate) fn present_base64wsdash(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    if src.is_empty() {
        return p.write("-");
    }
    present_base64ws(src, p, min, max)
}

pub(crate) fn parse_hex<T>(p: &mut Parser<'_, '_>, min: usize, max: usize) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    p.pull(|s| {
        let octets = &mut [0u8; 0xFFFF];
        let octets = octets
            .get_mut(..s.len() / 2)
            .ok_or_else(ParseError::value_large)?;
        hex::decode_to_slice(s, octets).map_err(|_| ParseError::value_invalid())?;
        if octets.len() < min {
            Err(ParseError::value_small())
        } else if octets.len() > max {
            Err(ParseError::value_large())
        } else {
            Ok(octets[..].into())
        }
    })
}

pub(crate) fn parse_hexws<T>(
    p: &mut Parser<'_, '_>,
    min: usize,
    max: usize,
) -> Result<T, ParseError>
where
    T: for<'a> From<&'a [u8]>,
{
    let mut left_over: Option<u8> = None;
    let temp = &mut [0u8; 0xFFFF];
    let temp = temp.get_mut(..max).ok_or_else(ParseError::value_large)?;
    let mut len = 0;
    p.pull_rest(|s| {
        let mut s = s.as_bytes();
        let mut temp = &mut temp[len..];
        if let Some(lob) = left_over {
            let Some((&b, rest)) = s.split_first() else {
                return Ok(());
            };
            let input = [lob, b];
            let (head, tail) = temp.split_first_mut().ok_or_else(ParseError::value_large)?;
            hex::decode_to_slice(input, core::slice::from_mut(head))
                .map_err(|_| ParseError::value_invalid())?;
            s = rest;
            temp = tail;
            len += 1;
            left_over = None;
        }
        let take = s.len() & !1;
        let temp = temp
            .get_mut(..take / 2)
            .ok_or_else(ParseError::value_large)?;
        hex::decode_to_slice(&s[..take], temp).map_err(|_| ParseError::value_invalid())?;
        left_over = s.get(take + 1).copied();
        len += temp.len();
        Ok(())
    })?;
    if left_over.is_some() {
        return Err(ParseError::value_invalid());
    }
    let bytes = &temp[..len];
    if bytes.len() < min {
        Err(ParseError::value_small())
    } else if bytes.len() > max {
        Err(ParseError::value_large())
    } else {
        Ok(bytes.into())
    }
}

pub(crate) fn present_hex(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    let src = src.as_ref();
    match src.len() {
        len if len < min => Err(PresentError::value_small()),
        len if len > max => Err(PresentError::value_large()),
        _ => {
            let mut p = p.incremental();
            let mut output = [0u8; 4096];
            for input in src.chunks(output.len() / 2) {
                let output = &mut output[..input.len() * 2];
                hex::encode_to_slice(input, output).unwrap();
                for b in output.iter_mut() {
                    *b = b.to_ascii_uppercase();
                }
                p.write(unsafe { core::str::from_utf8_unchecked(output) })?;
            }
            Ok(())
        }
    }
}

pub(crate) fn present_hexws(
    src: impl AsRef<[u8]>,
    p: &mut Presenter<'_, '_>,
    min: usize,
    max: usize,
) -> Result<(), PresentError> {
    present_hex(src.as_ref(), p, min, max)
}

macro_rules! wire_helpers {
    (
        $prefix:expr,
        $get:ident,
        $write:ident,
        $pack:ident,
        $pack_len:ident,
        $unpack:ident,
        $unpacked_len:ident
    ) => {
        pub(crate) fn $pack(
            src: impl AsRef<[u8]>,
            dst: &mut Writer<'_>,
            min: usize,
            max: usize,
        ) -> Result<(), PackError> {
            let src = src.as_ref();
            match src.len() {
                len if len < min => Err(PackError::value_small()),
                len if len > max => Err(PackError::value_large()),
                _ => dst.$write(src),
            }
        }

        pub(crate) fn $pack_len(
            src: impl AsRef<[u8]>,
            min: usize,
            max: usize,
        ) -> Result<usize, PackError> {
            let src = src.as_ref();
            match src.len() {
                len if len < min => Err(PackError::value_small()),
                len if len > max => Err(PackError::value_large()),
                len => len.checked_add($prefix).ok_or_else(PackError::value_large),
            }
        }

        pub(crate) fn $unpack<'a, T>(
            reader: &mut Reader<'a>,
            min: usize,
            max: usize,
        ) -> Result<T, UnpackError>
        where
            T: From<&'a [u8]>,
        {
            let initial_index = reader.index();
            let tmp = reader.$get()?;
            match tmp.len() {
                len if len < min => Err(UnpackError::value_small_at(initial_index)),
                len if len > max => Err(UnpackError::value_large_at(initial_index)),
                _ => Ok(tmp.into()),
            }
        }

        pub(crate) fn $unpacked_len(
            reader: &mut Reader<'_>,
            min: usize,
            max: usize,
        ) -> Result<usize, UnpackError> {
            let initial_index = reader.index();
            reader.$get().and_then(|s| match s.len() {
                len if len < min => Err(UnpackError::value_small_at(initial_index)),
                len if len > max => Err(UnpackError::value_large_at(initial_index)),
                len => len
                    .checked_add($prefix)
                    .ok_or_else(|| UnpackError::value_large_at(initial_index)),
            })
        }
    };
}

wire_helpers!(
    0,
    get_bytes_rest,
    write_bytes,
    pack,
    packed_len,
    unpack,
    unpacked_len
);
wire_helpers!(
    1,
    get_bytes8,
    write_bytes8,
    pack8,
    packed8_len,
    unpack8,
    unpacked8_len
);
wire_helpers!(
    2,
    get_bytes16,
    write_bytes16,
    pack16,
    packed16_len,
    unpack16,
    unpacked16_len
);

#[derive(Clone, Copy, Debug)]
struct ValueInvalid;

impl From<ValueInvalid> for ParseError {
    fn from(_: ValueInvalid) -> Self {
        ParseError::value_invalid()
    }
}

impl From<ValueInvalid> for PackError {
    fn from(_: ValueInvalid) -> Self {
        PackError::value_invalid()
    }
}

impl From<ValueInvalid> for UnpackError {
    fn from(_: ValueInvalid) -> Self {
        UnpackError::value_invalid()
    }
}

fn is_ld<T, E>(t: T) -> Result<T, E>
where
    T: AsRef<[u8]>,
    E: From<ValueInvalid>,
{
    let accept = |b: u8| b.is_ascii_digit() || b.is_ascii_alphabetic();
    if t.as_ref().iter().copied().all(accept) {
        Ok(t)
    } else {
        Err(ValueInvalid.into())
    }
}

fn unpack_ld_check<T>(t: T) -> Result<T, UnpackError>
where
    T: AsRef<[u8]>,
{
    is_ld(t)
}

pub(crate) fn pack8_ld(
    src: impl AsRef<[u8]>,
    dst: &mut Writer<'_>,
    min: usize,
    max: usize,
) -> Result<(), PackError> {
    let src = src.as_ref();
    match src.len() {
        len if len < min => Err(PackError::value_small()),
        len if len > max => Err(PackError::value_large()),
        _ => dst.write_bytes8(src),
    }
}

pub(crate) fn packed8_len_ld(
    src: impl AsRef<[u8]>,
    min: usize,
    max: usize,
) -> Result<usize, PackError> {
    let src = src.as_ref();
    match src.len() {
        len if len < min => Err(PackError::value_small()),
        len if len > max => Err(PackError::value_large()),
        len => is_ld(src).map(|_| 1 + len),
    }
}

pub(crate) fn unpack8_ld<'a, T>(
    reader: &mut Reader<'a>,
    min: usize,
    max: usize,
) -> Result<T, UnpackError>
where
    T: From<&'a [u8]>,
{
    let s = reader.get_bytes8()?;
    match s.len() {
        len if len < min => Err(UnpackError::value_small()),
        len if len > max => Err(UnpackError::value_large()),
        _ => unpack_ld_check(s).map(Into::into),
    }
}

pub(crate) fn unpacked8_len_ld(
    reader: &mut Reader<'_>,
    min: usize,
    max: usize,
) -> Result<usize, UnpackError> {
    match reader.get_byte()? as usize {
        len if len < min => Err(UnpackError::value_small_at(reader.index() - 1)),
        len if len > max => Err(UnpackError::value_large_at(reader.index() - 1)),
        len => reader.get_bytes_map(len, |s| unpack_ld_check(s).map(|_| 1 + s.len())),
    }
}

pub(crate) fn cmp_fixed(
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
    n: usize,
) -> Result<Ordering, WireOrdError> {
    let l = left.peek(n).map_err(WireOrdError::Left)?;
    let r = right.peek(n).map_err(WireOrdError::Right)?;
    let order = l.cmp(r);
    left.skip(n).unwrap();
    right.skip(n).unwrap();
    Ok(order)
}

pub(crate) fn cmp(
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
    min: usize,
    max: usize,
) -> Result<Ordering, WireOrdError> {
    let l_initial_index = left.index();
    let l = left
        .get_bytes_rest()
        .and_then(|b| match b.len() {
            l if l < min => Err(UnpackError::value_small_at(l_initial_index)),
            l if l > max => Err(UnpackError::value_large_at(l_initial_index)),
            _ => Ok(b),
        })
        .map_err(WireOrdError::Left)?;
    let r_initial_index = right.index();
    let r = right
        .get_bytes_rest()
        .and_then(|b| match b.len() {
            l if l < min => Err(UnpackError::value_small_at(r_initial_index)),
            l if l > max => Err(UnpackError::value_large_at(r_initial_index)),
            _ => Ok(b),
        })
        .map_err(WireOrdError::Right)?;
    Ok(l.cmp(r))
}

pub(crate) fn cmp8(
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
    min: usize,
    max: usize,
) -> Result<Ordering, WireOrdError> {
    let l_len = left
        .get_byte()
        .and_then(|b| match b as usize {
            l if l < min => Err(UnpackError::value_small_at(left.index() - 1)),
            l if l > max => Err(UnpackError::value_large_at(left.index() - 1)),
            _ => Ok(b),
        })
        .map_err(WireOrdError::Left)?;
    let r_len = right
        .get_byte()
        .and_then(|b| match b as usize {
            l if l < min => Err(UnpackError::value_small_at(right.index() - 1)),
            l if l > max => Err(UnpackError::value_large_at(right.index() - 1)),
            _ => Ok(b),
        })
        .map_err(WireOrdError::Right)?;
    Ok(match l_len.cmp(&r_len) {
        Ordering::Equal => {
            let l_bytes = left.peek(l_len as usize).map_err(WireOrdError::Left)?;
            let r_bytes = right.peek(r_len as usize).map_err(WireOrdError::Right)?;
            let order = l_bytes.cmp(r_bytes);
            left.skip(l_len as usize).unwrap();
            right.skip(r_len as usize).unwrap();
            order
        }
        other => other,
    })
}

pub(crate) fn cmp8_ld(
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
    min: usize,
    max: usize,
) -> Result<Ordering, WireOrdError> {
    let l_len = left
        .get_byte()
        .and_then(|b| match b as usize {
            l if l < min => Err(UnpackError::value_small_at(left.index() - 1)),
            l if l > max => Err(UnpackError::value_large_at(left.index() - 1)),
            _ => Ok(b),
        })
        .map_err(WireOrdError::Left)?;
    let r_len = right
        .get_byte()
        .and_then(|b| match b as usize {
            l if l < min => Err(UnpackError::value_small_at(right.index() - 1)),
            l if l > max => Err(UnpackError::value_large_at(right.index() - 1)),
            _ => Ok(b),
        })
        .map_err(WireOrdError::Right)?;
    Ok(match l_len.cmp(&r_len) {
        Ordering::Equal => {
            let l_bytes = left
                .peek(l_len as usize)
                .and_then(unpack_ld_check)
                .map_err(WireOrdError::Left)?;
            let r_bytes = right
                .peek(r_len as usize)
                .and_then(unpack_ld_check)
                .map_err(WireOrdError::Right)?;
            let order = l_bytes.cmp(r_bytes);
            left.skip(l_len as usize).unwrap();
            right.skip(r_len as usize).unwrap();
            order
        }
        other => other,
    })
}

pub(crate) fn cmp16(
    left: &mut Reader<'_>,
    right: &mut Reader<'_>,
    min: usize,
    max: usize,
) -> Result<Ordering, WireOrdError> {
    let l_initial_index = left.index();
    let l_len_bytes = <[u8; 2]>::unpack(left).map_err(WireOrdError::Left)?;
    let len = match u16::from_be_bytes(l_len_bytes) as usize {
        l if l < min => {
            return Err(WireOrdError::Left(UnpackError::value_small_at(
                l_initial_index,
            )))
        }
        l if l > max => {
            return Err(WireOrdError::Left(UnpackError::value_large_at(
                l_initial_index,
            )))
        }
        l => l,
    };
    let r_len_bytes = <[u8; 2]>::unpack(right).map_err(WireOrdError::Right)?;
    match l_len_bytes.cmp(&r_len_bytes) {
        Ordering::Equal => (),
        other => return Ok(other),
    };
    let l_bytes = left.get_bytes(len).map_err(WireOrdError::Left)?;
    let r_bytes = right.get_bytes(len).map_err(WireOrdError::Right)?;
    Ok(l_bytes.cmp(r_bytes))
}

impl<const L: usize> Pack for [u8; L] {
    fn pack(&self, w: &mut Writer<'_>) -> Result<(), PackError> {
        w.write_forward(L)?.copy_from_slice(&self[..]);
        Ok(())
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(L)
    }
}

// TODO: ref unpack too (reuse for &Header)
impl<'a, const L: usize> Unpack<'a> for [u8; L] {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        reader.get_bytes(L).map(|s| s.try_into().unwrap())
    }
}

impl<const L: usize> UnpackedLen for [u8; L] {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        reader.skip(L).map(|_| L)
    }
}

impl<const L: usize> WireOrd for [u8; L] {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let left = left.get_bytes(L).map_err(WireOrdError::Left)?;
        let right = right.get_bytes(L).map_err(WireOrdError::Right)?;
        Ok(left.cmp(right))
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        self.cmp(other)
    }
}
