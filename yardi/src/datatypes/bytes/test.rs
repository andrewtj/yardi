#[cfg(not(feature = "std"))]
use alloc::{boxed::Box, string::String};

use crate::ascii::{self, Parse, ParseError, Parser, Present, PresentError, Presenter};

const BASE32HEX_SAMPLES: &'static [(&'static [u8], &'static str)] = &[
    (b"f", "CO"),
    (b"fo", "CPNG"),
    (b"foo", "CPNMU"),
    (b"foob", "CPNMUOG"),
    (b"fooba", "CPNMUOJ1"),
    (b"foobar", "CPNMUOJ1E8"),
];

struct Base32HexNpHelper<T>(T);

impl Parse for Base32HexNpHelper<Box<[u8]>> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        super::parse_base32hexnp(p, 0, !0).map(Self)
    }
}

impl<T> Present for Base32HexNpHelper<T>
where
    T: AsRef<[u8]>,
{
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        super::present_base32hexnp(self.0.as_ref(), p, 0, !0)
    }
}

#[test]
fn base32hex() {
    let c = Default::default();
    let mut s = String::new();
    for &(raw, ascii) in BASE32HEX_SAMPLES {
        s.clear();
        ascii::present::to_string(Base32HexNpHelper(raw), &mut s).unwrap();
        s.make_ascii_uppercase();
        assert_eq!(ascii, s);

        let h = ascii::parse::from_single_entry::<Base32HexNpHelper<Box<[u8]>>>(s.as_bytes(), &c)
            .unwrap();
        assert_eq!(&h.0[..], raw);
    }
}
