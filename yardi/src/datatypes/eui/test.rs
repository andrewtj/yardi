use crate::ascii::present;
use crate::rdata::{Eui48, Eui64};
#[cfg(not(feature = "std"))]
use alloc::string::String;
use core::str::FromStr;

#[test]
fn eui48_ascii() {
    let input = "00-11-22-33-44-55";
    let eui48 = Eui48::from_str(input).expect("eui48");
    assert_eq!(eui48.address, [0x00, 0x11, 0x22, 0x33, 0x44, 0x55]);
    let mut output = String::new();
    present::to_string(&eui48, &mut output).expect("present");
    assert_eq!(input, output);
}

#[test]
fn eui64_ascii() {
    let input = "00-11-22-33-44-55-66-77";
    let eui64 = Eui64::from_str(input).expect("eui64");
    assert_eq!(
        eui64.address,
        [0x00, 0x11, 0x22, 0x33, 0x44, 0x55, 0x66, 0x77]
    );
    let mut output = String::new();
    present::to_string(&eui64, &mut output).expect("present");
    assert_eq!(input, output);
}
