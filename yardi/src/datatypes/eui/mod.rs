#[cfg(test)]
mod test;

use crate::ascii::{ParseError, Parser, PresentError, Presenter};

pub(crate) fn parse48(p: &mut Parser<'_, '_>) -> Result<[u8; 6], ParseError> {
    let mut buf = [0u8; 6];
    parse(p, &mut buf)?;
    Ok(buf)
}

pub(crate) fn parse64(p: &mut Parser<'_, '_>) -> Result<[u8; 8], ParseError> {
    let mut buf = [0u8; 8];
    parse(p, &mut buf)?;
    Ok(buf)
}

fn parse(p: &mut Parser<'_, '_>, target: &mut [u8]) -> Result<(), ParseError> {
    p.pull(|s| {
        let s = s.as_bytes();
        let expected_len = target.len() * 2 + (target.len() - 1);
        if s.len() != expected_len {
            return Err(ParseError::value_invalid());
        }
        let mut target_iter = target.iter_mut();
        for o in s.split(|&b| b == b'-') {
            let t = target_iter.next().ok_or_else(ParseError::value_invalid)?;
            if o.len() != 2 {
                return Err(ParseError::value_invalid());
            }
            hex::decode_to_slice(o, core::slice::from_mut(t))
                .map_err(|_| ParseError::value_invalid())?;
        }
        if target_iter.next().is_some() {
            Err(ParseError::value_large())
        } else {
            Ok(())
        }
    })
}

pub(crate) fn present(s: &[u8], p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    let len_ok = s.len() == 6 || s.len() == 8;
    if !len_ok {
        return Err(PresentError::value_invalid());
    }
    let mut ip = p.incremental();
    let Some((&first, rest)) = s.split_first() else {
        return Err(PresentError::value_invalid());
    };
    ip.write_fmt(format_args!("{:02x}", first))?;
    for b in rest {
        ip.write_fmt(format_args!("-{:02x}", b))?;
    }
    Ok(())
}
