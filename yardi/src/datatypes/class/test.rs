#[cfg(not(feature = "std"))]
use alloc::string::ToString;

use super::Class;

#[test]
fn class_to_str_in() {
    assert_eq!("IN", Class::IN.to_string());
}

#[test]
fn class_from_str_in_upper() {
    assert_eq!(Class::IN, "IN".parse().expect("parse"));
}

#[test]
fn class_from_str_in_lower() {
    assert_eq!(Class::IN, "in".parse().expect("parse"));
}

#[test]
fn class_from_str_lower_in_generic() {
    assert_eq!(Class::IN, "CLASS1".parse().expect("parse"));
}

#[test]
fn class_from_str_upper_in_generic() {
    assert_eq!(Class::IN, "class1".parse().expect("parse"));
}

#[test]
fn class_to_str_65280() {
    assert_eq!("CLASS65280", Class(65280).to_string());
}

#[test]
fn class_from_str_upper_65280() {
    assert_eq!(Class(65280), "CLASS65280".parse().expect("parse"));
}

#[test]
fn class_from_str_lower_65280() {
    assert_eq!(Class(65280), "class65280".parse().expect("parse"));
}
