mod generated;
#[cfg(test)]
mod test;

use core::{
    fmt::{self, Debug, Display},
    str::FromStr,
};

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};

#[derive(
    Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
/// Represents a DNS Class.
pub struct Class(pub u16);

impl Class {
    /// Returns true if Class represents a Data Class. This method is fairly
    /// liberal and returns false only for ANY, CLASS0, CLASS65535, NONE, and
    /// Class in the QCLASS and Meta Class ranges.
    pub const fn is_data(self) -> bool {
        !matches!(
            self,
            Class(0)
                | Class(128..=253)
                | Class::NONE
                | Class::ANY
                | Class(57344..=65279)
                | Class(0xFFFF)
        )
    }
}

impl Parse for Class {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(Class::from_str)
    }
}

impl Present for Class {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        if let Some(s) = generated::to_str(*self) {
            p.write(s)
        } else {
            write!(p, "CLASS{}", self.0)
        }
    }
}

impl From<u16> for Class {
    fn from(class: u16) -> Self {
        Class(class)
    }
}

impl Display for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(s) = generated::to_str(*self) {
            Display::fmt(s, f)
        } else {
            write!(f, "CLASS{}", self.0)
        }
    }
}

impl Debug for Class {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

impl FromStr for Class {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(cl) = generated::from_str(s) {
            return Ok(cl);
        }
        let prefix = b"CLASS";
        if s.len() <= prefix.len() {
            return Err(ParseError::value_invalid());
        }
        let (maybe_prefix, maybe_int) = s.as_bytes().split_at(prefix.len());
        if prefix.eq_ignore_ascii_case(maybe_prefix) {
            crate::datatypes::int::parse_u16(maybe_int).map(Class)
        } else {
            Err(ParseError::value_invalid())
        }
    }
}
