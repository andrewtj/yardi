#[cfg(not(feature = "std"))]
use alloc::string::ToString;

use super::Ty;

#[test]
fn ty_to_str_srv() {
    assert_eq!("SRV", Ty::SRV.to_string());
}

#[test]
fn ty_from_str_upper_srv() {
    assert_eq!(Ty::SRV, "SRV".parse().expect("parse"));
}

#[test]
fn ty_from_str_lower_srv() {
    assert_eq!(Ty::SRV, "srv".parse().expect("parse"));
}

#[test]
fn ty_from_str_lower_srv_generic() {
    assert_eq!(Ty::SRV, "TYPE33".parse().expect("parse"));
}

#[test]
fn ty_from_str_upper_srv_generic() {
    assert_eq!(
        Ty::SRV,
        core::str::FromStr::from_str("type33").expect("parse")
    );
}

#[test]
fn ty_to_str_65280() {
    assert_eq!("TYPE65280", Ty(65280).to_string());
}

#[test]
fn ty_from_str_upper_65280() {
    assert_eq!(Ty(65280), "TYPE65280".parse().expect("parse"));
}

#[test]
fn ty_from_str_lower_65280() {
    assert_eq!(Ty(65280), "type65280".parse().expect("parse"));
}
