mod generated;
#[cfg(test)]
mod test;

use core::fmt::{self, Debug, Display};
use core::str::FromStr;

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};

#[derive(
    Clone, Copy, Default, Hash, Eq, PartialEq, Ord, PartialOrd, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
/// Represents a DNS Type.
pub struct Ty(pub u16);

impl Ty {
    /// Returns true if Ty represents a Data Type. This method is fairly
    /// liberal and returns false only for OPT, TYPE0, TYPE65535 and Tys
    /// in the QTYPE and Meta TYPE range.
    pub const fn is_data(self) -> bool {
        !matches!(self, Ty(0) | Ty::OPT | Ty(128..=255) | Ty(0xFFFF))
    }
}

impl Parse for Ty {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(Ty::from_str)
    }
}

impl Present for Ty {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        if let Some(s) = generated::to_str(*self) {
            p.write(s)
        } else {
            write!(p, "TYPE{}", self.0)
        }
    }
}

impl From<u16> for Ty {
    fn from(ty: u16) -> Self {
        Ty(ty)
    }
}

impl Display for Ty {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if let Some(s) = generated::to_str(*self) {
            Display::fmt(s, f)
        } else {
            write!(f, "TYPE{}", self.0)
        }
    }
}

impl Debug for Ty {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

impl FromStr for Ty {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if let Some(v) = generated::from_str(s) {
            return Ok(v);
        }
        let prefix = b"TYPE";
        if s.len() <= prefix.len() {
            return Err(ParseError::value_invalid());
        }
        let (maybe_prefix, maybe_int) = s.as_bytes().split_at(prefix.len());
        if prefix.eq_ignore_ascii_case(maybe_prefix) {
            crate::datatypes::int::parse_u16(maybe_int).map(Ty)
        } else {
            Err(ParseError::value_invalid())
        }
    }
}
