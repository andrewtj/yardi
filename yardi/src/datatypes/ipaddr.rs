use core::{
    cmp::Ordering,
    net::{Ipv4Addr, Ipv6Addr},
    str::FromStr,
};

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};
use crate::wire::{
    Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
};

macro_rules! impl_ipaddr {
    ($ty:ty, $size:expr) => {
        impl Present for $ty {
            fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
                write!(p, "{}", self)
            }
        }

        impl Parse for $ty {
            fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
                p.pull(|s| FromStr::from_str(s).map_err(|_| ParseError::value_invalid()))
            }
        }

        impl WireOrd for $ty {
            fn cmp_wire(
                left: &mut Reader<'_>,
                right: &mut Reader<'_>,
            ) -> Result<Ordering, WireOrdError> {
                crate::datatypes::bytes::cmp_fixed(left, right, $size)
            }
            fn cmp_as_wire(&self, other: &Self) -> Ordering {
                self.octets().cmp(&other.octets())
            }
        }

        impl Pack for $ty {
            fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
                dst.write_bytes(&self.octets()[..])
            }
            fn packed_len(&self) -> Result<usize, PackError> {
                Ok($size)
            }
        }

        impl<'a> Unpack<'a> for $ty {
            fn unpack(src: &mut Reader<'a>) -> Result<Self, UnpackError> {
                <[u8; $size]>::unpack(src).map(From::from)
            }
        }

        impl UnpackedLen for $ty {
            fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
                <[u8; $size]>::unpacked_len(reader)
            }
        }
    };
}

impl_ipaddr!(Ipv4Addr, 4);
impl_ipaddr!(Ipv6Addr, 16);
