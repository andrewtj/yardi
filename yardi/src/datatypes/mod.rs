//! DNS datatypes
pub(crate) mod bytes;
mod class;
pub(crate) mod eui;
pub(crate) mod ilnp;
#[macro_use]
pub(crate) mod int;
mod ipaddr;
pub mod name;
mod time;
mod ttl;
mod ty;

pub use self::class::Class;
#[doc(no_inline)]
pub use self::name::{name, Name};
pub use self::time::Time;
pub use self::ttl::Ttl;
pub use self::ty::Ty;

#[doc(no_inline)]
pub use crate::rdata::caa::CaaFlags;
#[doc(no_inline)]
pub use crate::rdata::cert::{CertAlg, CertTy};
#[doc(no_inline)]
pub use crate::rdata::csync::CsyncFlags;
#[doc(no_inline)]
pub use crate::rdata::dnskey::{DnskeyFlags, DnskeyProtocol};
#[doc(no_inline)]
pub use crate::rdata::ds::DsDigestAlg;
#[doc(no_inline)]
pub use crate::rdata::ipseckey::{IpseckeyAlg, IpseckeyGateway};
#[doc(no_inline)]
pub use crate::rdata::key::{KeyFlags, KeyProtocol};
#[doc(no_inline)]
pub use crate::rdata::nsec::NsecTypeBitmap;
#[doc(no_inline)]
pub use crate::rdata::nsec3::{Nsec3Flags, Nsec3HashAlg};
#[doc(no_inline)]
pub use crate::rdata::nsec3param::Nsec3paramFlags;
#[doc(no_inline)]
pub use crate::rdata::sshfp::{SshfpFpTy, SshfpPubKeyAlg};
#[doc(no_inline)]
pub use crate::rdata::tlsa::{TlsaCertUsage, TlsaMatchTy, TlsaSelector};
#[doc(no_inline)]
pub use crate::rdata::tsig::{TsigRcode, TsigTime};
#[doc(no_inline)]
pub use crate::rdata::txt::TxtData;
#[doc(no_inline)]
pub use crate::rdata::wks::WksBitmap;
