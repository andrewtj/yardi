#[cfg(not(feature = "std"))]
use alloc::string::String;
use core::{fmt::Write, str::FromStr};

use crate::ascii::{present, Context};
use crate::rdata::ChA;
use crate::wire::{Pack, Reader, Unpack, Writer};

#[test]
#[cfg_attr(miri, ignore)]
fn u16_octal() {
    let mut input = String::new();
    let mut output = String::new();
    for i in 0..=0xFFFF {
        input.clear();
        output.clear();
        write!(&mut input, ". {:o}", i).unwrap();
        let ch_a = ChA::from_str(&input).expect("parse");
        assert_eq!(ch_a.address, i);
        present::to_string(&ch_a, &mut output).expect("present");
        assert_eq!(input, output);
    }
}

macro_rules! test_ascii {
    ($test_name:ident, $ty:ty) => {
        #[test]
        fn $test_name() {
            let min = <$ty>::min_value();
            let max = <$ty>::max_value();
            let input: ($ty, $ty, $ty) = (min, 0, max);
            let expected_str = format!("{} 0 {}", min, max);
            let mut actual_str = String::with_capacity(expected_str.len());
            present::to_string(&input, &mut actual_str).expect("present");
            assert_eq!(expected_str, actual_str.as_str());
            let output: ($ty, $ty, $ty) = crate::ascii::parse::from_single_entry(
                actual_str.as_bytes(),
                Context::shared_default(),
            )
            .expect("parse");
            assert_eq!(input, output);
        }
    };
}

test_ascii!(ascii_i8, i8);
test_ascii!(ascii_i16, i16);
test_ascii!(ascii_i32, i32);
test_ascii!(ascii_i64, i64);

test_ascii!(ascii_u8, u8);
test_ascii!(ascii_u16, u16);
test_ascii!(ascii_u32, u32);
test_ascii!(ascii_u64, u64);

macro_rules! test_wire {
    ($test_name:ident, $ty:ty) => {
        #[test]
        fn $test_name() {
            let input: ($ty, $ty, $ty) = (<$ty>::min_value(), 0, <$ty>::max_value());
            let mut buf = [0u8; 0xFF];
            let mut w = Writer::new(&mut buf[..]);
            input.pack(&mut w).expect("pack");
            let mut r = Reader::new(w.written());
            let output: ($ty, $ty, $ty) = Unpack::unpack(&mut r).expect("unpack");
            assert_eq!(input, output);
        }
    };
}

test_wire!(wire_i8, i8);
test_wire!(wire_i16, i16);
test_wire!(wire_i32, i32);
test_wire!(wire_i64, i64);

test_wire!(wire_u8, u8);
test_wire!(wire_u16, u16);
test_wire!(wire_u32, u32);
test_wire!(wire_u64, u64);
