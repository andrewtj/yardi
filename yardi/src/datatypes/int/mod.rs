#[cfg(test)]
mod test;

use core::cmp::Ordering;

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};
use crate::wire::{
    Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
};

macro_rules! parse_uint(
    ($ty:ty, $str:expr) => {{
        debug_assert!(!$str.is_empty());
        let mut n: $ty = 0;
        for d in $str.iter() {
            let d = d.checked_sub(b'0').and_then(|v| {
                if v > 9 { None } else { Some(v as $ty) }
            }).ok_or(ParseError::value_invalid())?;
            n = n.checked_mul(10).and_then(|v| v.checked_add(d)).ok_or(ParseError::value_large())?;
        }
        n
    }}
);

macro_rules! parse_iint(
    ($ty:ty, $str:expr) => {{
        debug_assert!(!$str.is_empty());
        match $str[0] {
            b'-' => {
                let mut n: $ty = 0;
                for d in $str.iter().skip(1) {
                    let d = d.checked_sub(b'0').and_then(|v| {
                        if v > 9 { None } else { Some(v as $ty) }
                    }).ok_or(ParseError::value_invalid())?;
                    n = n.checked_mul(10).and_then(|v| v.checked_sub(d)).ok_or(ParseError::value_large())?;
                }
                n
            }
            b'+' => parse_uint!($ty, &$str[1..]),
            _ => parse_uint!($ty, $str),
        }
    }}
);

macro_rules! ImplInt {
    (_pack: $src:expr, $size:expr, $write:ident, $writer:expr) => {
        $src.to_be_bytes().pack($writer)
    };
    (_unpack: $ty:ident, $size:expr, $read:ident, $reader:expr) => {
        Ok($ty::from_be_bytes(<[u8; $size]>::unpack($reader)?))
    };
    ($ty:ident, $size:tt, $parse_f:ident, $parse_m:ident, $read:ident, $write:ident) => {
        pub(crate) fn $parse_f<T>(t: T) -> Result<$ty, ParseError>
        where
            T: AsRef<[u8]>,
        {
            let s = t.as_ref();
            // this check is here in case this is being called from a FromStr impl
            if s.is_empty() {
                return Err(ParseError::value_invalid());
            }
            Ok($parse_m!($ty, s))
        }
        impl Parse for $ty {
            fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
                p.pull(|s| $parse_f(s.as_bytes()))
            }
        }
        impl Present for $ty {
            fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
                write!(p, "{}", *self)
            }
        }
        impl WireOrd for $ty {
            fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
                let l = <[u8; $size]>::unpack(left).map_err(WireOrdError::Left)?;
                let r = <[u8; $size]>::unpack(right).map_err(WireOrdError::Right)?;
                Ok(l.cmp(&r))
            }
            fn cmp_as_wire(&self, other: &Self) -> Ordering {
                self.to_be_bytes().cmp(&other.to_be_bytes())
            }
        }
        impl Pack for $ty {
            fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
                ImplInt!(_pack: *self, $size, $write, dst)
            }
            fn packed_len(&self) -> Result<usize, PackError> {
                Ok($size)
            }
        }
        impl<'a> Unpack<'a> for $ty {
            fn unpack(src: &mut Reader<'a>) -> Result<Self, UnpackError> {
                ImplInt!(_unpack: $ty, $size, $read, src)
            }
        }
        impl UnpackedLen for $ty {
            fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
                <[u8; $size]>::unpacked_len(reader)
            }
        }
    };
}

ImplInt!(u8, 1, parse_u8, parse_uint, read_u8, write_u8);
ImplInt!(u16, 2, parse_u16, parse_uint, read_u16, write_u16);
ImplInt!(u32, 4, parse_u32, parse_uint, read_u32, write_u32);
ImplInt!(u64, 8, parse_u64, parse_uint, read_u64, write_u64);

ImplInt!(i8, 1, parse_i8, parse_iint, read_i8, write_i8);
ImplInt!(i16, 2, parse_i16, parse_iint, read_i16, write_i16);
ImplInt!(i32, 4, parse_i32, parse_iint, read_i32, write_i32);
ImplInt!(i64, 8, parse_i64, parse_iint, read_i64, write_i64);

pub(crate) fn parse_u16_octal(p: &mut Parser<'_, '_>) -> Result<u16, ParseError> {
    p.pull(|s| {
        let mut out = 0u16;
        for b in s.as_bytes().iter() {
            let t = b.wrapping_sub(b'0');
            if t > 7 {
                return Err(ParseError::value_invalid());
            }
            out = out.checked_shl(3).ok_or_else(ParseError::value_large)?;
            out |= u16::from(t);
        }
        Ok(out)
    })
}

pub(crate) fn present_u16_octal(mut n: u16, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    let mut tmp = [b'0'; 6];
    let mut i = tmp.len();
    while n != 0 {
        i -= 1;
        let t = 0b111 & n;
        n >>= 3;
        tmp[i] = t as u8 + b'0';
    }
    if i == tmp.len() {
        i -= 1;
    }
    let s = unsafe { core::str::from_utf8_unchecked(&tmp[i..]) };
    p.write(s)
}
