// TODO: drop underscores from "rfc_x"
use super::{Name, PTR_MASK};
use crate::wire::{Pack, Reader, Unpack, WireOrd, Writer};
#[cfg(not(feature = "std"))]
use alloc::{
    string::{String, ToString},
    vec::Vec,
};
use core::{fmt::Write, str::FromStr};

#[cfg(not(miri))]
#[test]
fn storage_alignment() {
    use super::{ExtArc, ExtHeader, ExtStatic, Header, Inline};
    macro_rules! offset {
        ($ty:ident . $($path:ident).+) => {
            unsafe {
                use core::mem::{ManuallyDrop, MaybeUninit};
                #[allow(invalid_value)]
                let t = <MaybeUninit<ManuallyDrop<$ty>>>::zeroed().assume_init();
                (&t.$($path).+ as *const _ as usize) - (&t as *const _ as usize)
            }
        };
    }
    macro_rules! same_offsets {
        (
            $first_ty:ident $(.$first_path:ident)+
            $(, $rest_ty:ident $(.$rest_path:ident)+)+
        ) => {
            let first_off = offset!($first_ty $(.$first_path)+);
            let first_ty_str = stringify!($first_ty $(.$first_path)+);
            $(
                assert_eq!(
                    first_off,
                    offset!($rest_ty $(.$rest_path)+),
                    "offset differs between {} and {}",
                    first_ty_str,
                    stringify!(
                        $rest_ty $(.$rest_path)+
                    ),
                );
            )+
        }
    }
    same_offsets!(
        Header.meta,
        Inline.header.meta,
        ExtHeader.header.meta,
        ExtStatic.header.meta,
        ExtArc.header.meta
    );
    same_offsets!(
        Header.len,
        Inline.header.len,
        ExtHeader.header.len,
        ExtStatic.header.len,
        ExtArc.header.len
    );
    same_offsets!(Inline.buf, ExtHeader.kind, ExtStatic.kind, ExtArc.kind);
    same_offsets!(ExtStatic.buf, ExtArc.buf);
}

#[test]
fn nmh_parse_escape() {
    use super::nmh_parse_escape;
    assert_eq!(nmh_parse_escape(b"a", 0), (b'a', 1));
    assert_eq!(nmh_parse_escape(b"12a", 0), (!0, !0));
    assert_eq!(nmh_parse_escape(b"123a", 0), (123, 3));
}

#[test]
fn nmh_parse_get_length() {
    use super::nmh_len;
    assert_eq!(nmh_len("com."), 4);
    assert_eq!(nmh_len("a."), 2);
    assert_eq!(nmh_len("\\097."), 2);
    assert_eq!(nmh_len("com."), 4);
    assert_eq!(nmh_len("example.com."), 12);
}

#[test]
fn names_iter() {
    let www_example_com = name!("www.example.com.");
    let example_com = name!("example.com.");
    let com = name!("com.");
    let mut iter = www_example_com.names();
    assert_eq!(iter.next(), Some(com));
    assert_eq!(iter.next(), Some(example_com));
    assert_eq!(iter.next(), Some(www_example_com.clone()));
    assert_eq!(iter.next(), None);
}

#[test]
fn name_ord() {
    let expect: &[Name] = &[
        name!("example."),
        name!("a.example."),
        name!("yljkjljk.a.example."),
        name!("Z.a.example."),
        name!("zABC.a.EXAMPLE."),
        name!("z.example."),
        name!("\\001.z.example."),
        name!("*.z.example."),
        name!("\\200.z.example."),
    ];
    let (left, right) = expect.split_at(expect.len() / 2);
    let mut v = Vec::with_capacity(expect.len());
    v.extend_from_slice(right);
    v.extend_from_slice(left);
    v.sort();
    assert_eq!(expect, &v[..]);
    for i in 0..v.len() {
        let t = v.remove(i);
        v.push(t);
        v.sort();
        assert_eq!(expect, &v[..]);
    }
}

#[test]
fn present_relative() {
    use core::fmt::Write;
    let example = name!("example.");
    let example_u = name!("EXAMPLE.");
    let www_example = name!("www.example.");
    let www_example_u = name!("WWW.EXAMPLE.");
    let mut all_matched = true;
    let cases = &[
        (example.clone(), example.clone(), "@"),
        (example_u.clone(), example.clone(), "EXAMPLE."),
        (example.clone(), example_u.clone(), "example."),
        (www_example.clone(), example.clone(), "www"),
        (example.clone(), www_example.clone(), "example."),
        (www_example.clone(), www_example.clone(), "@"),
        (www_example.clone(), www_example_u.clone(), "www.example."),
        (www_example_u.clone(), www_example.clone(), "WWW.EXAMPLE."),
    ];
    let mut out = String::new();
    let width = 13;
    let headers = &["a", "b", "expected", "actual", "match"];
    for (i, h) in headers.iter().enumerate() {
        let pad = if i == 0 { "" } else { "|" };
        write!(out, "{}{:w$}", pad, h, w = width).unwrap();
    }
    out.push('\n');
    for (i, _) in headers.iter().enumerate() {
        let pad = if i == 0 { "" } else { "|" };
        write!(out, "{}{:-<w$}", pad, '-', w = width).unwrap();
    }
    let mut tmp = String::with_capacity(12);
    let mut context = crate::ascii::Context::default();
    for &(ref a, ref b, expect) in cases {
        context.set_origin(b.clone());
        out.push('\n');
        tmp.clear();
        crate::ascii::present::to_string_context(&a, &mut tmp, &context).expect("to string");
        let m = tmp == expect;
        write!(
            out,
            "{:w$}|{:w$}|{:w$}|{:w$}|{:w$}",
            a,
            b,
            expect,
            tmp,
            m,
            w = width
        )
        .unwrap();
        all_matched &= m;
    }
    assert!(all_matched, "{out}");
}

#[test]
fn static_namebuf() {
    let expected_buf: &[u8] = b"\x07example\x03com";
    let actual = name!("example.com.");
    assert_eq!(actual.labels().count(), 2);
    assert_eq!(actual.buf(), expected_buf);
}

#[test]
fn pack_compress() {
    let mut buf = [0u8; 0xFF];
    let mut writer = Writer::new(&mut buf[..]);
    writer.set_format(crate::wire::Format::Compress(|_| true));
    let name = Name::from_str("example.com.").unwrap();
    for _ in 0..2 {
        name.pack(&mut writer).expect("write name");
    }
    assert_eq!(writer.written(), b"\x07example\x03com\x00\xC0\x00");
    let name2 = Name::from_str("example2.com.").unwrap();
    for _ in 0..2 {
        name2.pack(&mut writer).expect("write name");
    }
    assert_eq!(
        writer.written(),
        b"\x07example\x03com\x00\xC0\x00\x08example2\xC0\x08\xC0\x0F"
    );
}

#[test]
fn name_marshal() {
    let root = Name::from_str(".").expect("root");
    assert!(root.buf().is_empty());
    assert_eq!(".", root.to_string());
    assert!(root.labels().next().is_none());

    let net = Name::from_str("net.").expect("net");
    assert_eq!(b"\x03net", net.buf());
    assert_eq!("net.", net.to_string());
    let mut net_iter = net.labels();
    assert_eq!(net_iter.next(), Some(&b"net"[..]));
    assert!(net_iter.next().is_none());

    let example_net = Name::from_str_origin("example", &net).expect("example.net");
    assert_eq!(b"\x07example\x03net", example_net.buf());
    assert_eq!("example.net.", example_net.to_string());
    let mut example_net_iter = example_net.labels().rev();
    assert_eq!(example_net_iter.next(), Some(&b"example"[..]));
    assert_eq!(example_net_iter.next(), Some(&b"net"[..]));
    assert!(example_net_iter.next().is_none());

    let ddd = Name::from_str(r"\014.").expect("ddd");
    assert_eq!(&[1, 14], ddd.buf());
    assert_eq!(r"\014.", ddd.to_string());
    let mut ddd_iter = ddd.labels();
    assert_eq!(ddd_iter.next(), Some(&[14][..]));
    assert!(ddd_iter.next().is_none());
}

#[test]
fn parse_like_bind() {
    use crate::ascii::parse::Entry;
    let origin = name!("example.");
    let mut c = crate::ascii::Context::default();
    c.set_origin(origin.clone());
    let cases = &[
        (
            name!("foo\\ bah.example."),
            name!("foo bah.example."),
            r#""foo bah""#,
        ),
        (
            name!("foo\\ bah.example."),
            name!("foo bah.example."),
            r#""foo bah.example.""#,
        ),
    ];
    let mut failures = String::new();
    for &(ref expect, ref expect_alt, input) in cases {
        assert_eq!(expect, expect_alt);
        match crate::ascii::parse::from_entry::<Name>(
            input.as_bytes(),
            Default::default(),
            &c,
            true,
        ) {
            Ok(Entry::Value {
                read, ref value, ..
            }) if value == expect && read == input.len() => {}
            other => {
                write!(
                    failures,
                    "origin: {} input: {} expected: {} outcome: {:?}",
                    origin, input, expect, other
                )
                .unwrap();
            }
        }
    }
    assert!(failures.is_empty(), "{failures}");
}

#[test]
fn space_in_name() {
    let origin = name!("example.");
    let cases = &[
        (name!("\"foo\\ bah\".example."), r#""foo bah""#),
        (
            name!("\"foo\\ bah.example.\".example."),
            r#""foo bah.example.""#,
        ),
    ];
    let mut failures = String::new();
    for &(ref expect, ref input) in cases {
        match Name::from_str_origin(input, &origin) {
            Ok(ref n) if n == expect => (),
            other => {
                write!(
                    failures,
                    "origin: {} input: {} expected: {} got: {:?}",
                    origin, input, expect, other
                )
                .unwrap();
            }
        }
    }
    assert!(failures.is_empty(), "{failures}");
}

#[test]
#[cfg(feature = "std")]
fn hash_name() {
    use std::hash::{Hash, Hasher};
    fn hash<T>(t: T) -> u64
    where
        T: Hash,
    {
        let mut hasher = std::collections::hash_map::DefaultHasher::new();
        t.hash(&mut hasher);
        hasher.finish()
    }

    let a = name!("A.");
    let b = name!("a.");
    assert_eq!(a, b);
    assert_eq!(hash(a), hash(b));
}

#[test]
fn name_iter() {
    let name = name!("www.example.com.");
    let mut iter = name.labels();
    let www: &[u8] = b"www";
    let example: &[u8] = b"example";
    let com: &[u8] = b"com";
    assert_eq!(Some(com), iter.next());
    assert_eq!(Some(example), iter.next());
    assert_eq!(Some(www), iter.next());
    assert_eq!(None, iter.next());
    assert_eq!(vec![com, example, www], name.labels().collect::<Vec<_>>());
}

#[test]
fn name_iter_back() {
    let name = name!("www.example.com.");
    let mut iter = name.labels();
    let www: &[u8] = b"www";
    let example: &[u8] = b"example";
    let com: &[u8] = b"com";
    assert_eq!(Some(www), iter.next_back());
    assert_eq!(Some(example), iter.next_back());
    assert_eq!(Some(com), iter.next_back());
    assert_eq!(None, iter.next_back());
    assert_eq!(
        vec![www, example, com],
        name.labels().rev().collect::<Vec<_>>()
    );
}

#[test]
fn name_iter_back_and_forward() {
    let name = name!("www.example.com.au.");
    let mut iter = name.labels();
    let www: &[u8] = b"www";
    let example: &[u8] = b"example";
    let com: &[u8] = b"com";
    let au: &[u8] = b"au";
    assert_eq!(Some(www), iter.next_back());
    assert_eq!(Some(au), iter.next());
    assert_eq!(Some(example), iter.next_back());
    assert_eq!(Some(com), iter.next());
    assert_eq!(None, iter.next_back());
    assert_eq!(None, iter.next());
}

#[test]
fn name_iter_last_ptr() {
    let b: &[u8] = &[1, b'b', 0, 1, b'a', PTR_MASK, 0];
    let mut r = Reader::new_index(b.into(), 3);
    let n = Name::unpack(&mut r).expect("n");
    assert_eq!(vec![b"b", b"a"], n.labels().collect::<Vec<_>>());
}

#[test]
fn name_push() {
    let mut n = Name::new();
    for i in 1..128 {
        n.push(&[i]).expect("push label");
    }
    for (i, l) in n.labels().enumerate() {
        assert_eq!(l[0], i as u8 + 1);
    }
}

#[test]
fn name_push_top2() {
    let mut n = Name::new();
    assert_eq!(n, name!("."));
    n.push_top(b"a").unwrap();
    assert_eq!(n, name!("a."));
    n.push_top(b"b").unwrap();
    assert_eq!(n, name!("a.b."));
}

#[test]
fn name_push_top() {
    let mut n = Name::new();
    for i in 1..128 {
        n.push_top(&[i]).expect("push_top label");
    }
    for (i, l) in n.labels().rev().enumerate() {
        assert_eq!(l[0], i as u8 + 1);
    }
}

#[test]
fn name_parent() {
    let example_com = name!("example.com.");
    let com = name!("com.");
    let root = name!(".");
    assert_ne!(example_com, com);
    assert_eq!(example_com.parent(), Some(com.clone()));
    assert_ne!(example_com, com);
    assert_eq!(com.parent(), Some(Name::new()));
    assert_eq!(root.parent(), None);
}

#[test]
fn name_canonical_cow() {
    let canon = name!("example.");
    assert_eq!(canon.buf().as_ptr(), canon.to_canonical().buf().as_ptr(),);
    let mixed = name!("eXaMpLE.");
    assert_ne!(mixed.buf().as_ptr(), mixed.to_canonical().buf().as_ptr(),);
}

#[test]
fn root_predecessor() {
    let max_label = [0xFF; 63];
    let root = name!(".");
    let mut expect = Name::default();
    expect.push(&max_label[..]).unwrap();
    expect.push(&max_label[..]).unwrap();
    expect.push(&max_label[..]).unwrap();
    assert_eq!(expect.len(), 193);
    expect.push(&max_label[..61]).unwrap();
    assert_eq!(root.predecessor(&root), &expect);
}

#[test]
fn rfc_4471_name_predecessor_a() {
    let apex = name!("example.com.");
    let input = name!("foo.example.com.");
    let expect = name!(
        r"\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.fon\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.example.com."
    );
    assert_eq!(input.predecessor(&apex), expect);
}

#[test]
fn rfc_4471_name_predecessor_b() {
    let apex = name!("example.com.");
    let input = name!(r"\000.foo.example.com.");
    let expect = name!("foo.example.com.");
    assert_eq!(input.predecessor(&apex), expect);
}

#[test]
fn rfc_4471_name_predecessor_c() {
    let apex = name!("example.com.");
    let input = name!(r"foo\000.example.com.");
    let expect = name!(
        r"\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.foo.example.com."
    );
    assert_eq!(input.predecessor(&apex), expect);
}

#[test]
fn rfc_4471_name_predecessor_d() {
    let apex = name!("example.com.");
    let input = name!(r"fo\[.example.com.");
    let expect = name!(
        r"\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.fo\@\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.example.com."
    );
    assert_eq!(input.predecessor(&apex), expect);
}

#[test]
fn rfc_4471_name_predecessor_e() {
    let apex = name!("example.com.");
    let input = name!("example.com.");
    let expect = name!(
        r"\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.example.com."
    );
    assert_eq!(input.predecessor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_a() {
    let apex = name!("example.com.");
    let input = name!("foo.example.com.");
    let expect = name!(r"\000.foo.example.com.");
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_b() {
    let apex = name!("example.com.");
    let input = name!("fooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com.");
    let expect = name!(
        r"fooooooooooooooooooooooooooooooooooooooooooooooo\000.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_c() {
    let apex = name!("example.com.");
    let input = name!("foooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com.");
    let expect = name!(
        r"fooooooooooooooooooooooooooooooooooooooooooooooop.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_d() {
    let apex = name!("example.com.");
    let input = name!(
        r"\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    let expect = name!(
        r"oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooop.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_e() {
    let apex = name!("example.com.");
    let input = name!(
        r"foooooooooooooooooooooooooooooooooooooooo\255\255\255\255\255\255\255\255.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    let expect = name!(
        r"fooooooooooooooooooooooooooooooooooooooop.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_f() {
    let apex = name!("example.com.");
    let input = name!(
        r"fooooooooooooooooooooooooooooooooooooooooooooooo\@.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    let expect = name!(
        r"fooooooooooooooooooooooooooooooooooooooooooooooo\[.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.example.com."
    );
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn rfc_4471_name_successor_g() {
    let apex = name!("example.com.");
    let input = name!(
        r"\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255\255.example.com."
    );
    let expect = name!("example.com.");
    assert_eq!(input.successor(&apex), expect);
}

#[test]
fn minimal_cover() {
    let apex = name!("example.com.");
    let example = name!("www.example.com.");
    let before = example.predecessor(&apex);
    let after = example.successor(&apex);
    assert!(before < example);
    assert!(example < after);
    assert_eq!(before.successor(&apex), example);
    assert_eq!(after.predecessor(&apex), example);
}

#[test]
fn rfc_4470_cover_example() {
    let before = name!("exampld.com.");
    let after = name!("example-.com.");
    let target = name!("example.com.");
    assert!(before < target);
    assert!(target < after);
}

#[test]
fn rfc_4470_cover_example_wild() {
    let before = name!(r"\).com.");
    let after = name!("+.com.");
    let target = name!("*.com.");
    assert!(before < target);
    assert!(target < after);
}

#[test]
fn pop() {
    let mut n = name!("example.com.");
    assert!(n.pop());
    assert_eq!(n, name!("com."));
    assert!(n.pop());
    assert!(n.is_root());
    assert!(!n.pop());
}

#[test]
fn pop_top() {
    let mut n = name!("example.com.");
    assert!(n.pop_top());
    assert_eq!(n, name!("example."));
    assert!(n.pop_top());
    assert!(n.is_root());
    assert!(!n.pop_top());
}

#[test]
fn cut() {
    let n = name!("www.example.com.");
    assert_eq!(n.cut(0), Some(name!(".")));
    assert_eq!(n.cut(1), Some(name!("com.")));
    assert_eq!(n.cut(2), Some(name!("example.com.")));
    assert_eq!(n.cut(3), Some(name!("www.example.com.")));
    assert_eq!(n.cut(4), Some(name!("www.example.com.")));
}

#[test]
fn name_ptr_to_ptr() {
    let buf: &[u8] = b"\x07example\x00\xC0\x00\xC0\x09";
    let mut r = Reader::new(buf.into());
    assert_eq!(Name::unpack(&mut r), Ok(name!("example.")));
    assert_eq!(r.index(), 9);
    assert_eq!(Name::unpack(&mut r), Ok(name!("example.")));
    assert_eq!(r.index(), 11);
    assert_eq!(Name::unpack(&mut r), Ok(name!("example.")));
}

#[test]
fn invalid_name_forward_ptr() {
    let buf: &[u8] = b"\xC0\x02\x07example\x00";
    let mut r = Reader::new_index(buf.into(), 2);
    r.set_index(2);
    assert!(Name::unpack(&mut r).is_ok());
    assert_eq!(r.index(), buf.len());
    r.set_index(0);
    assert!(Name::unpack(&mut r).is_err());
}

#[test]
fn invalid_name_matryoshka() {
    let buf: &[u8] = b"\x01\x02\xC0\x01\x07example\x00";
    let mut r = Reader::new_index(buf.into(), 1);
    assert!(Name::unpack(&mut r).is_ok());
    assert_eq!(r.index(), buf.len());
    r.set_index(2);
    assert!(Name::unpack(&mut r).is_err());
}

#[test]
fn invalid_name_too_long() {
    let l = &[b'a'; 63][..];
    let mut buf = Vec::with_capacity(512);
    for _ in 0..(buf.capacity() - 1) / (1 + l.len()) {
        buf.push(l.len() as u8);
        buf.extend_from_slice(l);
    }
    buf.push(0);
    let mut r = Reader::new(&buf);
    assert!(Name::unpack(&mut r).is_err());
    r.set_index(0);
    let mut r2 = r.clone();
    assert!(Name::cmp_wire(&mut r, &mut r2).is_err());
    r.set_index(0);
    r2.set_index(0);
    assert!(Name::cmp_wire_canonical(&mut r, &mut r2).is_err());
}

#[test]
fn hostname() {
    assert!(!name!(".").is_host());
    assert!(!name!("-.").is_host());
    assert!(name!("a.").is_host());
    assert!(name!("1.").is_host());
    assert!(name!("a-.").is_host());
}

#[test]
fn name_macro() {
    let test = name!("example.com.");
    assert_eq!(Name::from_str_origin("example.com.", None).unwrap(), test);
}

fn max_name_bytes() -> Vec<u8> {
    let mut buf = Vec::with_capacity(0xFF);
    buf.resize(0xFF, 0);
    for l in buf.chunks_mut(2).filter(|s| s.len() == 2) {
        l[0] = 1;
        l[1] = b'a';
    }
    buf
}

const MAX_NAME_STR: &str = "a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.";

#[test]
fn name_max() {
    let w = max_name_bytes();
    let n = Name::unpack(&mut Reader::new(&w)).unwrap();
    assert_eq!(127, n.labels().count());
    assert_eq!(n.to_string(), MAX_NAME_STR);
}

#[test]
fn macro_name_max() {
    let n = name!("a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.a.");
    assert_eq!(127, n.labels().count());
    assert_eq!(n.to_string(), MAX_NAME_STR);
}

#[test]
fn namebuf_max() {
    let w = max_name_bytes();
    let nw = Name::unpack(&mut Reader::new(&w)).unwrap();
    assert_eq!(nw.labels().count(), 127);
    assert_eq!(nw.to_string(), MAX_NAME_STR);
    let ns = Name::from_str(MAX_NAME_STR).expect("parse");
    assert_eq!(ns.labels().count(), 127);
    assert_eq!(ns.to_string(), MAX_NAME_STR);
    assert_eq!(nw, ns);
}

#[test]
fn dont_compress_root() {
    let root = name!(".");
    let x = name!("x.");
    let mut buf = [0u8; 12];
    let mut w = Writer::new(&mut buf[..]);
    root.pack(&mut w).unwrap();
    root.pack(&mut w).unwrap();
    x.pack(&mut w).unwrap();
    x.pack(&mut w).unwrap();
    assert_eq!(w.written(), &[0, 0, 1, b'x', 0, 0xC0, 0x02]);
}

#[test]
fn tail() {
    let example = name!("example.");
    assert_eq!(example.len(), "\x00example\x00".len());
    let example_com = name!("example.com.");
    assert_eq!(Some(example.clone()), example_com.clone().tail());
    assert_eq!(example.tail(), Some(Name::new()));
    assert_eq!(Name::new().tail(), None);
}

#[test]
fn sensitivity() {
    let a_l = name!("a.");
    let a_u = name!("A.");
    assert_eq!(a_l, a_u);
    assert!(a_l.sensitive_eq(&a_l));
    assert!(a_u.sensitive_eq(&a_u));
    assert!(!a_l.sensitive_eq(&a_u));
    assert!(!a_u.sensitive_eq(&a_l));
    let seq: &[&Name] = &[&a_l, &a_u, &a_l, &a_u];
    let mut buf = [0u8; 0xFF];
    let mut w = Writer::new(&mut buf[..]);
    for n in seq {
        n.pack(&mut w).unwrap();
    }
    assert_eq!(w.written(), &[1, b'a', 0, 0xC0, 0, 0xC0, 0, 0xC0, 0]);
    let mut buf = [0u8; 0xFF];
    let mut w = Writer::new(&mut buf[..]);
    w.set_format(crate::wire::Format::CompressSensitive(|_| true));
    for n in seq {
        n.pack(&mut w).unwrap();
    }
    assert_eq!(w.written(), &[1, b'a', 0, 1, b'A', 0, 0xC0, 0, 0xC0, 3]);
}
