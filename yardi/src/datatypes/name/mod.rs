// TODO: consider building in common labels (com,www,_tcp,_udp,etc)
// TODO: allow for disallowing compression in first name encountered
// TODO: Label wrapper - make it easy to do the right thing by default
// TODO: should be parsing names as character-strings connected by
//       dots: https://datatracker.ietf.org/doc/html/rfc1035#section-5.1
// TODO: should have a cache of Names in Reader organised by both name
//       and offset. should allow for faster unpacking when compression
//       is involved. won't make much of a difference with memory
//       footprint, though if the Names are reusable elsewhere it might.
#![doc = r#"
DNS Names

Names are restricted to 255 bytes and 127 labels. Empty
labels and unqualified domain names are not represented.

Compression pointers that point to compression pointers,
point forwards, or that lead to overlapping an area already
visited are considered malicious and will not be unpacked.

```
use yardi::datatypes::name;

let example_com = name!("example.com.");

let com_label = "com".as_bytes();
let example_label = "example".as_bytes();

let forward_labels: Vec<_> = example_com.labels().collect();
assert_eq!(&[com_label, example_label], &forward_labels[..]);

let reverse_labels: Vec<_> = example_com.labels().rev().collect();
assert_eq!(&[example_label, com_label], &reverse_labels[..]);
```
"#]

// TODO: should group methods in some sort of logical order
// TODO: helper for DNAME substitutions?
// TODO: reparent name?
//       name!("_http._tcp.").reparent(name!("example.com."))
//       name!("client.zone.").reparent(name!("alt."))
mod label; // TODO: inline
#[cfg(test)]
mod test;

pub(crate) use self::label::LabelHeader; // TODO: expose LabelHeader?
use self::label::{decode_ptr, PTR_MASK};
pub use crate::name;
use crate::{
    ascii::{Context, Parse, ParseError, Parser, Present, PresentError, Presenter},
    encoding::{byte_to_ddd, parse_escape},
    wire::{
        Pack, PackError, Reader, Unpack, UnpackError, UnpackedLen, WireOrd, WireOrdError, Writer,
    },
};
use alloc::sync::Arc;
#[cfg(not(feature = "std"))]
use alloc::{borrow::ToOwned, vec::Vec};
use core::{
    cmp::{Eq, Ord, Ordering, PartialOrd},
    fmt::{self, Debug, Display},
    hash::{Hash, Hasher},
    mem::ManuallyDrop,
    str::FromStr,
};

/// Maximum length of a Name in wire format.
// TODO: Move to Name
pub const MAX: usize = 255;
/// Maximum length of a Name in ASCII.
// TODO: Move to Name
pub const MAX_STR: usize = (3 * 63 * 4) + (62 * 4) + 4;
pub use self::label::MAX as LABEL_MAX;
pub use self::label::MAX_STR as LABEL_MAX_STR;
pub(crate) const PTR_MAX: usize = 0x3FFF;

#[doc(hidden)]
pub const fn nmh_len(s: &str) -> usize {
    if s.is_empty() {
        panic!("empty name");
    }
    let b = s.as_bytes();
    if b.len() == 1 {
        if b[0] == b'.' {
            return 0;
        }
        panic!("unqualified name");
    }
    let mut i = 0;
    let mut lab_len = 0;
    let mut name_len = 0;
    while i < b.len() {
        match b[i] {
            b'.' if lab_len == 0 || lab_len > 63 => {
                panic!("empty label");
            }
            b'.' => {
                i += 1;
                name_len += lab_len + 1;
                lab_len = 0;
            }
            b'\\' => {
                i += 1;
                i = nmh_parse_escape(b, i).1;
                lab_len += 1;
            }
            b' '..=b'~' => {
                lab_len += 1;
                i += 1;
            }
            _ => panic!("invalid name"),
        }
    }
    if lab_len != 0 {
        panic!("unqualified name");
    }
    if name_len > 255 {
        panic!("name too long");
    }
    name_len
}

#[doc(hidden)]
pub const fn nmh_parse_escape(buf: &[u8], mut i: usize) -> (u8, usize) {
    match buf[i] {
        d_a @ b'0'..=b'1' => {
            i += 1;
            if let d_b @ b'0'..=b'9' = buf[i] {
                i += 1;
                if let d_c @ b'0'..=b'9' = buf[i] {
                    i += 1;
                    let v = ((d_a - b'0') * 100) + ((d_b - b'0') * 10) + (d_c - b'0');
                    return (v, i);
                }
            }
        }
        d_a @ b'2' => {
            i += 1;
            match buf[i] {
                d_b @ b'0'..=b'4' => {
                    i += 1;
                    if let d_c @ b'0'..=b'9' = buf[i] {
                        i += 1;
                        let v = ((d_a - b'0') * 100) + ((d_b - b'0') * 10) + (d_c - b'0');
                        return (v, i);
                    }
                }
                d_b @ b'5' => {
                    i += 1;
                    if let d_c @ b'0'..=b'5' = buf[i] {
                        i += 1;
                        let v = ((d_a - b'0') * 100) + ((d_b - b'0') * 10) + (d_c - b'0');
                        return (v, i);
                    }
                }
                _ => (),
            }
        }
        b'3'..=b'9' => (),
        v @ b' '..=b'~' => {
            i += 1;
            return (v, i);
        }
        _ => (),
    }
    (!0, !0)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub(crate) enum Style {
    Canonical,
    Compress,
    CompressSensitive,
    Plain,
}

/// A DNS Name.
pub struct Name {
    storage: Storage,
}

#[repr(C)]
union Storage {
    header: Header,
    inline: Inline,
    ext_header: ExtHeader,
    ext_static: ExtStatic,
    ext_arc: ManuallyDrop<ExtArc>,
}

impl Storage {
    fn len(&self) -> usize {
        unsafe {
            if !self.header.ext() {
                return usize::from(self.inline.header.len);
            }
            match self.ext_header.kind {
                ExtKind::Arc => usize::from(self.ext_arc.header.len - self.ext_arc.offset),
                ExtKind::Static => usize::from(self.ext_static.header.len),
            }
        }
    }
}

impl Drop for Storage {
    fn drop(&mut self) {
        unsafe {
            if self.header.ext() && self.ext_header.kind == ExtKind::Arc {
                ManuallyDrop::drop(&mut self.ext_arc);
            }
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct Header {
    meta: u8,
    len: u8,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct Inline {
    header: Header,
    buf: [u8; Self::BUF_SIZE],
}

impl Inline {
    const BUF_SIZE: usize = size_of::<Vec<u8>>() - size_of::<Header>();
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct ExtHeader {
    header: Header,
    kind: ExtKind,
}

#[repr(u8)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum ExtKind {
    Static,
    Arc,
}

#[repr(u8)]
#[derive(Clone, Copy, Debug)]
enum ExtStaticKind {
    Marker = ExtKind::Static as u8,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct ExtStatic {
    header: Header,
    kind: ExtStaticKind,
    // TODO: offset and neg_offset like Arc to enable buffer reuse?
    buf: &'static [u8],
}

#[repr(u8)]
#[derive(Clone, Copy, Debug)]
enum ExtArcKind {
    Marker = ExtKind::Arc as u8,
}

#[repr(C)]
#[derive(Clone, Debug)]
struct ExtArc {
    header: Header,
    kind: ExtArcKind,
    offset: u8,
    // TODO: neg_offset, shrinking
    buf: Arc<[u8]>,
}

#[derive(Clone)]
struct Builder {
    buf: [u8; 254],
    buf_len: u8,
    labels: u8,
}

impl Default for Builder {
    fn default() -> Self {
        Builder {
            buf: [0u8; 254],
            buf_len: 0,
            labels: 0,
        }
    }
}

impl Builder {
    fn push(&mut self, l: &[u8]) -> Result<(), ()> {
        if let None | Some(0xFF) = self.buf_len.checked_add(l.len() as u8 + 1) {
            return Err(());
        };
        self.buf[usize::from(self.buf_len)] = l.len() as u8;
        self.buf_len += 1;
        self.buf[usize::from(self.buf_len)..][..l.len()].copy_from_slice(l);
        self.buf_len += l.len() as u8;
        self.labels += 1;
        Ok(())
    }
    fn forward(&mut self, len: usize) -> Result<&mut [u8], ()> {
        assert!(len > 0 && len < 64);
        let Builder {
            ref mut buf,
            ref mut buf_len,
            ref mut labels,
        } = self;
        let start = usize::from(*buf_len);
        let end = start + 1 + len;
        let mut label = buf.get_mut(start..end).ok_or(())?;
        label[0] = len as u8;
        label = &mut label[1..];
        assert_eq!(label.len(), len);
        *buf_len = end as u8;
        *labels += 1;
        Ok(label)
    }
    fn build(&self) -> Name {
        let len = usize::from(self.buf_len);
        #[cfg(debug_assertions)]
        {
            let mut s = &self.buf[..len];
            while !s.is_empty() {
                let l = s[0];
                assert!(l > 0 && l < 64, "bad len: {}", l);
                s = &s[1 + usize::from(l)..];
            }
        }
        let storage = if len > Inline::BUF_SIZE {
            let buf: Arc<[u8]> = self.buf[..len].into();
            let ext_arc = ExtArc {
                header: Header {
                    meta: Header::META_EXT | self.labels,
                    len: self.buf_len + 1,
                },
                kind: ExtArcKind::Marker,
                offset: 0,
                buf,
            };
            let ext_arc = ManuallyDrop::new(ext_arc);
            Storage { ext_arc }
        } else {
            let mut inline = Inline {
                header: Header {
                    meta: self.labels,
                    len: self.buf_len + 1,
                },
                buf: [0u8; Inline::BUF_SIZE],
            };
            inline.buf[..len].copy_from_slice(&self.buf[..len]);
            Storage { inline }
        };
        Name { storage }
    }
}

impl Header {
    const META_EXT: u8 = 0b1000_0000;
    const META_LCM: u8 = 0b0111_1111;
    fn ext(&self) -> bool {
        Self::META_EXT & self.meta == Self::META_EXT
    }
    fn labels(&self) -> u8 {
        Self::META_LCM & self.meta
    }
}

impl Default for Name {
    fn default() -> Self {
        Name::new()
    }
}

impl Clone for Name {
    fn clone(&self) -> Self {
        let storage = unsafe {
            match self.storage.header.ext() {
                true => match self.storage.ext_header.kind {
                    ExtKind::Arc => {
                        let ext_arc = ExtArc::clone(&self.storage.ext_arc);
                        let ext_arc = ManuallyDrop::new(ext_arc);
                        Storage { ext_arc }
                    }
                    ExtKind::Static => {
                        let ext_static = self.storage.ext_static;
                        Storage { ext_static }
                    }
                },
                false => {
                    let inline = self.storage.inline;
                    Storage { inline }
                }
            }
        };
        Self { storage }
    }
}

#[allow(clippy::len_without_is_empty)]
impl Name {
    /// Root.
    pub const ROOT: &'static Self = &Self::new();
    /// Construct a new `Name`.
    pub const fn new() -> Self {
        let inline = Inline {
            header: Header { meta: 0, len: 1 },
            buf: [0u8; Inline::BUF_SIZE],
        };
        let storage = Storage { inline };
        Name { storage }
    }
    /// Returns the length of this name in wire format. May be longer in string format.
    pub fn len(&self) -> usize {
        self.storage.len()
    }
    /// Returns true if this name is the root name.
    pub fn is_root(&self) -> bool {
        unsafe { self.storage.header.labels() == 0 }
    }
    /// Returns true if this name is a host name.
    ///
    /// This crate considers a name containing one or more labels (beyond the root
    /// empty label) that each start with a letter or digit and otherwise only
    /// contain letters, digests or dashes to be a host name. Letters are limited
    /// to the (US) ASCII range.
    pub fn is_host(&self) -> bool {
        !self.is_root() && self.is_host_or_root()
    }
    /// Returns true if this name represents a host name or the root name.
    // TODO: does this pull its weight?
    pub fn is_host_or_root(&self) -> bool {
        fn ld(b: u8) -> bool {
            matches!(b,
                b'0'..=b'9' | b'A'..=b'Z' | b'a'..=b'z')
        }
        fn ldh(b: u8) -> bool {
            b == b'-' || ld(b)
        }
        self.labels()
            .all(|l| ld(l[0]) && l[1..].iter().cloned().all(ldh))
    }
    #[doc = r#"
Construct a `Name` by parsing `name` relative to `origin`.
```
use yardi::datatypes::{name, Name};
let origin = name!("example.");
let example = Name::from_str_origin("www", &origin).expect("valid name");
assert_eq!(example, name!("www.example."));
```
"#]
    pub fn from_str_origin<'a, T>(name: &str, origin: T) -> Result<Self, ParseError>
    where
        T: Into<Option<&'a Name>>,
    {
        let name = name.as_bytes();
        if name == b"@" || name.is_empty() {
            return origin
                .into()
                .map(ToOwned::to_owned)
                .ok_or_else(ParseError::value_no_origin);
        }
        if name == b"." {
            return Ok(Name::default());
        }
        let mut builder = Builder::default();
        let mut iter = name.iter().copied();
        let mut label = [0u8; 63];
        let mut label_index = 0;
        while let Some(b) = iter.next() {
            match b {
                b'.' => {
                    if label_index == 0 {
                        return Err(ParseError::value_empty_label());
                    }
                    // TODO: Builder should produce specific error?
                    builder
                        .push(&label[..label_index])
                        .map_err(|_| ParseError::value_large())?;
                    label_index = 0;
                }
                // Space is accepted here because it's assumed that input
                // has already been cut into text tokens that have been
                // delineated in some way. (Possibly quotation marks.)
                b' '..=b'~' => {
                    let b = if b == b'\\' {
                        parse_escape(&mut iter)?
                    } else {
                        b
                    };
                    if label_index >= 63 {
                        return Err(ParseError::value_long_label());
                    }
                    label[label_index] = b;
                    label_index += 1;
                }
                _ => return Err(ParseError::bad_ascii()),
            };
        }
        if label_index != 0 {
            // TODO: Builder should produce specific error?
            builder
                .push(&label[..label_index])
                .map_err(|_| ParseError::value_large())?;
            let o = origin.into().ok_or_else(ParseError::value_no_origin)?;
            for l in o.labels().rev() {
                builder.push(l).map_err(|_| ParseError::value_large())?;
            }
        }
        Ok(builder.build())
    }
    #[doc = r#"
Add a label to the bottom of this `Name`.
```
use yardi::datatypes::{name, Name};
let mut example: Name = name!("com.");
assert!(example.push(b"example").is_ok());
assert_eq!(example, name!("example.com."));
```
"#]
    // TODO: NameMutateError::{InvalidLabel, NameTooLong}
    // TODO: reuse buffer
    pub fn push(&mut self, label: &[u8]) -> Result<(), ()> {
        if label.is_empty() || label.len() > 63 {
            return Err(()); // TODO: Error types
        }
        let new_len = self.len().checked_add(label.len() + 1).ok_or(())?;
        let mut builder = Builder::default();
        builder.push(label)?;
        for l in self.labels().rev() {
            builder.push(l)?;
        }
        *self = builder.build();
        debug_assert_eq!(self.len(), new_len as usize);
        Ok(())
    }
    #[doc = r#"
Add a label to the top of this `Name`.
```
use yardi::datatypes::{name, Name};
let mut example: Name = name!("example.");
assert!(example.push_top(b"com").is_ok());
assert_eq!(example, name!("example.com."));
```
"#]
    // TODO: NameMutateError::{InvalidLabel, NameTooLong}
    // TODO: reuse buffer
    pub fn push_top(&mut self, label: &[u8]) -> Result<(), ()> {
        if label.is_empty() || label.len() > 63 {
            return Err(()); // TODO: Error types
        }
        let new_len = self.len().checked_add(label.len() + 1).ok_or(())?;
        let mut builder = Builder::default();
        for l in self.labels().rev() {
            builder.push(l)?;
        }
        builder.push(label)?;
        *self = builder.build();
        debug_assert_eq!(self.len(), new_len);
        Ok(())
    }
    // TODO: is this useful? if so, make it efficient.
    /// Remove the last (left-most) label from this name.
    pub fn pop(&mut self) -> bool {
        if let Some(parent) = self.parent() {
            *self = parent;
            true
        } else {
            false
        }
    }
    // TODO: is this useful? if so, make it efficient.
    /// Remove the first (right-most) label from this name.
    pub fn pop_top(&mut self) -> bool {
        let labels = self.labels().count();
        if labels == 0 {
            return false;
        }
        let mut builder = Builder::default();
        for l in self.labels().rev().take(labels - 1) {
            builder.push(l).unwrap();
        }
        *self = builder.build();
        true
    }
    /// Constructs a `Name` from an Iterator of `Label`.
    // TODO: is this useful? collect? could be, for eg: DNSCURVE
    // TODO: NameMutateError::{InvalidLabel, NameTooLong}
    pub fn from_labels<'a, T>(labels: T) -> Result<Self, ()>
    where
        T: Iterator<Item = &'a [u8]>,
    {
        let mut builder = Builder::default();
        for l in labels {
            builder.push(l)?;
        }
        Ok(builder.build())
    }
    #[doc(hidden)]
    /// This is a private interface for use by macros defined in this crate.
    pub const unsafe fn from_static(buf: &'static [u8]) -> Self {
        let mut labels = 0u8;
        let mut i = 0;
        while i < buf.len() {
            let l = buf[i];
            i += 1;
            i += l as usize;
            labels += 1;
        }
        let ext_static = ExtStatic {
            header: Header {
                meta: Header::META_EXT | labels,
                len: buf.len() as u8 + 1,
            },
            kind: ExtStaticKind::Marker,
            buf,
        };
        let storage = Storage { ext_static };
        Self { storage }
    }
    /// Returns an iterator over this `Name`'s labels.
    pub fn labels(&self) -> Labels<'_> {
        let buf = self.buf();
        let cursor = 0;
        let labels = unsafe { self.storage.header.labels() };
        Labels {
            buf,
            cursor,
            labels,
        }
    }
    #[doc = r#"
Returns the parent of this `Name`, if any.
```
use yardi::datatypes::{name, Name};
let com = name!("com.");
let example_com = name!("example.com.");
assert_eq!(example_com.parent(), Some(com.clone()));
assert_eq!(com.parent(), Some(Name::new()));
assert!(Name::new().parent().is_none());
```
"#]
    pub fn parent(&self) -> Option<Name> {
        unsafe {
            if self.storage.header.labels() == 0 {
                return None;
            }
            let storage = if !self.storage.header.ext() {
                let mut inline = self.storage.inline;
                let offset = 1 + inline.buf[0];
                inline.buf.copy_within(usize::from(offset).., 0);
                inline.header.meta = inline.header.labels() - 1;
                inline.header.len -= offset;
                Storage { inline }
            } else if self.storage.ext_header.kind == ExtKind::Static {
                let mut ext_static = self.storage.ext_static;
                let offset = 1 + ext_static.buf[0];
                ext_static.buf = &ext_static.buf[usize::from(offset)..];
                ext_static.header.meta = Header::META_EXT | (ext_static.header.labels() - 1);
                ext_static.header.len -= offset;
                Storage { ext_static }
            } else {
                assert_eq!(self.storage.ext_header.kind, ExtKind::Arc);
                let mut ext_arc = ExtArc::clone(&self.storage.ext_arc);
                ext_arc.offset += 1 + ext_arc.buf[usize::from(ext_arc.offset)];
                ext_arc.header.meta = Header::META_EXT | (ext_arc.header.labels() - 1);
                let ext_arc = ManuallyDrop::new(ext_arc);
                Storage { ext_arc }
            };
            Some(Name { storage })
        }
    }
    #[doc = r#"
Return the name formed by removing the top label, if any.
```
use yardi::datatypes::{name, Name};
let example_com: Name = name!("example.com.");
assert_eq!(Some(name!("example.")), example_com.tail());
```
"#]
    // TODO: is this useful? if so make it efficient
    pub fn tail(&self) -> Option<Name> {
        if self.labels().count() == 0 {
            return None;
        }
        let mut me = self.clone();
        assert!(me.pop_top());
        Some(me)
    }
    #[doc = r#"
Returns the Name formed by cutting this `Name` at `labels` labels from the root.
```
use yardi::name;
let com = name!("com.");
let example_com = name!("example.com.");
assert_eq!(example_com.cut(1), Some(com));
```
"#]
    pub fn cut(&self, labels: usize) -> Option<Name> {
        // TODO: make this efficient
        let mut new = self.clone();
        for _ in 0..self.labels().count().saturating_sub(labels) {
            new = new.parent()?;
        }
        Some(new)
    }
    /// Derives preceding name using [RFC 4471](https://tools.ietf.org/html/rfc4471)'s
    /// [Absolute Method](https://tools.ietf.org/html/rfc4471#section-3.1.1).
    /// Panics if `apex` does not contain `self`.
    pub fn predecessor(&self, apex: &Name) -> Name {
        if !apex.is_suffix(self) {
            panic!("apex must be a suffix of self");
        }
        // TODO: would a Builder::build_rev() that reversed the name on output make this easier?
        let mut temp = [0u8; 254];
        let mut name_start = temp.len() + 1 - self.len();
        let buf = self.buf();
        temp[name_start..].copy_from_slice(buf);
        let left_most_label = if buf.is_empty() {
            buf
        } else {
            &buf[..1 + usize::from(buf[0])]
        };
        /*
        1.  If N is the same as the owner name of the zone apex, prepend N
            repeatedly with labels of the maximum length possible consisting
            of octets of the maximum sort value (e.g., 0xff) until N is the
            maximum length possible; otherwise proceed to the next step.
        */
        if self == apex {
        }
        /*
        2.  If the least significant (left-most) label of N consists of a
            single octet of the minimum sort value (e.g., 0x00), remove that
            label; otherwise proceed to the next step.
        */
        else if left_most_label == [1, 0] {
            return self.parent().unwrap();
        }
        /*
        3.  If the least significant (right-most) octet in the least
            significant (left-most) label of N is the minimum sort value,
            remove the least significant octet and proceed to step 5.
        */
        else if left_most_label.ends_with(&[0]) {
            let src = name_start + 1..name_start + usize::from(left_most_label[0]);
            let dest = name_start + 2;
            temp.copy_within(src, dest);
            temp[name_start + 1] = temp[name_start] - 1;
            name_start += 1;
        }
        /*
        4.  Decrement the value of the least significant (right-most) octet
            of the least significant (left-most) label, skipping any values
            that correspond to uppercase US-ASCII letters, and then append
            the least significant (left-most) label with as many octets as
            possible of the maximum sort value.  Proceed to the next step.
        */
        else {
            let left_most_label_len = usize::from(temp[name_start]);
            let right_most_octet_index = name_start + left_most_label_len;
            temp[right_most_octet_index] = match temp[right_most_octet_index] - 1 {
                b'A'..=b'Z' => b'A' - 1,
                other => other,
            };
            let fill = (63 - left_most_label_len).min(name_start - 1);
            let src = name_start..name_start + left_most_label_len + 1;
            name_start -= fill;
            temp.copy_within(src, name_start);
            temp[name_start] += fill as u8;
            for b in temp[name_start + left_most_label_len + 1..][..fill].iter_mut() {
                *b = 0xFF;
            }
        }
        /*
        5.  Prepend N repeatedly with labels of as long a length as possible
            consisting of octets of the maximum sort value until N is the
            maximum length possible.
        */
        for b in temp[..name_start].iter_mut() {
            *b = 0xFF;
        }
        let mut labels = self.labels().count() as u8;
        while name_start >= 64 {
            name_start -= 64;
            temp[name_start] = 63;
            labels += 1;
        }
        if name_start > 1 {
            temp[0] = name_start as u8 - 1;
            name_start = 0;
            labels += 1;
        }
        let ext_arc = ExtArc {
            header: Header {
                meta: Header::META_EXT | labels,
                len: 255 - name_start as u8,
            },
            kind: ExtArcKind::Marker,
            offset: 0,
            buf: temp[name_start..].into(),
        };
        assert!(ext_arc.header.ext());
        let ext_arc = ManuallyDrop::new(ext_arc);
        assert!(ext_arc.header.ext());
        let storage = Storage { ext_arc };
        unsafe {
            assert!(storage.header.ext());
            assert_eq!(storage.ext_header.kind, ExtKind::Arc);
        }
        Name { storage }
    }
    /// Derives succeeding name using [RFC 4471](https://tools.ietf.org/html/rfc4471)'s
    /// [Absolute Method](https://tools.ietf.org/html/rfc4471#section-3.1.2).
    /// Panics if `apex` does not contain `self`.
    pub fn successor(&self, apex: &Name) -> Name {
        if !apex.is_suffix(self) {
            panic!("apex must be a suffix of self");
        }
        /*
        1.  If N is two or more octets shorter than the maximum DNS name
            length, prepend N with a label containing a single octet of the
            minimum sort value (e.g., 0x00); otherwise proceed to the next
            step.
        */
        // We don't include root label: 252 + 2 + 1 = 255
        let mut builder = Builder::default();
        if self.len() <= 253 {
            builder.push(&[0]).unwrap();
            for l in self.labels().rev() {
                builder.push(l).unwrap();
            }
            return builder.build();
        }
        let mut cursor = None;
        loop {
            let me = cursor.as_ref().unwrap_or(self);
            /*
            2.  If N is one octet shorter than the maximum DNS name length and
                the least significant (left-most) label is one or more octets
                shorter than the maximum label length, append an octet of the
                minimum sort value to the least significant label; otherwise
                proceed to the next step.
            */
            // We don't include root label: 253 + 1 + 1 = 255
            let buf = me.buf();
            if buf.len() == 253 && buf[0] < 63 {
                let first_label_len = usize::from(buf[0]);
                let new_label_len = 1 + first_label_len;
                let new_label = builder.forward(new_label_len).unwrap();
                new_label[..first_label_len].copy_from_slice(&buf[1..new_label_len]);
                new_label[first_label_len] = 0;
                for l in self.labels().rev().skip(1) {
                    builder.push(l).unwrap();
                }
                return builder.build();
            }
            /*
            3.  Increment the value of the least significant (right-most) octet
                in the least significant (left-most) label that is less than the
                maximum sort value (e.g., 0xff), skipping any values that
                correspond to uppercase US-ASCII letters, and then remove any
                octets to the right of that one.  If all octets in the label are
                the maximum sort value, then proceed to the next step.
            */
            let mut i = usize::from(buf[0]);
            while i > 0 {
                if buf[i] != 255 {
                    let new_label = builder.forward(i).unwrap();
                    new_label.copy_from_slice(&buf[1..][..i]);
                    i -= 1;
                    new_label[i] = match new_label[i] + 1 {
                        b'A'..=b'Z' => b'Z' + 1,
                        other => other,
                    };
                    for l in cursor.as_ref().unwrap_or(self).labels().rev().skip(1) {
                        builder.push(l).unwrap();
                    }
                    return builder.build();
                }
                i -= 1;
            }
            /*
            4.  Remove the least significant (left-most) label.  Unless N is now
                the same as the owner name of the zone apex (this will occur only
                if N was the maximum possible name in canonical DNS name order,
                and thus has wrapped to the owner name of zone apex), repeat
                starting at step 2.
            */
            let parent = me.parent().unwrap();
            if parent == apex {
                return parent;
            }
            cursor = Some(parent);
        }
    }
    // TODO: ascendants -> parents? ancestors?
    /// Return an iterator over a names ascendants.
    pub fn names(&self) -> Names<'_> {
        let cursor_backward = self.labels().count() as u8;
        Names {
            name: self,
            cursor_forward: 0,
            cursor_backward,
        }
    }
    /// Returns true if this name is in canonical format.
    pub fn is_canonical(&self) -> bool {
        !self
            .labels()
            .rev()
            .all(|l| l.iter().any(u8::is_ascii_uppercase))
    }
    /// Converts this name to canonical format.
    pub fn to_canonical(&self) -> Name {
        if self.is_canonical() {
            return self.clone();
        }
        let mut builder = Builder::default();
        for l in self.labels().rev() {
            let label = builder.forward(l.len()).unwrap();
            for i in 0..l.len() {
                label[i] = l[i].to_ascii_lowercase();
            }
        }
        builder.build()
    }
    /// Returns true if `self` is the direct parent of `other`.
    pub fn is_parent(&self, other: &Name) -> bool {
        let self_labels = self.labels().count();
        let other_labels = other.labels().count();
        (self_labels + 1 == other_labels) && self.is_ascendant(other)
    }
    /// Returns true if `self` is ascendant of `other`.
    pub fn is_ascendant(&self, other: &Name) -> bool {
        let mut a = self.labels();
        let mut b = other.labels();
        loop {
            match (a.next(), b.next()) {
                (None, b) => break b.is_some(),
                (Some(a), Some(b)) if a.eq_ignore_ascii_case(b) => {}
                (_, _) => break false,
            }
        }
    }
    /// Returns true if `self` is a suffix of `other`.
    pub fn is_suffix(&self, other: &Name) -> bool {
        let mut a = self.labels();
        let mut b = other.labels();
        loop {
            match (a.next(), b.next()) {
                (None, _) => break true,
                (Some(a), Some(b)) => {
                    // TODO: don't think this is covered by any test other than NameTrees
                    if !a.eq_ignore_ascii_case(b) {
                        break false;
                    }
                }
                (_, None) => break false,
            }
        }
    }
    /// Returns true if `self` is a suffix of `other` when compared in a case sensitive manner.
    pub fn is_suffix_sensitive(&self, other: &Name) -> bool {
        let mut a = self.labels();
        let mut b = other.labels();
        loop {
            match (a.next(), b.next()) {
                (None, _) => break true,
                (Some(a), Some(b)) => {
                    if a != b {
                        break false;
                    }
                }
                (_, _) => break false,
            }
        }
    }
    /// Tests if the `self` and `other` `Name`s are equivalent when compared case sensitively.
    pub fn sensitive_eq(&self, other: &Name) -> bool {
        if self.len() != other.len() || self.labels().count() != other.labels().count() {
            return false;
        }
        let mut iter_a = self.labels().rev();
        let mut iter_b = other.labels().rev();
        // TODO: this looks like redundant logic
        let eq = (&mut iter_a).zip(&mut iter_b).all(|(a, b)| a == b);
        eq && iter_a.next().is_none() && iter_b.next().is_none()
    }
    pub(self) fn buf(&self) -> &[u8] {
        unsafe {
            match self.storage.header.ext() {
                true => match self.storage.ext_header.kind {
                    ExtKind::Arc => {
                        &self.storage.ext_arc.buf[usize::from(self.storage.ext_arc.offset)..]
                    }
                    ExtKind::Static => self.storage.ext_static.buf,
                },
                false => &self.storage.inline.buf[..usize::from(self.storage.header.len - 1)],
            }
        }
    }
    // TODO: if self == context.last_name(), can emit empty... helper for that?
    // TODO: if self == context.origin(), can emit "@"... probably don't always want to though
    fn write_ascii<'a>(&self, cursor: &'a mut [u8; MAX_STR], context: &Context) -> &'a str {
        let matched_labels = context.origin().and_then(|origin| {
            if origin.is_suffix_sensitive(self) {
                Some(origin.labels().count())
            } else {
                None
            }
        });
        if Some(self.labels().count()) == matched_labels {
            cursor[0] = b'@';
            return unsafe { core::str::from_utf8_unchecked(&cursor[..1]) };
        };
        let mut rest = &mut cursor[..];
        for (i, l) in self
            .labels()
            .rev()
            .take(self.labels().count() - matched_labels.unwrap_or(0))
            .enumerate()
        {
            if i > 0 {
                rest[0] = b'.';
                rest = &mut rest[1..];
            }
            for &b in l.iter() {
                match b {
                    b if !(b' '..=b'~').contains(&b) || b == b' ' => {
                        let ddd = byte_to_ddd(b);
                        let (target, new_rest) = rest.split_at_mut(4);
                        target.copy_from_slice(&ddd[..]);
                        rest = new_rest;
                    }
                    b' ' | b'"' | b'.' | b'(' | b')' | b';' | b'@' | b'\\' => {
                        let (target, new_rest) = rest.split_at_mut(2);
                        target.copy_from_slice(&[b'\\', b]);
                        rest = new_rest;
                    }
                    b => {
                        rest[0] = b;
                        rest = &mut rest[1..];
                    }
                }
            }
        }
        if matched_labels.is_none() {
            rest[0] = b'.';
            rest = &mut rest[1..];
        }
        let len = MAX_STR - rest.len();
        unsafe { core::str::from_utf8_unchecked(&cursor[..len]) }
    }
}

// TODO: is this necessary?
impl<'a> From<&'a Name> for Name {
    fn from(name: &Name) -> Self {
        name.clone()
    }
}

impl Hash for Name {
    fn hash<H>(&self, state: &mut H)
    where
        H: Hasher,
    {
        // TODO: should hash an extra terminating byte?
        for l in self.labels().rev() {
            l.len().hash(state);
            for b in l.iter().map(u8::to_ascii_lowercase) {
                b.hash(state);
            }
        }
        0u8.hash(state);
    }
}

impl PartialEq for Name {
    fn eq(&self, other: &Self) -> bool {
        // TODO: this can use eq_ignore_ascii_case across a slice 'cause 0..=63 doesn't contain
        // letters
        self.len() == other.len()
            && self.labels().count() == other.labels().count()
            && self
                .labels()
                .rev()
                .zip(other.labels().rev())
                .all(|(a, b)| a.eq_ignore_ascii_case(b))
    }
}

// TODO: is this necessary?
impl<'a> PartialEq<Name> for &'a Name {
    fn eq(&self, other: &Name) -> bool {
        <Name as PartialEq<Name>>::eq(self, other)
    }
}

// TODO: is this necessary?
impl<'a> PartialEq<&'a Name> for Name {
    fn eq(&self, other: &&'a Name) -> bool {
        <Name as PartialEq<Name>>::eq(self, other)
    }
}

impl Display for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut buf = [0u8; MAX_STR];
        let s = self.write_ascii(&mut buf, Context::shared_default());
        f.pad(s)
    }
}

impl Debug for Name {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        Display::fmt(self, f)
    }
}

impl FromStr for Name {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::from_str_origin(s, None)
    }
}

impl Eq for Name {}

impl PartialOrd<Name> for Name {
    fn partial_cmp(&self, other: &Name) -> Option<Ordering> {
        Some(<Name as Ord>::cmp(self, other))
    }
}

impl<'a> PartialOrd<Name> for &'a Name {
    fn partial_cmp(&self, other: &Name) -> Option<Ordering> {
        Some(<Name as Ord>::cmp(self, other))
    }
}

fn label_ord(a: &[u8], b: &[u8]) -> Ordering {
    let mut a_bytes = a.iter().map(u8::to_ascii_lowercase);
    let mut b_bytes = b.iter().map(u8::to_ascii_lowercase);
    loop {
        match (a_bytes.next(), b_bytes.next()) {
            (None, None) => break Ordering::Equal,
            (None, Some(_)) => break Ordering::Less,
            (Some(_), None) => break Ordering::Greater,
            (Some(a), Some(b)) => match a.cmp(&b) {
                Ordering::Equal => (),
                other => break other,
            },
        }
    }
}

impl Ord for Name {
    fn cmp(&self, other: &Name) -> Ordering {
        let mut a = self.labels();
        let mut b = other.labels();
        loop {
            match (a.next(), b.next()) {
                (None, None) => break Ordering::Equal,
                (None, Some(_)) => break Ordering::Less,
                (Some(_), None) => break Ordering::Greater,
                (Some(a), Some(b)) => match label_ord(a, b) {
                    Ordering::Equal => (),
                    other => break other,
                },
            }
        }
    }
}

impl Present for Name {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        let mut buf = [0u8; MAX_STR];
        let s = self.write_ascii(&mut buf, p.context());
        p.write(s)
    }
}

impl Parse for Name {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let origin = p.context().origin();
        p.pull(|s| Self::from_str_origin(crate::ascii::parse::pop_quotes(s), origin))
    }
}

fn label_cmp_wire(a: &[u8], b: &[u8]) -> Ordering {
    match a.len().cmp(&b.len()) {
        Ordering::Equal => a.cmp(b),
        other => other,
    }
}

fn label_cmp_wire_canon(a: &[u8], b: &[u8]) -> Ordering {
    match a.len().cmp(&b.len()) {
        Ordering::Equal => {
            let a_bytes = a.iter().map(u8::to_ascii_lowercase);
            let b_bytes = b.iter().map(u8::to_ascii_lowercase);
            for (a, b) in a_bytes.zip(b_bytes) {
                match a.cmp(&b) {
                    Ordering::Equal => (),
                    other => return other,
                }
            }
            Ordering::Equal
        }
        other => other,
    }
}

impl WireOrd for Name {
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        let mut iter_l = WireLabelIter::new(left);
        let mut iter_r = WireLabelIter::new(right);
        loop {
            let opt_l = iter_l.next_label().map_err(WireOrdError::Left)?;
            let opt_r = iter_r.next_label().map_err(WireOrdError::Right)?;
            match (opt_l, opt_r) {
                (None, None) => return Ok(Ordering::Equal),
                (None, Some(_)) => return Ok(Ordering::Less),
                (Some(_), None) => return Ok(Ordering::Greater),
                (Some(l), Some(r)) => match label_cmp_wire(l, r) {
                    Ordering::Equal => (),
                    other => return Ok(other),
                },
            }
        }
    }
    fn cmp_wire_canonical(
        left: &mut Reader<'_>,
        right: &mut Reader<'_>,
    ) -> Result<Ordering, WireOrdError> {
        let mut iter_l = WireLabelIter::new(left);
        let mut iter_r = WireLabelIter::new(right);
        loop {
            let opt_l = iter_l.next_label().map_err(WireOrdError::Left)?;
            let opt_r = iter_r.next_label().map_err(WireOrdError::Right)?;
            match (opt_l, opt_r) {
                (None, None) => return Ok(Ordering::Equal),
                (None, Some(_)) => return Ok(Ordering::Less),
                (Some(_), None) => return Ok(Ordering::Greater),
                (Some(l), Some(r)) => match label_cmp_wire_canon(l, r) {
                    Ordering::Equal => (),
                    other => return Ok(other),
                },
            }
        }
    }
    // TODO: is cmp_as_wire actually useful for something?
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        let mut siter = self.labels().rev();
        let mut oiter = other.labels().rev();
        loop {
            match (siter.next(), oiter.next()) {
                (None, None) => break Ordering::Equal,
                (None, Some(_)) => break Ordering::Less,
                (Some(_), None) => break Ordering::Greater,
                (Some(s), Some(o)) => {
                    let slen = s.len() as u8;
                    let olen = o.len() as u8;
                    match slen.cmp(&olen) {
                        Ordering::Equal => (),
                        other => break other,
                    }
                    match s.cmp(o) {
                        Ordering::Equal => (),
                        other => break other,
                    }
                }
            }
        }
    }
    fn cmp_as_wire_canonical(&self, other: &Self) -> Ordering {
        let mut siter = self.labels().rev();
        let mut oiter = other.labels().rev();
        loop {
            match (siter.next(), oiter.next()) {
                (None, None) => return Ordering::Equal,
                (None, Some(_)) => return Ordering::Less,
                (Some(_), None) => return Ordering::Greater,
                (Some(s), Some(o)) => {
                    let slen = s.len() as u8;
                    let olen = o.len() as u8;
                    match slen.cmp(&olen) {
                        Ordering::Equal => (),
                        other => break other,
                    }
                    for (a, b) in s
                        .iter()
                        .map(u8::to_ascii_lowercase)
                        .zip(o.iter().map(u8::to_ascii_lowercase))
                    {
                        match a.cmp(&b) {
                            Ordering::Equal => (),
                            other => return other,
                        }
                    }
                }
            }
        }
    }
}

impl UnpackedLen for Name {
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        Name::unpack(reader).map(|n| n.len())
    }
}

impl Pack for Name {
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        pack(dst, self)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        Ok(self.len())
    }
}

impl<'a> Unpack<'a> for Name {
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError>
    where
        Self: Sized,
    {
        let mut builder = Builder::default();
        let mut wire_labels = WireLabelIter::new(reader);
        while let Some(l) = wire_labels.next_label()? {
            // TODO: infallible?
            builder.push(l).unwrap();
        }
        Ok(builder.build())
    }
}

// TODO: should this include the root? what is this useful for?
#[doc = r#"
An iterator over a `Name`s ascendants starting from the first name
below the root name.
```
use yardi::datatypes::name;
let com = name!("com.");
let example_com = name!("example.com.");
let mut iter = example_com.names();
assert_eq!(iter.next(), Some(com));
// TODO: shouldn't have to clone here
assert_eq!(iter.next(), Some(example_com.clone()));
assert!(iter.next().is_none());
```
"#]
#[derive(Debug, Clone)]
pub struct Names<'a> {
    name: &'a Name,
    cursor_forward: u8,
    cursor_backward: u8,
}

impl<'a> Iterator for Names<'a> {
    type Item = Name;
    fn next(&mut self) -> Option<Self::Item> {
        if self.cursor_forward < self.cursor_backward {
            let name = self.name.cut(self.cursor_forward as usize + 1);
            debug_assert!(name.is_some());
            self.cursor_forward += 1;
            name
        } else {
            None
        }
    }
    fn nth(&mut self, n: usize) -> Option<Self::Item> {
        self.cursor_forward = self
            .cursor_forward
            .saturating_add(n.try_into().unwrap_or(0xFF));
        self.next()
    }
}

impl<'a> DoubleEndedIterator for Names<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        if self.cursor_backward > self.cursor_forward {
            let name = self.name.cut(self.cursor_backward as usize);
            debug_assert!(name.is_some());
            self.cursor_backward -= 1;
            name
        } else {
            None
        }
    }
    fn nth_back(&mut self, n: usize) -> Option<Self::Item> {
        self.cursor_backward = self
            .cursor_backward
            .saturating_sub(n.try_into().unwrap_or(0xFF));
        self.next_back()
    }
}

#[test]
fn test_names() {
    let n = name!("www.example.com.");
    {
        let mut names = n.names();
        assert_eq!(names.next(), Some(name!("com.")));
        assert_eq!(names.next(), Some(name!("example.com.")));
        assert_eq!(names.next(), Some(name!("www.example.com.")));
        assert!(names.next().is_none());
    }
    {
        let mut names = n.names().skip(2);
        assert_eq!(names.next(), Some(name!("www.example.com.")));
        assert!(names.next().is_none());
    }
    {
        let mut names = n.names();
        assert_eq!(names.next_back(), Some(name!("www.example.com.")));
        assert_eq!(names.next_back(), Some(name!("example.com.")));
        assert_eq!(names.next_back(), Some(name!("com.")));
        assert!(names.next_back().is_none());
    }
    for (i, expect) in n.names().enumerate() {
        assert_eq!(Some(expect), n.names().nth(i));
    }
    for (i, expect) in n.names().rev().enumerate() {
        assert_eq!(Some(expect), n.names().nth_back(i));
    }
}

#[doc = r#"
A double-ended iterator over immutable references to the `Label` contained in a `Name`.
```
use yardi::datatypes::name;
let www_example_com = name!("www.example.com.");
let mut iter = www_example_com.labels();
assert_eq!(iter.next(), Some("com".as_bytes()));
assert_eq!(iter.next_back(), Some("www".as_bytes()));
assert_eq!(iter.next(), Some("example".as_bytes()));
assert!(iter.next_back().is_none());
```
"#]
// TODO: this needs to be reimplemented without unsafe
// TODO: implement ExactSizeIterator
#[derive(Clone, Debug)]
pub struct Labels<'a> {
    buf: &'a [u8],
    cursor: usize,
    labels: u8,
}

// TODO: impl last, nth
impl<'a> Iterator for Labels<'a> {
    type Item = &'a [u8];
    fn next(&mut self) -> Option<Self::Item> {
        if self.labels > 0 {
            self.labels -= 1;
            let mut skip = self.labels;
            let mut cursor = self.cursor;
            assert!(cursor + 1 < self.buf.len());
            while skip > 0 || self.buf[cursor] & PTR_MASK == PTR_MASK {
                match self.buf[cursor] {
                    0 => unreachable!(),
                    n if n <= 63 => {
                        skip -= 1;
                        cursor += n as usize + 1;
                        assert!(cursor + 1 < self.buf.len());
                    }
                    ptr_a if PTR_MASK & ptr_a == PTR_MASK => {
                        let ptr_b = self.buf[cursor + 1];
                        let ptr = decode_ptr([ptr_a, ptr_b]);
                        cursor = ptr;
                        assert!(cursor + 1 < self.buf.len());
                    }
                    _ => unreachable!(),
                }
            }
            let len = self.buf[cursor] as usize;
            assert!(len > 0 && len <= 63);
            cursor += 1;
            let start = cursor;
            cursor += len;
            assert!(cursor <= self.buf.len());
            Some(&self.buf[start..cursor])
        } else {
            None
        }
    }
    fn count(self) -> usize {
        self.labels as usize
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        if self.labels > 0 {
            (1, Some(self.labels as usize))
        } else {
            // TODO: is this correct?
            (0, None)
        }
    }
}

// TODO: impl nth_back
impl<'a> DoubleEndedIterator for Labels<'a> {
    fn next_back(&mut self) -> Option<Self::Item> {
        while self.labels > 0 {
            assert!(self.cursor + 1 < self.buf.len());
            let b = self.buf[self.cursor];
            match b {
                0 => unreachable!(),
                len if len <= 63 => {
                    self.labels -= 1;
                    let start = self.cursor + 1;
                    let next = len as usize + self.cursor + 1;
                    debug_assert!(next <= self.buf.len());
                    self.cursor = next;
                    return Some(&self.buf[start..next]);
                }
                ptr_a if PTR_MASK & ptr_a == PTR_MASK => {
                    let ptr_b = self.buf[self.cursor + 1];
                    let ptr = decode_ptr([ptr_a, ptr_b]);
                    self.cursor = ptr;
                }
                _ => unreachable!(),
            }
        }
        None
    }
}

// TODO: track initial position for marking errors
// TODO: use a drop impl to reset start position instead of
//       a second reader
struct WireLabelIter<'a, 'b> {
    src: &'a mut Reader<'b>,
    src_ptr: Option<Reader<'b>>,
    done: bool,
    last_start: usize,
    len: u8,
    labels: u8,
}

impl<'a, 'b> WireLabelIter<'a, 'b> {
    fn new(src: &'a mut Reader<'b>) -> Self {
        let index = src.index();
        WireLabelIter {
            src,
            src_ptr: None,
            done: false,
            last_start: index,
            len: 1, // start at 1 to reserve space for the empty label
            labels: 0,
        }
    }
    fn next_label(&mut self) -> Result<Option<&'b [u8]>, UnpackError> {
        // TODO: capture initial index to use in errors
        self.done = true;
        loop {
            let r = self.src_ptr.as_mut().unwrap_or(self.src);
            match LabelHeader::unpack(r)? {
                LabelHeader::Len(0) => break,
                LabelHeader::Len(len) => {
                    let index = r.index();
                    r.skip(len as usize)?;
                    self.len = self
                        .len
                        .checked_add(1 + len)
                        .ok_or_else(UnpackError::value_large)?;
                    self.labels += 1;
                    self.done = false;
                    let label = &self.src.buffer()[index..index + len as usize];
                    return Ok(Some(label));
                }
                LabelHeader::Pointer(ptr) => {
                    let ptr = ptr as usize;
                    if ptr >= self.last_start {
                        // TODO: set the index of the error
                        return Err(UnpackError::value_invalid_name_pointer());
                    }
                    let mut new_reader = self.src.clone();
                    new_reader.set_index(ptr);
                    new_reader.truncate(self.last_start).unwrap();
                    self.last_start = ptr;
                    self.src_ptr = Some(new_reader);
                }
            }
        }
        Ok(None)
    }
}

fn pack(writer: &mut Writer<'_>, name: &Name) -> Result<(), PackError> {
    if name.labels().count() == 0 {
        return writer.write_bytes(&[0]);
    }
    // TODO: probably better to reference the fn directly when the style is set
    match writer.infer_name_style() {
        Style::Canonical => pack_canonical(writer, name),
        Style::Compress => pack_compress::<PackInsensitive>(writer, name),
        Style::CompressSensitive => pack_compress::<PackSensitive>(writer, name),
        Style::Plain => pack_plain(writer, name),
    }
}

fn pack_canonical(writer: &mut Writer<'_>, name: &Name) -> Result<(), PackError> {
    for l in name.labels().rev() {
        let t = writer.write_forward(1 + l.len())?;
        t[0] = l.len() as u8;
        for (d, b) in t[1..].iter_mut().zip(l.iter().map(u8::to_ascii_lowercase)) {
            *d = b;
        }
    }
    // no point dropping a compression mark if this is a canonical buffer
    writer.write_bytes(&[0])
}

trait PackCompare {
    fn pc_eq(a: &[u8], b: &[u8]) -> bool;
    fn pc_ne(a: &[u8], b: &[u8]) -> bool {
        !Self::pc_eq(a, b)
    }
}

struct PackInsensitive;

impl PackCompare for PackInsensitive {
    fn pc_eq(a: &[u8], b: &[u8]) -> bool {
        a.eq_ignore_ascii_case(b)
    }
}

struct PackSensitive;

impl PackCompare for PackSensitive {
    fn pc_eq(a: &[u8], b: &[u8]) -> bool {
        a == b
    }
}

fn pack_compress<C>(writer: &mut Writer<'_>, name: &Name) -> Result<(), PackError>
where
    C: PackCompare,
{
    fn to_ptr(index: usize) -> [u8; 2] {
        debug_assert!(index <= PTR_MAX);
        [PTR_MASK | (index >> 8) as u8, index as u8]
    }
    let labels = name.labels().count();
    let mut matched_labels = 0;
    let mut partial_ptr = None;
    let mut full_ptr = None;
    for i in writer.name_index().iter().map(|&n| n as usize) {
        let buf = writer.written();
        let match_from = match buf[i] {
            0 => 0,
            a if Some([a, buf[i + 1]]) == partial_ptr => matched_labels,
            _ => continue,
        };
        match match_down::<C>(buf, name, match_from, i) {
            (n, index) if n == name.labels().count() => {
                full_ptr = Some(to_ptr(index));
                break;
            }
            (n, index) if n > matched_labels => {
                matched_labels = n;
                partial_ptr = Some(to_ptr(index));
            }
            _ => (),
        }
    }
    if let Some(ptr) = full_ptr {
        return writer.write_bytes(&ptr[..]);
    }
    if let Some(ptr) = partial_ptr {
        for l in name.labels().rev().take(labels - matched_labels) {
            writer.write_bytes8(l)?;
        }
        let new_i = writer.len();
        writer.write_bytes(&ptr[..])?;
        writer.name_index_push(new_i);
        Ok(())
    } else {
        pack_plain(writer, name)
    }
}

fn match_down<C>(buf: &[u8], name: &Name, mut from_label: usize, mut index: usize) -> (usize, usize)
where
    C: PackCompare,
{
    for l in name.labels().skip(from_label) {
        let new_index = match index.checked_sub(1 + l.len()) {
            Some(i) => i,
            None => break,
        };
        if buf[new_index] != l.len() as u8 || <C>::pc_ne(l, &buf[new_index + 1..index]) {
            break;
        }
        index = new_index;
        from_label += 1;
    }
    (from_label, index)
}

fn pack_plain(writer: &mut Writer<'_>, name: &Name) -> Result<(), PackError> {
    for l in name.labels().rev() {
        writer.write_bytes8(l)?;
    }
    let new_i = writer.len();
    writer.write_bytes(&[0])?;
    writer.name_index_push(new_i);
    Ok(())
}

#[test]
fn for_hn() {
    let buf = &[1, b'a', 1, b'b', 0xC0, 00];
    let mut r = Reader::new(buf);
    assert!(Name::unpack(&mut r).is_err());
}

#[test]
fn cow_parent() {
    let mut n = Name::new();
    n.push(&[b'a'; 63][..]).unwrap();
    n.push(&[b'b'; 63][..]).unwrap();
    n.push(&[b'c'; 63][..]).unwrap();
    let mut p = n.parent().unwrap();
    fn get_ptr(n: &Name) -> Arc<[u8]> {
        unsafe {
            assert!(n.storage.header.ext());
            assert_eq!(n.storage.ext_header.kind, ExtKind::Arc);
            Arc::clone(&n.storage.ext_arc.buf)
        }
    }
    let n_arc = get_ptr(&n);
    let p_arc = get_ptr(&p);
    assert!(Arc::ptr_eq(&n_arc, &p_arc));
    p.push(&[b'c'; 62]).unwrap();
    let p_arc = get_ptr(&p);
    assert!(!Arc::ptr_eq(&n_arc, &p_arc));
}

#[test]
fn comparators() {
    let a = name!("a.b.");
    let b = name!("B.a.");
    assert_eq!(a.cmp(&b), Ordering::Greater);
    assert_eq!(a.cmp_as_wire(&b), Ordering::Greater);
    assert_eq!(a.cmp_as_wire_canonical(&b), Ordering::Less);

    let c = name!("a.");
    let d = name!("A.");
    assert_eq!(c.cmp(&d), Ordering::Equal);
    assert_eq!(c.cmp_as_wire(&d), Ordering::Greater);
    assert_eq!(c.cmp_as_wire_canonical(&d), Ordering::Equal);
}
