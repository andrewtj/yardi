use core::fmt::Debug;

use crate::wire::{Unpack, UnpackError};

/// Maximum length of a label in wire format.
pub const MAX: usize = 63;
/// Maximum length of a label in ASCII.
pub const MAX_STR: usize = MAX * 4;
pub(super) const PTR_MASK: u8 = 0b1100_0000;

pub(super) fn decode_ptr(ptr: [u8; 2]) -> usize {
    debug_assert_eq!(PTR_MASK & ptr[0], PTR_MASK);
    u16::from_be_bytes([!PTR_MASK & ptr[0], ptr[1]]) as usize
}

#[derive(Debug)]
pub(crate) enum LabelHeader {
    Len(u8),
    Pointer(u16),
}

impl<'a> Unpack<'a> for LabelHeader {
    fn unpack(reader: &mut crate::wire::Reader<'a>) -> Result<Self, UnpackError> {
        match reader.get_byte()? {
            len @ 0..=63 => Ok(LabelHeader::Len(len)),
            ptr_a if PTR_MASK & ptr_a == PTR_MASK => {
                let ptr_b = reader.get_byte()?;
                let ptr = u16::from_be_bytes([!PTR_MASK & ptr_a, ptr_b]);
                Ok(LabelHeader::Pointer(ptr))
            }
            _ => Err(UnpackError::value_unknown_label_header()),
        }
    }
}
