#[cfg(test)]
mod test;

use core::{cmp::Ordering, str::FromStr};

#[cfg(feature = "std")]
use std::time::SystemTime;

use crate::ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter};

// http://howardhinnant.github.io/date_algorithms.html#days_from_civil
fn days_from_civil(mut y: u32, m: u32, d: u32) -> u32 {
    y -= (m <= 2) as u32;
    let era = y / 400;
    let yoe = y - era * 400;
    let doy_t1 = if m > 2 { m - 3 } else { m + 9 };
    let doy_t2 = 153 * doy_t1 + 2;
    let doy = doy_t2 / 5 + d - 1;
    let doe = yoe * 365 + yoe / 4 - yoe / 100 + doy;
    era * 146097 + doe - 719468
}

// http://howardhinnant.github.io/date_algorithms.html#civil_from_days
fn civil_from_days(mut z: u32) -> (u32, u32, u32) {
    z += 719468;
    let era = z / 146097;
    let doe = z - era * 146097; // [0, 146096]
    let yoe = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365; // [0, 399]
    let y = yoe + era * 400;
    let doy = doe - (365 * yoe + yoe / 4 - yoe / 100); // [0, 365]
    let mp = (5 * doy + 2) / 153; // [0, 11]
    let d = doy - (153 * mp + 2) / 5 + 1; // [1, 31]
    let m = if mp < 10 { mp + 3 } else { mp - 9 }; // [1, 12]
    (y + (m <= 2) as u32, m, d)
}

fn is_leap_year(year: u32) -> bool {
    (year % 4 == 0) & ((year % 100 != 0) | (year % 400 == 0))
}

fn parse_time14(s: &str) -> Result<u32, ParseError> {
    if s.len() != 14 || s.as_bytes().iter().any(|&x| x.wrapping_sub(b'0') > 9) {
        return Err(ParseError::value_invalid());
    }
    let year: u32 = s[..4].parse().map_err(|_| ParseError::value_invalid())?;
    if !(1970..=2106).contains(&year) {
        return Err(ParseError::value_invalid());
    }
    let month: u32 = s[4..6].parse().map_err(|_| ParseError::value_invalid())?;
    let days_in_month = match month {
        1 => 31,
        2 if is_leap_year(year) => 29,
        2 => 28,
        3 => 31,
        4 => 30,
        5 => 31,
        6 => 30,
        7 => 31,
        8 => 31,
        9 => 30,
        10 => 31,
        11 => 30,
        12 => 31,
        _ => return Err(ParseError::value_invalid()),
    };
    if !(1..=12).contains(&month) {
        return Err(ParseError::value_invalid());
    }
    let day: u32 = s[6..8].parse().map_err(|_| ParseError::value_invalid())?;
    if day < 1 || day > days_in_month {
        return Err(ParseError::value_invalid());
    }
    let hour: u32 = s[8..10].parse().map_err(|_| ParseError::value_invalid())?;
    if hour > 23 {
        return Err(ParseError::value_invalid());
    }
    let minute: u32 = s[10..12].parse().map_err(|_| ParseError::value_invalid())?;
    if minute > 59 {
        return Err(ParseError::value_invalid());
    }
    let second: u32 = s[12..14].parse().map_err(|_| ParseError::value_invalid())?;
    if second > 59 {
        return Err(ParseError::value_invalid());
    }
    if year == 2106 {
        let cur: [u32; 5] = [month, day, hour, minute, second];
        let max: [u32; 5] = [2, 7, 6, 28, 15];
        for i in 0..5 {
            match cur[i].cmp(&max[i]) {
                Ordering::Less => break,
                Ordering::Equal => {}
                Ordering::Greater => return Err(ParseError::value_invalid()),
            }
        }
    }
    Ok((days_from_civil(year, month, day) * 86400) + (hour * 3600) + (minute * 60) + second)
}

// TODO: Helpers for working with SystemTime.
#[derive(
    Copy, Clone, Default, PartialEq, Eq, PartialOrd, Ord, Pack, Unpack, UnpackedLen, WireOrd,
)]
#[yardi(crate = "crate")]
/// A 32-bit unsigned number of seconds elapsed since 1 January 1970 00:00:00 UTC, ignoring leap seconds.
pub struct Time(pub u32);

impl Time {
    /// Returns a `Time` representing the current time.
    #[cfg(feature = "std")]
    pub fn now() -> Self {
        // TODO: gracefully handle a bad clock
        let now = SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap()
            .as_secs();
        Time((now % u64::from(u32::MAX)) as u32)
    }
    /// Returns a pair of `Time` representing self less `fudge` and `self`
    /// plus `fudge`.
    pub fn window(self, fudge: u32) -> (Self, Self) {
        self.offset(fudge, fudge)
    }
    /// Returns a pair of `Time` representing `self` less `back`
    /// and `self` plus `forward`.
    pub fn offset(self, back: u32, forward: u32) -> (Self, Self) {
        let incept = Time(self.0.wrapping_sub(back));
        let expire = Time(self.0.wrapping_add(forward));
        (incept, expire)
    }
}

impl FromStr for Time {
    type Err = ParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s.len() {
            14 => parse_time14(s).map(Time),
            1..=10 => crate::datatypes::int::parse_u32(s).map(Time),
            _ => Err(ParseError::value_invalid()),
        }
    }
}

impl core::fmt::Display for Time {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        let remsecs = self.0 % 86400;
        let hour = remsecs / 3600;
        let min = remsecs / 60 % 60;
        let sec = remsecs % 60;
        let days = self.0 / 86400;
        let (year, month, day) = civil_from_days(days);
        write!(
            f,
            "{:04}{:02}{:02}{:02}{:02}{:02}",
            year, month, day, hour, min, sec
        )
    }
}

impl core::fmt::Debug for Time {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        core::fmt::Display::fmt(self, f)
    }
}

impl Parse for Time {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(Time::from_str)
    }
}

// TODO: option to present in 14 digit format where allowed
impl Present for Time {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        write!(p, "{}", self)
    }
}
