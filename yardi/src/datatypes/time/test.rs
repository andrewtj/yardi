use core::str::FromStr;

use super::Time;

#[test]
fn test_parse_time() {
    assert!(Time::from_str("").is_err());
    assert!(Time::from_str("2003032217310").is_err());
    assert!(Time::from_str("2003032217310a").is_err());
    assert!(Time::from_str("200303221731 0").is_err());
    assert!(Time::from_str("19691231235959").is_err());
    assert!(Time::from_str("21060207062816").is_err());
    assert_eq!(Time(1456812000), Time::from_str("20160301060000").unwrap());
    assert_eq!(Time(1048354263), Time::from_str("20030322173103").unwrap());
    assert_eq!(
        Time(u32::min_value()),
        Time::from_str("19700101000000").unwrap()
    );
    assert_eq!(
        Time(u32::max_value()),
        Time::from_str("21060207062815").unwrap()
    );
}
