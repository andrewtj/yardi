//! Support for parsing and presenting DNS datastructures in ASCII presentation format.
mod context;
pub mod parse;
pub mod present;

pub use self::context::Context;
pub use self::parse::{Parse, ParseError, ParseErrorKind, Parser};
pub use self::present::{Present, PresentError, PresentErrorKind, Presenter};
