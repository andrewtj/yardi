use super::{
    super::{Comment, ParseOptions, Token},
    identify,
};

#[test]
fn newline() {
    let o = ParseOptions::default();
    {
        let case = "\r";
        identify(case.as_ref(), o, true).expect_err(case);
        assert_eq!(None, identify(case.as_ref(), o, false).expect(case));
    };
    let cases = &[("\r\n", 2), ("\r\n\x00", 2), ("\n", 1)];
    for &(s, l) in cases {
        assert_eq!(
            Some((Token::Newline, l)),
            identify(s.as_ref(), o, false).expect(s)
        );
        assert_eq!(
            Some((Token::Newline, l)),
            identify(s.as_ref(), o, true).expect(s)
        );
    }
}

#[test]
fn comment() {
    let o = ParseOptions::default();
    let cases = &[
        (";", None, Some(1)),
        (";\r", Some(1), Some(1)),
        ("; \r", Some(2), Some(2)),
    ];
    for &(s, a, b) in cases {
        assert_eq!(
            a.map(|l| (Token::Comment, l)),
            identify(s.as_ref(), o, false).expect(s)
        );
        assert_eq!(
            b.map(|l| (Token::Comment, l)),
            identify(s.as_ref(), o, true).expect(s)
        );
    }
    {
        let case = ";\x00\r";
        identify(case.as_ref(), o, true).expect_err(case);
    }
}

#[test]
fn quoted_str() {
    let o = ParseOptions::default();
    {
        let case = "\"";
        assert_eq!(None, identify(case.as_ref(), o, false).expect(case));
        identify(case.as_ref(), o, true).expect_err(case);
    }
    {
        let case = "\"\"";
        let expect = Some((Token::Text, 2));
        assert_eq!(expect, identify(case.as_ref(), o, false).expect(case));
        assert_eq!(expect, identify(case.as_ref(), o, true).expect(case));
    }
    {
        let case = "\" \"";
        let expect = Some((Token::Text, 3));
        assert_eq!(expect, identify(case.as_ref(), o, false).expect(case));
        assert_eq!(expect, identify(case.as_ref(), o, true).expect(case));
    }
    {
        let case = "\"\\\"";
        assert_eq!(None, identify(case.as_ref(), o, false).expect(case));
        identify(case.as_ref(), o, true).expect_err(case);
    }
    {
        let case = "\"\\\"\"";
        let expect = Some((Token::Text, 4));
        assert_eq!(expect, identify(case.as_ref(), o, false).expect(case));
        assert_eq!(expect, identify(case.as_ref(), o, true).expect(case));
    }
    {
        let case = "\"\x00\"";
        assert!(identify(case.as_ref(), o, false).is_err());
        identify(case.as_ref(), o, true).expect_err(case);
    }
}

#[test]
fn text() {
    let mut o = ParseOptions::default();
    o.set_comment(Comment::Text).set_ignore_parenthesis(true);
    let cases: &[&str] = &[";", "(", ")", "a\\(b"];
    for case in cases {
        assert_eq!(None, identify(case.as_ref(), o, false).expect(case));
        assert_eq!(
            Some((Token::Text, case.len())),
            identify(case.as_ref(), o, true).expect(case)
        );
    }
    {
        let case = "tree ";
        let expect = Some((Token::Text, 4));
        assert_eq!(expect, identify(case.as_ref(), o, false).expect(case));
        assert_eq!(expect, identify(case.as_ref(), o, true).expect(case));
    }
    {
        let case = "\x00";
        identify(case.as_ref(), o, false).expect_err(case);
        identify(case.as_ref(), o, true).expect_err(case);
    }
}

#[test]
fn whitespace() {
    let o = ParseOptions::default();
    let cases: &[&str] = &[" \t", "\t "];
    for case in cases {
        assert_eq!(None, identify(case.as_ref(), o, false).expect(case));
        assert_eq!(
            Some((Token::Whitespace, case.len())),
            identify(case.as_ref(), o, true).expect(case)
        );
    }
}
