#[cfg(test)]
mod test;

// TODO: 2021 edition
use super::{Comment, ParseError, ParseOptions, Token};
use core::mem;

pub(super) fn identify(
    buf: &[u8],
    options: ParseOptions,
    eof: bool,
) -> Result<Option<(Token, usize)>, ParseError> {
    match buf.first().copied() {
        None => Ok(None),
        Some(b'\r') | Some(b'\n') => newline(buf, options, eof),
        Some(b';') => match options.comment() {
            Comment::Accept => comment(buf, options, eof),
            Comment::Text => str(buf, options, eof),
            Comment::Reject => Err(ParseError::disallowed_comment()),
        },
        Some(b' ') | Some(b'\t') => whitespace(buf, options, eof),
        Some(b'"') => quoted_str(buf, options, eof),
        Some(b'(') if !options.ignore_parenthesis() => Ok(Some((Token::ParenOpen, 1))),
        Some(b')') if !options.ignore_parenthesis() => Ok(Some((Token::ParenClose, 1))),
        Some(b) if b > b' ' && b <= b'~' => str(buf, options, eof),
        _ => Err(ParseError::bad_ascii()),
    }
}

fn newline(
    buf: &[u8],
    _options: ParseOptions,
    eof: bool,
) -> Result<Option<(Token, usize)>, ParseError> {
    match buf[0] {
        b'\n' => Ok(Some((Token::Newline, 1))),
        b'\r' => match buf.get(1).cloned() {
            Some(b'\n') => Ok(Some((Token::Newline, 2))),
            None if !eof => Ok(None),
            _ => Err(ParseError::dangling_newline()),
        },
        _ => Ok(None),
    }
}

fn comment(
    mut buf: &[u8],
    _options: ParseOptions,
    eof: bool,
) -> Result<Option<(Token, usize)>, ParseError> {
    /*
    let len = buf
        .iter()
        .take_while(|&&b| b == b'\t' || (b' '..=b'~').contains(&b))
        .count();
    match buf.get(len).cloned() {
        None if eof => Ok(Some((Token::Comment, len))),
        None => Ok(None),
        Some(b'\r') | Some(b'\n') => Ok(Some((Token::Comment, len))),
        Some(_) => Err(ParseError::bad_ascii()),
    }
    */
    debug_assert_eq!(buf[0], b';');
    let mut len = 0;
    while let Some(v) = pop_usize(&mut buf) {
        let accept = (_hasbetween(v, b' ' - 1, b'~' + 1) * 0x80) | _hasvalue(v, b'\t');
        let accept = (accept >> 7) * 255;
        let accept = accept.leading_ones() as usize / 8;
        len += accept;
        if accept < size_of::<usize>() {
            let next = (v >> ((size_of::<usize>() - 1 - accept) * 8)) as u8;
            return if matches!(next, b'\n' | b'\r') {
                Ok(Some((Token::Comment, len)))
            } else {
                Err(ParseError::bad_ascii())
            };
        }
    }
    for b in buf.iter().copied() {
        match b {
            b'\r' | b'\n' => return Ok(Some((Token::Comment, len))),
            b'\t' | b' '..=b'~' => len += 1,
            _ => {
                return Err(ParseError::bad_ascii());
            }
        }
    }
    Ok((len > 0 && eof).then_some((Token::Comment, len)))
}

fn quoted_str(
    mut buf: &[u8],
    _options: ParseOptions,
    eof: bool,
) -> Result<Option<(Token, usize)>, ParseError> {
    debug_assert_eq!(buf[0], b'"');
    buf = &buf[1..];
    let mut len = 1;
    let mut escaped = false;
    while let Some(v) = pop_usize(&mut buf) {
        let mut backslashes = _hasvalue(v, b'\\');
        let mut quotes = _hasvalue(v, b'"');
        if mem::take(&mut escaped) {
            let retain = usize::MAX >> 8;
            backslashes &= retain;
            quotes &= retain;
        }
        while backslashes > 0x80 {
            let offset = backslashes.leading_zeros();
            let remove_mask = !((!(usize::MAX >> 8)) >> offset);
            backslashes &= remove_mask;
            let remove_mask = !((!(usize::MAX >> 8)) >> (offset + 8));
            quotes &= remove_mask;
            backslashes &= remove_mask;
            // reject escaped newline or carriage return
        }
        let accept = (_hasbetween(v, b' ' - 1, b'~' + 1) * 0x80) | _hasvalue(v, b'\t');
        let accept = ((accept & !quotes) >> 7) * 255;
        let accept = accept.leading_ones() as usize / 8;
        len += accept;
        if accept < size_of::<usize>() {
            let bit_off = (size_of::<usize>() - (accept + 1)) * 8;
            return match (v >> bit_off) as u8 {
                b'"' => {
                    if (0xFF & (quotes >> bit_off)) != 0 {
                        Ok(Some((Token::Text, len + 1)))
                    } else {
                        Err(ParseError::bad_ascii())
                    }
                }
                b' '..=b'~' | b'\t' => Err(ParseError::dangling_quote()),
                _ => Err(ParseError::bad_ascii()),
            };
        }
        escaped = backslashes > 0;
    }
    for b in buf.iter().copied() {
        match b {
            b'\\' => escaped = !escaped,
            b'"' => {
                if !escaped {
                    return Ok(Some((Token::Text, len + 1)));
                }
                escaped = false
            }
            b' '..=b'~' | b'\t' => escaped = false,
            _ => return Err(ParseError::bad_ascii()),
        }
        len += 1;
    }
    if eof {
        Err(ParseError::dangling_quote())
    } else {
        Ok(None)
    }
}

fn str(
    mut buf: &[u8],
    options: ParseOptions,
    eof: bool,
) -> Result<Option<(Token, usize)>, ParseError> {
    let mut len = 0;
    let mut escaped = false;
    let ignore_comments = options.comment() == Comment::Text;
    let comment_fn = if ignore_comments {
        _has_comment_no_impl
    } else {
        _has_comment_impl
    };
    let ignore_paren = options.ignore_parenthesis();
    let paren_fn = if ignore_paren {
        _has_paren_no_impl
    } else {
        _has_paren_impl
    };
    while let Some(v) = pop_usize(&mut buf) {
        let mut backslashes = _hasvalue(v, b'\\');
        let n_or_r = _hasvalue(v, b'\n') | _hasvalue(v, b'\r');
        let mut seperators = _hasvalue(v, b'\t')
            | _hasvalue(v, b' ')
            | _hasvalue(v, b'"')
            | (comment_fn)(v)
            | (paren_fn)(v)
            | n_or_r;
        if mem::take(&mut escaped) {
            if !(usize::MAX >> 8) & n_or_r > 0 {
                return Err(ParseError::bad_ascii());
            }
            let retain = usize::MAX >> 8;
            backslashes &= retain;
            seperators &= retain;
        }
        let mut bad_n_or_r = usize::MAX;
        while backslashes > 0x80 {
            let remove_mask = !(usize::MAX >> 8) >> backslashes.leading_zeros();
            backslashes &= !remove_mask;
            let remove_mask = !(remove_mask >> 8);
            seperators &= remove_mask;
            backslashes &= remove_mask;
            // reject escaped newline or carriage return
            // TODO: this needs test coverage
            if (!remove_mask) & n_or_r != 0 {
                bad_n_or_r = bad_n_or_r.min(((!remove_mask) & n_or_r).leading_zeros() as usize / 8);
            }
        }
        escaped = backslashes > 0;
        let accept = (_hasbetween(v, b' ' - 1, b'~' + 1) * 0x80) | _hasvalue(v, b'\t');
        let xaccept = accept;
        let accept = ((accept & !seperators) >> 7) * 255;
        let accept = accept.leading_ones() as usize / 8;
        len += accept;
        if bad_n_or_r <= accept {
            // TODO: needs a new error
            return Err(ParseError::bad_ascii());
        }
        if accept < size_of::<usize>() {
            if ((xaccept | n_or_r) & (!(usize::MAX >> 8) >> (accept * 8))) == 0 {
                return Err(ParseError::bad_ascii());
            }
            return Ok((len > 0).then_some((Token::Text, len)));
        }
    }
    for b in buf.iter().copied() {
        match b {
            b'\n' | b'\r' => {
                return if escaped {
                    // TODO: needs a new error
                    Err(ParseError::bad_ascii())
                } else {
                    Ok(Some((Token::Text, len)))
                };
            }
            b'(' | b')' if !escaped && !ignore_paren => {
                return Ok((len > 0).then_some((Token::Text, len)));
            }
            b';' if !escaped && !ignore_comments => {
                return Ok((len > 0).then_some((Token::Text, len)));
            }
            b' ' | b'\t' | b'"' => {
                if !escaped {
                    return Ok((len > 0).then_some((Token::Text, len)));
                }
                escaped = false
            }
            b'\\' => {
                escaped = !escaped;
            }
            b'!'..=b'~' => {
                escaped = false;
            }
            _ => return Err(ParseError::bad_ascii()),
        }
        len += 1;
    }
    if escaped && eof {
        return Err(ParseError::dangling_escape());
    }
    Ok((len > 0 && eof).then_some((Token::Text, len)))
}

fn whitespace(
    mut buf: &[u8],
    _options: ParseOptions,
    eof: bool,
) -> Result<Option<(Token, usize)>, ParseError> {
    let mut len = 0;
    while let Some(v) = pop_usize(&mut buf) {
        let m = (((_hasvalue(v, b'\t') | _hasvalue(v, b' ')) >> 7) * 255).leading_ones() / 8;
        len += m as usize;
        if m < 8 {
            return Ok(Some((Token::Whitespace, len)));
        }
    }
    for b in buf.iter().copied() {
        match b {
            b' ' | b'\t' => len += 1,
            _ => return Ok(Some((Token::Whitespace, len))),
        }
    }
    Ok(eof.then_some((Token::Whitespace, len)))
}

fn _has_comment_impl(v: usize) -> usize {
    _hasvalue(v, b';')
}

fn _has_comment_no_impl(_v: usize) -> usize {
    0
}

fn _has_paren_impl(v: usize) -> usize {
    _hasvalue(v, b'(') | _hasvalue(v, b')')
}

fn _has_paren_no_impl(_v: usize) -> usize {
    0
}

fn pop_usize(s: &mut &[u8]) -> Option<usize> {
    if s.len() < size_of::<usize>() {
        return None;
    }
    let (value, rest) = s.split_at(size_of::<usize>());
    *s = rest;
    Some(usize::from_be_bytes(value.try_into().unwrap()))
}

fn _hasbetween(x: usize, m: u8, n: u8) -> usize {
    debug_assert!(m < n);
    debug_assert!(0 < m && m <= 127);
    debug_assert!(n <= 128);
    /*
    #define hasbetween(x,m,n) \
    ((~0UL/255*(127+(n))-((x)&~0UL/255*127)&~(x)&((x)&~0UL/255*127)+~0UL/255*(127-(m)))&~0UL/255*128)
    */
    let set_first_bit_in_lanes = usize::MAX / 255;
    let set_all_but_last_bit_in_lanes = set_first_bit_in_lanes * 127;
    ((set_first_bit_in_lanes * (127 + usize::from(n)) - ((x) & set_first_bit_in_lanes * 127)
        & !(x)
        & ((x) & set_all_but_last_bit_in_lanes) + set_first_bit_in_lanes * (127 - usize::from(m)))
        & usize::MAX / 255 * 128)
        >> 7
}

#[cfg(all(feature = "std", target_pointer_width = "64"))]
#[test]
fn test_hasbetween() {
    for i in 2..=127 {
        let value = usize::from_be_bytes([0, 0, 0, 0, 0, i - 1, i, i + 1]);
        let out = _hasbetween(value, i - 1, i + 1);
        assert_eq!(out, 0x0100);
    }
    let target = usize::from_be_bytes([b'w', b'w', b'w', b'.', b'1', b'2', b'3', b'4']);
    let accept1 = _hasbetween(target, b'.' + 1, b'\\');
    eprintln!("{:16x}", accept1);
    let accept2 = _hasbetween(target, b'\\', b'~' + 1);
    eprintln!("{:16x}", accept2);
    let accept3 = _hasvalue(target, b'-') >> 7;
    eprintln!("{:16x}", accept3);
    eprintln!("{} wtf", ((accept1 | accept2) / 128) % 255);
    let accept = (accept1 | accept2 | accept3) * 255;
    eprintln!("{:16x}", accept);
    assert_eq!(3, accept.leading_ones() / 8);
}

fn _hasvalue(x: usize, n: u8) -> usize {
    /*
    #define hasvalue(x,n) \
    (haszero((x) ^ (~0UL/255 * (n))))
    */
    _haszero(x ^ (usize::MAX / 255 * usize::from(n)))
}

fn _haszero(v: usize) -> usize {
    if _haszero_fast_but_imprecise(v) > 0 {
        _haszero_slow_but_accurate(v)
    } else {
        0
    }
}

fn _haszero_slow_but_accurate(v: usize) -> usize {
    // bool hasZeroByte = ~((((v & 0x7F7F7F7F) + 0x7F7F7F7F) | v) | 0x7F7F7F7F);
    let m = usize::MAX / 255 * 0x7f;
    !((((v & m) + m) | v) | m)
}

fn _haszero_fast_but_imprecise(v: usize) -> usize {
    /*
    #define haszero(v) (((v) - 0x01010101UL) & ~(v) & 0x80808080UL)
    */
    assert_eq!(u32::MAX / 255, 0x01010101);
    let lsb = usize::MAX / 255;
    assert_eq!(u32::MAX / 255 * 0x80, 0x80808080);
    let msb = lsb * 0x80;
    v.wrapping_sub(lsb) & !v & msb
}
