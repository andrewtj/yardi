//! Support for parsing DNS structures from ASCII presentation format.
pub use crate::errors::{ParseError, ParseErrorKind};
use alloc::borrow::Cow;
#[cfg(not(feature = "std"))]
use alloc::borrow::ToOwned;
#[cfg(feature = "std")]
use std::io;

mod scan;

use super::context::Context;

pub use yardi_derive::Parse;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Token {
    ParenOpen,
    ParenClose,
    Comment,
    Newline,
    Whitespace,
    Text,
}

// TODO: this seems off - why does it assert? what was the intention?
pub(crate) fn pop_quotes(s: &str) -> &str {
    let b = s.as_bytes();
    assert!(!b.is_empty());
    let len = b.len();
    if b[0] == b'"' {
        assert!(len > 1 && b[len - 1] == b'"');
        let b = &b[1..len - 1];
        unsafe { core::str::from_utf8_unchecked(b) }
    } else {
        s
    }
}

/// Create a new instance of a type from a `Parser`.
pub trait Parse {
    /// Parse `Self` from a `Parser`.
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError>
    where
        Self: Sized;
}

impl<'a, T> Parse for Cow<'a, T>
where
    T: ?Sized + ToOwned,
    <T as ToOwned>::Owned: Parse,
{
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let t: <T as ToOwned>::Owned = Parse::parse(p)?;
        Ok(Cow::Owned(t))
    }
}

impl<T: Parse> Parse for Option<T> {
    fn parse(entry: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        if entry.peek()?.is_none() {
            Ok(None)
        } else {
            Ok(Some(T::parse(entry)?))
        }
    }
}

#[cfg(feature = "std")]
impl From<ParseError> for io::Error {
    fn from(err: ParseError) -> io::Error {
        io::Error::new(io::ErrorKind::InvalidData, err)
    }
}

/// The result of a sucessful parse.
#[derive(Debug, PartialEq, Eq)]
pub enum Entry<T> {
    /// End of input reached.
    End,
    /// Parsed an entry containing a value.
    Value {
        /// The amount of input consumed.
        read: usize,
        /// The new position after consuming `read`.
        pos: Position,
        /// The value parsed.
        value: T,
    },
    /// Parsed an entry containing only whitespace or a comment.
    Empty {
        /// The amount of input consumed.
        read: usize,
        /// The new position after consuming `read`.
        pos: Position,
    },
    /// There was insufficient input to parse a complete entry.
    Short,
}

/// Parse a `T` that implements `Parse` from an entry.
pub fn from_entry<T>(
    buf: &[u8],
    position: Position,
    context: &Context,
    eof: bool,
) -> Result<Entry<T>, ParseError>
where
    T: Parse,
{
    from_entry_f(buf, position, context, eof, T::parse)
}

/// Use a function to parse a type from an entry.
// TODO: rename from_entry_with ?
pub fn from_entry_f<F, T>(
    buf: &[u8],
    position: Position,
    context: &Context,
    eof: bool,
    f: F,
) -> Result<Entry<T>, ParseError>
where
    F: FnOnce(&mut Parser<'_, '_>) -> Result<T, ParseError>,
    T: Sized,
{
    if buf.is_empty() {
        return Ok(if eof { Entry::End } else { Entry::Short });
    }
    let mut e = Parser::new(buf, position, context, eof);
    if e.skip_empty_entry()? {
        let read = buf.len() - e.state.buf.len();
        let pos = e.state.position;
        return Ok(Entry::Empty { read, pos });
    }
    let r = f(&mut e).map_err(|err| {
        if err.position().is_none() {
            err.set_position(e.state.position)
        } else {
            err
        }
    });
    if e.state.saw_eob {
        return Ok(Entry::Short);
    }
    match e.close_entry() {
        _ if e.state.saw_eob => return Ok(Entry::Short),
        Err(err) => return Err(err),
        _ => (),
    }
    let value = r?;
    let read = buf.len() - e.state.buf.len();
    let pos = e.state.position;
    Ok(Entry::Value { read, pos, value })
}

/// Parse a single `T` from `buf`.
///
/// Parentheses can be used to continue an entry over multiple lines. Comments
/// on the same line as the entry are accepted. Leading and trailing comments
/// and empty lines are not accepted.
pub fn from_single_entry<T>(buf: &[u8], context: &Context) -> Result<T, ParseError>
where
    T: Parse,
{
    from_single_entry_f(buf, context, T::parse)
}

/// Parse a single `T` from `buf` using a function.
///
/// Parentheses can be used to continue an entry over multiple lines. Comments
/// on the same line as the entry are accepted. Leading and trailing comments
/// and empty lines are not accepted.
pub fn from_single_entry_f<F, T>(buf: &[u8], context: &Context, f: F) -> Result<T, ParseError>
where
    F: Fn(&mut Parser<'_, '_>) -> Result<T, ParseError>,
    T: Sized,
{
    let mut e = Parser::new(buf, Position::default(), context, true);
    if e.skip_empty_entry()? {
        return Err(ParseError::leading_junk());
    }
    let t = f(&mut e).map_err(|err| {
        if err.position().is_none() {
            err.set_position(e.state.position)
        } else {
            err
        }
    })?;
    e.close_entry()?;
    if e.state.buf.is_empty() {
        Ok(t)
    } else {
        Err(ParseError::trailing_junk())
    }
}

/// Skip an entry. This can still fail if `buf` doesn't contain ASCII.
pub fn skip_entry(buf: &[u8], position: Position, eof: bool) -> Result<Entry<&str>, ParseError> {
    if buf.is_empty() {
        return Ok(if eof { Entry::End } else { Entry::Short });
    }
    let mut e = Parser::new(buf, position, Context::shared_default(), eof);
    if e.skip_empty_entry()? {
        let read = buf.len() - e.state.buf.len();
        let pos = e.state.position;
        return Ok(Entry::Empty { read, pos });
    }
    let r = e.pull_rest(|_| Ok(()));
    if e.state.saw_eob {
        return Ok(Entry::Short);
    }
    r?;
    match e.close_entry() {
        _ if e.state.saw_eob => return Ok(Entry::Short),
        Err(err) => return Err(err),
        _ => (),
    }
    let read = buf.len() - e.state.buf.len();
    let value = unsafe { core::str::from_utf8_unchecked(&buf[..read]) };
    let pos = e.state.position;
    Ok(Entry::Value { read, pos, value })
}

/// Comment handling method
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Comment {
    /// Accept comments
    Accept,
    /// Treat comments as regular text
    Text,
    /// Reject comments
    Reject,
}

/// Parsing options
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct ParseOptions {
    ignore_parenthesis: bool,
    comment: Comment,
}

impl ParseOptions {
    pub(crate) const DEFAULT: Self = ParseOptions {
        ignore_parenthesis: false,
        comment: Comment::Accept,
    };
    /// Return comment handling method
    pub fn comment(&self) -> Comment {
        self.comment
    }
    /// Set comment handling method
    pub fn set_comment(&mut self, comment: Comment) -> &mut Self {
        self.comment = comment;
        self
    }
    /// Returns true if parenthesis are treated as regular input
    pub fn ignore_parenthesis(&self) -> bool {
        self.ignore_parenthesis
    }
    /// Set to true to parenthesis as regular input
    pub fn set_ignore_parenthesis(&mut self, ignore: bool) -> &mut Self {
        self.ignore_parenthesis = ignore;
        self
    }
}

impl Default for ParseOptions {
    fn default() -> ParseOptions {
        ParseOptions::DEFAULT
    }
}

/// Parse contents of an entry.
#[derive(Debug)]
pub struct Parser<'a: 'b, 'b> {
    state: Snapshot<'b>,
    context: &'a Context,
    eof: bool,
}

impl<'a, 'b> Parser<'a, 'b> {
    fn new(buf: &'b [u8], position: Position, context: &'a Context, eof: bool) -> Self {
        Parser {
            state: Snapshot {
                position,
                saw_eob: false,
                paren: ParenCounter::default(),
                buf,
                txt_len: None,
                leading_ws: false,
            },
            context,
            eof,
        }
    }
    /// Returns true if the current entry begins with whitespace.
    pub fn leading_whitespace(&mut self) -> Result<bool, ParseError> {
        Ok(self.state.leading_ws)
    }
    /// Returns the next token, if any.
    pub fn peek(&mut self) -> Result<Option<&str>, ParseError> {
        self.next_text()
    }
    /// Calls `f` with the next token in the current entry. If no token is
    /// available a `ParseError` is returned.
    pub fn pull<T, U, F>(&mut self, f: F) -> Result<T, ParseError>
    where
        U: Into<Pull<T>>,
        F: FnOnce(&str) -> U,
    {
        match f(self.next_text_must()?).into() {
            Pull::Accept(t) => {
                self.accept();
                Ok(t)
            }
            Pull::Decline(t) => Ok(t),
            Pull::Reject(err) => Err(if err.position().is_none() {
                err.set_position(self.state.position)
            } else {
                err
            }),
        }
    }
    /// Calls `f` with the tokens remaining in the current entry.
    pub fn pull_rest<F>(&mut self, mut f: F) -> Result<(), ParseError>
    where
        F: FnMut(&str) -> Result<(), ParseError>,
    {
        loop {
            if let Some(s) = self.next_text()? {
                if let Err(err) = f(s) {
                    break Err(if err.position().is_none() {
                        err.set_position(self.state.position)
                    } else {
                        err
                    });
                }
            } else {
                break Ok(());
            }
            self.accept();
        }
    }
    /// Returns a `Snapshot` that can be restored with `restore`.
    pub fn snapshot(&self) -> Snapshot<'b> {
        self.state.clone()
    }
    /// Restores state to `Snapshot` as provided by `snapshot`.
    pub fn restore(&mut self, snapshot: &Snapshot<'b>) {
        self.state.clone_from(snapshot)
    }
    /// Returns a reference to the current parse `Context`.
    pub fn context(&self) -> &'a Context {
        self.context
    }
    fn next_text(&mut self) -> Result<Option<&str>, ParseError> {
        if let Some(txt_len) = self.state.txt_len {
            let b = &self.state.buf[..txt_len];
            let s = unsafe { core::str::from_utf8_unchecked(b) };
            return Ok(Some(s));
        }
        let r = loop {
            match self.scan()? {
                None => break Ok(None),
                Some((Token::Text, len)) => {
                    self.state.txt_len = Some(len);
                    let b = &self.state.buf[..len];
                    let s = unsafe { core::str::from_utf8_unchecked(b) };
                    break Ok(Some(s));
                }
                Some((Token::Newline, _)) if self.state.paren.is_zero() => {
                    break Ok(None);
                }
                Some((tok, len)) => {
                    if let Err(err) = self.state.apply_token(tok, len) {
                        break Err(err);
                    }
                }
            }
        };
        r.map_err(|e| {
            if e.position().is_none() {
                e.set_position(self.state.position)
            } else {
                e
            }
        })
    }
    fn accept(&mut self) {
        let len = self.state.txt_len.take().unwrap();
        self.state.apply_token(Token::Text, len).unwrap();
    }
    fn next_text_must(&mut self) -> Result<&str, ParseError> {
        self.next_text()
            .and_then(|o| o.ok_or_else(ParseError::no_token))
    }
    fn skip_empty_entry(&mut self) -> Result<bool, ParseError> {
        let mut last_empty = None;
        while let Some((tok, len)) = self.scan()? {
            self.state.leading_ws = self.state.leading_ws || tok == Token::Whitespace;
            if tok == Token::Text {
                self.state.txt_len = Some(len);
                break;
            }
            self.state.apply_token(tok, len)?;
            if self.state.paren.is_zero() && (tok == Token::Comment || tok == Token::Newline) {
                if tok == Token::Newline {
                    self.state.leading_ws = false;
                }
                last_empty = Some(self.snapshot());
            } else if self.eof && self.state.buf.is_empty() {
                if self.state.paren.is_zero() {
                    return Ok(true);
                } else {
                    return Err(ParseError::unbalanced_parenthesis());
                }
            }
        }
        if let Some(empty) = last_empty {
            self.restore(&empty);
            Ok(true)
        } else {
            Ok(false)
        }
    }
    fn close_entry(&mut self) -> Result<(), ParseError> {
        while let Some((tok, len)) = self.scan()? {
            match tok {
                Token::Text => return Err(ParseError::trailing_junk()),
                tok @ Token::Newline if self.state.paren.is_zero() => {
                    return self.state.apply_token(tok, len);
                }
                tok => self.state.apply_token(tok, len)?,
            }
        }
        if self.state.paren.is_zero() && self.eof {
            Ok(())
        } else {
            Err(ParseError::unbalanced_parenthesis())
        }
    }
    fn scan(&mut self) -> Result<Option<(Token, usize)>, ParseError> {
        let r = scan::identify(self.state.buf, *self.context.parse_options(), self.eof);
        if Ok(None) == r && !self.eof {
            self.state.saw_eob = true;
        }
        r
    }
}

/// Contains restorable parse state.
#[derive(Debug, Clone)]
pub struct Snapshot<'a> {
    position: Position,
    saw_eob: bool,
    paren: ParenCounter,
    buf: &'a [u8],
    txt_len: Option<usize>,
    leading_ws: bool,
}

impl<'a> Snapshot<'a> {
    fn apply_token(&mut self, token: Token, len: usize) -> Result<(), ParseError> {
        let new_buf = &self.buf[len..];
        match token {
            Token::ParenOpen => self.paren.inc()?,
            Token::ParenClose => self.paren.dec()?,
            _ => (),
        }
        self.buf = new_buf;
        if token == Token::Newline {
            self.position.inc_row(len);
        } else {
            self.position.inc_col(len);
        }
        Ok(())
    }
}

#[derive(Clone, Copy, Debug, Default)]
struct ParenCounter(u8);

impl ParenCounter {
    fn inc(&mut self) -> Result<(), ParseError> {
        self.0 = self
            .0
            .checked_add(1)
            .ok_or_else(ParseError::parenthesis_overflow)?;
        Ok(())
    }
    fn dec(&mut self) -> Result<(), ParseError> {
        self.0 = self
            .0
            .checked_sub(1)
            .ok_or_else(ParseError::unbalanced_parenthesis)?;
        Ok(())
    }
    fn is_zero(self) -> bool {
        self.0 == 0
    }
}

/// Tracks the position in parsed input.
///
/// The members of this struct saturate when incremented beyond their maximum
/// value.
#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
pub struct Position {
    /// Row.
    pub row: u32,
    /// Column.
    pub col: u32,
    /// Byte.
    pub byte: u64,
}

impl Position {
    fn inc_col(&mut self, amt: usize) {
        self.byte = self.byte.saturating_add(amt as u64);
        self.col = if amt < (u32::MAX as usize) {
            self.col.saturating_add(amt as u32)
        } else {
            u32::MAX
        };
    }
    fn inc_row(&mut self, amt: usize) {
        self.byte = self.byte.saturating_add(amt as u64);
        self.col = 0;
        self.row = self.row.saturating_add(1);
    }
}

// TODO: can std::ops::ControlFlow replace this?
/// Returned to `Parser::pull` to indicate whether to accept or reject a token.
#[derive(Clone, Debug)]
pub enum Pull<T> {
    /// Accept the token and return `Ok(T)`.
    Accept(T),
    /// Reject the token and return `Ok(T)`.
    Decline(T),
    /// Reject the token and return a `Err(ParseError)`.
    Reject(ParseError),
}

impl<T> From<Result<T, ParseError>> for Pull<T> {
    fn from(result: Result<T, ParseError>) -> Pull<T> {
        match result {
            Ok(t) => Pull::Accept(t),
            Err(err) => Pull::Reject(err),
        }
    }
}

#[test]
fn trailing_blanks_are_fine() {
    use crate::ascii::ParseError;
    let position = Default::default();
    let context = Default::default();
    assert!(matches!(
        from_entry::<u8>(b" ", position, &context, true),
        Ok(Entry::Empty { read: 1, .. })
    ));
    assert!(matches!(
        from_entry::<u8>(b" ", position, &context, false),
        Ok(Entry::Short)
    ));
    assert!(matches!(
        from_entry::<u8>(b" ()", position, &context, true),
        Ok(Entry::Empty { read: 3, .. })
    ));
    assert!(matches!(
        from_entry::<u8>(b" ()", position, &context, false),
        Ok(Entry::Short)
    ));
    assert!(matches!(
        from_entry::<u8>(b" (", position, &context, true),
        Err(ParseError { .. })
    ));
    assert!(matches!(
        from_entry::<u8>(b" (", position, &context, false),
        Ok(Entry::Short),
    ));
}
