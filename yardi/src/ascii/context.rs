use core::fmt::{self, Debug};

use crate::ascii::{parse::ParseOptions, ParseError};
use crate::datatypes::{Class, Name, Ttl};

type ParseWksIpProtocolFn = fn(&str) -> Result<u8, ParseError>;
type ParseWksServiceFn = fn(Option<u8>, &str) -> Result<u16, ParseError>;

static SHARED: Context = Context {
    origin: None,
    default_ttl: None,
    last_owner: None,
    last_class: None,
    last_ttl: None,
    parse_wks_ip_protocol: |s| crate::datatypes::int::parse_u8(s),
    parse_wks_service: |_, s| crate::datatypes::int::parse_u16(s),
    parse_options: ParseOptions::DEFAULT,
};

/// Contains state used during parsing and presenting
#[derive(Clone)]
pub struct Context {
    origin: Option<Name>,
    default_ttl: Option<Ttl>,
    last_owner: Option<Name>,
    last_class: Option<Class>,
    last_ttl: Option<Ttl>,
    parse_wks_ip_protocol: ParseWksIpProtocolFn,
    parse_wks_service: ParseWksServiceFn,
    parse_options: ParseOptions,
}

impl Context {
    /// Returns a reference to this `Context`'s `ParseOptions`.
    pub fn parse_options(&self) -> &ParseOptions {
        &self.parse_options
    }
    /// Returns a mutable reference to this `Context`'s `ParseOptions`.
    pub fn parse_options_mut(&mut self) -> &mut ParseOptions {
        &mut self.parse_options
    }
    /// Returns the function used to parse a WKS RR's IP Protocol.
    pub fn parse_wks_ip_protocol(&self) -> ParseWksIpProtocolFn {
        self.parse_wks_ip_protocol
    }
    /// Set the function used to parse a WKS RR's IP Protocol.
    pub fn set_parse_wks_ip_protocol(&mut self, f: ParseWksIpProtocolFn) -> &mut Self {
        self.parse_wks_ip_protocol = f;
        self
    }
    /// Returns the function used to parse a WKS RR's Services.
    pub fn parse_wks_service(&self) -> ParseWksServiceFn {
        self.parse_wks_service
    }
    /// Set the function used to parse a WKS RR's Services.
    pub fn set_parse_wks_service(&mut self, f: ParseWksServiceFn) -> &mut Self {
        self.parse_wks_service = f;
        self
    }
    /// Returns a static reference to a default `Context`.
    pub fn shared_default() -> &'static Context {
        &SHARED
    }
    /// Returns the current origin, if any.
    pub fn origin(&self) -> Option<&Name> {
        self.origin.as_ref()
    }
    /// Sets the current origin.
    pub fn set_origin(&mut self, name: Name) -> &mut Self {
        self.origin = Some(name);
        self
    }
    /// Removes the current origin.
    pub fn remove_origin(&mut self) -> &mut Self {
        self.origin = None;
        self
    }
    /// Returns the default TTL.
    pub fn default_ttl(&self) -> Option<Ttl> {
        self.default_ttl
    }
    /// Sets the default TTL.
    pub fn set_default_ttl(&mut self, ttl: Ttl) -> &mut Self {
        self.default_ttl = Some(ttl);
        self
    }
    /// Returns the last owner name, if any.
    pub fn last_owner(&self) -> Option<&Name> {
        self.last_owner.as_ref()
    }
    /// Sets the last owner name.
    // TODO: would cloning from an &Name make sense?
    pub fn set_last_owner(&mut self, name: Name) -> &mut Self {
        self.last_owner = Some(name);
        self
    }
    /// Returns the last Class, if any.
    pub fn last_class(&self) -> Option<Class> {
        self.last_class
    }
    /// Sets the last Class. Panics if `class` is not a data class.
    pub fn set_last_class(&mut self, class: Class) -> &mut Self {
        assert!(class.is_data(), "class must represent a DATA CLASS");
        self.last_class = Some(class);
        self
    }
    /// Returns the last TTL, if any.
    pub fn last_ttl(&self) -> Option<Ttl> {
        self.last_ttl
    }
    /// Sets the last TTL.
    pub fn set_last_ttl(&mut self, ttl: Ttl) -> &mut Self {
        self.last_ttl = Some(ttl);
        self
    }
    /// Returns in order of preference the default TTL, the last TTL, or no TTL.
    pub fn infer_ttl(&self) -> Option<Ttl> {
        self.default_ttl.or(self.last_ttl)
    }
    fn parse_wks_ip_protocol_raw(&self) -> *const ParseWksIpProtocolFn {
        self.parse_wks_ip_protocol as *const ParseWksIpProtocolFn
    }
    fn parse_wks_service_raw(&self) -> *const ParseWksServiceFn {
        self.parse_wks_service as *const ParseWksServiceFn
    }
}

impl Debug for Context {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Context")
            .field("origin", &self.origin)
            .field("default_ttl", &self.default_ttl)
            .field("last_owner", &self.last_owner)
            .field("last_class", &self.last_class)
            .field("last_ttl", &self.last_ttl)
            .field("parse_wks_ip_protocol", &self.parse_wks_ip_protocol_raw())
            .field("parse_wks_service", &self.parse_wks_service_raw())
            .finish()
    }
}

impl Default for Context {
    fn default() -> Self {
        SHARED.clone()
    }
}

// TODO: Is this really useful?
impl Default for &Context {
    fn default() -> Self {
        &SHARED
    }
}

impl PartialEq for Context {
    fn eq(&self, other: &Self) -> bool {
        self.default_ttl == other.default_ttl
            && self.last_class == other.last_class
            && self.last_ttl == other.last_ttl
            && self.parse_wks_ip_protocol_raw() == other.parse_wks_ip_protocol_raw()
            && self.parse_wks_service_raw() == other.parse_wks_service_raw()
            && self.origin == other.origin
            && self.last_owner == other.last_owner
    }
}

impl Eq for Context {}
