//! Support for presenting DNS datastructures in ASCII presentation format.

use alloc::borrow::Cow;
#[cfg(not(feature = "std"))]
use alloc::{borrow::ToOwned, string::String};
use core::fmt::{self, Write};

use crate::ascii::Context;
pub use crate::errors::{PresentError, PresentErrorKind};
pub use yardi_derive::Present;

/// Present `T` to a string with the default `Context`.
pub fn to_string<T: Present>(t: T, s: &mut String) -> Result<&mut String, PresentError> {
    to_string_context(t, s, Context::shared_default())
}

/// Present `T` to a string with `Context`.
pub fn to_string_context<'a, T: Present>(
    t: T,
    s: &'a mut String,
    context: &Context,
) -> Result<&'a mut String, PresentError> {
    t.present(&mut Presenter {
        context,
        target: s,
        need_sep: false,
    })?;
    Ok(s)
}

/// Present a value in DNS ASCII presentation format.
pub trait Present {
    /// Present the value using the given `Presenter`.
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError>;
}

impl<'a, T> Present for &'a T
where
    T: ?Sized + Present,
{
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        T::present(*self, p)
    }
}

impl<'a, T> Present for Cow<'a, T>
where
    T: ?Sized + ToOwned + Present,
    <T as ToOwned>::Owned: Present,
{
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        match *self {
            Cow::Borrowed(b) => b.present(p),
            Cow::Owned(ref o) => o.present(p),
        }
    }
}

impl<T: Present> Present for Option<T> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        self.as_ref().map(|t| t.present(p)).unwrap_or(Ok(()))
    }
}

impl Present for str {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        p.write(self)
    }
}

/// A canvas for emitting DNS structures in presentation format.
///
/// The DNS text format is effectively (US) ASCII. While `Presenter` does not
/// enforce that writes to it are in ASCII, it is an error to write non-ASCII
/// values and those values cannot be parsed by this crate.
#[derive(Debug)]
pub struct Presenter<'c, 't> {
    context: &'c Context,
    target: &'t mut String,
    need_sep: bool,
}

impl<'c, 't> Presenter<'c, 't> {
    /// Returns a reference to this `Presenter`'s context.
    pub fn context(&self) -> &Context {
        self.context
    }
    /// Write a string.
    pub fn write(&mut self, ascii: &str) -> Result<(), PresentError> {
        self.write_fmt(format_args!("{}", ascii))
    }
    /// Format and write a string using `std::fmt`'s machinery.
    // TODO: is this a wart?
    pub fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> Result<(), PresentError> {
        let added_sep = if self.need_sep {
            self.target.push(' ');
            self.need_sep = false;
            true
        } else {
            false
        };
        let old_len = self.target.len();
        self.target
            .write_fmt(fmt)
            .map_err(|_| PresentError::value_invalid())?;
        if added_sep && old_len == self.target.len() {
            self.target.truncate(old_len - 1);
        };
        self.need_sep = true;
        Ok(())
    }
    /// Returns a wrapper for this `Presenter` that does not put whitespace between
    /// calls to `write` or `write_fmt`.
    pub fn incremental<'s>(&'s mut self) -> PresenterInc<'s, 'c, 't> {
        PresenterInc {
            writer: self,
            did_write: false,
        }
    }
}

/// Like `Presenter` but does not put whitespace between calls to `write` or `write_fmt`.
#[derive(Debug)]
pub struct PresenterInc<'s, 'c, 't> {
    writer: &'s mut Presenter<'c, 't>,
    did_write: bool,
}

impl<'s, 'c, 't> PresenterInc<'s, 'c, 't> {
    /// Write a string
    pub fn write(&mut self, ascii: &str) -> Result<(), PresentError> {
        write!(self, "{}", ascii)
    }
    /// Format and write a string using `std::fmt`'s machinery.
    // TODO: is this a wart?
    pub fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> Result<(), PresentError> {
        let added_sep = if !self.did_write && self.writer.need_sep {
            self.writer.target.push(' ');
            true
        } else {
            false
        };
        let old_len = self.writer.target.len();
        self.writer
            .target
            .write_fmt(fmt)
            .map_err(|_| PresentError::value_invalid())?;
        if added_sep && old_len == self.writer.target.len() {
            self.writer.target.truncate(old_len - 1);
        } else {
            self.writer.need_sep = true;
            self.did_write = true;
        }
        Ok(())
    }
}
