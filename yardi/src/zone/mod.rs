//! Zone file structures and parsing.
use crate::{
    ascii::parse::Pull,
    datatypes::{Name, Ttl},
    iss::ISS,
};
use alloc::borrow::Cow;
#[cfg(not(feature = "std"))]
use alloc::string::{String, ToString};
#[cfg(feature = "std")]
use std::{
    fs::File,
    io,
    path::{Path, PathBuf},
};

#[cfg(feature = "std")]
use crate::ascii::{
    parse::{Entry, Position},
    Context,
};
use crate::{
    ascii::{Parse, ParseError, Parser, Present, PresentError, Presenter},
    msg::Rr,
    rdata::Generic,
};

#[cfg(feature = "std")]
const BUF_CAP_DEFAULT: usize = 1 << 20;
#[cfg(feature = "std")]
const BUF_CAP_MAX: usize = 10 * BUF_CAP_DEFAULT;

const INCLUDE: ISS<'_> = ISS("$INCLUDE");
const ORIGIN: ISS<'_> = ISS("$ORIGIN");
const TTL: ISS<'_> = ISS("$TTL");

/// An error returned by `ZoneReader`.
#[derive(Debug)]
#[cfg(feature = "std")]
pub struct Error {
    /// Path of the file being parsed.
    pub path: PathBuf,
    /// Either a `std::io::Error` or an `ascii::parse::ParseError`.
    pub error: IoParseError,
}

#[cfg(feature = "std")]
impl Error {
    /// Return true if the error is an `ascii::parse::ParseError`.
    pub fn is_parse(&self) -> bool {
        self.error.is_parse()
    }
    /// Returns true if the error is a `std::io::Error`.
    pub fn is_io(&self) -> bool {
        self.error.is_io()
    }
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}

#[cfg(feature = "std")]
impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // TODO: better Error message
        f.write_fmt(format_args!("{:?}", self))
    }
}

/// Either a `std::io::Error` or an `ascii::parse::ParseError`.
#[derive(Debug)]
#[cfg(feature = "std")]
pub enum IoParseError {
    /// An `ascii::parse::ParseError`.
    Parse(ParseError),
    /// A `std::io::Error`.
    Io(io::Error),
}

#[cfg(feature = "std")]
impl IoParseError {
    /// Return true if the error is an `ascii::parse::ParseError`.
    pub fn is_parse(&self) -> bool {
        match *self {
            IoParseError::Parse(_) => true,
            IoParseError::Io(_) => false,
        }
    }
    /// Returns true if the error is a `std::io::Error`.
    pub fn is_io(&self) -> bool {
        match *self {
            IoParseError::Parse(_) => false,
            IoParseError::Io(_) => true,
        }
    }
}

#[cfg(feature = "std")]
impl std::error::Error for IoParseError {}

#[cfg(feature = "std")]
impl std::fmt::Display for IoParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // TODO: better Error message
        f.write_fmt(format_args!("{:?}", self))
    }
}

#[cfg(feature = "std")]
impl From<ParseError> for IoParseError {
    fn from(err: ParseError) -> Self {
        IoParseError::Parse(err)
    }
}

#[cfg(feature = "std")]
impl From<io::Error> for IoParseError {
    fn from(err: io::Error) -> Self {
        IoParseError::Io(err)
    }
}

/// Parses and intreprets zone files on disk returning a stream of `Rr`.
#[derive(Debug)]
#[cfg(feature = "std")]
pub struct ZoneReader {
    max_includes: usize,
    handles: Vec<Handle>,
}

#[cfg(feature = "std")]
impl ZoneReader {
    /// Returns a `ZoneReader` for the zone at `path` with a `Context` updated
    /// with `origin`.
    pub fn new<P>(path: P, context: Context) -> Result<Self, io::Error>
    where
        P: Into<PathBuf>,
    {
        let handle = Handle::new(path.into(), context)?;
        Ok(ZoneReader {
            max_includes: 12,
            handles: vec![handle],
        })
    }
    /// Skips the next entry and any intermediate empty entries before it.
    /// If a `$INCLUDE` is queued due to an `io::Error` that will be skipped too.
    pub fn skip_entry(&mut self) -> Result<Option<String>, Error> {
        while !self.handles.is_empty() {
            match self.handles.last_mut().map(Handle::skip_entry) {
                Some(r @ Ok(_)) => return r,
                Some(Err(err)) => return Err(err),
                None => self.handles.pop(),
            };
        }
        Ok(None)
    }
    /// Return the position of the current entry, if any.
    pub fn position(&self) -> Option<Position> {
        self.handles.last().map(Handle::position)
    }
    /// Returns a `Path` to the current file, if any.
    pub fn file_name(&self) -> Option<&Path> {
        self.handles.last().map(Handle::file_name)
    }
    /// Return the maximum number of includes to follow.
    pub fn max_includes(&self) -> usize {
        self.max_includes
    }
    /// Set the maximum number of includes to follow.
    pub fn set_max_includes(&mut self, max: usize) -> &mut Self {
        self.max_includes = max;
        self
    }
}

// TODO: should this return nothing after encountering an error?
#[cfg(feature = "std")]
impl Iterator for ZoneReader {
    type Item = Result<Rr<Generic>, Error>;
    fn next(&mut self) -> Option<Self::Item> {
        loop {
            let accept_handle = self.handles.len() <= self.max_includes;
            match self.handles.last_mut().and_then(|h| h.next(accept_handle)) {
                Some(HandleItem::Rr(rr)) => return Some(Ok(rr)),
                Some(HandleItem::Handle(handle)) => self.handles.push(handle),
                Some(HandleItem::Err(err)) => return Some(Err(err)),
                None => {
                    self.handles.pop();
                    if self.handles.is_empty() {
                        return None;
                    }
                }
            }
        }
    }
}

#[cfg(feature = "std")]
enum HandleItem {
    Rr(Rr<Generic>),
    Handle(Handle),
    Err(Error),
}

#[derive(Debug)]
#[cfg(feature = "std")]
struct Handle {
    path: PathBuf,
    file: File,
    buf: Vec<u8>,
    cursor: usize,
    max_cap: usize,
    eof: bool,
    context: Context,
    position: Position,
}

#[cfg(feature = "std")]
impl Handle {
    fn new(path: PathBuf, context: Context) -> Result<Self, io::Error> {
        let file = File::open(&path)?;
        Ok(Handle {
            path,
            file,
            buf: Vec::with_capacity(BUF_CAP_DEFAULT),
            cursor: 0,
            max_cap: BUF_CAP_MAX,
            eof: false,
            context,
            position: Position::default(),
        })
    }
    fn skip_entry(&mut self) -> Result<Option<String>, Error> {
        loop {
            let skip_result = crate::ascii::parse::skip_entry(self.view(), self.position, self.eof)
                .map_err(|err| Error {
                    path: self.path.clone(),
                    error: err.into(),
                })?;
            match skip_result {
                Entry::End => return Ok(None),
                Entry::Empty { read, pos } => {
                    self.position = pos;
                    self.consume(read);
                }
                Entry::Value { read, pos, value } => {
                    let value = value.to_owned();
                    self.position = pos;
                    self.consume(read);
                    return Ok(Some(value));
                }
                Entry::Short => {
                    self.fill().map_err(|err| Error {
                        path: self.path.clone(),
                        error: err.into(),
                    })?;
                }
            }
        }
    }
    fn position(&self) -> Position {
        self.position
    }
    fn file_name(&self) -> &Path {
        &self.path
    }
    fn view(&self) -> &[u8] {
        &self.buf[self.cursor..]
    }
    fn consume(&mut self, amount: usize) {
        self.cursor += amount;
        debug_assert!(self.cursor <= self.buf.len());
    }
    fn fill(&mut self) -> Result<usize, io::Error> {
        assert!(!self.eof);
        if self.buf.len() == self.buf.capacity() {
            if self.cursor != 0 {
                // TODO: copy_within
                unsafe {
                    let keep = self.buf.len() - self.cursor;
                    self.buf
                        .as_ptr()
                        .add(self.cursor)
                        .copy_to(self.buf.as_mut_ptr(), keep);
                    self.buf.set_len(keep);
                    self.cursor = 0;
                }
            } else if self.buf.capacity() < self.max_cap {
                let new_size = std::cmp::min(self.buf.capacity() * 3 / 2, self.max_cap);
                let additional = new_size - self.buf.capacity();
                self.buf.reserve(additional);
            } else {
                let err = io::Error::new(io::ErrorKind::Other, "no room");
                return Err(err);
            }
        }
        unsafe {
            let p = self.buf.as_mut_ptr().add(self.buf.len());
            let len = self.buf.capacity() - self.buf.len();
            let s = std::slice::from_raw_parts_mut(p, len);
            let read = io::Read::read(&mut self.file, s)?;
            let new_len = self.buf.len() + read;
            self.buf.set_len(new_len);
            self.eof = read == 0;
            Ok(read)
        }
    }
    fn next(&mut self, accept_handle: bool) -> Option<HandleItem> {
        loop {
            let parse_result = crate::ascii::parse::from_entry::<ZoneEntry<'_>>(
                self.view(),
                self.position,
                &self.context,
                self.eof,
            );
            match parse_result {
                Ok(Entry::Short) => {
                    debug_assert!(!self.eof);
                    if let Err(err) = self.fill() {
                        return Some(HandleItem::Err(Error {
                            path: self.path.clone(),
                            error: err.into(),
                        }));
                    }
                }
                Ok(Entry::End) => return None,
                Ok(Entry::Empty { read, pos }) => {
                    self.consume(read);
                    self.position = pos;
                }
                Ok(Entry::Value { read, pos, value }) => match value {
                    ZoneEntry::Origin(OriginDirective(name)) => {
                        self.consume(read);
                        self.position = pos;
                        self.context.set_origin(name);
                    }
                    ZoneEntry::Ttl(TtlDirective(ttl)) => {
                        self.consume(read);
                        self.position = pos;
                        self.context.set_default_ttl(ttl);
                    }
                    ZoneEntry::Rr(rr) => {
                        self.consume(read);
                        self.position = pos;
                        self.context
                            .set_last_ttl(rr.ttl)
                            .set_last_class(rr.class)
                            .set_last_owner(rr.name.clone());
                        return Some(HandleItem::Rr(rr));
                    }
                    ZoneEntry::Include(_) if !accept_handle => {
                        // TODO: should this error be a new variant?
                        let error = IoParseError::Io(io::Error::new(
                            io::ErrorKind::Other,
                            "too many includes",
                        ));
                        let path = self.path.clone();
                        return Some(HandleItem::Err(Error { path, error }));
                    }
                    ZoneEntry::Include(IncludeDirective { file, mut origin }) => {
                        let mut c = self.context.clone();
                        if let Some(name) = origin.take() {
                            c.set_origin(name);
                        }
                        let path = self
                            .path
                            .parent()
                            .unwrap_or_else(|| "".as_ref())
                            .join(&file as &str);
                        match Handle::new(path.clone(), c) {
                            Ok(handle) => {
                                self.consume(read);
                                self.position = pos;
                                return Some(HandleItem::Handle(handle));
                            }
                            Err(err) => {
                                return Some(HandleItem::Err(Error {
                                    path,
                                    error: err.into(),
                                }));
                            }
                        }
                    }
                },
                Err(err) => {
                    return Some(HandleItem::Err(Error {
                        path: self.path.clone(),
                        error: err.into(),
                    }));
                }
            }
        }
    }
}

/// Represents a `$ORIGIN` directive as defined in [RFC 1035](https://tools.ietf.org/html/rfc1035#section-5.1).
#[derive(Debug, PartialEq, Eq)]
pub struct OriginDirective(pub Name);

impl OriginDirective {
    fn parse_body(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        Ok(OriginDirective(Parse::parse(p)?))
    }
}

impl Parse for OriginDirective {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(|s| {
            if s == ORIGIN {
                Ok(())
            } else {
                Err(ParseError::value_unknown())
            }
        })?;
        Self::parse_body(p)
    }
}

impl Present for OriginDirective {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        ORIGIN.present(p)?;
        self.0.present(p)
    }
}

/// Represents a `$INCLUDE` directive as defined in [RFC 1035](https://tools.ietf.org/html/rfc1035#section-5.1).
#[derive(Debug, PartialEq, Eq)]
pub struct IncludeDirective<'a> {
    /// The file to include. Must be a simple ASCII string with no escapes.
    pub file: Cow<'a, str>,
    /// An optional origin. Relative names will be made absolute against the
    /// current origin. Relative names cannot be presented as this crate does
    /// not represent them.
    pub origin: Option<Name>,
}

impl<'a> IncludeDirective<'a> {
    fn parse_body(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let file = p
            .pull(|s| {
                // This logic is supposed to imitate what BIND appears to do.
                // The only change it should make to input is to unescape quotes
                // in quoted strings.
                if s.as_bytes()[0] == b'"' {
                    let mut f = String::with_capacity(s.len());
                    let mut last_escape = false;
                    for &b in s.as_bytes()[1..s.len() - 1].iter() {
                        match b {
                            b'"' if last_escape => f.push(b as char),
                            b'\\' if !last_escape => (),
                            _ if last_escape => {
                                f.push('\\');
                                f.push(b as char);
                            }
                            _ => f.push(b as char),
                        }
                        last_escape = b == b'\\';
                    }
                    Ok(f)
                } else {
                    Ok(s.to_string())
                }
            })
            .map(Cow::Owned)?;
        let origin = Parse::parse(p)?;
        Ok(IncludeDirective { file, origin })
    }
}

impl<'a> Parse for IncludeDirective<'a> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(|s| {
            if s == INCLUDE {
                Ok(())
            } else {
                Err(ParseError::value_unknown())
            }
        })?;
        Self::parse_body(p)
    }
}

impl<'a> Present for IncludeDirective<'a> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        INCLUDE.present(p)?;
        let mut temp = String::with_capacity(self.file.len() + 2);
        temp.push('"');
        let mut last_was_backslash = false;
        for b in self.file.as_bytes().iter().cloned() {
            match b {
                b'"' => {
                    if last_was_backslash {
                        // can't be parsed by BIND as I understand it
                        return Err(PresentError::value_invalid());
                    }
                    temp.push_str(r#"\""#);
                }
                b if (b' '..=b'~').contains(&b) => {
                    temp.push(b as char);
                    last_was_backslash = b == b'\\';
                }
                _ => return Err(PresentError::value_invalid()),
            }
        }
        p.write(&temp)?;
        if self.origin.is_some() {
            self.origin.present(p)?;
        }
        Ok(())
    }
}

/// Represents a `$TTL` directive as defined in [RFC 2308](https://tools.ietf.org/html/rfc2308#section-4).
#[derive(Debug, PartialEq, Eq)]
pub struct TtlDirective(pub Ttl);

impl TtlDirective {
    fn parse_body(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        Ok(TtlDirective(Parse::parse(p)?))
    }
}

impl Parse for TtlDirective {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        p.pull(|s| {
            if s == TTL {
                Ok(())
            } else {
                Err(ParseError::value_unknown())
            }
        })?;
        Self::parse_body(p)
    }
}

impl Present for TtlDirective {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        TTL.present(p)?;
        self.0.present(p)
    }
}

/// An entry in a zone file
#[derive(Debug, PartialEq, Eq)]
pub enum ZoneEntry<'a> {
    /// Origin Directive
    Origin(OriginDirective),
    /// Include Directive
    Include(IncludeDirective<'a>),
    /// Resource Record
    Rr(Rr<Generic>),
    /// TTL Directive
    Ttl(TtlDirective),
}

impl<'a> Present for ZoneEntry<'a> {
    fn present(&self, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
        match *self {
            ZoneEntry::Origin(ref origin) => origin.present(p),
            ZoneEntry::Include(ref include) => include.present(p),
            ZoneEntry::Rr(ref rr) => rr.present(p),
            ZoneEntry::Ttl(ref ttl) => ttl.present(p),
        }
    }
}

impl<'a> Parse for ZoneEntry<'a> {
    fn parse(p: &mut Parser<'_, '_>) -> Result<Self, ParseError> {
        let f = if p.leading_whitespace()? {
            |p| Rr::parse(p).map(ZoneEntry::Rr)
        } else {
            p.pull(|s| -> Pull<fn(_) -> _> {
                let s = s.as_bytes();
                if s == INCLUDE {
                    Pull::Accept(|p| IncludeDirective::parse_body(p).map(ZoneEntry::Include))
                } else if s == ORIGIN {
                    Pull::Accept(|p| OriginDirective::parse_body(p).map(ZoneEntry::Origin))
                } else if s == TTL {
                    Pull::Accept(|p| TtlDirective::parse_body(p).map(ZoneEntry::Ttl))
                } else if s[0] == b'$' {
                    Pull::Reject(ParseError::value_unknown())
                } else {
                    Pull::Decline(|p| Rr::parse(p).map(ZoneEntry::Rr))
                }
            })?
        };
        f(p)
    }
}
