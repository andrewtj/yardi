use crate::wire::{Pack, Reader, Writer};

#[test]
fn test_reader() {
    let mut r = Reader::new(b"123456");
    assert!(!r.is_empty());
    let a = r.get_bytes(3).expect("3");
    assert!(!r.is_empty());
    let b = r.get_bytes(2).expect("2");
    assert!(!r.is_empty());
    let c = r.get_bytes(1).expect("1");
    assert_eq!(&a[..], b"123");
    assert_eq!(&b[..], b"45");
    assert_eq!(&c[..], b"6");
}

#[test]
fn test_writerp16() {
    let mut buf = [0u8; 18];
    {
        let mut w = Writer::new(&mut buf[..]);
        let buf = w.write_forward(1).unwrap();
        assert_eq!(buf.len(), 1);
    }
    {
        let mut w = Writer::new(&mut buf[..0]);
        assert!(w.writer16().is_err());
    }
    {
        let mut w = Writer::new(&mut buf[..1]);
        assert!(w.writer16().is_err());
    }
    let mut w = Writer::new(&mut buf[..]);
    w.write_bytes(&[0u8; 6][..]).expect("w: write bytes");
    {
        let mut a = w.writer16().expect("a");
        assert!(a.truncate(7).is_err());
        assert!(a.truncate(8).is_ok());
        a.write_bytes(&[3, 2, 1]).expect("a: write bytes");
        0u8.pack(&mut a).expect("a: write byte");
        let mut b = a.writer16().expect("b");
        b.write_bytes(&[4, 4, 4, 4]).expect("b: write bytes");
        assert_eq!(b.remaining(), 0);
    }
    let mut r = Reader::new(w.written());
    assert_eq!(r.get_bytes(6), Ok(&[0; 6][..]));
    {
        let mut r16 = r.reader16().expect("reader 16");
        assert_eq!(r16.get_bytes(4), Ok(&[3, 2, 1, 0][..]));
        assert_eq!(r16.get_bytes16(), Ok(&[4, 4, 4, 4][..]));
        assert!(r16.is_empty());
    }
    assert!(r.is_empty());
}
