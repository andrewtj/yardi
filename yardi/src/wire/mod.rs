//! Support for reading and writing DNS structures in wire format.
#[cfg(test)]
mod test;

use crate::datatypes::{
    name::{Style as NameStyle, PTR_MAX as NAME_PTR_MAX},
    Ty,
};
pub use crate::errors::{PackError, PackErrorKind, UnpackError, UnpackErrorKind};
use alloc::borrow::{Cow, ToOwned};
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;
use core::{
    cmp::Ordering,
    ops::{Deref, DerefMut, Range},
};
// TODO: ?
pub use yardi_derive::{Pack, Unpack, UnpackedLen, WireOrd};

/// A helper for reading wire format DNS structures from a byte slice.
#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Reader<'a> {
    bytes: &'a [u8],
    index: usize,
}

impl<'a> Reader<'a> {
    /// Construct a new `Reader`.
    pub fn new(bytes: &'a [u8]) -> Self {
        Self::new_index(bytes, 0)
    }
    /// Construct a new `Reader` positioned at `index`.
    pub fn new_index(bytes: &'a [u8], index: usize) -> Self {
        Reader { bytes, index }
    }
    /// Read a single byte.
    // TODO: I prefer read_byte, etc
    pub fn get_byte(&mut self) -> Result<u8, UnpackError> {
        if let Some(&b) = self.bytes.get(self.index) {
            self.index += 1;
            Ok(b)
        } else {
            Err(UnpackError::short_at(self.index))
        }
    }
    /// Read ahead `len` bytes with out changing the current index.
    pub fn peek(&self, len: usize) -> Result<&'a [u8], UnpackError> {
        self.index
            .checked_add(len)
            .and_then(move |read| self.bytes.get(self.index..read))
            .ok_or_else(|| UnpackError::short_at(self.index))
    }
    /// Read `len` bytes.
    pub fn get_bytes(&mut self, len: usize) -> Result<&'a [u8], UnpackError> {
        let end = self.index.checked_add(len).ok_or_else(UnpackError::short)?;
        if end > self.bytes.len() {
            return Err(UnpackError::short_at(self.index));
        }
        let bytes = &self.bytes[self.index..end];
        self.index = end;
        Ok(bytes)
    }
    /// Read a one-byte length prefixed byte sequence.
    pub fn get_bytes8(&mut self) -> Result<&'a [u8], UnpackError> {
        let len = self.get_byte()? as usize;
        self.get_bytes(len)
    }
    /// Read a two-byte length prefixed byte sequence.
    pub fn get_bytes16(&mut self) -> Result<&'a [u8], UnpackError> {
        let len = u16::unpack(self)? as usize;
        self.get_bytes(len)
    }
    /// Read `len` bytes and use `f` to transform them.
    pub fn get_bytes_map<F, T>(&mut self, len: usize, f: F) -> Result<T, UnpackError>
    where
        F: FnOnce(&[u8]) -> Result<T, UnpackError>,
    {
        let end = self.index.checked_add(len).ok_or_else(UnpackError::short)?;
        let s = self.bytes[..]
            .get(self.index..end)
            .ok_or_else(|| UnpackError::short_at(self.index))?;
        let t = f(s)?;
        self.index = end;
        Ok(t)
    }
    /// Read remaining bytes.
    pub fn get_bytes_rest(&mut self) -> Result<&'a [u8], UnpackError> {
        if self.index > self.bytes.len() {
            return Err(UnpackError::short_at(self.index));
        }
        let bytes = &self.bytes[self.index..];
        self.index = self.bytes.len();
        Ok(bytes)
    }
    /// Returns bytes up to the current index.
    pub fn get_bytes_seen(&mut self) -> Result<&'a [u8], UnpackError> {
        if self.index > self.bytes.len() {
            return Err(UnpackError::short_at(self.index));
        }
        Ok(&self.bytes[..self.index])
    }
    /// Skips forward `len` bytes.
    pub fn skip(&mut self, len: usize) -> Result<(), UnpackError> {
        let new_index = self
            .index
            .checked_add(len)
            .ok_or_else(UnpackError::value_large)?;
        if new_index > self.bytes.len() {
            return Err(UnpackError::short_at(self.index));
        }
        self.index = new_index;
        Ok(())
    }
    /// Unpack a u8 length and skip forward by that many bytes.
    pub fn skip8(&mut self) -> Result<(), UnpackError> {
        let len = self.get_byte()?;
        self.skip(len as usize)
    }
    /// Unpack a u16 length and skip forward by that many bytes.
    pub fn skip16(&mut self) -> Result<(), UnpackError> {
        let len = u16::unpack(self)?;
        self.skip(len as usize)
    }
    /// Return the current index.
    // TODO: rename cursor
    pub fn index(&self) -> usize {
        self.index
    }
    /// Set this `Reader`'s index.
    pub fn set_index(&mut self, index: usize) -> &mut Self {
        self.index = index;
        self
    }
    /// Truncate the underlying buffer at `index`.
    pub fn truncate(&mut self, index: usize) -> Result<(), UnpackError> {
        if index > self.bytes.len() {
            return Err(UnpackError::short_at(self.index));
        }
        self.bytes = &self.bytes[..index];
        Ok(())
    }
    /// Read a two-byte length and return a `Reader` whose buffer ends
    /// after that number of bytes. This `Reader` will be positioned
    /// where the new `Reader` ends.
    pub fn reader16(&mut self) -> Result<Reader<'a>, UnpackError> {
        let len = u16::unpack(self)? as usize;
        let mut child = self.clone();
        self.skip(len)?;
        child.truncate(self.index())?;
        Ok(child)
    }
    /// Returns true if there are no more bytes available.
    pub fn is_empty(&self) -> bool {
        self.index >= self.bytes.len()
    }
    /// Returns the number of bytes available to read.
    pub fn unread(&self) -> usize {
        self.bytes.len().saturating_sub(self.index)
    }
    /// Returns a reference to the underlying buffer.
    pub fn buffer(&self) -> &'a [u8] {
        self.bytes
    }
    // TODO: necessary?
    // TODO: does this work behind compressed?
    // TODO: what about mut? is this actually useful?
    /// Consumes self and returns the underlying buffer.
    pub fn into_buffer(self) -> &'a [u8] {
        self.bytes
    }
}

/// Format is used to specify how a `Name` is written to the buffer.
// TODO: I can't think of a use for custom compression/canonicalisation fns
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum Format {
    /// Write Names in canonical format. If the function suppled returns true
    /// a Writer scoped for a Ty will write Names in canonical format,
    /// otherwise Names will be written as-is and without compression.
    Canonical(fn(Ty) -> bool),
    /// Writes Names with compression enabled. If the function supplied returns
    /// true a Writer scoped for a Ty will write Names using compression,
    /// otherwise Names will be written as-is.
    Compress(fn(Ty) -> bool),
    /// The same as Format::Compress but with compression targets compared in
    /// an ASCII case sensitive manner.
    CompressSensitive(fn(Ty) -> bool),
    /// Writes names with compression enabled. A Writer scoped for a Ty will
    /// compressed names for well-known (RFC 1035), non-obsolete TYPEs.
    /// This is equivalent to `Format::Compress(::rdata::compress)`.
    Dns,
    /// The same as Format::Dns but with compression targets compared in an
    /// ASCII case sensitive manner.
    DnsSensitive,
    /// Write Names in canonical format. A Writer scoped for a Ty will write
    /// names in canonical format if required for the TYPE, otherwise Names
    /// will be written as-is. This is equivalent to:
    /// `Format::Canonical(::dnssec::canonical)`.
    Dnssec,
    /// Writes names with compression enabled. A Writer scoped for a Ty will
    /// compressed names for the TYPEs specified in
    /// [Multicast DNS (RFC 6762)](https://tools.ietf.org/html/rfc6762#section-18.14).
    /// This is equivalent to `Format::Compress(::rdata::compress_mdns)`.
    Mdns,
    /// The same as `Format::Mdns` but with compression targets
    /// compared in an ASCII case sensitive manner.
    MdnsSensitive,
    /// Names are written as-is.
    Plain,
}

// TODO: Decide whether Writer should work with Vec<u8>, &mut [u8], or both.
/// A helper for writing DNS structures to a byte slice in wire format.
// TODO: does fanf have a better compression method?
#[derive(Debug)]
pub struct Writer<'b> {
    names: Vec<u16>, // TODO: SmallVec<[u16; 11]> = 40 bytes, worth it?
    name_style: Option<NameStyle>,
    buf: &'b mut [u8],
    window: Range<usize>,
    cursor: usize,
    format: Format,
}

impl<'b> Writer<'b> {
    /// Construct a new `Writer`.
    pub fn new(buf: &'b mut [u8]) -> Self {
        let window = 0..buf.len();
        Writer {
            names: <Vec<u16>>::new(),
            name_style: None,
            buf,
            window,
            cursor: 0,
            format: Format::Dns,
        }
    }
    /// Returns the current `Format`.
    pub fn format(&self) -> Format {
        self.format
    }
    /// Sets a new `Format`.
    pub fn set_format(&mut self, format: Format) -> &mut Self {
        self.format = format;
        self
    }
    /// Truncates at `index`. If this `Writer` is wrapped by `WriterP16`
    /// `index` cannot be less than the start of the prefixed window.
    // TODO: Error: Invalid / Short
    pub fn truncate(&mut self, index: usize) -> Result<(), ()> {
        if index < self.window.start {
            return Err(());
        }
        self.cursor = index;
        if index > NAME_PTR_MAX {
            return Ok(());
        }
        let cut = self.names.partition_point(|&x| x < (index as u16));
        self.names.truncate(cut);
        Ok(())
    }
    pub(crate) fn name_index(&self) -> &[u16] {
        &self.names
    }
    pub(crate) fn name_index_push(&mut self, offset: usize) -> &mut Self {
        if offset <= NAME_PTR_MAX {
            let offset = offset as u16;
            debug_assert!(self.names.iter().all(|&o| o < offset));
            self.names.push(offset);
        }
        self
    }
    pub(crate) fn infer_name_style(&self) -> NameStyle {
        if let Some(style) = self.name_style {
            style
        } else {
            match self.format {
                Format::Canonical(_) | Format::Dnssec => NameStyle::Canonical,
                Format::Compress(_) | Format::Dns | Format::Mdns => NameStyle::Compress,
                Format::CompressSensitive(_) | Format::DnsSensitive | Format::MdnsSensitive => {
                    NameStyle::CompressSensitive
                }
                Format::Plain => NameStyle::Plain,
            }
        }
    }
    /// Changes the style of name to pack based on `ty` and the Writer's Format.
    /// If the writer has already been set to write names in a specific style
    /// via `for_ty()`, `compress()` or `canonical()` the style will not be
    /// changed. The returned wrapper restores the original pack style when it
    /// is dropped.
    pub fn for_ty(
        &mut self,
        ty: Ty,
    ) -> impl DerefMut<Target = Writer<'b>> + AsMut<Writer<'b>> + '_ {
        if let Some(style) = self.name_style {
            return WriterNS {
                writer: self,
                initial_name_style: Some(style),
            };
        }
        let initial_name_style = self.name_style;
        self.name_style = Some(match self.format {
            Format::Plain => NameStyle::Plain,
            Format::Compress(f) if f(ty) => NameStyle::Compress,
            Format::CompressSensitive(f) if f(ty) => NameStyle::CompressSensitive,
            Format::Dns if crate::rdata::compress(ty) => NameStyle::Compress,
            Format::DnsSensitive if crate::rdata::compress(ty) => NameStyle::CompressSensitive,
            Format::Mdns if crate::rdata::compress_mdns(ty) => NameStyle::Compress,
            Format::MdnsSensitive if crate::rdata::compress_mdns(ty) => {
                NameStyle::CompressSensitive
            }
            Format::Canonical(f) if f(ty) => NameStyle::Canonical,
            Format::Dnssec if crate::dnssec::canonical(ty) => NameStyle::Canonical,
            _ => NameStyle::Plain,
        });
        WriterNS {
            writer: self,
            initial_name_style,
        }
    }
    /// Configures the `Writer` to pack names in canonical format. The returned
    /// wrapper restores the original pack style when it is dropped.
    pub fn canonical(&mut self) -> impl DerefMut<Target = Writer<'b>> + AsMut<Writer<'b>> + '_ {
        let initial_name_style = self.name_style;
        self.name_style = Some(NameStyle::Canonical);
        WriterNS {
            writer: self,
            initial_name_style,
        }
    }
    /// Configures the `Writer` to pack names in compressed format. The returned
    /// wrapper restores the original pack style when it is dropped.
    pub fn compress(&mut self) -> impl DerefMut<Target = Writer<'b>> + AsMut<Writer<'b>> + '_ {
        let initial_name_style = self.name_style;
        self.name_style = Some(NameStyle::Compress);
        WriterNS {
            writer: self,
            initial_name_style,
        }
    }
    /// Configures the `Writer` to pack names as-is without compression. The
    /// returned wrapper restores the original pack style when it is dropped.
    pub fn plain(&mut self) -> impl DerefMut<Target = Writer<'b>> + AsMut<Writer<'b>> + '_ {
        let initial_name_style = self.name_style;
        self.name_style = Some(NameStyle::Plain);
        WriterNS {
            writer: self,
            initial_name_style,
        }
    }
    /// Writes `bytes` to the underlying buffer.
    pub fn write_bytes(&mut self, bytes: &[u8]) -> Result<(), PackError> {
        self.write_forward(bytes.len())
            .map(|b| b.copy_from_slice(bytes))
    }
    /// Writes `bytes` to the underlying buffer with a one-byte length prefix.
    pub fn write_bytes8(&mut self, bytes: &[u8]) -> Result<(), PackError> {
        if bytes.len() > 0xFF {
            return Err(PackError::value_large());
        };
        self.write_forward(1 + bytes.len()).map(|b| {
            b[0] = bytes.len() as u8;
            b[1..].copy_from_slice(bytes);
        })
    }
    /// Writes `bytes` to the underlying buffer with a two-byte length prefix.
    pub fn write_bytes16(&mut self, bytes: &[u8]) -> Result<(), PackError> {
        if bytes.len() > 0xFFFF {
            return Err(PackError::value_large());
        }
        self.write_forward(2 + bytes.len()).map(|b| {
            let (prefix, payload) = b.split_at_mut(2);
            prefix[..].copy_from_slice(&(bytes.len() as u16).to_be_bytes()[..]);
            payload.copy_from_slice(bytes);
        })
    }
    /// Returns a mutable byte slice of length `size` from the underlying buffer.
    pub fn write_forward(&mut self, size: usize) -> Result<&mut [u8], PackError> {
        let t = self
            .buf
            .get_mut(self.cursor..self.window.end)
            .and_then(|s| s.get_mut(..size))
            .ok_or(PackError::no_space())?;
        self.cursor += size;
        Ok(t)
    }
    /// Returns a wrapper that writes a two-byte length prefix to the data
    /// written via it.
    pub fn writer16(
        &mut self,
    ) -> Result<impl DerefMut<Target = Writer<'b>> + AsMut<Writer<'b>> + '_, PackError> {
        if self.remaining() < 2 {
            return Err(PackError::no_space());
        }
        self.cursor += 2;
        let original_window = self.window.clone();
        self.window.start = self.cursor;
        self.window.end = self
            .window
            .start
            .saturating_add(0xFFFF)
            .min(self.window.end);
        Ok(WriterP16 {
            writer: self,
            original_window,
        })
    }
    /// Returns a reference to the underlying byte slice truncated at the last
    /// byte written.
    pub fn written(&self) -> &[u8] {
        &self.buf[..self.cursor]
    }
    /// Returns a mutable reference to the underlying byte slice truncated
    /// at the last byte written.
    pub fn written_mut(&mut self) -> &mut [u8] {
        &mut self.buf[..self.cursor]
    }
    /// Consumes self returning a reference to the underlying byte slice
    /// truncated at the last byte written.
    // TODO: this doesn't work behind compressed
    pub fn into_written(self) -> &'b [u8] {
        &self.buf[..self.cursor]
    }
    /// Consumes self returning a mutable reference to the underlying byte
    /// slice truncated at the last byte written.
    pub fn into_written_mut(self) -> &'b mut [u8] {
        &mut self.buf[..self.cursor]
    }
    /// Returns the amount written to this `Writer`.
    pub fn len(&self) -> usize {
        self.cursor
    }
    /// Returns true if this `Writer` is empty.
    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }
    /// Space remaing in this `Writer`.
    pub fn remaining(&self) -> usize {
        self.buf.len() - self.cursor
    }
}

/// A wrapper for a `Writer` that prepends a two-byte length prefix to the
/// content written via it.
#[derive(Debug)]
struct WriterP16<'s, 'b> {
    writer: &'s mut Writer<'b>,
    original_window: Range<usize>,
}

impl<'s, 'b> AsRef<Writer<'b>> for WriterP16<'s, 'b> {
    fn as_ref(&self) -> &Writer<'b> {
        self.writer
    }
}

impl<'s, 'b> AsMut<Writer<'b>> for WriterP16<'s, 'b> {
    fn as_mut(&mut self) -> &mut Writer<'b> {
        self.writer
    }
}

impl<'s, 'b> Deref for WriterP16<'s, 'b> {
    type Target = Writer<'b>;
    fn deref(&self) -> &Self::Target {
        self.writer
    }
}

impl<'s, 'b> DerefMut for WriterP16<'s, 'b> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.writer
    }
}

impl<'s, 'b> Drop for WriterP16<'s, 'b> {
    fn drop(&mut self) {
        let wrote = self.writer.cursor - self.writer.window.start;
        assert!(wrote < 0xFFFF);
        let prefix_range = self.writer.window.start - 2..self.writer.window.start;
        self.writer.buf[prefix_range].copy_from_slice(&(wrote as u16).to_be_bytes()[..]);
        self.writer.window.clone_from(&self.original_window);
    }
}

/// Holds a writer temporarily configured for packing names in a particular
/// style. Restores the previous name packing style when dropped.
#[derive(Debug)]
struct WriterNS<'s, 'b> {
    writer: &'s mut Writer<'b>,
    initial_name_style: Option<NameStyle>,
}

impl<'s, 'b> AsRef<Writer<'b>> for WriterNS<'s, 'b> {
    fn as_ref(&self) -> &Writer<'b> {
        self.writer
    }
}

impl<'s, 'b> AsMut<Writer<'b>> for WriterNS<'s, 'b> {
    fn as_mut(&mut self) -> &mut Writer<'b> {
        self.writer
    }
}

impl<'s, 'b> Deref for WriterNS<'s, 'b> {
    type Target = Writer<'b>;
    fn deref(&self) -> &Self::Target {
        self.writer
    }
}

impl<'s, 'b> DerefMut for WriterNS<'s, 'b> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.writer
    }
}

impl<'s, 'b> Drop for WriterNS<'s, 'b> {
    fn drop(&mut self) {
        self.writer.name_style = self.initial_name_style;
    }
}

/// An error returned when comparing a type in wire format.
#[derive(Clone, Debug, PartialEq, Eq)]
pub enum WireOrdError {
    /// The left value failed to unpack.
    Left(UnpackError),
    /// The right value failed to unpack.
    Right(UnpackError),
}

/// Compare a type in wire format.
// TODO: instead of Ordering::Equal, return (uncompressed) bytes covered
// TODO: Is there a more elegant way of doing this? Return an Iterator of bytes maybe? Then structs could be compared with wire.
//       ^^ possibly the comparison needs to be on a stream of bytes for it to be correct?
// TODO: !! Maybe replace with a dnssec::CanonicalCmp trait?
//              trait CanonicalCmp {
//                  fn canonical_cmp(&self, other: &Self) -> Ordering {
//                      self.canonical_cmp_lower_names(other, false)
//                  }
//                  fn canonical_cmp_lower_names(&self, other: &Self) -> Ordering;
//              }
//
pub trait WireOrd {
    /// Compare wire.
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError>;
    /// Compare wire with names evaluated in canonical form.
    fn cmp_wire_canonical(
        left: &mut Reader<'_>,
        right: &mut Reader<'_>,
    ) -> Result<Ordering, WireOrdError> {
        Self::cmp_wire(left, right)
    }
    /// Compare `Self` as though it were in wire format.
    fn cmp_as_wire(&self, other: &Self) -> Ordering;
    /// Compare `Self` as though it were in canonical wire format.
    fn cmp_as_wire_canonical(&self, other: &Self) -> Ordering {
        self.cmp_as_wire(other)
    }
}

/// Determine the unpacked length of an item.
pub trait UnpackedLen {
    /// Returns the unpacked (and decompressed) length of the item.
    /// For types that represent RR data this is expected to return
    /// an error if the length exceeds 65,535 bytes.
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError>;
}

impl<'a, B> UnpackedLen for Cow<'a, B>
where
    B: ?Sized + ToOwned,
    <B as ToOwned>::Owned: UnpackedLen,
{
    fn unpacked_len(reader: &mut Reader<'_>) -> Result<usize, UnpackError> {
        <<B as ToOwned>::Owned>::unpacked_len(reader)
    }
}

impl<'a, T> WireOrd for Cow<'a, T>
where
    T: ?Sized + ToOwned + WireOrd,
{
    fn cmp_wire(left: &mut Reader<'_>, right: &mut Reader<'_>) -> Result<Ordering, WireOrdError> {
        T::cmp_wire(left, right)
    }
    fn cmp_wire_canonical(
        left: &mut Reader<'_>,
        right: &mut Reader<'_>,
    ) -> Result<Ordering, WireOrdError> {
        T::cmp_wire_canonical(left, right)
    }
    fn cmp_as_wire(&self, other: &Self) -> Ordering {
        T::cmp_as_wire(self, other)
    }
    fn cmp_as_wire_canonical(&self, other: &Self) -> Ordering {
        T::cmp_as_wire_canonical(self, other)
    }
}

/// Methods for writing a value in DNS wire format.
pub trait Pack {
    /// Packs the item to Writer. For types that represent RR data this
    /// is expected to return an error if the length exceeds 65,535 bytes.
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError>;
    /// Packs the item to a writer with a 2 byte length prefix.
    fn pack16(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        let dst = &mut dst.writer16()?;
        self.pack(dst)
    }
    /// Returns the packed (uncompressed) length of the item. For types
    /// that represent RR data this is expected to return an error if
    /// the length exceeds 65,535 bytes.
    fn packed_len(&self) -> Result<usize, PackError>;
}

impl<T> Pack for Option<T>
where
    T: Pack,
{
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        if let Some(t) = self.as_ref() {
            t.pack(dst)
        } else {
            Ok(())
        }
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        if let Some(t) = self.as_ref() {
            t.packed_len()
        } else {
            Ok(0)
        }
    }
}

impl<'a, T> Pack for &'a T
where
    T: Pack,
{
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        T::pack(*self, dst)
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        T::packed_len(*self)
    }
}

impl<'a, B> Pack for Cow<'a, B>
where
    B: ?Sized + ToOwned + Pack,
    <B as ToOwned>::Owned: Pack,
{
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        match *self {
            Cow::Borrowed(b) => Pack::pack(b, dst),
            Cow::Owned(ref o) => Pack::pack(o, dst),
        }
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        match *self {
            Cow::Borrowed(b) => Pack::packed_len(b),
            Cow::Owned(ref o) => Pack::packed_len(o),
        }
    }
}

impl<T> Pack for [T]
where
    T: Pack,
{
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        self.iter().map(|t| t.pack(dst)).collect()
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        self.iter().map(|t| t.packed_len()).sum()
    }
}

impl<'a, T> Pack for &'a [T]
where
    T: Pack,
{
    fn pack(&self, dst: &mut Writer<'_>) -> Result<(), PackError> {
        self.iter().map(|t| t.pack(dst)).collect()
    }
    fn packed_len(&self) -> Result<usize, PackError> {
        self.iter().map(|t| t.packed_len()).sum()
    }
}

pub trait Unpack<'a>: Sized {
    /// Unpack `self` from a `Reader`.
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError>;
    /// Unpack a 2 byte length prefixed serialisation of `self`.
    fn unpack16(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        let mut r = reader.reader16()?;
        let me = Self::unpack(&mut r)?;
        if r.is_empty() {
            Ok(me)
        } else {
            Err(UnpackError::trailing_junk_at(r.index()))
        }
    }
}

impl<'a, T> Unpack<'a> for Option<T>
where
    T: Unpack<'a>,
{
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        if reader.is_empty() {
            Ok(None)
        } else {
            T::unpack(reader).map(Some)
        }
    }
}

impl<'a, B> Unpack<'a> for Cow<'a, B>
where
    B: Clone + Unpack<'a>,
{
    fn unpack(reader: &mut Reader<'a>) -> Result<Self, UnpackError> {
        Ok(Cow::Owned(Unpack::unpack(reader)?))
    }
}
