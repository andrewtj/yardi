use super::SSA;

#[test]
fn rfc1982_u8_comparable() {
    let cases: &[(u8, u8)] = &[
        (1, 0),
        (44, 0),
        (100, 0),
        (100, 44),
        (200, 100),
        (255, 200),
        (0, 255),
        (100, 255),
        (0, 200),
        (44, 200),
    ];
    for &(g, l) in cases {
        assert!(g.ssa_gt(l));
        assert!(l.ssa_lt(g));
        assert!(!l.ssa_gt(g));
        assert!(!g.ssa_lt(l));
    }
}

#[test]
fn rfc1982_u8_uncomparable() {
    let cases: &[(u8, u8)] = &[(0, 0), (0, 128), (1, 129), (2, 130), (127, 255)];
    for &(a, b) in cases {
        assert!(!a.ssa_gt(b));
        assert!(!b.ssa_gt(a));
    }
}

#[test]
fn rfc1982_u8_add() {
    let cases: &[(u8, u8, Option<u8>)] = &[
        (255, 1, Some(0)),
        (100, 100, Some(200)),
        (200, 100, Some(44)),
    ];
    for &(a, b, expect) in cases {
        assert_eq!(a.ssa_inc(b), expect);
    }
}

macro_rules! test_impl {
    ($ty:tt, $s:expr) => {
        mod $ty {
            use super::super::SSA;
            #[test]
            fn test_max() {
                let max: $ty = $s;
                let two: $ty = 2;
                let expected_max: $ty = two.pow((core::mem::size_of::<$ty>() as u32 * 8) - 1);
                assert_eq!(max, expected_max);
                assert_eq!(($ty::max_value() / 2) + 1, expected_max);
            }
            #[test]
            fn test_comparable() {
                let zero: $ty = 0;
                for i1 in (zero..10).into_iter().chain(zero.wrapping_sub(10)..0) {
                    let i2 = i1.wrapping_sub($s / 2);
                    assert!(i1.ssa_gt(i2));
                    assert!(i2.ssa_lt(i1));
                    assert!(!i2.ssa_gt(i1));
                    assert!(!i1.ssa_lt(i2));
                }
            }
            #[test]
            fn test_uncomparable() {
                let zero: $ty = 0;
                for i1 in (zero..10).into_iter().chain(zero.wrapping_sub(10)..0) {
                    let i2 = i1.wrapping_add($s);
                    assert!(!i1.ssa_gt(i1));
                    assert!(!i2.ssa_gt(i2));
                }
            }
        }
    };
}
test_impl!(u8, (u8::max_value() / 2) + 1);
test_impl!(u16, (u16::max_value() / 2) + 1);
test_impl!(u32, (u32::max_value() / 2) + 1);
test_impl!(u64, (u64::max_value() / 2) + 1);
