// TODO: is this worth keeping? is it correct?

//! Support for [Sequence Space (Serial Number) Arithmetic][1].
//!
//! [1]: https://tools.ietf.org/html/rfc1982 "Serial Number Arithmetic"

#[cfg(test)]
mod test;

/// Sequence Space (Serial Number) Arithmetic.
pub trait SSA: Sized {
    /// Returns whether `self` is less than `other`.
    fn ssa_lt(self, other: Self) -> bool;
    /// Returns whether `self` is greater than `other`.
    fn ssa_gt(self, other: Self) -> bool;
    /// Increment `self` by `other`, if possible.
    fn ssa_inc(self, other: Self) -> Option<Self>;
}

macro_rules! impl_ssa {
    ($ty:ty, $s:expr) => {
        impl SSA for $ty {
            fn ssa_lt(self, other: $ty) -> bool {
                // (i1 < i2 and i2 - i1 < 2^(SERIAL_BITS - 1)) or
                // (i1 > i2 and i1 - i2 > 2^(SERIAL_BITS - 1))
                let (i1, i2) = (self, other);
                ((i1 < i2) && (i2 - i1) < $s) || ((i1 > i2) && (i1 - i2) > $s)
            }
            fn ssa_gt(self, other: $ty) -> bool {
                // (i1 < i2 and i2 - i1 > 2^(SERIAL_BITS - 1)) or
                // (i1 > i2 and i1 - i2 < 2^(SERIAL_BITS - 1))
                let (i1, i2) = (self, other);
                ((i1 < i2) && (i2 - i1) > $s) || ((i1 > i2) && (i1 - i2) < $s)
            }
            fn ssa_inc(self, other: $ty) -> Option<Self> {
                if other < $s {
                    Some(self.wrapping_add(other))
                } else {
                    None
                }
            }
        }
    };
}
impl_ssa!(u8, (u8::MAX / 2) + 1);
impl_ssa!(u16, (u16::MAX / 2) + 1);
impl_ssa!(u32, (u32::MAX / 2) + 1);
impl_ssa!(u64, (u64::MAX / 2) + 1);
