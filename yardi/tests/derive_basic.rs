use yardi_derive::*;

#[derive(Debug, Parse, Present, Pack, Unpack, UnpackedLen, WireOrd)]
pub struct Test(u8);

#[test]
fn derive_basic_test() {
    use yardi as y;
    fn req_imp<T>(_: T)
    where
        T: y::ascii::parse::Parse
            + y::ascii::present::Present
            + y::wire::Pack
            + for<'a> y::wire::Unpack<'a>
            + y::wire::UnpackedLen
            + y::wire::WireOrd,
    {
    }
    req_imp(Test(0));
}

pub trait Me {
    type Me;
}

impl<T> Me for T {
    type Me = T;
}

#[derive(Debug, Parse, Present, Pack, Unpack, UnpackedLen, WireOrd, Rdata)]
#[yardi(rr_ty = 65280)]
pub struct Test2<T = u8>(T);

#[derive(Debug, Parse, Present, Pack, Unpack, UnpackedLen, WireOrd, Rdata)]
#[yardi(rr_ty = 65280)]
pub struct Test3<T: Me>(T::Me);

#[test]
fn derive_basic_test2() {
    use yardi as y;
    fn req_imp<T>(_: T)
    where
        T: y::ascii::parse::Parse
            + y::ascii::present::Present
            + y::wire::Pack
            + for<'a> y::wire::Unpack<'a>
            + y::wire::UnpackedLen
            + y::wire::WireOrd
            + y::rdata::Rdata
            + y::rdata::StaticRdata,
    {
    }
    req_imp(Test2::<u8>(0));
    req_imp(Test3::<u8>(0));
}
