mod asset;

#[test]
fn it_works() {
    asset!(Me: "asset.rs");
    let dir = Me::extract().expect("extract");
    let expect = include_str!("asset.rs");
    let actual = std::fs::read_to_string(dir.path().join("asset.rs")).expect("read asset.rs");
    assert_eq!(expect, actual);
}
