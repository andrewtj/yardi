#![cfg(feature = "std")]

mod asset;

use yardi::ascii::{Context, ParseError};
use yardi::datatypes::{name, Class, Ty};
use yardi::msg::{Header, Question, Rr};
use yardi::rdata::Generic;
use yardi::wire::{Reader, Unpack, Writer};
use yardi::zone::ZoneReader;

asset!(Types: "sidnwb_types.txt");

#[test]
fn sidn_parse() {
    let asset_dir = Types::extract().expect("extract assets");
    let mut context = Context::default();
    context.set_origin(name!("sidnlabs.nl."));
    context.set_parse_wks_ip_protocol(|s| match s {
        "tcp" => Ok(6),
        "udp" => Ok(17),
        _ => s.parse().map_err(|_| ParseError::value_unknown()),
    });
    context.set_parse_wks_service(|_, s| match s {
        "domain" => Ok(53),
        "tcpmux" => Ok(1),
        "ftp" => Ok(21),
        "smtp" => Ok(25),
        "telnet" => Ok(23),
        _ => s.parse().map_err(|_| ParseError::value_unknown()),
    });
    let path = asset_dir.path().join("sidnwb_types.txt");
    let mut reader = ZoneReader::new(&path, context).expect("open zone");
    let mut failures = 0;
    while let Some(result) = reader.next() {
        if let Err(err) = result {
            failures += 1;
            let pos = reader.position();
            let s = reader.skip_entry().expect("Option<_>").expect("ascii");
            println!("; Failure {} at {:?} - {:?}:\n{}", failures, pos, err, s);
        }
    }
    if failures != 0 {
        panic!("Failed to parse {} entries", failures);
    }
}

struct MsgIter {
    reader: Reader<'static>,
}

impl MsgIter {
    fn new(buf: &'static [u8]) -> Self {
        let reader = Reader::new(buf);
        MsgIter { reader }
    }
}

impl Iterator for MsgIter {
    type Item = &'static [u8];
    fn next(&mut self) -> Option<Self::Item> {
        if self.reader.is_empty() {
            return None;
        }
        Some(self.reader.get_bytes16().unwrap())
    }
}

#[test]
fn sidn_unpack() {
    let qname = name!("types.wb.sidnlabs.nl.");
    let mut ascii_tmp = String::with_capacity(0xFFF);
    for (i, msg) in MsgIter::new(include_bytes!("sidnwb_types.bin")).enumerate() {
        println!("msg {}", i);
        let mut r = Reader::new(msg);
        let header = Header::unpack(&mut r).expect("header");
        assert_eq!(header.qdcount(), 1);
        let q = Question::unpack(&mut r).expect("q");
        assert_eq!(q.name, qname);
        assert_eq!(q.ty, Ty::AXFR);
        assert_eq!(q.class, Class::IN);
        for i in 0..header.ancount() + header.nscount() + header.arcount() {
            {
                let mut r = r.clone();
                let q = Question::unpack(&mut r).expect("pseudo q");
                println!(" {} - {} {}", i, q.name, q.ty);
            }
            let rr = <Rr<Generic>>::unpack(&mut r).expect("rr");
            let ty = rr.data.ty();
            {
                let mut data_r = rr.data.reader();
                let data_r_initial = data_r.index();
                let mut data_w_buf = vec![0; 0xFFFF];
                let mut data_w = Writer::new(&mut data_w_buf[..]);
                data_w.set_format(::yardi::wire::Format::Plain);
                ::yardi::rdata::copy(rr.class, ty, &mut data_r, &mut data_w).expect("copy");
                assert!(data_r.is_empty());
                let mut data_w_r = Reader::new(data_w.written());
                assert!(::yardi::rdata::eq(
                    rr.class,
                    ty,
                    data_r.set_index(data_r_initial),
                    &mut data_w_r
                )
                .expect("eq"));
                assert!(data_w_r.is_empty());
            }
            ascii_tmp.clear();
            ::yardi::ascii::present::to_string(&rr.data, &mut ascii_tmp).expect("present");
            let rt = Generic::from_str(rr.class, ty, &ascii_tmp[..]).expect("rdata");
            assert_eq!(&rr.data, &rt);
        }
        assert!(r.is_empty());
    }
}
