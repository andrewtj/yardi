#![cfg(feature = "std")]

mod asset;

use std::net::Ipv4Addr;

use yardi::ascii::Context;
use yardi::datatypes::{name, Class, Name, Ttl};
use yardi::msg::Rr;
use yardi::rdata::*;
use yardi::wire::Unpack;
use yardi::zone::ZoneReader;

asset!(
    ZoneFiles:
    "rfc1035_zone_file_extra_include.txt",
    "rfc1035_zone_file_main.txt",
    "rfc1035_zone_file_mailboxes.txt",
);

#[test]
fn test_include_limit() {
    let asset_dir = ZoneFiles::extract().expect("extract zone files");
    let cases = &[(0, 0, true), (1, 11, true), (2, 17, false)];
    let mut context = Context::default();
    context
        .set_origin(name!("ISI.EDU."))
        .set_last_ttl(Ttl(86_400));
    for (i, &(limit, ok, should_fail)) in cases.iter().enumerate() {
        println!("case {}", i);
        let path = asset_dir.path().join("rfc1035_zone_file_extra_include.txt");
        let mut reader = ZoneReader::new(&path, context.clone()).expect("load");
        reader.set_max_includes(limit);
        for _ in 0..ok {
            reader.next().expect("result item").expect("ok");
        }
        if should_fail {
            reader.next().expect("failed result item").expect_err("err");
        } else {
            assert!(reader.next().is_none());
        }
    }
}

#[allow(clippy::needless_pass_by_value)]
fn take<T>(parser: &mut ZoneReader, expected_name: Name, expected_data: T)
where
    T: StaticRdata + for<'a> Unpack<'a> + ::std::fmt::Debug + PartialEq,
{
    match parser.next() {
        Some(Ok(Rr {
            name,
            class: Class::IN,
            ttl,
            data,
        })) => {
            assert_eq!(name, expected_name);
            assert_eq!(data.ty(), T::TY);
            assert_eq!(ttl, Ttl(86_400));
            let d = T::unpack(&mut data.reader()).expect("T::unpack");
            assert_eq!(expected_data, d);
            println!("{:?}", (name, data.ty()));
        }
        x => panic!("did not expect: {:?}", x),
    };
}
#[test]
fn test_rfc1035_example() {
    let asset_dir = ZoneFiles::extract().expect("extract zone files");
    let mut context = Context::default();
    context
        .set_origin(name!("ISI.EDU."))
        .set_last_ttl(Ttl(86_400));
    let path = asset_dir.path().join("rfc1035_zone_file_main.txt");
    let mut r = ZoneReader::new(path, context).expect("open main");
    take(
        &mut r,
        name!("ISI.EDU."),
        Soa {
            mname: name!("VENERA.ISI.EDU."),
            rname: name!("Action\\.domains.ISI.EDU."),
            serial: 20,
            refresh: Ttl(7200),
            retry: Ttl(600),
            expire: Ttl(3_600_000),
            minimum: Ttl(60),
        },
    );
    take(
        &mut r,
        name!("ISI.EDU."),
        Ns {
            ns: name!("A.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("ISI.EDU."),
        Ns {
            ns: name!("VENERA.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("ISI.EDU."),
        Ns {
            ns: name!("VAXA.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("ISI.EDU."),
        Mx {
            preference: 10,
            exchange: name!("VENERA.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("ISI.EDU."),
        Mx {
            preference: 20,
            exchange: name!("VAXA.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("A.ISI.EDU."),
        A {
            ip: Ipv4Addr::new(26, 3, 0, 103),
        },
    );
    take(
        &mut r,
        name!("VENERA.ISI.EDU."),
        A {
            ip: Ipv4Addr::new(10, 1, 0, 52),
        },
    );
    take(
        &mut r,
        name!("VENERA.ISI.EDU."),
        A {
            ip: Ipv4Addr::new(128, 9, 0, 32),
        },
    );
    take(
        &mut r,
        name!("VAXA.ISI.EDU."),
        A {
            ip: Ipv4Addr::new(10, 2, 0, 27),
        },
    );
    take(
        &mut r,
        name!("VAXA.ISI.EDU."),
        A {
            ip: Ipv4Addr::new(128, 9, 0, 33),
        },
    );
    take(
        &mut r,
        name!("MOE.ISI.EDU."),
        Mb {
            madname: name!("A.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("LARRY.ISI.EDU."),
        Mb {
            madname: name!("A.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("CURLEY.ISI.EDU."),
        Mb {
            madname: name!("A.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("STOOGES.ISI.EDU."),
        Mg {
            mgmname: name!("MOE.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("STOOGES.ISI.EDU."),
        Mg {
            mgmname: name!("LARRY.ISI.EDU."),
        },
    );
    take(
        &mut r,
        name!("STOOGES.ISI.EDU."),
        Mg {
            mgmname: name!("CURLEY.ISI.EDU."),
        },
    );
    assert!(r.next().is_none());
}
