use std::cmp::Ordering;

use yardi::ascii::{present, Context, ParseError, Parser, PresentError, Presenter};
use yardi::wire::{Pack, PackError, Reader, Unpack, UnpackError, WireOrd, WireOrdError, Writer};
use yardi_derive::*;

fn check_utf8(s: &[u8]) -> Result<&str, UnpackError> {
    std::str::from_utf8(&s).map_err(|_| UnpackError::value_invalid())
}

macro_rules! pack_fn {
    (
        $pack:ident,
        $write:ident,
        $unpack:ident,
        $read:ident,
        $cmp:ident,
        $unpacked_len:ident
    ) => {
        fn $pack(s: &str, w: &mut Writer) -> Result<(), PackError> {
            w.$write(s.as_bytes())
        }
        fn $unpack(r: &mut Reader) -> Result<String, UnpackError> {
            let s = r.$read()?;
            check_utf8(&s[..]).map(|s| s.to_string())
        }
        fn $cmp(left: &mut Reader, right: &mut Reader) -> Result<Ordering, WireOrdError> {
            let l = left.$read().map_err(WireOrdError::Left)?;
            check_utf8(&l).map_err(WireOrdError::Left)?;
            let r = right.$read().map_err(WireOrdError::Right)?;
            check_utf8(&r).map_err(WireOrdError::Right)?;
            Ok(l.cmp(&r))
        }
        fn $unpacked_len(r: &mut Reader) -> Result<usize, UnpackError> {
            r.$read().and_then(|b| {
                check_utf8(&b[..])?;
                Ok(b.len())
            })
        }
    };
}

pack_fn!(pack, write_bytes, unpack, get_bytes_rest, cmp, unpacked_len);
pack_fn!(
    pack8,
    write_bytes8,
    unpack8,
    get_bytes8,
    cmp8,
    unpacked_len8
);
pack_fn!(
    pack16,
    write_bytes16,
    unpack16,
    get_bytes16,
    cmp16,
    unpacked_len16
);

fn cmpas(l: &str, r: &str) -> Ordering {
    l.cmp(r)
}

fn cmp8as(l: &str, r: &str) -> Ordering {
    match (l.len() as u8).cmp_as_wire(&(r.len() as u8)) {
        Ordering::Equal => cmpas(l, r),
        other => other,
    }
}

fn cmp16as(l: &str, r: &str) -> Ordering {
    match (l.len() as u16).cmp_as_wire(&(r.len() as u16)) {
        Ordering::Equal => cmpas(l, r),
        other => other,
    }
}

fn parse(p: &mut Parser<'_, '_>) -> Result<String, ParseError> {
    p.pull(|s| Ok(s.to_string()))
}

fn present(s: &str, p: &mut Presenter<'_, '_>) -> Result<(), PresentError> {
    p.write(s)
}

fn pack8_len(s: &str) -> Result<usize, PackError> {
    if s.len() > 0xFF {
        Err(PackError::value_large())
    } else {
        Ok(1 + s.len())
    }
}

fn pack16_len(s: &str) -> Result<usize, PackError> {
    if s.len() > 0xFFFF {
        Err(PackError::value_large())
    } else {
        Ok(2 + s.len())
    }
}

#[derive(Debug, Clone, PartialEq, Parse, Present, Pack, Unpack, UnpackedLen, WireOrd, Rdata)]
#[yardi(rr_ty = "65280")]
struct Prefix {
    #[yardi(
        pack = "pack16",
        packed_len = "pack16_len",
        unpack = "unpack16",
        present = "present",
        parse = "parse",
        cmp_wire = "cmp16",
        cmp_wire_canonical = "cmp16",
        cmp_as_wire = "cmp16as",
        cmp_as_wire_canonical = "cmp16as",
        unpacked_len = "unpacked_len16"
    )]
    p16: String,
    #[yardi(
        pack = "pack8",
        packed_len = "pack8_len",
        unpack = "unpack8",
        present = "present",
        parse = "parse",
        cmp_wire = "cmp8",
        cmp_wire_canonical = "cmp8",
        cmp_as_wire = "cmp8as",
        cmp_as_wire_canonical = "cmp8as",
        unpacked_len = "unpacked_len8"
    )]
    p8: String,
    #[yardi(
        pack = "pack",
        packed_len = "|m| Ok(m.len())",
        unpack = "unpack",
        present = "present",
        parse = "parse",
        cmp_wire = "cmp",
        cmp_wire_canonical = "cmp",
        cmp_as_wire = "cmpas",
        cmp_as_wire_canonical = "cmpas",
        unpacked_len = "unpacked_len"
    )]
    np: String,
}

#[test]
fn derive_wire() {
    let input = b"\x00\x01a\x01bc";
    let expect = Prefix {
        p16: "a".to_string(),
        p8: "b".to_string(),
        np: "c".to_string(),
    };
    let mut r = Reader::new(input);
    let actual = Prefix::unpack(&mut r).expect("unpack");
    assert_eq!(expect, actual);
    let mut buf = [0u8; 6];
    assert_eq!(buf.len(), input.len());
    let mut w = Writer::new(&mut buf);
    actual.pack(&mut w).expect("pack a");
    assert_eq!(w.written(), input);
    assert_eq!(
        Prefix::cmp_wire(&mut Reader::new(input), &mut Reader::new(input)),
        Ok(Ordering::Equal)
    );
    let less = b"\x00\x01a\x01bb";
    assert_eq!(
        Prefix::cmp_wire(&mut Reader::new(input), &mut Reader::new(less)),
        Ok(Ordering::Greater)
    );
    assert_eq!(
        Prefix::cmp_wire(&mut Reader::new(less), &mut Reader::new(input)),
        Ok(Ordering::Less)
    );
}

#[test]
fn derive_ascii() {
    let input = "a b c";
    let expect = Prefix {
        p16: "a".to_string(),
        p8: "b".to_string(),
        np: "c".to_string(),
    };
    let actual: Prefix =
        ::yardi::ascii::parse::from_single_entry(input.as_bytes(), Context::shared_default())
            .expect("Prefix");
    assert_eq!(actual, expect);
    let mut output = String::new();
    present::to_string(&actual, &mut output).expect("present");
    assert_eq!(input, output);
}

#[test]
fn derive_custom() {
    #[derive(Debug, PartialEq, Parse)]
    struct Bound<T>(T);
    let input = "1";
    assert_eq!(
        Ok(Bound(1u8)),
        yardi::ascii::parse::from_single_entry(input.as_bytes(), Context::shared_default())
    );
}
