use yardi::{
    datatypes::{name, Class, Time, Ttl, Ty},
    dnssec,
    msg::{sig0, Header, Opcode, Question, Rcode, Rr},
    rdata::Key,
    wire::{Pack, Unpack, Writer},
};

#[test]
fn ecdsap256sha256() {
    let keyrr_str = include_str!("support/Ksig0-ecdsap256sha256.example.+013+07495.key");
    let priv_str = include_str!("support/Ksig0-ecdsap256sha256.example.+013+07495.private");
    let bind = crate::Bind::shared().expect("bind");
    let keyrr = {
        let mut ctx = yardi::ascii::Context::default();
        ctx.set_default_ttl(Ttl(0));
        let rr: Rr<Key> =
            yardi::ascii::parse::from_single_entry(keyrr_str.as_bytes(), &ctx).expect("Rr<Key>");
        rr
    };
    let mut signer = {
        let pkmap = dnssec::PrivateKeyMap::new(priv_str).expect("private key map");
        dnssec::boxed_sign(&pkmap, &keyrr.data.public_key).expect("signer")
    };

    let question = Question {
        name: name!("example."),
        ty: Ty::SOA,
        class: Class::IN,
    };

    let mut query_buf = [0u8; 512];
    let query_buf = {
        let mut w = Writer::new(&mut query_buf[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header"))
            .set_opcode(Opcode::QUERY)
            .set_qdcount(1);
        question.pack(&mut w).expect("pack question");
        sig0::sign_request(
            &keyrr.name,
            dnssec::SigAlg::ECDSAP256SHA256,
            keyrr.data.tag(),
            signer.as_mut(),
            Time::now(),
            &mut w,
        )
        .expect("sign");
        w.into_written()
    };

    let response_buf = bind.tcp_exchange(query_buf);
    let mut r = yardi::wire::Reader::new(&response_buf);
    let h = Header::unpack(&mut r).expect("unpack header");
    assert_eq!(h.rcode(), Rcode::NOERROR);

    drop(bind);
}
