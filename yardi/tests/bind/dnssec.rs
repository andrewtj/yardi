use yardi::datatypes::{name, Class, Name, Ttl, Ty};
use yardi::dnssec::{self, SigAlg};
use yardi::msg::edns::{EdnsFlags, OptHeader};
use yardi::msg::{Header, Question, Rcode};
use yardi::rdata::{Dnskey, Rrsig};
use yardi::wire::{self, Pack, Reader, Unpack, WireOrd, Writer};

#[test]
fn dnskey() {
    let bind = crate::Bind::shared().expect("bind");

    let apex = name!("EXAMPLE.");

    let mut query_buf = [0u8; 512];
    let query_buf = {
        let mut w = Writer::new(&mut query_buf[..]);
        Header::default()
            .set_qdcount(1)
            .set_arcount(1)
            .pack(&mut w)
            .expect("write header");
        Question {
            name: apex.clone(),
            ty: Ty::DNSKEY,
            class: Class::IN,
        }
        .pack(&mut w)
        .expect("pack question");
        name!(".").pack(&mut w).expect("pack opt owner name");
        Ty::OPT.pack(&mut w).expect("pack opt ty");
        OptHeader {
            udp_payload_size: 1024,
            rcode_bits: 0,
            version: 0,
            flags: EdnsFlags::DO,
        }
        .pack(&mut w)
        .expect("pack opt meta");
        w.writer16().expect("pack opt data");
        w.into_written()
    };

    let response_buf = bind.tcp_exchange(query_buf);
    let mut r = Reader::new(&response_buf);
    let response_h = Header::unpack(&mut r).expect("header");
    assert_eq!(response_h.rcode(), Rcode::NOERROR);
    assert_eq!(response_h.qdcount(), 1);
    let ancount = response_h.ancount() as usize;
    assert_eq!(ancount % 2, 0);
    let _ = Question::unpack(&mut r).expect("unpack question");
    let mut dnskeys = Vec::with_capacity(ancount / 2);
    let mut rrsigs = Vec::with_capacity(ancount / 2);
    for _ in 0..ancount {
        let owner_name = Name::unpack(&mut r).expect("unpack owner name");
        assert_eq!(owner_name, apex);
        let ty = Ty::unpack(&mut r).expect("unpack ty");
        assert_eq!(Class::IN, Class::unpack(&mut r).expect("unpack class"));
        let _ = Ttl::unpack(&mut r).expect("unpack ttl");
        let mut rdata_r = r.reader16().expect("unpack data");
        match ty {
            Ty::DNSKEY => {
                let start = rdata_r.index();
                let t = <Dnskey>::unpack(&mut rdata_r).expect("unpack dnskey");
                assert!(rdata_r.is_empty());
                let end = rdata_r.index();
                dnskeys.push((t, (start, end)));
            }
            Ty::RRSIG => {
                let t = <Rrsig>::unpack(&mut rdata_r).expect("unpack rrsig");
                rrsigs.push(t);
            }
            _ => panic!("unexpected type: {}", ty),
        }
    }
    assert_eq!(dnskeys.len(), rrsigs.len());
    dnskeys.sort_by(|&(_, (l_start, l_end)), &(_, (r_start, r_end))| {
        let mut b_l = response_buf.clone();
        b_l.truncate(l_end);
        let mut r_l = Reader::new_index(&b_l, l_start);
        let mut b_r = response_buf.clone();
        b_r.truncate(r_end);
        let mut r_r = Reader::new_index(&b_r, r_start);
        <Dnskey>::cmp_wire_canonical(&mut r_l, &mut r_r).expect("cmp wire canonical")
    });
    let dnskey_rrset_len = dnskeys.iter().fold(0, |n, &(_, (start, end))| {
        n + apex.len() + 2 + 2 + 4 + 2 + (end - start)
    });
    let mut buf = vec![0; dnskey_rrset_len];
    let mut w = Writer::new(&mut buf[..]);
    w.set_format(wire::Format::Dnssec);
    let ttl = rrsigs[0].original_ttl;
    for (ref d, _) in &dnskeys {
        apex.pack(&mut w).expect("pack owner");
        Ty::DNSKEY.pack(&mut w).expect("pack ty");
        Class::IN.pack(&mut w).expect("pack class");
        ttl.pack(&mut w).expect("pack ttl");
        d.pack16(&mut w).expect("pack data");
    }
    assert_eq!(w.len(), dnskey_rrset_len);
    let dnskey_rrset = w.into_written();
    let mut buf = vec![0u8; 2 * dnskey_rrset_len];
    for rrsig in &rrsigs {
        if rrsig.alg == SigAlg::RSAMD5 {
            continue;
        }
        let mut w = Writer::new(&mut buf[..]);
        let key = dnskeys
            .iter()
            .find(|(ref k, _)| k.tag() == rrsig.key_tag)
            .map(|(ref k, _)| k)
            .expect("key for signature");
        rrsig.pack_no_sig(&mut w).expect("write digest data");
        w.write_bytes(dnskey_rrset).expect("write rrset");
        assert_eq!(
            dnssec::verify(key.alg, &key.public_key, w.written(), &rrsig.signature),
            dnssec::VerifyResult::Valid
        );
    }
}
