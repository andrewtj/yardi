echo "oversized TXT \"This RRset is too large for a TCP query but can be transferred\""
for i in $(seq -w 0 1 255); do
    printf " TXT "
    for _ in $(seq 1 255); do
        printf "\%s" $i
    done
    printf "\n"
done