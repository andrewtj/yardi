use yardi::{
    datatypes::{name, Class, Name, TsigTime, Ty},
    msg::{
        self,
        tsig::{self, Options, TsigRr, HMAC_SHA256, HMAC_SHA512},
        Header, Opcode, Question, Rcode,
    },
    wire::{Pack, Reader, Unpack, Writer},
};

macro_rules! test_alg {
    ($mod_name:ident, $key_name:expr, $alg_name:expr, $bits:expr) => {
        mod $mod_name {
            use super::*;
            #[test]
            fn query() {
                let secret = [0u8; $bits / 8];
                test_query(&$alg_name, &$key_name, &secret[..], 0)
            }

            #[test]
            fn query_trunc() {
                let secret = [0u8; $bits / 8];
                let trunc = std::cmp::max(10, $bits / 8 / 2);
                test_query(&$alg_name, &$key_name, &secret[..], trunc)
            }

            #[test]
            fn axfr() {
                let secret = [0u8; $bits / 8];
                test_axfr(&$alg_name, &$key_name, &secret[..], 0)
            }

            #[test]
            fn axfr_trunc() {
                let secret = [0u8; $bits / 8];
                let trunc = std::cmp::max(10, $bits / 8 / 2);
                test_axfr(&$alg_name, &$key_name, &secret[..], trunc)
            }
        }
    };
}
test_alg!(hmac_sha256, HMAC_SHA256, HMAC_SHA256, 256);
test_alg!(hmac_sha512, HMAC_SHA512, HMAC_SHA512, 512);

fn split_message(msg: &[u8]) -> Option<(&[u8], TsigRr)> {
    let mut r = Reader::new(msg);
    tsig::seek(&mut r).expect("Option<_>")
}

fn query_buf_len(q: &Question, key_name: &Name, alg_name: &Name, trunc: u16) -> usize {
    let mac_len = if trunc == 0 {
        tsig::mac_len(alg_name).expect("alg should be supported")
    } else {
        trunc as usize
    };
    12 + q.packed_len().expect("q packed len") + key_name.len() +
            2 /* class */ + 2 /* ty */ + 4 /* ttl */ + 2 /* rdlen */ + alg_name.len() +
            6 /* time signed*/ + 2 /* fudge */ + 2 /* mac len */ + mac_len /* mac */ +
            2 /* original_id */ + 2 /* error */ + 2 /* other len */
}

fn test_query(alg: &Name, key_name: &Name, key_secret: &[u8], trunc: u16) {
    let bind = crate::Bind::shared().expect("bind");

    let question = Question {
        name: name!("example."),
        ty: Ty::SOA,
        class: Class::IN,
    };

    let mut options = Options::default();
    options.trunc(trunc);

    let mut vr;
    let mut query_buf = vec![0; query_buf_len(&question, key_name, alg, trunc)];
    let query_buf = {
        let mut w = Writer::new(&mut query_buf);
        Header::from_bytes_mut(w.write_forward(12).expect("header bytes"))
            .set_opcode(Opcode::UPDATE)
            .set_qdcount(1);
        question.pack(&mut w).expect("pack question");
        vr = tsig::sign_request_options(
            &mut w,
            alg,
            key_name,
            key_secret,
            TsigTime::now(),
            &options,
        )
        .expect("sign");
        w.into_written()
    };

    let response_buf = bind.tcp_exchange(query_buf);
    let (partial_message, tsig_rr) = split_message(&response_buf).expect("split signed message");

    vr.update(&[0xEE; 12][..], TsigTime::now(), &tsig_rr)
        .expect_err("Validated bad message");

    vr.update(partial_message, TsigTime::now(), &tsig_rr)
        .expect("Invalidated good message");

    drop(bind);
}

fn test_axfr(alg: &Name, key_name: &Name, key_secret: &[u8], trunc: u16) {
    let bind = crate::Bind::shared().expect("bind");

    let question = Question {
        name: name!("example."),
        ty: Ty::AXFR,
        class: Class::IN,
    };

    let mut vr;
    let mut query_buf = vec![0; query_buf_len(&question, key_name, alg, trunc)];
    let query_buf = {
        let mut w = Writer::new(&mut query_buf[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header bytes"))
            .set_opcode(Opcode::QUERY)
            .set_qdcount(1);
        question.pack(&mut w).expect("pack question");
        vr = tsig::sign_request(&mut w, alg, key_name, key_secret, TsigTime::now()).expect("sign");
        w.into_written()
    };

    let mut seen_last = false;
    let mut unsigned = <Vec<Vec<u8>>>::new();
    let options = Options::default();
    let mut seen = 0;
    for response_buf in bind.tcp_exchange_mult(query_buf) {
        seen += 1;
        assert!(!seen_last);
        seen_last = {
            let mut r = Reader::new(&response_buf);
            let h = Header::unpack(&mut r).expect("unpack header");
            assert_eq!(h.rcode(), Rcode::NOERROR);
            assert_eq!(h.arcount(), 1);
            match h.qdcount() {
                0 => (),
                1 => msg::skip_q(&mut r).expect("skip q"),
                _ => unreachable!(),
            }
            for _ in 1..h.ancount() {
                msg::skip_rr(&mut r).expect("skip answer rr");
            }
            Name::unpack(&mut r).expect("unpack last answer name");
            Ty::unpack(&mut r).expect("unpack last answer ty") == Ty::SOA
        };
        if let Some((partial, tsig_rr)) = split_message(&response_buf) {
            vr.update_unsigned(
                partial,
                &tsig_rr,
                TsigTime::now(),
                &options,
                unsigned.iter().map(AsRef::as_ref),
            )
            .expect("update signed subsequent message");
            unsigned.clear();
        } else {
            assert!(!seen_last);
            unsigned.push(response_buf);
        }
    }
    assert!(seen_last);
    assert!(seen > 1);
}
