use yardi::datatypes::{name, Class, Name, Ty};
use yardi::msg::edns::{self, ClientSubnet, Nsid, Opt, OptCode, OptRead, TcpKeepalive};
use yardi::msg::{Header, Question, Rcode};
use yardi::wire::{Pack, Reader, Unpack, Writer};

#[test]
fn nsid_query() {
    let bind = crate::Bind::shared().expect("bind");

    let question = Question {
        name: name!("example."),
        ty: Ty::CNAME,
        class: Class::IN,
    };
    let mut q = [0u8; 0xFF];
    let q = {
        let mut w = Writer::new(&mut q[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header buf"))
            .set_qdcount(1)
            .set_arcount(1);
        question.pack(&mut w).expect("write question");
        (Name::new(), Ty::OPT, 4096u16, 0u32)
            .pack(&mut w)
            .expect("write edns header");
        (OptCode::NSID, 0u16)
            .pack(w.writer16().expect("writer 16").as_mut())
            .expect("write nsid opt");
        w.into_written()
    };

    let r = bind.tcp_exchange(q);
    let mut r_r = Reader::new(&r);
    let r_h = Header::unpack(&mut r_r).expect("response header");
    assert_eq!(r_h.rcode(), Rcode::NOERROR);
    assert_eq!(r_h.qdcount(), 1);
    assert_eq!(r_h.arcount(), 1);
    let (_opt_h, optreader) = edns::seek_opt(&mut r_r)
        .expect("Option<_>")
        .expect("(OptHeader, OptReader)");
    let opt_read = optreader
        .get::<Nsid<&[u8]>>()
        .expect("Option<OptRead<Nsid>>")
        .expect("OptRead<Nsid>");
    if let OptRead::Opt(opt) = opt_read {
        assert_eq!(opt.payload, b"yardi");
    } else {
        panic!("opt_read: {:?}", opt_read);
    }

    drop(bind);
}

#[test]
fn client_subnet() {
    let bind = crate::Bind::shared().expect("bind");

    let question = Question {
        name: name!("example."),
        ty: Ty::CNAME,
        class: Class::IN,
    };
    let mut q = [0u8; 0xFF];
    let q = {
        let mut w = Writer::new(&mut q[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header buf"))
            .set_qdcount(1)
            .set_arcount(1);
        question.pack(&mut w).expect("pack question");
        (Name::new(), Ty::OPT, 4096u16, 0u32)
            .pack(&mut w)
            .expect("write edns header");
        ClientSubnet {
            family: 1,
            source_prefix_len: 24,
            scope_prefix_len: 0,
            addr: [127, 0, 0],
        }
        .pack_code(w.writer16().expect("writer 16").as_mut())
        .expect("pack code");
        w.into_written()
    };

    let r = bind.tcp_exchange(q);
    let mut r_r = Reader::new(&r);
    let r_h = Header::unpack(&mut r_r).expect("response header");
    assert_eq!(r_h.rcode(), Rcode::NOERROR);
    assert_eq!(r_h.qdcount(), 1);
    assert_eq!(r_h.arcount(), 1);
    let (_opt_h, optreader) = edns::seek_opt(&mut r_r)
        .expect("Option<_>")
        .expect("(OptHeader, OptReader)");
    let opt_read = optreader
        .get::<ClientSubnet<&[u8]>>()
        .expect("Option<OptRead<ClientSubnet>>")
        .expect("OptRead<ClientSubnet>");
    if let OptRead::Opt(opt) = opt_read {
        assert_eq!(opt.family, 1);
        assert_eq!(opt.source_prefix_len, 24);
    } else {
        panic!("opt_read: {:?}", opt_read);
    }

    drop(bind);
}

#[test]
fn tcp_keepalive() {
    let bind = crate::Bind::shared().expect("bind");

    let question = Question {
        name: name!("example."),
        ty: Ty::CNAME,
        class: Class::IN,
    };
    let mut q = [0u8; 0xFF];
    let q = {
        let mut w = Writer::new(&mut q[..]);
        Header::from_bytes_mut(w.write_forward(12).expect("header buf"))
            .set_qdcount(1)
            .set_arcount(1);
        question.pack(&mut w).expect("pack question");
        (Name::new(), Ty::OPT, 4096u16, 0u32)
            .pack(&mut w)
            .expect("write edns header");
        (OptCode::TCP_KEEPALIVE, 0u16)
            .pack(w.writer16().expect("writer 16").as_mut())
            .expect("write opt");
        w.into_written()
    };

    let r = bind.tcp_exchange(q);
    let mut r_r = Reader::new(&r);
    let r_h = Header::unpack(&mut r_r).expect("response header");
    assert_eq!(r_h.rcode(), Rcode::NOERROR);
    assert_eq!(r_h.qdcount(), 1);
    assert_eq!(r_h.arcount(), 1);
    let (_opt_h, optreader) = edns::seek_opt(&mut r_r)
        .expect("Option<_>")
        .expect("(OptHeader, OptReader)");
    let maybe_opt_read = optreader
        .get::<TcpKeepalive>()
        .expect("Option<OptRead<TcpKeepalive>>");
    match maybe_opt_read {
        None | Some(OptRead::Opt(_)) => (),
        Some(other) => panic!("no opt or valid opt expected, got: {:?}", other),
    }

    drop(bind);
}
