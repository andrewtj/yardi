#[macro_export]
macro_rules! asset {
    ($name:ident : $($asset:expr,)*) => {
        #[derive(Clone, Copy, Debug)]
        pub struct $name;

        impl $name {
            const ASSETS: &'static [(&'static str, &'static [u8])] = &[
                $(($asset, include_bytes!($asset))),*
            ];
            pub fn extract() -> std::io::Result<::tempfile::TempDir> {
                let dir = ::tempfile::TempDir::new()?;
                for &(name, content) in Self::ASSETS {
                    let dest = dir.path().join(name);
                    std::fs::create_dir_all(dest.parent().unwrap())?;
                    std::fs::write(dest, content)?;
                }
                Ok(dir)
            }
        }
    };
    ($name:ident : $($asset:expr),*) => {
        asset!($name: $($asset,)*);
    };
}
