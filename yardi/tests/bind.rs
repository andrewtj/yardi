#![cfg(all(feature = "bind_tests", not(miri)))]

// TODO: Remove these tests?
#[path = "asset.rs"]
mod asset;

use std::{
    io::{ErrorKind, Read, Write},
    net::{TcpStream, UdpSocket},
    process,
    sync::{Arc, Mutex, Once, Weak},
};

macro_rules! tests {
    ($($m:ident => $p:expr,)*) => {
        $(
            #[path = $p]
            mod $m;
        )*
    }
}

tests! {
    dnssec => "bind/dnssec.rs",
    edns => "bind/edns.rs",
    sig0 => "bind/sig0.rs",
    tsig => "bind/tsig.rs",
}

asset!(BindSandbox:
    "bind/support/example.db",
    "bind/support/example.db.oversized",
    "bind/support/example.db.signed",
    "bind/support/Kexample.+008+47140.key",
    "bind/support/Kexample.+008+47140.private",
    "bind/support/Kexample.+013+52582.key",
    "bind/support/Kexample.+013+52582.private",
    "bind/support/Ksig0-ecdsap256sha256.example.+013+07495.key",
    "bind/support/named.conf",
);

struct Bind {
    dir: tempfile::TempDir,
    child: process::Child,
    port: u16,
}

impl Drop for Bind {
    fn drop(&mut self) {
        if let Err(err) = self.child.kill() {
            println!(
                "Error killing named in {}: {}",
                self.dir.path().display(),
                err
            );
        }
    }
}

impl Bind {
    fn shared() -> Result<Arc<Self>, Box<dyn std::error::Error>> {
        static mut SHARED: *const Mutex<Weak<Bind>> = ::std::ptr::null();
        static ONCE: Once = Once::new();
        let mu = unsafe {
            ONCE.call_once(|| {
                SHARED = &*Box::leak(Box::new(Mutex::new(Weak::new())));
            });
            SHARED.as_ref().ok_or("SHARED once failed")?
        };
        let mut wref = mu.lock().expect("lock mutex");
        if let Some(arc) = wref.upgrade() {
            Ok(arc)
        } else {
            let arc = Arc::new(Bind::new()?);
            *wref = Arc::downgrade(&arc);
            Ok(arc)
        }
    }
    fn new() -> Result<Self, Box<dyn std::error::Error>> {
        let dir = BindSandbox::extract()?;
        let wd = dir.path().join("bind/support");
        let named_path = wd.join("named");
        // work around path based security tools...
        std::fs::copy(
            which::which("named").map_err(|e| e.to_string())?,
            &named_path,
        )?;
        let port = UdpSocket::bind("127.0.0.1:0")?.local_addr()?.port();
        let port_str = port.to_string();
        let id = format!("{}{}", env!("CARGO_PKG_NAME"), std::process::id());
        let mut cmd = process::Command::new("./named");
        cmd.current_dir(&wd)
            .args([
                "-p",
                &port_str,
                "-g",
                "-c",
                "named.conf",
                "-n",
                "1",
                "-D",
                &id,
            ])
            .stdin(process::Stdio::piped())
            .stderr(process::Stdio::piped());
        let mut child = cmd.spawn().or_else(|_| {
            std::thread::sleep(std::time::Duration::from_secs(1));
            cmd.spawn()
        })?;
        let mut attempts = 3;
        while TcpStream::connect(("127.0.0.1", port)).is_err() {
            if let Some(status) = child.try_wait()? {
                let out = child.wait_with_output()?;
                let stderr = String::from_utf8_lossy(&out.stderr[..]);
                return Err(format!("named died: {:?}\n{}", status, stderr).into());
            }
            attempts -= 1;
            if attempts == 0 {
                return Err("failed to connected to named".into());
            }
            std::thread::sleep(std::time::Duration::from_millis(100));
        }
        Ok(Bind { dir, child, port })
    }
    pub fn tcp_exchange(&self, msg: &[u8]) -> Vec<u8> {
        self.tcp_exchange_mult(msg).next().expect("response")
    }
    pub fn tcp_exchange_mult(&self, msg: &[u8]) -> impl Iterator<Item = Vec<u8>> {
        assert!(msg.len() <= 0xFFFF);
        let mut tmp = vec![0; 2 + msg.len()];
        tmp[..2].copy_from_slice(&(msg.len() as u16).to_be_bytes()[..]);
        tmp[2..].copy_from_slice(msg);
        let mut stream = TcpStream::connect(("127.0.0.1", self.port)).expect("connect");
        let five_seconds = ::std::time::Duration::new(5, 0);
        stream
            .set_read_timeout(Some(five_seconds))
            .expect("set read timeout");
        stream
            .set_write_timeout(Some(five_seconds))
            .expect("set write timeout");
        stream.set_nodelay(true).expect("set nodelay");
        stream.write_all(&tmp[..]).expect("write query");
        MultiResponse { stream }
    }
}

struct MultiResponse {
    stream: std::net::TcpStream,
}

impl Iterator for MultiResponse {
    type Item = Vec<u8>;
    fn next(&mut self) -> Option<Self::Item> {
        let mut tmp = [0u8; 2];
        if let Err(e) = self.stream.read_exact(&mut tmp[..]) {
            match e.kind() {
                ErrorKind::WouldBlock | ErrorKind::TimedOut => return None,
                _ => panic!("error reading response: {:?}", e),
            }
        }
        let len = u16::from_be_bytes([tmp[0], tmp[1]]) as usize;
        let mut buf = vec![0; len];
        self.stream.read_exact(&mut buf[..]).expect("read msg");
        Some(buf)
    }
}
