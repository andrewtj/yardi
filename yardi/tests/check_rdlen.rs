use yardi::{
    ascii::present,
    rdata::Caa,
    wire::{Pack, Reader, Unpack, UnpackedLen, WireOrd, Writer},
};

fn too_large_wire() -> Vec<u8> {
    let mut v = vec![b'b'; 1 + (1 + 1) + 0xFFFF];
    v[0] = 0;
    v[1] = 1;
    v[2] = b'a';
    v
}

#[test]
fn parse_too_large_is_err() {
    let mut s = String::with_capacity(0xFFFF * 2);
    s.push_str("0 abcd ");
    for _ in 0..0xFFFF {
        s.push_str("d");
    }
    assert!(s.parse::<Caa>().is_err());
}

#[test]
fn present_too_large_is_err() {
    let t = Caa::<&[u8], &[u8]> {
        tag: b"abcd",
        value: &[b'd'; 0xFFFF],
        ..Default::default()
    };
    let mut s = String::new();
    assert!(present::to_string(&t, &mut s).is_err());
}

#[test]
fn unpack_too_large_is_err() {
    let v = too_large_wire();
    let mut reader = Reader::new(&v);
    let r = <Caa>::unpack(&mut reader);
    assert!(r.is_err());
}

#[test]
fn unpacked_len_too_large_is_err() {
    let v = too_large_wire();
    let mut reader = Reader::new(&v);
    let r = <Caa>::unpacked_len(&mut reader);
    assert!(r.is_err());
}

#[test]
fn pack_too_large_is_err() {
    let t = Caa::<&[u8], &[u8]> {
        tag: b"abcd",
        value: &[b'd'; 0xFFFF],
        ..Default::default()
    };
    let mut buf = vec![0; 0xFFFF * 2];
    let mut w = Writer::new(&mut buf[..]);
    assert!(t.pack(&mut w).is_err());
}

#[test]
fn pack_len_too_large_is_err() {
    let t = Caa::<&[u8], &[u8]> {
        tag: b"abcd",
        value: &[b'd'; 0xFFFF],
        ..Default::default()
    };
    assert!(t.packed_len().is_err());
}

#[test]
fn wire_ord_too_large_is_err() {
    let v = too_large_wire();
    let mut l = Reader::new(&v);
    let mut r = l.clone();
    assert!(<Caa>::cmp_wire(&mut l, &mut r).is_err());
}
