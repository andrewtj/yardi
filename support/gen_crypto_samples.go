package main

import (
	"crypto"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"

	"github.com/miekg/dns"
)

const BASE_PATH = "../yardi/src/dnssec"

func init() {
	wd, err := os.Getwd()
	if err != nil {
		panic(err)
	}
	dir_suf := fmt.Sprintf("%csupport", os.PathSeparator)
	if !strings.HasSuffix(wd, dir_suf) {
		panic("expected working dir to end in " + dir_suf)
	}
}

func exists(paths ...string) bool {
	for _, path := range paths {
		_, err := os.Stat(path)
		switch {
		case err == nil:
		case os.IsNotExist(err):
			return false
		default:
			panic(err)
		}
	}
	return true
}

func main() {
	rsa_bits := []int{1024, 1280, 2040, 2048, 3072, 4096}
	cases := []struct {
		path string
		alg  uint8
		bits []int
	}{
		{"rsa", dns.RSASHA1, rsa_bits},
		{"rsa", dns.RSASHA256, rsa_bits},
		{"rsa", dns.RSASHA512, rsa_bits},
		{"eddsa", dns.ED25519, []int{32 * 8}},
		{"ecdsa", dns.ECDSAP256SHA256, []int{256}},
		{"ecdsa", dns.ECDSAP384SHA384, []int{384}},
	}

	for _, c := range cases {
		for _, bits := range c.bits {
			title := fmt.Sprintf("%s/%d", dns.AlgorithmToString[c.alg], bits)
			path := fmt.Sprintf("%s/%s/%s_%d",
				BASE_PATH,
				c.path,
				strings.ToLower(dns.AlgorithmToString[c.alg]),
				bits,
			)
			rr_fn := path + "_rr.txt"
			priv_fn := path + "_private.txt"
			if exists(rr_fn, priv_fn) {
				log.Printf("sample for %s exists", title)
				continue
			} else {
				log.Printf("generating sample for %s", title)
			}
			rr_fh, err := os.Create(rr_fn)
			if err != nil {
				log.Printf("failed to create %s: %v", rr_fn, err)
			}
			priv_fh, err := os.Create(priv_fn)
			if err != nil {
				log.Printf("failed to create %s: %v", priv_fn, err)
				rr_fh.Close()
			}
			if err := generate_sample(rr_fh, priv_fh, c.alg, bits); err != nil {
				log.Printf("failed to generate sample: %v", err)
			}
			if err := rr_fh.Close(); err != nil {
				log.Printf("failed to close %s: %v", rr_fn, err)
			}
			if err := priv_fh.Close(); err != nil {
				log.Printf("failed to close %s: %v", priv_fn, err)
			}
			log.Printf("generated samples for %s", title)
		}
	}

}

func generate_sample(rr_out, priv_out io.Writer, alg uint8, bits int) error {
	dnskey := &dns.DNSKEY{
		dns.RR_Header{
			"example.net.",
			dns.TypeDNSKEY,
			dns.ClassINET,
			3600,
			0,
		},
		257,
		3,
		alg,
		"",
	}
	pkey, err := dnskey.Generate(bits)
	if err != nil {
		return err
	}
	ds := dnskey.ToDS(dns.SHA1)
	a := &dns.A{
		dns.RR_Header{
			"www.example.net.",
			dns.TypeA,
			dns.ClassINET,
			3600,
			0,
		},
		net.IP{192, 0, 2, 1},
	}
	rrsig := &dns.RRSIG{
		dns.RR_Header{
			"www.example.net.",
			dns.TypeRRSIG,
			dns.ClassINET,
			3600,
			0,
		},
		dns.TypeA,
		alg,
		3,
		3600,
		1284026679,
		1281607479,
		dnskey.KeyTag(),
		dnskey.Hdr.Name,
		"",
	}
	err = rrsig.Sign(pkey.(crypto.Signer), []dns.RR{a})
	if err != nil {
		return err
	}
	_, err = fmt.Fprintf(rr_out,
		"; %d-bit %s sample\n%s\n%s\n%s\n%s\n",
		bits,
		dns.AlgorithmToString[alg],
		dnskey,
		ds,
		a,
		rrsig,
	)
	if err != nil {
		return err
	}
	_, err = fmt.Fprintf(priv_out, dnskey.PrivateKeyString(pkey))
	return err
}
