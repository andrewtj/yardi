#!/usr/bin/env bash
set -eux -o pipefail
export RUSTFLAGS="-C link-dead-code"
rustc --version
cargo --version
named -V
cargo tarpaulin \
	-p yardi --features bind_tests --examples --tests \
	-l -o json -o html --output-dir target/cov
