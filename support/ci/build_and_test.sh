#!/usr/bin/env bash
set -eux -o pipefail
rustc --version
cargo --version
named -V
cargo build -vv
cargo build -vv --features std
cargo test -vv --verbose --no-run --all-features
cargo test -vv --verbose --no-fail-fast
cargo test -vv --verbose --no-fail-fast --features bind_tests
