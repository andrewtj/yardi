#!/usr/bin/env bash
set -eux -o pipefail
rustc --version
cargo --version
cargo install -f --git https://github.com/kbknapp/cargo-outdated
cargo outdated -w -R --exit-code 1
for crate in codegen fuzz; do
    pushd "${crate}"
    cargo outdated -w -R --exit-code 1
    popd
done
