#!/usr/bin/env bash
set -eux -o pipefail
export DEBIAN_FRONT_END=noninteractive
export RUSTFLAGS="-C link-dead-code"
apt-get update -yqq
apt-get install -yqq -o=Dpkg::Use-Pty=0 bind9 bind9-dnsutils
