#!/usr/bin/env bash
set -eux -o pipefail
export DEBIAN_FRONT_END=noninteractive
export RUSTFLAGS="-C link-dead-code"
apt-get update -yqq
apt-get install -yqq -o=Dpkg::Use-Pty=0 \
  build-essential cmake binutils-dev libcurl4-openssl-dev zlib1g-dev \
  libdw-dev libiberty-dev jq unzip
rustc --version
cargo --version
named -V
cargo install -f cargo-kcov
cargo kcov --print-install-kcov-sh | sh
cargo kcov --no-fail-fast --all -- --verify --include-pattern=yardi/src
find target/cov -type l -delete
