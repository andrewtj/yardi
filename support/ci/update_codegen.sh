#!/usr/bin/env bash
set -eux -o pipefail
rustup component add rustfmt
rustc --version
cargo --version
pushd "codegen"
cargo run update
git checkout assets/cache
popd
git diff --minimal --exit-code
