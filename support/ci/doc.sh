#!/usr/bin/env bash
set -eux -o pipefail
rustc --version
cargo --version
cargo doc
